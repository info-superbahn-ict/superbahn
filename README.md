# superbahn

## 介绍
项目汇总，该项目是后面go重构的项目目录结构

## 项目开发规约

[SUPB 开发规约](doc/项目开发规约.md)

## 子项目设计报告

[SUPB MANAGER 设计报告](doc/supbmanager/design/manager_design_report.md)

## 子项目CLI手册

[SUPB MANAGER CLI手册](doc/supbmanager/cli/manager-v1.02.pdf)

## 子项目开发说明

1.  cmd/{子项目名称}/{主函数}
    ~~~
    该目录下创建程序main函数，具体可参考 cmd/supbmanager/main.go 
    main函数会转到初始化函数 internal/supbmanager/root/root.go
    ~~~
    
2.  internal/{子项目名称}/...
    ~~~
    该目录下开发实现该子项目的代码， 可参考 internal/supbmanager/... 的目录设计
    ~~~

3.  pkg/{子项目名称}/...
    ~~~
    改目录下开发实现sdk，每个模块可供其他模块调用的包，也可以自己调用， 可参考 pkg/supbmanager/... 的目录设计
    ~~~

4.   scripts/{子项目名称}/...
     ~~~
     编译、部署、运行脚本
     ~~~

5.   example/{子项目名称}/...
     ~~~
     测试用例，运行试例
     ~~~

6.   config/...
     ~~~
     配置文件
     ~~~

7.   doc/...
     ~~~
     文档
     ~~~
     
## 注意事项

1.  不要在其他项目目录先做修改，这是协同开发项目
2.  实在怕出问题可以开分支
