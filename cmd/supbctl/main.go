package main

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
)

func main() {
	// enable Ctrl C to quit beautifully
	ctx, cancel := context.WithCancel(context.Background())

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	// start to run
	if rootCmd,err := supbctl.NewCommand(ctx, filepath.Base(os.Args[0])); err != nil {
		fmt.Printf("new command %v",err)
	}else{
		if err = rootCmd.Execute(); err != nil {
			fmt.Printf("command execute %v", err)
			cancel()
		}
	}
}

