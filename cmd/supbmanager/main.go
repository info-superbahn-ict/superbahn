package main

import (
	"context"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/root"
	log "github.com/sirupsen/logrus"
	"path/filepath"

	"os"
	"os/signal"
	"syscall"
)

func main() {
	// enable Ctrl C to quit beautifully
	ctx, cancel := context.WithCancel(context.Background())

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()


	// log.SetReportCaller(true)
	log.SetFormatter(&log.TextFormatter{
		ForceColors:   true,
		DisableColors: false,
		ForceQuote: true,
		DisableQuote: false,
		PadLevelText: true,
		EnvironmentOverrideColors: true,
		//FieldMap: log.FieldMap{
		//        log.FieldKeyTime:  "@timestamp",
		//        log.FieldKeyLevel: "@level",
		//        log.FieldKeyMsg:   "@message",
		//},
	})
	log.SetLevel(log.DebugLevel)
	entry := log.WithContext(ctx).WithFields(log.Fields{
		"function": "main",
	})

	// todo start to run
	if rootCmd, err := controller.NewCommand(ctx, filepath.Base(os.Args[0])); err != nil {
		entry.Fatalf("new command %v", err)
	} else {
		if err = rootCmd.Execute(); err != nil {
			entry.Errorf("command execute %v", err)
			cancel()
		}
	}
}
