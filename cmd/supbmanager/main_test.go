package main

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"testing"
	"time"

	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
)

var count = int64(0)

func TestServer(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	serve := http.NewServeMux()
	serve.HandleFunc("/get", get)

	server := &http.Server{
		Addr:         "10.16.0.180:10020",
		ReadTimeout:  2 * time.Second,
		WriteTimeout: 2 * time.Second,
		Handler:      serve,
	}

	go func() {
		if err := server.ListenAndServe(); err != nil {
			fmt.Printf("stop")
		}
		fmt.Printf("listen closed")
	}()

	<-ctx.Done()
}

func get(w http.ResponseWriter, rq *http.Request) {

	count++
	//buffers, _ := ioutil.ReadAll(rq.Body)

	fmt.Printf("recv %v\n", count)

}

func TestKafka(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	nu, err := kafkaNervous.NewNervous(ctx, "../../config/nervous_config.json", "test")
	if err != nil {
		fmt.Printf("new kafka %v", err)
	}

	resp,err := nu.RPCCall(define.INIT_SPONGE_NERVOUS,"funcList")
	if err != nil {
		fmt.Printf("new kafka %v", err)
	}
	if resp !=nil {
		fmt.Printf("%v",resp)
	}

	//err = nu.Subscribe("jaeger_traces")
	//if err != nil {
	//	fmt.Printf("new kafka %v", err)
	//}
	//
	//fmt.Printf("start traces ")
	//tk := time.NewTicker(time.Second)
	//for {
	//	select {
	//	case <-tk.C:
	//		mg, err := nu.Receive("jaeger_traces")
	//		if err != nil {
	//			fmt.Printf("new kafka %v", err)
	//		}
	//
	//		fmt.Printf("topic:%v key: %v data:%v", mg.Topic, mg.SlaveKey, mg.Value)
	//	case <-ctx.Done():
	//		return
	//	}
	//}
}
