package main

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules"
	supbmods "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
)

func main() {
	// enable Ctrl C to quit beautifully
	ctx, cancel := context.WithCancel(context.Background())
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	//
	supbEnv := supbenv.NewSupbEnvs(ctx,"supb.env")
	supbMod := supbmods.NewSupbManager(ctx,"supb.mod")
	mods := modules.NewFactory(ctx,"supb.module")

	program := filepath.Base(os.Args[0])
	cmd := &cobra.Command{
		Use:   program,
		Short: program + " is manager program execute entry",
		RunE: func(cmd *cobra.Command, args []string) error {
			vpr := viper.New()
			supbEnv.ViperConfig(vpr)
			supbMod.ViperConfig(vpr)
			mods.ViperConfig(vpr)

			if err:= vpr.BindPFlags(cmd.Flags());err !=nil{
				return fmt.Errorf("viper bind pflags %v",err)
			}

			supbEnv.InitViper(vpr)
			supbMod.InitViper(vpr)
			mods.InitViper(vpr)

			// this config option
			supbEnv.OptionConfig(supbenv.WithFormat("text"))
			supbMod.OptionConfig(supbmods.WithRecorder(supbEnv.Recorder()))

			mods.OptionConfig(modules.WithSupbResMgr(supbMod.Res()))
			mods.OptionConfig(modules.WithSupbMetricsClr(supbMod.Metrics()))
			mods.OptionConfig(modules.WithSupbLogsClr(supbMod.Logs()))
			mods.OptionConfig(modules.WithStrategyClr(supbMod.StrategyGate()))
			mods.OptionConfig(modules.WithIndex(supbMod.Index()))
			mods.OptionConfig(modules.WithTrace(supbMod.Trace()))


			// initialize and run
			supbEnv.Initialize()
			supbMod.Initialize()
			mods.Initialize(modules.WithRPC(supbMod.RPC().RPC()))

			logger := supbEnv.Logger("supb")
			logger.Infof("Initialize finished")
			<-ctx.Done()

			if err := mods.Close() ;err !=nil{
				logger.Errorf("supb module close %v",err)
			}
			if err := supbMod.Close() ;err !=nil{
				logger.Errorf("supb mod close %v",err)
			}
			if err := supbEnv.Close() ;err !=nil{
				logger.Errorf("supb env close %v",err)
			}
			return nil
		},
	}

	supbEnv.InitFlags(cmd.Flags())
	supbMod.InitFlags(cmd.Flags())
	mods.InitFlags(cmd.Flags())

	if err := cmd.Execute(); err != nil {
			log.Errorf("command execute %v", err)
			cancel()
	}
}