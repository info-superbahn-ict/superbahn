package main

import (
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbsponge/models"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbsponge/routers"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/gredis"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/kafka"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/logging"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/setting"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func init() {
	//设置
	setting.Setup()
	//数据库
	models.Setup()
	//redis
	gredis.Setup()
	////日志
	logging.Setup()
	//rpc
	kafka.ListenOctopus()
}

func main() {
	//定时监听资源状态
	//go v1.ListenResourcesStatus(context.Background())

	gin.SetMode(setting.ServerSetting.RunMode)

	routersInit := routers.InitRouter()

	endPoint := fmt.Sprintf(":%d", setting.ServerSetting.HttpPort)

	server := &http.Server{
		Addr:    endPoint,
		Handler: routersInit,
	}

	log.Printf("[info] start http server listening %s", endPoint)

	server.ListenAndServe()
	defer kafka.Close()
}
