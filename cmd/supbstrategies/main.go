package main

import (
	"context"
	root "gitee.com/info-superbahn-ict/superbahn/internal/supbstrategies"
	"github.com/sirupsen/logrus"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
)

func main() {
	// enable Ctrl C to quit beautifully
	ctx, cancel := context.WithCancel(context.Background())

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors:   true,
		DisableColors: false,
	})
	logrus.SetLevel(logrus.DebugLevel)
	log := logrus.WithContext(ctx).WithFields(logrus.Fields{
		"function": "main",
	})

	// todo start to run
	if rootCmd, err := root.NewCommand(ctx, filepath.Base(os.Args[0])); err != nil {
		log.Fatalf("new command %v", err)
	} else {
		if err = rootCmd.Execute(); err != nil {
			log.Errorf("command execute %v", err)
			cancel()
		}
	}
}
