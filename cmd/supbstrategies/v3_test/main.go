package main

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbstrategies/v3_test/strategies"
	supbmods "gitee.com/info-superbahn-ict/superbahn/pkg/supbstrategies/supb-modules"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
)

func main() {
	// enable Ctrl C to quit beautifully
	ctx, cancel := context.WithCancel(context.Background())
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	//
	supbEnv := supbenv.NewSupbEnvs(ctx,"supb.env")
	supbMod := supbmods.NewSupbManager(ctx,"supb.mod")
	strategy := strategies.NewSupbStrategy(ctx,"supb.strategy")

	program := filepath.Base(os.Args[0])
	cmd := &cobra.Command{
		Use:   program,
		Short: program + " is manager program execute entry",
		RunE: func(cmd *cobra.Command, args []string) error {
			vpr := viper.New()
			supbEnv.ViperConfig(vpr)
			supbMod.ViperConfig(vpr)
			strategy.ViperConfig(vpr)

			if err:= vpr.BindPFlags(cmd.Flags());err !=nil{
				return fmt.Errorf("viper bind pflags %v",err)
			}

			supbEnv.InitViper(vpr)
			supbMod.InitViper(vpr)
			strategy.InitViper(vpr)

			// this config option
			supbEnv.OptionConfig()
			supbMod.OptionConfig()
			strategy.OptionConfig()

			// initialize and run
			supbEnv.Initialize(supbenv.WithFormat("json"))
			supbMod.Initialize()
			strategy.Initialize(strategies.WithRecorder(supbEnv.Recorder()),strategies.WithRPC(supbMod.RPC()))

			logger := supbEnv.Logger("supb")
			logger.Infof("Initialize finished")

			<-ctx.Done()

			if err := strategy.Close() ;err !=nil{
				logger.Errorf("supb module close %v",err)
			}
			if err := supbMod.Close() ;err !=nil{
				logger.Errorf("supb mod close %v",err)
			}
			if err := supbEnv.Close() ;err !=nil{
				logger.Errorf("supb env close %v",err)
			}
			return nil
		},
	}

	supbEnv.InitFlags(cmd.Flags())
	supbMod.InitFlags(cmd.Flags())
	strategy.InitFlags(cmd.Flags())

	if err := cmd.Execute(); err != nil {
			log.Errorf("command execute %v", err)
			cancel()
	}
}