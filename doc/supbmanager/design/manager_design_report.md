# SUPBMANAGER 设计报告

## 摘要 （是什么？）
&emsp;supbmanager是信息高铁资为管理和控制的组件提供服务的平台，主要功能是将管理strategy和被管理的resource进行关联，同时管理和保障strategy的稳定运行

## 简介 （1. 为什么要做？ 或者它存在的意义?  2. 主要设计方法）

&emsp;现有的资源管理框架是有特定管理目标的，提供管理的也是有边界的。信息高铁整个项目旨在统筹算力，跨域调控，管理的目标和边界都将更加广阔，supbmanager旨在提供更广泛目标和更大管理的边界。

&emsp;strategy是可自定义的，object也是可自定义抽象的，被关联的strategy和object可以实现各种各样的管理。

## 设计

### 总体构成

&emsp;supbmanager主要为strategy运行提供平台，组成部分包括:__*CLI和前端交互*__、 __*SPONGE同步*__和__*strategy管理*__。

- __*CLI和前端交互*__ 主要提供运行strategy、关联object和strategy、strategy的增删改查等交互接口。

- __*SPONGE同步*__ 主要负责同步管理object的信息和状态

- __*strategy管理*__ 主要负责维护和保障strategy的运行

#### 水平架构图

<div align="center">
<img src="img/水平架构图.png" height="60%" width="60%"/>
</div>

#### 垂直架构图

<div align="center">
<img src="img/垂直架构图.png" height="60%" width="60%"/>
</div>

#### 详细设计

##### CLI和前端交互

&emsp;CLI提供的功能包括bind、list

- bind: 将strategy和object关联

~~~
    测试样例
~~~
&emsp;前端交互的功能包括get、delete、load、upload、run、stop、restart、update、push、pull
- get: 获取和显示相应实体信息
~~~
   具体方法参数：
   
   //显示静态策略
   listStaticStrategies：{
   "op"     : "listStaticStrategies",
   "object" : "",
   "data"   : "",
   }
   
   //显示动态策略
   listRunningStrategies：{
   "op"     : "listRunningStrategies",
   "object" : "",
   "data"   : "",
   }
   
   //显示image信息
   listImages：{
   "op"     : "listImage",
   "object" : "",
   "data"   : "",
   }
   
   //显示resources统计
   listResourcesStatistic：{
   "op"     : "listResourcesStatistic",
   "object" : "",
   "data"   : "",
   }
   
   //获取特定静态策略
   getStaticStrategy：{
   "op"     : "getStaticStrategies",
   "object" : "name",
   "data"   : "",
   }
   
   //获取特定动态策略
   getRunningStrategy：{
   "op"     : "getRunningStrategies",
   "object" : "guid",
   "data"   : "",
   }
   
   //获取特定cluster
   getCluster：{
   "op"     : "getClusterInfo",
   "object" : "guid",
   "data"   : "",
   }
   
   //获取特定cluster相关resources
   getClusterResources：{
   "op"     : "getClusterResources",
   "object" : "guid",
   "data"   : "",
   }
   
   //获取静态cluster-info信息
   getStatisticClusterInfo：{
   "op"     : "getStatisticClusterInfo",
   "object" : "clusterGuid",
   "data"   : "",
   }
   
   //获取特定resources
   getSpecificResources：{
   "op"     : "getSpecificResources",
   "object" : "ResourceID",
   "data"   : "",
   }
   
   //获取关联关系图
   getRelationshipGraph：{
   "op"     : "getRelationshipGraph",
   "object" : "clusterGuid",
   "data"   : "",
   }
~~~

- delete: 删除相应实体（或实体属性）
~~~
    具体方法参数：
    
   //删除指定动态策略 
   deleteRunningStrategy：{
   "op"     : "deleteRunningStrategy",
   "object" : "guid",
   "data"   : "",
   }
   
   //删除指定静态策略
   deleteStaticStrategy：{
   "op"     : "deleteStaticStrategy",
   "object" : "name",
   "data"   : "",
   }
   
   //删除指定image
   deleteImage：{
   "op"     : "deleteImage",
   "object" : "image",
   "data"   : "",
   }
~~~

- load: 部署相应实体
~~~
    具体方法参数：
   
   //加载一个静态策略 
   loadStrategy：{
   "op"     : "loadStrategy",
   "object" : "name",
   "data"   : "",
   }
   
   //加载一个静态image
   loadImage：{
   "op"     : "loadImage",
   "object" : "image",
   "data"   : "",
   }
~~~

- upload: 重载相应实体
~~~
    具体方法参数：
   
   //重载一个动态策略 
   uploadStrategy：{
   "op"     : "uploadStrategy",
   "object" : "guid",
   "data"   : "",
   }
   
   //重载一个动态image
   uploadImage：{
   "op"     : "uploadImage",
   "object" : "image",
   "data"   : "",
   }
~~~

- run: 运行（并且在静态策略中删除，保留为动态策略）相应策略
~~~
    具体方法参数：
   
   //运行一个指定的静态策略 
   runStaticStrategy：{
   "op"     : "runStaticStrategy",
   "object" : "name",
   "data"   : "",
   }
~~~

- stop: 暂停（并且在动态策略中删除，保留为静态策略）相应策略
~~~
    具体方法参数：
    
   //暂停一个指定的动态策略 
   stopRunningStrategy：{
   "op"     : "stopRunningStrategy",
   "object" : "guid",
   "data"   : "",
   }
~~~

- restart: 重启（并且在动态策略中删除，保留为静态策略）相应策略
~~~
    具体方法参数：
    
   //重启一个指定动态策略 
   restartStrategy：{
   "op"     : "restartStrategy",
   "object" : "guid",
   "data"   : "",
   }
~~~

- update: 修改相应静态或动态策略
~~~
    具体方法参数：
    
   //修改一个指定静态策略 
   updateStaticStrategy：{
   "op"     : "updateStaticStrategy",
   "object" : "name",
   "data"   : "",
   "strategy"   : "s *strategies.Strategy",
   }
   
   //修改一个指定动态策略 
   updateRunningStrategy：{
   "op"     : "updateRunningStrategy",
   "object" : "guid",
   "data"   : "",
   "strategy"   : "s *strategies.Strategy",
   }
~~~

- push: 上传相应策略
~~~
    具体方法参数：
    
   //上传一个指定的静态策略 
   pushStaticStrategy：{
   "op"     : "pushStaticStrategy",
   "object" : "",
   "data"   : "data",
   }
~~~

- pull: 下载相应策略
~~~
    具体方法参数：
    
   //下载一个指定的静态策略 
   pullStaticStrategy：{
   "op"     : "pullStaticStrategy",
   "object" : "name",
   "data"   : "",
   }
~~~
##### SPONGE同步

&emsp;等待完善

##### strategy管理

&emsp;等待完善

## 总结和引用

&emsp;supbmanager还处在早起开发阶段，存在众多的不成熟，功能十分欠缺，需要努力建设