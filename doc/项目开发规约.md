# SUPB编程规约-golang

## go编码规约
程序编码规范采用：[Go 语言编码规范](Uber编码规约.md)，里面规范了很多很有用的编码，比较齐全，后续规约为项目补充规约
- 代码审查工具
- option配置模式推荐

## 目录规约

- cmd： 程序入口
- config： 配置文件
- internal： 程序主体代码
- pkg： supb sdk
- scripts： 脚本
- build：编译的二进制
- vendor：第三方包

## 结构定义规约

### 基本数据
&emsp;基本数据结构包括但不限于参数封装、实例抽象等。主要作用是存储、传递信息
~~~go
/*
	模式1: 结构成员构成简单（依赖、层次少）。
	规约：
		1.可以省略New函数，直接创建赋值，也可以实现默认的new函数。
		2.因为结构体简单，可返回指针也可返回结构体。
		3.需要实现DeepCopy函数
*/
type Mode1 struct {
    Key1 string
    Key2 string
}

// 可以省略
func NewMode2()*Mode2{
    // ...
}

func(r Mode1)DeepCopy()*Mode1{
    // ...
}

/*
	模式2: 结构成员构成复杂，结构内部成员嵌套层次多，体量大，例如，容器等实例的抽象。
	规约：	
		1.需要实现默认New函数，默认值在包中声明即可，传参New函数可省略。
		2.因为结构复杂，为减少传参开销，返回值用指针。
		3.需要实现DeepCopy函数
*/
type Mode2 struct {
    Key1 string
    //... 
    KeyN string
}

var const(
	DefaultKey1 = ""
	DefaultKey1 = ""
)

func NewMode2()*Mode2{
    // ...
}

// 可以省略
func NewMode2WithOption(option ...)*Mode2{
    // ...
}

func(r Mode1)DeepCopy()*Mode1{
    // ...
}
~~~

### 局部模块
&emsp;模块类数据结构包括但不限于服务管理等，主要功能是实现高内聚的模块，并且提供服务
~~~go
/*
	模式1：服务功能比较简单，体量小，内置开发，配置外传
	规约：
		1.需要带参数的New函数，默认new可省略，New函数做完初始化
		2.需要实现close函数
		3.指针传递
		4.option模式采用functional option，方便灵活添加
*/
type Service1 struct {
    Key1 string
    Key2 string
}

type option func(srv *Service1)

// 可省略
func NewService1() (*Service1,error) {
    // ...
}

// option 的定义可放在包内
func NewService1WithOption(opt ...option) (*Service1,error) {
    // default
	s := Service1{}
	// appley function to set
	for _,apply := range opt {
		apply(&s)
	}
}

func WithLogger(log *logrus.Entry) option {
	return func(manager *Manager) {
		manager.logger = log
	}
}

func (r Service1)Close() {
    
}

/*
	模式2：服务功能复杂，体量大，可能提供给第三方人员开发，需要支持自配置
	规约：
		1.需要默认new函数，做简单初始化，后续初始化由init去做
		2.需要实现close、Init函数
		3.需要实现上层需要的函数
		4.指针传递
*/
type Service2 struct {
    Key1 string
    Key2 string
}

// 可省略
func NewService2() *Service2 {
    // ...
}

// 将自己的参数添加到参数选项中
func (r Service2) InitFlags(flagSet *FlagSet) {
    
}

// 从viper获取配置信息
func (r Service2) InitFromViper(v *Viper) {

}

// 获取系统配置，例如日志等
func (r Service2) InitFromOption(opt *option) {
    
}

func (r Service2)Close() {
    
}
~~~
### 全局模块
&emsp;全局模块类数据结构包括但不限于日志、性能采集等，功能是刚需，使用非常频繁
~~~go
/*
	模式1：模块简单，无需配置
	规约： 
		1.初始化在go语言包初始函数init中完成即可
*/
var log = logrus.logger

func init() {
	
}

/*
	模式2：需要配置，会启动一定的服务
	规约： 
		1.提供Initialize、close函数
*/

var nervous = Nervous{}

// option 在包中定义
func Initialize(opt *Option){
	// 可能会 go run
}

func Close(){
}
~~~

## 编码规约

### 命名规约
命名尽量简短，不要刻意简短，区分度要高
- 结构、函数、变量命名使用驼峰命名法, 小写开头表示外部不可见（private），大写开头表示外部可见（public）
- 接口以er结尾
- 目录命令一律用小写，尽量缩写，多关键可以用 - 连接
- 文件命令一律小写，关键字不缩写，尽量少用关键字缩短命令长度，多关键字用 _ 连接

### 日志&错误处理规约
&emsp;supb模块越来越大，调用栈越来越深，错误一直上报不是很好的解决方法，局部模块和全局模块内部也可打印日志，遵循一个原则，日志层次少，深栈调用，尽量上报后在打印。提高程序可阅读性。

### 尾处理规约
&emsp;除开宕机外，程序主动结束或者Ctrl+C结束都应该处理好结尾工作，以便下次启动不报错，或者能重续断点。


### 注释规约
&emsp;不同开发者开发的组件之间的调用接口必须有注释，包括参数、返回说明，以及简单举例等等，要求是调用包是，通过IDE悬挂鼠标在包上可以看到注释。
- 普通注释，在变量和方法名上面，不空行
- 弃用注释和BUG注释在
~~~go
/*
（弃用添加 Deprecated：）
（有BUG添加 Deprecated：）
（普通注释规约）:
	1.功能描述，哪些是必须参数
	2.参数、返回描述
	3.简单exmaple
*/
func test(){
}
~~~

### 检查规约
&emsp;go编码规约中已推荐代码审查工具golangci-lint，后续将用它来规范代码

## 基础包规约

### 日志
- github.com/sirupsen/logrus（欢迎推荐其他更好用的日志），logrus是现在go语言最受欢迎的日志包之一，简介好用，基本功能齐全。兼容go默认标准日志，提供hook机制，提供field来鼓励日志结构化，可热拔插。
- [uber-go/zap](https://www.topgoer.com/%E9%A1%B9%E7%9B%AE/log/ZapLogger.html)，性能很好，基本功能齐全

### 配置
&emsp;viper + corba是大型项目常用的参数配置包，比较方便

### 性能采集
&emsp;jaeger trace是我们项目开发后期做性能优化时准备用到的数据采集包

### 编码解码
&emsp;encode/json、gopkg.in/yaml.v2是对yaml和json文件解码的基础包，能满足目前我们的基本需求，后期可能更换

### 数据监控
&emsp;基本数据监控目前使用 github.com/shirou/gopsutil，更细粒度的数据监控 github.com/containerd/cgroups + github.com/hodgesds/perf-utils

### 资源调控
&emsp;github.com/containerd/cgroups

### 其他包规约
&emsp;目前还在开发中，有更好的包，比较重要的包都会写入规约