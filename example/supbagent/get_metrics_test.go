package supbagent

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"testing"
	"time"

	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
)

func TestGuid(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	nu, _ := kafkaNervous.NewNervous(ctx, "../../config/nervous_config.json", "test_guid")

	//_ = nu.Run()

	// _ = nu.RPCRegister(define.R, func(args ...interface{}) (interface{}, error) {
	// 	// todo deal the metrics
	// 	return "", nil
	// })

	// time.Sleep(time.Second * time.Duration(2))
	// 0126d73600
	fmt.Printf("open push")

	// todo open metrics push
	_, _ = nu.RPCCall("013f1bf300", define.RPCFunctionNameOfObjectRunningOnAgentForOpenMetricsPush, "test_guid")

	time.Sleep(time.Second * time.Duration(2))
	fmt.Printf("close push")

	// todo close metrics push
	// _, _ = nu.RPCCall("01b33c6800", define.RPCFunctionNameOfObjectRunningOnAgentForOpenTracePush, manager_containers.MetricsPushOff)

	<-ctx.Done()
	_ = nu.Close()
}
