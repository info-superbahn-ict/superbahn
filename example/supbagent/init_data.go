package supbagent

import (
	"context"
	"fmt"

	//"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/apis"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/options"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources/manager_containers/containers"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/third_party/docker"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
)

var contain *containers.Container

// func InitExampleData(api *apis.Manager, opt *options.Options) error {
// 	// 启动 hotrod 测试容器
// 	imageTag := "jaeger-supbtest:v0.1"
// 	// imageTag := "supb-car-test:v0.1"
// 	env := make(map[string]string)
// 	ports := make(map[string]string)

// 	//env["JAEGER_AGENT_HOST"] = "10.16.0.180"
// 	//env["JAEGER_AGENT_PORT"] = "10035"
// 	//env["JAEGER_SAMPLER_TYPE"] = "remote"
// 	//env["JAEGER_SAMPLING_ENDPOINT"] = "http://10.16.0.180:10037/sampling"

// 	env["WOEKERS"] = "10"
// 	env["INTERVAL"] = "5000" // 单位微妙
// 	env["JAEGER_AGENT_HOST_PORT"] = opt.JaegerAgentHostPort // agent 6831
// 	env["SUPB_GUID"] = "test_guid"
// 	// ports["10038"] = "8000"

// 	hostGuid := api.GetHostManager().GetHostGuid()
// 	// uuid作为containerName
// 	containerName := uuid.NewV4().String()

// 	var err error
// 	guid, err := api.GetContainerManager().CreateContainerWithPortsForCar(imageTag, containerName, hostGuid, env, ports, api.GetAgentNervous())
// 	if err != nil {
// 		return err
// 	}
// 	contain, err = api.GetContainerManager().GetContainer(guid)
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }

func InitExample(ctx context.Context) {
	// 启动 hotrod 测试容器
	imageTag := "hotrod-container:v0.2"
	env := make(map[string]string)
	ports := make(map[string]string)

	env["JAEGER_AGENT_HOST"] = "10.16.0.180"
	env["JAEGER_AGENT_PORT"] = "10035"
	env["JAEGER_SAMPLING_ENDPOING"] = "http://10.16.0.180:10037/"

	ports["10038"] = "8080"

	// uuid作为containerName
	containerName := uuid.NewV4().String()

	controller, err := docker.NewDockerController(ctx, options.DefaultDockerUrl)

	if err != nil {
		logrus.WithContext(context.Background()).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "InitJaegerEnv",
		}).Infof("Init Failure: %v", err)
	}

	// todo 增加端口映射配置
	_, err = controller.StartContainerWithPorts(imageTag, containerName, env, ports)

	if err != nil {
		logrus.WithContext(context.Background()).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "InitExampleData",
		}).Infof("Init Failure: %v", err)
	}
}

func InitExampleNoPort(ctx context.Context) {
	// 启动 hotrod 测试容器
	imageTag := "hotrod-container:v0.1"
	env := make(map[string]string)
	ports := make(map[string]string)

	env["JAEGER_AGENT_HOST"] = "10.16.0.180"
	env["JAEGER_AGENT_PORT"] = "10035"
	env["JAEGER_SAMPLING_ENDPOING"] = "http://10.16.0.180:10037/"

	ports["10038"] = "8080"

	// uuid作为containerName
	containerName := uuid.NewV4().String()

	controller, err := docker.NewDockerController(ctx, options.DefaultDockerUrl)

	if err != nil {
		logrus.WithContext(context.Background()).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "InitJaegerEnv",
		}).Infof("Init Failure: %v", err)
	}

	// todo 增加端口映射配置
	_, err = controller.StartContainer(imageTag, containerName, env)

	if err != nil {
		logrus.WithContext(context.Background()).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "InitExampleData",
		}).Infof("Init Failure: %v", err)
	}
}

func ClearInit() error {
	cll, err := docker.NewDockerController(context.Background(), options.DefaultDockerUrl)
	if err != nil {
		return fmt.Errorf("create client %v", err)
	}

	if err = cll.Stop(contain.ContainerId); err != nil {
		return fmt.Errorf("stop jaeger %v", err)
	}

	if err = cll.Remove(contain.ContainerId); err != nil {
		return fmt.Errorf("remove jaeger %v", err)
	}
	return nil
}

func InitJaegerQuery(ctx context.Context) {
	imageTag := "jaegertracing/jaeger-query:latest"
	env := make(map[string]string)
	ports := make(map[string]string)

	env["SPAN_STORAGE_TYPE"] = "elasticsearch"
	env["ES_SERVER_URLS"] = "http://152.136.134.100:9200"

	ports["10040"] = "16686"

	// uuid作为containerName
	containerName := uuid.NewV4().String()

	controller, err := docker.NewDockerController(ctx, options.DefaultDockerUrl)

	if err != nil {
		logrus.WithContext(context.Background()).WithFields(logrus.Fields{
			define.RPCFunctionNameOfAgentForCreateObject: "InitJaegerQuery",
		}).Infof("Init Failure: %v", err)
	}

	// todo 增加端口映射配置
	_, err = controller.StartContainerWithPorts(imageTag, containerName, env, ports)

	if err != nil {
		logrus.WithContext(context.Background()).WithFields(logrus.Fields{
			define.RPCFunctionNameOfAgentForCreateObject: "InitJaegerQuery",
		}).Infof("Init Failure: %v", err)
	}

}
