package supbagent

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"testing"

	"gitee.com/info-superbahn-ict/superbahn/internal/supbagent/root/config"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/apis"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/collector"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/controller"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/options"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	//"github.com/sirupsen/logrus"
)

// func TestInitExampleData(t *testing.T) {
// 	ctx, cancel := context.WithCancel(context.Background())

// 	sig := make(chan os.Signal, 1)
// 	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
// 	go func() {
// 		<-sig
// 		cancel()
// 	}()

// 	api := getAPI(ctx)

// 	if api != nil {
// 		InitExampleData(api,nil)

// 		map_container := api.GetContainerManager().ListContainers()
// 		for k := range map_container {
// 			logrus.WithContext(context.Background()).WithFields(logrus.Fields{
// 				define.LogsPrintCommonLabelOfFunction: "TestInitExampleData",
// 			}).Infof("guid: %v", k)
// 		}

// 	}

// }

func TestInitExample(t *testing.T) {
	InitExample(context.Background())
}

func TestInitJaegerQuery(t *testing.T) {
	InitJaegerQuery(context.Background())
}

func TestInitExampleNoPort(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	InitExampleNoPort(ctx)
}

//func TestClearInit(t *testing.T) {
//	ctx, cancel := context.WithCancel(context.Background())
//
//	sig := make(chan os.Signal, 1)
//	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
//	go func() {
//		<-sig
//		cancel()
//	}()
//
//	//api := getAPI(ctx)
//
//	//ClearInit(api)
//}

func getAPI(ctx context.Context) *apis.Manager {
	opt := options.Options{}
	config.SetDefaultOpts(&opt)

	Nic, err := kafkaNervous.NewNervous(ctx, "../../config/nervous_config.json", define.RPCCommonGuidOfAgent)
	//Nic.Run()
	if err != nil {
		fmt.Println("Init kafkaNervous fail")
		return nil
	}

	collector, err := collector.NewCollector(ctx, &opt)
	//collector, err := collector.NewCollector(ctx, &opt, Nic)

	if err != nil {
		fmt.Println("Init collector fail")
		return nil
	}

	controller, err := controller.NewController(ctx, &opt)
	if err != nil {
		fmt.Println("Init controller fail")
		return nil
	}

	api, err := apis.NewManager(ctx, &opt, collector, controller, Nic, define.RPCCommonGuidOfAgent)

	if err != nil {
		fmt.Println("Init api fail")
		return nil
	}

	return api
}

func TestTemp(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	Nic, err := kafkaNervous.NewNervous(ctx, "../../config/nervous_config.json", "test")
	//Nic.Run()
	if err != nil {
		fmt.Printf("Init kafkaNervous fail %v", err)
		return
	}

	//Nic.RPCRegister(define.RPCFunctionNameOfManagerStrategyCollectorObjectLogs, func(args ...interface{}) (interface{}, error) {
	//	fmt.Printf("recv %v",args)
	//	return "ok",nil
	//})
	//
	//
	resp, err := Nic.RPCCall("0126d73600", define.RPCFunctionNameOfAgentForListObject, "test")
	//resp, err := Nic.RPCCall("015d30a700", define.RPCFunctionNameOfObjectRunningOnAgentForOpenTracePush, "test")
	//resp, err := Nic.RPCCall("01f6c79200", define.RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush, manager_containers.MetricsPushOff)
	if err != nil {
		fmt.Printf("Init kafkaNervous fail %v", err)
		return
	}

	<-ctx.Done()

	//resp, err := Nic.RPCCall("01bd15e400", define.RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush, manager_containers.MetricsPushOff)
	//if err != nil {
	//	fmt.Printf("Init kafkaNervous fail %v", err)
	//	return
	//}
	fmt.Printf("resp %v ", resp)
}
