package supbmanager

import (
	"crypto/rand"
	"fmt"
	"strings"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/level"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/options"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_clusters"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_strategies"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_strategies/strategies"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/sponge_resources"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
	"gitee.com/info-superbahn-ict/superbahn/third_party/etcd"
	"gopkg.in/yaml.v2"
)

func InitExampleData(etcd etcd.Recorder) {
	userList := map[string]string{
		"default":        "test_token",
		"tangshibo":      "test_token",
		"wutianze":       "test_token",
		"xujingkai":      "test_token",
		"guilaoshi":      "test_token",
		"yaolaoshi":      "test_token",
		"fanghaolei":     "test_token",
		"duhanlin":       "test_token",
		"yezhuangzhuang": "test_token",
		"mayingjie":      "test_token",
	}

	bts, err := yaml.Marshal(userList)
	if err != nil {
		fmt.Printf("encode userlist %v", err)
	}

	err = etcd.Put(resources.UserIdListSavePath, string(bts))
	if err != nil {
		fmt.Printf("put userlist %v", err)
	}

	const rct = 5
	var (
		guis  [rct]string
		des   [rct]string
		hosts [rct]string
	)

	for i := 0; i < rct; i++ {
		randBytes := make([]byte, 8)
		if _, err := rand.Read(randBytes); err != nil {
			fmt.Printf("generate name %v", err)
			return
		}
		guis[i] = strings.Join([]string{"guid", fmt.Sprintf("%x", randBytes)}, "_")

		randBytes = make([]byte, 2)
		if _, err = rand.Read(randBytes); err != nil {
			fmt.Printf("generate name %v", err)
			return
		}
		des[i] = strings.Join([]string{"description", fmt.Sprintf("%x", randBytes)}, "_")

		randBytes = make([]byte, 2)
		if _, err = rand.Read(randBytes); err != nil {
			fmt.Printf("generate name %v", err)
			return
		}
		hosts[i] = strings.Join([]string{"host", fmt.Sprintf("%x", randBytes)}, "_")
	}

	for i := 0; i < rct; i++ {
		res := &spongeregister.Resource{
			GuId:        guis[i],
			Description: des[i],
			Status:      spongeregister.StatusReady,
			Area:        "cluster1",
			PreNode:     hosts[i],
			SubNode:     "",
			OType:       i,
			IsMonitor:   1,
			IsControl:   1,
		}

		bts, err = yaml.Marshal(res)
		if err != nil {
			fmt.Printf("encode device1 %v", err)
		}
		if err = etcd.Put(fmt.Sprintf(sponge_resources.ResourcesSavePathPrefixFormat, "default")+res.GuId, string(bts)); err != nil {
			fmt.Printf("put device1 %v", err)
		}
	}

	strategy1 := strategies.Strategy{
		StrategyVersion:          strategies.DefaultVersion,
		StrategyName:             "LOG_DUMP_ELK",
		StrategyKind:             level.MasterLevel1,
		StrategyImage:            "log_dump_to_elk",
		StrategyShortDescription: strategies.DefaultShortDescription,
		StrategyDescription:      strategies.DefaultDescription,

		StrategyContainerEnv: map[string]string{
			"ETCD_URL":     options.DefaultedEtcd1,
			"META_KEY":     fmt.Sprintf(manager_clusters.ClustersSavePathPrefixFormat, "{USER}"),
			"META_OUT_KEY": fmt.Sprintf(manager_clusters.ClustersResSavePathPrefixFormat, "{USER}"),

			"ELK_URL": options.DefaultedElkUrl,
		},
	}

	bts, err = yaml.Marshal(strategy1)
	if err != nil {
		fmt.Printf("encode strategy1 %v", err)
	}
	if err = etcd.Put(fmt.Sprintf(manager_strategies.StaticStrategiesSavePathPrefixFormat, "default")+strategy1.StrategyName, string(bts)); err != nil {
		fmt.Printf("put strategy1 %v", err)
	}

	strategy2 := strategies.Strategy{
		StrategyVersion:          strategies.DefaultVersion,
		StrategyName:             "METRICS_STATISTIC",
		StrategyKind:             level.MasterLevel2,
		StrategyImage:            "metrics_statistic",
		StrategyShortDescription: strategies.DefaultShortDescription,
		StrategyDescription:      strategies.DefaultDescription,

		StrategyContainerEnv: map[string]string{
			"ETCD_URL":     options.DefaultedEtcd1,
			"META_KEY":     fmt.Sprintf(manager_clusters.ClustersSavePathPrefixFormat, "{USER}"),
			"META_OUT_KEY": fmt.Sprintf(manager_clusters.ClustersResSavePathPrefixFormat, "{USER}"),

			"ELK_URL": options.DefaultedElkUrl,
		},
	}

	bts, err = yaml.Marshal(strategy2)
	if err != nil {
		fmt.Printf("encode strategy1 %v", err)
	}
	if err = etcd.Put(fmt.Sprintf(manager_strategies.StaticStrategiesSavePathPrefixFormat, "default")+strategy2.StrategyName, string(bts)); err != nil {
		fmt.Printf("put strategy1 %v", err)
	}

	strategy3 := strategies.Strategy{
		StrategyVersion:          strategies.DefaultVersion,
		StrategyName:             "TRACE_DUMPER",
		StrategyKind:             level.MasterLevel3,
		StrategyImage:            "trace_dump_to_elk",
		StrategyShortDescription: strategies.DefaultShortDescription,
		StrategyDescription:      strategies.DefaultDescription,

		StrategyContainerEnv: map[string]string{
			"ETCD_URL":     options.DefaultedEtcd1,
			"META_KEY":     fmt.Sprintf(manager_clusters.ClustersSavePathPrefixFormat, "{USER}"),
			"META_OUT_KEY": fmt.Sprintf(manager_clusters.ClustersResSavePathPrefixFormat, "{USER}"),

			"ELK_URL": options.DefaultedElkUrl,
		},
	}

	bts, err = yaml.Marshal(strategy3)
	if err != nil {
		fmt.Printf("encode strategy1 %v", err)
	}
	if err = etcd.Put(fmt.Sprintf(manager_strategies.StaticStrategiesSavePathPrefixFormat, "default")+strategy3.StrategyName, string(bts)); err != nil {
		fmt.Printf("put strategy1 %v", err)
	}

	strategy4 := strategies.Strategy{
		StrategyVersion:          strategies.DefaultVersion,
		StrategyName:             "SIMPLE_TEST",
		StrategyKind:             level.MasterLevel1,
		StrategyImage:            "simple_test",
		StrategyShortDescription: strategies.DefaultShortDescription,
		StrategyDescription:      strategies.DefaultDescription,

		StrategyContainerEnv: map[string]string{
			"ETCD_URL":     options.DefaultedEtcd1,
			"META_KEY":     fmt.Sprintf(manager_clusters.ClustersSavePathPrefixFormat, "{USER}"),
			"META_OUT_KEY": fmt.Sprintf(manager_clusters.ClustersResSavePathPrefixFormat, "{USER}"),

			"ELK_URL": options.DefaultedElkUrl,
			"TEST_GUID":"01d6bc9b00",
		},
	}

	bts, err = yaml.Marshal(strategy4)
	if err != nil {
		fmt.Printf("encode strategy4 %v", err)
	}
	if err = etcd.Put(fmt.Sprintf(manager_strategies.StaticStrategiesSavePathPrefixFormat, "default")+strategy4.StrategyName, string(bts)); err != nil {
		fmt.Printf("put strategy4 %v", err)
	}

	strategy5 := strategies.Strategy{
		StrategyVersion:          strategies.DefaultVersion,
		StrategyName:             "METRICS_STATISTIC_2",
		StrategyKind:             level.MasterLevel2,
		StrategyImage:            "metrics_statistic_full_image",
		StrategyShortDescription: strategies.DefaultShortDescription,
		StrategyDescription:      strategies.DefaultDescription,

		StrategyContainerEnv: map[string]string{
			"ETCD_URL":     options.DefaultedEtcd1,
			"META_KEY":     fmt.Sprintf(manager_clusters.ClustersSavePathPrefixFormat, "{USER}"),
			"META_OUT_KEY": fmt.Sprintf(manager_clusters.ClustersResSavePathPrefixFormat, "{USER}"),

			"ELK_URL": options.DefaultedElkUrl,
		},
	}

	bts, err = yaml.Marshal(strategy5)
	if err != nil {
		fmt.Printf("encode strategy5 %v", err)
	}
	if err = etcd.Put(fmt.Sprintf(manager_strategies.StaticStrategiesSavePathPrefixFormat, "default")+strategy5.StrategyName, string(bts)); err != nil {
		fmt.Printf("put strategy5 %v", err)
	}

	strategy6 := strategies.Strategy{
		StrategyVersion:          strategies.DefaultVersion,
		StrategyName:             "TRACE_DUMPER_2",
		StrategyKind:             level.MasterLevel3,
		StrategyImage:            "trace_dump_to_etcd",
		StrategyShortDescription: strategies.DefaultShortDescription,
		StrategyDescription:      strategies.DefaultDescription,

		StrategyContainerEnv: map[string]string{
			"ETCD_URL":     options.DefaultedEtcd1,
			"META_KEY":     fmt.Sprintf(manager_clusters.ClustersSavePathPrefixFormat, "{USER}"),
			"META_OUT_KEY": fmt.Sprintf(manager_clusters.ClustersResSavePathPrefixFormat, "{USER}"),

			"ELK_URL": options.DefaultedElkUrl,
		},
	}

	bts, err = yaml.Marshal(strategy6)
	if err != nil {
		fmt.Printf("encode strategy6 %v", err)
	}
	if err = etcd.Put(fmt.Sprintf(manager_strategies.StaticStrategiesSavePathPrefixFormat, "default")+strategy6.StrategyName, string(bts)); err != nil {
		fmt.Printf("put strategy6 %v", err)
	}
}

type Example struct {
}

func init() {

}
