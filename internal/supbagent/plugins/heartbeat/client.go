package main

import (
	"context"
	"runtime"
	"strconv"
	"time"

	"gitee.com/info-superbahn-ict/superbahn/pkg/plugins"
	"gitee.com/info-superbahn-ict/superbahn/pkg/register"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/apis"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
	"github.com/sirupsen/logrus"
)

func PluginsName() string {
	return "agent.heartbeat"
}

func NewPlugin(ctx context.Context, p *apis.Manager) (plugins.PluginFactory, error) {
	return &NervousClientPlugin{
		ctx: ctx,
		api: p,
	}, nil
}

type NervousClientPlugin struct {
	ctx context.Context
	api *apis.Manager
}

func (r *NervousClientPlugin) Run() {

	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "agent.heartbeat.Run",
	})

	name := PluginsName()
	log.Infof("plugins %v initialization complete", name)

	ttk := time.NewTicker(time.Duration(1000) * time.Millisecond)
	cpuNum := runtime.NumCPU()
	for {
		select {
		case <-ttk.C:

			syncInfo := &spongeregister.SyncInfo{
				Guids:   make(map[string]int),
				Metrics: make(map[string]spongeregister.SyncMetric),
			}

			// add agent|server status,  metrics
			agentGuid := r.api.GetHostManager().GetHostGuid()
			syncInfo.Guids[agentGuid] = spongeregister.StatusReady

			syncInfo.Metrics[agentGuid] = spongeregister.SyncMetric{
				CpuSet:   "0-" + strconv.Itoa(cpuNum-1),
				CpuRatio: 1.0,
			}

			// add container status,  metrics
			ctList := r.api.GetContainerManager().ListContainers()
			for guid, ctn := range ctList {
				syncInfo.Guids[guid] = ctn.Status

				limitation, err := r.api.GetContainerManager().GetContainerResource(guid)
				if err != nil {
					continue
				}

				syncInfo.Metrics[guid] = spongeregister.SyncMetric{
					CpuSet:   limitation.CpuSets,
					CpuRatio: limitation.CpuRatio,
				}
			}

			if err := register.UpdateStatusDevice(r.api.GetAgentNervous(), syncInfo); err != nil {
				log.Errorf("sync device failed: %v", err)
			}
			//log.Debugf("send beat %v",syncInfo)
		case <-r.ctx.Done():
			log.Infof("plugins %v closed", name)
			return
		}
	}

}

func (r *NervousClientPlugin) Close() {

}

func (r *NervousClientPlugin) Call(f string, args ...interface{}) (interface{}, error) {
	return nil, nil
}
