package main

import (
	"context"

	"gitee.com/info-superbahn-ict/superbahn/pkg/plugins"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/apis"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

func PluginsName() string {
	return "agent.nervous.host.cli"
}

func NewPlugin(ctx context.Context, p *apis.Manager) (plugins.PluginFactory, error) {
	return &AgentNervousClientPlugin{
		ctx: ctx,
		api: p,
	}, nil
}

type AgentNervousClientPlugin struct {
	ctx context.Context
	api *apis.Manager
}

func (r *AgentNervousClientPlugin) Run() {

	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "agent.plugin.host.cli",
	})

	if err := r.api.GetAgentNervous().RPCRegister(define.RPCFunctionNameOfAgentForListObject, r.list); err != nil {
		log.Errorf("spongeregister %v", err)
		return
	}

	if err := r.api.GetAgentNervous().RPCRegister(define.RPCFunctionNameOfAgentForGetAgentInfo, r.get); err != nil {
		log.Errorf("spongeregister %v", err)
		return
	}

	if err := r.api.GetAgentNervous().RPCRegister(define.RPCFunctionNameOfAgentForDeleteObject, r.delete); err != nil {
		log.Errorf("spongeregister %v", err)
		return
	}

	if err := r.api.GetAgentNervous().RPCRegister(define.RPCFunctionNameOfAgentForCreateObject, r.create); err != nil {
		log.Errorf("spongeregister %v", err)
		return
	}

	if err := r.api.GetAgentNervous().RPCRegister(define.RPCFunctionNameOfAgentForDeployment, r.deploy); err != nil {
		log.Errorf("spongeregister %v", err)
		return
	}

	if err := r.api.GetAgentNervous().RPCRegister(define.RPCFunctionNameOfAgentForDeployment, r.deploy); err != nil {
		log.Errorf("spongeregister %v", err)
		return
	}

	name := PluginsName()
	log.Infof("plugins %v initialization complete", name)

	<-r.ctx.Done()
	log.Infof("plugins %v complete closed", name)
}

func (r *AgentNervousClientPlugin) Close() {

}

func (r *AgentNervousClientPlugin) Call(f string, args ...interface{}) (interface{}, error) {
	return nil, nil
}
