package main

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/options"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_clusters"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
)

func TestRpc(t *testing.T) {
	nu, err := kafkaNervous.NewNervous(context.Background(), "../../../../config/nervous_config.json", "test")
	if err != nil {
		fmt.Printf("new nerouvs client %v", err)
		return
	}

	//_ = nu.Run()

	resp, err := nu.RPCCallCustom("container_guid_55f8c76e77eb2675", 100, 500, define.RPCFunctionNameOfObjectRunningOnAgentForStartObject)
	if err != nil {
		fmt.Printf("call create %v", err)
		return
	}

	_ = nu.Close()

	fmt.Printf("%s\n", resp)
}

func TestCreate(t *testing.T) {
	nu, err := kafkaNervous.NewNervous(context.Background(), "../../../../config/nervous_config.json", "test_new_container")
	if err != nil {
		fmt.Printf("new nerouvs client %v", err)
		return
	}
	fmt.Printf("new nerouvs client ")

	randBytes := make([]byte, 32)
	if _, err = rand.Read(randBytes); err != nil {
		fmt.Printf("generate name %v", err)
		return
	}
	StrategyContainerName := strings.Join([]string{"simple-test", fmt.Sprintf("%x", randBytes)}, "-")

	testTag := spongeregister.Tag{
		Key:   "test",
		Type:  "string",
		Value: "test",
	}

	info := &clireq.ClientRequest{
		ListAll:       clireq.ListAll,
		ContainerName: StrategyContainerName,
		ImageTag:      "jamming-cpu-core-1:v0.1",
		Envs: map[string]string{
			"ETCD_URL":     options.DefaultedEtcd1,
			"META_KEY":     fmt.Sprintf(manager_clusters.ClustersSavePathPrefixFormat, "default"),
			"META_OUT_KEY": fmt.Sprintf(manager_clusters.ClustersResSavePathPrefixFormat, "default"),
		},
		Tags: []spongeregister.Tag{testTag},
	}

	bts, err := json.Marshal(info)
	if err != nil {
		fmt.Printf("marshall info %v", bts)
		return
	}

	resp, err := nu.RPCCallCustom("0126d73600", 10, 500, define.RPCFunctionNameOfAgentForCreateObject, string(bts))
	if err != nil {
		fmt.Printf("call create %v", err)
		return
	}

	_ = nu.Close()

	fmt.Printf("%s\n", resp)
}

func TestDeployment(t *testing.T) {
	nu, err := kafkaNervous.NewNervous(context.Background(), "../../../../config/nervous_config.json", "test_deployment5")
	if err != nil {
		fmt.Printf("new nerouvs client %v", err)
		return
	}
	fmt.Printf("new nerouvs client ")

	reqNum := 3

	reqList := make([]clireq.ClientRequest, reqNum)

	for i := 0; i < reqNum; i++ {

		testTag := spongeregister.Tag{
			Key:   "applicationKey",
			Type:  "string",
			Value: "test",
		}

		info := clireq.ClientRequest{
			ListAll:  clireq.ListAll,
			ImageTag: "39.101.140.145:5000/interference-cpu-core-1:v0.1",
			Envs: map[string]string{
				"ETCD_URL":     options.DefaultedEtcd1,
				"META_KEY":     fmt.Sprintf(manager_clusters.ClustersSavePathPrefixFormat, "default"),
				"META_OUT_KEY": fmt.Sprintf(manager_clusters.ClustersResSavePathPrefixFormat, "default"),
			},
			Tags: []spongeregister.Tag{testTag},
		}

		reqList[i] = info
	}

	bts, err := json.Marshal(reqList)

	if err != nil {
		fmt.Printf("marshall info %v", bts)
		return
	}

	fmt.Printf("prepare request:  %v", reqList)

	resp, err := nu.RPCCallCustom("01ad28f000", 10, 500, define.RPCFunctionNameOfAgentForDeployment, string(bts))
	if err != nil {
		fmt.Printf("call create %v", err)
		return
	}

	_ = nu.Close()

	fmt.Printf("%s\n", resp)
}

func TestDelete(t *testing.T) {
	nu, err := kafkaNervous.NewNervous(context.Background(), "../../../../config/nervous_config.json", "test_new_container")
	if err != nil {
		fmt.Printf("new nerouvs client %v", err)
		return
	}

	//nu.Run()

	info := &clireq.ClientRequest{
		Guid: "container_guid_80cc03456294cbba",
	}

	bts, err := json.Marshal(info)
	if err != nil {
		fmt.Printf("marshall info %v", bts)
		return
	}

	resp, err := nu.RPCCallCustom(define.RPCCommonGuidOfAgent, 30, 500, define.RPCFunctionNameOfAgentForDeleteObject, string(bts))
	if err != nil {
		fmt.Printf("call create %v", err)
		return
	}

	_ = nu.Close()

	fmt.Printf("%s\n", resp)
}

func TestStop(t *testing.T) {
	nu, err := kafkaNervous.NewNervous(context.Background(), "../../../../config/nervous_config.json", "test_new_container")
	if err != nil {
		fmt.Printf("new nerouvs client %v", err)
		return
	}

	//nu.Run()

	info := &clireq.ClientRequest{
		Guid: "container_guid_80cc03456294cbba",
	}

	bts, err := json.Marshal(info)
	if err != nil {
		fmt.Printf("marshall info %v", bts)
		return
	}

	resp, err := nu.RPCCallCustom(define.RPCCommonGuidOfAgent, 30, 500, define.RPCFunctionNameOfAgentForDeleteObject, string(bts))
	if err != nil {
		fmt.Printf("call create %v", err)
		return
	}

	_ = nu.Close()

	fmt.Printf("%s\n", resp)
}
