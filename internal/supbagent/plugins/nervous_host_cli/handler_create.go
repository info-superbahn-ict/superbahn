package main

import (
	"encoding/json"
	"fmt"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

func (r *AgentNervousClientPlugin) create(args ...interface{}) (interface{}, error) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "agent.plugin.host.cli.create",
	})

	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err := json.Unmarshal([]byte(args[0].(string)), req)
	if err != nil {
		log.Errorf("decoce bytes %v", err)
		return nil, fmt.Errorf("decoce bytes %v", err)
	}
	log.Debugf("receive create clireq %v", req)

	// todo 处理 + return
	return r.createResource(req)
}

func (r *AgentNervousClientPlugin) createResource(req *clireq.ClientRequest) (string, error) {
	guid, err := r.api.GetContainerManager().CreateContainer(req.ImageTag, req.ContainerName,req.Runtime,req.Workdir,req.ImagePullPolicy, r.api.GetHostManager().GetHostGuid(), req.Envs, req.Ports, req.Lifecycle,req.LivenessProbe,req.ReadinessProbe,req.Reasources,req.SecurityContext,req.VolumeMounts,req.Cmds, req.TerminationMessagePath,req.TerminationMessagePolicy,r.api.GetAgentNervous(), req.Tags)
	if err != nil {
		return "", err
	}

	ct, err := r.api.GetContainerManager().GetContainer(guid)
	if err != nil {
		return "", err
	}

	response := clireq.ClientResponse{
		Guid:          guid,
		ImageTag:      ct.ImageName,
		ContainerName: ct.ContainerName,
		ContainerId:   ct.ContainerId,
		ContainerHost: r.api.GetHostManager().GetHost().HostIP,
	}

	rspByte, err := json.Marshal(response)
	if err != nil {
		return "", fmt.Errorf("marshal req %v", err)
	}
	return string(rspByte), nil
}
