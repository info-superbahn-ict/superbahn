package main

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

func (r *AgentNervousClientPlugin) delete(args ...interface{}) (interface{}, error) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "agent.plugin.host.cli.delete",
	})

	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err := json.Unmarshal([]byte(args[0].(string)), req)
	if err != nil {
		log.Errorf("decoce bytes %v", err)
		return nil, fmt.Errorf("decoce bytes %v", err)
	}
	log.Debugf("receive delete clireq %v", req)

	// todo 处理 + return
	return r.removeResource(req)
}

func (r *AgentNervousClientPlugin) removeResource(req *clireq.ClientRequest) (string, error) {
	if req.DeleteAll == clireq.DeleteAll {
		return "", r.removeAllResource()
	} else {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "agent.plugin.host.cli.delete",
		}).Debugf("guid: %v", req.Guid)
		if !r.api.GetContainerManager().ContainContainer(req.Guid) {
			return "", fmt.Errorf("guid %v is nil", req.Guid)
		}
		if err := r.api.GetContainerManager().DeleteContainer(req.Guid, r.api.GetAgentNervous()); err != nil {
			return "", fmt.Errorf("delete %v %v", req.Guid, err)
		}
		return fmt.Sprintf("DELETE %v SUCCEED", req.Guid), nil
	}
}

func (r *AgentNervousClientPlugin) removeAllResource() error {
	return r.api.GetContainerManager().DeleteAllContainers(r.api.GetAgentNervous())
}
