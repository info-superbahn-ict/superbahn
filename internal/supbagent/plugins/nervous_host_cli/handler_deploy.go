package main

import (
	"encoding/json"
	"fmt"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources/manager_containers/containers"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

func (r *AgentNervousClientPlugin) deploy(args ...interface{}) (interface{}, error) {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "agent.plugin.host.cli.deploy",
	})

	// todo 解码参数
	var req = []clireq.ClientRequest{}
	err := json.Unmarshal([]byte(args[0].(string)), &req)
	if err != nil {
		log.Errorf("decoce bytes %v", err)
		return nil, fmt.Errorf("decoce bytes %v", err)
	}

	log.Debugf("receive create clireq %v", req)

	// todo 处理 + return
	return r.deployResources(req)
}

func (r *AgentNervousClientPlugin) deployResources(reqList []clireq.ClientRequest) (string, error) {
	ctns := make([]containers.Container, len(reqList))
	for index, req := range reqList {
		ctns[index] = containers.Container{
			ImageName:     req.ImageTag,
			ContainerName: req.ContainerName,
			Envs:          req.Envs,
			Ports:         req.Ports,
			Tags:          req.Tags,
			Cmds:          req.Cmds,
		}
	}

	rltMap, err := r.api.GetContainerManager().Deploy(ctns, r.api.GetHostManager().GetHostGuid(), r.api.GetAgentNervous())

	if err != nil {
		for _, value := range rltMap {
			value.ContainerHost = r.api.GetHostManager().GetHost().HostIP
			// r.api.GetContainerManager().DeleteContainer(value.Guid, r.api.GetAgentNervous())
		}

		return "", err
	}

	rspByte, err := json.Marshal(rltMap)
	if err != nil {
		return "", fmt.Errorf("marshal req %v", err)
	}
	return string(rspByte), nil
}
