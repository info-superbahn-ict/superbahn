package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

func (r *AgentNervousClientPlugin) get(args ...interface{}) (interface{}, error) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "agent.plugin.host.cli.get",
	})

	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err := json.Unmarshal([]byte(args[0].(string)), req)
	if err != nil {
		log.Errorf("decoce bytes %v", err)
		return nil, fmt.Errorf("decoce bytes %v", err)
	}
	log.Debugf("receive get clireq %v", req)

	// todo 处理 + return
	return r.getAgent(req)
}

func (r *AgentNervousClientPlugin) getAgent(req *clireq.ClientRequest) (string, error) {
	switch req.ResourceType {
	case resources.ResourceTypeHost:
		return r.listResourceHostBrief()
	case resources.ResourceTypeContainer:
		return r.getResourceContainerBrief(req.Guid)
	default:
		return "", fmt.Errorf("the resourceType is error")
	}
}

func (r *AgentNervousClientPlugin) getResourceContainerBrief(guid string) (string, error) {
	containers := r.api.GetContainerManager().ListContainers()
	ct, ok := containers[guid]
	if !ok {
		return "", fmt.Errorf("the guid does not exist")
	}

	resp := new(bytes.Buffer)
	resp.WriteString(fmt.Sprintf("%-30v%-30v%-30v%-20v\n", "GUID", "IMAGE_NAME", "CONTAINER_NAME", "CONTAINER_ID"))
	resp.WriteString(fmt.Sprintf("%-30v%-30v%-30v%-20v\n", guid, ct.ImageName, ct.ContainerName, ct.ContainerId))
	resp.WriteString("\n")
	return resp.String(), nil
}
