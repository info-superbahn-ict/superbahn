package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

var test string

func (r *AgentNervousClientPlugin) list(args ...interface{}) (interface{}, error) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "agent.plugin.host.cli.list",
	})

	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err := json.Unmarshal([]byte(args[0].(string)), req)
	if err != nil {
		log.Errorf("decoce bytes %v", err)
		return nil, fmt.Errorf("decoce bytes %v", err)
	}
	log.Debugf("receive get clireq %v", req)

	test = "test"

	// todo 处理 + return
	return r.listResource(req)
}

func (r *AgentNervousClientPlugin) listResource(req *clireq.ClientRequest) (string, error) {
	if req.ListAll == clireq.ListAll {
		hostBrief, err := r.listResourceHostBrief()
		if err != nil {
			return "", fmt.Errorf("get host %v", err)
		}
		container, err := r.listResourceContainerBrief()
		if err != nil {
			return "", fmt.Errorf("get container %v", err)
		}
		return strings.Join([]string{hostBrief, container}, ""), nil
	} else {
		switch req.ResourceType {
		case resources.ResourceTypeHost:
			return r.listResourceHostBrief()
		case resources.ResourceTypeContainer:
			return r.listResourceContainerBrief()
		default:
			return "", fmt.Errorf("resrouce type must be [%v]", resources.ResourceTypes)
		}
	}
}

func (r *AgentNervousClientPlugin) listResourceHostBrief() (string, error) {
	guid := r.api.GetHostManager().GetHostGuid()
	host := r.api.GetHostManager().GetHost()

	resp := new(bytes.Buffer)
	resp.WriteString(fmt.Sprintf("%-30v%-20v%-20v%-10v%-25v\n", "GUID", "NAME", "IP", "CPU_CORES", "MEM_BYTES"))
	resp.WriteString(fmt.Sprintf("%-30v%-20v%-20v%-10v%-25v\n", guid, host.HostName, host.HostIP, host.CPUCores, host.MemSize))
	resp.WriteString("\n")
	return resp.String(), nil
}

func (r *AgentNervousClientPlugin) listResourceContainerBrief() (string, error) {
	resp := new(bytes.Buffer)

	resp.WriteString(fmt.Sprintf("%-30v%-30v%-30v%-20v\n", "GUID", "IMAGE_NAME", "CONTAINER_NAME", "CONTAINER_ID"))
	for guid, ct := range r.api.GetContainerManager().ListContainers() {
		resp.WriteString(fmt.Sprintf("%-30v%-30v%-30v%-20v\n", guid, ct.ImageName, ct.ContainerName, ct.ContainerId))
	}
	resp.WriteString("\n")

	return resp.String(), nil
}
