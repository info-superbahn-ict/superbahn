package main

import (
	"encoding/json"
	"fmt"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

func (r *AgentNervousClientPlugin) stop(args ...interface{}) (interface{}, error) {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "agent.plugin.host.cli.stop",
	})

	if len(args) < 1 {
		return nil, fmt.Errorf("parse error")
	}

	var req = &clireq.ClientRequest{}
	err := json.Unmarshal([]byte(args[0].(string)), req)
	if err != nil {
		log.Errorf("decoce bytes %v", err)
		return nil, fmt.Errorf("decoce bytes %v", err)
	}
	log.Debugf("receive get clireq %v", req)

	return r.stopResource(req)

}

func (r *AgentNervousClientPlugin) stopResource(req *clireq.ClientRequest) (string, error) {
	err := r.api.GetContainerManager().StopContainer(req.Guid)

	if err != nil {
		return "", fmt.Errorf("stop %v failed", req.Guid)
	}

	return req.Guid, nil
}
