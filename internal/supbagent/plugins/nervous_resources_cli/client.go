package main

import (
	"context"
	"gitee.com/info-superbahn-ict/superbahn/pkg/plugins"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/apis"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

func PluginsName() string {
	return "agent.nervous.resources.cli"
}

func NewPlugin(ctx context.Context, p *apis.Manager) (plugins.PluginFactory, error) {
	return &NervousClientPlugin{
		ctx: ctx,
		api: p,
	}, nil
}

type NervousClientPlugin struct {
	ctx context.Context
	api *apis.Manager
}

func (r *NervousClientPlugin) Run() {

	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "agent.plugin.resource.cli",
	})

	go func() {
		if err := r.syncFuncMap();err !=nil {
			log.Errorf("sync function map %v",err)
		}
		log.Infof("sync func map finished")
	}()

	name := PluginsName()
	log.Infof("plugins %v initialization complete", name)

	<-r.ctx.Done()
	log.Infof("plugins %v closed", name)
}

func (r *NervousClientPlugin) Close() {

}

func (r *NervousClientPlugin) Call(f string, args ...interface{}) (interface{}, error) {
	return nil, nil
}




