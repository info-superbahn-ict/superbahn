package main

import (
	"fmt"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources/manager_containers"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

func (r *NervousClientPlugin) syncFuncMap() error {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "agent.plugin.resource.cli.syncFuncMap",
	})

	funcMap, err := r.api.GetContainerManager().GetContainersRPCFunctionMap()
	if err != nil {
		return fmt.Errorf("get funftionMap %v", err)
	}
	nuMap := make(map[string]nervous.Controller)

	log.Debugf("sync function map %v", funcMap)
	for guid, fMap := range funcMap {
		nu, err := kafkaNervous.NewNervous(r.ctx, r.api.GetOption().NervousConfig, guid)
		if err != nil {
			return fmt.Errorf("guid %v new nervous %v", guid, err)
		}

		//if err = nu.Run(); err != nil {
		//	return fmt.Errorf("guid %v run nervous %v", guid, err)
		//}

		for fn, fc := range fMap {
			log.Debugf("function name: %v, fc: %p", fn, fc)
			if err = nu.RPCRegister(fn, fc); err != nil {
				return fmt.Errorf("guid %v spongeregister func %v %v", guid, fn, err)
			}
		}

		nuMap[guid] = nu
	}

	rch := r.api.GetContainerManager().GetContainerWatcher()
	for {
		log.Debugf("nuMap %v", nuMap)
		select {
		case op := <-rch:
			log.Debugf("op: %v, guid: %v", op.Op, op.Guid)
			switch op.Op {
			case manager_containers.WatchOpCreate:
				fMap, err := r.api.GetContainerManager().GetContainerRPCFunctionMap(op.Guid)
				if err != nil {
					return fmt.Errorf("guid %v get funftionMap %v", op.Guid, err)
				}
				nu, err := kafkaNervous.NewNervous(r.ctx, r.api.GetOption().NervousConfig, op.Guid)
				if err != nil {
					return fmt.Errorf("guid %v new nervous %v", op.Guid, err)
				}

				for fn, fc := range fMap {
					log.Debugf("function name: %v, fc: %v", fn, &fc)
					if err = nu.RPCRegister(fn, fc); err != nil {
						return fmt.Errorf("guid %v spongeregister func %v %v", op.Guid, fn, err)
					}
				}

				nuMap[op.Guid] = nu
			case manager_containers.WatchOpDelete:
				err := nuMap[op.Guid].Close()
				if err != nil {
					return fmt.Errorf("guid %v close nervous %v", op.Guid, err)
				}
				delete(nuMap, op.Guid)
				//r.api.GetContainerManager().DeleteFunctionMap(op.Guid)
			}
		case <-r.ctx.Done():
			return nil
		}
	}
}
