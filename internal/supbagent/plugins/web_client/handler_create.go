/**
 * @author  yezz
 * @date  2022/1/6 16:27
 */
package web_client

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

func (r *WebClientPlugin) create(w http.ResponseWriter, rq *http.Request) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.create",
	})

	//todo get data
	buffers, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		responseErr(&w, log, "read body: %v", err)
	}

	//解码
	req := &clireq.ClientRequest{}
	err = json.Unmarshal(buffers, req)
	if err != nil {
		responseErr(&w, log, "decode bytes: %v", err)
	}
	log.Debugf("receive create clireq %v", req)

	//处理+返回
	res, err := r.createResource(req)
	if err != nil {
		responseErr(&w, log, "create info: %v", err)
	}
	resp := &clireq.ClientResponse{
		Message: res,
	}
	bts, err := json.Marshal(resp)
	if err != nil {
		responseErr(&w, log, "marshal json: %v", err)
	}
	if bt, err := w.Write(bts); err != nil {
		log.Errorf("create func: %v", err)
	} else {
		log.Info(fmt.Sprintf("CREATE REPONSE (%vB)", bt))
	}
}

func (r *WebClientPlugin) createResource(req *clireq.ClientRequest) (string, error) {
	guid, err := r.api.GetContainerManager().CreateContainer(req.ImageTag, req.ContainerName,req.Runtime,req.Workdir, r.api.GetHostManager().GetHostGuid(), req.Envs, req.Ports, req.Cmds, r.api.GetAgentNervous(), req.Tags)
	if err != nil {
		return "", err
	}

	ct, err := r.api.GetContainerManager().GetContainer(guid)
	if err != nil {
		return "", err
	}

	response := clireq.ClientResponse{
		Guid:          guid,
		ImageTag:      ct.ImageName,
		ContainerName: ct.ContainerName,
		ContainerId:   ct.ContainerId,
	}

	rspByte, err := json.Marshal(response)
	if err != nil {
		return "", fmt.Errorf("marshal req %v", err)
	}
	return string(rspByte), nil
}
