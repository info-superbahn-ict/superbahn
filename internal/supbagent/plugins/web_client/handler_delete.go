/**
 * @author  yezz
 * @date  2022/1/6 16:27
 */
package web_client

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

func (r *WebClientPlugin) delete(w http.ResponseWriter, rq *http.Request) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.delete",
	})

	//todo get data
	buffers, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		responseErr(&w, log, "read body: %v", err)
	}

	//解码
	req := &clireq.ClientRequest{}
	err = json.Unmarshal(buffers, req)
	if err != nil {
		responseErr(&w, log, "decode bytes: %v", err)
	}
	log.Debugf("receive delete clireq %v", req)

	//处理+返回
	res, err := r.removeResource(req)
	if err != nil {
		responseErr(&w, log, "delete info: %v", err)
	}
	resp := &clireq.ClientResponse{
		Message: res,
	}
	bts, err := json.Marshal(resp)
	if err != nil {
		responseErr(&w, log, "marshal json: %v", err)
	}
	if bt, err := w.Write(bts); err != nil {
		log.Errorf("delete func: %v", err)
	} else {
		log.Info(fmt.Sprintf("DELETE REPONSE (%vB)", bt))
	}
}

func (r *WebClientPlugin) removeResource(req *clireq.ClientRequest) (string, error) {
	if req.DeleteAll == clireq.DeleteAll {
		return "", r.removeAllResource()
	} else {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "agent.plugin.host.cli.delete",
		}).Debugf("guid: %v", req.Guid)
		if !r.api.GetContainerManager().ContainContainer(req.Guid) {
			return "", fmt.Errorf("guid %v is nil", req.Guid)
		}
		if err := r.api.GetContainerManager().DeleteContainer(req.Guid, r.api.GetAgentNervous()); err != nil {
			return "", fmt.Errorf("delete %v %v", req.Guid, err)
		}
		return fmt.Sprintf("DELETE %v SUCCEED", req.Guid), nil
	}
}

func (r *WebClientPlugin) removeAllResource() error {
	return r.api.GetContainerManager().DeleteAllContainers(r.api.GetAgentNervous())
}
