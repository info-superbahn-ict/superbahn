/**
 * @author  yezz
 * @date  2022/1/6 16:28
 */
package web_client

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources/manager_containers/containers"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

func (r *WebClientPlugin) deploy(w http.ResponseWriter, rq *http.Request) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.deploy",
	})

	//todo get data
	buffers, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		responseErr(&w, log, "read body: %v", err)
	}

	//解码
	var req = []clireq.ClientRequest{}
	err = json.Unmarshal(buffers, &req)
	if err != nil {
		responseErr(&w, log, "decode bytes: %v", err)
	}
	log.Debugf("receive deploy clireq %v", req)

	//处理+返回
	res, err := r.deployResources(req)
	if err != nil {
		responseErr(&w, log, "list info: %v", err)
	}
	resp := &clireq.ClientResponse{
		Message: res,
	}
	bts, err := json.Marshal(resp)
	if err != nil {
		responseErr(&w, log, "marshal json: %v", err)
	}
	if bt, err := w.Write(bts); err != nil {
		log.Errorf("deploy func: %v", err)
	} else {
		log.Info(fmt.Sprintf("DEPLOY REPONSE (%vB)", bt))
	}
}

func (r *WebClientPlugin) deployResources(reqList []clireq.ClientRequest) (string, error) {
	ctns := make([]containers.Container, len(reqList))
	for index, req := range reqList {
		ctns[index] = containers.Container{
			ImageName:     req.ImageTag,
			ContainerName: req.ContainerName,
			Envs:          req.Envs,
			Ports:         req.Ports,
			Tags:          req.Tags,
		}
	}

	rltMap, err := r.api.GetContainerManager().Deploy(ctns, r.api.GetHostManager().GetHostGuid(), r.api.GetAgentNervous())

	if err != nil {
		for _, value := range rltMap {
			r.api.GetContainerManager().DeleteContainer(value.Guid, r.api.GetAgentNervous())
		}

		return "", err
	}

	rspByte, err := json.Marshal(rltMap)
	if err != nil {
		return "", fmt.Errorf("marshal req %v", err)
	}
	return string(rspByte), nil
}
