/**
 * @author  yezz
 * @date  2022/1/6 16:28
 */
package web_client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

func (r *WebClientPlugin) get(w http.ResponseWriter, rq *http.Request) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.get",
	})

	//todo get data
	buffers, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		responseErr(&w, log, "read body: %v", err)
	}

	//解码
	req := &clireq.ClientRequest{}
	err = json.Unmarshal(buffers, req)
	if err != nil {
		responseErr(&w, log, "decode bytes: %v", err)
	}
	log.Debugf("receive get clireq %v", req)

	//处理+返回
	res, err := r.getAgent(req)
	if err != nil {
		responseErr(&w, log, "get info: %v", err)
	}
	resp := &clireq.ClientResponse{
		Message: res,
	}
	bts, err := json.Marshal(resp)
	if err != nil {
		responseErr(&w, log, "marshal json: %v", err)
	}
	if bt, err := w.Write(bts); err != nil {
		log.Errorf("get func: %v", err)
	} else {
		log.Info(fmt.Sprintf("GET REPONSE (%vB)", bt))
	}
}

func (r *WebClientPlugin) getAgent(req *clireq.ClientRequest) (string, error) {
	switch req.ResourceType {
	case resources.ResourceTypeHost:
		return r.listResourceHostBrief()
	case resources.ResourceTypeContainer:
		return r.getResourceContainerBrief(req.Guid)
	default:
		return "", fmt.Errorf("the resourceType is error")
	}
}

func (r *WebClientPlugin) getResourceContainerBrief(guid string) (string, error) {
	containers := r.api.GetContainerManager().ListContainers()
	ct, ok := containers[guid]
	if !ok {
		return "", fmt.Errorf("the guid does not exist")
	}

	resp := new(bytes.Buffer)
	resp.WriteString(fmt.Sprintf("%-30v%-30v%-30v%-20v\n", "GUID", "IMAGE_NAME", "CONTAINER_NAME", "CONTAINER_ID"))
	resp.WriteString(fmt.Sprintf("%-30v%-30v%-30v%-20v\n", guid, ct.ImageName, ct.ContainerName, ct.ContainerId))
	resp.WriteString("\n")
	return resp.String(), nil
}
