/**
 * @author  yezz
 * @date  2022/1/6 16:24
 */
package web_client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"strings"
)

func (r *WebClientPlugin) list(w http.ResponseWriter, rq *http.Request) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.list",
	})

	//todo get data
	buffers, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		responseErr(&w, log, "read body: %v", err)
	}

	//解码
	req := &clireq.ClientRequest{}
	err = json.Unmarshal(buffers, req)
	if err != nil {
		responseErr(&w, log, "decode bytes: %v", err)
	}
	log.Debugf("receive list clireq %v", req)

	//处理+返回
	res, err := r.listResource(req)
	if err != nil {
		responseErr(&w, log, "list info: %v", err)
	}
	resp := &clireq.ClientResponse{
		Message: res,
	}
	bts, err := json.Marshal(resp)
	if err != nil {
		responseErr(&w, log, "marshal json: %v", err)
	}
	if bt, err := w.Write(bts); err != nil {
		log.Errorf("list func: %v", err)
	} else {
		log.Info(fmt.Sprintf("LIST REPONSE (%vB)", bt))
	}
}

func (r *WebClientPlugin) listResource(req *clireq.ClientRequest) (string, error) {
	if req.ListAll == clireq.ListAll {
		hostBrief, err := r.listResourceHostBrief()
		if err != nil {
			return "", fmt.Errorf("get host %v", err)
		}
		container, err := r.listResourceContainerBrief()
		if err != nil {
			return "", fmt.Errorf("get container %v", err)
		}
		return strings.Join([]string{hostBrief, container}, ""), nil
	} else {
		switch req.ResourceType {
		case resources.ResourceTypeHost:
			return r.listResourceHostBrief()
		case resources.ResourceTypeContainer:
			return r.listResourceContainerBrief()
		default:
			return "", fmt.Errorf("resrouce type must be [%v]", resources.ResourceTypes)
		}
	}
}

func (r *WebClientPlugin) listResourceHostBrief() (string, error) {
	guid := r.api.GetHostManager().GetHostGuid()
	host := r.api.GetHostManager().GetHost()

	resp := new(bytes.Buffer)
	resp.WriteString(fmt.Sprintf("%-30v%-20v%-20v%-10v%-25v\n", "GUID", "NAME", "IP", "CPU_CORES", "MEM_BYTES"))
	resp.WriteString(fmt.Sprintf("%-30v%-20v%-20v%-10v%-25v\n", guid, host.HostName, host.HostIP, host.CPUCores, host.MemSize))
	resp.WriteString("\n")
	return resp.String(), nil
}

func (r *WebClientPlugin) listResourceContainerBrief() (string, error) {
	resp := new(bytes.Buffer)

	resp.WriteString(fmt.Sprintf("%-30v%-30v%-30v%-20v\n", "GUID", "IMAGE_NAME", "CONTAINER_NAME", "CONTAINER_ID"))
	for guid, ct := range r.api.GetContainerManager().ListContainers() {
		resp.WriteString(fmt.Sprintf("%-30v%-30v%-30v%-20v\n", guid, ct.ImageName, ct.ContainerName, ct.ContainerId))
	}
	resp.WriteString("\n")

	return resp.String(), nil
}
