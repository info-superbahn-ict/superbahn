/**
 * @author  yezz
 * @date  2022/1/6 16:29
 */
package web_client

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

func (r *WebClientPlugin) resume(w http.ResponseWriter, rq *http.Request) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.resume",
	})

	//todo get data
	buffers, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		responseErr(&w, log, "read body: %v", err)
	}

	//解码
	req := &clireq.ClientRequest{}
	err = json.Unmarshal(buffers, req)
	if err != nil {
		responseErr(&w, log, "decode bytes: %v", err)
	}
	log.Debugf("receive resume clireq %v", req)

	//处理+返回
	res, err := r.resumeResource(req)
	if err != nil {
		responseErr(&w, log, "resume info: %v", err)
	}
	resp := &clireq.ClientResponse{
		Message: res,
	}
	bts, err := json.Marshal(resp)
	if err != nil {
		responseErr(&w, log, "marshal json: %v", err)
	}
	if bt, err := w.Write(bts); err != nil {
		log.Errorf("resume func: %v", err)
	} else {
		log.Info(fmt.Sprintf("RESUME REPONSE (%vB)", bt))
	}
}

func (r *WebClientPlugin) resumeResource(req *clireq.ClientRequest) (string, error) {
	err := r.api.GetContainerManager().ResumeContainer(req.Guid)

	if err != nil {
		return "", fmt.Errorf("resume %v failed", req.Guid)
	}

	return req.Guid, nil
}
