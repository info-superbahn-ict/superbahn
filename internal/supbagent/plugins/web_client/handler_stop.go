/**
 * @author  yezz
 * @date  2022/1/6 16:29
 */
package web_client

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

func (r *WebClientPlugin) stop(w http.ResponseWriter, rq *http.Request) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.stop",
	})

	//todo get data
	buffers, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		responseErr(&w, log, "read body: %v", err)
	}

	//解码
	req := &clireq.ClientRequest{}
	err = json.Unmarshal(buffers, req)
	if err != nil {
		responseErr(&w, log, "decode bytes: %v", err)
	}
	log.Debugf("receive stop clireq %v", req)

	//处理+返回
	res, err := r.stopResource(req)
	if err != nil {
		responseErr(&w, log, "stop resource: %v", err)
	}
	resp := &clireq.ClientResponse{
		Message: res,
	}
	bts, err := json.Marshal(resp)
	if err != nil {
		responseErr(&w, log, "marshal json: %v", err)
	}
	if bt, err := w.Write(bts); err != nil {
		log.Errorf("stop func: %v", err)
	} else {
		log.Info(fmt.Sprintf("STOP REPONSE (%vB)", bt))
	}
}

func (r *WebClientPlugin) stopResource(req *clireq.ClientRequest) (string, error) {
	err := r.api.GetContainerManager().StopContainer(req.Guid)

	if err != nil {
		return "", fmt.Errorf("stop %v failed", req.Guid)
	}

	return req.Guid, nil
}
