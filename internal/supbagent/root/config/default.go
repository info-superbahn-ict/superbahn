package config

import (
	"fmt"
	"io"
	"os"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/options"
	"github.com/sirupsen/logrus"
	"github.com/spf13/pflag"
)

func SetDefaultOpts(c *options.Options) {

	c.NervousConfig = options.DefaultNervousConfig

	c.LogPath = options.DefaultLogPath

	c.PluginPath = options.DefaultPluginsPath

	c.DockerUrl = options.DefaultDockerUrl

	c.JaegerCollectorUrl = options.DefaultJaegerUrl

	c.JaegerCollectorRoute = options.DefaultJaegerRoute

	c.JaegerAgentHostPort = "10.16.0.180:10035"

	c.ContainerCgroupPerfix = "/docker"

	//c.CliServerUrl = "10.16.0.180:10045"
}

func CheckOpts(c *options.Options) error {

	if c.NervousConfig == "" {
		return fmt.Errorf("nervous config miss")
	}

	if c.LogPath == "" {
		return fmt.Errorf("log path miss")
	}
	return nil
}

func InstallOpts(flags *pflag.FlagSet, c *options.Options) {
	flags.StringVar(&c.NervousConfig, "nvn", c.NervousConfig, "the nervous config path")
	flags.StringVar(&c.LogPath, "log", c.LogPath, "the log path")
	flags.StringVar(&c.DockerUrl, "docker", c.DockerUrl, "the log path")
	flags.StringVar(&c.JaegerCollectorUrl, "supb-collector-url", c.JaegerCollectorUrl, "the log path")
	flags.StringVar(&c.JaegerCollectorRoute, "supb-collector-route", c.JaegerCollectorRoute, "the log path")
	flags.StringVar(&c.JaegerAgentHostPort, "jaeger-agent", c.JaegerAgentHostPort, "jaeger agent host")
	flags.StringVar(&c.HostTag, "host-tag", "CPU", "host tag")
	flags.StringVar(&c.ContainerCgroupPerfix, "container-cgroup-perfix", c.ContainerCgroupPerfix, "type of container cgroup path")
}

func InitLogs(c *options.Options) error {
	if file, err := os.OpenFile(c.LogPath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0777); err != nil {
		return fmt.Errorf("open log file %v", err)
	} else {
		logrus.SetOutput(io.MultiWriter(io.Writer(file), os.Stdout))
	}
	return nil
}
