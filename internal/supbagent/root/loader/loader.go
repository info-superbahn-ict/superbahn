package loader

import (
	"context"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"plugin"
	"sync"

	"gitee.com/info-superbahn-ict/superbahn/pkg/plugins"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/apis"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	log "github.com/sirupsen/logrus"
)

const (
	RootPath = "supbagent.loader.loader."
)

type Loader struct {
	ctx     context.Context
	manager *apis.Manager
	plugins sync.Map
}

func NewLoader(ctx context.Context, m *apis.Manager) *Loader {
	return &Loader{
		ctx:     ctx,
		manager: m,
	}
}

func (c *Loader) Run() {
	log := log.WithContext(c.ctx).WithFields(log.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "Run",
	})

	// todo load local Loader
	cfg := c.manager.GetOption()
	if err := c.loadPlugins(cfg.PluginPath); err != nil {
		log.Errorf("load local plugins %v", err)
	}

	// todo run Loader
	//c.manager.GetProvider().ApplyPlugin(c.CallPlugins)

	log.Info("loader is running")
	<-c.ctx.Done()
	log.Info("loader is closed")
}

func (c *Loader) loadPlugins(path string) error {
	log := log.WithContext(c.ctx).WithFields(log.Fields{
		define.LogsPrintCommonLabelOfFunction: "loadPlugins",
	})

	if fis, err := ioutil.ReadDir(path); err != nil {
		return fmt.Errorf("check path %v", err)
	} else {
		for _, file := range fis {
			var (
				name   string
				pluNew plugins.PluginFactory
			)

			log.Infof("load Loader %v", file.Name())

			p, err := plugin.Open(filepath.Join(path, file.Name()))
			if err != nil {
				return fmt.Errorf("load plugins %v", err)
			}

			n, err := p.Lookup("PluginsName")
			if err != nil {
				return fmt.Errorf("load function PluginsName %v", err)
			}

			if getName, ok := n.(func() string); ok {
				name = getName()
			} else {
				return fmt.Errorf("can't tranfrom function PluginsName %v", err)
			}

			plu, err := p.Lookup("NewPlugin")
			if err != nil {
				return fmt.Errorf("load function NewPlugin %v", err)
			}

			if NewPlugin, ok := plu.(func(context.Context, *apis.Manager) (plugins.PluginFactory, error)); ok {
				// todo init
				pluNew, err = NewPlugin(c.ctx, c.manager)
				if err != nil {
					return fmt.Errorf("new plugins %v", err)
				}
				if err = c.register(name, pluNew); err != nil {
					return fmt.Errorf("regiter plugins %v", err)
				}
			} else {
				return fmt.Errorf("can't find function PluginsName %v", err)
			}
		}
	}

	// todo run it
	c.plugins.Range(func(key, value interface{}) bool {
		go value.(plugins.PluginFactory).Run()
		return true
	})

	return nil
}

func (c *Loader) register(name string, plu plugins.PluginFactory) error {
	if _, ok := c.plugins.Load(name); ok {
		return fmt.Errorf("plugins %v aready registered", name)
	}
	c.plugins.Store(name, plu)
	plu.Close()
	return nil
}

func (c *Loader) remove(name string) {
	if plu, ok := c.plugins.Load(name); ok {
		plu.(plugins.PluginFactory).Close()
	}
	c.plugins.Delete(name)
}

func (c *Loader) CallPlugins(name string, f string, args ...interface{}) (interface{}, error) {
	if plu, ok := c.plugins.Load(name); !ok {
		return nil, fmt.Errorf("plugins %v not exist", name)
	} else {
		if d, err := plu.(plugins.PluginFactory).Call(f, args); err != nil {
			return nil, fmt.Errorf("call plugins %v", err)
		} else {
			return d, nil
		}

	}
}
