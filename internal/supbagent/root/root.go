package root

import (
	"bytes"
	"context"
	"fmt"

	"gitee.com/info-superbahn-ict/superbahn/third_party/docker"

	"gitee.com/info-superbahn-ict/superbahn/internal/supbagent/root/config"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbagent/root/loader"
	"gitee.com/info-superbahn-ict/superbahn/pkg/register"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/apis"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/collector"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/controller"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/options"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources/manager_host"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

const (
	RootPath = "supbagent.root.root."
)

var agentTag = spongeregister.Tag{
	Key:   "system",
	Type:  "string",
	Value: "agent",
}

func NewCommand(ctx context.Context, name string) (*cobra.Command, error) {
	var opt = &options.Options{}

	// providers program entry, and passing consts pointer
	cmd := &cobra.Command{
		Use:   name,
		Short: name + "supbagent.",
		Long:  name + `supbagent`,
		RunE: func(cmd *cobra.Command, args []string) error {
			return runRootCommand(ctx, opt)
		},
	}

	// load config config, low level
	config.SetDefaultOpts(opt)

	// load file config, middle level
	//todo add file config here

	// load command config, high level
	config.InstallOpts(cmd.Flags(), opt)
	return cmd, nil
}

// main routine
func runRootCommand(ctx context.Context, opt *options.Options) error {

	if err := config.InitLogs(opt); err != nil {
		return fmt.Errorf("init log %v", err)
	}

	log := logrus.WithContext(ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "runRootCommand",
	})

	// 使用随机初始id向sponge注册
	tempGuid := uuid.NewV4().String()
	nu, err := kafkaNervous.NewNervous(ctx, opt.NervousConfig, tempGuid)
	if err != nil {
		return fmt.Errorf("new temp nerouvs: %v", err)
	}

	resource := &spongeregister.Resource{
		OType:     1,
		Status:    1,
		IsMonitor: 1,
		IsControl: 1,
		Tags: []spongeregister.Tag{
			agentTag,
			{
				Key:   "HOST",
				Type:  "string",
				Value: opt.HostTag,
			},
		},
	}

	hostName, _ := manager_host.GetName()
	hostMac, _ := manager_host.GetMac()
	resource.Description = hostName + hostMac

	guid, err := register.Register(nu, resource)

	if err != nil {
		return fmt.Errorf("register guid:  %v", err)
	}
	resource.GuId = guid

	if err = nu.Close(); err != nil {
		return fmt.Errorf("close temp nervous: %v", err)
	}

	log.Infof("agent register guid: %v", guid)

	deferCtx, deferCancel := context.WithCancel(context.Background())
	defer deferCancel()

	newNu, err := kafkaNervous.NewNervous(deferCtx, opt.NervousConfig, guid)
	if err != nil {
		return fmt.Errorf("new nerouvs: %v", err)
	}

	cll, err := collector.NewCollector(deferCtx, opt)
	if err != nil {
		return fmt.Errorf("new collector: %v", err)
	}

	ctl, err := controller.NewController(deferCtx, opt)
	if err != nil {
		return fmt.Errorf("new controller: %v", err)
	}

	manager, err := apis.NewManager(ctx, opt, cll, ctl, newNu, guid)
	if err != nil {
		return fmt.Errorf("new manager: %v", err)
	}

	// 启动 jaeger env
	// info, err := createJaeger(ctx, opt)
	// if err != nil {
	// 	return fmt.Errorf("create jaeger %v", err)
	// }

	plu := loader.NewLoader(deferCtx, manager)
	go plu.Run()

	log.Info("agent initialization complete")

	<-ctx.Done()

	log.Info("agent start clearing...")

	errs := new(bytes.Buffer)

	// todo delete all device, then delete agent

	if err = manager.GetContainerManager().DeleteAllContainers(newNu); err != nil {
		errs.WriteString(fmt.Sprintf("delete containers: %v\n", err))
	}

	log.Info("delete all containers")

	if err = register.DeleteDevice(newNu, resource); err != nil {
		errs.WriteString(fmt.Sprintf("delete agent: %v\n", err))
	}

	log.Info("agent is offline")

	// if err = closeJaeger(info, opt); err != nil {
	// 	errs.WriteString(fmt.Sprintf("create jaeger %v\n", err))
	// }

	log.Info("agent closed")
	if errs.Len() == 0 {
		return nil
	}
	return fmt.Errorf(errs.String())
}

func createJaeger(ctx context.Context, opt *options.Options) (*docker.ContainerInformation, error) {
	imageTag := "jaeger-env:v0.10"
	containerName := "jaeger-env"
	env := make(map[string]string)
	ports := make(map[string]string)

	// env["SPAN_STORAGE_TYPE"] = "supbagent"
	env["SPAN_STORAGE_URL"] = "http://" + opt.JaegerCollectorUrl + opt.JaegerCollectorRoute
	//env["KAFKA_BROKERS"] = brokerUrl

	ports["10034"] = "5775/udp"
	ports["10035"] = "6831/udp"
	ports["10036"] = "6832/udp"
	ports["10037"] = "5778"

	cll, err := docker.NewDockerController(ctx, opt.DockerUrl)
	if err != nil {
		return nil, fmt.Errorf("init jaeger %v", err)
	}

	// todo 增加端口映射配置
	info, err := cll.StartContainerWithPorts(imageTag, containerName, env, ports)
	if err != nil {
		return nil, fmt.Errorf("start container %v", err)
	}
	return info, nil
}

func closeJaeger(info *docker.ContainerInformation, opt *options.Options) error {
	cll, err := docker.NewDockerController(context.Background(), opt.DockerUrl)
	if err != nil {
		return fmt.Errorf("create client %v", err)
	}

	if err = cll.Stop(info.CID); err != nil {
		return fmt.Errorf("stop jaeger %v", err)
	}

	if err = cll.Remove(info.CID); err != nil {
		return fmt.Errorf("remove jaeger %v", err)
	}
	return nil
}
