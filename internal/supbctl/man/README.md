# 编写方法
- 创建文件，比如要为sponge提供文档，则创建supb-sponge.1文件，可以直接拷贝现有模板：`cp supb-nervous.1 supb-sponge.1`
- 打开文件以后，按照自己提供的命令修改，可选的用[]括起来，比如docker run命令，必须要跟一个镜像，那么镜像IMAGE不括，但可以指定分配给容器的cpu，那么这一条OPTION则要括起来，最终文档上写法就是docker run [--cpu-shares=[=0]] IMAGE，里面[=0]表示默认值为0。推荐使用`man docker-run`来参考docker的写法
- groff标准宏简介（基本上下面几个掌握就能正常使用了，需要更复杂格式可以网上搜）：
    - \fB、\fI 和 \fR 分别用来改变字体为粗体、斜体和正体（罗马字体）。\fP 设置字体为前一个选择的字体。
    - .SH 请求用于标记一个节section的开始。注意，大部分的 Unix man 手册页依次使用 NAME、 SYNOPSIS、DESCRIPTION、FILES、SEE ALSO、NOTES、AUTHOR 和 BUGS 等节
    - .SS 开始一个子节
    - 编辑文件时，换行不是真的换行，只会多出一个空格，换行必须使用.TP
# 安装文档
- 运行`sudo ./generate.sh`即自动安装文档
# 查看文档
- 运行`man supb-[COMMAND]`，比如要看sponge的文档，则运行`man supb-sponge`或`man supb sponge`
