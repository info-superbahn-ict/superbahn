package options

import (
	"fmt"
	"github.com/spf13/pflag"
)

type Opts struct {
	NervousPath string
	WebUrl string
}

func SetDefaultOpts(c *Opts) {
	c.NervousPath = "../../config/nervous_config.json"
	c.WebUrl = ""
}

func CheckOpts(c *Opts) error {
	if c.NervousPath == "" {
		return fmt.Errorf("nervous config path is nil\n")
	}
	return nil
}

func InstallRootFlags(flags *pflag.FlagSet, c *Opts) {
	flags.StringVarP(&c.NervousPath, "config","", c.NervousPath, "the nervous config.")
}
