package supbctl

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/options"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbagent"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbmanager"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbsponge"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbnervous"
	"github.com/spf13/cobra"
)

func NewCommand(ctx context.Context, name string) (*cobra.Command,error) {
	var opt = &options.Opts{}

	// 当没有子命令时，主命令的入口
	cmd := &cobra.Command{
		Use:   name,
		Short: name + " policy",
		Long:  name + ` octopus controller policy`,
		RunE: func(cmd *cobra.Command, args []string) error {
			return rootInfo(name)
		},
	}
	// todo 默认全局参数
	options.SetDefaultOpts(opt)
	// todo 文件配置全局参数

	// todo 全局参数
	cmd.PersistentFlags().StringVarP(&opt.NervousPath, "config","", opt.NervousPath, "the nervous config.")
	cmd.PersistentFlags().StringVarP(&opt.WebUrl, "url","", opt.WebUrl, "the nervous config.")

	// todo 子命令加载
	supbmanager.InstallManagerFlags(ctx,cmd,opt)
	supbsponge.InstallSpongeFlags(ctx,cmd,opt)
	supbagent.InstallAgentFlags(ctx,cmd,opt)
	supbnervous.InstallNervousFlags(ctx,cmd,opt)
	return cmd,nil
}

// 当没有子命令时，主命令的入口
func rootInfo(name string) error {
	fmt.Printf("NOTE:\n\t%-10v COMMAND [OPTIONS] [ARG...]\n",name)
	return nil
}
