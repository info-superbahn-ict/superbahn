package supbagent

import (
	"context"
	"fmt"

	rootOpt "gitee.com/info-superbahn-ict/superbahn/internal/supbctl/options"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbagent/commands_nervous"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbagent/options"
	"github.com/spf13/cobra"
)

func InstallAgentFlags(ctx context.Context, rootCmd *cobra.Command, c *rootOpt.Opts) {
	// todo 导入全局参数
	var agentOpts = &options.AgentOpts{}
	agentOpts.Apply(c)

	// todo Get
	agentCli := &cobra.Command{
		Use:   "agent",
		Short: "superbahn agent commands",
		RunE: func(cmd *cobra.Command, args []string) error {
			return agentInfo(agentOpts)
		},
		TraverseChildren: true,
	}

	// todo 默认参数配置
	options.ReadAgentEnvOptions()

	agentCli.PersistentFlags().StringVarP(&agentOpts.AgentGuid, "connect", "c", options.AgentGuid, "connect an agent then send a command")

	// todo 挂钩子
	rootCmd.AddCommand(agentCli)

	// todo 当然可以继续添加子命令层级，仿照root添加方式即可，在  ManagerCmd.AddCommand(子命令)
	commands_nervous.InstallAgentCommandsFlags(ctx, agentCli, agentOpts)
}

func agentInfo(opts *options.AgentOpts) error {
	fmt.Printf("NOTE:\n\t agent [get|create|delete|...] [OPTIONS] [ARG...]\n")
	return nil
}
