package commands_nervous

import (
	"context"

	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbagent/options"
	"github.com/spf13/cobra"
)

const (
	tryTime     = 100
	tryInterval = 500
)

func InstallAgentCommandsFlags(ctx context.Context, manager *cobra.Command, opt *options.AgentOpts) {
	// todo list
	list := &cobra.Command{
		Use:   "list [-a isAll] [-t objectType]",
		Short: "list devices or services information from agent",
		RunE: func(cmd *cobra.Command, args []string) error {
			return listImpl(ctx, opt, args)
		},
		TraverseChildren: true,
	}
	list.Flags().BoolVarP(&opt.ListAll, "all", "a", false, "")
	list.Flags().StringVarP(&opt.ResourceType, "type", "t", opt.ResourceType, "")

	// todo Create
	create := &cobra.Command{
		Use:   "create [-c agent_guid] [-e environment_variable]... [-p ports]... [-n name] image cmds...",
		Short: "create container from image tag",
		RunE: func(cmd *cobra.Command, args []string) error {
			return createImpl(ctx, opt, args)
		},
	}

	create.Flags().StringVarP(&opt.ContainerName, "name", "n", opt.ContainerName, "set name")
	create.Flags().StringArrayVarP(&opt.Evnironments, "env", "e", opt.Evnironments, "add running environment")
	create.Flags().StringArrayVarP(&opt.Ports, "port", "p", opt.Ports, "set exposed ports")

	// todo Delete
	delete := &cobra.Command{
		Use:   "delete [-c agent_guid] guid",
		Short: "delete resources in agent",
		RunE: func(cmd *cobra.Command, args []string) error {
			return deleteImpl(ctx, opt, args)
		},
	}

	// todo Get
	get := &cobra.Command{
		Use:   "get [-c agent_guid] [-t type] guid",
		Short: "get resource information, use guid",
		RunE: func(cmd *cobra.Command, args []string) error {
			return getImpl(ctx, opt, args)
		},
	}
	get.Flags().StringVarP(&opt.ResourceType, "type", "t", opt.ResourceType, "")

	// todo Func
	function := &cobra.Command{
		Use:   "exec [-f funcName] [-g target_guid]  args...",
		Short: "execute a function served by resource",
		RunE: func(cmd *cobra.Command, args []string) error {
			return funcImpl(ctx, opt, args)
		},
	}

	function.Flags().StringVarP(&opt.Guid, "guid", "g", "test", "target")
	function.Flags().StringVarP(&opt.FuncName, "funcName", "f", opt.FuncName, "")
	function.Flags().StringVarP(&opt.PushFlag, "pushFlag", "", "off", "")

	manager.AddCommand(function)
	manager.AddCommand(list)
	manager.AddCommand(create)
	manager.AddCommand(delete)
	manager.AddCommand(get)
}
