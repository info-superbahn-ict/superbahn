package commands_nervous

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbagent/options"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
)

func createImpl(ctx context.Context, opt *options.AgentOpts, args []string) error {

	if len(args) < 1 {
		return fmt.Errorf("please select an image")
	}

	imageTag := args[0]

	var cmds []string

	if len(args) > 1 {
		cmds = args[1:]
	} else {
		cmds = []string{""}
	}

	envs := conver2Map(opt.Evnironments, "=")
	ports := conver2Map(opt.Ports, ":")

	req := clireq.ClientRequest{
		ImageTag:      imageTag,
		ContainerName: opt.ContainerName,
		Envs:          envs,
		Ports:         ports,
		Cmds:          cmds,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		return fmt.Errorf("marshal req %v", err)
	}

	Nic, err := kafkaNervous.NewNervous(ctx, opt.ConfigPath, "CLI")
	if err != nil {
		return fmt.Errorf("new %v", err)
	}

	resp, err := Nic.RPCCallCustom(opt.AgentGuid, tryTime, tryInterval, define.RPCFunctionNameOfAgentForCreateObject, string(reqBytes))
	if err != nil {
		return fmt.Errorf("call get %v", err)
	}

	fmt.Printf("%s", resp)

	return nil
}

func conver2Map(strs []string, sep string) map[string]string {
	rltMap := make(map[string]string)
	if len(strs) == 0 {
		return rltMap
	}

	for _, str := range strs {
		arr := strings.Split(str, sep)

		if len(arr) == 2 {
			rltMap[arr[0]] = arr[1]
		}

	}

	for key, value := range rltMap {
		fmt.Printf("key: %s value: %s", key, value)
	}

	return rltMap
}
