package commands_nervous

import (
	"context"
	"encoding/json"
	"fmt"

	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbagent/options"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
)

func deleteImpl(ctx context.Context, opt *options.AgentOpts, args []string) error {

	if len(args) < 1 {
		return fmt.Errorf("please add guid")
	}

	guid := args[0]

	req := clireq.ClientRequest{
		Guid: guid,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		return fmt.Errorf("marshal req %v", err)
	}

	Nic, err := kafkaNervous.NewNervous(ctx, opt.ConfigPath, "CLI")
	if err != nil {
		return fmt.Errorf("new %v", err)
	}

	resp, err := Nic.RPCCallCustom(opt.AgentGuid, tryTime, tryInterval, define.RPCFunctionNameOfAgentForDeleteObject, string(reqBytes))
	if err != nil {
		return fmt.Errorf("call get %v", err)
	}

	fmt.Printf("%s", resp)

	return nil

}
