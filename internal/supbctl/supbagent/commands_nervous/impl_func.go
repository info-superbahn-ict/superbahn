package commands_nervous

import (
	"context"
	"fmt"

	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbagent/options"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
)

func funcImpl(ctx context.Context, opt *options.AgentOpts, args []string) error {

	Nic, err := kafkaNervous.NewNervous(ctx, opt.ConfigPath, "CLI")
	if err != nil {
		return fmt.Errorf("new %v", err)
	}
	switch opt.FuncName {
	case define.RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush:
		resp, err := Nic.RPCCallCustom(opt.Guid, tryTime, tryInterval, opt.FuncName, opt.PushFlag)
		if err != nil {
			return fmt.Errorf("call get %v", err)
		}
		fmt.Printf("%s", resp)
	case define.RPCFunctionNameOfObjectRunningOnAgentForListFunction:
		resp, err := Nic.RPCCallCustom(opt.Guid, tryTime, tryInterval, opt.FuncName)
		if err != nil {
			return fmt.Errorf("call get %v", err)
		}
		fmt.Printf("%s", resp)
	case define.RPCFunctionNameOfObjectRunningOnAgentForStartObject:
		resp, err := Nic.RPCCallCustom(opt.Guid, tryTime, tryInterval, opt.FuncName)
		if err != nil {
			return fmt.Errorf("call get %v", err)
		}
		fmt.Printf("%s", resp)
	case define.RPCFunctionNameOfObjectRunningOnAgentForStopObject:
		resp, err := Nic.RPCCallCustom(opt.Guid, tryTime, tryInterval, opt.FuncName)
		if err != nil {
			return fmt.Errorf("call get %v", err)
		}
		fmt.Printf("%s", resp)
	case define.RPCFunctionNameOfObjectRunningOnAgentForResumeObject:
		resp, err := Nic.RPCCallCustom(opt.Guid, tryTime, tryInterval, opt.FuncName)
		if err != nil {
			return fmt.Errorf("call get %v", err)
		}
		fmt.Printf("%s", resp)
	case define.RPCFunctionNameOfObjectRunningOnAgentForOpenMetricsPush:
		resp, err := Nic.RPCCallCustom(opt.Guid, tryTime, tryInterval, opt.FuncName, opt.PushFlag)
		if err != nil {
			return fmt.Errorf("call get %v", err)
		}
		fmt.Printf("%s", resp)
	case define.RPCFunctionNameOfObjectRunningOnAgentForOpenTracePush:
		resp, err := Nic.RPCCallCustom(opt.Guid, tryTime, tryInterval, opt.FuncName, opt.PushFlag)
		if err != nil {
			return fmt.Errorf("call get %v", err)
		}
		fmt.Printf("%s", resp)
	case define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuRatio:
		resp, err := Nic.RPCCallCustom(opt.Guid, tryTime, tryInterval, opt.FuncName, args[0])
		if err != nil {
			return fmt.Errorf("call get %v", err)
		}
		fmt.Printf("%s", resp)

	case define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuSets:
		resp, err := Nic.RPCCallCustom(opt.Guid, tryTime, tryInterval, opt.FuncName, args[0])
		if err != nil {
			return fmt.Errorf("call get %v", err)
		}
		fmt.Printf("%s", resp)
	case define.RPCFunctionNameOfObjectRunningOnAgentForSetNetUpstreamBandwidth:
		resp, err := Nic.RPCCallCustom(opt.Guid, tryTime, tryInterval, opt.FuncName, args[0])
		if err != nil {
			return fmt.Errorf("call get %v", err)
		}
		fmt.Printf("%s", resp)
	case define.RPCFunctionNameOfObjectRunningOnAgentForSetNetLatency:
		resp, err := Nic.RPCCallCustom(opt.Guid, tryTime, tryInterval, opt.FuncName, args[0])
		if err != nil {
			return fmt.Errorf("call get %v", err)
		}
		fmt.Printf("%s", resp)
	case define.RPCFunctionNameOfObjectRunningOnAgentForGetLimitationConfig:
		resp, err := Nic.RPCCallCustom(opt.Guid, tryTime, tryInterval, opt.FuncName, args)
		if err != nil {
			return fmt.Errorf("call get %v", err)
		}
		fmt.Printf("%s", resp)
	default:
		fmt.Printf("the funcName is error\n")
	}
	return nil
}
