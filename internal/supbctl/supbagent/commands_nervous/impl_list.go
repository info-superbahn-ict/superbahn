package commands_nervous

import (
	"context"
	"encoding/json"
	"fmt"

	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbagent/options"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
)

func listImpl(ctx context.Context, opt *options.AgentOpts, args []string) error {
	req := clireq.ClientRequest{
		ResourceType: opt.ResourceType,
	}
	if opt.ListAll {
		req.ListAll = clireq.ListAll
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		return fmt.Errorf("marshal req %v", err)
	}

	Nic, err := kafkaNervous.NewNervous(ctx, opt.ConfigPath, "CLI")
	if err != nil {
		return fmt.Errorf("new %v", err)
	}

	//if err = Nic.Run();err!=nil{
	//	return fmt.Errorf("nervous run %v",err)
	//}

	resp, err := Nic.RPCCallCustom(opt.AgentGuid, tryTime, tryInterval, define.RPCFunctionNameOfAgentForListObject, string(reqBytes))
	if err != nil {
		return fmt.Errorf("call get %v", err)
	}

	fmt.Printf("%s", resp)

	return nil
}
