package options

import "os"

var AgentGuid = "agent"

/*
	可识别参数读取
*/

func ReadAgentEnvOptions() {
	guid := os.Getenv("SUPB_AGENT")

	if guid != "" {
		AgentGuid = guid
	}
}
