package options

import rootOpt "gitee.com/info-superbahn-ict/superbahn/internal/supbctl/options"

type AgentOpts struct {
	AgentGuid     string
	ConfigPath    string
	WebUrl        string
	Guid          string
	ListAll       bool
	ResourceType  string
	ImageTag      string
	ContainerName string
	// Envs          map[string]string
	Evnironments []string
	Ports        []string
	FuncName     string
	PushFlag     string
}

func (e *AgentOpts) Apply(c *rootOpt.Opts) {
	e.ConfigPath = c.NervousPath
	e.WebUrl = c.WebUrl
}
