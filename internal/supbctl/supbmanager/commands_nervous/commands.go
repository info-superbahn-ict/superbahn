package commands_nervous

import (
	"context"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbmanager/options"

	"github.com/spf13/cobra"
)

const (
	tryTime = 100
	tryInterval = 500
)

func InstallManagerCommandsFlags(ctx context.Context, manager *cobra.Command, opt *options.ManagerOpts) {
	// todo Get
	get := &cobra.Command{
		Use:   "get",
		Short: "capture the brief information of objects in control center objects and list them",
		RunE: func(cmd *cobra.Command, args []string) error {
			return getImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}
	get.Flags().StringVar(&opt.ResourceType,"type",opt.ResourceType,"select resource type to get")
	get.Flags().StringVar(&opt.Status,"status",opt.Status,"select status to get")
	get.Flags().BoolVar(&opt.All,"all",opt.All,"get all specified objects")

	// todo Detail
	detail := &cobra.Command{
		Use:   "detail",
		Short: "capture the detail description of objects in the control center",
		RunE: func(cmd *cobra.Command, args []string) error {
			return detailImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}
	detail.Flags().BoolVar(&opt.MoreInfo,"more-info",opt.MoreInfo,"show a more detailed description of the object")
	detail.Flags().StringVar(&opt.ResourceType,"type",opt.ResourceType,"select resource type to detail")
	detail.Flags().StringVar(&opt.Status,"status",opt.Status,"select status to detail")

	// todo Delete
	del := &cobra.Command{
		Use:   "delete",
		Short: "delete strategies or bindings from manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return deleteImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}
	del.Flags().StringVar(&opt.ResourceType,"type",opt.ResourceType,"select resource type to delete")
	del.Flags().BoolVar(&opt.All,"all",opt.All,"delete all static strategies or bindings")

	// todo Bind
	bind := &cobra.Command{
		Use:   "bind",
		Short: "bind resources to clusters of manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return bindImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}
	bind.Flags().StringVar(&opt.Slave,"slave",opt.Slave,"select salve to bind")
	bind.Flags().StringVar(&opt.Master,"master",opt.Master,"select master to bind")

	// todo Metrics
	log := &cobra.Command{
		Use:   "log",
		Short: "log running strategies information from manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return logImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}
	log.Flags().StringVar(&opt.ResourceType,"type",opt.Slave,"select resource type to log")

	// todo Pull
	pull := &cobra.Command{
		Use:   "pull",
		Short: "pull static strategy from manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return pullImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}

	// todo Push
	push := &cobra.Command{
		Use:   "push",
		Short: "push static strategy to manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return pushImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}

	// todo Run
	run := &cobra.Command{
		Use:   "run",
		Short: "run static strategies of manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return runImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}
	run.Flags().StringVar(&opt.ResourceType,"type",opt.Slave,"select resource type to run")

	// todo Stop
	stop := &cobra.Command{
		Use:   "stop",
		Short: "stop running strategies of manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return stopImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}

	manager.AddCommand(get)
	manager.AddCommand(detail)
	manager.AddCommand(del)
	manager.AddCommand(bind)
	manager.AddCommand(log)
	manager.AddCommand(pull)
	manager.AddCommand(push)
	manager.AddCommand(run)
	manager.AddCommand(stop)
}

