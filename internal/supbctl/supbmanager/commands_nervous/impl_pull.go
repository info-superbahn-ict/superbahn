package commands_nervous


import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbmanager/options"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
)

func pullImpl(ctx context.Context,opt *options.ManagerOpts,args []string)error{
	req := clireq.ClientRequest{
		Names: args,
	}

	reqBytes,err:=json.Marshal(req)
	if err!=nil{
		return fmt.Errorf("marshal req %v",err)
	}

	Nic,err := kafkaNervous.NewNervous(ctx,opt.ConfigPath,"CLI")
	if err!=nil{
		return fmt.Errorf("new %v",err)
	}

	//if err = Nic.Run();err!=nil{
	//	return fmt.Errorf("nervous run %v",err)
	//}

	resp,err := Nic.RPCCallCustom(define.RPCCommonGuidOfManager,tryTime,tryInterval,"pull",string(reqBytes))
	if err !=nil {
		return fmt.Errorf("call pull %v",err)
	}

	fmt.Printf("%s",resp)

	return nil
}