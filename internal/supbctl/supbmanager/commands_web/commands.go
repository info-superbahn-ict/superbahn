package commands_web

import (
	"context"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbmanager/options"

	"github.com/spf13/cobra"
)

const (
	tryTime = 100
	tryInterval = 500
)

func InstallManagerCommandsFlags(ctx context.Context, manager *cobra.Command, opt *options.ManagerOpts) {
	// todo Get
	get := &cobra.Command{
		Use:   "get",
		Short: "get devices or services information from manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return getImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}
	get.Flags().StringVar(&opt.ResourceType,"type",opt.Status,"select resource type to detail")
	get.Flags().StringVar(&opt.Status,"status",opt.Status,"select status to get")
	get.Flags().BoolVar(&opt.All,"all",opt.All,"")

	// todo Detail
	detail := &cobra.Command{
		Use:   "detail",
		Short: "detail devices or services information from manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return detailImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}
	detail.Flags().StringVar(&opt.ResourceType,"type",opt.Status,"select resource type to detail")
	detail.Flags().StringVar(&opt.Status,"status",opt.Status,"select status to detail")

	// todo Delete
	del := &cobra.Command{
		Use:   "delete",
		Short: "delete strategies from manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return deleteImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}
	del.Flags().StringVar(&opt.ResourceType,"type",opt.Status,"select resource type to delete")
	del.Flags().BoolVar(&opt.All,"all",opt.All,"")

	// todo Bind
	bind := &cobra.Command{
		Use:   "bind",
		Short: "bind strategies of manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return bindImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}
	bind.Flags().StringVar(&opt.Slave,"slave",opt.Slave,"select salve to bind")
	bind.Flags().StringVar(&opt.Master,"master",opt.Master,"select master to bind")

	// todo Metrics
	log := &cobra.Command{
		Use:   "log",
		Short: "log devices or services information from manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return logImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}
	log.Flags().StringVar(&opt.ResourceType,"type",opt.Slave,"select resource type to log")

	// todo Pull
	pull := &cobra.Command{
		Use:   "pull",
		Short: "pull devices or services information from manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return pullImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}

	// todo Push
	push := &cobra.Command{
		Use:   "push",
		Short: "push devices or services information to manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return pushImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}

	// todo Run
	run := &cobra.Command{
		Use:   "run",
		Short: "run strategies of manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return runImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}
	run.Flags().StringVar(&opt.ResourceType,"type",opt.Slave,"select resource type to run")

	// todo Stop
	stop := &cobra.Command{
		Use:   "stop",
		Short: "stop strategies of manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return stopImpl(ctx,opt, args)
		},
		TraverseChildren: true,
	}

	manager.AddCommand(get)
	manager.AddCommand(detail)
	manager.AddCommand(del)
	manager.AddCommand(bind)
	manager.AddCommand(log)
	manager.AddCommand(pull)
	manager.AddCommand(push)
	manager.AddCommand(run)
	manager.AddCommand(stop)
}

