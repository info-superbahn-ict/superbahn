package commands_web
import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbmanager/options"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"io/ioutil"
	"net/http"
)

func runImpl(ctx context.Context,opt *options.ManagerOpts,args []string)error{
	req := clireq.ClientRequest{
		ResourceType: opt.ResourceType,
		Names: args,
	}

	reqBytes,err:=json.Marshal(req)
	if err!=nil{
		return fmt.Errorf("marshal req %v",err)
	}

	data, err := http.Post("http://"+opt.WebUrl+"/supbplugins/webclient/run","application/json",bytes.NewBuffer(reqBytes))
	if err != nil {
		return fmt.Errorf("get url %v", err)
	}

	bts, err := ioutil.ReadAll(data.Body)
	if err != nil {
		return fmt.Errorf("read body %v", err)
	}

	resp := clireq.ClientResponse{}
	if err = json.Unmarshal(bts,&resp);err !=nil{
		return fmt.Errorf("unmarshall body %v", err)
	}

	fmt.Printf("%v",resp.Message)
	return nil
}