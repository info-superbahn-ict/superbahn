package supbmanager

import (
	"context"
	"fmt"
	rootOpt "gitee.com/info-superbahn-ict/superbahn/internal/supbctl/options"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbmanager/commands_nervous"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbmanager/commands_web"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbmanager/options"
	"github.com/spf13/cobra"
)

func InstallManagerFlags(ctx context.Context,rootCmd *cobra.Command, c *rootOpt.Opts) {
	// todo 导入全局参数
	var managerOpts = &options.ManagerOpts{}
	managerOpts.Apply(c)

	/*
		todo 创建命令、加载函数， args 是直接读取命令行的参数。
		eg： get a b c
		args = [a,b,c]
		eg: get a --p=b c d (假设p是配置好的可识别参数)
		args = [a,c,d]
	*/

	// todo Get
	managerCli := &cobra.Command{
		Use: "manager",
		Short: "get info from manager",
		RunE: func(cmd *cobra.Command, args []string) error {
			return managerInfo(managerOpts)
		},
		TraverseChildren: true,
	}

	// todo 默认参数配置
	options.SetDefaultManagerOpts(managerOpts)

	// todo 挂钩子
	rootCmd.AddCommand(managerCli)

	// todo 当然可以继续添加子命令层级，仿照root添加方式即可，在  ManagerCmd.AddCommand(子命令)

	if managerOpts.WebUrl == "" {
		commands_nervous.InstallManagerCommandsFlags(ctx,managerCli, managerOpts)
	}else{
		commands_web.InstallManagerCommandsFlags(ctx,managerCli, managerOpts)
	}

}

func managerInfo(opts *options.ManagerOpts) error {
	fmt.Printf("NOTE:\n\tsupbctl manager [get|delete|detail|...] [OPTIONS] [ARG...]\n")
	return nil
}

