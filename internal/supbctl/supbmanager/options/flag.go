package options

import (
	"github.com/spf13/pflag"
)

const (
	ManagerGuid = "control"
)

/*
	可识别参数读取
 */

func SetDefaultManagerOpts(c *ManagerOpts) {
	c.MoreInfo=false
	c.Role = ""
	c.Status = ""

	c.All = false
	c.WebUrl=""
}


func InstallManagerFlags(flags *pflag.FlagSet, c *ManagerOpts) {
	flags.BoolVar(&c.All,"all",c.All,"show all entities")
	flags.StringVar(&c.Role,"role",c.Role,"describe the entity play role")
	flags.StringVar(&c.Status,"status",c.Status,"describe the strategy status")
	flags.BoolVar(&c.MoreInfo,"more-info",c.MoreInfo,"show a more detailed description of the object")
}