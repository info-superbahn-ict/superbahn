package options

import rootOpt "gitee.com/info-superbahn-ict/superbahn/internal/supbctl/options"

/*
	子命令各自的参数
 */

type ManagerOpts struct {

	Role string
	MoreInfo bool
	DeleteImage string
	StrategiesFile string
	StrategiesImage string
	LogStoreFile string



	ResourceType string
	All bool
	Status string
	ConfigPath string
	Slave string
	Master string
	File string
	WebUrl string
}

func (e *ManagerOpts) Apply(c *rootOpt.Opts)  {
	e.ConfigPath = c.NervousPath
	e.WebUrl = c.WebUrl
}