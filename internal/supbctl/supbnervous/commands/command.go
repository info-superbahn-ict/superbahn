package commands

import (
	"fmt"
	"context"
	"github.com/spf13/cobra"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbnervous/options"
kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"github.com/Shopify/sarama"
)
func newClusterAdmin(configAddress string, guid string)(sarama.ClusterAdmin,error){
	configContent, err:=kafkaNervous.ReadJsonFile(configAddress)
	if err!=nil{
		fmt.Printf("read config file fail\n")
		return nil,err
	}
	config := sarama.NewConfig()
	config.ClientID = guid
	return sarama.NewClusterAdmin([]string{configContent["KAFKA_BROKERS"].(string)},config)
}
func InstallNervousCommandsFlags(ctx context.Context, nervous *cobra.Command, opt *options.NervousOpts) {

	hostCli := &cobra.Command{
		Use: "host",
		Short: "Show the basic info of Kafka broker.",
		RunE: func(cmd *cobra.Command, args []string) error {
			configContent,err:=kafkaNervous.ReadJsonFile(opt.ConfigPath)
			if err != nil {
			fmt.Printf("read file from%s fails\n",opt.ConfigPath)
			}else{
				fmt.Printf("broker's address:%s\n",configContent["KAFKA_BROKERS"].(string))
		}
			return nil
		},
		TraverseChildren: true,
	}

	receiveCli := &cobra.Command{
		Use: "receive",
		Short: "receive one message from the topic, block if no message",
		RunE: func(cmd *cobra.Command, args []string) error {
			if nv,err := kafkaNervous.NewNervous(ctx,opt.ConfigPath,opt.MyGuid);err!=nil{
			fmt.Printf("create nervous fail\n")
				return err

			}else{
			if err:=nv.Subscribe(opt.TopicName);err!=nil{
				fmt.Printf("subscribe to topic:%s fail\n",opt.TopicName)
				return err
			}
			if msg,err:=nv.Receive(opt.TopicName);err!=nil{
				fmt.Printf("receive from topic:%s fail\n",opt.TopicName)
				return err
			}else{
				fmt.Println(msg)
			}
			if err:=nv.Close();err!=nil{
			fmt.Printf("Close Nervous fail\n")
				return err

			}
		}
			return nil
		},
		TraverseChildren: true,
	}
	
	var extratopic string
	watchCli := &cobra.Command{
		Use: "watch",
		Short: "continue watching on a topic pair, you should provide the extraTopic",
		RunE: func(cmd *cobra.Command, args []string) error {
			if nv,err := kafkaNervous.NewNervous(ctx,opt.ConfigPath,opt.MyGuid);err!=nil{
				fmt.Printf("create nervous fail\n")
				return err

			}else{
				go func(){
				for{
					if msg,err:=nv.Receive(opt.TopicName);err!=nil{
						fmt.Printf("receive from topic:%s fail\n",opt.TopicName)
					}else{
						fmt.Println(msg)
					}
					select{
					case <- ctx.Done():
						return
					default:
						continue
					}

				}
			}()
			for{
					if msg,err:=nv.Receive(extratopic);err!=nil{
						fmt.Printf("receive from topic:%s fail\n",opt.TopicName)
					}else{
						fmt.Println(msg)
					}
					select{
					case <- ctx.Done():
						return nil
					default:
						continue
					}

				}

					if err:=nv.Close();err!=nil{
					fmt.Printf("Close Nervous fail\n")
					return err

				}
			}
			return nil
		},
		TraverseChildren: true,
	}
	watchCli.Flags().StringVar(&extratopic,"extratopic","","the other topic to be watched")

	var msgKey string
	var msgValue string
	sendCli := &cobra.Command{
		Use: "send",
		Short: "send one message to the topic",
		RunE: func(cmd *cobra.Command, args []string) error {
			if nv,err := kafkaNervous.NewNervous(ctx,opt.ConfigPath,opt.MyGuid);err!=nil{
			fmt.Printf("create nervous fail\n")
				return err

			}else{
			if err:=nv.Send(opt.TopicName,msgKey,msgValue);err!=nil{
				fmt.Printf("send to topic:%s fail\n",opt.TopicName)
				return err
			}else{
			fmt.Println("send success")
			}
			if err:=nv.Close();err!=nil{
			fmt.Printf("Close Nervous fail\n")
				return err

			}
		}
			return nil
		},
		TraverseChildren: true,
	}
	sendCli.Flags().StringVar(&msgKey,"key","","SlaveKey for the message, default is \"\"")
	sendCli.Flags().StringVar(&msgValue,"value","test message value","Value of the message")

	listTopicsCli := &cobra.Command{
		Use: "topics",
		Short: "list all topics in a broker",
		RunE: func(cmd *cobra.Command, args []string) error {
			ca, err:=newClusterAdmin(opt.ConfigPath,opt.MyGuid)
			if err!=nil{
				fmt.Printf("create ClusterAdmin fail\n")
				return err
			}
			if topicDetails, err0:=ca.ListTopics();err0!=nil{
				fmt.Printf("List Topics fail\n")
				return err0
			}else{
				for k, v := range topicDetails{
					fmt.Printf("topic: %s with %d partitions\n",k,v.NumPartitions)
				}
			}

			if err=ca.Close();err!=nil{
			fmt.Printf("Close Nervous fail\n")
				return err
			}
			return nil
		},
		TraverseChildren: true,
	}
	
	deleteTopicCli := &cobra.Command{
		Use: "delete_topic",
		Short: "delete the topic in a broker",
		RunE: func(cmd *cobra.Command, args []string) error {
			ca, err:=newClusterAdmin(opt.ConfigPath,opt.MyGuid)
			if err!=nil{
				fmt.Printf("create ClusterAdmin fail\n")
				return err
			}
			if err =ca.DeleteTopic(opt.TopicName);err!=nil{
				fmt.Printf("Delete Topic: %s fail\n",opt.TopicName)
				return err
			}else{
				fmt.Printf("delete done\n")
			}

			if err=ca.Close();err!=nil{
				fmt.Printf("Close Nervous fail\n")
				return err
			}
			return nil
		},
		TraverseChildren: true,
	}

	listConsumerGroupsCli := &cobra.Command{
		Use: "groups",
		Short: "list available consumer groups in a broker",
		RunE: func(cmd *cobra.Command, args []string) error {
			ca, err:=newClusterAdmin(opt.ConfigPath,opt.MyGuid)
			if err!=nil{
				fmt.Printf("create ClusterAdmin fail\n")
				return err
			}
			if groupsmap, err0:=ca.ListConsumerGroups();err0!=nil{
				fmt.Printf("List Consumer Groups fail\n")
				return err
			}else{
				for k, v := range groupsmap{
					fmt.Println(k,v)
				}
			}

			if err=ca.Close();err!=nil{
			fmt.Printf("Close ClusterAdmin fail\n")
				return err
			}
			return nil
		},
		TraverseChildren: true,
	}
	nervous.AddCommand(receiveCli)
	nervous.AddCommand(watchCli)
	nervous.AddCommand(sendCli)
	nervous.AddCommand(hostCli)
	nervous.AddCommand(listTopicsCli)
	nervous.AddCommand(deleteTopicCli)
	nervous.AddCommand(listConsumerGroupsCli)
}
