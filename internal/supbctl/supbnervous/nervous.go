package supbnervous

import (
	"fmt"
	"context"
	"github.com/spf13/cobra"
	rootOpt "gitee.com/info-superbahn-ict/superbahn/internal/supbctl/options"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbnervous/options"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbnervous/commands"
)

func InstallNervousFlags(ctx context.Context,rootCmd *cobra.Command, c *rootOpt.Opts) {
	// todo 导入全局参数
	var nervousOpts = &options.NervousOpts{}
	nervousOpts.Apply(c)

	/*
		todo 创建命令、加载函数， args 是直接读取命令行的参数。
		eg： get a b c
		args = [a,b,c]
		eg: get a --p=b c d (假设p是配置好的可识别参数)
		args = [a,c,d]
	*/

	nervousCli := &cobra.Command{
		Use: "nervous",
		Short: "get info from nervous",
		RunE: func(cmd *cobra.Command, args []string) error {
			return nervousInfo(nervousOpts)
		},
		TraverseChildren: true,
	}

	// todo 配置命令行局部参数
	options.InstallNervousFlags(nervousCli.PersistentFlags(), nervousOpts)

	// todo 挂钩子
	rootCmd.AddCommand(nervousCli)

	commands.InstallNervousCommandsFlags(ctx,nervousCli, nervousOpts)
	}

func nervousInfo(opts *options.NervousOpts) error {
	fmt.Printf("NOTE:\n\tsbahnctl nervous [host|log|...]\n")
	return nil
}

