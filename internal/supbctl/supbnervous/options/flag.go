package options

import (
	"github.com/spf13/pflag"
)

/*
	可识别参数读取
 */



func InstallNervousFlags(flags *pflag.FlagSet, c *NervousOpts) {
	flags.StringVar(&c.MyGuid,"tmpguid","clinervous","provide a temp guid for this terminal")
	flags.StringVar(&c.TopicName,"topic","","provide the topic name you wanna subscribe or publish")
	}
