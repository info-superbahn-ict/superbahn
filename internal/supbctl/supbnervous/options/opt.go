package options

import rootOpt "gitee.com/info-superbahn-ict/superbahn/internal/supbctl/options"

/*
	子命令各自的参数
 */

type NervousOpts struct {
	ConfigPath string
	MyGuid string
	TopicName string
}

func (e *NervousOpts) Apply(c *rootOpt.Opts)  {
	e.ConfigPath = c.NervousPath
}
