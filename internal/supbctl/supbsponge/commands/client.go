package commands

import (
	"errors"
	"io"
	"io/ioutil"
	"net/http"
)

const version = "0.0.1"

//const baseURL = "http://10.16.0.180:8087"
const baseURL = "http://39.101.140.145:8077"

//const baseURL = "http://localhost:8087"

func init() {

}

type Client struct {
	HTTPClient *http.Client
	//ApiKey string
	BaseURL string
}

func NewClient() *Client {
	var cli Client
	//cli.ApiKey = apiKey
	cli.BaseURL = baseURL

	return &cli
}

func (cli *Client) httpClient() *http.Client {
	if cli.HTTPClient != nil {
		return cli.HTTPClient
	}
	return http.DefaultClient
}

func (cli *Client) do(req *http.Request) (*http.Response, error) {
	//req.Header.Set("X-API-SlaveKey", cli.ApiKey)
	req.Header.Set("Content-SlaveTypes", "application/json")
	req.Header.Set("Accept", "application/json")
	return cli.httpClient().Do(req)
}

func (cli *Client) error(statusCode int, body io.Reader) error {
	buf, err := ioutil.ReadAll(body)
	if err != nil || len(buf) == 0 {
		return errors.New("request failed with status code!")
	}
	return errors.New("request failed with status code!")
}
