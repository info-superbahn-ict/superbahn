package commands

type RegisterInfo struct {
	Description string
	Status      int
	Area        string
	DependNode  string
	OType       int
	Utilization int
	Tags        []Tag
}

type TagGroupSearchInfo struct {
	OType  int `json:"oType"`
	Status int `json:"status"`
	Tag    Tag `json:"tag"`
}

type Tag struct {
	Key   string
	Type  string
	Value interface{}
}

type Resp struct {
}
