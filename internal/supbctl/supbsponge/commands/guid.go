package commands

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

const (
	defaultUrl = ""
)

type listResponse struct {
	result map[string]interface{} `json:"result"`
}

type clearResponse struct {
	Resps []*Resp `json:"list"`
}

type humanResponse struct {
	Resps []*Resp `json:"list"`
}

type relationResponse struct {
	Resps []*Resp `json:"list"`
}

type groupResponse struct {
	Resps []*Resp `json:"list"`
}

type addResponse struct {
	Resps []*Resp `json:"list"`
}

type registerResponse struct {
	Resps []*Resp `json:"list"`
}

type findResponse struct {
	Resps []*Resp `json:"list"`
}

type deleteResponse struct {
	Resps []*Resp `json:"list"`
}

//列出资源信息（all：全部；alive：活跃的；dead：停止的）
func (cli *Client) List(param string) ([]byte, error) {

	path := "/api/v1/"

	switch param {
	case "all":
		path += "deviceInfos"
	case "alive":
		path += "deviceInfos?status=1"
	case "dead":
		path += "deviceInfos?status=2"
	default:
		return nil, fmt.Errorf("the parameter %v is not supported", param)
	}

	reqURL := cli.BaseURL + path

	req, err := http.NewRequest(http.MethodGet, reqURL, nil)
	if err != nil {
		return nil, fmt.Errorf("create HTTP request: %w", err)
	}

	resp, err := cli.do(req)
	if err != nil {
		return nil, fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
		return nil, fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	}

	var result map[string]interface{}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("parse HTTP body: %w", err)
	}

	if err := json.Unmarshal(data, &result); err != nil {
		return nil, fmt.Errorf("parse HTTP body: %w", err)
	}

	str, _ := json.Marshal(result["data"])
	return str, nil
}

//清理dead资源
func (cli *Client) Clear() (string, error) {
	path := "/api/v1/deleteResources/2"
	reqURL := cli.BaseURL + path

	req, err := http.NewRequest(http.MethodDelete, reqURL, nil)
	if err != nil {
		return "failed", fmt.Errorf("create HTTP request: %w", err)
	}

	resp, err := cli.do(req)
	if err != nil {
		return "failed", fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
		return "failed", fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	}

	var result map[string]interface{}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "failed", fmt.Errorf("parse HTTP body: %w", err)
	}

	if err := json.Unmarshal(data, &result); err != nil {
		return "failed", fmt.Errorf("parse HTTP body: %w", err)
	}

	return "success", nil
}

//返回某guId对应的资源信息
func (cli *Client) Human(param string) (string, error) {
	path := "/api/v1/deviceInfo?guId=" + param
	reqURL := cli.BaseURL + path

	req, err := http.NewRequest(http.MethodGet, reqURL, nil)
	if err != nil {
		return "", fmt.Errorf("create HTTP request: %w", err)
	}

	resp, err := cli.do(req)
	if err != nil {
		return "", fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
		return "", fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	}

	var result map[string]interface{}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("parse HTTP body: %w", err)
	}

	if err := json.Unmarshal(data, &result); err != nil {
		return "", fmt.Errorf("parse HTTP body: %w", err)
	}
	str, _ := json.Marshal(result["data"])
	return string(str), nil
}

//TODO RELATION
func (cli *Client) Relation(param string) (string, error) {
	path := "/api/v1/relationResources?guId=" + param
	reqURL := cli.BaseURL + path

	req, err := http.NewRequest(http.MethodGet, reqURL, nil)
	if err != nil {
		return "", fmt.Errorf("create HTTP request: %w", err)
	}

	resp, err := cli.do(req)
	if err != nil {
		return "", fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("parse HTTP body: %w", err)
	}
	var result map[string]interface{}
	if err := json.Unmarshal(data, &result); err != nil {
		return "", fmt.Errorf("parse HTTP body: %w", err)
	}
	str, _ := json.Marshal(result["data"])
	return string(str), nil

	// if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
	// 	return fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	// }

	// var r relationResponse
	// if err := json.NewDecoder(resp.Body).Decode(&r); err != nil {
	// 	return fmt.Errorf("parse HTTP body: %w", err)
	// }
	//return nil
}

//TODO GROUP
func (cli *Client) Group() error {
	path := "/blackboard/device/human"
	reqURL := cli.BaseURL + path

	req, err := http.NewRequest(http.MethodPost, reqURL, nil)
	if err != nil {
		return fmt.Errorf("create HTTP request: %w", err)
	}

	resp, err := cli.do(req)
	if err != nil {
		return fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
		return fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	}

	var r groupResponse
	if err := json.NewDecoder(resp.Body).Decode(&r); err != nil {
		return fmt.Errorf("parse HTTP body: %w", err)
	}

	return nil
}

//TODO ADD
func (cli *Client) Add() error {
	path := "/blackboard/device/add"
	reqURL := cli.BaseURL + path

	req, err := http.NewRequest(http.MethodPost, reqURL, nil)
	if err != nil {
		return fmt.Errorf("create HTTP request: %w", err)
	}

	resp, err := cli.do(req)
	if err != nil {
		return fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
		return fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	}

	var r addResponse
	if err := json.NewDecoder(resp.Body).Decode(&r); err != nil {
		return fmt.Errorf("parse HTTP body: %w", err)
	}

	return nil
}

//注册
func (cli *Client) Register(ri RegisterInfo) (string, error) {
	path := "/api/v1/spongeRegister"
	reqURL := cli.BaseURL + path
	//fmt.Println(reqURL)
	jsons, _ := json.Marshal(ri)
	//fmt.Println(string(jsons))
	body := strings.NewReader(string(jsons))

	req, err := http.NewRequest(http.MethodPost, reqURL, body)
	if err != nil {
		return "failed", fmt.Errorf("create HTTP request: %w", err)
	}

	resp, err := cli.do(req)
	if err != nil {
		return "failed", fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
		return "failed", fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	}

	var result map[string]interface{}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("parse HTTP body: %w", err)
	}

	if err := json.Unmarshal(data, &result); err != nil {
		return "", fmt.Errorf("parse HTTP body: %w", err)
	}
	str, _ := json.Marshal(result["data"])
	if string(str) == "null" {
		return result["msg"].(string), nil
	}
	return string(str), nil
}

//TODO FIND
func (cli *Client) Find(param string) (string, error) {
	path := "/api/v1/searchResources?keyWord=" + param
	reqURL := cli.BaseURL + path

	req, err := http.NewRequest(http.MethodGet, reqURL, nil)
	if err != nil {
		return "", fmt.Errorf("create HTTP request: %w", err)
	}

	resp, err := cli.do(req)
	if err != nil {
		return "", fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	// if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
	// 	return "", fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	// }

	var result map[string]interface{}
	data, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return "", fmt.Errorf("parse HTTP body: %w", err)
	}

	if err := json.Unmarshal(data, &result); err != nil {
		return "", fmt.Errorf("parse HTTP body: %w", err)
	}
	str, _ := json.Marshal(result["data"])
	if string(str) == "null" {
		return result["msg"].(string), nil
	}
	return string(str), nil
}

//DELETE
func (cli *Client) Delete(param string) (string, error) {
	path := "/api/v1/deleteResource/" + param
	reqURL := cli.BaseURL + path

	req, err := http.NewRequest(http.MethodDelete, reqURL, nil)
	if err != nil {
		return "fail", fmt.Errorf("create HTTP request: %w", err)
	}

	resp, err := cli.do(req)
	if err != nil {
		return "fail", fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
		return "fail", fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	}

	var result map[string]interface{}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("parse HTTP body: %w", err)
	}

	if err := json.Unmarshal(data, &result); err != nil {
		return "", fmt.Errorf("parse HTTP body: %w", err)
	}

	return "success", nil
}

func (cli *Client) GetTagGroupInfos(info TagGroupSearchInfo) (string, error) {
	path := "/api/v1/tagGroupInfos"
	reqURL := cli.BaseURL + path

	jsons, _ := json.Marshal(info)
	//fmt.Println(string(jsons))
	body := strings.NewReader(string(jsons))
	req, err := http.NewRequest(http.MethodPost, reqURL, body)
	if err != nil {
		return "fail", fmt.Errorf("create HTTP request: %w", err)
	}

	resp, err := cli.do(req)
	if err != nil {
		return "fail", fmt.Errorf("do HTTP request: %w", err)
	}
	defer resp.Body.Close()

	if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
		return "fail", fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	}

	var result map[string]interface{}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("parse HTTP body: %w", err)
	}

	if err := json.Unmarshal(data, &result); err != nil {
		return "", fmt.Errorf("parse HTTP body: %w", err)
	}
	str, _ := json.Marshal(result["data"])
	if string(str) == "null" {
		return result["msg"].(string), nil
	}
	return string(str), nil
}
