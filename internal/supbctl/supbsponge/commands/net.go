package commands

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type kafkaResponse struct {
	Resps []*Resp `json:"list"`
}

type setIpResponse struct {
	Resps []*Resp `json:"list"`
}

type topicSubResponse struct {
	Resps []*Resp `json:"list"`
}

type topicPubResponse struct {
	Resps []*Resp `json:"list"`
}

//TODO KAFKA
func (cli *Client) Kafka() ([]*Resp, error){
	path := "/blackboard/device/kafka"
	reqURL := cli.BaseURL + path

	req,err := http.NewRequest(http.MethodGet,reqURL,nil)
	if err != nil {
		return nil,fmt.Errorf("create HTTP request: %w", err)
	}

	resp,err := cli.do(req)
	if err != nil {
		return nil,fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
		return nil, fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	}

	var r kafkaResponse
	if err := json.NewDecoder(resp.Body).Decode(&r);err != nil {
		return nil, fmt.Errorf("parse HTTP body: %w", err)
	}

	return r.Resps, nil
}

//TODO SETIP
func (cli *Client) SetIp() ([]*Resp, error){
	path := "/blackboard/device/setIp"
	reqURL := cli.BaseURL + path

	req,err := http.NewRequest(http.MethodGet,reqURL,nil)
	if err != nil {
		return nil,fmt.Errorf("create HTTP request: %w", err)
	}

	resp,err := cli.do(req)
	if err != nil {
		return nil,fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
		return nil, fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	}

	var r setIpResponse
	if err := json.NewDecoder(resp.Body).Decode(&r);err != nil {
		return nil, fmt.Errorf("parse HTTP body: %w", err)
	}

	return r.Resps, nil
}

//TODO TOPICSUB
func (cli *Client) TopicSub() ([]*Resp, error){
	path := "/blackboard/device/setIp"
	reqURL := cli.BaseURL + path

	req,err := http.NewRequest(http.MethodGet,reqURL,nil)
	if err != nil {
		return nil,fmt.Errorf("create HTTP request: %w", err)
	}

	resp,err := cli.do(req)
	if err != nil {
		return nil,fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
		return nil, fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	}

	var r topicSubResponse
	if err := json.NewDecoder(resp.Body).Decode(&r);err != nil {
		return nil, fmt.Errorf("parse HTTP body: %w", err)
	}

	return r.Resps, nil
}

//TODO TOPICPUB
func (cli *Client) TopicPub() ([]*Resp, error){
	path := "/blackboard/device/setIp"
	reqURL := cli.BaseURL + path

	req,err := http.NewRequest(http.MethodGet,reqURL,nil)
	if err != nil {
		return nil,fmt.Errorf("create HTTP request: %w", err)
	}

	resp,err := cli.do(req)
	if err != nil {
		return nil,fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
		return nil, fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	}

	var r topicPubResponse
	if err := json.NewDecoder(resp.Body).Decode(&r);err != nil {
		return nil, fmt.Errorf("parse HTTP body: %w", err)
	}

	return r.Resps, nil
}