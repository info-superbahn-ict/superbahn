package commands

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type restartResponse struct {
	Resps []*Resp `json:"list"`
}

type stopResponse struct {
	Resps []*Resp `json:"list"`
}

//TODO RESTART
func (cli *Client) Restart() ([]*Resp, error){
	path := "/blackboard/device/kafka"
	reqURL := cli.BaseURL + path

	req,err := http.NewRequest(http.MethodGet,reqURL,nil)
	if err != nil {
		return nil,fmt.Errorf("create HTTP request: %w", err)
	}

	resp,err := cli.do(req)
	if err != nil {
		return nil,fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
		return nil, fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	}

	var r restartResponse
	if err := json.NewDecoder(resp.Body).Decode(&r);err != nil {
		return nil, fmt.Errorf("parse HTTP body: %w", err)
	}

	return r.Resps, nil
}

//TODO STOP
func (cli *Client) Stop() ([]*Resp, error){
	path := "/blackboard/device/kafka"
	reqURL := cli.BaseURL + path

	req,err := http.NewRequest(http.MethodGet,reqURL,nil)
	if err != nil {
		return nil,fmt.Errorf("create HTTP request: %w", err)
	}

	resp,err := cli.do(req)
	if err != nil {
		return nil,fmt.Errorf("do HTTP request: %w", err)
	}

	defer resp.Body.Close()

	if !(resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices) {
		return nil, fmt.Errorf("HTTP response: %w", cli.error(resp.StatusCode, resp.Body))
	}

	var r stopResponse
	if err := json.NewDecoder(resp.Body).Decode(&r);err != nil {
		return nil, fmt.Errorf("parse HTTP body: %w", err)
	}

	return r.Resps, nil
}