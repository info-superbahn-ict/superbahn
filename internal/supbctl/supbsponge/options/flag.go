package options

import "github.com/spf13/pflag"

func InstallSpongeFlags(flags *pflag.FlagSet, c *SpongeOpts) {
	flags.StringVarP(&c.Url, "restart", "r", c.Url, "restart sponge")
}

func InstallGuidFlags(flags *pflag.FlagSet, c *SpongeRunOpts) {
	//flags.StringVarP(&c.Url,"list","l",c.Url,"list guid")
	//flags.StringVarP(&c.Url,"clear","c",c.Url,"clear dead guid")
	//flags.StringVarP(&c.Url,"human","",c.Url,"")
	//flags.StringVarP(&c.Url,"relation","",c.Url,"")
	//flags.StringVarP(&c.Url,"group","g",c.Url,"")
	//flags.StringVarP(&c.Url,"add","a",c.Url,"")
	//flags.StringVarP(&c.Url,"register","",c.Url,"")
	//flags.StringVarP(&c.Url,"find","f",c.Url,"")
	//flags.StringVarP(&c.Url,"delete","d",c.Url,"")
}

func SetDefaultSpongeOpts(c *SpongeOpts) {
	c.Url = ""
}
