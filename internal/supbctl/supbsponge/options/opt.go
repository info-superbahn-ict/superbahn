package options

import rootOpt "gitee.com/info-superbahn-ict/superbahn/internal/supbctl/options"

type SpongeOpts struct {
	Url string
}

func (e *SpongeOpts) Apply(c *rootOpt.Opts){
	e.Url = c.NervousPath
}

type SpongeRunOpts struct {
	Url string
}

func (e *SpongeRunOpts) Apply(c *SpongeOpts){
	e.Url = c.Url
}