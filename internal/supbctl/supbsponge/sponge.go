package supbsponge

import (
	"context"
	"errors"
	"fmt"
	rootOpt "gitee.com/info-superbahn-ict/superbahn/internal/supbctl/options"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbsponge/commands"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbctl/supbsponge/options"
	"github.com/spf13/cobra"
	"strconv"
)

func InstallSpongeFlags(ctx context.Context, rootCmd *cobra.Command, c *rootOpt.Opts) {
	// todo 导入全局参数
	var (
		spongeOpts    = &options.SpongeOpts{}
		spongeRunOpts = &options.SpongeRunOpts{}
	)
	spongeOpts.Apply(c)

	/*
		todo 创建命令、加载函数， args 是直接读取命令行的参数。
		eg： get a b c
		args = [a,b,c]
		eg: get a --p=b c d (假设p是配置好的可识别参数)
		args = [a,c,d]
	*/

	// todo Get
	spongeCli := &cobra.Command{
		Use:   "sponge",
		Short: "CLI tool for sponge system",
		RunE: func(cmd *cobra.Command, args []string) error {
			return spongeInfo(spongeOpts)
		},
		TraverseChildren: true,
	}

	// todo 默认参数配置
	options.SetDefaultSpongeOpts(spongeOpts)

	// todo 配置命令行局部参数
	options.InstallSpongeFlags(spongeCli.PersistentFlags(), spongeOpts)
	options.InstallGuidFlags(spongeCli.PersistentFlags(), spongeRunOpts)

	// todo 挂钩子
	rootCmd.AddCommand(spongeCli)

	// todo 当然可以继续添加子命令层级，仿照root添加方式即可，在  ManagerCmd.AddCommand(子命令)
	spongeCli.AddCommand(runCmd())
	spongeCli.AddCommand(guidCmd())
	spongeCli.AddCommand(netCmd())
}

func spongeInfo(opts *options.SpongeOpts) error {
	fmt.Printf("NOTE:\n\tsbahnctl manager [get|delete|detail|...] [OPTIONS] [ARG...]\n")
	return nil
}

func runCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "run",
		Short: "restart or stop sponge system",
		RunE: func(cmd *cobra.Command, args []string) error {
			if len(args) <= 0 {
				return errors.New("one parameter is required!")
			}

			cli := commands.NewClient()
			flags := cmd.Flags()

			//调用restart函数
			if flags.ShorthandLookup("r") != nil {
				fmt.Println(flags.ShorthandLookup("r").Shorthand)
				_, err := cli.Restart()
				if err != nil {
					return err
				}
			}

			//调用stop函数
			if flags.ShorthandLookup("s") != nil {
				fmt.Println(flags.ShorthandLookup("s").Shorthand)
				_, err := cli.Stop()
				if err != nil {
					return err
				}
			}

			return nil
		},
	}

	cmd.Flags().StringP("restart", "r", "", "restart the sponge system")
	cmd.Flags().StringP("stop", "s", "", "stop the sponge system")

	return cmd
}

func guidCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "guid",
		Short: "a series of operations about sponge guid",
		RunE: func(cmd *cobra.Command, args []string) error {
			cli := commands.NewClient()
			flags := cmd.Flags()

			//调用list函数
			if flags.Lookup("list").Changed {
				var param = "all"
				if len(args) > 0 {
					param = args[0]
				}

				data, err := cli.List(param)
				if err != nil {
					return err
				}
				fmt.Println(string(data[:]))
			}

			//调用clear函数
			if flags.Lookup("clear").Changed {
				data, err := cli.Clear()
				if err != nil {
					return err
				}
				fmt.Println(data)
			}

			//调用human函数
			if flags.Lookup("parse").Changed {
				if len(args) < 1 {
					return fmt.Errorf("the parameter is'n nil")
				}
				data, err := cli.Human(args[0])
				if err != nil {
					return err
				}
				fmt.Println(data)
			}

			//调用relation函数--TODO
			if flags.Lookup("relation").Changed {
				if len(args) < 1 {
					return fmt.Errorf("the parameter is'n nil")
				}
				data, err := cli.Relation(args[0])
				if err != nil {
					return err
				}
				fmt.Println(data)
			}

			//调用group函数--TODO
			if flags.Lookup("group").Changed {
				fmt.Println(flags.ShorthandLookup("g").Shorthand)
				err := cli.Group()
				if err != nil {
					return err
				}
			}

			//修改baseURl---TODO 不能持久化
			if flags.Lookup("url").Changed {
				if len(args) < 1 {
					return fmt.Errorf("the parameter is'n nil")
				}
				//fmt.Println(args[0])
				cli.BaseURL = args[0]
			}

			//调用register函数
			if flags.Lookup("sign").Changed {
				if len(args) < 5 {
					return fmt.Errorf("the number of parameters can't be less than 5")
				}
				var registerInfo commands.RegisterInfo
				registerInfo.Description = args[0]
				status, err := strconv.Atoi(args[1])
				if err != nil {
					fmt.Println("status(args[1]) must be int")
				}
				registerInfo.Status = status
				registerInfo.Area = args[2]
				registerInfo.DependNode = args[3]
				oType, err := strconv.Atoi(args[4])
				if err != nil {
					fmt.Println("oType(args[4] must be int")
				}
				registerInfo.OType = oType

				tagsLen := len(args) - 5
				if tagsLen%3 != 0 {
					return fmt.Errorf("the tags parameters are missing")
				}
				tags := make([]commands.Tag, 0, tagsLen/3)
				for i := 0; i < tagsLen; i += 3 {
					tag := commands.Tag{
						Key:   args[5+i],
						Type:  args[5+i+1],
						Value: args[5+i+2],
					}
					tags = append(tags, tag)
				}
				registerInfo.Tags = tags
				data, err := cli.Register(registerInfo)
				if err != nil {
					fmt.Println("register has error")
				}
				fmt.Println(data)
			}

			//调用find函数
			if flags.Lookup("find").Changed {
				if len(args) < 1 {
					return fmt.Errorf("the parameter is'n nil")
				}
				data, err := cli.Find(args[0])
				if err != nil {
					return err
				}
				fmt.Println(data)
			}

			//调用delete函数
			if flags.Lookup("delete").Changed {
				if len(args) < 1 {
					return fmt.Errorf("the parameter is'n nil")
				}
				data, err := cli.Delete(args[0])
				if err != nil {
					return err
				}
				fmt.Println(data)
			}

			if flags.Lookup("tagInfos").Changed {
				if len(args) < 4 {
					return fmt.Errorf("the number of parameters can't be less than 4")
				}

				oType, err := strconv.Atoi(args[0])
				if err != nil {
					return fmt.Errorf("oType(args[0] must be int")
				}
				status, err := strconv.Atoi(args[1])
				if err != nil {
					return fmt.Errorf("status(args[1] must be int")
				}
				tag := commands.Tag{
					Key:   args[2],
					Value: args[3],
				}
				info := commands.TagGroupSearchInfo{
					OType:  oType,
					Status: status,
					Tag:    tag,
				}
				data, err := cli.GetTagGroupInfos(info)
				if err != nil {
					return err
				}
				fmt.Println(data)
			}

			return nil
		},
	}

	cmd.Flags().StringP("list", "l", "", "list guids in conditions")
	cmd.Flags().StringP("clear", "c", "", "clear resources of some dead guid")
	cmd.Flags().StringP("parse", "p", "", "show info about some guid in human can understand")
	cmd.Flags().BoolP("relation", "t", false, "list some guid that have relation about one guid")
	cmd.Flags().StringP("group", "g", "", "get some guid group")
	cmd.Flags().StringP("url", "u", "", "modify the base URL")
	cmd.Flags().StringP("sign", "s", "", "sign in with sponge")
	cmd.Flags().StringP("find", "f", "", "find some guid")
	cmd.Flags().StringP("delete", "d", "", "delete some guid")
	cmd.Flags().BoolP("tagInfos", "", false, "get tagsGroupInfos in conditions")

	return cmd
}

func netCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "net",
		Short: "show net information about sponge",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			var err error
			return err
		},
		RunE: func(cmd *cobra.Command, args []string) error {

			cli := commands.NewClient()
			flags := cmd.Flags()

			//调用delete函数
			if flags.ShorthandLookup("k") != nil {
				fmt.Println(flags.ShorthandLookup("k").Shorthand)
				_, err := cli.Kafka()
				if err != nil {
					return err
				}
			}

			//调用setIp函数
			if flags.ShorthandLookup("s") != nil {
				fmt.Println(flags.ShorthandLookup("s").Shorthand)
				_, err := cli.SetIp()
				if err != nil {
					return err
				}
			}

			//调用topicSub函数
			if flags.Lookup("topicSub") != nil {
				fmt.Println(flags.Lookup("topicSub"))
				_, err := cli.TopicSub()
				if err != nil {
					return err
				}
			}

			//调用topicPub函数
			if flags.Lookup("topicPub") != nil {
				fmt.Println(flags.Lookup("topicPub"))
				_, err := cli.TopicPub()
				if err != nil {
					return err
				}
			}

			return nil
		},
	}

	cmd.Flags().StringP("kafka", "k", "", "show kafka broker info")
	cmd.Flags().StringP("setIp", "s", "", "set ip and port about kafka broker")
	cmd.Flags().StringP("topicSub", "", "", "show all topic of sub")
	cmd.Flags().StringP("topicPub", "", "", "show all topic of pub")

	return cmd
}
