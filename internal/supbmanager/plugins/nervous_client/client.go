package main

import (
	"context"
	"gitee.com/info-superbahn-ict/superbahn/pkg/plugins"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/apis"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	 "github.com/sirupsen/logrus"
)

const (
	RPCTryTime = 100
	RPCTryInterval = 500
)

func PluginsName() (string,string,string) {
	return "NervousClient","default","test_token"
}

func NewPlugin(ctx context.Context,p *apis.Apis) (plugins.PluginFactory,error){
	return &NervousClientPlugin{
		ctx: ctx,
		api: p,
	}, nil
}

type NervousClientPlugin struct {
	ctx      context.Context
	api *apis.Apis
}


func (r *NervousClientPlugin) Close()  {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "nervous.client.close",
	})
	name,user,token := PluginsName()
	log.Infof("close plugin %v, user, %v teoken %v",name,user,token)
}

func (r *NervousClientPlugin) Call(f string,args... interface{}) (interface{},error) {
	return nil,nil
}


func (r *NervousClientPlugin) Run() {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "server.nervous.Run",
	})

	if err:= r.api.GetNervous().RPCRegister("get", r.get);err!=nil{
		log.Errorf("spongeregister %v",err)
		return
	}

	if err:= r.api.GetNervous().RPCRegister("detail", r.detail);err!=nil{
		log.Errorf("spongeregister %v",err)
		return
	}

	if err:= r.api.GetNervous().RPCRegister("delete", r.delete);err!=nil{
		log.Errorf("spongeregister %v",err)
		return
	}

	if err:= r.api.GetNervous().RPCRegister("run", r.run);err!=nil{
		log.Errorf("spongeregister %v",err)
		return
	}

	if err:= r.api.GetNervous().RPCRegister("stop", r.stop);err!=nil{
		log.Errorf("spongeregister %v",err)
		return
	}

	if err:= r.api.GetNervous().RPCRegister("push", r.push);err!=nil{
		log.Errorf("spongeregister %v",err)
		return
	}

	if err:= r.api.GetNervous().RPCRegister("pull", r.pull);err!=nil{
		log.Errorf("spongeregister %v",err)
		return
	}

	if err:= r.api.GetNervous().RPCRegister("log", r.log);err!=nil{
		log.Errorf("spongeregister %v",err)
		return
	}

	if err:= r.api.GetNervous().RPCRegister("bind", r.bind);err!=nil{
		log.Errorf("spongeregister %v",err)
		return
	}

	name,user,token := PluginsName()
	log.Infof("plugins %v initialization complete, user %v, token %v",name,user,token)

	<-r.ctx.Done()
}