package main

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	log "github.com/sirupsen/logrus"
)

func (r *NervousClientPlugin) bind(args ...interface{}) (interface{}, error) {
	// todo init
	entry := log.WithContext(r.ctx).WithFields(log.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.nervous.server.bind",
	})

	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err := json.Unmarshal([]byte(args[0].(string)),req)
	if err != nil {
		entry.Errorf("decoce bytes %v", err)
		return nil, fmt.Errorf("decoce bytes %v", err)
	}
	entry.Debugf("receive bind clireq %v", req)

	// todo 处理

	return r.bindingWithBriefInfo(req)
}

func (r *NervousClientPlugin) bindingWithBriefInfo(req *clireq.ClientRequest) (string, error) {
	if err := r.api.BindingResourcesToCluster(req.Slave, req.Master); err != nil {
		return "", fmt.Errorf("binding slave to master %v", err)
	}
	return fmt.Sprintf("BINDING SLAVE %v TO MASTER %v\n", req.Slave, req.Master), nil
}