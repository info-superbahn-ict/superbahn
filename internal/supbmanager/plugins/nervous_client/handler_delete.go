package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

func (r *NervousClientPlugin) delete(args ...interface{}) (interface{}, error) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.nervous.server.delete",
	})

	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err := json.Unmarshal([]byte(args[0].(string)),req)
	if err != nil {
		log.Errorf("decoce bytes %v", err)
		return nil, fmt.Errorf("decoce bytes %v", err)
	}
	log.Debugf("receive delete clireq %v", req)

	// todo 处理

	return r.deleteWithBriefInfo(req)
}

func (r *NervousClientPlugin) deleteWithBriefInfo(req *clireq.ClientRequest) (string, error) {
	switch req.ResourceType {
	case clireq.ResourceTypeStrategies:
		if req.DeleteAll == clireq.DeleteAll {
			return r.deleteAllStaticStrategiesWithBriefInfo()
		} else {
			return r.deleteStaticStrategiesWithBriefInfo(req.Names)
		}
	case clireq.ResourceTypeBindings:
		if req.DeleteAll == clireq.DeleteAll {
			return r.deleteAllBindingsWithBriefInfo()
		} else {
			return r.deleteBindingsWithBriefInfo(req.Guids)
		}
	default:
		return "", fmt.Errorf("request type error, just allow [user_defined static strategies(not running)|bindings]")
	}
}


func (r *NervousClientPlugin) deleteAllStaticStrategiesWithBriefInfo() (string, error) {
	resp := new(bytes.Buffer)
	for name := range r.api.GetStrategyManager().ListStaticStrategy() {
		if err := r.api.GetStrategyManager().DeleteStaticStrategy(name); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "StrategiesManager.DeleteStaticStrategy",
			}).Debugf("delete all static strategies %v %v", name, err)
			resp.WriteString(fmt.Sprintf("DELETE %v FAILD, REMOVE STRATEGY ERROR\n", name))
		} else {
			resp.WriteString(fmt.Sprintf("DELETE %v SUCCEED\n", name))
		}
	}
	return resp.String(), nil
}

func (r *NervousClientPlugin) deleteStaticStrategiesWithBriefInfo(names []string) (string, error) {
	resp := new(bytes.Buffer)
	for _, name := range names {
		if err := r.api.GetStrategyManager().DeleteStaticStrategy(name); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "StrategiesManager.DeleteStaticStrategy",
			}).Debugf("delete static strategy %v %v", name, err)
			resp.WriteString(fmt.Sprintf("DELETE %v FAILD, REMOVE STRATEGY ERROR\n", name))
		} else {
			resp.WriteString(fmt.Sprintf("DELETE %v SUCCEED\n", name))
		}

	}
	return resp.String(), nil
}

func (r *NervousClientPlugin) deleteAllBindingsWithBriefInfo() (string, error) {
	resp := new(bytes.Buffer)
	for _, s := range r.api.GetBindingsManager().ListBindings() {
		if err := r.api.GetBindingsManager().DeleteBinding(s.SlaveGuid, s.BindingName); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "BindingManager.DeleteBinding",
			}).Debugf("delete all bindings %v %v", s.BindingName, err)
			resp.WriteString(fmt.Sprintf("DELETE %v FAILD, REMOVE BINDING ERROR\n", s.BindingName))
		} else {
			resp.WriteString(fmt.Sprintf("DELETE %v SUCCEED\n", s.BindingName))
		}
	}
	return resp.String(), nil
}

func (r *NervousClientPlugin) deleteBindingsWithBriefInfo(guids []string) (string, error) {
	resp := new(bytes.Buffer)
	for _, slaveGuid := range guids {
		for _, v := range r.api.GetBindingsManager().GetDevicesBindings(slaveGuid) {
			if err := r.api.GetBindingsManager().DeleteBinding(v.SlaveGuid, v.BindingName); err != nil {
				logrus.WithContext(r.ctx).WithFields(logrus.Fields{
					define.LogsPrintCommonLabelOfFunction: "BindingManager.DeleteBinding",
				}).Debugf("delete binding %v %v", v.BindingName, err)
				resp.WriteString(fmt.Sprintf("DELETE %v FAILD, REMOVE BINDING ERROR\n", v.BindingName))
			} else {
				resp.WriteString(fmt.Sprintf("DELETE %v SUCCEED\n", v.BindingName))
			}
		}
	}
	return resp.String(), nil
}