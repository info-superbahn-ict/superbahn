package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	log "github.com/sirupsen/logrus"
)

func (r *NervousClientPlugin) get(args ...interface{}) (interface{}, error) {
	// todo init
	entry := log.WithContext(r.ctx).WithFields(log.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.nervous.server.get",
	})

	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err := json.Unmarshal([]byte(args[0].(string)),req)
	if err != nil {
		entry.Errorf("decoce bytes %v", err)
		return nil, fmt.Errorf("decoce bytes %v", err)
	}
	entry.Debugf("receive get clireq %v", req)

	// todo 处理 + return

	return r.getWithBriefInfo(req)
}


func (r *NervousClientPlugin) getWithBriefInfo(req *clireq.ClientRequest) (string, error) {
	switch req.ResourceType {
	//case clireq.ResourceTypeDevices:
	//	if req.ListAll == clireq.ListAll {
	//		return r.listDevicesBriefInfo(), nil
	//	} else {
	//		return r.getDevicesBriefInfo(req.Guids), nil
	//	}
	//case clireq.ResourceTypeServices:
	//	if req.ListAll == clireq.ListAll {
	//		return r.listServicesBriefInfo(), nil
	//	} else {
	//		return r.getServicesBriefInfo(req.Guids), nil
	//	}
	case clireq.ResourceTypeResources:
		if req.ListAll == clireq.ListAll {
			return r.listResourcesBriefInfo(), nil
		} else {
			return r.getResourcesBriefInfo(req.Guids), nil
		}
	case clireq.ResourceTypeClusters:
		if req.ListAll == clireq.ListAll {
			return r.listClustersBriefInfo(), nil
		} else {
			return r.getClustersBriefInfo(req.Guids), nil
		}
	case clireq.ResourceTypeStrategies:
		if req.ListAll == clireq.ListAll {
			if req.IsRunning == clireq.IsRunning {
				return r.listRunningStrategyBriefInfo(), nil
			} else {
				return r.listStaticStrategyBriefInfo(), nil
			}
		} else {
			if req.IsRunning == clireq.IsRunning {
				return r.getRunningStrategyBriefInfo(req.Guids), nil
			} else {
				return r.getStaticStrategyBriefInfo(req.Names), nil
			}
		}
	case clireq.ResourceTypeBindings:
		if req.ListAll == clireq.ListAll {
			return r.listBindingsBriefInfo(), nil
		} else {
			return r.getBindingsBriefInfo(req.Guids), nil
		}
	default:
		return "", fmt.Errorf("request type error, just allow [resources|clusters|strategies|bindings]")
	}
}

func (r *NervousClientPlugin) listResourcesBriefInfo() string {
	resp := new(bytes.Buffer)
	resp.WriteString(fmt.Sprintf("%-30v %-10v %-40v %-30v\n", "GUID", "TYPE", "DESCRIPTION", "PRENODE"))
	for _, v := range r.api.GetResourcesManager().ListResources() {
		resp.WriteString(fmt.Sprintf("%-30v %-10v %-40v %-30v\n", v.GuId, v.OType, v.Description, v.PreNode))
	}
	return resp.String()
}
func (r *NervousClientPlugin) getResourcesBriefInfo(guids []string) string {

	resp := new(bytes.Buffer)
	resp.WriteString(fmt.Sprintf("%-30v %-10v %-40v %-30v\n", "GUID", "TYPE", "DESCRIPTION", "PRENODE"))
	for _, guid := range guids {
		d := r.api.GetResourcesManager().GetResources(guid)
		resp.WriteString(fmt.Sprintf("%-30v %-10v %-40v %-30v\n", d.GuId, d.OType, d.Description, d.PreNode))
	}
	return resp.String()
}

//
//func (r *NervousClientPlugin) listDevicesBriefInfo() string {
//	resp := new(bytes.Buffer)
//	resp.WriteString(fmt.Sprintf("%-10v %-40v %-15v %-30v\n", "VERSION", "NAME", "TYPE", "GUID"))
//	for _, v := range r.api.GetDevicesManager().ListDevices() {
//		resp.WriteString(fmt.Sprintf("%-10v %-40v %-15v %-30v\n", v.Version, v.DeviceName, v.ResourceType, v.Guid))
//	}
//	return resp.String()
//}
//
//func (r *NervousClientPlugin) getDevicesBriefInfo(guids []string) string {
//
//	resp := new(bytes.Buffer)
//	resp.WriteString(fmt.Sprintf("%-10v %-40v %-15v %-30v\n", "VERSION", "NAME", "TYPE", "GUID"))
//	for _, guid := range guids {
//		d := r.api.GetDevicesManager().GetDevices(guid)
//		resp.WriteString(fmt.Sprintf("%-10v %-40v %-15v %-30v\n", d.Version, d.DeviceName, d.ResourceType, d.Guid))
//	}
//	return resp.String()
//}
//
//func (r *NervousClientPlugin) listServicesBriefInfo() string {
//	resp := new(bytes.Buffer)
//	resp.WriteString(fmt.Sprintf("%-10v %-40v %-15v %-30v\n", "VERSION", "NAME", "TYPE", "GUID"))
//	for _, v := range r.api.GetServicesManager().ListServices() {
//		resp.WriteString(fmt.Sprintf("%-10v %-40v %-15v %-30v\n", v.Version, v.ServiceName, v.ResourceType, v.Guid))
//	}
//	return resp.String()
//}
//
//func (r *NervousClientPlugin) getServicesBriefInfo(guids []string) string {
//
//	resp := new(bytes.Buffer)
//	resp.WriteString(fmt.Sprintf("%-10v %-40v %-15v %-30v\n", "VERSION", "NAME", "TYPE", "GUID"))
//	for _, guid := range guids {
//		s := r.api.GetServicesManager().GetServices(guid)
//		resp.WriteString(fmt.Sprintf("%-10v %-40v %-15v %-30v\n", s.Version, s.ServiceName, s.ResourceType, s.Guid))
//	}
//	return resp.String()
//}

func (r *NervousClientPlugin) listClustersBriefInfo() string {
	resp := new(bytes.Buffer)
	resp.WriteString(fmt.Sprintf("%-10v %-20v %-30v %-40v %-15v %-30v\n", "VERSION", "SHORTDESCRIPTION","DESCRIPTION", "NAME", "KIND", "IMAGE"))
	for _, v := range r.api.GetClustersManager().ListCluster() {
		resp.WriteString(fmt.Sprintf("%-10v %-20v %-30v %-40v %-15v %-30v\n", v.ClusterVersion,v.ClusterShortDescription,v.ClusterDescription, v.ClusterName, v.ClusterMasterKind, v.ClusterMasterImage))
	}
	return resp.String()
}

func (r *NervousClientPlugin) getClustersBriefInfo(guids []string) string {
	resp := new(bytes.Buffer)
	resp.WriteString(fmt.Sprintf("%-10v %-20v %-30v %-40v %-15v %-30v\n", "VERSION", "SHORTDESCRIPTION","DESCRIPTION", "NAME", "KIND", "IMAGE"))
	for _, guid := range guids {
		c := r.api.GetClustersManager().GetCluster(guid)
		resp.WriteString(fmt.Sprintf("%-10v %-20v %-30v %-40v %-15v %-30v\n", c.ClusterVersion, c.ClusterShortDescription,c.ClusterDescription,c.ClusterName, c.ClusterMasterKind, c.ClusterMasterImage))
	}
	return resp.String()
}

func (r *NervousClientPlugin) listRunningStrategyBriefInfo() string {
	resp := new(bytes.Buffer)
	resp.WriteString(fmt.Sprintf("%-10v %-20v %-30v %-15v %-20v %-30v \n", "VERSION", "SHORTDESCRIPTION","DESCRIPTION", "KIND", "IMAGE", "GUID"))
	for _, v := range r.api.GetStrategyManager().ListRunningStrategy() {
		resp.WriteString(fmt.Sprintf("%-10v %-20v %-30v %-15v %-20v %-30v \n", v.StrategyVersion,v.StrategyShortDescription,v.StrategyDescription, v.StrategyKind, v.StrategyImage, v.StrategyGuid))
	}
	return resp.String()
}

func (r *NervousClientPlugin) listStaticStrategyBriefInfo() string {
	resp := new(bytes.Buffer)
	resp.WriteString(fmt.Sprintf("%-10v %-20v %-30v %-15v %-20v %-30v \n", "VERSION", "SHORTDESCRIPTION","DESCRIPTION", "KIND", "IMAGE", "NAME"))
	for _, v := range r.api.GetStrategyManager().ListStaticStrategy() {
		resp.WriteString(fmt.Sprintf("%-10v %-20v %-30v %-15v %-20v %-30v \n", v.StrategyVersion,v.StrategyShortDescription,v.StrategyDescription, v.StrategyKind, v.StrategyImage, v.StrategyName))
	}
	return resp.String()
}

func (r *NervousClientPlugin) getRunningStrategyBriefInfo(guids []string) string {
	resp := new(bytes.Buffer)
	resp.WriteString(fmt.Sprintf("%-10v %-20v %-30v %-15v %-20v %-30v \n", "VERSION", "SHORTDESCRIPTION","DESCRIPTION", "KIND", "IMAGE", "GUID"))
	for _, guid := range guids {
		s, err := r.api.GetStrategyManager().GetRunningStrategy(guid)
		if err != nil {
			resp.WriteString(fmt.Sprintf("%-10v %-20v %-30v %-15v %-20v %-30v \n", "NIL", "NIL", "NIL", "NIL", "NIL","NIL"))
		} else {
			resp.WriteString(fmt.Sprintf("%-10v %-20v %-30v %-15v %-20v %-30v \n", s.StrategyVersion, s.StrategyShortDescription, s.StrategyDescription, s.StrategyKind, s.StrategyImage, s.StrategyGuid))
		}
	}
	return resp.String()
}

func (r *NervousClientPlugin) getStaticStrategyBriefInfo(guids []string) string {
	resp := new(bytes.Buffer)
	resp.WriteString(fmt.Sprintf("%-10v %-20v %-30v %-40v %-15v %-20v \n", "VERSION", "SHORTDESCRIPTION","DESCRIPTION", "NAME", "KIND", "IMAGE"))
	for _, guid := range guids {
		s, err := r.api.GetStrategyManager().GetStaticStrategy(guid)
		if err != nil {
			resp.WriteString(fmt.Sprintf("%-10v %-20v %-30v %-40v %-15v %-20v \n", "NIL","NIL","NIL","NIL", "NIL", "NIL"))
		} else {
			resp.WriteString(fmt.Sprintf("%-10v %-20v %-30v %-40v %-15v %-20v\n", s.StrategyVersion, s.StrategyShortDescription, s.StrategyDescription, s.StrategyName, s.StrategyKind, s.StrategyImage))
		}
	}
	return resp.String()
}

func (r *NervousClientPlugin) listBindingsBriefInfo() string {
	resp := new(bytes.Buffer)
	resp.WriteString(fmt.Sprintf("%-10v %-40v %-15v %-30v\n", "VERSION", "NAME", "MASTER", "SLAVE"))
	for _, v := range r.api.GetBindingsManager().ListBindings() {
		resp.WriteString(fmt.Sprintf("%-10v %-40v %-15v %-30v\n", v.Version, v.BindingName, v.MasterGuid, v.SlaveGuid))
	}
	return resp.String()
}

func (r *NervousClientPlugin) getBindingsBriefInfo(guids []string) string {
	resp := new(bytes.Buffer)
	resp.WriteString(fmt.Sprintf("%-10v %-40v %-15v %-30v\n", "VERSION", "NAME", "MASTER", "SLAVE"))
	for _, guid := range guids {
		for _, v := range r.api.GetBindingsManager().GetDevicesBindings(guid) {
			resp.WriteString(fmt.Sprintf("%-10v %-40v %-15v %-30v\n", v.Version, v.BindingName, v.MasterGuid, v.SlaveGuid))
		}
	}
	return resp.String()
}
