package main

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	log "github.com/sirupsen/logrus"
)

func (r *NervousClientPlugin) log (args ...interface{}) (interface{}, error) {
	// todo init
	entry := log.WithContext(r.ctx).WithFields(log.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.nervous.server.log",
	})

	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err := json.Unmarshal([]byte(args[0].(string)),req)
	if err != nil {
		entry.Errorf("decoce bytes %v", err)
		return nil, fmt.Errorf("decoce bytes %v", err)
	}
	entry.Debugf("receive log clireq %v", req)

	// todo 处理


	return r.logStrategyWithInfo(req)
}

func (r *NervousClientPlugin) logStrategyWithInfo(req *clireq.ClientRequest) (string, error) {
	switch req.ResourceType {
	case clireq.ResourceTypeStrategies:
		return r.logRunningStrategiesWithInfo(req.Guids[0])
	default:
		return "", fmt.Errorf("request type error, just allow [strategies(not running)]")
	}
}

func (r *NervousClientPlugin) logRunningStrategiesWithInfo(guid string) (string, error) {
	st, err := r.api.GetStrategyManager().GetRunningStrategy(guid)
	if err != nil {
		return "", err
	}

	return r.api.GetDocker().Log(st.StrategyContainerId)
}

func (r *NervousClientPlugin) logRunningStrategiesWithInfoRPCFromAgent(guid string) (string, error) {
	resp,err :=r.api.GetNervous().RPCCallCustom(define.RPCCommonGuidOfAgent,RPCTryTime,RPCTryInterval,define.RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush,guid)
	return resp.(string),err
}