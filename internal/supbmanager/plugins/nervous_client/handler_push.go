package main

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	log "github.com/sirupsen/logrus"
)

func (r *NervousClientPlugin) push(args ...interface{}) (interface{}, error) {
	// todo init
	entry := log.WithContext(r.ctx).WithFields(log.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.nervous.server.push",
	})

	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err := json.Unmarshal([]byte(args[0].(string)), req)
	if err != nil {
		entry.Errorf("decoce bytes %v", err)
		return nil, fmt.Errorf("decoce bytes %v", err)
	}
	entry.Debugf("receive push clireq %v", req)

	// todo 处理

	return r.pushStaticStrategy(req)
}

func (r *NervousClientPlugin) pushStaticStrategy(req *clireq.ClientRequest) (string, error) {
	if err := r.api.GetStrategyManager().PushStaticStrategy(req.Data); err != nil {
		return "", fmt.Errorf("push %v", err)
	}
	return fmt.Sprintf("PUSH STRATEGY SUCCEED"), nil
}
