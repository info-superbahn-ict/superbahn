package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	log "github.com/sirupsen/logrus"
)

func (r *NervousClientPlugin) run(args ...interface{}) (interface{}, error) {
	// todo init
	entry := log.WithContext(r.ctx).WithFields(log.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.nervous.server.run",
	})

	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err := json.Unmarshal([]byte(args[0].(string)),req)
	if err != nil {
		entry.Errorf("decoce bytes %v", err)
		return nil, fmt.Errorf("decoce bytes %v", err)
	}
	entry.Debugf("receive run clireq %v", req)

	// todo 处理


	return r.runWithBriefInfo(req)
}

func (r *NervousClientPlugin) runWithBriefInfo(req *clireq.ClientRequest) (string, error) {
	switch req.ResourceType {
	case clireq.ResourceTypeStrategies:
		return r.runStaticStrategiesWithBriefInfo(req.Names)
	default:
		return "", fmt.Errorf("request type error, just allow [static strategies(not running)]")
	}
}

func (r *NervousClientPlugin) runStaticStrategiesWithBriefInfo(names []string) (string, error) {
	resp := new(bytes.Buffer)
	for _, name := range names {
		if err := r.api.RunStaticStrategy(name); err != nil {
			resp.WriteString(fmt.Sprintf("RUN STATIC STRATEGY %v FAILED: %v\n", name, err))
		} else {
			resp.WriteString(fmt.Sprintf("RUN STATIC STRATEGY %v SUCCEED\n", name))
		}
	}
	return resp.String(), nil
}