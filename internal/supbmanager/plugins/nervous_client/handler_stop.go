package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	log "github.com/sirupsen/logrus"
)

func (r *NervousClientPlugin) stop(args ...interface{}) (interface{}, error) {
	// todo init
	entry := log.WithContext(r.ctx).WithFields(log.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.nervous.server.stop",
	})

	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err := json.Unmarshal([]byte(args[0].(string)),req)
	if err != nil {
		entry.Errorf("decoce bytes %v", err)
		return nil, fmt.Errorf("decoce bytes %v", err)
	}
	entry.Debugf("receive stop clireq %v", req)

	// todo 处理 + return

	return r.stopRunningStrategiesWithBriefInfo(req)
}

func (r *NervousClientPlugin) stopRunningStrategiesWithBriefInfo(req *clireq.ClientRequest) (string, error) {
	resp := new(bytes.Buffer)
	for _, guid := range req.Guids {

		if err := r.api.DeleteRunningStrategy(guid); err != nil {
			resp.WriteString(fmt.Sprintf("STOP STRATEGY %v FAILED: %v", guid, err))
		} else {
			resp.WriteString(fmt.Sprintf("STOP STRATEGY %v SUCCEED", guid))
		}

	}
	return resp.String(), nil
}