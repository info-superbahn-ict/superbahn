package main

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
	"github.com/sirupsen/logrus"
)

func (r *syncer) initListResourceFromSponge() (*spongeregister.ListResources, error) {

	resp,err := r.api.GetNervous().RPCCallCustom(define.INIT_SPONGE_OCTOPUS,100,500,define.LIST_INFOS)
	if err !=nil {
		return nil, fmt.Errorf("rpc call %v",err)
	}

	list := &spongeregister.ListResources{}
	if err =json.Unmarshal([]byte(resp.(string)),list);err !=nil {
		return nil, fmt.Errorf("unmarshall resp %v",err)
	}

	return list, nil
}

func (r *syncer) InitResourceFromSponge(list *spongeregister.ListResources) error {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "apis.InitResourceFromSponge",
	})
	for guid, res := range list.Resources {
		if err := r.api.GetResourcesManager().AddResources(&res); err != nil {
			log.Errorf("add device %v failed %v", guid, err)
		} else {
			log.Infof("add device %v succeed", guid)
		}
	}
	return nil
}

func (r *syncer) sync(args ...interface{}) (interface{},error){
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "syncer.sync",
	})

	if len(args) == 0 {
		log.Errorf("can't find json byte")
		return nil, fmt.Errorf("can't find json byte")
	}

	log.Debugf("recive %v",args[0])

	return r.syncSpongeResourceToManager([]byte(args[0].(string)))

	//bts ,_ := json.Marshal(args)
	//
	//return r.syncSpongeResourceToManager(bts)
}

func (r *syncer) syncSpongeResourceToManager(data []byte) (string, error) {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "syncer.sync.syncSpongeResourceToManager",
	})

	var res = &spongeregister.ResourceSync{}
	err := json.Unmarshal(data, res)
	if err != nil {
		return "", fmt.Errorf("decoce bytes %v", err)
	}

	rs := &spongeregister.Resource{
		GuId: res.GuId,
		Description: res.Description,
		Status:res.Status,
		Area:res.Area,
		PreNode:res.PreNode,
		SubNode:res.SubNode,
		OType:res.OType,
		IsMonitor:res.IsMonitor,
		IsControl:res.IsControl,
	}

	switch res.Op {
	case spongeregister.OpADD:
		if err := r.api.GetResourcesManager().AddResources(rs); err != nil {
			log.Errorf("add Resources %v failed %v", rs.GuId, err)
		} else {
			log.Infof("add Resources %v succeed", rs.GuId)
		}
	case spongeregister.OpUPD:
		if err := r.api.GetResourcesManager().UpdateResources(rs); err != nil {
			log.Errorf("add Resources %v failed %v", rs.GuId, err)
		} else {
			log.Infof("add Resources %v succeed", rs.GuId)
		}
	case spongeregister.OpDEL:
		if err := r.api.GetResourcesManager().DeleteResources(rs.GuId); err != nil {
			log.Errorf("add Resources %v failed %v", rs.GuId, err)
		} else {
			log.Infof("add Resources %v succeed", rs.GuId)
		}
	default:
		return "", fmt.Errorf("unkown guid %v", res.GuId)
	}
	return "SYNC SUCCEED", nil
}