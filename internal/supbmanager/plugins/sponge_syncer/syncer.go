package main

import (
	"context"
	"gitee.com/info-superbahn-ict/superbahn/pkg/plugins"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/apis"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)


func PluginsName() (string,string,string) {
	return "SpongeSyncer","default","test_token"
}

func NewPlugin(ctx context.Context, p *apis.Apis) (plugins.PluginFactory, error) {
	return &syncer{
		ctx:     ctx,
		api: p,
	}, nil
}

type syncer struct {
	ctx      context.Context
	api *apis.Apis
}


func (r *syncer) Run() {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.sponge_syncer.run",
	})

	log.Infof("start sync with sponge  ...")

	devices, err := r.initListResourceFromSponge()
	if err != nil {
		log.Errorf("list resrouce %v", err)
	}else{
		log.Infof("storage sync data sponge  ...")
		err = r.InitResourceFromSponge(devices)
		if err !=nil {
			log.Errorf("init resrouce %v",err)
		}
	}

	if err = r.api.GetNervous().RPCRegister(define.RPCFunctionNameOfManagerReceiveResourceSyncMessageFromSponge,r.sync);err !=nil{
		log.Errorf("start rpc %v", err)
	}

	if _,err = r.api.GetNervous().RPCCallCustom(define.INIT_SPONGE_OCTOPUS,10,500,define.SPONGE_SYNC_PUSH,define.SPONGE_SYNC_PUSH_ON);err !=nil{
		log.Errorf("start rpc %v", err)
	}

	name,user,token := PluginsName()
	log.Infof("plugins %v initialization complete, user %v ,token %v", name,user,token)
	<-r.ctx.Done()
}


func (r *syncer) Close() {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.sponge_syncer.close",
	})
	if _,err := r.api.GetNervous().RPCCallCustom(define.INIT_SPONGE_OCTOPUS,10,500,define.SPONGE_SYNC_PUSH,define.SPONGE_SYNC_PUSH_OFF);err !=nil {
		log.Errorf("start rpc %v", err)
	}

	name,user,token := PluginsName()
	log.Infof("close plugin %v, user, %v teoken %v",name,user,token)
}

func (r *syncer) Call(f string,args... interface{})(interface{},error) {
	return nil,nil
}



