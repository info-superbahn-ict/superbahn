package main

import (
	"context"
	"encoding/json"
	"fmt"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
	"testing"
)

func TestList(t *testing.T) {
	nu,_ := kafkaNervous.NewNervous(context.Background(),"../../../../config/nervous_config.json",define.RPCCommonGuidOfManager)

	resp,err := nu.RPCCallCustom(define.INIT_SPONGE_OCTOPUS,100,500,define.LIST_INFOS)
	if err !=nil {
		fmt.Printf("rpc call %v",err)
	}

	list := &spongeregister.ListResources{}
	if err =json.Unmarshal([]byte(resp.(string)),list);err !=nil {
		fmt.Printf("unmarshall call %v",err)
	}

	fmt.Printf("resp: %v",resp)
}
