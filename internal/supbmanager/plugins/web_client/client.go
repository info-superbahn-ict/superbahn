package main

import (
	"context"
	"gitee.com/info-superbahn-ict/superbahn/pkg/plugins"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/apis"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

const (
	WebServerReadTimeOut = 5
	WebServerWriteTimeOut = 5
)

func PluginsName() (string,string,string) {
	return "WebClient","default","test_token"
}

func NewPlugin(ctx context.Context,  p *apis.Apis) (plugins.PluginFactory, error) {
	return &WebClientPlugin{
		ctx:     ctx,
		api: p,
	}, nil
}

type WebClientPlugin struct {
	ctx     context.Context
	api *apis.Apis
	server   *http.Server
}

func (r *WebClientPlugin) Close() {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "server.web.Run",
	})
	name,user,token := PluginsName()
	log.Infof("close plugin %v, user, %v teoken %v",name,user,token)
}

func (r *WebClientPlugin) Call(f string, args ...interface{}) (interface{}, error) {
	return nil, nil
}

func (r *WebClientPlugin) Run() {


	patternPrefix := "/supbmanager/plugins/webclient/"

	serve := http.NewServeMux()
	serve.HandleFunc(patternPrefix+"get", r.list)
	serve.HandleFunc(patternPrefix+"detail", r.detail)
	serve.HandleFunc(patternPrefix+"delete", r.delete)
	serve.HandleFunc(patternPrefix+"run", r.run)
	serve.HandleFunc(patternPrefix+"stop", r.stop)
	serve.HandleFunc(patternPrefix+"push", r.push)
	serve.HandleFunc(patternPrefix+"pull", r.pull)
	serve.HandleFunc(patternPrefix+"log", r.log)
	serve.HandleFunc(patternPrefix+"bind", r.bind)

	r.server = &http.Server{
		Addr:         r.api.GetOption().CliServerUrl,
		ReadTimeout:  WebServerReadTimeOut * time.Second,
		WriteTimeout: WebServerWriteTimeOut * time.Second,
		Handler:      serve,
	}

	go func() {
		if err := r.server.ListenAndServe(); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "server.web.Run",
			}).Infof("server stop %v", err)
		}
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "server.web.Run",
		}).Infof("listen closed")
	}()

	name,user,token := PluginsName()
	logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "server.web.Run",
	}).Infof("plugins %v initialization complete, user %v ,token %v", name,user,token)

	<-r.ctx.Done()
}
