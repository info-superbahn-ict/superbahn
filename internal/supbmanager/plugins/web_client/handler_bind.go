package main

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

func (r *WebClientPlugin) bind(w http.ResponseWriter, rq *http.Request) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.bind",
	})

	// todo get data
	buffers, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		responseErr(&w,log,"read body %v",err)
		return
	}

	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err = json.Unmarshal(buffers,req)
	if err != nil {
		responseErr(&w,log,"decode bytes %v",err)
	}
	log.Debugf("receive bind clireq %v", req)

	// todo 处理 + return
	res,err := r.bindingWithBriefInfo(req)
	if err !=nil {
		responseErr(&w,log,"bind info %v",err)
	}

	resp := &clireq.ClientResponse{
		Message: res,
	}

	bts,err :=json.Marshal(resp)
	if err !=nil {
		responseErr(&w,log,"marshal %v",err)
	}

	if bt, err := w.Write(bts); err != nil {
		log.Errorf("bind function %v", err)
	} else {
		log.Infof(fmt.Sprintf("BIND REPONSE (%vB)", bt))
	}
}

func (r *WebClientPlugin) bindingWithBriefInfo(req *clireq.ClientRequest) (string, error) {
	if err := r.api.BindingResourcesToCluster(req.Slave, req.Master); err != nil {
		return "", fmt.Errorf("binding slave to master %v", err)
	}
	return fmt.Sprintf("BINDING SLAVE %v TO MASTER %v", req.Slave, req.Master), nil
}