package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)


func (r *WebClientPlugin) delete(w http.ResponseWriter, rq *http.Request) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.delete",
	})

	// todo get data
	buffers, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		responseErr(&w,log,"read body %v",err)
		return
	}


	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err = json.Unmarshal(buffers,req)
	if err != nil {
		responseErr(&w,log,"decode bytes %v",err)
	}
	log.Debugf("receive delete clireq %v", req)

	// todo 处理 + return

	res,err := r.deleteWithBriefInfo(req)
	if err !=nil {
		responseErr(&w,log,"delete info %v",err)
	}

	resp := &clireq.ClientResponse{
		Message: res,
	}

	bts,err :=json.Marshal(resp)
	if err !=nil {
		responseErr(&w,log,"marshal %v",err)
	}

	if bt, err := w.Write(bts); err != nil {
		log.Errorf("delete function %v", err)
	} else {
		log.Infof(fmt.Sprintf("DELETE REPONSE (%vB)", bt))
	}
}


func (r *WebClientPlugin) deleteWithBriefInfo(req *clireq.ClientRequest) (string, error) {
	switch req.ResourceType {
	case clireq.ResourceTypeStrategies:
		if req.DeleteAll == clireq.DeleteAll {
			return r.deleteAllStaticStrategiesWithBriefInfo()
		} else {
			return r.deleteStaticStrategiesWithBriefInfo(req.Names)
		}
	case clireq.ResourceTypeBindings:
		if req.DeleteAll == clireq.DeleteAll {
			return r.deleteAllBindingsWithBriefInfo()
		} else {
			return r.deleteBindingsWithBriefInfo(req.Guids)
		}
	default:
		return "", fmt.Errorf("request type error, just allow [user_defined static strategies(not running)|bindings]")
	}
}


func (r *WebClientPlugin) deleteAllStaticStrategiesWithBriefInfo() (string, error) {
	resp := new(bytes.Buffer)
	for name := range r.api.GetStrategyManager().ListStaticStrategy() {
		if err := r.api.GetStrategyManager().DeleteStaticStrategy(name); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "StrategiesManager.DeleteStaticStrategy",
			}).Debugf("delete all static strategies %v %v", name, err)
			resp.WriteString(fmt.Sprintf("DELETE %v FAILD, REMOVE STRATEGY ERROR\n", name))
		} else {
			resp.WriteString(fmt.Sprintf("DELETE %v SUCCEED\n", name))
		}
	}
	return resp.String(), nil
}

func (r *WebClientPlugin) deleteStaticStrategiesWithBriefInfo(names []string) (string, error) {
	resp := new(bytes.Buffer)
	for _, name := range names {
		if err := r.api.GetStrategyManager().DeleteStaticStrategy(name); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "StrategiesManager.DeleteStaticStrategy",
			}).Debugf("delete static strategy %v %v", name, err)
			resp.WriteString(fmt.Sprintf("DELETE %v FAILD, REMOVE STRATEGY ERROR\n", name))
		} else {
			resp.WriteString(fmt.Sprintf("DELETE %v SUCCEED\n", name))
		}

	}
	return resp.String(), nil
}

func (r *WebClientPlugin) deleteAllBindingsWithBriefInfo() (string, error) {
	resp := new(bytes.Buffer)
	for _, s := range r.api.GetBindingsManager().ListBindings() {
		if err := r.api.GetBindingsManager().DeleteBinding(s.SlaveGuid, s.BindingName); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "BindingManager.DeleteBinding",
			}).Debugf("delete all bindings %v %v", s.BindingName, err)
			resp.WriteString(fmt.Sprintf("DELETE %v FAILD, REMOVE BINDING ERROR\n", s.BindingName))
		} else {
			resp.WriteString(fmt.Sprintf("DELETE %v SUCCEED\n", s.BindingName))
		}
	}
	return resp.String(), nil
}

func (r *WebClientPlugin) deleteBindingsWithBriefInfo(guids []string) (string, error) {
	resp := new(bytes.Buffer)
	for _, slaveGuid := range guids {
		for _, v := range r.api.GetBindingsManager().GetDevicesBindings(slaveGuid) {
			if err := r.api.GetBindingsManager().DeleteBinding(v.SlaveGuid, v.BindingName); err != nil {
				logrus.WithContext(r.ctx).WithFields(logrus.Fields{
					define.LogsPrintCommonLabelOfFunction: "BindingManager.DeleteBinding",
				}).Debugf("delete binding %v %v", v.BindingName, err)
				resp.WriteString(fmt.Sprintf("DELETE %v FAILD, REMOVE BINDING ERROR\n", v.BindingName))
			} else {
				resp.WriteString(fmt.Sprintf("DELETE %v SUCCEED\n", v.BindingName))
			}
		}
	}
	return resp.String(), nil
}