package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

func (r *WebClientPlugin) detail(w http.ResponseWriter, rq *http.Request) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.detail",
	})

	// todo get data
	buffers, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		responseErr(&w,log,"read body %v",err)
		return
	}


	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err = json.Unmarshal(buffers,req)
	if err != nil {
		responseErr(&w,log,"decode bytes %v",err)
	}
	log.Debugf("receive detail clireq %v", req)

	// todo 处理 + return
	res,err := r.detailWithBriefInfo(req)
	if err !=nil {
		responseErr(&w,log,"detail info %v",err)
	}

	resp := &clireq.ClientResponse{
		Message: res,
	}

	bts,err :=json.Marshal(resp)
	if err !=nil {
		responseErr(&w,log,"marshal %v",err)
	}

	if bt, err := w.Write(bts); err != nil {
		log.Errorf("detail function %v", err)
	} else {
		log.Infof(fmt.Sprintf("DETAIL REPONSE (%vB)", bt))
	}
}


func (r *WebClientPlugin) detailWithBriefInfo(req *clireq.ClientRequest) (string, error) {
	switch req.ResourceType {
	//case clireq.ResourceTypeDevices:
	//	return r.detailDevicesInfo(req.Guids), nil
	//case clireq.ResourceTypeServices:
	//	return r.detailServicesInfo(req.Guids), nil
	case clireq.ResourceTypeResources:
		return r.detailResourcesInfo(req.Guids), nil
	case clireq.ResourceTypeClusters:
		return r.detailClustersInfo(req.Guids), nil
	case clireq.ResourceTypeStrategies:
		if req.IsRunning == clireq.IsRunning {
			return r.detailRunningStrategyInfo(req.Guids), nil
		} else {
			return r.detailStaticStrategyInfo(req.Names), nil
		}
	case clireq.ResourceTypeBindings:
		return r.detailBindingsInfo(req.Guids), nil
	default:
		return "", fmt.Errorf("request type error, just allow [resources|clusters|strategies|bindings]")
	}
}

func (r *WebClientPlugin) detailResourcesInfo(guids []string) string {
	resp := new(bytes.Buffer)
	for _, guid := range guids {
		d := r.api.GetResourcesManager().GetResources(guid)
		resp.WriteString(fmt.Sprintf("%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%v\n",
			"*************************************************",
			"GUID", d.GuId,
			"OTYPE", d.OType,
			"STATUS", d.Status,
			"AREA", d.Area,
			"DESCRIPTION", d.Description,
			"PRENODE", d.PreNode,
			"SUBNODE", d.SubNode,
			"CONTROL", d.IsControl,
			"MONITOR", d.IsMonitor,
			"*************************************************"))
	}
	return resp.String()
}

//func (r *WebClientPlugin) detailDevicesInfo(guids []string) string {
//	resp := new(bytes.Buffer)
//	for _, guid := range guids {
//		d := r.api.GetDevicesManager().GetDevices(guid)
//		resp.WriteString(fmt.Sprintf("%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%v\n",
//			"*************************************************",
//			"VERSION", d.Version,
//			"NAME", d.DeviceName,
//			"TYPE", d.DeviceType,
//			"GUID", d.Guid,
//			"RESOURCES_TYPE", d.ResourceType,
//			"RESOURCE_ID", d.ResourceID,
//			"PRODUCT_ID", d.ProductID,
//			"MANMUFACTURE_ID", d.ManufactureID,
//			"*************************************************"))
//	}
//	return resp.String()
//}
//
//func (r *WebClientPlugin) detailServicesInfo(guids []string) string {
//	resp := new(bytes.Buffer)
//	for _, guid := range guids {
//		d := r.api.GetServicesManager().GetServices(guid)
//		resp.WriteString(fmt.Sprintf("%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%v\n",
//			"*************************************************",
//			"VERSION", d.Version,
//			"NAME", d.ServiceName,
//			"TYPE", d.ServiceType,
//			"GUID", d.Guid,
//			"RESOURCES_TYPE", d.ResourceType,
//			"RESOURCE_ID", d.ResourceID,
//			"PRODUCT_ID", d.ProductID,
//			"MANMUFACTURE_ID", d.ManufactureID,
//			"*************************************************"))
//	}
//	return resp.String()
//}

func (r *WebClientPlugin) detailClustersInfo(guids []string) string {
	resp := new(bytes.Buffer)
	for _, guid := range guids {
		d := r.api.GetClustersManager().GetCluster(guid)
		envs := new(bytes.Buffer)
		for k, v := range d.ClusterMasterContainerEnvs {
			envs.WriteString(fmt.Sprintf("\n  -%-20v:\t%v", k, v))
		}
		args := new(bytes.Buffer)
		for k, v := range d.ClusterMasterContainerArgs {
			args.WriteString(fmt.Sprintf("\n  -%-20v:\t%v", k, v))
		}
		res := new(bytes.Buffer)
		for k, v := range d.ClusterMasterContainerRes {
			res.WriteString(fmt.Sprintf("\n  -%-20v:\t%v", k, v))
		}
		slaves := new(bytes.Buffer)
		for k, v := range d.ClusterSlaves {
			slaves.WriteString(fmt.Sprintf("\n  -%-30v:\t%v", k, v))
		}
		resources := new(bytes.Buffer)
		for k, v := range d.ClusterResources {
			resources.WriteString(fmt.Sprintf("\n  -%-30v:\t%v", k, v))
		}
		//devices := new(bytes.Buffer)
		//for k, v := range d.ClusterDevices {
		//	devices.WriteString(fmt.Sprintf("\n  -%-30v:\t%v", k, v))
		//}
		//services := new(bytes.Buffer)
		//for k, v := range d.ClusterServices {
		//	services.WriteString(fmt.Sprintf("\n  -%-30v:\t%v", k, v))
		//}

		resp.WriteString(fmt.Sprintf("%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n\n%v\n",
			"*************************************************",
			"VERSION", d.ClusterVersion,
			"SHORTDESCRIPTION", d.ClusterShortDescription,
			"DESCRIPTION", d.ClusterDescription,
			"NAME", d.ClusterName,
			"KIND", d.ClusterMasterKind,
			"IMAGE", d.ClusterMasterImage,
			"GUID", d.ClusterMasterGuid,
			"CONTAINER_ID", d.ClusterMasterContainerId,
			"CONTAINER_NAME", d.ClusterMasterContainerName,
			"CONTAINER_ENVS", envs.String(),
			"CONTAINER_ARGS", args.String(),
			"CONTAINER_RES", res.String(),
			"CLUSTERS_SLAVES", slaves.String(),
			//"CLUSTERS_DEVICES", devices.String(),
			//"CLUSTERS_SERVICES", services.String(),
			"CLUSTERS_SERVICES", resources.String(),
			"*************************************************"))
	}
	return resp.String()
}

func (r *WebClientPlugin) detailRunningStrategyInfo(guids []string) string {
	resp := new(bytes.Buffer)
	for _, guid := range guids {
		d, err := r.api.GetStrategyManager().GetRunningStrategy(guid)
		if err != nil {
			resp.WriteString(fmt.Sprintf("%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%v\n",
				"*************************************************",
				"VERSION", "NIL",
				"SHORTDESCRIPTION", "NIL",
				"DESCRIPTION", "NIL",
				"NAME", "NIL",
				"KIND", "NIL",
				"IMAGE", "NIL",
				"GUID", guid,
				"CONTAINER_ID", "NIL",
				"CONTAINER_NAME", "NIL",
				"CONTAINER_ENVS", "NIL",
				"*************************************************"))
		} else {
			envs := new(bytes.Buffer)
			for k, v := range d.StrategyContainerEnv {
				envs.WriteString(fmt.Sprintf("\n  -%-20v:\t%v", k, v))
			}

			resp.WriteString(fmt.Sprintf("%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%v\n",
				"*************************************************",
				"VERSION", d.StrategyVersion,
				"SHORTDESCRIPTION", d.StrategyShortDescription,
				"DESCRIPTION", d.StrategyDescription,
				"NAME", d.StrategyName,
				"KIND", d.StrategyKind,
				"IMAGE", d.StrategyImage,
				"GUID", d.StrategyGuid,
				"CONTAINER_ID", d.StrategyContainerId,
				"CONTAINER_NAME", d.StrategyContainerName,
				"CONTAINER_ENVS", envs.String(),
				"*************************************************"))
		}
	}
	return resp.String()
}


func (r *WebClientPlugin) detailStaticStrategyInfo(names []string) string {
	resp := new(bytes.Buffer)
	for _, name := range names {
		d, err := r.api.GetStrategyManager().GetStaticStrategy(name)
		if err != nil {
			resp.WriteString(fmt.Sprintf("%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%v\n",
				"*************************************************",
				"VERSION", "NIL",
				"SHORTDESCRIPTION", "NIL",
				"DESCRIPTION", "NIL",
				"NAME", name,
				"KIND", "NIL",
				"IMAGE", "NIL",
				"CONTAINER_ENVS", "NIL",
				"*************************************************"))
		} else {
			envs := new(bytes.Buffer)
			for k, v := range d.StrategyContainerEnv {
				envs.WriteString(fmt.Sprintf("\n  -%-20v:\t%v", k, v))
			}

			resp.WriteString(fmt.Sprintf("%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%v\n",
				"*************************************************",
				"VERSION", d.StrategyVersion,
				"SHORTDESCRIPTION", d.StrategyShortDescription,
				"DESCRIPTION", d.StrategyDescription,
				"NAME", d.StrategyName,
				"KIND", d.StrategyKind,
				"IMAGE", d.StrategyImage,
				"CONTAINER_ENVS", envs.String(),
				"*************************************************"))
		}
	}
	return resp.String()
}

func (r *WebClientPlugin) detailBindingsInfo(guids []string) string {
	resp := new(bytes.Buffer)
	for _, guid := range guids {
		bds := r.api.GetBindingsManager().GetDevicesBindings(guid)
		for _, d := range bds {
			resp.WriteString(fmt.Sprintf("%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%-20v%v\n%v\n",
				"*************************************************",
				"VERSION", d.Version,
				"NAME", d.BindingName,
				"MASTER", d.MasterGuid,
				"SLAVE", d.SlaveGuid,
				"MASTER_LEVEL", d.MasterLevel,
				"SLAVE_LEVEL", d.SlaveLevel,
				"*************************************************"))
		}
	}
	return resp.String()
}
