package main

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)


func (r *WebClientPlugin) log(w http.ResponseWriter, rq *http.Request) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.log",
	})

	// todo get data
	buffers, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		responseErr(&w,log,"read body %v",err)
		return
	}


	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err = json.Unmarshal(buffers,req)
	if err != nil {
		responseErr(&w,log,"decode bytes %v",err)
	}
	log.Debugf("receive log clireq %v", req)

	// todo 处理 + return

	res,err := r.logStrategyWithInfo(req)
	if err !=nil {
		responseErr(&w,log,"log info %v",err)
	}

	resp := &clireq.ClientResponse{
		Message: res,
	}

	bts,err :=json.Marshal(resp)
	if err !=nil {
		responseErr(&w,log,"marshal %v",err)
	}

	if bt, err := w.Write(bts); err != nil {
		log.Errorf("log function %v", err)
	} else {
		log.Infof(fmt.Sprintf("LOG REPONSE (%vB)", bt))
	}
}

func (r *WebClientPlugin) logStrategyWithInfo(req *clireq.ClientRequest) (string, error) {
	switch req.ResourceType {
	case clireq.ResourceTypeStrategies:
		return r.logRunningStrategiesWithInfo(req.Guids[0])
	default:
		return "", fmt.Errorf("request type error, just allow [running strategies(not static)]")
	}
}

func (r *WebClientPlugin) logRunningStrategiesWithInfo(guid string) (string, error) {
	st, err := r.api.GetStrategyManager().GetRunningStrategy(guid)
	if err != nil {
		return "", err
	}
	return r.api.GetDocker().Log(st.StrategyContainerId)
}
