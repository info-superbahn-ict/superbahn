package main

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)



func (r *WebClientPlugin) pull (w http.ResponseWriter, rq *http.Request) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.pull",
	})

	// todo get data
	buffers, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		responseErr(&w,log,"read body %v",err)
		return
	}


	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err = json.Unmarshal(buffers,req)
	if err != nil {
		responseErr(&w,log,"decode bytes %v",err)
	}
	log.Debugf("receive pull clireq %v", req)

	// todo 处理 + return

	res,err := r.pullStaticStrategy(req)
	if err !=nil {
		responseErr(&w,log,"pull info %v",err)
	}

	resp := &clireq.ClientResponse{
		Message: res,
	}

	bts,err :=json.Marshal(resp)
	if err !=nil {
		responseErr(&w,log,"marshal %v",err)
	}

	if bt, err := w.Write(bts); err != nil {
		log.Errorf("pull function %v", err)
	} else {
		log.Infof(fmt.Sprintf("PULL REPONSE (%vB)", bt))
	}
}

func (r *WebClientPlugin) pullStaticStrategy(req *clireq.ClientRequest) (string, error) {
	return r.api.GetStrategyManager().PullStaticStrategy(req.Names[0])
}