package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)



func (r *WebClientPlugin) run (w http.ResponseWriter, rq *http.Request) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.run",
	})

	// todo get data
	buffers, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		responseErr(&w,log,"read body %v",err)
		return
	}


	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err = json.Unmarshal(buffers,req)
	if err != nil {
		responseErr(&w,log,"decode bytes %v",err)
	}
	log.Debugf("receive run clireq %v", req)

	// todo 处理 + return
	res,err := r.runWithBriefInfo(req)
	if err !=nil {
		responseErr(&w,log,"run info %v",err)
	}

	resp := &clireq.ClientResponse{
		Message: res,
	}

	bts,err :=json.Marshal(resp)
	if err !=nil {
		responseErr(&w,log,"marshal %v",err)
	}

	if bt, err := w.Write(bts); err != nil {
		log.Errorf("run function %v", err)
	} else {
		log.Infof(fmt.Sprintf("RUN REPONSE (%vB)", bt))
	}
}


func (r *WebClientPlugin) runWithBriefInfo(req *clireq.ClientRequest) (string, error) {
	switch req.ResourceType {
	case clireq.ResourceTypeStrategies:
		return r.runStaticStrategiesWithBriefInfo(req.Names)
	default:
		return "", fmt.Errorf("request type error, just allow [static strategies(not running)]")
	}
}

func (r *WebClientPlugin) runStaticStrategiesWithBriefInfo(names []string) (string, error) {
	resp := new(bytes.Buffer)
	for _, name := range names {
		if err := r.api.RunStaticStrategy(name); err != nil {
			resp.WriteString(fmt.Sprintf("RUN STATIC STRATEGY %v FAILED: %v\n", name, err))
		} else {
			resp.WriteString(fmt.Sprintf("RUN STATIC STRATEGY %v SUCCEED\n", name))
		}
	}
	return resp.String(), nil
}