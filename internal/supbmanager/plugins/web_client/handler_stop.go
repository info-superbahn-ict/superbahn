package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/clireq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)


func (r *WebClientPlugin) stop (w http.ResponseWriter, rq *http.Request) {
	// todo init
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.stop",
	})

	// todo get data
	buffers, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		responseErr(&w,log,"read body %v",err)
		return
	}


	// todo 解码参数
	var req = &clireq.ClientRequest{}
	err = json.Unmarshal(buffers,req)
	if err != nil {
		responseErr(&w,log,"decode bytes %v",err)
	}
	log.Debugf("receive stop clireq %v", req)

	// todo 处理 + return

	res,err := r.stopRunningStrategiesWithBriefInfo(req)
	if err !=nil {
		responseErr(&w,log,"stop info %v",err)
	}

	resp := &clireq.ClientResponse{
		Message: res,
	}

	bts,err :=json.Marshal(resp)
	if err !=nil {
		responseErr(&w,log,"marshal %v",err)
	}

	if bt, err := w.Write(bts); err != nil {
		log.Errorf("stop function %v", err)
	} else {
		log.Infof(fmt.Sprintf("STOP REPONSE (%vB)", bt))
	}
}

func (r *WebClientPlugin) stopRunningStrategiesWithBriefInfo(req *clireq.ClientRequest) (string, error) {
	resp := new(bytes.Buffer)
	for _, guid := range req.Guids {
		if err := r.api.DeleteRunningStrategy(guid); err != nil {
			resp.WriteString(fmt.Sprintf("STOP STRATEGY %v FAILED: %v", guid, err))
		} else {
			resp.WriteString(fmt.Sprintf("STOP STRATEGY %v SUCCEED", guid))
		}
	}
	return resp.String(), nil
}