package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func responseErr(w *http.ResponseWriter, entry *log.Entry,format string,err error) {
	msg := fmt.Sprintf(format, err)
	if _, err := fmt.Fprintf(*w, msg); err != nil {
		entry.Errorf("write response %v", err)
	}
	entry.Error(err)
}
