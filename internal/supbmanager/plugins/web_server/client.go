package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitee.com/info-superbahn-ict/superbahn/pkg/plugins"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/apis"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
)

const (
	WebServerReadTimeOut  = 5
	WebServerWriteTimeOut = 5
)

func PluginsName() (string, string, string) {
	return "WebServer", "default", "test_token"
}

func NewPlugin(ctx context.Context, p *apis.Apis) (plugins.PluginFactory, error) {
	return &WebServerPlugin{
		ctx: ctx,
		api: p,
	}, nil
}

type WebServerPlugin struct {
	ctx    context.Context
	api    *apis.Apis
	server *http.Server
}

func (c *WebServerPlugin) Close() {
	log := logrus.WithContext(c.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "server.web.server.Run",
	})
	name, user, token := PluginsName()
	log.Infof("close plugin %v, user, %v teoken %v", name, user, token)
}

func (c *WebServerPlugin) Call(f string, args ...interface{}) (interface{}, error) {
	return nil, nil
}

// func (c *WebServerPlugin) Run() {

// 	patternPrefix := "/supbmanager/plugins/webserver/run"

// 	serve := http.NewServeMux()
// 	serve.HandleFunc(patternPrefix+"get", c.get)

// 	rsServer := cors.Default().Handler(serve)

// 	c.server = &http.Server{
// 		Addr:         c.api.GetOption().WebServerUrl,
// 		ReadTimeout:  WebServerReadTimeOut * time.Second,
// 		WriteTimeout: WebServerWriteTimeOut * time.Second,
// 		Handler: rsServer,
// 	}

// 	go func() {
// 		if err := c.server.ListenAndServe(); err != nil {
// 			logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 				define.LogsPrintCommonLabelOfFunction: "server.web.Run",
// 			}).Infof("server stop %v", err)
// 		}
// 		logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 			define.LogsPrintCommonLabelOfFunction: "server.web.Run",
// 		}).Infof("listen closed")
// 	}()

// 	name,user,token := PluginsName()
// 	logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 		define.LogsPrintCommonLabelOfFunction: "server.web.Run",
// 	}).Infof("plugins %v initialization complete, user %v ,token %v", name,user,token)

// 	<-c.ctx.Done()
// }

// func (c *WebServerPlugin) Delete() {

// 	patternPrefix := "/supbmanager/plugins/webserver/delete"

// 	serve := http.NewServeMux()
// 	serve.HandleFunc(patternPrefix+"get", c.get)

// 	rsServer := cors.Default().Handler(serve)

// 	c.server = &http.Server{
// 		Addr:         c.api.GetOption().WebServerUrl,
// 		ReadTimeout:  WebServerReadTimeOut * time.Second,
// 		WriteTimeout: WebServerWriteTimeOut * time.Second,
// 		Handler: rsServer,
// 	}

// 	go func() {
// 		if err := c.server.ListenAndServe(); err != nil {
// 			logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 				define.LogsPrintCommonLabelOfFunction: "server.web.Delete",
// 			}).Infof("server stop %v", err)
// 		}
// 		logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 			define.LogsPrintCommonLabelOfFunction: "server.web.Delete",
// 		}).Infof("listen closed")
// 	}()

// 	name,user,token := PluginsName()
// 	logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 		define.LogsPrintCommonLabelOfFunction: "server.web.Delete",
// 	}).Infof("plugins %v initialization complete, user %v ,token %v", name,user,token)

// 	<-c.ctx.Done()
// }

// func (c *WebServerPlugin) Load() {

// 	patternPrefix := "/supbmanager/plugins/webserver/load"

// 	serve := http.NewServeMux()
// 	serve.HandleFunc(patternPrefix+"get", c.get)

// 	rsServer := cors.Default().Handler(serve)

// 	c.server = &http.Server{
// 		Addr:         c.api.GetOption().WebServerUrl,
// 		ReadTimeout:  WebServerReadTimeOut * time.Second,
// 		WriteTimeout: WebServerWriteTimeOut * time.Second,
// 		Handler: rsServer,
// 	}

// 	go func() {
// 		if err := c.server.ListenAndServe(); err != nil {
// 			logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 				define.LogsPrintCommonLabelOfFunction: "server.web.Load",
// 			}).Infof("server stop %v", err)
// 		}
// 		logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 			define.LogsPrintCommonLabelOfFunction: "server.web.Load",
// 		}).Infof("listen closed")
// 	}()

// 	name,user,token := PluginsName()
// 	logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 		define.LogsPrintCommonLabelOfFunction: "server.web.Load",
// 	}).Infof("plugins %v initialization complete, user %v ,token %v", name,user,token)

// 	<-c.ctx.Done()
// }

// func (c *WebServerPlugin) Upload() {

// 	patternPrefix := "/supbmanager/plugins/webserver/upload"

// 	serve := http.NewServeMux()
// 	serve.HandleFunc(patternPrefix+"get", c.get)

// 	rsServer := cors.Default().Handler(serve)

// 	c.server = &http.Server{
// 		Addr:         c.api.GetOption().WebServerUrl,
// 		ReadTimeout:  WebServerReadTimeOut * time.Second,
// 		WriteTimeout: WebServerWriteTimeOut * time.Second,
// 		Handler: rsServer,
// 	}

// 	go func() {
// 		if err := c.server.ListenAndServe(); err != nil {
// 			logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 				define.LogsPrintCommonLabelOfFunction: "server.web.Upload",
// 			}).Infof("server stop %v", err)
// 		}
// 		logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 			define.LogsPrintCommonLabelOfFunction: "server.web.Upload",
// 		}).Infof("listen closed")
// 	}()

// 	name,user,token := PluginsName()
// 	logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 		define.LogsPrintCommonLabelOfFunction: "server.web.Upload",
// 	}).Infof("plugins %v initialization complete, user %v ,token %v", name,user,token)

// 	<-c.ctx.Done()
// }

// func (c *WebServerPlugin) Stop() {

// 	patternPrefix := "/supbmanager/plugins/webserver/stop"

// 	serve := http.NewServeMux()
// 	serve.HandleFunc(patternPrefix+"get", c.get)

// 	rsServer := cors.Default().Handler(serve)

// 	c.server = &http.Server{
// 		Addr:         c.api.GetOption().WebServerUrl,
// 		ReadTimeout:  WebServerReadTimeOut * time.Second,
// 		WriteTimeout: WebServerWriteTimeOut * time.Second,
// 		Handler: rsServer,
// 	}

// 	go func() {
// 		if err := c.server.ListenAndServe(); err != nil {
// 			logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 				define.LogsPrintCommonLabelOfFunction: "server.web.Stop",
// 			}).Infof("server stop %v", err)
// 		}
// 		logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 			define.LogsPrintCommonLabelOfFunction: "server.web.Stop",
// 		}).Infof("listen closed")
// 	}()

// 	name,user,token := PluginsName()
// 	logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 		define.LogsPrintCommonLabelOfFunction: "server.web.Stop",
// 	}).Infof("plugins %v initialization complete, user %v ,token %v", name,user,token)

// 	<-c.ctx.Done()
// }

// func (c *WebServerPlugin) Restart() {

// 	patternPrefix := "/supbmanager/plugins/webserver/restart"

// 	serve := http.NewServeMux()
// 	serve.HandleFunc(patternPrefix+"get", c.get)

// 	rsServer := cors.Default().Handler(serve)

// 	c.server = &http.Server{
// 		Addr:         c.api.GetOption().WebServerUrl,
// 		ReadTimeout:  WebServerReadTimeOut * time.Second,
// 		WriteTimeout: WebServerWriteTimeOut * time.Second,
// 		Handler: rsServer,
// 	}

// 	go func() {
// 		if err := c.server.ListenAndServe(); err != nil {
// 			logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 				define.LogsPrintCommonLabelOfFunction: "server.web.Restart",
// 			}).Infof("server stop %v", err)
// 		}
// 		logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 			define.LogsPrintCommonLabelOfFunction: "server.web.Restart",
// 		}).Infof("listen closed")
// 	}()

// 	name,user,token := PluginsName()
// 	logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 		define.LogsPrintCommonLabelOfFunction: "server.web.Restart",
// 	}).Infof("plugins %v initialization complete, user %v ,token %v", name,user,token)

// 	<-c.ctx.Done()
// }

func (c *WebServerPlugin) Run() {

	patternPrefix := "/supbmanager/plugins/webserver/"

	serve := http.NewServeMux()
	serve.HandleFunc(patternPrefix+"get", c.get)
	serve.HandleFunc(patternPrefix+"delete", c.delete)
	serve.HandleFunc(patternPrefix+"run", c.run)
	serve.HandleFunc(patternPrefix+"stop", c.stop)
	serve.HandleFunc(patternPrefix+"load", c.load)
	serve.HandleFunc(patternPrefix+"upload", c.upload)
	serve.HandleFunc(patternPrefix+"restart", c.restart)
	serve.HandleFunc(patternPrefix+"update", c.update)
	serve.HandleFunc(patternPrefix+"push", c.push)
	serve.HandleFunc(patternPrefix+"pull", c.pull)
	rsServer := cors.Default().Handler(serve)

	fmt.Printf(c.api.GetOption().WebServerUrl)

	c.server = &http.Server{
		Addr:        c.api.GetOption().WebServerUrl,
		ReadTimeout: WebServerReadTimeOut * time.Second,
		Handler:     rsServer,
	}

	go func() {
		if err := c.server.ListenAndServe(); err != nil {
			logrus.WithContext(c.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "server.web.Run",
			}).Infof("server stop %v", err)
		}
		logrus.WithContext(c.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "server.web.Run",
		}).Infof("listen closed")
	}()

	name, user, token := PluginsName()
	logrus.WithContext(c.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "server.web.Run",
	}).Infof("plugins %v initialization complete, user %v ,token %v", name, user, token)

	<-c.ctx.Done()
}
