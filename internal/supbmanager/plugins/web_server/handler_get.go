package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/webreq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

// func (c *WebServerPlugin) get(w http.ResponseWriter, r *http.Request) {
// 	log := logrus.WithContext(c.ctx).WithFields(logrus.Fields{
// 		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.get",
// 	})
// 	if bt, err := w.Write([]byte("hello")); err != nil {
// 		log.Errorf("get function %v", err)
// 	} else {
// 		log.Infof(fmt.Sprintf("GET REPONSE (%vB)", bt))
// 	}
// }

func (c *WebServerPlugin) get(w http.ResponseWriter, r *http.Request) {
	// todo init
	log := logrus.WithContext(c.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.get",
	})

	// todo get data
	buffers, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responseErr(&w, log, "read body %v", err)
		return
	}

	log.Debugf("receive get clireq buffers %s", buffers)

	// todo 解码参数
	var req = &webreq.Request{}
	err = json.Unmarshal(buffers, req)
	if err != nil {
		responseErr(&w, log, "decode bytes %v", err)
	}
	log.Debugf("receive get clireq %v", req)

	// todo 处理 + return
	var resp = &webreq.Response{}

	switch req.Op {
	case webreq.OpListStaticStrategies:
		resp, err = listStaticStrategies(c.api, req)
		if err != nil {
			responseErr(&w, log, "get info %v", err)
		}
	case webreq.OpListRunningStrategies:
		resp, err = listRunningStrategies(c.api, req)
		if err != nil {
			responseErr(&w, log, "get info %v", err)
		}
	case webreq.OpListApplications:
		resp, err = listApplications(c.api, req)
		if err != nil {
			responseErr(&w, log, "get info %v", err)
		}
	case webreq.OpListImage:
		resp, err = listImages(c.api, req)
		if err != nil {
			responseErr(&w, log, "get info %v", err)
		}
	case webreq.OpGetStaticStrategy:
		resp, err = getStaticStrategy(c.api, req)
		if err != nil {
			responseErr(&w, log, "get info %v", err)
		}
	case webreq.OpGetRunningStrategy:
		resp, err = getRunningStrategy(c.api, req)
		if err != nil {
			responseErr(&w, log, "get info %v", err)
		}
	case webreq.OpGetClusterInfo:
		resp, err = getCluster(c.api, req)
		if err != nil {
			responseErr(&w, log, "get info %v", err)
		}
	case webreq.OpGetClusterResources:
		resp, err = getClusterResources(c.api, req)
		if err != nil {
			responseErr(&w, log, "get info %v", err)
		}
	//case webreq.OpGetClusterDevices:
	//	resp,err = getClusterDevices(c.api,req)
	//	if err !=nil {
	//		responseErr(&w,log,"get info %v",err)
	//	}
	//case webreq.OpGetClusterServices:
	//	resp,err = getClusterServices(c.api,req)
	//	if err !=nil {
	//		responseErr(&w,log,"get info %v",err)
	//	}
	case webreq.OpGetStatisticClusterInfo:
		resp, err = getStatisticClusterInfo(c.api, req)
		if err != nil {
			responseErr(&w, log, "get info %v", err)
		}
	case webreq.OpGetSpecificResources:
		resp,err = getSpecificResources(c.api,req)
		if err !=nil {
			responseErr(&w,log,"get info %v",err)
		}
	case webreq.OpListResourcesStatistic:
		resp,err = listResourcesStatistic(c.api,req)
		if err !=nil {
			responseErr(&w,log,"get info %v",err)
		}
	case webreq.OpGetRelationshipGraph:
		resp, err = getRelationshipGraph(c.api, req)
		if err != nil {
			responseErr(&w, log, "get info %v", err)
		}
	default:

	}

	bts, err := json.Marshal(resp)
	if err != nil {
		responseErr(&w, log, "marshal %v", err)
	}

	if bt, err := w.Write(bts); err != nil {
		log.Errorf("get function %v", err)
	} else {
		log.Infof(fmt.Sprintf("GET REPONSE (%vB)", bt))
	}
}
