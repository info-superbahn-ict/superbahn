package main

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/webreq"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

func (c *WebServerPlugin) push (w http.ResponseWriter, r *http.Request) {
	// todo init
	log := logrus.WithContext(c.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "plugins.web.server.push",
	})

	// todo get data
	buffers, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responseErr(&w,log,"read body %v",err)
		return
	}

	log.Debugf("receive push clireq %s", buffers)

	// todo 解码参数
	var req = &webreq.Request{}
	err = json.Unmarshal(buffers,req)
	if err != nil {
		responseErr(&w,log,"decode bytes %v",err)
	}
	log.Debugf("receive push clireq %v", req)

	// todo 处理 + return
	var resp = &webreq.Response{}

	switch req.Op {
	case webreq.OpPushStaticStrategy:
		resp,err = pushStaticStrategy(c.api,req)
		if err !=nil {
			responseErr(&w,log,"push info %v",err)
		}
	default:

	}

	bts,err :=json.Marshal(resp)
	if err !=nil {
		responseErr(&w,log,"marshal %v",err)
	}

	if bt, err := w.Write(bts); err != nil {
		log.Errorf("push function %v", err)
	} else {
		log.Infof(fmt.Sprintf("PUSH REPONSE (%vB)", bt))
	}
}

