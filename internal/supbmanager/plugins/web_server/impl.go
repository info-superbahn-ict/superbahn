package main

import (
	"fmt"
	"net/http"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/apis"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/webreq"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
)

func responseErr(w *http.ResponseWriter, entry *log.Entry, format string, err error) {
	msg := fmt.Sprintf(format, err)
	if _, err := fmt.Fprintf(*w, msg); err != nil {
		entry.Errorf("write response %v", err)
	}
	entry.Error(err)
}

func listStaticStrategies(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	sts := api.GetStrategyManager().ListStaticStrategyReturnList()

	api.Run()
	return &webreq.Response{
		Data: sts,
	}, nil
}

func listRunningStrategies(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	sts := api.GetStrategyManager().ListRunningStrategyReturnList()

	return &webreq.Response{
		Data: sts,
	}, nil
}

func listApplications(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	sts := api.GetApplicationManager().ListApplicationsList()

	return &webreq.Response{
		Data: sts,
	}, nil
}

func listImages(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	images, err := api.ListImages()
	if err != nil {
		return nil, fmt.Errorf("list image %v", err)
	}
	return &webreq.Response{
		Data: images,
	}, nil
}

func getStaticStrategy(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	st, err := api.GetStrategyManager().GetStaticStrategy(req.Object)
	if err != nil {
		return nil, fmt.Errorf("get static strategy %v", err)
	}
	return &webreq.Response{
		Data: st,
	}, nil
}

func getRunningStrategy(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	st, err := api.GetStrategyManager().GetRunningStrategy(req.Object)
	if err != nil {
		return nil, fmt.Errorf("get static strategy %v", err)
	}
	return &webreq.Response{
		Data: st,
	}, nil
}

func getCluster(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	st := api.GetClustersManager().GetCluster(req.Object)
	if st == nil {
		return nil, fmt.Errorf("cluster is not exist")
	}
	return &webreq.Response{
		Data: st,
	}, nil
}

func getClusterResources(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	st := api.GetClustersManager().GetCluster(req.Object)
	if st == nil {
		return nil, fmt.Errorf("cluster does not exist")
	}
	ds := make([]*spongeregister.Resource, len(st.ClusterResources))
	i := 0
	for guid := range st.ClusterResources {
		ds[i] = api.GetResourcesManager().GetResources(guid)
		i++
	}
	return &webreq.Response{
		Data: ds,
	}, nil
}

//
//func getClusterDevices(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
//	st :=api.GetClustersManager().GetCluster(req.Object)
//	if st==nil {
//		return nil, fmt.Errorf("cluster is not exist")
//	}
//	ds := make([]*devices.Device,len(st.ClusterDevices))
//	i :=0
//	for guid := range st.ClusterDevices {
//		ds[i] = api.GetDevicesManager().GetDevices(guid)
//		i++
//	}
//	return &webreq.Response{
//		Data: ds,
//	},nil
//}
//
//func getClusterServices(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
//	st :=api.GetClustersManager().GetCluster(req.Object)
//	if st==nil {
//		return nil, fmt.Errorf("cluster is not exist")
//	}
//	ds := make([]*services.Service,len(st.ClusterDevices))
//	i :=0
//	for guid := range st.ClusterDevices {
//		ds[i] = api.GetServicesManager().GetServices(guid)
//		i++
//	}
//	return &webreq.Response{
//		Data: ds,
//	},nil
//}

func deleteStaticStrategy(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	st := api.GetStrategyManager().DeleteStaticStrategy(req.Object)
	if st != nil {
		return nil, fmt.Errorf("delete static strategy ")
	}
	return &webreq.Response{
		Data: st,
	}, nil
}

func deleteRunningStrategy(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {

	err := api.DeleteRunningStrategy(req.Object)
	if err != nil {
		return nil, fmt.Errorf("delete running strategy ")
	}
	return &webreq.Response{
		Data: "",
	}, err
}

func deleteImage(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	err := api.DeleteImage(req.Object)
	if err != nil {
		return nil, fmt.Errorf("delete image")
	}
	return &webreq.Response{
		Data: err,
	}, nil
}

func uploadStrategy(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	st := api.GetStrategyManager().UploadRunningStrategy(req.Object)
	if st != nil {
		return nil, fmt.Errorf("upload running strategy ")
	}
	return &webreq.Response{
		Data: st,
	}, nil
}

func uploadImage(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	err := api.UploadImage(req.Object)
	if err != nil {
		return nil, fmt.Errorf("upload image")
	}
	return &webreq.Response{
		Data: err,
	}, nil
}

func updateStaticStrategy(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	err := api.GetStrategyManager().UpdateStaticStrategy(req.Object, req.Strategy)
	if err != nil {
		return nil, fmt.Errorf("update running strategy ")
	}
	return &webreq.Response{
		Data: err,
	}, nil
}

func updateRunningStrategy(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	err := api.GetStrategyManager().UpdateRunningStrategy(req.Object, req.Strategy)
	if err != nil {
		return nil, fmt.Errorf("update static strategy")
	}
	return &webreq.Response{
		Data: err,
	}, nil
}

func loadStrategy(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	st := api.GetStrategyManager().LoadStaticStrategy(req.Object)
	if st != nil {
		return nil, fmt.Errorf("load static strategy ")
	}
	return &webreq.Response{
		Data: st,
	}, nil
}

func loadImage(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	err := api.LoadImage(req.Object)
	if err != nil {
		return nil, fmt.Errorf("load image")
	}
	return &webreq.Response{
		Data: err,
	}, nil
}

func runStaticStrategy(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	err := api.RunStaticStrategy(req.Object)
	if err != nil {
		return nil, fmt.Errorf("run strategy %v", err)
	}

	logrus.Debugf("run strategy %v", err)

	st, err := api.GetStrategyManager().GetRunningStrategy(req.Object)
	if err != nil {
		return nil, fmt.Errorf("get running strategy %v", err)
	}

	logrus.Debugf("get running strategy %v %v", err, st)

	return &webreq.Response{
		Data: st,
	}, nil
}

func runApplication(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {

	err := api.RunApplication(req.Object)
	if err != nil {
		return nil, fmt.Errorf("run application %v", err)
	}

	logrus.Debugf("run application %v", err)

	st, err := api.GetApplicationManager().GetApplication(req.Object)
	if err != nil {
		return nil, fmt.Errorf("get application %v", err)
	}

	logrus.Debugf("get application %v %v", err, st)

	return &webreq.Response{
		Data: st,
	}, nil
}

// func stopApplication(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {

// 	err:= api.StopApplication(req.Object)
// 	if err != nil {
// 		return nil, fmt.Errorf("run application %v",err)
// 	}

// 	logrus.Debugf("run application %v",err)

// 	st,err := api.GetApplicationManager().GetApplication(req.Object)
// 	if err != nil {
// 		return nil, fmt.Errorf("get application %v",err)
// 	}

// 	logrus.Debugf("get application %v %v",err,st)

// 	return &webreq.Response{
// 		Data: st,
// 	},nil
// }

func stopRunningStrategy(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {

	err := api.StopRunningStrategy(req.Object)
	if err != nil {
		return nil, fmt.Errorf("stop strategy %v", err)
	}

	logrus.Debugf("stop strategy %v", err)

	st, err := api.GetStrategyManager().GetStaticStrategy(req.Object)
	if err != nil {
		return nil, fmt.Errorf("get static strategy %v", err)
	}

	logrus.Debugf("get static strategy %v %v", err, st)

	return &webreq.Response{
		Data: st,
	}, nil
}

func restartStrategy(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {

	err := api.RestartRunningStrategy(req.Object)
	if err != nil {
		return nil, fmt.Errorf("stop strategy %v", err)
	}

	logrus.Debugf("stop strategy %v", err)

	st, err := api.GetStrategyManager().GetRunningStrategy(req.Object)
	if err != nil {
		return nil, fmt.Errorf("get running strategy %v", err)
	}

	logrus.Debugf("get running strategy %v %v", err, st)
	return &webreq.Response{
		Data: nil,
	}, nil
}

func getStatisticClusterInfo(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {

	st := api.GetClustersManager().GetStatisticClusterInfo(req.Object)
	if st == nil {
		return nil, fmt.Errorf("cluster does not exist")
	}

	if len(st.ClusterSlaves) == 0 {
		return nil, nil
	}

	listMap := make(map[string]int, len(st.ClusterSlaves))

	for _, v := range st.ClusterSlaves {
		switch v {
		case spongeregister.TypeServer:
			if _, ok := listMap["server"]; !ok {
				listMap["server"] = 0
			}
			listMap["server"]++
		case spongeregister.TypePc:
			if _, ok := listMap["pc"]; !ok {
				listMap["pc"] = 0
			}
			listMap["pc"]++
		case spongeregister.TypeDocker:
			if _, ok := listMap["container"]; !ok {
				listMap["container"] = 0
			}
			listMap["container"]++
		case spongeregister.TypeDevice:
			if _, ok := listMap["device"]; !ok {
				listMap["device"] = 0
			}
			listMap["device"]++
		case spongeregister.TypeNervous:
			if _, ok := listMap["nervous"]; !ok {
				listMap["nervous"] = 0
			}
			listMap["nervous"]++
		case spongeregister.TypeControlCenter:
			if _, ok := listMap["controller"]; !ok {
				listMap["controller"] = 0
			}
			listMap["controller"]++
		case spongeregister.TypeStrategy:
			if _, ok := listMap["strategy"]; !ok {
				listMap["strategy"] = 0
			}
			listMap["strategy"]++
		case spongeregister.TypeCluster:
			if _, ok := listMap["cluster"]; !ok {
				listMap["cluster"] = 0
			}
			listMap["cluster"]++
		}
	}

	return &webreq.Response{
		Data: listMap,
	}, nil
}

func getRelationshipGraph(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	st := api.GetClustersManager().GetRelationshipGraph(req.Object)
	if st == nil {
		return nil, fmt.Errorf("cluster does not have a relationship")
	}
	return &webreq.Response{
		Data: st,
	}, nil
}

func getSpecificResources(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	st := api.GetResourcesManager().GetSpecificResources(req.Object)
	if st == nil {
		return nil, fmt.Errorf("resource does not exist")
	}
	return &webreq.Response{
		Data: st,
	}, nil
}

func listResourcesStatistic(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	st := api.GetResourcesManager().ListResourcesStatistic()
	if st == nil {
		return nil, fmt.Errorf("resource does not exist")
	}
	return &webreq.Response{
		Data: st,
	}, nil
}

func pushStaticStrategy(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	err := api.GetStrategyManager().PushStaticStrategy(req.Data)
	if err != nil {
		return nil, fmt.Errorf("push static strayegy")
	}
	return &webreq.Response{
		Data: err,
	}, nil
}

func pullStaticStrategy(api *apis.Apis, req *webreq.Request) (*webreq.Response, error) {
	st, err := api.GetStrategyManager().PullStaticStrategy(req.Object)
	if err != nil {
		return nil, fmt.Errorf("pull static strategy %v", err)
	}
	return &webreq.Response{
		Data: st,
	}, nil
}
