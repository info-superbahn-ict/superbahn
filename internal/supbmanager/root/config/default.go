package config

import (
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/options"
	"github.com/sirupsen/logrus"
	"github.com/spf13/pflag"
	"io"
	"os"
)

func SetDefaultOpts(c *options.Options) {
	c.EtcdEndPoints = []string{options.DefaultedEtcd1}
	c.EtcdTimeOutSeconds = options.DefaultedEtcdTimeOut

	c.ElkUrl = options.DefaultedElkUrl
	c.ElkTimeOutSeconds = options.DefaultedElkTimeOut

	c.WebServerUrl = options.DefaultWebUrl
	c.CliServerUrl = options.DefaultCliUrl

	c.NervousConfig = options.DefaultNervousConfig
	c.LogPath = options.DefaultLogPath
	c.PluginPath = options.DefaultPluginsPath
	c.DockerUrl = options.DefaultDockerUrl
}

func CheckOpts(c *options.Options) error {

	if c.EtcdEndPoints == nil {
		return fmt.Errorf("etcd points miss")
	}

	if c.ElkUrl == "" {
		return fmt.Errorf("elk url miss")
	}

	if c.NervousConfig == "" {
		return fmt.Errorf("nervous config miss")
	}

	if c.LogPath == "" {
		return fmt.Errorf("log path miss")
	}
	return nil
}


func InstallOpts(flags *pflag.FlagSet, c *options.Options) {
	flags.StringSliceVar(&c.EtcdEndPoints, "etcd", c.EtcdEndPoints, "the etcd urls eg. \"10.130.10.245:14001,xxx.\"")
	flags.StringVar(&c.ElkUrl, "elk", c.ElkUrl, "the elk url eg. \"http://152.136.134.100:9200\"")
	flags.StringVar(&c.NervousConfig, "nvn", c.NervousConfig, "the nervous config path")
	flags.StringVar(&c.LogPath, "log", c.LogPath, "the log path")
	flags.StringVar(&c.DockerUrl, "collector_docker", c.DockerUrl, "the log path")
}

func InitLogs(c *options.Options)error  {
	if file,err :=os.OpenFile(c.LogPath,os.O_CREATE|os.O_WRONLY|os.O_APPEND,0777);err!=nil{
		return fmt.Errorf("open log file %v",err)
	}else{
		logrus.SetOutput(io.MultiWriter(io.Writer(file),os.Stdout))
	}
	return nil
}