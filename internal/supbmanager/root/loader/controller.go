package loader

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/plugins"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/apis"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/supbmanager"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"path/filepath"
	"plugin"
	"sync"
)


type Loader struct {
	ctx      context.Context
	manager *supbmanager.Manager
	plugins  sync.Map
}

func NewLoader(ctx context.Context,m *supbmanager.Manager) *Loader {
	return &Loader{
		ctx: ctx,
		manager:m,
	}
}

func (c *Loader) Run(){
	entry := log.WithContext(c.ctx).WithFields(log.Fields{
		define.LogsPrintCommonLabelOfFunction: "corerRun",
	})

	// todo load local Loader
	cfg := c.manager.GetOption()
	if err := c.loadPlugins(cfg.PluginPath);err !=nil{
		entry.Errorf("load local plugins %v",err)
	}

	// todo run Loader
	//c.manager.GetProvider().ApplyPlugin(c.CallPlugins)

	entry.Info("loader is running")
	<-c.ctx.Done()
	entry.Info("loader is closed")
}

func (c *Loader) loadPlugins (path string) error{
	entry := log.WithContext(c.ctx).WithFields(log.Fields{
		define.LogsPrintCommonLabelOfFunction: "loadPlugins",
	})

	if fis,err := ioutil.ReadDir(path);err !=nil{
		return fmt.Errorf("check path %v",err)
	}else{
		for _,file := range fis {
			var (
				name,user,token string
				pluNew          plugins.PluginFactory
			)

			entry.Infof("load Loader %v",file.Name())

			p ,err := plugin.Open(filepath.Join(path,file.Name()))
			if err!=nil {
				return fmt.Errorf("load plugins %v",err)
			}

			n , err:=p.Lookup("PluginsName")
			if err != nil {
				return fmt.Errorf("load function PluginsName %v",err)
			}


			if getName, ok := n.(func() (string,string,string)); ok {
				name,user,token = getName()
			}else {
				return fmt.Errorf("can't tranfrom function PluginsName %v",err)
			}

			plu , err:=p.Lookup("NewPlugin")
			if err != nil {
				return fmt.Errorf("load function NewPlugin %v",err)
			}

			if NewPlugin, ok := plu.(func(context.Context,*apis.Apis)(plugins.PluginFactory,error)); ok {
				// todo init
				api,err := apis.NewUserApis(c.ctx,user,token,c.manager)
				if err !=nil {
					return fmt.Errorf("new apis %v",err)
				}
				pluNew,err = NewPlugin(c.ctx,api)
				if err !=nil{
					return fmt.Errorf("new plugins %v",err)
				}
				if err = c.register(name,pluNew);err !=nil{
					return fmt.Errorf("regiter plugins %v",err)
				}
			}else {
				return fmt.Errorf("can't find function PluginsName %v",err)
			}
		}
	}

	// todo run it
	c.plugins.Range(func(key, value interface{}) bool {
		go value.(plugins.PluginFactory).Run()
		return true
	})

	return nil
}

func (c *Loader)Close() error {
	c.plugins.Range(func(key, value interface{}) bool {
		value.(plugins.PluginFactory).Close()
		return true
	})
	return nil
}


func (c *Loader) register(name string,plu plugins.PluginFactory) error{
	if _,ok:=c.plugins.Load(name);ok{
		return fmt.Errorf("plugins %v aready registered",name)
	}
	c.plugins.Store(name,plu)
	return nil
}

func (c *Loader) remove(name string){
	if plu,ok:=c.plugins.Load(name);ok{
		plu.(plugins.PluginFactory).Close()
	}
	c.plugins.Delete(name)
}

func (c *Loader) CallPlugins(name string,f string,args... interface{})(interface{},error){
	if plu,ok:=c.plugins.Load(name);!ok{
		return nil,fmt.Errorf("plugins %v not exist",name)
	}else {
		if d,err := plu.(plugins.PluginFactory).Call(f,args);err !=nil{
			return nil,fmt.Errorf("call plugins %v",err)
		}else {
			return d,nil
		}

	}
}