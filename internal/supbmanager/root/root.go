package controller

import (
	"context"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/root/config"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/root/loader"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/supbmanager"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/third_party/docker"
	"gitee.com/info-superbahn-ict/superbahn/third_party/elk"
	"gitee.com/info-superbahn-ict/superbahn/third_party/etcd"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/options"

	"fmt"
	"github.com/sirupsen/logrus"

	example "gitee.com/info-superbahn-ict/superbahn/example/supbmanager"
	"github.com/spf13/cobra"
	"time"
)

func NewCommand(ctx context.Context, name string) (*cobra.Command, error) {
	var opt = &options.Options{}

	// providers program entry, and passing consts pointer
	cmd := &cobra.Command{
		Use:   name,
		Short: name + "manager.",
		Long:  name + `manager`,
		RunE: func(cmd *cobra.Command, args []string) error {
			return runRootCommand(ctx, opt)
		},
	}

	// load config config, low level
	config.SetDefaultOpts(opt)

	// load file config, middle level
	//todo add file config here

	// load command config, high level
	config.InstallOpts(cmd.Flags(), opt)
	return cmd, nil
}

// main routine
func runRootCommand(ctx context.Context, opt *options.Options) error {
	var (
		err error
		lg  *loader.Loader

		ed      etcd.Recorder
		ek      elk.Recorder
		nu      supbnervous.Controller
		dk      docker.Controller
		manager *supbmanager.Manager
	)

	log := logrus.WithContext(ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "root",
	})

	// todo sponge register guid
	guid := define.RPCCommonGuidOfManager

	// 初始化日志
	if err = config.InitLogs(opt); err != nil {
		return fmt.Errorf("init log %v", err)
	}

	// 检查参数
	if err = config.CheckOpts(opt); err != nil {
		return fmt.Errorf("check config %v", err)
	}

	// 创建 elk
	//if ek, err = elk.NewMetricsRecorder(ctx, opt.ElkUrl); err != nil {
	//	return fmt.Errorf("new elk client %v", err)
	//}

	// 创建 etcd
	if ed, err = etcd.NewMetasRecorder(ctx, opt.EtcdEndPoints, time.Duration(opt.EtcdTimeOutSeconds)*time.Second); err != nil {
		return fmt.Errorf("new etcd client %v", err)
	}

	// 连接总线
	if nu, err = kafkaNervous.NewNervous(ctx, opt.NervousConfig, guid); err != nil {
		return fmt.Errorf("new nerouvs client %v", err)
	}

	log.Infof("adress %p",nu)

	// 连接docker
	if dk, err = docker.NewDockerController(ctx, opt.DockerUrl); err != nil {
		return fmt.Errorf("new collector_docker client %v", err)
	}
	// 初始化测试数据
	example.InitExampleData(ed)

	if manager,err = supbmanager.NewManager(ctx,ed,ek,nu,dk,opt);err !=nil{
		return fmt.Errorf("new manager %v", err)
	}

	lg = loader.NewLoader(ctx, manager)

	log.Info("manager initialization complete")

	// todo start run
	go lg.Run()

	// todo wait for close
	<-ctx.Done()

	if err = lg.Close(); err != nil {
		log.Infof("close loader %v", err)
	}

	// todo clear
	if err = ed.Close(); err != nil {
		log.Infof("close etcd %v", err)
	}
	if err = dk.Close(); err != nil {
		log.Infof("close collector_docker client %v", err)
	}
	return nil
}
