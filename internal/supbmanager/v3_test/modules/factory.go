package modules

import (
	"bytes"
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/model"
	nervous_apis "gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/nervous-apis"
	web_apis "gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/web-apis"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"strings"
)

const (
	Webserver = "webapis"
	NervousServer = "nervousapis"
)

var Modules = map[string]func(ctx context.Context, prefix string) model.Module{
	Webserver: web_apis.NewServer,
	NervousServer: nervous_apis.NewServer,
}

type ModuleFactory struct {
	modules map[string]model.Module
}

func NewFactory(ctx context.Context, prefix string) *ModuleFactory {
	f := ModuleFactory{
		modules: make(map[string]model.Module),
	}

	for name, newModule := range Modules {
		f.modules[name] = newModule(ctx, strings.Join([]string{prefix, name}, "."))
	}

	return &f
}

func (r *ModuleFactory) InitFlags(flags *pflag.FlagSet) {
	for _, mod := range r.modules {
		mod.InitFlags(flags)
	}
}

func (r *ModuleFactory) ViperConfig(viper *viper.Viper) {
	for _, mod := range r.modules {
		mod.InitViper(viper)
	}
}

func (r *ModuleFactory) InitViper(viper *viper.Viper) {
	for _, mod := range r.modules {
		mod.InitViper(viper)
	}
}

func (r *ModuleFactory) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *ModuleFactory) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}

	for _, mod := range r.modules {
		mod.Initialize()
	}
}

func (r *ModuleFactory) Run() {
	for _, mod := range r.modules {
		go mod.Run()
	}
}

func (r *ModuleFactory) Close() error {
	errs := new(bytes.Buffer)
	for name, mod := range r.modules {
		if err := mod.Close(); err != nil {
			errs.WriteString(fmt.Sprintf("mod %v close %v", name, err))
		}
	}
	if errs.Len() > 0 {
		return fmt.Errorf("%v", errs.String())
	}
	return nil
}
