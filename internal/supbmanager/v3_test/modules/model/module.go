package model

import (
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

type Option func(mod Module)

type Module interface {
	OptionConfig(opts...Option)
	Initialize(opts...Option)
	InitFlags(flags *pflag.FlagSet)
	InitViper(flags *viper.Viper)
	Run()
	Close() error
}