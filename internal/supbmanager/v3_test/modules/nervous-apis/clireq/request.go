package clireq

const (
	StrategyRPC = "strategy"
	MonitorRPC = "monitor"

	StrategyControlInfo       = "strategy.control.info"
	LogsContainer       	  = "monitor.logs.container.latest"
	MetricsContainerNow    = "monitor.metrics.container.now"
)

type StrategyRequest struct {
	Op         string            `json:"object.op"`
	Key  string `json:"object.key"`
	Name string `json:"object.metrics.name"`
}


type MonitorRequest struct {
	Op         string            `json:"object.op"`
	Key        string            `json:"object.key"`
	Kind       string            `json:"object.kind"`
	Guid       string            `json:"object.guid"`
	StartTime  int64             `json:"object.metrics.time.start"`
	EndTime    int64             `json:"object.metrics.time.end"`
	TimeToNow  int64             `json:"object.metrics.time.duration"`
	LogsCount  int64             `json:"object.logs.count"`
	LogsFilter map[string]string `json:"object.logs.filter"`


	// 必选
	Service string `json:"object.traces.service"`

	// 可选
	Operation   string `json:"object.traces.operation"`
	Start       string `json:"object.traces.time.start"`
	End         string `json:"object.traces.time.end"`
	MaxDuration string `json:"object.traces.duration.max"`
	MinDuration string `json:"object.traces.duration.min"`
	Limit       string `json:"object.traces.limit"`
	TraceId 	string `json:"object.traces.id"`
}