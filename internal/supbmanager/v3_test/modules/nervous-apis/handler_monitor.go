package nervous_apis

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/nervous-apis/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"net/http"
	"time"
)

func (r *NervousServer) monitor(args ...interface{}) (interface{}, error) {
	var req = &clireq.MonitorRequest{}

	r.logger.Infof("STRATEGY")

	if err := json.Unmarshal([]byte(args[0].(string)), req); err != nil {

		return "", err
	}

	switch req.Op {
	case clireq.LogsContainer:
		return r.containerLogs(req)
	case clireq.MetricsContainerNow:
		return r.containerMetricsNow(req)
	default:
		return "", fmt.Errorf("unkown type")
	}
}

func (r *NervousServer) containerLogs(req *clireq.MonitorRequest)  (interface{}, error) {
	if r.supbLogClr == nil {
		return "",fmt.Errorf("manager is nil")
	}

	if err := r.checkKeyAndGuid(req.Key, req.Guid, req.Kind); err != nil {
		return "",err
	}

	data, err := r.supbLogClr.GetLogLatest(req.Guid, int(req.LogsCount))
	if err != nil {
		return "",err
	}

	return data[0], nil
}


func (r *NervousServer) containerMetricsNow(req *clireq.MonitorRequest) (string, error)  {
	if r.supbMetrics == nil {
		return "",fmt.Errorf("manager is nil")
	}

	if err := r.checkKeyAndGuid(req.Key, req.Guid, req.Kind); err != nil {
		return "",err
	}

	data, err := r.supbMetrics.GetMetricsNow(req.Guid, time.Second)
	if err != nil {
		return "",err
	}

	r.logger.Infof("%v ",data)

	rData := clireq.Response{
		Code: http.StatusOK,
		Message: "succeed",
		Data:data,
	}

	resp,err := json.Marshal(rData)
	if err != nil {
		return "", err
	}

	return string(resp), nil
}

func (r *NervousServer) checkKeyAndGuid(key, guid string, kind string) error {
	switch kind {
	case clireq.MonitorStrategy:
		st, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, key)
		if err != nil {
			return fmt.Errorf("%v", err)
		}

		match := false
		for _, v := range st.StrategyContainers {
			if v.Guid == guid {
				match = true
			}
		}
		if !match {
			return fmt.Errorf("guid don't exist")
		}
	case clireq.MonitorApplication:
		app, err := r.supbResMgr.GetApplication(types.DynamicApplication, key)
		if err != nil {
			return fmt.Errorf("%v", err)
		}

		match := false
		for _, v := range app.ApplicationContainers {
			if v.Guid == guid {
				match = true
			}
		}
		if !match {
			return fmt.Errorf("guid don't exist")
		}
	default:
		return fmt.Errorf("unkwon kind")
	}
	return nil
}