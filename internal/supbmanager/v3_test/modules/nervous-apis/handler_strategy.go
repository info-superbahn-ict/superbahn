package nervous_apis

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/nervous-apis/clireq"
	m2 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"net/http"
)

func (r *NervousServer) strategy(args ...interface{}) (interface{}, error) {
	var req = &clireq.StrategyRequest{}

	r.logger.Infof("STRATEGY")

	if err := json.Unmarshal([]byte(args[0].(string)), req); err != nil {
		return "", err
	}

	switch req.Op {
	case clireq.StrategyControlInfo:
		return r.strategyControlInfo(req.Key)
	default:
		return "", fmt.Errorf("unkown type")
	}
}

func (r *NervousServer) strategyControlInfo(strategyKey string) (string, error) {
	if r.supbStrategy == nil || r.supbResMgr == nil {
		return "", fmt.Errorf("nil stratgy or resrouce mgr")
	}

	st, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, strategyKey)
	if err != nil {
		return "", fmt.Errorf("[%v]",err)
	}

	apps, i := make([]*m2.SupbApplicationBaseInfo, len(st.StrategySlaves)), 0
	for _, slave := range st.StrategySlaves {
		app, err := r.supbResMgr.GetApplication(types.DynamicApplication, string(slave.SlaveKey))
		if err != nil {
			return "", err
		}
		apps[i] = app
		i++
	}

	rData := clireq.Response{
		Code: http.StatusOK,
		Message: "succeed",
		Data:apps,
	}

	resp,err := json.Marshal(rData)
	if err != nil {
		return "", err
	}

	return string(resp), err
}
