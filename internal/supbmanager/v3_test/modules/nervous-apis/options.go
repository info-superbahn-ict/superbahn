package nervous_apis

import (
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbindex"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supblog"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbmetric"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbstrategy"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbtrace"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
)

func WithSupbResMgr(resources *supbres.SupbResources)model.Option{
	return func(mod model.Module) {
		if md,ok := mod.(*NervousServer);ok{
			md.supbResMgr = resources
		}
	}
}

func WithSupbLogClr(logClr *supblog.SupbLog)model.Option{
	return func(mod model.Module) {
		if md,ok := mod.(*NervousServer);ok{
			md.supbLogClr = logClr
		}
	}
}

func WithSupbMetricsClr(metricsClr *supbmetric.SupbMetrics)model.Option{
	return func(mod model.Module) {
		if md,ok := mod.(*NervousServer);ok{
			md.supbMetrics = metricsClr
		}
	}
}

func WithSupbStrategyClr(strategyClr *supbstrategy.SupbStrategy)model.Option{
	return func(mod model.Module) {
		if md,ok := mod.(*NervousServer);ok{
			md.supbStrategy = strategyClr
		}
	}
}


func WithSupbIndex(index *supbindex.Index)model.Option{
	return func(mod model.Module) {
		if md,ok := mod.(*NervousServer);ok{
			md.supbIndex = index
		}
	}
}

func WithTrace(trace *supbtrace.SupbTrace)model.Option{
	return func(mod model.Module) {
		if md,ok := mod.(*NervousServer);ok{
			md.supbTrace = trace
		}
	}
}

func WithRPC(rpc nervous.Controller)model.Option{
	return func(mod model.Module) {
		if md,ok := mod.(*NervousServer);ok{
			md.rpc = rpc
		}
	}
}
