package nervous_apis

import (
	"context"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/model"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/nervous-apis/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbindex"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supblog"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbmetric"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbstrategy"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbtrace"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	tryTimes   = ".rpc.try.times"
	tryInterval = ".rpc.try.interval"
)

type NervousServer struct {
	ctx    context.Context
	prefix string

	tryTimes    int
	tryInterval      int

	logger log.Logger

	rpc  nervous.Controller
	supbResMgr *supbres.SupbResources
	supbLogClr *supblog.SupbLog
	supbMetrics  *supbmetric.SupbMetrics
	supbStrategy *supbstrategy.SupbStrategy
	supbIndex  *supbindex.Index
	supbTrace *supbtrace.SupbTrace
}

func NewServer(ctx context.Context, prefix string) model.Module {
	return &NervousServer{
		ctx:    ctx,
		prefix: prefix,
		logger: log.NewLogger(prefix),
	}
}

func (r *NervousServer) InitFlags(flags *pflag.FlagSet) {
	flags.Int(r.prefix+tryTimes, 100, "webserver route")
	flags.Int(r.prefix+tryInterval, 50, "webserver url")
}

func (r *NervousServer) ViperConfig(viper *viper.Viper) {

}

func (r *NervousServer) InitViper(viper *viper.Viper) {
	r.tryTimes = viper.GetInt(r.prefix + tryTimes)
	r.tryInterval = viper.GetInt(r.prefix + tryInterval)
}

func (r *NervousServer) OptionConfig(opts ...model.Option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *NervousServer) Initialize(opts ...model.Option) {
	for _, apply := range opts {
		apply(r)
	}

	if r.supbResMgr==nil {
		r.logger.Fatalf("supb resource manager is nil")
	}

	r.logger.Infof("%v Initialize",r.prefix)
	go r.Run()
}

func (r *NervousServer) Close() error {
	r.logger.Infof("%v closed",r.prefix)
	return nil
}


func (r *NervousServer) Run() {
	if err:= r.rpc.RPCRegister(clireq.StrategyRPC, r.strategy);err!=nil{
		r.logger.Errorf("spongeregister %v",err)
		return
	}

	if err:= r.rpc.RPCRegister(clireq.MonitorRPC, r.monitor);err!=nil{
		r.logger.Errorf("spongeregister %v",err)
		return
	}
	r.logger.Infof("start listen...")
	<-r.ctx.Done()
}

