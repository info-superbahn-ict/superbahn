package modules

import (
	nervous_apis "gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/nervous-apis"
	web_apis "gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/web-apis"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbindex"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supblog"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbmetric"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbstrategy"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbtrace"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
)

type option func(mod *ModuleFactory)

func WithSupbResMgr(resources *supbres.SupbResources) option{
	return func(mod *ModuleFactory) {
		mod.modules[Webserver].OptionConfig(web_apis.WithSupbResMgr(resources))
		mod.modules[NervousServer].OptionConfig(nervous_apis.WithSupbResMgr(resources))
	}
}


func WithSupbMetricsClr(metricsClr *supbmetric.SupbMetrics) option{
	return func(mod *ModuleFactory) {
		mod.modules[Webserver].OptionConfig(web_apis.WithSupbMetricsClr(metricsClr))
		mod.modules[NervousServer].OptionConfig(nervous_apis.WithSupbMetricsClr(metricsClr))
	}
}

func WithSupbLogsClr(logsClr *supblog.SupbLog) option{
	return func(mod *ModuleFactory) {
		mod.modules[Webserver].OptionConfig(web_apis.WithSupbLogClr(logsClr))
		mod.modules[NervousServer].OptionConfig(nervous_apis.WithSupbLogClr(logsClr))
	}
}

func WithStrategyClr(strategyClr *supbstrategy.SupbStrategy) option {
	return func(mod *ModuleFactory) {
		mod.modules[Webserver].OptionConfig(web_apis.WithSupbStrategyClr(strategyClr))
		mod.modules[NervousServer].OptionConfig(nervous_apis.WithSupbStrategyClr(strategyClr))
	}
}


func WithIndex(index *supbindex.Index) option {
	return func(mod *ModuleFactory) {
		mod.modules[Webserver].OptionConfig(web_apis.WithSupbIndex(index))
		mod.modules[NervousServer].OptionConfig(nervous_apis.WithSupbIndex(index))
	}
}

func WithTrace(trace *supbtrace.SupbTrace) option {
	return func(mod *ModuleFactory) {
		mod.modules[Webserver].OptionConfig(web_apis.WithTrace(trace))
		mod.modules[NervousServer].OptionConfig(nervous_apis.WithTrace(trace))
	}
}

func WithRPC(rpc nervous.Controller)option{
	return func(mod *ModuleFactory) {
		mod.modules[NervousServer].OptionConfig(nervous_apis.WithRPC(rpc))
	}
}