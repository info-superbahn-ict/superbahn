package supb_etcd

import (
	"context"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbrpc"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	rpcPrefix   = ".rpc.func.prefix"
	handleTimeOut = ".handle.timeOut"
)

type SupbEtcd struct {
	ctx    context.Context
	prefix string

	rpcPrefix   string
	rpcGuid      string
	handleTimeOut int64

	logger log.Logger
	//rpc
	supbRpcMgr *supbrpc.SupbRpc
	supbResMgr *supbres.SupbResources
}

func NewSupbEtcd(ctx context.Context, prefix string) model.Module {
	return &SupbEtcd{
		ctx:    ctx,
		prefix: prefix,
		logger: log.NewLogger(prefix),
	}
}

func (r *SupbEtcd) InitFlags(flags *pflag.FlagSet) {
	flags.String(r.prefix+rpcPrefix, "supb.manager.rpc.etcd", "webserver route")
	flags.Int64(r.prefix+handleTimeOut, 5, "webserver url")
}

func (r *SupbEtcd) InitViper(viper *viper.Viper) {
	r.rpcPrefix = viper.GetString(r.prefix + rpcPrefix)
	r.handleTimeOut = viper.GetInt64(r.prefix + handleTimeOut)
}

func (r *SupbEtcd) OptionConfig(opts ...model.Option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *SupbEtcd) Initialize(opts ...model.Option) {
	for _, apply := range opts {
		apply(r)
	}

	if r.supbResMgr==nil {
		r.logger.Fatalf("supb resource manager is nil")
	}

	if r.supbRpcMgr==nil {
		r.logger.Fatalf("supb rpc manager is nil")
	}

	r.logger.Infof("%v Initialize",r.prefix)
}

func (r *SupbEtcd) Close() error {
	r.logger.Infof("%v closed",r.prefix)
	return nil
}

func (r *SupbEtcd) Run() {
	if err := r.supbRpcMgr.RPC().RPCRegister(r.prefix+".put",r.put);err !=nil{
		r.logger.Errorf("register etcd put %v",err)
		return
	}
	//if err := r.supbRpcMgr.RPC().RPCRegister(r.prefix+".get",r.get);err !=nil{
	//	r.logger.Errorf("register etcd get %v",err)
	//	return
	//}
	//if err := r.supbRpcMgr.RPC().RPCRegister(r.prefix+".put",r.put);err !=nil{
	//	r.logger.Errorf("register etcd put %v",err)
	//	return
	//}
	//if err := r.supbRpcMgr.RPC().RPCRegister(r.prefix+".put",r.put);err !=nil{
	//	r.logger.Errorf("register etcd put %v",err)
	//	return
	//}
}



