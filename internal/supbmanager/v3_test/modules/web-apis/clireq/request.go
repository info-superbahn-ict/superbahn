package clireq

const (
	// 基础策略
	ListBaseStrategies = "list.base.strategies"

	// todo 策略

	// list
	ListStaticStrategies  = "list.static.strategies"
	ListDynamicStrategies = "list.dynamic.strategies"

	// list filter
	ListStaticStrategiesWithFilter  = "list.static.strategies.filter"
	ListDynamicStrategiesWithFilter = "list.dynamic.strategies.filter"
	ListStrategiesMetricsList       = "list.dynamic.strategies.metrics.now"

	// get
	GetStaticStrategy  = "get.static.strategy"
	GetDynamicStrategy = "get.dynamic.strategy"

	GetStaticStrategyLinks      = "get.statics.strategy.links"
	GetDynamicStrategyLinks     = "get.dynamic.strategy.links"
	GetStaticStrategyState      = "get.static.strategy.status"
	GetDynamicStrategyState     = "get.dynamic.strategy.status"
	GetStrategyLevels           = "get.strategy.levels"
	GetStrategyStatus           = "get.strategy.status"
	GetStrategyMetricsNow       = "get.dynamic.strategy.metrics.now"
	GetStrategyMetricsThreshold = "get.dynamic.strategy.metrics.threshold"
	GetStrategyStatistic        = "get.dynamic.strategy.statistic.number"
	GetStrategyStatisticPercent = "get.dynamic.strategy.statistic.percentage"
	//GetStrategyContainers       = "get.dynamic.strategy.containers"
	//GetStrategyApplications     = "get.dynamic.strategy.applications"
	GetStrategyDetailInfo = "get.dynamic.strategy.detail.info"

	// run
	RunStaticStrategy = "run.static.strategy"

	// stop
	StopDynamicStrategy = "stop.dynamic.strategy"

	// restart
	RestartDynamicStrategy = "restart.dynamic.strategy"

	// remove
	RemoveStaticStrategy  = "remove.static.strategy"
	RemoveDynamicStrategy = "remove.dynamic.strategy"

	// update
	UpdateStaticStrategy  = "update.static.strategy"
	UpdateDynamicStrategy = "update.dynamic.strategy"
	AddStrategy           = "update.add.strategy"

	// todo 应用

	// list
	ListStaticApplications  = "list.static.applications"
	ListDynamicApplications = "list.dynamic.applications"

	// list filter
	ListStaticApplicationsWithFilter  = "list.static.applications.filter"
	ListDynamicApplicationsWithFilter = "list.dynamic.applications.filter"
	ListApplicationsMetricsList       = "list.dynamic.applications.metrics.now"

	// get
	GetStaticApplication  = "get.static.application"
	GetDynamicApplication = "get.dynamic.application"

	GetStaticApplicationLinks             = "get.statics.application.links"
	GetDynamicApplicationLinks            = "get.dynamic.application.links"
	GetStaticApplicationStatus            = "get.static.application.status"
	GetDynamicApplicationStatus           = "get.dynamic.application.status"
	GetApplicationLevels                  = "get.application.levels"
	GetApplicationStatus                  = "get.application.status"
	GetApplicationMetricsNow              = "get.dynamic.application.metrics.now"
	GetApplicationMetricsThreshold        = "get.dynamic.application.metrics.threshold"
	GetApplicationStatisticDataType       = "get.dynamic.application.statistic.data.list"
	GetApplicationStatisticData           = "get.dynamic.application.statistic.data"
	GetApplicationStatisticDataTypeFilter = "get.dynamic.application.statistic.data.list.filter"
	//GetApplicationDevices           = "get.dynamic.application.devices"
	//GetApplicationDeviceInfo        = "get.dynamic.application.device.info"
	GetApplicationDetailInfo = "get.dynamic.application.detail.info"

	// run
	RunStaticApplication = "run.static.application"

	// stop
	StopDynamicApplication = "stop.dynamic.application"

	// restart
	RestartDynamicApplication = "restart.dynamic.application"

	// remove
	RemoveStaticApplication  = "remove.static.application"
	RemoveDynamicApplication = "remove.dynamic.application"

	// update
	UpdateStaticApplication  = "update.static.application"
	UpdateDynamicApplication = "update.dynamic.application"
	AddApplication           = "update.add.application"

	// todo link
	LinkStrategiesAndApplications = "link.and.unlink.multi"

	// todo log
	MetricsMonitorSwitch = "monitor.metrics.switch.status"
	LogsMonitorSwitch    = "monitor.logs.switch.status"

	MetricsContainerNow    = "monitor.metrics.container.now"
	MetricsContainerCPU    = "monitor.metrics.container.series.cpu"
	MetricsContainerMemory = "monitor.metrics.container.series.memory"
	MetricsContainerNetRx  = "monitor.metrics.container.series.net.rx"
	MetricsContainerNetTx  = "monitor.metrics.container.series.net.tx"
	MetricsContainerAll    = "monitor.metrics.container.series.all"
	MetricsAdd             = "monitor.metrics.container.add"
	MetricsDelete          = "monitor.metrics.container.delete"

	LogsContainer       = "monitor.logs.container.latest"
	LogsContainerFilter = "monitor.logs.container.filter"
	LogsAdd             = "monitor.logs.container.add"
	LogsDelete          = "monitor.logs.container.delete"
	LogsLevel           = "monitor.logs.level"
	LogsStatistic       = "monitor.logs.statistic"

	TracesGetDag		= "monitor.traces.dag"
	TracesGetStatistic		= "monitor.traces.statistic"
	TracesGetList		= "monitor.traces.select"
	TracesGetDataLatest		= "monitor.traces.select.latest"
	TracesGetData		= "monitor.traces.select.id"
	TracesGetServices		= "monitor.traces.select.service"
	TracesGetOperation		= "monitor.traces.select.operation"

	StrategyDataTypes = "strategy.data.types"
	StrategyData      = "strategy.data"
	StrategyUrl       = "strategy.url"
	StrategyControlInfo       = "strategy.control.info"

	ApplicationTypes = "application.data.types"
	ApplicationData  = "application.data"
	ApplicationUrl   = "application.url"

	Relationship             = "relationship.all"
	RelationshipVisitable    = "relationship.visitable"
	RelationshipSub          = "relationship.subgraph"
	RelationshipDetail       = "relationship.detail"
	RelationshipFilter       = "relationship.filter"
	RelationshipFilterKind   = "relationship.filter.kinds"
	RelationshipFilterStatus = "relationship.filter.status"

	IndexSwitchOn     = "index.switch.on"
	IndexSwitchOff    = "index.switch.off"
	IndexSwitchStatus = "index.switch.status"
	IndexListGet      = "index.list.show"
	IndexListSave     = "index.list.save"
	IndexGraphAdd     = "index.graph.add"
	IndexGraphDelete  = "index.graph.delete"
)

type ListRequest struct {
	Op          string            `json:"object.op"`
	Filters     map[string]string `json:"object.filter"`
	MetricsKeys []string          `json:"object.keys"`
}

type GetRequest struct {
	Op            string `json:"object.op"`
	Key           string `json:"object.key"`
	Name          string `json:"object.metrics.name"`
	Guid          string `json:"object.devices.guid"`
	MetricsFilter string `json:"object.metrics.filter"`
}

type RunRequest struct {
	Op   string `json:"object.op"`
	Key  string `json:"object.key"`
	Name string `json:"object.name"`
	//ContainerName string `json:"object.container.name.prefix"`
	LinksKey map[string]string `json:"object.links"`
}

type StopRequest struct {
	Op  string `json:"object.op"`
	Key string `json:"object.key"`
}

type RestartRequest struct {
	Op  string `json:"object.op"`
	Key string `json:"object.key"`
}

type RemoveRequest struct {
	Op  string `json:"object.op"`
	Key string `json:"object.key"`
}

type UpdateRequest struct {
	Op   string      `json:"object.op"`
	Data interface{} `json:"object.data"`
}

type LinkRequest struct {
	Op              string `json:"object.op"`
	StrategyKey     string `json:"object.key.strategy"`
	StrategyType    string `json:"object.type.strategy"`
	ApplicationKey  string `json:"object.key.application"`
	ApplicationType string `json:"object.type.application"`
}

type MultiLinkRequest struct {
	Op         string            `json:"object.op"`
	Key        string            `json:"object.key"`
	Type       string            `json:"object.type"`
	Kind       string            `json:"object.kind"`
	LinksKey   map[string]string `json:"object.links"`
	UnLinksKey map[string]string `json:"object.unlinks"`
}

type MonitorRequest struct {
	Op         string            `json:"object.op"`
	Key        string            `json:"object.key"`
	Kind       string            `json:"object.kind"`
	Guid       string            `json:"object.guid"`
	StartTime  int64             `json:"object.metrics.time.start"`
	EndTime    int64             `json:"object.metrics.time.end"`
	TimeToNow  int64             `json:"object.metrics.time.duration"`
	LogsCount  int64             `json:"object.logs.count"`
	LogsFilter map[string]string `json:"object.logs.filter"`


	// 必选
	Service string `json:"object.traces.service"`

	// 可选
	Operation   string `json:"object.traces.operation"`
	Start       string `json:"object.traces.time.start"`
	End         string `json:"object.traces.time.end"`
	MaxDuration string `json:"object.traces.duration.max"`
	MinDuration string `json:"object.traces.duration.min"`
	Limit       string `json:"object.traces.limit"`
	TraceId 	string `json:"object.traces.id"`
}

type StrategyRequest struct {
	Op   string `json:"object.op"`
	Key  string `json:"object.key"`
	Name string `json:"object.metrics.name"`
}

type ApplicationRequest struct {
	Op   string `json:"object.op"`
	Key  string `json:"object.key"`
	Name string `json:"object.metrics.name"`
}

type RelationshipRequest struct {
	Op     string            `json:"object.op"`
	Key    string            `json:"object.key"`
	Kind   string            `json:"object.kind"`
	Filter map[string]string `json:"object.filter"`
}

type IndexRequest struct {
	Op   string           `json:"object.op"`
	Page string           `json:"object.page"`
	Key  string           `json:"object.key"`
	Name string           `json:"object.name"`
	Mod  string           `json:"object.mod"`
	Type string           `json:"object.type"`
	Kind string				`json:"object.kind"`
	List []int `json:"object.list.ids"`
}
