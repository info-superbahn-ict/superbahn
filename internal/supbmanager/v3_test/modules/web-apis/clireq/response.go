package clireq

import (
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
)

const (
	HttpSucceed = "succeed"

	FilterKeyName   = "name"
	FilterKeyStatus = "status"
	FilterKeyLevel  = "level"

	MultiLinkStrategy    = "strategy"
	MultiLinkApplication = "application"

	LogFilterType = "type"
	LogFilterContext = "context"

	MonitorStrategy = "strategy"
	MonitorApplication = "application"

	SwitchOn = "on"
	SwitchOff = "off"

	MetricsKindCPU = "cpu"
	MetricsKindMemory = "memory"
	MetricsKindNetRx = "netRx"
	MetricsKindNetTx = "netTx"
	MetricsKindAll = "all"


	RelationshipCategoryStrategyConfig = "StrategyConfig"
	RelationshipCategoryStrategyInstance = "StrategyInstance"
	RelationshipCategoryStrategyContainers = "StrategyContainers"
	RelationshipCategoryApplicationConfig = "ApplicationConfig"
	RelationshipCategoryApplicationInstance = "ApplicationInstance"
	RelationshipCategoryApplicationDevices = "ApplicationDevices"

)

var RelationShipType = map[int]string{
	0:RelationshipCategoryStrategyConfig,
	1:RelationshipCategoryStrategyInstance,
	2:RelationshipCategoryStrategyContainers,
	3:RelationshipCategoryApplicationConfig,
	4:RelationshipCategoryApplicationInstance,
	5:RelationshipCategoryApplicationDevices,
}

var RelationShipTypeT = map[string]int{
	RelationshipCategoryStrategyConfig:0,
	RelationshipCategoryStrategyInstance:1,
	RelationshipCategoryStrategyContainers:2,
	RelationshipCategoryApplicationConfig:3,
	RelationshipCategoryApplicationInstance:4,
	RelationshipCategoryApplicationDevices:5,
}

type Response struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type RunTimeResourceUsage struct {
	CPUUsage    float64 `json:"cpu.usage"`
	CPUTotal    float64 `json:"cpu.total"`
	MemoryUsage float64 `json:"mem.usage"`
	MemoryTotal float64 `json:"mem.total"`
	NetTx       float64 `json:"net.receive"`
	NetRx       float64 `json:"net.transport"`
}


type StrategyLinks struct {
	Linked map[types.SupbApplicationKey]*StrategyLink `json:"strategy_linked_app"`
	UnLink map[types.SupbApplicationKey]*StrategyLink `json:"strategy_unlink_app"`
}

type ApplicationLinks struct {
	Linked map[types.SupbStrategyKey]*ApplicationLink `json:"application_linked_strategy"`
	UnLink map[types.SupbStrategyKey]*ApplicationLink `json:"application_unlink_strategy"`
}

type StrategyLink struct {
	Name   string `json:"strategy_link_name"`
	Key    string `json:"strategy_link_key"`
	Type   string `json:"strategy_link_type"`
	Status string `json:"strategy_link_status"`
	Level  string `json:"strategy_link_level"`
}

type ApplicationLink struct {
	Name   string `json:"application_link_name"`
	Key    string `json:"application_link_key"`
	Type   string `json:"application_link_type"`
	Status string `json:"application_link_status"`
	Level  string `json:"application_link_level"`
}

type MultiLinkResponse struct {
	LinksFailed    map[string]string `json:"object.links.failed"`
	LinksSucceed   map[string]string `json:"object.links.succeed"`
	UnLinksFailed  map[string]string `json:"object.unlinks.failed"`
	UnLinksSucceed map[string]string `json:"object.unlinks.succeed"`
}

type RelationShipNode struct {
	Key    string `json:"key"`
	Name   string `json:"name"`
	Value int `json:"value"`
	Category  int `json:"category"`
	Visitable int `json:"visitable"`
}

type RelationShipLink struct {
	Source    int `json:"source"`
	Target   int `json:"target"`
	Visitable int `json:"visitable"`
}

type DAGNode struct {
	Name string
	Size float64
}

type DAGLink struct {
	Source int
	Target int
	Label string
	LabelSize float64
}