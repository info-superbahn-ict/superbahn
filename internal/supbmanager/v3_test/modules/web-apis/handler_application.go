package web_apis

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/web-apis/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"io/ioutil"
	"net/http"
)

func (r *WebServer) application(w http.ResponseWriter, rq *http.Request) {
	var req, resp = &clireq.ApplicationRequest{}, &clireq.Response{}

	r.logger.Infof("STRATEGY")

	if buffers, err := ioutil.ReadAll(rq.Body); err != nil {
		r.responseErr(w, "read body %v", err)
		return
	} else {
		if err = json.Unmarshal(buffers, req); err != nil {
			r.responseErr(w, "unmarshal %v", err)
			return
		}
	}
	switch req.Op {
	case clireq.ApplicationTypes:
		resp = r.applicationDataType(req.Key)
	case clireq.ApplicationData:
		resp = r.applicationData(req.Key,req.Name)
	case clireq.ApplicationUrl:
		resp = r.applicationUrl(req.Key)
	default:
		resp = &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown type"),
			Data:    nil,
		}
	}

	bts, _ := json.Marshal(resp)
	if bt, err := w.Write(bts); err != nil {
		r.logger.Errorf("get function %v", err)
	} else {
		r.logger.Infof(fmt.Sprintf("STRATEGY REPONSE (%vB)", bt))
	}
}

func (r *WebServer) applicationDataType(appKey string) *clireq.Response {
	if r.supbStrategy ==nil || r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	_ ,err := r.supbResMgr.GetApplication(types.DynamicApplication,appKey)
	if err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	info,err := r.supbStrategy.GetCommonInfo()
	if err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}


	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    info,
	}
}

func (r *WebServer) applicationData(appKey, metricsName string) *clireq.Response {
	if r.supbStrategy ==nil || r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	_ ,err := r.supbResMgr.GetApplication(types.DynamicApplication,appKey)
	if err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	info,err := r.supbStrategy.GetCommonData(metricsName)
	if err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}


	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    info,
	}
}


func (r *WebServer) applicationUrl(appKey string) *clireq.Response {
	if r.supbStrategy ==nil || r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	app ,err := r.supbResMgr.GetApplication(types.DynamicApplication,appKey)
	if err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	url:="http://39.105.146.89:8080/pages/car.html"

	for _,v := range app.ApplicationTags {
		if v.Key == "URL"{
			url = v.Value
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    url,
	}
}
