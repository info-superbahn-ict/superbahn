package web_apis

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/web-apis/clireq"
	m2 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/status"
	m1 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

func (r *WebServer) get(w http.ResponseWriter, rq *http.Request) {
	var req, resp = &clireq.GetRequest{}, &clireq.Response{}

	r.logger.Infof("GET")

	if buffers, err := ioutil.ReadAll(rq.Body); err != nil {
		r.responseErr(w, "read body %v", err)
		return
	} else {
		if err = json.Unmarshal(buffers, req); err != nil {
			r.responseErr(w, "unmarshal %v", err)
			return
		}
	}

	switch req.Op {
	// todo get 策略
	case clireq.GetStaticStrategy:
		resp = r.getStrategy(types.StaticStrategy, req.Key)
	case clireq.GetDynamicStrategy:
		resp = r.getStrategy(types.DynamicStrategy, req.Key)

	// todo get 应用
	case clireq.GetStaticApplication:
		resp = r.getApplication(types.StaticApplication, req.Key)
	case clireq.GetDynamicApplication:
		resp = r.getApplication(types.DynamicApplication, req.Key)

	// todo get 策略 实时资源
	case clireq.GetStrategyMetricsNow:
		resp = r.getDynamicStrategyMetricsNow(req.Key)
	case clireq.GetStrategyMetricsThreshold:
		resp = r.getDynamicStrategyMetricsThreshold()

	// todo get 应用 实时资源
	case clireq.GetApplicationMetricsNow:
		resp = r.getDynamicApplicationMetricsNow(req.Key)
	case clireq.GetApplicationMetricsThreshold:
		resp = r.getDynamicApplicationMetricsThreshold()

	// todo get 策略 连接
	case clireq.GetStaticStrategyLinks:
		resp = r.getStrategyLinks(types.StaticStrategy, req.Key)
	case clireq.GetDynamicStrategyLinks:
		resp = r.getStrategyLinks(types.DynamicStrategy, req.Key)

	// todo get 应用 连接
	case clireq.GetStaticApplicationLinks:
		resp = r.getApplicationLinks(types.StaticStrategy, req.Key)
	case clireq.GetDynamicApplicationLinks:
		resp = r.getApplicationLinks(types.DynamicApplication, req.Key)

	// todo get 策略 状态
	case clireq.GetStaticStrategyState:
		resp = r.getStrategyStatue(types.StaticStrategy, req.Key)
	case clireq.GetDynamicStrategyState:
		resp = r.getStrategyStatue(types.DynamicStrategy, req.Key)

	// todo get 应用 状态
	case clireq.GetStaticApplicationStatus:
		resp = r.getApplicationStatue(types.StaticStrategy, req.Key)
	case clireq.GetDynamicApplicationStatus:
		resp = r.getApplicationStatue(types.DynamicStrategy, req.Key)

	// todo get 策略 等级 集合
	case clireq.GetStrategyLevels:
		resp = r.getLevelsOfStrategy()

	// todo get 应用 等级 集合
	case clireq.GetApplicationLevels:
		resp = r.getLevelsOfApplication()

	// todo get 策略 状态 集合
	case clireq.GetStrategyStatus:
		resp = r.getStatusOfStrategy()

	// todo get 应用 状态 集合
	case clireq.GetApplicationStatus:
		resp = r.getStatusOfApplication()

	// todo get 策略 统计 信息
	case clireq.GetStrategyStatistic:
		resp = r.getStrategyStatistic(req.Key)
	case clireq.GetStrategyStatisticPercent:
		resp = r.getStrategyStatisticPercent(req.Key)
	//case clireq.GetStrategyContainers:
	//	resp = r.getStrategyContainers(req.Key)
	//case clireq.GetStrategyApplications:
	//	resp = r.getStrategyApplications(req.Key)
	case clireq.GetStrategyDetailInfo:
		resp = r.getStrategyDetailInfo(req.Key)

	case clireq.GetApplicationStatisticDataType:
		resp = r.getApplicationStatisticDataType(req.Key)
	case clireq.GetApplicationStatisticData:
		resp = r.getApplicationStatisticData(req.Key,req.Name)
	case clireq.GetApplicationStatisticDataTypeFilter:
		resp = r.getApplicationStatisticDataTypeFilter(req.Key,req.MetricsFilter)
	//case clireq.GetApplicationDevices:
	//	resp = r.getApplicationDevices(req.Key)
	//case clireq.GetApplicationDeviceInfo:
	//	resp = r.getApplicationDevicesInfo(req.Key,req.Guid)
	case clireq.GetApplicationDetailInfo:
		resp = r.getApplicationDetailInfo(req.Key)
	default:
		resp = &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown type"),
			Data:    nil,
		}
	}

	bts, _ := json.Marshal(resp)

	if bt, err := w.Write(bts); err != nil {
		r.logger.Errorf("get function %v", err)
	} else {
		r.logger.Infof(fmt.Sprintf("GET REPONSE (%vB)", bt))
	}

}

func (r *WebServer) getStrategy(types string, strategyKey string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	st, err := r.supbResMgr.GetStrategy(types, strategyKey)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    st,
	}
}

func (r *WebServer) getApplication(types string, appKey string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	app, err := r.supbResMgr.GetApplication(types, appKey)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    app,
	}
}

func (r *WebServer) getDynamicStrategyMetricsNow(stKey string) *clireq.Response {
	if r.supbResMgr == nil || r.supbMetrics == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager or metrics is nil",
			Data:    nil,
		}
	}

	st,err :=r.supbResMgr.GetStrategy(types.DynamicStrategy,stKey)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: fmt.Sprintf("%v",err),
			Data:    nil,
		}
	}

	runTime := &clireq.RunTimeResourceUsage{}

	var cpuUsed float64 = 0
	var memUsed float64 = 0
	for _,ct := range st.StrategyContainers {
		mc,err  := r.supbMetrics.GetMetricsNow(ct.Guid,time.Second)
		if err !=nil {
			return &clireq.Response{
				Code:    http.StatusInternalServerError,
				Message: fmt.Sprintf("%v",err),
				Data:    nil,
			}
		}
		runTime.CPUTotal += mc.CpuTotal
		cpuUsed += mc.CpuTotal * mc.CPUPercentage
		runTime.MemoryTotal += mc.MemoryTotal
		memUsed += mc.MemoryTotal * mc.MemoryPercentage
		runTime.NetTx += mc.NetworkTx
		runTime.NetRx += mc.NetworkRx
	}

	runTime.CPUUsage = cpuUsed / runTime.CPUTotal
	runTime.MemoryUsage = memUsed / runTime.MemoryTotal


	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    runTime,
	}
}

func (r *WebServer) getDynamicApplicationMetricsNow(stKey string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	app,err :=r.supbResMgr.GetApplication(types.DynamicApplication,stKey)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: fmt.Sprintf("%v",err),
			Data:    nil,
		}
	}

	runTime := &clireq.RunTimeResourceUsage{}

	var cpuUsed float64 = 0
	var memUsed float64 = 0
	for _,ct := range app.ApplicationContainers {
		mc,err  := r.supbMetrics.GetMetricsNow(ct.Guid,time.Second)
		if err !=nil {
			return &clireq.Response{
				Code:    http.StatusInternalServerError,
				Message: fmt.Sprintf("%v",err),
				Data:    nil,
			}
		}
		runTime.CPUTotal += mc.CpuTotal
		cpuUsed += mc.CpuTotal * mc.CPUPercentage
		runTime.MemoryTotal += mc.MemoryTotal
		memUsed += mc.MemoryTotal * mc.MemoryPercentage
		runTime.NetTx += mc.NetworkTx
		runTime.NetRx += mc.NetworkRx
	}

	runTime.CPUUsage = cpuUsed / runTime.CPUTotal
	runTime.MemoryUsage = memUsed / runTime.MemoryTotal

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    runTime,
	}
}

func (r *WebServer) getStrategyLinks(stTypes, stKey string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	st, err := r.supbResMgr.GetStrategy(stTypes, stKey)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	appS, err := r.supbResMgr.ListApplications(types.StaticApplication)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	appD, err := r.supbResMgr.ListApplications(types.DynamicApplication)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	links := &clireq.StrategyLinks{}

	links.Linked = make(map[types.SupbApplicationKey]*clireq.StrategyLink, len(st.StrategySlaves))
	for k, _ := range st.StrategySlaves {
		links.Linked[k] = &clireq.StrategyLink{}
	}

	links.UnLink = make(map[types.SupbApplicationKey]*clireq.StrategyLink, len(appS)+len(appD)-len(st.StrategySlaves))

	for _, v := range appS {
		if _, ok := links.Linked[v.ApplicationKey]; ok {
			links.Linked[v.ApplicationKey].Name = v.ApplicationName
			links.Linked[v.ApplicationKey].Key = string(v.ApplicationKey)
			links.Linked[v.ApplicationKey].Type = v.ApplicationType
			links.Linked[v.ApplicationKey].Status = v.ApplicationStatus
			links.Linked[v.ApplicationKey].Level = v.ApplicationLevel
		} else {
			links.UnLink[v.ApplicationKey] = &clireq.StrategyLink{
				Name:   v.ApplicationName,
				Type:   v.ApplicationType,
				Key:    string(v.ApplicationKey),
				Status: v.ApplicationStatus,
				Level:  v.ApplicationLevel,
			}
		}
	}

	for _, v := range appD {
		if _, ok := links.Linked[v.ApplicationKey]; ok {
			links.Linked[v.ApplicationKey].Name = v.ApplicationName
			links.Linked[v.ApplicationKey].Key = string(v.ApplicationKey)
			links.Linked[v.ApplicationKey].Type = v.ApplicationType
			links.Linked[v.ApplicationKey].Status = v.ApplicationStatus
			links.Linked[v.ApplicationKey].Level = v.ApplicationLevel
		} else {
			links.UnLink[v.ApplicationKey] = &clireq.StrategyLink{
				Name:   v.ApplicationName,
				Type:   v.ApplicationType,
				Key:    string(v.ApplicationKey),
				Status: v.ApplicationStatus,
				Level:  v.ApplicationLevel,
			}
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    links,
	}
}

func (r *WebServer) getApplicationLinks(appTypes, appKey string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	app, err := r.supbResMgr.GetApplication(appTypes, appKey)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	sts, err := r.supbResMgr.ListStrategies(types.StaticStrategy)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	std, err := r.supbResMgr.ListStrategies(types.DynamicStrategy)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	links, size := &clireq.ApplicationLinks{}, len(app.ApplicationStaticStrategies)+len(app.ApplicationDynamicStrategies)

	links.Linked = make(map[types.SupbStrategyKey]*clireq.ApplicationLink, size)
	for k, _ := range app.ApplicationStaticStrategies {
		links.Linked[k] = &clireq.ApplicationLink{}
	}
	for k, _ := range app.ApplicationDynamicStrategies {
		links.Linked[k] = &clireq.ApplicationLink{}
	}

	links.UnLink = make(map[types.SupbStrategyKey]*clireq.ApplicationLink, len(sts)+len(std)-size)

	for _, v := range sts {
		if _, ok := links.Linked[v.StrategyKey]; ok {
			links.Linked[v.StrategyKey].Name = v.StrategyName
			links.Linked[v.StrategyKey].Key = string(v.StrategyKey)
			links.Linked[v.StrategyKey].Type = v.StrategyType
			links.Linked[v.StrategyKey].Status = v.StrategyStatus
			links.Linked[v.StrategyKey].Level = string(v.StrategyLevel)
		} else {
			links.UnLink[v.StrategyKey] = &clireq.ApplicationLink{
				Name:   v.StrategyName,
				Type:   v.StrategyType,
				Key:    string(v.StrategyKey),
				Status: v.StrategyStatus,
				Level:  string(v.StrategyLevel),
			}
		}
	}

	for _, v := range std {
		if _, ok := links.Linked[v.StrategyKey]; ok {
			links.Linked[v.StrategyKey].Name = v.StrategyName
			links.Linked[v.StrategyKey].Key = string(v.StrategyKey)
			links.Linked[v.StrategyKey].Type = v.StrategyType
			links.Linked[v.StrategyKey].Status = v.StrategyStatus
			links.Linked[v.StrategyKey].Level = string(v.StrategyLevel)
		} else {
			links.UnLink[v.StrategyKey] = &clireq.ApplicationLink{
				Name:   v.StrategyName,
				Type:   v.StrategyType,
				Key:    string(v.StrategyKey),
				Status: v.StrategyStatus,
				Level:  string(v.StrategyLevel),
			}
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    links,
	}
}

func (r *WebServer) getStrategyStatue(types string, strategyKey string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	st, err := r.supbResMgr.GetStrategy(types, strategyKey)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    st.StrategyStatus,
	}
}

func (r *WebServer) getApplicationStatue(types string, appKey string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	app, err := r.supbResMgr.GetApplication(types, appKey)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    app.ApplicationStatus,
	}
}

func (r *WebServer) getLevelsOfStrategy() *clireq.Response {
	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    m1.Levels(),
	}
}

func (r *WebServer) getLevelsOfApplication() *clireq.Response {
	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    m2.Levels(),
	}
}

func (r *WebServer) getStatusOfStrategy() *clireq.Response {
	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    status.DynamicStatus(),
	}
}

func (r *WebServer) getStatusOfApplication() *clireq.Response {
	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    status.DynamicStatus(),
	}
}

func (r *WebServer) getStrategyStatistic(strategyKey string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}
	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data: map[string]int{
			"devices":      5,
			"container":    30,
			"netInterface": 4,
			"server":       1,
			"iot-car":      3,
		},
	}
}

func (r *WebServer) getStrategyStatisticPercent(strategyKey string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}
	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data: map[string]float64{
			"devices":      5.0 / 43,
			"container":    30.0 / 43,
			"netInterface": 4.0 / 43,
			"server":       1.0 / 43,
			"iot-car":      3.0 / 43,
		},
	}
}

//func (r *WebServer) getStrategyContainers(strategyKey string) *clireq.Response {
//	if r.supbResMgr == nil {
//		return &clireq.Response{
//			Code:    http.StatusInternalServerError,
//			Message: "resource manager is nil",
//			Data:    nil,
//		}
//	}
//
//	r.logger.Infof("%v", strategyKey)
//	st, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, strategyKey)
//	if err != nil {
//		return &clireq.Response{
//			Code:    http.StatusBadRequest,
//			Message: fmt.Sprintf("%v", err),
//			Data:    nil,
//		}
//	}
//
//	return &clireq.Response{
//		Code:    http.StatusOK,
//		Message: clireq.HttpSucceed,
//		Data:    st.StrategyContainers,
//	}
//}
//
//func (r *WebServer) getStrategyApplications(strategyKey string) *clireq.Response {
//	if r.supbResMgr == nil {
//		return &clireq.Response{
//			Code:    http.StatusInternalServerError,
//			Message: "resource manager is nil",
//			Data:    nil,
//		}
//	}
//
//	r.logger.Infof("%v", strategyKey)
//	st, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, strategyKey)
//	if err != nil {
//		return &clireq.Response{
//			Code:    http.StatusBadRequest,
//			Message: fmt.Sprintf("%v", err),
//			Data:    nil,
//		}
//	}
//
//	apps := make([]*m2.SupbApplicationBaseInfo, len(st.StrategySlaves))
//	index := 0
//	for _, app := range st.StrategySlaves {
//		if a, err := r.supbResMgr.GetApplication(app.SlaveTypes, string(app.SlaveKey)); err != nil {
//			return &clireq.Response{
//				Code:    http.StatusInternalServerError,
//				Message: fmt.Sprintf("%v", err),
//				Data:    st.StrategySlaves,
//			}
//		} else {
//			apps[index], index = a, index+1
//		}
//	}
//
//	return &clireq.Response{
//		Code:    http.StatusOK,
//		Message: clireq.HttpSucceed,
//		Data:    apps,
//	}
//}

func (r *WebServer) getStrategyDetailInfo(strategyKey string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	r.logger.Infof("%v", strategyKey)
	st, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, strategyKey)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	apps := make([]*m2.SupbApplicationBaseInfo, len(st.StrategySlaves))
	index := 0
	for _, app := range st.StrategySlaves {
		if a, err := r.supbResMgr.GetApplication(app.SlaveTypes, string(app.SlaveKey)); err != nil {
			return &clireq.Response{
				Code:    http.StatusInternalServerError,
				Message: fmt.Sprintf("%v", err),
				Data:    st.StrategySlaves,
			}
		} else {
			apps[index], index = a, index+1
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    apps,
	}
}

func (r *WebServer) getDynamicStrategyMetricsThreshold() *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	set1 := map[string]float64{
		"cpu": 60.0,
		"memory": 60.0,
		"networkTx": 200,
		"networkRx": 400,
	}


	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    set1,
	}
}


func (r *WebServer) getDynamicApplicationMetricsThreshold() *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	set1 := map[string]float64{
		"cpu": 60.0,
		"memory": 60.0,
		"networkTx": 200,
		"networkRx": 400,
	}


	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    set1,
	}
}


func (r *WebServer) getApplicationStatisticDataType(appKey string) *clireq.Response {
	if r.supbStrategy ==nil || r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	_ ,err := r.supbResMgr.GetApplication(types.DynamicApplication,appKey)
	if err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	info,err := r.supbStrategy.GetCommonInfo()
	if err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}


	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    info,
	}
}


func (r *WebServer) getApplicationStatisticData(appKey,name string) *clireq.Response {
	if r.supbStrategy ==nil || r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	_ ,err := r.supbResMgr.GetApplication(types.DynamicApplication,appKey)
	if err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	info,err := r.supbStrategy.GetCommonData(name)
	if err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}


	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    info,
	}
}


func (r *WebServer) getApplicationStatisticDataTypeFilter(appKey,filterName string) *clireq.Response {
	if r.supbStrategy ==nil || r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	_ ,err := r.supbResMgr.GetApplication(types.DynamicApplication,appKey)
	if err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	info,err := r.supbStrategy.GetCommonInfo()
	if err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	res := make(map[string]string)
	for k,v := range info {
		if strings.Contains(k,filterName){
			res[k]=v
		}
	}


	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    res,
	}
}


//func (r *WebServer) getApplicationDevices(appKey string) *clireq.Response {
//	if r.supbStrategy ==nil || r.supbResMgr == nil {
//		return &clireq.Response{
//			Code:    http.StatusInternalServerError,
//			Message: "resource manager is nil",
//			Data:    nil,
//		}
//	}
//
//	app ,err := r.supbResMgr.GetApplication(types.DynamicApplication,appKey)
//	if err !=nil {
//		return &clireq.Response{
//			Code:    http.StatusBadRequest,
//			Message: fmt.Sprintf("%v", err),
//			Data:    nil,
//		}
//	}
//
//
//	return &clireq.Response{
//		Code:    http.StatusOK,
//		Message: clireq.HttpSucceed,
//		Data:    app.Devices(),
//	}
//}
//
//func (r *WebServer) getApplicationDevicesInfo(appKey string,guid string) *clireq.Response {
//	if r.supbStrategy ==nil || r.supbResMgr == nil {
//		return &clireq.Response{
//			Code:    http.StatusInternalServerError,
//			Message: "resource manager is nil",
//			Data:    nil,
//		}
//	}
//
//	app ,err := r.supbResMgr.GetApplication(types.DynamicApplication,appKey)
//	if err !=nil {
//		return &clireq.Response{
//			Code:    http.StatusBadRequest,
//			Message: fmt.Sprintf("%v", err),
//			Data:    nil,
//		}
//	}
//
//	info := app.DevicesInfo(guid)
//	if info ==nil {
//		return &clireq.Response{
//			Code:    http.StatusBadRequest,
//			Message: fmt.Sprintf("guid is nil"),
//			Data:    nil,
//		}
//	}
//
//
//	return &clireq.Response{
//		Code:    http.StatusOK,
//		Message: clireq.HttpSucceed,
//		Data:    info,
//	}
//}

func (r *WebServer) getApplicationDetailInfo(appKey string) *clireq.Response {
	if r.supbStrategy ==nil || r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	app ,err := r.supbResMgr.GetApplication(types.DynamicApplication,appKey)
	if err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	info := app.Devices()
	if info ==nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("guid is nil"),
			Data:    nil,
		}
	}


	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    info,
	}
}