package web_apis

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/web-apis/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbindex"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"io/ioutil"
	"net/http"
)

func (r *WebServer) index (w http.ResponseWriter, rq *http.Request) {
	var req,resp = &clireq.IndexRequest{},&clireq.Response{}

	r.logger.Infof("RUN")

	if buffers, err := ioutil.ReadAll(rq.Body);err != nil {
		r.responseErr(w,"read body %v",err)
		return
	}else{
		if err = json.Unmarshal(buffers, req); err != nil {
			r.responseErr(w,  "unmarshal %v", err)
			return
		}
	}
	switch req.Op {
	case clireq.IndexSwitchOn:
		resp = r.indexSwitchOn(req)
	case clireq.IndexSwitchOff:
		resp = r.indexSwitchOff(req)
	case clireq.IndexSwitchStatus:
		resp = r.indexSwitchStatue(req)
	case clireq.IndexListGet:
		resp = r.indexListGet(req)
	case clireq.IndexListSave:
		resp = r.indexListSave(req)
	case clireq.IndexGraphAdd:
		resp = r.indexGraphAdd(req)
	case clireq.IndexGraphDelete:
		resp = r.indexGraphDelete(req)
	default:
		resp = &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown type"),
			Data:    nil,
		}
	}

	bts, _ := json.Marshal(resp)
	if bt, err := w.Write(bts); err != nil {
		r.logger.Errorf("get function %v", err)
	} else {
		r.logger.Infof(fmt.Sprintf("LINK REPONSE (%vB)", bt))
	}
}

func (r *WebServer) indexSwitchOn(req *clireq.IndexRequest) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	switch req.Page {
	case supbindex.TypeStrategy:
		if _, err := r.supbResMgr.GetStrategy(types.DynamicStrategy,req.Key);err !=nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v",err),
				Data:   "" ,
			}
		}
	case supbindex.TypeApplication:
		if _, err := r.supbResMgr.GetApplication(types.DynamicApplication,req.Key);err !=nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v",err),
				Data:   "" ,
			}
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown type"),
			Data:   "" ,
		}
	}

	item,err := r.supbIndex.TypeKeyPair(req.Page,req.Key)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:   "" ,
		}
	}

	if err := r.supbIndex.SwitchOn(item,req.Mod);err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:   "" ,
		}
	}

	sh := r.supbIndex.GetSwitch(item)

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:   sh ,
	}
}

func (r *WebServer) indexSwitchOff(req *clireq.IndexRequest) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	switch req.Page {
	case supbindex.TypeStrategy:
		if _, err := r.supbResMgr.GetStrategy(types.DynamicStrategy,req.Key);err !=nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v",err),
				Data:   "" ,
			}
		}
	case supbindex.TypeApplication:
		if _, err := r.supbResMgr.GetApplication(types.DynamicApplication,req.Key);err !=nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v",err),
				Data:   "" ,
			}
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown type"),
			Data:   "" ,
		}
	}

	item,err := r.supbIndex.TypeKeyPair(req.Page,req.Key)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:   "" ,
		}
	}


	if err := r.supbIndex.SwitchOff(item,req.Mod);err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:   "" ,
		}
	}

	sh := r.supbIndex.GetSwitch(item)

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:   sh ,
	}
}

func (r *WebServer) indexSwitchStatue(req *clireq.IndexRequest) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	switch req.Page {
	case supbindex.TypeStrategy:
		if _, err := r.supbResMgr.GetStrategy(types.DynamicStrategy,req.Key);err !=nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v",err),
				Data:   "" ,
			}
		}
	case supbindex.TypeApplication:
		if _, err := r.supbResMgr.GetApplication(types.DynamicApplication,req.Key);err !=nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v",err),
				Data:   "" ,
			}
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown type"),
			Data:   "" ,
		}
	}

	item,err := r.supbIndex.TypeKeyPair(req.Page,req.Key)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:   "" ,
		}
	}

	sh := r.supbIndex.GetSwitch(item)

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:   sh ,
	}
}


func (r *WebServer) indexListGet(req *clireq.IndexRequest) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	list := r.supbIndex.GetList()

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:   list ,
	}
}



func (r *WebServer) indexListSave(req *clireq.IndexRequest) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	r.supbIndex.ReSortList(req.List)

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:   "",
	}
}

func (r *WebServer) indexGraphAdd(req *clireq.IndexRequest) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	err := r.indexRelationShipNodeCheck(req)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:   "" ,
		}
	}

	item,err :=r.supbIndex.TypeKeyPairRelationShip(req.Page,req.Key,req.Kind,req.Name)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:   "" ,
		}
	}
	err = r.supbIndex.GraphAdd(item)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:   "" ,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:   "" ,
	}
}

func (r *WebServer) indexGraphDelete(req *clireq.IndexRequest) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	err := r.indexRelationShipNodeCheck(req)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:   "" ,
		}
	}

	item,err :=r.supbIndex.TypeKeyPairRelationShip(req.Page,req.Key,req.Kind,req.Name)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:   "" ,
		}
	}
	err = r.supbIndex.GraphDelete(item)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:   "" ,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:   "" ,
	}
}

func (r *WebServer) indexRelationShipNodeCheck(req *clireq.IndexRequest) error {
	switch req.Kind {
	case clireq.RelationshipCategoryStrategyConfig:
		_ , err := r.supbResMgr.GetStrategy(types.StaticStrategy, req.Key)
		if err != nil {
			return err
		}
		return nil
	case clireq.RelationshipCategoryStrategyInstance:
		_ , err := r.supbResMgr.GetStrategy(types.DynamicStrategy, req.Key)
		if err != nil {
			return err
		}
		return nil
	case clireq.RelationshipCategoryStrategyContainers:
		sts, err := r.supbResMgr.ListStrategies(types.DynamicStrategy)
		if err != nil {
			return err
		}
		for _, v := range sts {
			for _, ct := range v.StrategyContainers {
				if ct.Guid == req.Key {
					return nil
				}
			}
		}

		return fmt.Errorf("unknow guid")
	case clireq.RelationshipCategoryApplicationConfig:
		_ , err := r.supbResMgr.GetApplication(types.StaticApplication, req.Key)
		if err != nil {
			return err
		}
		return nil
	case clireq.RelationshipCategoryApplicationInstance:
		_ , err := r.supbResMgr.GetApplication(types.DynamicApplication, req.Key)
		if err != nil {
			return err
		}
		return nil
	case clireq.RelationshipCategoryApplicationDevices:
		sts, err := r.supbResMgr.ListApplications(types.DynamicApplication)
		if err != nil {
			return err
		}
		for _, v := range sts {
			for _, ct := range v.ApplicationContainers {
				if ct.Guid == req.Key {
					return nil
				}
			}
		}

		return fmt.Errorf("unkown guid")
	default:
		return fmt.Errorf("unkown kind")
	}
}
