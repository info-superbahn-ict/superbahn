package web_apis

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/web-apis/clireq"
	"io/ioutil"
	"net/http"
)

func (r *WebServer) link(w http.ResponseWriter, rq *http.Request) {
	var req, resp = &clireq.MultiLinkRequest{}, &clireq.Response{}

	r.logger.Infof("LINK")

	if buffers, err := ioutil.ReadAll(rq.Body); err != nil {
		r.responseErr(w, "read body %v", err)
		return
	} else {
		if err = json.Unmarshal(buffers, req); err != nil {
			r.responseErr(w, "unmarshal %v", err)
			return
		}
	}
	switch req.Op {
	case clireq.LinkStrategiesAndApplications:
		resp = r.multiLinkImpl(req)
	default:
		resp = &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown type"),
			Data:    nil,
		}
	}

	bts, _ := json.Marshal(resp)
	if bt, err := w.Write(bts); err != nil {
		r.logger.Errorf("get function %v", err)
	} else {
		r.logger.Infof(fmt.Sprintf("LINK REPONSE (%vB)", bt))
	}
}

func (r *WebServer) multiLinkImpl(req *clireq.MultiLinkRequest) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	multiResp := clireq.MultiLinkResponse{
		LinksFailed: map[string]string{},
		LinksSucceed: map[string]string{},
		UnLinksFailed: map[string]string{},
		UnLinksSucceed: map[string]string{},
	}

	switch req.Kind {
	case clireq.MultiLinkStrategy:
		for k,v := range req.LinksKey {
			if err := r.supbResMgr.Link(v, k, req.Type, req.Key); err != nil {
				multiResp.LinksFailed[k] = fmt.Sprintf("error: %v",err)
			}else{
				multiResp.LinksSucceed[k] = v
			}
		}
		for k,v := range req.UnLinksKey {
			if err := r.supbResMgr.UnLink(v, k, req.Type, req.Key); err != nil {
				multiResp.UnLinksFailed[k] = fmt.Sprintf("error: %v",err)
			}else{
				multiResp.UnLinksSucceed[k] = v
			}
		}
	case clireq.MultiLinkApplication:
		for k,v := range req.LinksKey {
			if err := r.supbResMgr.Link(req.Type, req.Key,v, k); err != nil {
				multiResp.LinksFailed[k] = fmt.Sprintf("error: %v",err)
			}else{
				multiResp.LinksSucceed[k] = v
			}
		}
		for k,v := range req.UnLinksKey {
			if err := r.supbResMgr.UnLink(req.Type, req.Key,v, k); err != nil {
				multiResp.UnLinksFailed[k] = fmt.Sprintf("error: %v",err)
			}else{
				multiResp.UnLinksSucceed[k] = v
			}
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown kind %v, must be [%v|%v]",req.Kind,clireq.MultiLinkStrategy,clireq.MultiLinkApplication),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    multiResp,
	}
}



//
//func (r *WebServer) linkImpl(req *clireq.LinkRequest) *clireq.Response {
//	if r.supbResMgr == nil {
//		return &clireq.Response{
//			Code:    http.StatusInternalServerError,
//			Message: "resource manager is nil",
//			Data:    nil,
//		}
//	}
//
//	if err := r.supbResMgr.Link(req.ApplicationType, req.ApplicationKey, req.StrategyType, req.StrategyKey); err != nil {
//		return &clireq.Response{
//			Code:    http.StatusBadRequest,
//			Message: fmt.Sprintf("%v", err),
//			Data:    nil,
//		}
//	}
//
//	return &clireq.Response{
//		Code:    http.StatusOK,
//		Message: clireq.HttpSucceed,
//		Data:    "",
//	}
//}
//
//func (r *WebServer) unlinkImpl(req *clireq.LinkRequest) *clireq.Response {
//	if r.supbResMgr == nil {
//		return &clireq.Response{
//			Code:    http.StatusInternalServerError,
//			Message: "resource manager is nil",
//			Data:    nil,
//		}
//	}
//
//	if err := r.supbResMgr.UnLink(req.ApplicationType, req.ApplicationKey, req.StrategyType, req.StrategyKey); err != nil {
//		return &clireq.Response{
//			Code:    http.StatusBadRequest,
//			Message: fmt.Sprintf("%v", err),
//			Data:    nil,
//		}
//	}
//
//	return &clireq.Response{
//		Code:    http.StatusOK,
//		Message: clireq.HttpSucceed,
//		Data:    nil,
//	}
//}
//
