package web_apis

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/web-apis/clireq"
	m2 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications/model"
	m1 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

func (r *WebServer) list(w http.ResponseWriter, rq *http.Request) {
	var req, resp = &clireq.ListRequest{}, &clireq.Response{}

	r.logger.Infof("LIST")

	if buffers, err := ioutil.ReadAll(rq.Body); err != nil {
		r.responseErr(w, "read body %v", err)
		return
	} else {
		if err = json.Unmarshal(buffers, req); err != nil {
			r.responseErr(w, "unmarshal %v", err)
			return
		}
	}

	switch req.Op {
	// todo list 策略
	case clireq.ListStaticStrategies:
		resp = r.listStrategies(types.StaticStrategy)
	case clireq.ListDynamicStrategies:
		resp = r.listStrategies(types.DynamicStrategy)

	// todo list 应用
	case clireq.ListStaticApplications:
		resp = r.listApplications(types.StaticApplication)
	case clireq.ListDynamicApplications:
		resp = r.listApplications(types.DynamicApplication)

	// todo list 策略 筛选
	case clireq.ListStaticStrategiesWithFilter:
		resp = r.listStrategiesWithFilter(types.StaticStrategy, req.Filters)
	case clireq.ListDynamicStrategiesWithFilter:
		resp = r.listStrategiesWithFilter(types.DynamicApplication, req.Filters)

	// todo list 策略 筛选
	case clireq.ListStaticApplicationsWithFilter:
		resp = r.listApplicationsWithFilter(types.StaticApplication,req.Filters)
	case clireq.ListDynamicApplicationsWithFilter:
		resp = r.listApplicationsWithFilter(types.DynamicApplication,req.Filters)

	case clireq.ListStrategiesMetricsList:
		resp = r.listStrategiesMetricsNow(req.MetricsKeys)
	case clireq.ListApplicationsMetricsList:
		resp = r.listApplicationsMetricsNow(req.MetricsKeys)

	default:
		resp = &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown type %v",req.Op),
			Data:    nil,
		}
	}

	bts, _ := json.Marshal(resp)
	if bt, err := w.Write(bts); err != nil {
		r.logger.Errorf("get function %v", err)
	} else {
		r.logger.Infof(fmt.Sprintf("LINK REPONSE (%vB)", bt))
	}
}

func (r *WebServer) listStrategies(types string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	sts, err := r.supbResMgr.ListStrategies(types)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    sts,
	}
}

func (r *WebServer) listApplications(types string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	apps, err := r.supbResMgr.ListApplications(types)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    apps,
	}
}

func (r *WebServer) listStrategiesWithFilter(types string, filters map[string]string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	sts, err := r.supbResMgr.ListStrategies(types)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	var stFilters []*m1.SupbStrategyBaseInfo

	for _, strategy := range sts {
		match := true
		for k, v := range filters {
			switch k {
			case clireq.FilterKeyName:
				if !strings.Contains(strategy.StrategyName, v) {
					match = false
				}
			case clireq.FilterKeyStatus:
				if !strings.EqualFold(strategy.StrategyStatus, v) {
					match = false
				}
			case clireq.FilterKeyLevel:
				if !strings.EqualFold(string(strategy.StrategyLevel), v) {
					match = false
				}
			default:
				match = false
			}
		}
		if match {
			stFilters = append(stFilters, strategy.DeepCopy())
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    stFilters,
	}
}

func (r *WebServer) listApplicationsWithFilter(types string, filters map[string]string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	apps, err := r.supbResMgr.ListApplications(types)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	var appFilters []*m2.SupbApplicationBaseInfo

	r.logger.Infof("r %v",filters)

	for _, app := range apps {
		match := true
		for k, v := range filters {
			switch k {
			case clireq.FilterKeyName:
				if !strings.Contains(app.ApplicationName, v) {
					match = false
				}
			case clireq.FilterKeyStatus:
				if !strings.EqualFold(app.ApplicationStatus, v) {
					match = false
				}
			case clireq.FilterKeyLevel:
				if !strings.EqualFold(app.ApplicationLevel, v) {
					match = false
				}
			default:
				match = false
			}
		}
		if match {
			appFilters = append(appFilters, app.DeepCopy())
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    appFilters,
	}
}


func (r *WebServer) listStrategiesMetricsNow(keys []string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}


	dt := make(map[string]*clireq.RunTimeResourceUsage)
	for _, key := range keys {
		st,err :=r.supbResMgr.GetStrategy(types.DynamicStrategy,key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusInternalServerError,
				Message: fmt.Sprintf("%v",err),
				Data:    nil,
			}
		}

		runTime := &clireq.RunTimeResourceUsage{}

		var cpuUsed float64 = 0
		var memUsed float64 = 0
		for _,ct := range st.StrategyContainers {
			mc,err  := r.supbMetrics.GetMetricsNow(ct.Guid,time.Second)
			if err !=nil {
				return &clireq.Response{
					Code:    http.StatusInternalServerError,
					Message: fmt.Sprintf("%v",err),
					Data:    nil,
				}
			}
			runTime.CPUTotal += mc.CpuTotal
			cpuUsed += mc.CpuTotal * mc.CPUPercentage
			runTime.MemoryTotal += mc.MemoryTotal
			memUsed += mc.MemoryTotal * mc.MemoryPercentage
			runTime.NetTx += mc.NetworkTx
			runTime.NetRx += mc.NetworkRx
		}

		runTime.CPUUsage = cpuUsed / runTime.CPUTotal
		runTime.MemoryUsage = memUsed / runTime.MemoryTotal

		dt[key] =  runTime
	}

	//offset := time.Now().Second() % 60
	//
	//dt := make(map[string]*clireq.RunTimeResourceUsage)
	//for _,k := range keys {
	//	dt[k]= &clireq.RunTimeResourceUsage{
	//		CPUTotal:    4000,
	//		CPUUsage:    10.2 + float64(offset)/2,
	//		MemoryTotal: 8 * 1024 * 1204 * 1204,
	//		MemoryUsage: 40.2 + float64(offset)/6,
	//		NetRx:       float64(rand.Uint64() % 100),
	//		NetTx:       float64(rand.Uint64() % 100),
	//	}
	//}


	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    dt,
	}
}

func (r *WebServer) listApplicationsMetricsNow(keys []string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	dt := make(map[string]*clireq.RunTimeResourceUsage)
	for _, key := range keys {
		app,err :=r.supbResMgr.GetApplication(types.DynamicApplication,key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusInternalServerError,
				Message: fmt.Sprintf("%v",err),
				Data:    nil,
			}
		}

		runTime := &clireq.RunTimeResourceUsage{}

		var cpuUsed float64 = 0
		var memUsed float64 = 0
		for _,ct := range app.ApplicationContainers {
			mc,err  := r.supbMetrics.GetMetricsNow(ct.Guid,time.Second)
			if err !=nil {
				return &clireq.Response{
					Code:    http.StatusInternalServerError,
					Message: fmt.Sprintf("%v",err),
					Data:    nil,
				}
			}
			runTime.CPUTotal += mc.CpuTotal
			cpuUsed += mc.CpuTotal * mc.CPUPercentage
			runTime.MemoryTotal += mc.MemoryTotal
			memUsed += mc.MemoryTotal * mc.MemoryPercentage
			runTime.NetTx += mc.NetworkTx
			runTime.NetRx += mc.NetworkRx
		}

		runTime.CPUUsage = cpuUsed / runTime.CPUTotal
		runTime.MemoryUsage = memUsed / runTime.MemoryTotal

		dt[key] =  runTime
	}


	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    dt,
	}
}