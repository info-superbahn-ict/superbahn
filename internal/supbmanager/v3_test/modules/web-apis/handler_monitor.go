package web_apis

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/web-apis/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	supreq "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbtrace/request"
	"io/ioutil"
	"math"
	"net/http"
	"sort"
	"strconv"
	"time"
)

func (r *WebServer) metrics(w http.ResponseWriter, rq *http.Request) {
	var req, resp = &clireq.MonitorRequest{}, &clireq.Response{}

	r.logger.Infof("METRIC")

	if buffers, err := ioutil.ReadAll(rq.Body); err != nil {
		r.responseErr(w, "read body %v", err)
		return
	} else {
		if err = json.Unmarshal(buffers, req); err != nil {
			r.responseErr(w, "unmarshal %v", err)
			return
		}
	}

	switch req.Op {

	case clireq.MetricsAdd:
		resp = r.containerMetricsAdd(req)
	case clireq.MetricsDelete:
		resp = r.containerMetricsDelete(req)
	case clireq.MetricsContainerCPU:
		resp = r.containerMetrics(req, clireq.MetricsKindCPU)
	case clireq.MetricsContainerMemory:
		resp = r.containerMetrics(req, clireq.MetricsKindMemory)
	case clireq.MetricsContainerNetRx:
		resp = r.containerMetrics(req, clireq.MetricsKindNetRx)
	case clireq.MetricsContainerNetTx:
		resp = r.containerMetrics(req, clireq.MetricsKindNetTx)
	case clireq.MetricsContainerAll:
		resp = r.containerMetrics(req, clireq.MetricsKindAll)
	case clireq.MetricsContainerNow:
		resp = r.containerMetricsNow(req)

	case clireq.LogsContainer:
		resp = r.containerLogs(req)
	case clireq.LogsContainerFilter:
		resp = r.containerLogsWithFilter(req)
	case clireq.LogsAdd:
		resp = r.containerLogsAdd(req)
	case clireq.LogsDelete:
		resp = r.containerLogsDelete(req)
	case clireq.LogsLevel:
		resp = r.containerLogsLevel(req)
	case clireq.LogsStatistic:
		resp = r.containerLogsStatistic(req)

	case clireq.TracesGetDag:
		resp = r.traceDAG(req)
	case clireq.TracesGetStatistic:
		resp = r.traceStatistic(req)
	case clireq.TracesGetServices:
		resp = r.traceServices(req)
	case clireq.TracesGetOperation:
		resp = r.traceOperate(req)
	case clireq.TracesGetList:
		resp = r.traceList(req)
	case clireq.TracesGetData:
		resp = r.traceData(req)
	case clireq.TracesGetDataLatest:
		resp = r.traceDataLatest(req)

	case clireq.MetricsMonitorSwitch:
		resp = r.containerMetricsSwitch(req)
	case clireq.LogsMonitorSwitch:
		resp = r.containerLogsSwitch(req)

	default:
		resp = &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown type"),
			Data:    nil,
		}
	}

	bts, _ := json.Marshal(resp)

	if bt, err := w.Write(bts); err != nil {
		r.logger.Errorf("get function %v", err)
	} else {
		r.logger.Infof(fmt.Sprintf("METRIC REPONSE (%vB)", bt))
	}

}

func (r *WebServer) containerMetricsAdd(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbMetrics == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	if err := r.checkKeyAndGuid(req.Key, req.Guid, req.Kind); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	r.supbMetrics.AddGuid(req.Guid)

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    "",
	}
}

func (r *WebServer) containerMetricsDelete(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbMetrics == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	if err := r.checkKeyAndGuid(req.Key, req.Guid, req.Kind); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	r.supbMetrics.DeleteGuid(req.Guid)

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    "",
	}
}

func (r *WebServer) containerMetrics(req *clireq.MonitorRequest, metricsKind string) *clireq.Response {
	if r.supbMetrics == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	if err := r.checkKeyAndGuid(req.Key, req.Guid, req.Kind); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	var (
		err  error
		data interface{}
	)

	//r.logger.Infof("%v %v",time.Unix(req.StartTime,0),time.Unix(req.EndTime,0))

	switch metricsKind {
	case clireq.MetricsKindCPU:
		data, err = r.supbMetrics.GetCPUWithTimeRange(req.Guid, time.Unix(req.StartTime,  req.EndTime), time.Unix(req.EndTime, 0))
	case clireq.MetricsKindMemory:
		data, err = r.supbMetrics.GetMemoryWithTimeRange(req.Guid, time.Unix(req.StartTime,  req.EndTime), time.Unix(req.EndTime, 0))
	case clireq.MetricsKindNetRx:
		data, err = r.supbMetrics.GetNetRxWithTimeRange(req.Guid, time.Unix(req.StartTime,  req.EndTime), time.Unix(req.EndTime, 0))
	case clireq.MetricsKindNetTx:
		data, err = r.supbMetrics.GetNetTxWithTimeRange(req.Guid, time.Unix(req.StartTime,  req.EndTime), time.Unix(req.EndTime, 0))
	case clireq.MetricsKindAll:
		cpu, err := r.supbMetrics.GetCPUWithTimeRange(req.Guid, time.Unix(req.StartTime, req.EndTime), time.Unix(req.EndTime, 0))
		if err != nil {
			return &clireq.Response{Code: http.StatusBadRequest, Message: fmt.Sprintf("%v", err), Data: nil}
		}
		mem, err := r.supbMetrics.GetMemoryWithTimeRange(req.Guid, time.Unix(req.StartTime,  req.EndTime), time.Unix(req.EndTime, 0))
		if err != nil {
			return &clireq.Response{Code: http.StatusBadRequest, Message: fmt.Sprintf("%v", err), Data: nil}
		}
		rx, err := r.supbMetrics.GetNetRxWithTimeRange(req.Guid, time.Unix(req.StartTime,  req.EndTime), time.Unix(req.EndTime, 0))
		if err != nil {
			return &clireq.Response{Code: http.StatusBadRequest, Message: fmt.Sprintf("%v", err), Data: nil}
		}
		tx, err := r.supbMetrics.GetNetTxWithTimeRange(req.Guid, time.Unix(req.StartTime,  req.EndTime), time.Unix(req.EndTime, 0))
		if err != nil {
			return &clireq.Response{Code: http.StatusBadRequest, Message: fmt.Sprintf("%v", err), Data: nil}
		}
		data = map[string]interface{}{
			"cpu":    cpu,
			"memory": mem,
			"netRx":  rx,
			"netTx":  tx,
		}
	}

	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    data,
	}
}

func (r *WebServer) containerMetricsNow(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbMetrics == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	if err := r.checkKeyAndGuid(req.Key, req.Guid, req.Kind); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	data, err := r.supbMetrics.GetMetricsNow(req.Guid, time.Second)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    data,
	}
}

func (r *WebServer) containerLogs(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbLogClr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	if err := r.checkKeyAndGuid(req.Key, req.Guid, req.Kind); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	data, err := r.supbLogClr.GetLogLatest(req.Guid, int(req.LogsCount))
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}
	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    data,
	}
}

func (r *WebServer) containerLogsWithFilter(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbLogClr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	if err := r.checkKeyAndGuid(req.Key, req.Guid, req.Kind); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	data, err := r.supbLogClr.GetLogWithFilter(req.Guid, int(req.LogsCount), req.LogsFilter)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    data,
	}

}

func (r *WebServer) containerLogsAdd(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbLogClr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	if err := r.checkKeyAndGuid(req.Key, req.Guid, req.Kind); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	r.supbLogClr.AddGuid(req.Guid)

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    "",
	}
}

func (r *WebServer) containerLogsDelete(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbLogClr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	if err := r.checkKeyAndGuid(req.Key, req.Guid, req.Kind); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	r.supbLogClr.DeleteGuid(req.Guid)

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    "",
	}
}

func (r *WebServer) containerLogsLevel(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbLogClr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	switch req.Kind {
	case clireq.MonitorStrategy:
		_, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}

		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data:    r.supbLogClr.LogLevels(),
		}
	case clireq.MonitorApplication:
		_, err := r.supbResMgr.GetApplication(types.DynamicApplication, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}

		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data:    r.supbLogClr.LogLevels(),
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkwon kind must be [%v|%v]", clireq.MonitorStrategy, clireq.MonitorApplication),
			Data:    nil,
		}
	}
}

func (r *WebServer) containerLogsStatistic(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbLogClr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	if err := r.checkKeyAndGuid(req.Key, req.Guid, req.Kind); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	statistic, err := r.supbLogClr.GetLogStatistic(req.Guid)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    statistic,
	}

}

func (r *WebServer) containerMetricsSwitch(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbLogClr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	switch req.Kind {
	case clireq.MonitorStrategy:
		st, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}

		res := make(map[string]string, len(st.StrategyContainers))

		for _, v := range st.StrategyContainers {
			if r.supbMetrics.IsMonitor(v.Guid) {
				res[v.Guid] = "on"
			} else {
				res[v.Guid] = "off"
			}
		}

		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data:    res,
		}
	case clireq.MonitorApplication:
		app, err := r.supbResMgr.GetApplication(types.DynamicApplication, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}

		res := make(map[string]string, len(app.ApplicationContainers))
		for _, v := range app.ApplicationContainers {
			if r.supbMetrics.IsMonitor(v.Guid) {
				res[v.Guid] = "on"
			} else {
				res[v.Guid] = "off"
			}
		}

		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data:    res,
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkwon kind must be [%v|%v]", clireq.MonitorStrategy, clireq.MonitorApplication),
			Data:    nil,
		}
	}
}

func (r *WebServer) containerLogsSwitch(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbLogClr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	switch req.Kind {
	case clireq.MonitorStrategy:
		st, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}

		res := make(map[string]string, len(st.StrategyContainers))

		for _, v := range st.StrategyContainers {
			if r.supbLogClr.IsMonitor(v.Guid) {
				res[v.Guid] = "on"
			} else {
				res[v.Guid] = "off"
			}
		}

		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data:    res,
		}
	case clireq.MonitorApplication:
		app, err := r.supbResMgr.GetApplication(types.DynamicApplication, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}

		res := make(map[string]string, len(app.ApplicationContainers))
		for _, v := range app.ApplicationContainers {
			if r.supbLogClr.IsMonitor(v.Guid) {
				res[v.Guid] = "on"
			} else {
				res[v.Guid] = "off"
			}
		}

		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data:    res,
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkwon kind must be [%v|%v]", clireq.MonitorStrategy, clireq.MonitorApplication),
			Data:    nil,
		}
	}
}

func (r *WebServer) checkKeyAndGuid(key, guid string, kind string) error {
	switch kind {
	case clireq.MonitorStrategy:
		st, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, key)
		if err != nil {
			return fmt.Errorf("%v", err)
		}

		match := false
		for _, v := range st.StrategyContainers {
			if v.Guid == guid {
				match = true
			}
		}
		if !match {
			return fmt.Errorf("guid don't exist")
		}
	case clireq.MonitorApplication:
		app, err := r.supbResMgr.GetApplication(types.DynamicApplication, key)
		if err != nil {
			return fmt.Errorf("%v", err)
		}

		match := false
		for _, v := range app.ApplicationContainers {
			if v.Guid == guid {
				match = true
			}
		}
		if !match {
			return fmt.Errorf("guid don't exist")
		}
	default:
		return fmt.Errorf("unkwon kind")
	}
	return nil
}

func (r *WebServer) traceDAG(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbTrace == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "tracer is nil",
			Data:    nil,
		}
	}
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "tracer is nil",
			Data:    nil,
		}
	}

	switch req.Kind {
	case clireq.MonitorStrategy:
		_, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
	case clireq.MonitorApplication:
		_, err := r.supbResMgr.GetApplication(types.DynamicApplication, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkown kind"),
			Data:    nil,
		}
	}

	params := supreq.Params{
		//End: req.End,
		End:      strconv.Itoa(int(time.Now().Unix() * 1000)),
		LookBack: "604800000",
	}
	data, err := r.supbTrace.GetDependenciesDAG(params)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	nodesMap := make(map[string]int)
	links := make([]clireq.DAGLink, len(data))

	for _, v := range data {
		nodesMap[v.(map[string]interface{})["parent"].(string)]++
		nodesMap[v.(map[string]interface{})["child"].(string)]++
	}

	nodes, nid := make([]*clireq.DAGNode, len(nodesMap)), 0
	for k, v := range nodesMap {
		nodes[nid] = &clireq.DAGNode{
			Name: k,
			Size: float64(v) + baseValue,
		}
		nodesMap[k] = nid
		nid++
	}

	for k, v := range data {
		links[k].LabelSize = v.(map[string]interface{})["callCount"].(float64)
		links[k].Source = nodesMap[v.(map[string]interface{})["parent"].(string)]
		links[k].Target = nodesMap[v.(map[string]interface{})["child"].(string)]
		links[k].Label = ""
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data: map[string]interface{}{
			"nodes": nodes,
			"links": links,
		},
	}
}

func (r *WebServer) traceStatistic(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbTrace == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "tracer is nil",
			Data:    nil,
		}
	}
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "tracer is nil",
			Data:    nil,
		}
	}

	switch req.Kind {
	case clireq.MonitorStrategy:
		_, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
	case clireq.MonitorApplication:
		_, err := r.supbResMgr.GetApplication(types.DynamicApplication, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkown kind"),
			Data:    nil,
		}
	}

	params := supreq.Params{
		Service:     req.Service,
		//Start:       strconv.Itoa((int(time.Now().Unix()) - 604800) *1000000),
		//End:         strconv.Itoa(int(time.Now().Unix()) * 1000000),
		LookBack:  "604800",

		//Service:     "frontend",
		//Operation: 	req.Operation,
		//Start:       "1640837157459000",
		//End:         "1640923557459000",
		//MaxDuration: "300ms",
		//MinDuration: "100ms",
		Limit: "20",
	}

	data, err := r.supbTrace.GetTraces(params)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	r.logger.Infof("trace count [%v]",len(data.([]interface{})))

	//proc := data.([]interface{})[0].(map[string]interface{})["processes"].(map[string]interface{})
	//spans := data.([]interface{})[0].(map[string]interface{})["spans"].([]interface{})
	avgs := make(map[string]float64)
	//sns := make(map[string]string)
	//ops := make(map[string]string)
	count := make(map[string]float64)
	st := make(map[string][]float64)

	for _, v := range data.([]interface{}) {
		//proc := v.(map[string]interface{})["processes"].(map[string]interface{})
		spans := v.(map[string]interface{})["spans"].([]interface{})
		//
		//for k := range proc {
		//	srvs[k] = 0
		//	sns[k] = proc[k].(map[string]interface{})["serviceName"].(string)
		//	count[k] = 0
		//	st[k] = []float64{}
		//}

		for _, v2 := range spans {
			op := v2.(map[string]interface{})["operationName"].(string)
			d := v2.(map[string]interface{})["duration"].(float64) / 1000

			count[op] ++
			avgs[op] += d
			st[op] = append(st[op], d)


			//p := v2.(map[string]interface{})["processID"].(string)
			//d := v2.(map[string]interface{})["duration"].(float64) / 1000
			//srvs[p] += d
			//count[p] ++
			//st[p] = append(st[p], d)
			//ops[op] = sns[p]
		}
	}

	r.logger.Infof("service count %v",count)
	//r.logger.Infof("service st %v",st)
	//r.logger.Infof("service srvs %v",srvs)

	mean := make(map[string]float64, len(avgs))
	p95 := make(map[string]float64, len(avgs))
	std := make(map[string]float64, len(avgs))
	for k, v := range avgs {
		avg := v/ count[k]

		mean[k] = avg
		sort.Float64s(st[k])
		p95[k] = st[k][int(count[k]*0.95)]

		var sd float64
		for _, v2 := range st[k] {
			sd += math.Pow(v2-avg, 2)
		}
		std[k] = math.Sqrt(sd / count[k])

		//srvName := sns[k]
		//avg := v / count[k]
		//
		//mean[srvName] = avg
		//
		//sort.Float64s(st[k])
		//p95[srvName] = st[k][int(count[k]*0.95)]
		//
		//var sd float64
		//for _, v2 := range st[k] {
		//	sd += math.Pow(v2-avg, 2)
		//}
		//std[srvName] = math.Sqrt(sd / count[k])
	}

	Resp, rid := make([][]interface{}, len(avgs)), 0

	for k := range mean {
		Resp[rid] = []interface{}{
			k, fmt.Sprintf("%.2f",mean[k]), fmt.Sprintf("%.2f",std[k]), fmt.Sprintf("%.2f",p95[k]),
		}
		rid++
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    Resp,
	}
}

func (r *WebServer) traceServices(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbTrace == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "tracer is nil",
			Data:    nil,
		}
	}
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "tracer is nil",
			Data:    nil,
		}
	}

	switch req.Kind {
	case clireq.MonitorStrategy:
		_, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
	case clireq.MonitorApplication:
		_, err := r.supbResMgr.GetApplication(types.DynamicApplication, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkown kind"),
			Data:    nil,
		}
	}

	data, err := r.supbTrace.GetServices()
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    data,
	}
}

func (r *WebServer) traceOperate(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbTrace == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "tracer is nil",
			Data:    nil,
		}
	}
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "tracer is nil",
			Data:    nil,
		}
	}

	switch req.Kind {
	case clireq.MonitorStrategy:
		_, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
	case clireq.MonitorApplication:
		_, err := r.supbResMgr.GetApplication(types.DynamicApplication, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkown kind"),
			Data:    nil,
		}
	}

	data, err := r.supbTrace.GetOperations(req.Service)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    data,
	}
}

func (r *WebServer) traceList(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbTrace == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "tracer is nil",
			Data:    nil,
		}
	}
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "tracer is nil",
			Data:    nil,
		}
	}

	switch req.Kind {
	case clireq.MonitorStrategy:
		_, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
	case clireq.MonitorApplication:
		_, err := r.supbResMgr.GetApplication(types.DynamicApplication, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkown kind"),
			Data:    nil,
		}
	}

	bts, _ := json.Marshal(req)
	r.logger.Infof("get trace %s", bts)

	params := supreq.Params{
		Service:     req.Service,
		Operation:   req.Operation,
		Start:       req.Start,
		End:         req.End,
		MaxDuration: req.MaxDuration,
		MinDuration: req.MinDuration,
		Limit:       req.Limit,

		//Service:     "frontend",
		//Start:       "1640837157459000",
		//End:         "1640923557459000",
		//MaxDuration: "300ms",
		//MinDuration: "100ms",
	}

	data, err := r.supbTrace.GetTraces(params)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	//r.logger.Infof("traces %v",data)

	//ids := make([]string, len(data.([]interface{})))
	//idNotes := make([]string, len(data.([]interface{})))
	idMaps := make(map[string]string, len(data.([]interface{})))
	for _, v := range data.([]interface{}) {
		traceId := v.(map[string]interface{})["traceID"].(string)
		//ids[k] = traceId
		operate := v.(map[string]interface{})["spans"].([]interface{})[0].(map[string]interface{})["operationName"].(string)
		//idNotes[k] = operate
		idMaps[traceId] = operate
		//for _, v2 := range v.(map[string]interface{})["spans"].([]interface{}) {
		//	spanId := v2.(map[string]interface{})["spanID"].(string)
		//	if spanId == traceId {
		//		operate := v2.(map[string]interface{})["operationName"].(string)
		//		idNotes[k] = operate
		//		idMaps[traceId] = operate
		//	}
		//	//idNotes[k] = v2.(map[string]interface{})["operationName"].(string)
		//	//idMaps[v2.(map[string]interface{})["operationName"].(string)] = v.(map[string]interface{})["traceID"].(string)
		//}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    idMaps,
	}
}

func (r *WebServer) traceDataLatest(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbTrace == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "tracer is nil",
			Data:    nil,
		}
	}
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "tracer is nil",
			Data:    nil,
		}
	}

	switch req.Kind {
	case clireq.MonitorStrategy:
		_, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
	case clireq.MonitorApplication:
		_, err := r.supbResMgr.GetApplication(types.DynamicApplication, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkown kind"),
			Data:    nil,
		}
	}

	bts, _ := json.Marshal(req)
	r.logger.Infof("get trace %s", bts)

	params := supreq.Params{
		Service:   req.Service,
		Operation: req.Operation,
		Limit:     "1",
		//Service:     "frontend",
		//Start:       "1640837157459000",
		//End:         "1640923557459000",
		//MaxDuration: "300ms",
		//MinDuration: "100ms",
	}

	r.logger.Infof("get trace param %v", params)

	trs, err := r.supbTrace.GetTraces(params)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	if len(trs.([]interface{})) == 0 {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("no trace"),
			Data:    nil,
		}
	}

	traceId := trs.([]interface{})[0].(map[string]interface{})["traceID"].(string)

	data, err := r.supbTrace.GetTrace(traceId)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	trace := data.([]interface{})[0]
	spans := trace.(map[string]interface{})["spans"].([]interface{})
	spanData, sid := make([][5]interface{}, len(spans)), 0

	for _, v := range spans {
		refs := v.(map[string]interface{})["references"].([]interface{})
		spanId := v.(map[string]interface{})["spanID"].(string)
		if len(refs) == 0 {
			spanData[0][0] = spanId
			spanData[0][1] = v.(map[string]interface{})["operationName"]
			spanData[0][2] = v.(map[string]interface{})["startTime"]
			spanData[0][4] = v.(map[string]interface{})["duration"]
			spanData[0][3] = spanData[0][2].(float64) + spanData[0][4].(float64)
			break
		}
	}

	lid, tail := 1, 1
	for {
		for _, v := range spans {
			refs := v.(map[string]interface{})["references"].([]interface{})
			spanId := v.(map[string]interface{})["spanID"].(string)
			for _, rf := range refs {
				parentId := rf.(map[string]interface{})["spanID"].(string)
				for i := sid; i < lid; i++ {
					if spanData[i][0].(string) == parentId {
						spanData[tail][0] = spanId
						spanData[tail][1] = v.(map[string]interface{})["operationName"]
						spanData[tail][2] = v.(map[string]interface{})["startTime"]
						spanData[tail][4] = v.(map[string]interface{})["duration"]
						spanData[tail][3] = spanData[tail][2].(float64) + spanData[tail][4].(float64)
						tail++
					}
				}
			}
		}
		sid = lid
		lid = tail
		if tail >= len(spans) {
			break
		}
	}

	respData := make([][5]interface{}, len(spans))
	for k, v := range spanData {
		respData[len(spans)-1-k] = v
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    respData,
	}

}

func (r *WebServer) traceData(req *clireq.MonitorRequest) *clireq.Response {
	if r.supbTrace == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "tracer is nil",
			Data:    nil,
		}
	}
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "tracer is nil",
			Data:    nil,
		}
	}

	switch req.Kind {
	case clireq.MonitorStrategy:
		_, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
	case clireq.MonitorApplication:
		_, err := r.supbResMgr.GetApplication(types.DynamicApplication, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkown kind"),
			Data:    nil,
		}
	}

	data, err := r.supbTrace.GetTrace(req.TraceId)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	trace := data.([]interface{})[0]
	spans := trace.(map[string]interface{})["spans"].([]interface{})
	spanData, sid := make([][5]interface{}, len(spans)), 0

	for _, v := range spans {
		refs := v.(map[string]interface{})["references"].([]interface{})
		spanId := v.(map[string]interface{})["spanID"].(string)
		if len(refs) == 0 {
			spanData[0][0] = spanId
			spanData[0][1] = v.(map[string]interface{})["operationName"]
			spanData[0][2] = v.(map[string]interface{})["startTime"]
			spanData[0][4] = v.(map[string]interface{})["duration"]
			spanData[0][3] = spanData[0][2].(float64) + spanData[0][4].(float64)
			break
		}
	}

	lid, tail := 1, 1
	for {
		for _, v := range spans {
			refs := v.(map[string]interface{})["references"].([]interface{})
			spanId := v.(map[string]interface{})["spanID"].(string)
			for _, rf := range refs {
				parentId := rf.(map[string]interface{})["spanID"].(string)
				for i := sid; i < lid; i++ {
					if spanData[i][0].(string) == parentId {
						spanData[tail][0] = spanId
						spanData[tail][1] = v.(map[string]interface{})["operationName"]
						spanData[tail][2] = v.(map[string]interface{})["startTime"]
						spanData[tail][4] = v.(map[string]interface{})["duration"]
						spanData[tail][3] = spanData[tail][2].(float64) + spanData[tail][4].(float64)
						tail++
					}
				}
			}
		}
		sid = lid
		lid = tail
		if tail >= len(spans) {
			break
		}
	}

	respData := make([][5]interface{}, len(spans))
	for k, v := range spanData {
		respData[len(spans)-1-k] = v
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    respData,
	}
}
