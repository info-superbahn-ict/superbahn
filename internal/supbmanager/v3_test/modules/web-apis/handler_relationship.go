package web_apis

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/web-apis/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/status"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"io/ioutil"
	"net/http"
	"strings"
)

const (
	baseValue = 10
)

func (r *WebServer) relationship(w http.ResponseWriter, rq *http.Request) {
	var req, resp = &clireq.RelationshipRequest{}, &clireq.Response{}

	r.logger.Infof("STRATEGY")

	if buffers, err := ioutil.ReadAll(rq.Body); err != nil {
		r.responseErr(w, "read body %v", err)
		return
	} else {
		if err = json.Unmarshal(buffers, req); err != nil {
			r.responseErr(w, "unmarshal %v", err)
			return
		}
	}
	switch req.Op {
	case clireq.Relationship:
		resp = r.relationshipMap()
	case clireq.RelationshipVisitable:
		resp = r.relationshipVisitable(req)
	case clireq.RelationshipSub:
		resp = r.relationshipSubgraph(req)
	case clireq.RelationshipDetail:
		resp = r.relationshipDetail(req)
	case clireq.RelationshipFilter:
		resp = r.relationshipFilter(req)
	case clireq.RelationshipFilterKind:
		resp = r.relationshipFilterKind(req)
	case clireq.RelationshipFilterStatus:
		resp = r.relationshipFilterStatus(req)
	default:
		resp = &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown type"),
			Data:    nil,
		}
	}

	bts, _ := json.Marshal(resp)
	if bt, err := w.Write(bts); err != nil {
		r.logger.Errorf("get function %v", err)
	} else {
		r.logger.Infof(fmt.Sprintf("STRATEGY REPONSE (%vB)", bt))
	}
}

func (r *WebServer) relationshipMap() *clireq.Response {

	categories := []map[string]interface{}{
		{
			"name": clireq.RelationshipCategoryStrategyConfig,
		},
		{
			"name": clireq.RelationshipCategoryStrategyInstance,
		},
		{
			"name": clireq.RelationshipCategoryStrategyContainers,
		},
		{
			"name": clireq.RelationshipCategoryApplicationConfig,
		},
		{
			"name": clireq.RelationshipCategoryApplicationInstance,
		},
		{
			"name": clireq.RelationshipCategoryApplicationDevices,
		},
	}

	sst, err := r.supbResMgr.ListStrategies(types.StaticStrategy)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	dst, err := r.supbResMgr.ListStrategies(types.DynamicStrategy)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	scts := 0
	for _, v := range dst {
		scts += len(v.StrategyContainers)

	}

	sapp, err := r.supbResMgr.ListApplications(types.StaticApplication)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	dapp, err := r.supbResMgr.ListApplications(types.DynamicApplication)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	dcts, lks := 0, 0
	for _, v := range dapp {
		dcts += len(v.ApplicationContainers)
		lks += len(v.ApplicationDynamicStrategies)
	}

	nodes, nid := make([]*clireq.RelationShipNode, len(sst)+len(sapp)+len(dst)+len(dapp)+scts+dcts), 0
	links, lid := make([]*clireq.RelationShipLink, len(dst)+len(dapp)+scts+dcts+lks), 0

	sstmap := make(map[string]int)
	for _, v := range sst {
		nodes[nid] = &clireq.RelationShipNode{
			Key:       string(v.StrategyKey),
			Name:      v.StrategyName,
			Value:     baseValue,
			Category:  0,
			Visitable: 1,
		}
		sstmap[string(v.StrategyKey)] = nid
		nid++
	}

	dstmap := make(map[string]int)
	for _, v := range dst {
		nodes[nid] = &clireq.RelationShipNode{
			Key:       string(v.StrategyKey),
			Name:      v.StrategyName,
			Value:     len(v.StrategyContainers) + len(v.StrategySlaves) + baseValue,
			Category:  1,
			Visitable: 1,
		}
		links[lid] = &clireq.RelationShipLink{
			Source:    nid,
			Target:    sstmap[string(v.StrategyKeyParent)],
			Visitable: 1,
		}
		nodes[sstmap[string(v.StrategyKeyParent)]].Value++
		dstmap[string(v.StrategyKey)] = nid
		nid++
		lid++
	}

	for _, st := range dst {
		tg := dstmap[string(st.StrategyKey)]
		for _, v := range st.StrategyContainers {
			nodes[nid] = &clireq.RelationShipNode{
				Key:       v.Guid,
				Name:      v.Name,
				Value:     baseValue,
				Category:  2,
				Visitable: 1,
			}
			links[lid] = &clireq.RelationShipLink{
				Source:    nid,
				Target:    tg,
				Visitable: 1,
			}
			lid++
			nid++
		}
	}

	sappmap := make(map[string]int)
	for _, v := range sapp {
		nodes[nid] = &clireq.RelationShipNode{
			Key:       string(v.ApplicationKey),
			Name:      v.ApplicationName,
			Value:     baseValue,
			Category:  3,
			Visitable: 1,
		}
		sappmap[string(v.ApplicationKey)] = nid
		nid++
	}

	dappmap := make(map[string]int)
	for _, v := range dapp {
		nodes[nid] = &clireq.RelationShipNode{
			Key:       string(v.ApplicationKey),
			Name:      v.ApplicationName,
			Value:     len(v.ApplicationContainers) + baseValue,
			Category:  4,
			Visitable: 1,
		}
		links[lid] = &clireq.RelationShipLink{
			Source:    nid,
			Target:    sappmap[string(v.ApplicationKeyParent)],
			Visitable: 1,
		}
		dappmap[string(v.ApplicationKey)] = nid
		nodes[sappmap[string(v.ApplicationKeyParent)]].Value++
		nid++
		lid++
	}

	for _, app := range dapp {
		tg := dappmap[string(app.ApplicationKey)]
		for _, v := range app.ApplicationContainers {
			nodes[nid] = &clireq.RelationShipNode{
				Key:       v.Guid,
				Name:      v.Name,
				Value:     baseValue,
				Category:  5,
				Visitable: 1,
			}
			links[lid] = &clireq.RelationShipLink{
				Source:    nid,
				Target:    tg,
				Visitable: 1,
			}
			lid++
			nid++
		}
	}

	for _, app := range dapp {
		sc := dappmap[string(app.ApplicationKey)]
		for _, v := range app.ApplicationDynamicStrategies {
			tg := dstmap[string(v.StrategyKey)]
			links[lid] = &clireq.RelationShipLink{
				Source:    sc,
				Target:    tg,
				Visitable: 1,
			}
			lid++
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data: map[string]interface{}{
			"type":       "force",
			"categories": categories,
			"nodes":      nodes,
			"links":      links,
		},
	}
}

func (r *WebServer) relationshipVisitable(req *clireq.RelationshipRequest) *clireq.Response {
	dst, err := r.supbResMgr.ListStrategies(types.DynamicStrategy)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	dapp, err := r.supbResMgr.ListApplications(types.DynamicApplication)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	keys := make(map[string]string)
	guids := make(map[string]string)
	switch req.Kind {
	case clireq.RelationshipCategoryStrategyConfig:
		keys[req.Key] = "ok"
		for _, v := range dst {
			if string(v.StrategyKeyParent) == req.Key {
				keys[string(v.StrategyKey)] = "ok"
			}

			for _, slave := range v.StrategySlaves {
				if slave.SlaveTypes == types.DynamicApplication {
					keys[string(slave.SlaveKey)] = "ok"
				}
			}

			for _, ct := range v.StrategyContainers {
				guids[ct.Guid] = "ok"
			}
		}

		for _, v := range dapp {
			if _, ok := keys[string(v.ApplicationKey)]; ok {
				for _, ct := range v.ApplicationContainers {
					guids[ct.Guid] = "ok"
				}
			}
		}
	case clireq.RelationshipCategoryStrategyInstance:
		keys[req.Key] = "ok"

		for _, v := range dst {
			if string(v.StrategyKey) == req.Key {
				for _, ct := range v.StrategyContainers {
					guids[ct.Guid] = "ok"
				}

				for _, slave := range v.StrategySlaves {
					if slave.SlaveTypes == types.DynamicApplication {
						keys[string(slave.SlaveKey)] = "ok"
					}
				}
				break
			}
		}

		for _, v := range dapp {
			if _, ok := keys[string(v.ApplicationKey)]; ok {
				for _, ct := range v.ApplicationContainers {
					guids[ct.Guid] = "ok"
				}
			}
		}
	case clireq.RelationshipCategoryStrategyContainers:
		guids[req.Key] = "ok"
	case clireq.RelationshipCategoryApplicationConfig:
		keys[req.Key] = "ok"
		for _, v := range dapp {
			if string(v.ApplicationKeyParent) == req.Key {
				keys[string(v.ApplicationKey)] = "ok"
				for _, ct := range v.ApplicationContainers {
					guids[ct.Guid] = "ok"
				}
			}
		}
	case clireq.RelationshipCategoryApplicationInstance:
		keys[req.Key] = "ok"
		for _, v := range dapp {
			if string(v.ApplicationKey) == req.Key {
				for _, ct := range v.ApplicationContainers {
					guids[ct.Guid] = "ok"
				}
			}
		}
	case clireq.RelationshipCategoryApplicationDevices:
		guids[req.Key] = "ok"
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkwon kind"),
			Data:    nil,
		}
	}

	//r.logger.Infof("keys [%v]  guids [%v]", keys, guids)

	resp := r.relationshipMap()
	for _, v := range resp.Data.(map[string]interface{})["nodes"].([]*clireq.RelationShipNode) {
		switch v.Category {
		case 0:
			if _, ok := keys[v.Key]; !ok {
				v.Visitable = 0
			}
		case 1:
			if _, ok := keys[v.Key]; !ok {
				v.Visitable = 0
			}
		case 2:
			if _, ok := guids[v.Key]; !ok {
				v.Visitable = 0
			}
		case 3:
			if _, ok := keys[v.Key]; !ok {
				v.Visitable = 0
			}
		case 4:
			if _, ok := keys[v.Key]; !ok {
				v.Visitable = 0
			}
		case 5:
			if _, ok := guids[v.Key]; !ok {
				v.Visitable = 0
			}
		}
	}
	return resp
}


func (r *WebServer) relationshipSubgraph(req *clireq.RelationshipRequest) *clireq.Response {

	categories := []map[string]interface{}{
		{
			"name": clireq.RelationshipCategoryStrategyConfig,
		},
		{
			"name": clireq.RelationshipCategoryStrategyInstance,
		},
		{
			"name": clireq.RelationshipCategoryStrategyContainers,
		},
		{
			"name": clireq.RelationshipCategoryApplicationConfig,
		},
		{
			"name": clireq.RelationshipCategoryApplicationInstance,
		},
		{
			"name": clireq.RelationshipCategoryApplicationDevices,
		},
	}

	dst, err := r.supbResMgr.ListStrategies(types.DynamicStrategy)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	dapp, err := r.supbResMgr.ListApplications(types.DynamicApplication)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	switch req.Kind {
	case clireq.RelationshipCategoryStrategyConfig:
		st, err := r.supbResMgr.GetStrategy(types.StaticStrategy, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}

		count := 1
		for _, v := range dst {
			if string(v.StrategyKeyParent) == req.Key {
				count++
				count += len(v.StrategyContainers)

				for _, a := range dapp {
					if _, ok := a.ApplicationDynamicStrategies[v.StrategyKey]; ok {
						count++

						count += len(a.ApplicationContainers)
					}
				}
			}
		}

		nodes, nid := make([]*clireq.RelationShipNode, count), 0
		links, lid := make([]*clireq.RelationShipLink, count-1), 0

		nodes[nid] = &clireq.RelationShipNode{
			Key:       string(st.StrategyKey),
			Name:      st.StrategyName,
			Value:     baseValue,
			Category:  0,
			Visitable: 1,
		}
		nid++

		dstmap := make(map[string]int)
		for _, v := range dst {
			if string(v.StrategyKeyParent) == req.Key {
				nodes[nid] = &clireq.RelationShipNode{
					Key:       string(v.StrategyKey),
					Name:      v.StrategyName,
					Value:     len(v.StrategyContainers) + len(v.StrategySlaves) + baseValue,
					Category:  1,
					Visitable: 1,
				}
				links[lid] = &clireq.RelationShipLink{
					Source:    nid,
					Target:    0,
					Visitable: 1,
				}
				nodes[0].Value++
				dstmap[string(v.StrategyKey)] = nid
				nid++
				lid++
			}
		}

		for _, v1 := range dst {
			if string(v1.StrategyKeyParent) == req.Key {
				tg := dstmap[string(v1.StrategyKey)]
				for _, v := range v1.StrategyContainers {
					nodes[nid] = &clireq.RelationShipNode{
						Key:       v.Guid,
						Name:      v.Name,
						Value:     baseValue,
						Category:  2,
						Visitable: 1,
					}
					links[lid] = &clireq.RelationShipLink{
						Source:    nid,
						Target:    tg,
						Visitable: 1,
					}
					lid++
					nid++
				}
			}

		}

		dappmap := make(map[string]int)
		for _, v1 := range dst {
			if string(v1.StrategyKeyParent) == req.Key {
				for _, v := range dapp {
					if _, ok := v.ApplicationDynamicStrategies[v1.StrategyKey]; ok {
						nodes[nid] = &clireq.RelationShipNode{
							Key:       string(v.ApplicationKey),
							Name:      v.ApplicationName,
							Value:     len(v.ApplicationContainers) + baseValue,
							Category:  4,
							Visitable: 1,
						}
						links[lid] = &clireq.RelationShipLink{
							Source:    nid,
							Target:    dstmap[string(v1.StrategyKey)],
							Visitable: 1,
						}
						tnid := nid

						dappmap[string(v.ApplicationKey)] = nid
						nodes[dstmap[string(v1.StrategyKey)]].Value++
						nid++
						lid++

						for _, ct := range v.ApplicationContainers {
							nodes[nid] = &clireq.RelationShipNode{
								Key:       ct.Guid,
								Name:      ct.Name,
								Value:     baseValue,
								Category:  5,
								Visitable: 1,
							}
							links[lid] = &clireq.RelationShipLink{
								Source:    nid,
								Target:    tnid,
								Visitable: 1,
							}
							lid++
							nid++
						}
					}
				}
			}
		}

		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data: map[string]interface{}{
				"type":       "force",
				"categories": categories,
				"nodes":      nodes,
				"links":      links,
			},
		}

	case clireq.RelationshipCategoryStrategyInstance:
		st, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}

		count := 1
		count += len(st.StrategyContainers)

		for _, a := range dapp {
			if _, ok := a.ApplicationDynamicStrategies[st.StrategyKey]; ok {
				count++
				count += len(a.ApplicationContainers)
			}
		}

		nodes, nid := make([]*clireq.RelationShipNode, count), 0
		links, lid := make([]*clireq.RelationShipLink, count-1), 0

		nodes[nid] = &clireq.RelationShipNode{
			Key:       string(st.StrategyKey),
			Name:      st.StrategyName,
			Value:     baseValue,
			Category:  1,
			Visitable: 1,
		}
		nid++

		for _, v := range st.StrategyContainers {
			nodes[nid] = &clireq.RelationShipNode{
				Key:       v.Guid,
				Name:      v.Name,
				Value:     baseValue,
				Category:  2,
				Visitable: 1,
			}
			links[lid] = &clireq.RelationShipLink{
				Source:    nid,
				Target:    0,
				Visitable: 1,
			}
			lid++
			nid++
		}

		for _, v := range dapp {
			if _, ok := v.ApplicationDynamicStrategies[st.StrategyKey]; ok {
				nodes[nid] = &clireq.RelationShipNode{
					Key:       string(v.ApplicationKey),
					Name:      v.ApplicationName,
					Value:     len(v.ApplicationContainers) + baseValue,
					Category:  4,
					Visitable: 1,
				}
				links[lid] = &clireq.RelationShipLink{
					Source:    nid,
					Target:    0,
					Visitable: 1,
				}
				tnid := nid

				nodes[0].Value++
				nid++
				lid++

				for _, ct := range v.ApplicationContainers {
					nodes[nid] = &clireq.RelationShipNode{
						Key:       ct.Guid,
						Name:      ct.Name,
						Value:     baseValue,
						Category:  5,
						Visitable: 1,
					}
					links[lid] = &clireq.RelationShipLink{
						Source:    nid,
						Target:    tnid,
						Visitable: 1,
					}
					lid++
					nid++
				}
			}
		}

		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data: map[string]interface{}{
				"type":       "force",
				"categories": categories,
				"nodes":      nodes,
				"links":      links,
			},
		}

	case clireq.RelationshipCategoryStrategyContainers:
		nodes := make([]*clireq.RelationShipNode, 1)
		var links []*clireq.RelationShipNode
		for _, v := range dst {
			for _, c := range v.StrategyContainers {
				if c.Guid == req.Key {
					nodes[0] = &clireq.RelationShipNode{
						Key:       c.Guid,
						Name:      c.Name,
						Value:     baseValue,
						Category:  2,
						Visitable: 1,
					}
					return &clireq.Response{
						Code:    http.StatusOK,
						Message: clireq.HttpSucceed,
						Data: map[string]interface{}{
							"type":       "force",
							"categories": categories,
							"nodes":      nodes,
							"links":      links,
						},
					}
				}
			}
		}

		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data: map[string]interface{}{
				"type":       "force",
				"categories": categories,
				"nodes":      nodes,
				"links":      links,
			},
		}

	case clireq.RelationshipCategoryApplicationConfig:
		app, err := r.supbResMgr.GetApplication(types.StaticApplication, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}

		count := 1

		for _, v := range dapp {
			if v.ApplicationKeyParent == app.ApplicationKey {
				count++

				count += len(v.ApplicationContainers)
			}
		}

		nodes, nid := make([]*clireq.RelationShipNode, count), 0
		links, lid := make([]*clireq.RelationShipLink, count-1), 0

		nodes[nid] = &clireq.RelationShipNode{
			Key:       string(app.ApplicationKey),
			Name:      app.ApplicationName,
			Value:     baseValue,
			Category:  3,
			Visitable: 1,
		}
		nid++

		for _, v := range dapp {
			if v.ApplicationKeyParent == app.ApplicationKey {
				nodes[nid] = &clireq.RelationShipNode{
					Key:       string(app.ApplicationKey),
					Name:      app.ApplicationName,
					Value:     baseValue,
					Category:  3,
					Visitable: 1,
				}

				links[lid] = &clireq.RelationShipLink{
					Source:    nid,
					Target:    0,
					Visitable: 1,
				}
				tnid := nid
				nid++
				lid++

				for _, c := range v.ApplicationContainers {
					nodes[nid] = &clireq.RelationShipNode{
						Key:       c.Guid,
						Name:      c.Name,
						Value:     baseValue,
						Category:  3,
						Visitable: 1,
					}

					links[lid] = &clireq.RelationShipLink{
						Source:    nid,
						Target:    tnid,
						Visitable: 1,
					}
					nid++
					lid++
				}
			}
		}

		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data: map[string]interface{}{
				"type":       "force",
				"categories": categories,
				"nodes":      nodes,
				"links":      links,
			},
		}
	case clireq.RelationshipCategoryApplicationInstance:
		app, err := r.supbResMgr.GetApplication(types.DynamicApplication, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}

		nodes, nid := make([]*clireq.RelationShipNode, len(app.ApplicationContainers)+1), 0
		links, lid := make([]*clireq.RelationShipLink, len(app.ApplicationContainers)), 0

		nodes[nid] = &clireq.RelationShipNode{
			Key:       string(app.ApplicationKey),
			Name:      app.ApplicationName,
			Value:     baseValue,
			Category:  4,
			Visitable: 1,
		}
		nid++

		for _, v := range app.ApplicationContainers {
			nodes[nid] = &clireq.RelationShipNode{
				Key:       v.Guid,
				Name:      v.Name,
				Value:     baseValue,
				Category:  5,
				Visitable: 1,
			}

			links[lid] = &clireq.RelationShipLink{
				Source:    nid,
				Target:    0,
				Visitable: 1,
			}

			nid++
			lid++
		}

		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data: map[string]interface{}{
				"type":       "force",
				"categories": categories,
				"nodes":      nodes,
				"links":      links,
			},
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkwon kind"),
			Data:    nil,
		}
	}
}

func (r *WebServer) relationshipDetail(req *clireq.RelationshipRequest) *clireq.Response {
	switch req.Kind {
	case clireq.RelationshipCategoryStrategyConfig:
		st, err := r.supbResMgr.GetStrategy(types.StaticStrategy, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data:    st,
		}
	case clireq.RelationshipCategoryStrategyInstance:
		st, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data:    st,
		}
	case clireq.RelationshipCategoryStrategyContainers:
		sts, err := r.supbResMgr.ListStrategies(types.DynamicStrategy)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
		for _, v := range sts {
			for _, ct := range v.StrategyContainers {
				if ct.Guid == req.Key {
					return &clireq.Response{
						Code:    http.StatusOK,
						Message: clireq.HttpSucceed,
						Data:    ct,
					}
				}
			}
		}

		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkown guid"),
			Data:    nil,
		}
	case clireq.RelationshipCategoryApplicationConfig:
		st, err := r.supbResMgr.GetApplication(types.StaticApplication, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data:    st,
		}
	case clireq.RelationshipCategoryApplicationInstance:
		st, err := r.supbResMgr.GetApplication(types.DynamicApplication, req.Key)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
		return &clireq.Response{
			Code:    http.StatusOK,
			Message: clireq.HttpSucceed,
			Data:    st,
		}
	case clireq.RelationshipCategoryApplicationDevices:
		sts, err := r.supbResMgr.ListApplications(types.DynamicApplication)
		if err != nil {
			return &clireq.Response{
				Code:    http.StatusBadRequest,
				Message: fmt.Sprintf("%v", err),
				Data:    nil,
			}
		}
		for _, v := range sts {
			for _, ct := range v.ApplicationContainers {
				if ct.Guid == req.Key {
					return &clireq.Response{
						Code:    http.StatusOK,
						Message: clireq.HttpSucceed,
						Data:    ct,
					}
				}
			}
		}

		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkown guid"),
			Data:    nil,
		}
	default:
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unkown kind"),
			Data:    nil,
		}
	}
}

func (r *WebServer) relationshipFilter(req *clireq.RelationshipRequest) *clireq.Response {
	resp := r.relationshipMap()
	for _, v := range resp.Data.(map[string]interface{})["nodes"].([]*clireq.RelationShipNode) {
		if _, ok := clireq.RelationShipType[v.Category]; ok {
			match := true
			for k, fv := range req.Filter {
				switch k {
				case "type":
					if clireq.RelationShipType[v.Category] != fv {
						match = false
					}
				case "keyword":
					if !strings.Contains(v.Name,fv) {
						match = false
					}
				case "status":
					stat,err := r.relationshipGetNodeStatus(v)
					if err!=nil || stat != fv {
						match = false
					}
				default:
					return &clireq.Response{
						Code:    http.StatusBadRequest,
						Message: fmt.Sprintf("filter type must be [type|status|keyword]"),
						Data:    nil,
					}
				}
				if !match{
					break
				}
			}

			if match {
				continue
			}
		}
		v.Visitable = 0
	}

	return resp
}

func (r *WebServer) relationshipFilterKind(req *clireq.RelationshipRequest) *clireq.Response {
	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data: []string{
			clireq.RelationshipCategoryStrategyConfig,
			clireq.RelationshipCategoryStrategyInstance,
			clireq.RelationshipCategoryStrategyContainers,
			clireq.RelationshipCategoryApplicationConfig,
			clireq.RelationshipCategoryApplicationInstance,
			clireq.RelationshipCategoryApplicationDevices,
		},
	}
}

func (r *WebServer) relationshipFilterStatus(req *clireq.RelationshipRequest) *clireq.Response {
	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    status.DynamicStatus(),
	}
}


func (r *WebServer) relationshipGetNodeStatus(node *clireq.RelationShipNode) (string,error) {
	switch clireq.RelationShipType[node.Category] {
	case clireq.RelationshipCategoryStrategyConfig:
		st, err := r.supbResMgr.GetStrategy(types.StaticStrategy, node.Key)
		if err != nil {
			return "",err
		}
		return st.StrategyStatus,nil
	case clireq.RelationshipCategoryStrategyInstance:
		st, err := r.supbResMgr.GetStrategy(types.DynamicStrategy, node.Key)
		if err != nil {
			return "",err
		}
		return st.StrategyStatus,nil
	case clireq.RelationshipCategoryStrategyContainers:
		sts, err := r.supbResMgr.ListStrategies(types.DynamicStrategy)
		if err != nil {
			return "",err
		}
		for _, v := range sts {
			for _, ct := range v.StrategyContainers {
				if ct.Guid == node.Key {
					return ct.Status,nil
				}
			}
		}

		return "",fmt.Errorf("unknow guid")
	case clireq.RelationshipCategoryApplicationConfig:
		st, err := r.supbResMgr.GetApplication(types.StaticApplication, node.Key)
		if err != nil {
			return "",err
		}
		return st.ApplicationStatus,nil
	case clireq.RelationshipCategoryApplicationInstance:
		st, err := r.supbResMgr.GetApplication(types.DynamicApplication, node.Key)
		if err != nil {
			return "",err
		}
		return st.ApplicationStatus,nil
	case clireq.RelationshipCategoryApplicationDevices:
		sts, err := r.supbResMgr.ListApplications(types.DynamicApplication)
		if err != nil {
			return "",err
		}
		for _, v := range sts {
			for _, ct := range v.ApplicationContainers {
				if ct.Guid == node.Key {
					return ct.Status,nil
				}
			}
		}

		return "",fmt.Errorf("unkown guid")
	default:
		return "",fmt.Errorf("unkown kind")
	}
}
