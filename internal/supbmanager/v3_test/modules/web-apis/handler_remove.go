package web_apis

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/web-apis/clireq"
	"io/ioutil"
	"net/http"
)

func (r *WebServer) remove(w http.ResponseWriter, rq *http.Request) {
	var
		req,resp = &clireq.RemoveRequest{},&clireq.Response{}


	r.logger.Infof("REMOVE")

	if buffers, err := ioutil.ReadAll(rq.Body);err != nil {
		r.responseErr(w,"read body %v",err)
		return
	}else{
		if err = json.Unmarshal(buffers, req); err != nil {
			r.responseErr(w,  "unmarshal %v", err)
			return
		}
	}
	switch req.Op {
	case clireq.RemoveStaticStrategy:
		resp = r.removeStaticStrategy(req.Key)
	case clireq.RemoveDynamicStrategy:
		resp = r.removeDynamicStrategy(req.Key)
	case clireq.RemoveStaticApplication:
		resp= r.removeStaticApplication(req.Key)
	case clireq.RemoveDynamicApplication:
		 resp= r.removeDynamicApplication(req.Key)
	default:
		resp = &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown type"),
			Data:    nil,
		}
	}

	bts, _ := json.Marshal(resp)
	if bt, err := w.Write(bts); err != nil {
		r.logger.Errorf("get function %v", err)
	} else {
		r.logger.Infof(fmt.Sprintf("LINK REPONSE (%vB)", bt))
	}
}

func (r *WebServer) removeStaticStrategy(strategyKey string) *clireq.Response{
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	st,err := r.supbResMgr.RemoveStaticStrategy(strategyKey)
	if err !=nil{
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data: st,
	}
}

func (r *WebServer) removeDynamicStrategy(strategyKey string) *clireq.Response{
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	st,err := r.supbResMgr.RemoveDynamicStrategy(strategyKey)
	if err !=nil{
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data: st,
	}
}

func (r *WebServer) removeStaticApplication(appKey string) *clireq.Response{
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	app,err := r.supbResMgr.RemoveStaticApplication(appKey)
	if err !=nil{
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data: app,
	}
}

func (r *WebServer) removeDynamicApplication(appKey string) *clireq.Response{
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	app,err := r.supbResMgr.RemoveDynamicApplication(appKey)
	if err !=nil{
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v",err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data: app,
	}
}

