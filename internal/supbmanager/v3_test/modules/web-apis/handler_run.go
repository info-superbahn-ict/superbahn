package web_apis

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/web-apis/clireq"
	"io/ioutil"
	"net/http"
)

func (r *WebServer) run(w http.ResponseWriter, rq *http.Request) {
	var req,resp = &clireq.RunRequest{},&clireq.Response{}

	r.logger.Infof("RUN")

	if buffers, err := ioutil.ReadAll(rq.Body);err != nil {
		r.responseErr(w,"read body %v",err)
		return
	}else{
		if err = json.Unmarshal(buffers, req); err != nil {
			r.responseErr(w,  "unmarshal %v", err)
			return
		}
	}
	switch req.Op {
	case clireq.RunStaticStrategy:
		resp = r.runStrategy(req)
	case clireq.RunStaticApplication:
		 resp = r.runApplication(req)
	default:
		resp = &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown type"),
			Data:    nil,
		}
	}

	bts, _ := json.Marshal(resp)
	if bt, err := w.Write(bts); err != nil {
		r.logger.Errorf("get function %v", err)
	} else {
		r.logger.Infof(fmt.Sprintf("LINK REPONSE (%vB)", bt))
	}
}


func (r *WebServer) runStrategy(req *clireq.RunRequest) *clireq.Response{
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	st,err := r.supbResMgr.RunStaticStrategies(req.Key,req.Name,req.LinksKey)
	if err !=nil{
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	for _,ct := range st.StrategyContainers {
		r.supbLogClr.AddGuid(ct.Guid)
		r.supbMetrics.AddGuid(ct.Guid)
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data: st,
	}
}

func (r *WebServer) runApplication(req *clireq.RunRequest) *clireq.Response{
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	app,err := r.supbResMgr.RunStaticApplication(req.Key,req.Name,req.LinksKey)
	if err !=nil{
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	for _,ct := range app.ApplicationContainers {
		r.supbLogClr.AddGuid(ct.Guid)
		r.supbMetrics.AddGuid(ct.Guid)
	}


	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data: app,
	}
}