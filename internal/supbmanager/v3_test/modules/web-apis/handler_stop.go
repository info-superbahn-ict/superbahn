package web_apis

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/web-apis/clireq"
	"io/ioutil"
	"net/http"
)

func (r *WebServer) stop(w http.ResponseWriter, rq *http.Request) {
	var
		req, resp = &clireq.StopRequest{}, &clireq.Response{}

	r.logger.Infof("GET")

	if buffers, err := ioutil.ReadAll(rq.Body); err != nil {
		r.responseErr(w, "read body %v", err)
		return
	} else {
		if err = json.Unmarshal(buffers, req); err != nil {
			r.responseErr(w, "unmarshal %v", err)
			return
		}
	}
	switch req.Op {
	case clireq.StopDynamicStrategy:
		resp = r.stopStrategy(req.Key)

	case clireq.StopDynamicApplication:
		resp = r.stopApplication(req.Key)
	default:
		resp = &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown type"),
			Data:    nil,
		}
	}

	bts, _ := json.Marshal(resp)
	if bt, err := w.Write(bts); err != nil {
		r.logger.Errorf("get function %v", err)
	} else {
		r.logger.Infof(fmt.Sprintf("LINK REPONSE (%vB)", bt))
	}
}

func (r *WebServer) stopStrategy(strategyKey string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	st, err := r.supbResMgr.StopDynamicStrategy(strategyKey)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    st,
	}
}

func (r *WebServer) stopApplication(appKey string) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	app, err := r.supbResMgr.StopDynamicApplication(appKey)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    app,
	}
}
