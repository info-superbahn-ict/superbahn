package web_apis

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/web-apis/clireq"
	m2 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications/model"
	m1 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"io/ioutil"
	"net/http"
)

func (r *WebServer) update(w http.ResponseWriter, rq *http.Request) {
	var
		req, resp = &clireq.UpdateRequest{}, &clireq.Response{}

	r.logger.Infof("UPDATE")

	if buffers, err := ioutil.ReadAll(rq.Body); err != nil {
		r.responseErr(w, "read body %v", err)
		return
	} else {
		if err = json.Unmarshal(buffers, req); err != nil {
			r.responseErr(w, "unmarshal %v", err)
			return
		}
	}
	switch req.Op {
	// todo 策略 更新
	case clireq.UpdateStaticStrategy:
		resp = r.updateStrategy(types.StaticStrategy, req.Data)
	case clireq.UpdateDynamicStrategy:
		resp = r.updateStrategy(types.DynamicStrategy, req.Data)

	// todo 应用 更新
	case clireq.UpdateStaticApplication:
		resp = r.updateApplication(types.StaticApplication, req.Data)
	case clireq.UpdateDynamicApplication:
		resp = r.updateApplication(types.DynamicApplication, req.Data)

	// todo 策略 添加
	case clireq.AddStrategy:
		resp = r.addStrategy(req.Data)

	// todo 应用 添加
	case clireq.AddApplication:
		resp = r.addApplication(req.Data)
	default:
		resp = &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unknown type"),
			Data:    nil,
		}
	}

	bts, _ := json.Marshal(resp)
	if bt, err := w.Write(bts); err != nil {
		r.logger.Errorf("get function %v", err)
	} else {
		r.logger.Infof(fmt.Sprintf("UPDATE REPONSE (%vB)", bt))
	}
}

func (r *WebServer) updateStrategy(types string, data interface{}) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	stUpdate := &m1.SupbStrategyBaseInfo{}

	bts, _ := json.Marshal(data)
	if err := json.Unmarshal(bts, stUpdate); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unmarshall data %v", err),
			Data:    nil,
		}
	}

	st,err := r.supbResMgr.GetStrategy(types,string(stUpdate.StrategyKey))
	if err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	if err = json.Unmarshal(bts, st); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unmarshall data %v", err),
			Data:    nil,
		}
	}

	if err = m1.MarshallCompose(st);err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}


	stPut,err := r.supbResMgr.PutStrategy(types, st)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("update %v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    stPut,
	}
}

func (r *WebServer) updateApplication(types string, data interface{}) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}



	appUpdate := &m2.SupbApplicationBaseInfo{}
	bts, _ := json.Marshal(data)
	if err := json.Unmarshal(bts, appUpdate); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unmarshall data %v", err),
			Data:    nil,
		}
	}

	app,err := r.supbResMgr.GetApplication(types,string(appUpdate.ApplicationKey))
	if err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	if err = json.Unmarshal(bts, app); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unmarshall data %v", err),
			Data:    nil,
		}
	}

	if err = m2.MarshallCompose(app);err !=nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("%v", err),
			Data:    nil,
		}
	}

	appPut,err := r.supbResMgr.PutApplication(types, app)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("update %v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    appPut,
	}
}

func (r *WebServer) addStrategy(data interface{}) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}


	st := &m1.SupbStrategyBaseInfo{}

	bts, _ := json.Marshal(data)
	if err := json.Unmarshal(bts, st); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unmarshall data %v", err),
			Data:    nil,
		}
	}

	stAdded,err := r.supbResMgr.AddStrategy(st)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("update %v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    stAdded,
	}
}

func (r *WebServer) addApplication(data interface{}) *clireq.Response {
	if r.supbResMgr == nil {
		return &clireq.Response{
			Code:    http.StatusInternalServerError,
			Message: "resource manager is nil",
			Data:    nil,
		}
	}

	app := &m2.SupbApplicationBaseInfo{}

	bts, _ := json.Marshal(data)
	if err := json.Unmarshal(bts, app); err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("unmarshall data %v", err),
			Data:    nil,
		}
	}

	appAdded, err := r.supbResMgr.AddApplication(app)
	if err != nil {
		return &clireq.Response{
			Code:    http.StatusBadRequest,
			Message: fmt.Sprintf("update %v", err),
			Data:    nil,
		}
	}

	return &clireq.Response{
		Code:    http.StatusOK,
		Message: clireq.HttpSucceed,
		Data:    appAdded,
	}
}
