package web_apis

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/web-apis/clireq"
	"net/http"
)

func (r *WebServer)responseErr(w http.ResponseWriter, format string, err error) {
	msg := fmt.Sprintf(format, err)

	resp := &clireq.Response{
		Code: http.StatusBadRequest,
		Message: msg,
		Data: nil,
	}

	bts, _ := json.Marshal(resp)

	if _, err := w.Write(bts); err != nil {
		r.logger.Errorf("write response %v", err)
	}
	r.logger.Errorf(format,err)
}
