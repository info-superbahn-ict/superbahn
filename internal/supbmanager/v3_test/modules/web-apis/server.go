package web_apis

import (
	"context"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbindex"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supblog"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbmetric"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbstrategy"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbtrace"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/rs/cors"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"net/http"
	"time"
)

const (
	routePrefix   = ".route.prefix"
	routeHostPort = ".route.url"
	handleTimeOut = ".handle.timeOut"
)

type WebServer struct {
	ctx    context.Context
	prefix string

	routePrefix   string
	routeUrl      string
	handleTimeOut int64

	logger log.Logger

	supbResMgr *supbres.SupbResources
	supbLogClr *supblog.SupbLog
	supbMetrics  *supbmetric.SupbMetrics
	supbStrategy *supbstrategy.SupbStrategy
	supbIndex  *supbindex.Index
	supbTrace *supbtrace.SupbTrace
}

func NewServer(ctx context.Context, prefix string) model.Module {
	return &WebServer{
		ctx:    ctx,
		prefix: prefix,
		logger: log.NewLogger(prefix),
	}
}

func (r *WebServer) InitFlags(flags *pflag.FlagSet) {
	flags.String(r.prefix+routePrefix, "/web/apis", "webserver route")
	flags.String(r.prefix+routeHostPort, "0.0.0.0:10000", "webserver url")
	flags.Int64(r.prefix+handleTimeOut, 5, "webserver url")
}

func (r *WebServer) ViperConfig(viper *viper.Viper) {

}

func (r *WebServer) InitViper(viper *viper.Viper) {
	r.routePrefix = viper.GetString(r.prefix + routePrefix)
	r.routeUrl = viper.GetString(r.prefix + routeHostPort)
	r.handleTimeOut = viper.GetInt64(r.prefix + handleTimeOut)
}

func (r *WebServer) OptionConfig(opts ...model.Option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *WebServer) Initialize(opts ...model.Option) {
	for _, apply := range opts {
		apply(r)
	}

	if r.supbResMgr==nil {
		r.logger.Fatalf("supb resource manager is nil")
	}

	r.logger.Infof("%v Initialize",r.prefix)
	go r.Run()
}

func (r *WebServer) Close() error {
	r.logger.Infof("%v closed",r.prefix)
	return nil
}


func (r *WebServer) Run() {
	handle := http.NewServeMux()
	handle.HandleFunc(r.routePrefix+"/list", r.list)
	handle.HandleFunc(r.routePrefix+"/get", r.get)
	handle.HandleFunc(r.routePrefix+"/update", r.update)
	handle.HandleFunc(r.routePrefix+"/run", r.run)
	handle.HandleFunc(r.routePrefix+"/remove", r.remove)
	handle.HandleFunc(r.routePrefix+"/restart", r.restart)
	handle.HandleFunc(r.routePrefix+"/stop", r.stop)
	handle.HandleFunc(r.routePrefix+"/link", r.link)
	handle.HandleFunc(r.routePrefix+"/metrics", r.metrics)
	handle.HandleFunc(r.routePrefix+"/strategy", r.strategy)
	handle.HandleFunc(r.routePrefix+"/application", r.application)
	handle.HandleFunc(r.routePrefix+"/relationship", r.relationship)
	handle.HandleFunc(r.routePrefix+"/index", r.index)

	handler := cors.Default().Handler(handle)

	server := &http.Server{
		Addr:        r.routeUrl,
		ReadTimeout: time.Duration(r.handleTimeOut) * time.Second,
		Handler:     handler,
	}

	go func() {
		if err := server.ListenAndServe(); err != nil {
			r.logger.Errorf("server %v",err)
		}
		r.logger.Infof("server finished")
	}()

	r.logger.Infof("%v start running...", r.prefix)
	<-r.ctx.Done()
}


