package jwt

import (
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/e"
	"github.com/gin-gonic/gin"
	"net/http"
)

func JWT() gin.HandlerFunc {
	return func(c *gin.Context) {
		var code int
		var data interface{}

		code = e.SUCCESS
		// TODO 暂时没有token校验，以后添加
		//token := c.Query("token")
		//
		//if token == "" {
		//	code = e.INVALID_PARAMS
		//} else {
		//	_,err := sync.ParseToken(token)
		//	if err != nil {
		//		switch err.(*jwt.ValidationError).Errors {
		//		case jwt.ValidationErrorExpired:
		//			code = e.ERROR_AUTH_CHECK_TOKEN_TIMEOUT
		//		default:
		//			code = e.ERROR_AUTH_CHECK_TOKEN_FAIL
		//		}
		//	}
		//}

		if code != e.SUCCESS {
			c.JSON(http.StatusUnauthorized, gin.H{
				"code": code,
				"msg":  e.GetMsg(code),
				"data": data,
			})

			c.Abort()
			return
		}

		c.Next()
	}
}
