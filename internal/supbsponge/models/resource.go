package models

import (
	"github.com/jinzhu/gorm"
)

type Resource struct {
	ID          int    `json:"id"`
	GuId        string `json:"guId"`
	Description string `json:"description"`
	Status      int    `json:"status"`
	Area        string `json:"area"`
	PreNode     string `json:"preNode"`
	SubNode     string `json:"subNode"`
	OType       int    `json:"oType"`
	Utilization int    `json:"utilization"`
	CreateTime  string `json:"createTime"`
	UpdateTime  string `json:"updateTime"`
	IsMonitor   int    `json:"isMonitor"`
	IsControl   int    `json:"isControl"`
	//SumNode     int    `json:"sumNode"`
	//AliveNode   int    `json:"aliveNode"`
	//AutoRemove  int    `json:"autoRemove"`
}

type ResourceDynamic struct {
	ID          int    `json:"id"`
	GuId        string `json:"guId"`
	Description string `json:"description"`
	Status      int    `json:"status"`
	Area        string `json:"area"`
	PreNode     string `json:"preNode"`
	SubNode     string `json:"subNode"`
	OType       int    `json:"oType"`
	Utilization int    `json:"utilization"`
	CreateTime  string `json:"createTime"`
	UpdateTime  string `json:"updateTime"`
	IsMonitor   int    `json:"isMonitor"`
	IsControl   int    `json:"isControl"`
	SumNode     int    `json:"sumNode"`
	AliveNode   int    `json:"aliveNode"`
	//AutoRemove  int    `json:"autoRemove"`
}

type ResourceMetric struct {
	ID          int     `json:"id"`
	GuId        string  `json:"guId"`
	Description string  `json:"description"`
	Status      int     `json:"status"`
	Area        string  `json:"area"`
	PreNode     string  `json:"preNode"`
	SubNode     string  `json:"subNode"`
	OType       int     `json:"oType"`
	Utilization int     `json:"utilization"`
	CreateTime  string  `json:"createTime"`
	UpdateTime  string  `json:"updateTime"`
	IsMonitor   int     `json:"isMonitor"`
	IsControl   int     `json:"isControl"`
	CpuSet      string  `json:"cpuSet"`
	CpuRatio    float64 `json:"cpuRatio"`
}

type Tag struct {
	ID         int    `json:"id"`
	ResourceId int    `json:"resourceId"`
	GuId       string `json:"guId"`
	TagKey     string `json:"tagKey"`
	TagType    string `json:"tagType"`
	TagValue   string `json:"tagValue"`
	CreateTime string `json:"createTime"`
	UpdateTime string `json:"updateTime"`
}

type TypeCount struct {
	OType int `json:"oType"`
	Count int `json:"count"`
}

type UtilizationInfo struct {
	Name  string `json:"name"`
	Value int    `json:"value"`
}

type CategoryName struct {
	Name string `json:"name"`
}

type CityResource struct {
	Name  string `json:"name"`
	Value int    `json:"value"`
}

type Metric struct {
	ID       int     `json:"id"`
	GuId     string  `json:"guId"`
	CpuSet   string  `json:"cpuSet"`
	CpuRatio float64 `json:"cpuRatio"`
}

// 检测资源是否已存在
func ExistResourceByGuId(guId string) (bool, error) {
	var resource Resource
	err := db.Select("gu_id").Where("gu_id = ? ", guId).First(&resource).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}

	if resource.GuId != "" {
		return true, nil
	}

	return false, nil
}

func CheckMetricExist(guId string) (bool, error) {
	var metric Metric
	err := db.Table("metric_info").Select("gu_id").Where("gu_id = ?", guId).First(&metric).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}

	if metric.GuId != "" {
		return true, nil
	}

	return false, nil
}

// 添加资源
func AddResource(data map[string]interface{}) error {
	tx := db.Begin()

	if err := tx.Error; err != nil {
		return err
	}

	resource := Resource{
		GuId:        data["gu_id"].(string),
		Description: data["description"].(string),
		Status:      data["status"].(int),
		Area:        data["area"].(string),
		PreNode:     data["pre_node"].(string),
		OType:       data["o_type"].(int),
		Utilization: data["utilization"].(int),
		IsMonitor:   data["is_monitor"].(int),
		IsControl:   data["is_control"].(int),
		//AutoRemove:  data["auto_remove"].(int),
		CreateTime: data["create_time"].(string),
		UpdateTime: data["update_time"].(string),
	}

	err := tx.Debug().Create(&resource).Error

	if err != nil {
		tx.Rollback()
		return err
	}

	tags := data["tags"].([]Tag)

	if len(tags) > 0 {
		for _, tag := range tags {
			tag.ResourceId = resource.ID
			err := tx.Debug().Create(&tag).Error
			if err != nil {
				tx.Rollback()
				return err
			}
		}
	}

	err = tx.Commit().Error
	if err != nil {
		tx.Rollback()
		return err
	}

	return nil
}

//添加标签
func AddTags(data map[string]interface{}) error {
	tag := Tag{
		ResourceId: data["resource_id"].(int),
		GuId:       data["gu_id"].(string),
		TagKey:     data["tag_key"].(string),
		TagType:    data["tag_type"].(string),
		TagValue:   data["tag_value"].(string),
		CreateTime: data["create_time"].(string),
		UpdateTime: data["update_time"].(string),
	}

	err := db.Create(&tag).Error

	if err != nil {
		return err
	}

	return nil
}

//更新标签
func UpdateTags(data map[string]interface{}) error {
	tag := Tag{
		ID:         data["id"].(int),
		TagValue:   data["tag_value"].(string),
		UpdateTime: data["update_time"].(string),
	}
	if err := db.Model(&Tag{}).Where("id = ?", tag.ID).Update(data).Error; err != nil {
		return err
	}
	return nil
}

// 更新资源子节点
func UpdateSubNode(guId string, data interface{}) error {
	if err := db.Model(&Resource{}).Where("gu_id = ?", guId).Updates(data).Error; err != nil {
		return err
	}
	return nil
}

func UpdateMetricInfo(guId string, data interface{}) error {
	if err := db.Model(&Metric{}).Where("gu_id = ?", guId).Updates(data).Error; err != nil {
		return err
	}
	return nil
}

func InsertMetricInfo(data map[string]interface{}) error {
	metric := Metric{
		GuId:     data["guId"].(string),
		CpuSet:   data["cpuSet"].(string),
		CpuRatio: data["cpuRatio"].(float64),
	}
	err := db.Create(&metric).Error

	if err != nil {
		return err
	}

	return nil
}

// 获取资源列表数目
func GetResourceTotal(maps interface{}) (int, error) {
	var count int
	if err := db.Model(&Resource{}).Where(maps).Count(&count).Error; err != nil {
		return 0, err
	}
	return count, nil
}

// 获取资源列表
func GetResources(pageNum int, pageSize int, maps interface{}) ([]*Resource, error) {
	var (
		resources []*Resource
		err       error
	)

	if pageSize > 0 || pageNum > 0 {
		err = db.Where(maps).Limit(pageSize).Find(&resources).Error
		//err = db.Where(maps).Find(&resources).Offset(pageNum).Limit(pageSize).Error
	} else {
		err = db.Where(maps).Find(&resources).Error
	}

	if err != nil || err == gorm.ErrRecordNotFound {
		return nil, err
	}
	return resources, nil
}

// 查看资源（根据GUID）
func GetResource(guId string) (*Resource, error) {
	var resource Resource
	err := db.Where("gu_id = ?", guId).First(&resource).Error
	if err != nil || err == gorm.ErrRecordNotFound {
		return nil, err
	}

	err = db.Model(&resource).Error
	if err != nil || err == gorm.ErrRecordNotFound {
		return nil, err
	}

	return &resource, nil
}

// 查询资源（模糊查询）
func SearchResources(str1 string, str2 string) ([]*Resource, error) {
	var (
		resources []*Resource
		err       error
	)
	queryStr1 := "%" + str1 + "%"
	queryStr2 := "%" + str2 + "%"
	err = db.Where("description like ? or area like ?", queryStr1, queryStr2).Find(&resources).Error
	if err != nil || err == gorm.ErrRecordNotFound {
		return nil, err
	}
	return resources, nil
}

// 删除指定资源--通过guIds
func DeleteResources(guIds []string) error {
	//if err := db.Where("status = ?", status).Delete(Resource{}).Error; err != nil {
	//	return err
	//}
	if err := db.Where("gu_id in (?)", guIds).Delete(Resource{}).Error; err != nil {
		return err
	}
	return nil
}

// 删除指定资源
func DeleteResource(guId string) error {
	if err := db.Where("gu_id = ?", guId).Delete(Resource{}).Error; err != nil {
		return err
	}
	return nil
}

//// 断开设备
//func DisConnect(guId string, data interface{}) error {
//	if err := db.Model(&Resource{}).Where("gu_id = ?", guId).Updates(data).Error; err != nil {
//		return err
//	}
//
//	return nil
//}
//
//// 重连设备
//func ReConnect(guId string, data interface{}) error {
//	if err := db.Model(&Resource{}).Where("gu_id = ?", guId).Updates(data).Error; err != nil {
//		return err
//	}
//	return nil
//}

// 更新状态
func UpdateStatus(guId string, data interface{}) error {
	if err := db.Model(&Resource{}).Where("gu_id = ?", guId).Updates(data).Error; err != nil {
		return err
	}

	return nil
}

// 查询对应设备类型数量
func SearchResourcesNum(maps map[string]int) ([]TypeCount, error) {
	var (
		countList []TypeCount
		err       error
	)
	status := maps["status"]
	oType := maps["o_type"]
	if oType == 0 {
		err = db.Table("resource_info").Select("o_type,count(*) as count").Where("status = ?", status).Group("o_type").Order("o_type").Find(&countList).Error
	} else {
		err = db.Table("resource_info").Select("o_type,count(*) as count").Where("status = ? and o_type = ?", status, oType).Find(&countList).Error
	}

	return countList, err
}

func GetUtilizationInfos(queryStrs []string) ([]UtilizationInfo, error) {
	var (
		utilizationInfos []UtilizationInfo
		err              error
	)

	for _, queryStr := range queryStrs {
		var utilizationInfo UtilizationInfo
		err = db.Table("resource_info").Select(queryStr).Find(&utilizationInfo).Error
		if err != nil || err == gorm.ErrRecordNotFound {
			return nil, err
		}
		utilizationInfos = append(utilizationInfos, utilizationInfo)
	}

	return utilizationInfos, nil
}

func GetTagGroupInfos(maps map[string]interface{}) ([]*ResourceMetric, error) {
	var (
		resources []*ResourceMetric
		err       error
	)

	oType, _ := maps["oType"].(int)
	status := maps["status"].(int)
	tagKey := maps["tagKey"].(string)
	tagValue := maps["tagValue"].(string)

	err = db.Table("resource_info").Select("resource_info.*,metric_info.cpu_set,metric_info.cpu_ratio").Joins("JOIN tag_info on resource_info.gu_id = tag_info.gu_id").Joins("LEFT JOIN metric_info on resource_info.gu_id = metric_info.gu_id").Where("resource_info.o_type = ? and resource_info.status = ? and tag_info.tag_key = ? and tag_info.tag_value = ?", oType, status, tagKey, tagValue).Find(&resources).Error

	return resources, err
}

func GetTagInfos() ([]*Tag, error) {
	var (
		tagInfos []*Tag
		err      error
	)

	err = db.Select("tag_info.*").Find(&tagInfos).Error

	return tagInfos, err
}

func GetDistinctTagInfos() ([]*Tag, error) {
	var (
		tagInfos []*Tag
		err      error
	)
	err = db.Table("tag_info").Select("tag_info.*").Where("tag_key = ?", "server.appkey").Group("gu_id").Find(&tagInfos).Error
	return tagInfos, err
}

func GetDistinctCategoryNames() ([]*CategoryName, error) {
	var (
		cNames []*CategoryName
		err    error
	)
	err = db.Table("tag_info").Select("tag_info.tag_value as name").Where("tag_key = ?", "server.appkey").Group("tag_value").Find(&cNames).Error
	return cNames, err
}

func GetTagInfosByGuId(guId string) ([]*Tag, error) {
	var (
		tags []*Tag
		err  error
	)
	err = db.Table("tag_info").Select("tag_info.*").Where("gu_id = ?", guId).Find(&tags).Error
	return tags, err
}

func GetResourceTagByKey(guid string, key string) (*Tag, error) {
	var (
		tag Tag
		err error
	)
	err = db.Table("tag_info").Select("tag_info.*").Where("gu_id = ? and tag_key = ?", guid, key).First(&tag).Error
	return &tag, err
}

func GetResourcesByCity() ([]CityResource, error) {
	var (
		cityResources []CityResource
		err           error
	)

	err = db.Table("resource_info").Select("area as name,count(*) as value").Group("area").Find(&cityResources).Error

	return cityResources, err
}

func DeleteTagInfos(guId string) error {
	if err := db.Table("tag_info").Where("gu_id = ?", guId).Delete(Tag{}).Error; err != nil {
		return err
	}
	return nil
}
