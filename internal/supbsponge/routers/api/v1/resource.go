package v1

import (
	"C"
	"encoding/json"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbsponge/service/resource_service"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/app"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/e"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/gredis"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/logging"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/setting"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/util"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/unknwon/com"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func AddResource(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form AddResourceForm
	)

	//校验合法性
	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}
	md5Str := (util.EncodeMD5(form.Description))[0:6]

	createTime := time.Now().Format("2006-01-02 15:04:05")
	updateTime := time.Now().Format("2006-01-02 15:04:05")

	guId := ""

	// TODO 此处应该根据不同的otype有不同的guId生成规则，后续补上
	//if form.OType == 1 {
	di := util.DeviceInfo{
		ManufacturerId: "01",
		ProductId:      md5Str,
		ResourceType:   0,
		ResourceId:     0,
	}

	//生成guId
	guId = util.GenerateDeviceInfo(di)
	//}

	utilization := rand.Intn(100)

	resourceService := resource_service.Resource{
		GuId:        guId,
		Description: form.Description,
		Status:      form.Status,
		Area:        form.Area,
		PreNode:     form.DependNode,
		OType:       form.OType,
		//AutoRemove:  form.AutoRemove,
		IsControl:   form.IsControl,
		IsMonitor:   form.IsMonitor,
		Utilization: utilization,
		CreateTime:  createTime,
		UpdateTime:  updateTime,
		Tags:        form.Tags,
	}

	// 判断guId是否已存在
	exists, err := resourceService.ExistByGuId()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EXIST_RESOURCE_FAIL, nil)
		return
	}
	if exists {
		appG.Response(http.StatusOK, e.ERROR_EXIST_RESOURCE, nil)
		return
	}

	err = resourceService.Add()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_ADD_RESOURCE_FAIL, nil)
		return
	}

	// 如果依赖节点不为空，在需要修改依赖节点下的子节点信息
	if form.DependNode != "" {
		preResourceService := resource_service.Resource{
			GuId: form.DependNode,
		}

		// 首先判断依赖节点是否存在
		exists, _ = preResourceService.ExistByGuId()
		if exists {
			// 查询出依赖节点下的子节点信息
			preResouce, err := preResourceService.GetDeviceInfo()
			if err != nil {
				appG.Response(http.StatusInternalServerError, e.ERROR_ADD_RESOURCE_FAIL, nil)
				return
			}
			var subNodes []string
			if strings.Contains(preResouce.SubNode, "|") {
				subNodes = strings.Split(preResouce.SubNode, "|")
			} else if preResouce.SubNode != "" {
				subNodes = append(subNodes, preResouce.SubNode)
			}
			subNodes = append(subNodes, guId)

			preResourceService := resource_service.Resource{
				GuId:    form.DependNode,
				SubNode: strings.Join(subNodes, "|"),
			}

			err = preResourceService.UpdateSubNode()
		}
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"guId": guId,
	})
}

type AddResourceForm struct {
	Description string `form:"description" valid:"Required;MaxSize(100)"`
	Status      int    `form:"status" valid:"Range(0,1)"`
	Area        string `form:"area" valid:"Required;MaxSize(100)"`
	DependNode  string `form:"dependNode" valid:"Required;MaxSize(100)"`
	OType       int    `form:"dependNode" valid:"Range(0,1)"`
	IsMonitor   int    `form:"isMonitor"`
	IsControl   int    `form:"isControl"`
	//AutoRemove  int                    `form:"autoRemove"`
	Utilization int                    `form:"utilization" valid:"Range(0,100)"`
	Tags        []resource_service.Tag `form:"tags"`
}

func GetDeviceInfo(c *gin.Context) {
	appG := app.Gin{C: c}
	guId := ""
	if arg := c.Query("guId"); arg != "" {
		guId = com.StrTo(arg).String()
	}

	//oType := -1
	//if arg := c.Query("oType"); arg != "" {
	//	oType = com.StrTo(arg).MustInt()
	//}

	resourceService := resource_service.Resource{
		GuId:  guId,
		//OType: oType,
	}

	resource, err := resourceService.GetDeviceInfo()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_NOT_EXIST_RESOURCE, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"deviceInfo": resource,
	})
}

func GetDeviceInfos(c *gin.Context) {
	appG := app.Gin{C: c}
	oType := -1
	if arg := c.Query("oType"); arg != "" {
		oType = com.StrTo(arg).MustInt()
	}

	//if oType != 1 {
	//	appG.Response(http.StatusInternalServerError, e.INVALID_PARAMS, nil)
	//}

	status := -1
	if arg := c.Query("status"); arg != "" {
		status = com.StrTo(arg).MustInt()
	}

	resourceService := resource_service.Resource{
		OType:    oType,
		Status:   status,
		PageNum:  util.GetPage(c),
		PageSize: setting.AppSetting.PageSize,
	}

	resources, err := resourceService.GetDeviceInfos()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_RESOURCES_FAIL, nil)
		return
	}

	count, err := resourceService.Count()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_COUNT_RESOURCE_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": resources,
		"count": count,
	})
}

var upGrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

//心跳功能（webSocket请求ping，返回pong）
func RenewalResource(c *gin.Context) {
	appG := app.Gin{C: c}

	//升级get请求为webSocket协议
	ws, err := upGrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, nil)
		return
	}

	defer ws.Close()

	for {
		//读取ws中的数据
		mt, message, err := ws.ReadMessage()
		if err != nil {
			break
		}
		guId := string(message)

		resourceService := resource_service.Resource{GuId: guId}
		exists, err := resourceService.ExistByGuId()
		if err != nil {
			break
		}
		if !exists {
			break
		}
		message = []byte("success")
		//写入ws数据
		err = ws.WriteMessage(mt, message)
		if err != nil {
			break
		}
	}
}

func DeleteResource(c *gin.Context) {
	appG := app.Gin{C: c}

	guId := com.StrTo(c.Param("guId")).String()

	resourceService := resource_service.Resource{GuId: guId}
	exists, err := resourceService.ExistByGuId()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EXIST_RESOURCE_FAIL, nil)
		return
	}
	if !exists {
		appG.Response(http.StatusOK, e.ERROR_NOT_EXIST_RESOURCE, nil)
		return
	}

	err = resourceService.DeleteResource()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_DELETE_RESOURCE_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func DeleteResources(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form DeleteResourcesForm
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}

	resourceService := resource_service.Resource{
		GuIds: form.GuIds,
	}

	err := resourceService.DeleteResources()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_DELETE_RESOURCE_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

type DeleteResourcesForm struct {
	GuIds []string `form:"guIds" valid:"Required;MaxSize(1000)"`
}

func SearchResources(c *gin.Context) {
	appG := app.Gin{C: c}
	var keyWord string
	if arg := c.Query("keyWord"); arg != "" {
		keyWord = com.StrTo(arg).String()
	}

	resourceService := resource_service.Resource{Description: keyWord, Area: keyWord}

	resources, err := resourceService.SearchResources()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_RESOURCES_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"lists": resources,
	})
}

type ConnectForm struct {
	GuId string `form:"guId" valid:"Required;MaxSize(100)"`
}

func DisConnect(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = ConnectForm{GuId: com.StrTo(c.Param("guId")).String()}
	)

	resourceService := resource_service.Resource{
		GuId:   form.GuId,
		Status: 2,
	}

	exists, err := resourceService.ExistByGuId()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EXIST_RESOURCE_FAIL, nil)
		return
	}
	if !exists {
		appG.Response(http.StatusOK, e.ERROR_NOT_EXIST_RESOURCE, nil)
		return
	}

	err = resourceService.UpdateStatus()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_DISCONNECT_RESOURCE_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func ReConnect(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = ConnectForm{GuId: com.StrTo(c.Param("guId")).String()}
	)

	resourceService := resource_service.Resource{
		GuId:   form.GuId,
		Status: 1,
	}

	exists, err := resourceService.ExistByGuId()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EXIST_RESOURCE_FAIL, nil)
		return
	}
	if !exists {
		appG.Response(http.StatusOK, e.ERROR_NOT_EXIST_RESOURCE, nil)
		return
	}

	err = resourceService.UpdateStatus()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_DISCONNECT_RESOURCE_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func SSet(c *gin.Context) {
	appG := app.Gin{C: c}
	var kv KVResource
	httpCode, errCode := app.BindAndValid(c, &kv)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}
	if err := gredis.Set(kv.Key, kv.Value, 3600); err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_DISCONNECT_RESOURCE_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

type KVResource struct {
	Key   string `form:"key" json:"key" binding:"required"`
	Value string `form:"value" json:"value" binding:"required"`
}

func SGet(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		val  interface{}
	)
	key := c.Query("key")
	data, err := gredis.Get(key)
	json.Unmarshal(data, &val)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_RESOURCES_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"value": val,
	})
}

func SDel(c *gin.Context) {
	appG := app.Gin{C: c}
	key := com.StrTo(c.Param("key")).String()
	_, err := gredis.Delete(key)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_DISCONNECT_RESOURCE_FAIL, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, nil)
}

func GetRelationMap(c *gin.Context) {
	appG := app.Gin{C: c}

	resourceService := resource_service.Resource{
		Status:   1,
		OType:    1,
		PageSize: 3,
		PageNum:  1,
	}

	nodes, links, err := resourceService.GetRelationMap()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_RELATIONMAP_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"nodes": map[string]interface{}{
			"count": len(nodes),
			"data":  nodes,
		},
		"links": map[string]interface{}{
			"count": len(links),
			"data":  links,
		},
	})
}

func GetRelationResources(c *gin.Context) {
	appG := app.Gin{C: c}
	guId := ""
	if arg := c.Query("guId"); arg != "" {
		guId = com.StrTo(arg).String()
	}

	resourceService := resource_service.Resource{
		GuId: guId,
	}
	relationResources, err := resourceService.GetRelationResources()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_RELATIONRESOURCES_FAIL, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"count":             len(relationResources),
		"relationResources": relationResources,
	})
}

func SearchResourcesByType(c *gin.Context) {
	appG := app.Gin{C: c}
	oType := 0
	if arg := c.Query("oType"); arg != "" {
		oType = com.StrTo(arg).MustInt()
	}

	resourceService := resource_service.Resource{
		OType: oType,
	}

	aliveCounts, deadCounts, err := resourceService.SearchResourcesByType()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_RESOURCES_FAIL, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"aliveCounts": aliveCounts,
		"deadCounts":  deadCounts,
	})
}

func GetSunburstMap(c *gin.Context) {
	appG := app.Gin{C: c}

	pageNum := 0
	if arg := c.Query("pageNum"); arg != "" {
		pageNum = com.StrTo(arg).MustInt()
	}

	pageSize := 0
	if arg := c.Query("pageSize"); arg != "" {
		pageSize = com.StrTo(arg).MustInt()
	}

	keyword := ""
	if arg := c.Query("keyword"); arg != "" {
		keyword = com.StrTo(arg).String()
	}

	oType := 0
	if arg := c.Query("oType"); arg != "" {
		oType = com.StrTo(arg).MustInt()
	}

	status := 0
	if arg := c.Query("status"); arg != "" {
		status = com.StrTo(arg).MustInt()
	}

	resourceService := resource_service.Resource{
		OType:    1,
		PageNum:  pageNum,
		PageSize: pageSize,
	}

	childs, err := resourceService.GetSunburstMap(keyword, oType, status)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_SUNBURSTMAP_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"childs": childs,
	})
}

func GetRelationGraph(c *gin.Context) {
	appG := app.Gin{C: c}

	guId := ""
	if arg := c.Query("guId"); arg != "" {
		guId = com.StrTo(arg).String()
	}

	resourceService := resource_service.Resource{
		GuId: guId,
	}

	nodes, links, categories, err := resourceService.GetRelationGraph()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_RELATIONGRAPH_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"nodes":      nodes,
		"links":      links,
		"categories": categories,
	})
}

func GetUtilizationInfo(c *gin.Context) {
	appG := app.Gin{C: c}

	resourceService := resource_service.Resource{}

	utilizationInfos, err := resourceService.GetUtilizationInfo()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_UTILIZATION_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"utilization": utilizationInfos,
	})
}

type TagGroupForm struct {
	OType  int                  `form:"oType" valid:"Range(1,9)"`
	Status int                  `form:"status" valid:"Range(1,2)"`
	Tag    resource_service.Tag `form:"tag" valid:"Required;MaxSize(100)"`
	//SlaveKey    string `form:"key" valid:"Required;MaxSize(100)"`
	//Value  string `form:"value" valid:"Required;MaxSize(100)"`
}

func GetTagGroupInfos(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form TagGroupForm
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}

	oType := form.OType
	status := form.Status
	tagKey := form.Tag.Key
	tagValue := form.Tag.Value.(string)

	resourceService := resource_service.Resource{
		OType:  oType,
		Status: status,
	}

	tagResources, err := resourceService.GetTagGroupInfos(tagKey, tagValue)
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_UTILIZATION_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"tagResources": tagResources,
	})
}

func GetAtlasMap(c *gin.Context) {
	appG := app.Gin{C: c}

	resourceService := resource_service.Resource{}

	nodes, links, categories, err := resourceService.GetAtlasMap()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_ATLASMAP_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"nodes":      nodes,
		"links":      links,
		"categories": categories,
	})
}

func GetStatisticDropdownList(c *gin.Context) {
	appG := app.Gin{C: c}

	resourceService := resource_service.Resource{}

	dropdownList, err := resourceService.GetStatisticDropdownList()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_REDIS_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"statisticDropdownList": dropdownList,
	})
}

func GetOtypeDropdownList(c *gin.Context) {
	appG := app.Gin{C: c}

	resourceService := resource_service.Resource{}

	otypeDropdownList, err := resourceService.GetOtypeDropdownList()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"otypeDropdownList": otypeDropdownList,
	})
}

func GetResourcesByCity(c *gin.Context) {
	appG := app.Gin{C: c}

	resourceService := resource_service.Resource{}

	cityResources, err := resourceService.GetResourcesByCity()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"cityResources": cityResources,
	})
}

func InsertTestData(c *gin.Context) {
	appG := app.Gin{C: c}

	oType := 0
	if arg := c.Query("oType"); arg != "" {
		oType = com.StrTo(arg).MustInt()
	}

	if oType == 0 {
		serverStrs := []string{"AAAAA", "BBBBB", "CCCCC", "DDDDD", "EEEEE", "FFFFF", "GGGGG"}

		//先插入服务器数据（根节点）
		for _, serverStr := range serverStrs {
			description := "11:22:33:44:55:66+00000-" + serverStr + "-00000"
			oType := 1
			addResource(description, "", oType)
		}
	} else {
		resourceService := resource_service.Resource{
			OType: oType,
		}
		resources, _ := resourceService.GetDeviceInfos()
		for _, resource := range resources {
			if resource.Utilization%2 == 0 {
				numbers := util.RandInt(3, 5)
				for i := 0; i < numbers; i++ {
					description := resource.Description + strconv.Itoa(i+1)
					dependNode := resource.GuId
					oType := resource.OType + 1
					addResource(description, dependNode, oType)
				}
			}
		}
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"msg": "success",
	})
}

func addResource(descrition string, dependNode string, oType int) {
	md5Str := (util.EncodeMD5(descrition))[0:6]
	createTime := time.Now().Format("2006-01-02 15:04:05")
	updateTime := time.Now().Format("2006-01-02 15:04:05")
	guId := ""
	di := util.DeviceInfo{
		ManufacturerId: "01",
		ProductId:      md5Str,
		ResourceType:   0,
		ResourceId:     0,
	}
	guId = util.GenerateDeviceInfo(di)
	utilization := rand.Intn(100)
	status := rand.Intn(2)
	if status == 0 {
		status = 2
	}
	area := "南研院"

	resourceService := resource_service.Resource{
		GuId:        guId,
		Description: descrition,
		Status:      status,
		Area:        area,
		PreNode:     dependNode,
		OType:       oType,
		Utilization: utilization,
		CreateTime:  createTime,
		UpdateTime:  updateTime,
	}

	// 判断guId是否已存在
	exists, err := resourceService.ExistByGuId()
	if err != nil {
		logging.Error(err)
		return
	}
	if exists {
		logging.Error(err)
		return
	}

	err = resourceService.Add()
	if err != nil {
		logging.Error(err)
		return
	}

	// 如果依赖节点不为空，在需要修改依赖节点下的子节点信息
	if dependNode != "" {
		preResourceService := resource_service.Resource{
			GuId: dependNode,
		}

		// 首先判断依赖节点是否存在 TODO
		exists, _ = preResourceService.ExistByGuId()
		if exists {
			// 查询出依赖节点下的子节点信息
			preResouce, err := preResourceService.GetDeviceInfo()
			if err != nil {
				logging.Error(err)
				return
			}
			var subNodes []string
			if strings.Contains(preResouce.SubNode, "|") {
				subNodes = strings.Split(preResouce.SubNode, "|")
			} else if preResouce.SubNode != "" {
				subNodes = append(subNodes, preResouce.SubNode)
			}
			subNodes = append(subNodes, guId)

			preResourceService := resource_service.Resource{
				GuId:    dependNode,
				SubNode: strings.Join(subNodes, "|"),
			}

			err = preResourceService.UpdateSubNode()
		}
	}
}

const (
	interval    = 10 * time.Second
	expiredTime = 2 * time.Second
)

//func ListenResourcesStatus(ctx context.Context) {
//	fmt.Println("listen resources status")
//	ttk := time.NewTicker(interval)
//	for {
//		select {
//		case <-ctx.Done():
//			return
//		case <-ttk.C:
//			resourceService := &resource_service.Resource{
//				Status:    -1,
//				OType:     -1,
//				IsControl: -1,
//				IsMonitor: -1,
//			}
//			infos, err := resourceService.GetDeviceInfos()
//			if err != nil {
//				fmt.Printf("Error getting device info %v\n", err)
//				continue
//			}
//
//			for _, info := range infos {
//				loc, err := time.LoadLocation("Local")
//				if err != nil {
//					fmt.Printf("Error loading %v\n", err)
//					continue
//				}
//				updateTime, err := time.ParseInLocation("2006-01-02 15:04:05", info.UpdateTime, loc)
//				if err != nil {
//					fmt.Printf("Error parsing update time %v\n", err)
//					continue
//				}
//
//				if info.Status == 0 && info.AutoRemove == 1 && time.Now().Sub(updateTime) > expiredTime {
//					res := &resource_service.Resource{
//						GuId: info.GuId,
//					}
//					if err := res.DeleteResource(); err != nil {
//						fmt.Printf("Error deleting resource %v\n", err)
//						continue
//					}
//				}
//			}
//		default:
//		}
//	}
//}
