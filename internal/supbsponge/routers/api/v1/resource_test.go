package v1

import (
	"fmt"
	utils "github.com/Valiben/gin_unit_test"
	"github.com/gin-gonic/gin"
	"testing"
)

func init(){
	router := gin.Default()
	router.POST("/api/v1/spongeRegister",AddResource)
	utils.SetRouter(router)
}

type OrdinaryResponse struct{
	Errno string `json:"errno"`
	Errmsg string `json:"errmsg"`
}

func TestAddResource(t *testing.T) {
	//定义发送post请求的内容
	addForm := map[string]interface{}{
		"Description":"11:22:33:44:55:66+00000-BBBBB-00000",
		"SlaveStatus":1,
		"Area":"南研院",
		"DependNode":"",
		"OType":1,
	}

	resp := OrdinaryResponse{}
	err := utils.TestHandlerUnMarshalResp("POST","/api/v1/spongeRegister","json",addForm,&resp)
	if err != nil {
		//t.Errorf("TestAddResource: %v",err)
		return
	}

	fmt.Println("result",resp)
}
