package routers

import (
	"gitee.com/info-superbahn-ict/superbahn/internal/supbsponge/middleware/jwt"
	v1 "gitee.com/info-superbahn-ict/superbahn/internal/supbsponge/routers/api/v1"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.Default()

	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	r.Use(cors.New(config))

	apiv1 := r.Group("/api/v1")

	apiv1.Use(jwt.JWT())
	{
		//获取设备信息列表
		apiv1.GET("/deviceInfos", v1.GetDeviceInfos)
		//获取单个设备信息
		apiv1.GET("/deviceInfo", v1.GetDeviceInfo)
		//添加资源
		apiv1.POST("/spongeregister", v1.AddResource)
		//心跳
		apiv1.GET("/renewal", v1.RenewalResource)
		//删除指定资源
		apiv1.DELETE("/deleteResource/:guId", v1.DeleteResource)
		//删除指定状态资源
		apiv1.POST("/deleteResources", v1.DeleteResources)
		//查询设备信息列表(根据关键字筛选)
		apiv1.GET("/searchResources", v1.SearchResources)
		//设备断开
		apiv1.POST("/disconnect", v1.DisConnect)
		//设备重连
		apiv1.POST("/reconnect", v1.ReConnect)
		//写入K-V信息
		apiv1.POST("/SSet", v1.SSet)
		//获得Value值
		apiv1.GET("/SGet", v1.SGet)
		//删除Key
		apiv1.DELETE("/SDel/:key", v1.SDel)
		//获取设备服务关联图
		apiv1.GET("/relationMap", v1.GetRelationMap)
		//获取设备关联资源（查询某guid对应设备的所有子节点设备）
		apiv1.GET("/relationResources", v1.GetRelationResources)
		//按照设备类型查询
		apiv1.GET("/searchResourcesByType", v1.SearchResourcesByType)
		//插入测试数据
		apiv1.GET("/insertTestData", v1.InsertTestData)
		//获取设备关联图（旭日图）
		apiv1.GET("/sunburstMap", v1.GetSunburstMap)
		//获取
		apiv1.GET("/relationGraph", v1.GetRelationGraph)
		//获取利用率统计信息
		apiv1.GET("/utilizationInfo", v1.GetUtilizationInfo)
		//tag有关查询
		apiv1.POST("/tagGroupInfos",v1.GetTagGroupInfos)
		//应用图谱
		apiv1.GET("/atlasMap",v1.GetAtlasMap)
		//图表下拉列表
		apiv1.GET("/statisticDropdownList",v1.GetStatisticDropdownList)
		//设备类型下拉列表
		apiv1.GET("/otypeDropdownList",v1.GetOtypeDropdownList)
		//按城市统计资源信息
		apiv1.GET("/cityResources",v1.GetResourcesByCity)
	}

	return r
}
