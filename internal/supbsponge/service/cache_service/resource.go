package cache_service

import (
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/e"
	"strconv"
	"strings"
)

type Resource struct {
	GuId    string
	Status  int
	OType   int
	KeyWord string

	TagKey   string
	TagValue string

	PageNum  int
	PageSize int
	Param    string
}

func (r *Resource) GetAliveAgentKey() string {
	keys := []string {
		e.CACHE_ALIVEAGENTS,
		"LIST",
	}

	if r.OType > 0 {
		keys = append(keys, strconv.Itoa(r.OType))
	}

	if r.Status > 0 {
		keys = append(keys,strconv.Itoa(r.Status))
	}

	return strings.Join(keys, "_")
}

func (r *Resource) GetRelationResourcesKey() string {
	keys := []string{
		e.CACHE_RELATIONRESOURCES,
		"INFO",
	}

	if r.GuId != "" {
		keys = append(keys, r.GuId)
	}

	return strings.Join(keys, "_")
}

func (r *Resource) GetSunburstMapKey() string {
	keys := []string{
		e.CACHE_SUNBURSTMAP,
		"INFO",
	}

	if r.KeyWord != "" {
		keys = append(keys, r.KeyWord)
	}

	if r.OType > 0 {
		keys = append(keys, strconv.Itoa(r.OType))
	}

	if r.Status > 0 {
		keys = append(keys, strconv.Itoa(r.Status))
	}

	return strings.Join(keys, "_")
}

func (r *Resource) GetResourceKey() string {
	keys := []string{
		e.CACHE_RESOURCE,
		"INFO",
	}

	if r.GuId != "" {
		keys = append(keys, r.GuId)
	}

	if r.OType >= 0 {
		keys = append(keys, strconv.Itoa(r.OType))
	}

	return strings.Join(keys, "_")
}

func (r *Resource) GetResourcesKey() string {
	keys := []string{
		e.CACHE_RESOURCES,
		"LIST",
	}

	if r.Status >= 0 {
		keys = append(keys, strconv.Itoa(r.Status))
	}

	if r.OType >= 0 {
		keys = append(keys, strconv.Itoa(r.OType))
	}

	if r.Param != "" {
		keys = append(keys, r.Param)
	}

	//if r.PageNum >= 0 {
	//	keys = append(keys, strconv.Itoa(r.PageNum))
	//}
	//
	//if r.PageSize >= 0 {
	//	keys = append(keys, strconv.Itoa(r.PageSize))
	//}

	return strings.Join(keys, "_")
}

func (r *Resource) GetSyncInfoKey() string {
	keys := []string{
		e.CACHE_SYNCINFO,
		"INFO",
	}

	if r.GuId != "" {
		keys = append(keys, r.GuId)
	}

	return strings.Join(keys, "_")
}
