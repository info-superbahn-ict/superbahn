package resource_service

import (
	"container/list"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbsponge/models"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbsponge/service/cache_service"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/e"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/gredis"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/logging"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
	"strconv"
	"strings"
	"time"
)

type Resource struct {
	GuId        string
	Description string
	Status      int
	Area        string
	PreNode     string
	SubNode     string
	OType       int
	Utilization int
	CreateTime  string
	UpdateTime  string
	IsMonitor   int
	IsControl   int
	//AutoRemove  int
	Tags []Tag

	CpuSet   string
	CpuRatio float64

	ID       int
	PageNum  int
	PageSize int
	Param    string
	GuIds    []string
}

type Tag struct {
	Key   string      `json:"key"`
	Type  string      `json:"type"`
	Value interface{} `json:"value"`
}

type Node struct {
	Name       string  `json:"name"`
	GuId       string  `json:"id"`
	Category   int     `json:"category"`
	SymbolSize float64 `json:"symbolSize"`
}

type Link struct {
	//SourceName string
	SourceGuId string `json:"source"`
	//TargetName string
	TargetGuId string `json:"target"`
}

type Category struct {
	Name string `json:"name"`
}

type Dropdown struct {
	ID    int    `json:"id"`
	Key   string `json:"key"`
	Value string `json:"value"`
}

type Child struct {
	Name        string   `json:"name"`
	GuId        string   `json:"guId"`
	Status      int      `json:"status"`
	OType       int      `json:"oType"`
	Description string   `json:"description"`
	Area        string   `json:"area"`
	Visiable    int      `json:"visiable"`
	Children    []*Child `json:"children"`
}

func (r *Resource) ExistByGuId() (bool, error) {
	return models.ExistResourceByGuId(r.GuId)
}

func (r *Resource) CheckMetricExist() (bool, error) {
	return models.CheckMetricExist(r.GuId)
}

func (r *Resource) Count() (int, error) {
	return models.GetResourceTotal(r.getMaps())
}

func (r *Resource) Add() error {
	tags := make([]models.Tag, 0)
	if len(r.Tags) > 0 {
		for _, tempTag := range r.Tags {
			var tag models.Tag
			tag.GuId = r.GuId
			tag.TagKey = tempTag.Key
			tag.TagType = tempTag.Type
			tag.TagValue = tempTag.Value.(string)
			tag.CreateTime = r.CreateTime
			tag.UpdateTime = r.UpdateTime
			tags = append(tags, tag)
		}
	}

	resource := map[string]interface{}{
		"gu_id":       r.GuId,
		"description": r.Description,
		"status":      r.Status,
		"area":        r.Area,
		"pre_node":    r.PreNode,
		"o_type":      r.OType,
		"is_monitor":  r.IsMonitor,
		"is_control":  r.IsControl,
		//"auto_remove": r.AutoRemove,
		"utilization": r.Utilization,
		"create_time": r.CreateTime,
		"update_time": r.UpdateTime,
		"tags":        tags,
	}

	err := models.AddResource(resource)
	if err != nil {
		return err
	}

	return nil
}

func (r *Resource) GetResourceInfo() (*models.Resource, error) {
	resource, err := models.GetResource(r.GuId)
	if err != nil {
		return nil, err
	}
	return resource, nil
}

func (r *Resource) GetDeviceInfo() (*models.Resource, error) {
	//var cacheResource *models.Resource

	//cache := cache_service.Resource{
	//	GuId: r.GuId,
	//}
	//key := cache.GetResourceKey()
	//if gredis.Exists(key) {
	//	data, err := gredis.Get(key)
	//	if err != nil {
	//		logging.Info(err)
	//	} else {
	//		json.Unmarshal(data, &cacheResource)
	//		return cacheResource, nil
	//	}
	//}

	resource, err := models.GetResource(r.GuId)
	if err != nil {
		return nil, err
	}
	queue := list.New()
	sumNode := 0
	aliveNode := 0
	queue.PushBack(r.GuId)

	for queue.Len() != 0 {
		element := queue.Front()
		queue.Remove(element)

		guid := element.Value.(string)
		res, err := models.GetResource(guid)
		if err != nil {
			logging.Error("Guid: %s, GetResource error:%v", guid, err)
			continue
		}

		if res.Status == 1 {
			aliveNode++
		}
		subNode := res.SubNode
		if strings.Contains(subNode, "|") {
			subNodes := strings.Split(subNode, "|")
			for _, guid := range subNodes {
				queue.PushBack(guid)
			}
		} else if subNode != "" {
			queue.PushBack(subNode)
		}
		sumNode++
	}
	//gredis.Set(key, resource, 3600)
	//resource.SumNode = sumNode
	//resource.AliveNode = aliveNode
	return resource, nil
}

func (r *Resource) CheckProperty() (*models.Resource, error) {
	resource, err := models.GetResource(r.GuId)
	if err != nil {
		return nil, err
	}

	return resource, nil
}

func (r *Resource) GetDeviceInfos() ([]*models.Resource, error) {
	var (
		resources, cacheResources []*models.Resource
	)

	cache := cache_service.Resource{
		Status:   r.Status,
		OType:    r.OType,
		PageNum:  r.PageNum,
		PageSize: r.PageSize,
	}
	key := cache.GetResourcesKey()
	if gredis.Exists(key) {
		data, err := gredis.Get(key)
		if err != nil {
			logging.Info(err)
		} else {
			json.Unmarshal(data, &cacheResources)
			return cacheResources, nil
		}
	}

	resources, err := models.GetResources(r.PageNum, r.PageSize, r.getMaps())
	if err != nil {
		return nil, err
	}

	return resources, nil
}

func (r *Resource) DeleteResource() error {
	err := r.DeleteTagInfos()
	if err != nil {
		return err
	}
	return models.DeleteResource(r.GuId)
}

func (r *Resource) DeleteResources() error {
	return models.DeleteResources(r.GuIds)
}

func (r *Resource) SearchResources() ([]*models.Resource, error) {
	resources, err := models.SearchResources(r.Description, r.Area)
	if err != nil {
		return nil, err
	}
	return resources, nil
}

//func (r *Resource) DisConnect() error {
//	data := make(map[string]interface{})
//	data["status"] = r.SlaveStatus
//	return models.DisConnect(r.GuId, data)
//}
//
//func (r *Resource) ReConnect() error {
//	data := make(map[string]interface{})
//	data["status"] = r.SlaveStatus
//	return models.ReConnect(r.GuId, data)
//}

func (r *Resource) RenewalDevice() string {
	exists, err := models.ExistResourceByGuId(r.GuId)
	if err != nil {
		return "failed"
	}
	if !exists {
		return "failed"
	}

	return "success"
}

func (r *Resource) UpdateStatus() error {
	data := make(map[string]interface{})
	data["status"] = r.Status
	data["update_time"] = time.Now().Format("2006-01-02 15:04:05")
	return models.UpdateStatus(r.GuId, data)
}

func (r *Resource) GetRelationMap() ([]Node, []Link, error) {
	nodes := make([]Node, 0)
	links := make([]Link, 0)
	resources, err := models.GetResources(r.PageNum, r.PageSize, r.getMaps())
	if err != nil {
		return nil, nil, err
	}

	for _, resource := range resources {
		var node Node
		node.Name = e.GetEngTypeName(resource.OType) + strconv.Itoa(resource.ID)
		node.GuId = resource.GuId
		nodes = append(nodes, node)
	}

	var rspNodes []Node
	var rspLinks []Link
	rspNodes, rspLinks = r.getRelationSub(resources, nodes, links)

	return rspNodes, rspLinks, nil
}

func (r *Resource) getRelationSub(resources []*models.Resource, nodes []Node, links []Link) ([]Node, []Link) {
	for _, resource := range resources {
		subNode := resource.SubNode
		var subNodes []string
		if strings.Contains(subNode, "|") {
			subNodes = strings.Split(subNode, "|")
		} else if subNode != "" {
			subNodes = append(subNodes, subNode)
		}

		var node Node
		var link Link
		var tempResources []*models.Resource

		for _, tempGuId := range subNodes {
			tempResource, _ := models.GetResource(tempGuId)
			node.GuId = tempGuId
			node.Name = e.GetEngTypeName(tempResource.OType) + strconv.Itoa(tempResource.ID)
			link.SourceGuId = resource.GuId
			//link.SourceName = e.GetEngTypeName(resource.OType) + strconv.Itoa(resource.ID)
			link.TargetGuId = tempGuId
			//link.TargetName = e.GetEngTypeName(tempResource.OType) + strconv.Itoa(tempResource.ID)
			nodes = append(nodes, node)
			links = append(links, link)
			tempResources = append(tempResources, tempResource)
		}
		nodes, links = r.getRelationSub(tempResources, nodes, links)
	}

	return nodes, links
}

func (r *Resource) GetRelationResources() ([]*models.Resource, error) {
	guId := r.GuId
	var (
		cacheResources, response []*models.Resource
	)
	cache := cache_service.Resource{
		GuId: guId,
	}
	key := cache.GetRelationResourcesKey()
	if gredis.Exists(key) {
		data, err := gredis.Get(key)
		if err != nil {
			logging.Error(err)
		} else {
			json.Unmarshal(data, &cacheResources)
			return cacheResources, nil
		}
	}

	resource, err := models.GetResource(r.GuId)
	if err != nil {
		return nil, err
	}
	var resources []*models.Resource
	resources = append(resources, resource)
	response = r.getRelationResources(guId, resources)
	gredis.Set(key, response, 3600)

	return response, nil
}

func (r *Resource) getRelationResources(guId string, resources []*models.Resource) []*models.Resource {
	resource, err := models.GetResource(guId)
	if err != nil {
		return nil
	}
	subNode := resource.SubNode
	var subNodes []string
	if strings.Contains(subNode, "|") {
		subNodes = strings.Split(subNode, "|")
	} else if subNode != "" {
		subNodes = append(subNodes, subNode)
	}
	for _, tempGuId := range subNodes {
		tempResource, _ := models.GetResource(tempGuId)
		resources = append(resources, tempResource)
		resources = r.getRelationResources(tempGuId, resources)
	}
	return resources
}

func (r *Resource) SearchResourcesByType() ([]models.TypeCount, []models.TypeCount, error) {
	maps := make(map[string]int)
	maps["o_type"] = r.OType
	maps["status"] = r.Status

	maps["status"] = 1
	aliveCounts, err := models.SearchResourcesNum(maps)
	if err != nil {
		return nil, nil, err
	}

	maps["status"] = 2
	deadCounts, err := models.SearchResourcesNum(maps)
	if err != nil {
		return nil, nil, err
	}

	return aliveCounts, deadCounts, nil
}

func (r *Resource) GetSunburstMap(keyword string, oType int, status int) ([]*Child, error) {
	var (
		rspChilds []*Child
	)

	//cache := cache_service.Resource{
	//	KeyWord: keyword,
	//	OType:   oType,
	//	Status:  status,
	//}

	//key := cache.GetSunburstMapKey()
	//fmt.Println(key)
	//if gredis.Exists(key) {
	//	data, err := gredis.Get(key)
	//	fmt.Println(data)
	//	if err != nil {
	//		logging.Error(err)
	//	} else {
	//		json.Unmarshal(data, &cacheChilds)
	//		return cacheChilds, nil
	//	}
	//}

	childs := make([]*Child, 0)
	resources, err := models.GetResources(r.PageNum, r.PageSize, r.getTempMaps())
	if err != nil {
		return nil, err
	}

	for _, resource := range resources {
		var child Child
		child.Name = e.GetEngTypeName(resource.OType) + strconv.Itoa(resource.ID)
		child.GuId = resource.GuId
		child.Status = resource.Status
		child.OType = resource.OType
		child.Description = resource.Description
		child.Area = resource.Area
		//TODO
		if oType > 0 && status > 0 {
			if strings.Contains(child.Description, keyword) && child.OType == oType && child.Status == status {
				child.Visiable = 1
			}
		} else if oType == 0 && status > 0 {
			if strings.Contains(child.Description, keyword) && child.Status == status {
				child.Visiable = 1
			}
		} else if status == 0 && oType > 0 {
			if strings.Contains(child.Description, keyword) && child.OType == oType {
				child.Visiable = 1
			}
		} else {
			if strings.Contains(child.Description, keyword) {
				child.Visiable = 1
			}
		}

		childs = append(childs, &child)
	}

	rspChilds = r.getSunburstMap(childs, keyword, oType, status)

	//gredis.Set(key, rspChilds, 3600)

	return rspChilds, nil
}

func (r *Resource) getSunburstMap(childs []*Child, keyword string, oType int, status int) []*Child {
	for k, child := range childs {
		resource, _ := models.GetResource(child.GuId)
		subNode := resource.SubNode
		var subNodes []string
		if subNode == "" {
			continue
		} else {
			if strings.Contains(subNode, "|") {
				subNodes = strings.Split(subNode, "|")
			} else {
				subNodes = append(subNodes, subNode)
			}
		}

		var subChilds []*Child
		for _, tempGuId := range subNodes {
			var subChild Child
			tempResource, _ := models.GetResource(tempGuId)
			if tempResource == nil {
				continue
			}
			subChild.Name = e.GetEngTypeName(tempResource.OType) + strconv.Itoa(tempResource.ID)
			subChild.GuId = tempGuId
			subChild.Status = tempResource.Status
			subChild.OType = tempResource.OType
			subChild.Description = tempResource.Description
			subChild.Area = tempResource.Area
			if oType > 0 && status > 0 {
				if strings.Contains(subChild.Description, keyword) && subChild.OType == oType && subChild.Status == status {
					subChild.Visiable = 1
				}
			} else if oType == 0 && status > 0 {
				if strings.Contains(subChild.Description, keyword) && subChild.Status == status {
					subChild.Visiable = 1
				}
			} else if status == 0 && oType > 0 {
				if strings.Contains(subChild.Description, keyword) && subChild.OType == oType {
					subChild.Visiable = 1
				}
			} else {
				if strings.Contains(subChild.Description, keyword) {
					subChild.Visiable = 1
				}
			}
			subChilds = append(subChilds, &subChild)
		}
		child.Children = r.getSunburstMap(subChilds, keyword, oType, status)
		childs[k] = child
	}
	return childs
}

func (r *Resource) GetRelationGraph() ([]Node, []Link, []Category, error) {
	nodes := make([]Node, 0)
	links := make([]Link, 0)
	categories := make([]Category, 0)

	resource, err := models.GetResource(r.GuId)
	if err != nil {
		return nil, nil, nil, err
	}

	var category Category
	category.Name = e.GetEngTypeName(resource.OType)
	categories = append(categories, category)

	var node Node
	node.Name = e.GetEngTypeName(resource.OType) + strconv.Itoa(resource.ID)
	node.GuId = resource.GuId
	nodes = append(nodes, node)

	var link Link

	if resource.PreNode != "" {
		preResource, err := models.GetResource(resource.PreNode)
		if err != nil {
			return nil, nil, nil, err
		}
		category.Name = e.GetEngTypeName(preResource.OType)
		categories = append(categories, category)

		node.Name = e.GetEngTypeName(preResource.OType) + strconv.Itoa(preResource.ID)
		node.GuId = preResource.GuId
		nodes = append(nodes, node)

		link.SourceGuId = resource.GuId
		//link.SourceName = e.GetEngTypeName(resource.OType) + strconv.Itoa(resource.ID)
		link.TargetGuId = resource.PreNode
		//link.TargetName = e.GetEngTypeName(preResource.OType) + strconv.Itoa(preResource.ID)
		links = append(links, link)
	}

	var subNodes []string
	if resource.SubNode != "" {
		if !strings.Contains(resource.SubNode, "|") {
			subNodes = append(subNodes, resource.SubNode)
		}
		subNodes = strings.Split(resource.SubNode, "|")

		subOType := resource.OType + 1
		category.Name = e.GetEngTypeName(subOType)
		categories = append(categories, category)

		for _, subNode := range subNodes {
			subResource, err := models.GetResource(subNode)
			if err != nil {
				return nil, nil, nil, err
			}
			node.Name = e.GetEngTypeName(subResource.OType) + strconv.Itoa(subResource.ID)
			node.GuId = subNode
			nodes = append(nodes, node)

			link.SourceGuId = resource.GuId
			//link.SourceName = e.GetEngTypeName(resource.OType) + strconv.Itoa(resource.ID)
			link.TargetGuId = subNode
			//link.TargetName = e.GetEngTypeName(subResource.OType) + strconv.Itoa(subResource.ID)
			links = append(links, link)
		}
	}

	return nodes, links, categories, nil
}

func (r *Resource) GetUtilizationInfo() ([]models.UtilizationInfo, error) {
	utilizationInfos, err := models.GetUtilizationInfos(e.UtiQueryStrs)
	if err != nil {
		return nil, err
	}

	return utilizationInfos, nil
}

func (r *Resource) GetTagGroupInfos(tagKey string, tagValue string) ([]*models.ResourceMetric, error) {
	maps := make(map[string]interface{}, 0)
	maps["oType"] = r.OType
	maps["status"] = r.Status
	maps["tagKey"] = tagKey
	maps["tagValue"] = tagValue
	resources, err := models.GetTagGroupInfos(maps)
	if err != nil {
		return nil, err
	}
	return resources, nil
}

func (r *Resource) GetAtlasMap() ([]Node, []Link, []Category, error) {
	nodes := make([]Node, 0)
	links := make([]Link, 0)
	categories := make([]Category, 0)
	tagInfos := make([]*models.Tag, 0)

	cNames, err := models.GetDistinctCategoryNames()
	cMap := make(map[string]int, 0)
	for _, cName := range cNames {
		var category Category
		category.Name = cName.Name
		categories = append(categories, category)
	}
	for k, v := range categories {
		cMap[v.Name] = k
	}

	dTagInfos, err := models.GetDistinctTagInfos()
	fmt.Println("dTagInfos:")
	fmt.Print(dTagInfos)
	tMap := make(map[string]string, 0)
	for _, dti := range dTagInfos {
		var node Node
		node.GuId = dti.GuId
		node.Name = dti.TagValue
		node.Category = cMap[dti.TagValue]
		node.SymbolSize = 20.0
		tMap[dti.TagValue] = dti.GuId
		nodes = append(nodes, node)
	}

	tagInfos, err = models.GetTagInfos()
	if err != nil {
		return nil, nil, nil, err
	}

	for _, tagInfo := range tagInfos {
		var link Link
		tagValues := make([]string, 0)
		if tagInfo.TagKey == "server.child" && tagInfo.TagValue != "" {
			if strings.Contains(tagInfo.TagValue, "|") {
				tagValues = strings.Split(tagInfo.TagValue, "|")
			} else {
				tagValues = append(tagValues, tagInfo.TagValue)
			}
			for _, tagValue := range tagValues {
				link.SourceGuId = tagInfo.GuId
				link.TargetGuId = tMap[tagValue]
				links = append(links, link)
			}
		}
	}

	if err != nil {
		return nil, nil, nil, err
	}

	return nodes, links, categories, nil
}

func (r *Resource) GetStatisticDropdownList() ([]Dropdown, error) {
	dropdownList := make([]Dropdown, 0)
	var dropdown Dropdown
	dropdown.ID = 1
	dropdown.Key = "searchResourcesByType"
	dropdown.Value = "节点活跃数统计"
	dropdownList = append(dropdownList, dropdown)

	dropdown.ID = 2
	dropdown.Key = "utilizationInfo"
	dropdown.Value = "利用率分布"
	dropdownList = append(dropdownList, dropdown)

	dropdown.ID = 3
	dropdown.Key = "atlasMap"
	dropdown.Value = "应用图谱"
	dropdownList = append(dropdownList, dropdown)

	//redisKey := "statisticDropdownList"

	//err := gredis.Set(redisKey, dropdownList, 3600)
	//if err != nil {
	//	return nil, err
	//}

	//var newDropdownlist []Dropdown
	//
	//data, err := gredis.Get(redisKey)
	//if err != nil {
	//	return nil, err
	//}
	//json.Unmarshal(data, &newDropdownlist)

	return dropdownList, nil
}

func (r *Resource) GetOtypeDropdownList() ([]Dropdown, error) {
	dropdownList := make([]Dropdown, 0)
	var dropdown Dropdown
	dropdown.ID = 1
	dropdown.Key = "1"
	dropdown.Value = "Server"
	dropdownList = append(dropdownList, dropdown)

	dropdown.ID = 2
	dropdown.Key = "2"
	dropdown.Value = "PC"
	dropdownList = append(dropdownList, dropdown)

	dropdown.ID = 3
	dropdown.Key = "3"
	dropdown.Value = "Docker"
	dropdownList = append(dropdownList, dropdown)

	dropdown.ID = 4
	dropdown.Key = "4"
	dropdown.Value = "External Device"
	dropdownList = append(dropdownList, dropdown)

	dropdown.ID = 5
	dropdown.Key = "5"
	dropdown.Value = "Bus"
	dropdownList = append(dropdownList, dropdown)

	dropdown.ID = 6
	dropdown.Key = "6"
	dropdown.Value = "Control"
	dropdownList = append(dropdownList, dropdown)

	dropdown.ID = 7
	dropdown.Key = "7"
	dropdown.Value = "Strategy"
	dropdownList = append(dropdownList, dropdown)

	dropdown.ID = 8
	dropdown.Key = "8"
	dropdown.Value = "Cluster"
	dropdownList = append(dropdownList, dropdown)

	return dropdownList, nil
}

func (r *Resource) GetResourcesByCity() ([]models.CityResource, error) {
	cityResources, err := models.GetResourcesByCity()
	if err != nil {
		return nil, err
	}
	return cityResources, nil
}

func (r *Resource) UpdateSubNode() error {
	data := make(map[string]interface{})
	data["subNode"] = r.SubNode
	return models.UpdateSubNode(r.GuId, data)
}

func (r *Resource) UpdateMetricInfo() error {
	data := make(map[string]interface{})
	data["guId"] = r.GuId
	data["cpuSet"] = r.CpuSet
	data["cpuRatio"] = r.CpuRatio

	return models.UpdateMetricInfo(r.GuId, data)
}

func (r *Resource) InsertMetricInfo() error {
	data := make(map[string]interface{})
	data["guId"] = r.GuId
	data["cpuSet"] = r.CpuSet
	data["cpuRatio"] = r.CpuRatio

	return models.InsertMetricInfo(data)
}

func (r *Resource) GetDeviceIds() ([]string, error) {
	listInfos, err := r.GetDeviceInfos()
	if err != nil {
		return nil, err
	}
	var listIds []string
	for _, r := range listInfos {
		listIds = append(listIds, r.GuId)
	}
	return listIds, nil
}

func (r *Resource) GetTagInfosByGuId() ([]*models.Tag, error) {
	var (
		tags []*models.Tag
		err  error
	)
	tags, err = models.GetTagInfosByGuId(r.GuId)

	if err != nil {
		return nil, err
	}
	return tags, nil
}

func (r *Resource) UpdateTagInfo(tag *spongeregister.Tag, resource *models.Resource) error {
	var err error

	updateTime := time.Now().Format("2006-01-02 15:04:05")

	tagMap := map[string]interface{}{
		"id":          resource.ID,
		"tag_value":   tag.Value,
		"update_time": updateTime,
	}

	err = models.UpdateTags(tagMap)
	if err != nil {
		return err
	}

	return nil
}

func (r *Resource) CreateTagInfo(tag *spongeregister.Tag, resource *models.Resource) error {
	var err error

	createTime := time.Now().Format("2006-01-02 15:04:05")
	updateTime := time.Now().Format("2006-01-02 15:04:05")

	tagMap := map[string]interface{}{
		"resource_id": resource.ID,
		"gu_id":       resource.GuId,
		"tag_key":     tag.Key,
		"tag_type":    tag.Type,
		"tag_value":   tag.Value,
		"create_time": createTime,
		"update_time": updateTime,
	}

	err = models.AddTags(tagMap)
	if err != nil {
		return err
	}

	return nil
}

func (r *Resource) GetResourceTagByKey(key string) (*models.Tag, error) {
	return models.GetResourceTagByKey(r.GuId, key)
}

func (r *Resource) getTempMaps() map[string]interface{} {
	maps := make(map[string]interface{})

	if r.OType > 0 {
		maps["o_type"] = r.OType
	}

	if r.Status > 0 {
		maps["status"] = r.Status
	}

	maps["is_monitor"] = 1

	maps["is_control"] = 1

	return maps
}

func (r *Resource) getMaps() map[string]interface{} {
	maps := make(map[string]interface{})

	if r.OType >= 0 {
		maps["o_type"] = r.OType
	}

	if r.Status > 0 {
		maps["status"] = r.Status
	}

	if r.IsMonitor >= 0 {
		maps["is_monitor"] = r.IsMonitor
	}

	if r.IsControl >= 0 {
		maps["is_control"] = r.IsControl
	}

	return maps
}

func (r *Resource) DeleteTagInfos() error {
	return models.DeleteTagInfos(r.GuId)
}
