package algorithm

import (
	"context"
	m1 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
)

const (
	SimpleTest = "simple_test"
	LogDumper = "log_dumper"
	MetricsDumper = "metrics_dumper"
	TraceDumper = "trace_dumper"
)

var StrategiesList = []string{SimpleTest,LogDumper,MetricsDumper,TraceDumper}

// todo select what strategy you want to use
//var Executor = map[string]func(ctx context.Context, nu supbnervous.Controller, clusterMeta *clusters.Cluster, outPut chan <- *clusters.ClusterRes){
//	SimpleTest:    simple_test.SimpleStrategy,
//	LogDumper:     log_dumper.LogDumper,
//	MetricsDumper: metrics_statistic.MetricsDumper,
//	TraceDumper:   trace_dumper.TraceDumper,
//}
//
//var Init =  map[string]func(ctx context.Context, nu supbnervous.Controller, clusterMeta *clusters.Cluster, outPut chan <- *clusters.ClusterRes){
//	SimpleTest:    simple_test.Init,
//	LogDumper:     log_dumper.Init,
//	MetricsDumper: metrics_statistic.Init,
//	TraceDumper:   trace_dumper.Init,
//}
//
//
//var Close = map[string]func(ctx context.Context, nu supbnervous.Controller, clusterMeta *clusters.Cluster, utPut chan <- *clusters.ClusterRes){
//	SimpleTest:    simple_test.Close,
//	LogDumper:     log_dumper.Close,
//	MetricsDumper: metrics_statistic.Close,
//	TraceDumper:   trace_dumper.Close,
//}


var Executor = map[string]func(ctx context.Context, nu supbnervous.Controller, clusterMeta *m1.SupbStrategyBaseInfo, updateMeta <-chan *m1.SupbStrategyBaseInfo){
	//"algorithmName": template.Executor,
}


var Close = map[string]func(ctx context.Context, nu supbnervous.Controller, clusterMeta *m1.SupbStrategyBaseInfo){
	//"algorithmName": template.Close,
}