package log_dumper

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources/manager_containers"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_clusters/clusters"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/third_party/elk"
	"github.com/sirupsen/logrus"
	"time"
)

type cache struct {
	Devices  map[string]string
	Services map[string]string
}

var (
	recorder      elk.Recorder
	strategyCache = &cache{}
)

type logElk struct {
	Guid string    `json:"guid"`
	Time time.Time `json:"timestamp"`
	Data string    `json:"message"`
}

func Init(ctx context.Context, nu supbnervous.Controller, clusterMeta *clusters.Cluster, outPut chan<- *clusters.ClusterRes) {
	var err error

	log := logrus.WithContext(ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "policy.algorithm.executor",
	})
	log.Infof("start metrics dumper")

	if recorder == nil {
		url, ok := clusterMeta.ClusterMasterContainerEnvs["ELK_URL"]
		if !ok {
			log.Fatalf("cluster meta don't find ELK_URL")
		}

		if recorder, err = elk.NewMetricsRecorder(ctx, url); err != nil {
			log.Fatalf("new recoder %v", err)
		}
	}
	//
	err = nu.RPCRegister(define.RPCFunctionNameOfManagerStrategyCollectorObjectLogs, func(args ...interface{}) (interface{}, error) {
		if len(args) < 2 {
			return "", fmt.Errorf("need two args, guid and data")
		}

		putLogData := &logElk{
			//Guid: args[0].(string),
			Guid: "hotrod_logs",
			Time: time.Now(),
			Data: args[1].(string),
		}

		bts, err := json.Marshal(putLogData)
		if err != nil {
			logrus.Errorf("marshal %v", err)
		}

		if err = recorder.Put(putLogData.Guid, string(bts)); err != nil {
			logrus.Errorf("recive %v  %v", putLogData.Guid, err)
		}
		return "RECEIVE", nil
	})

	if err != nil {
		log.Errorf("register log recive failed")
	}

	log.Infof("init finished")
}

/*
	the policy function maybe reboot automatically, because of the control meta change
*/
func LogDumper(ctx context.Context, nu supbnervous.Controller, clusterMeta *clusters.Cluster, outPut chan<- *clusters.ClusterRes) {
	log := logrus.WithContext(ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "policy.algorithm.executor",
	})
	log.Infof("start metrics dumper")

	for guid := range clusterMeta.ClusterResources {
		//mt := &agent_metrics.ContainerMetrics{}
		_, err := nu.RPCCallCustom(guid, 10, 500, define.RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush, clusterMeta.ClusterMasterGuid)
		if err != nil {
			log.Errorf("get %v metrics %v", guid, err)
		}
		log.Infof("open log push %v", guid)
	}
	//
	//err := nu.RPCRegister(define.RPCFunctionNameOfManagerStrategyCollectorObjectLogs,pushToElk)
	//
	//if err != nil {
	//	log.Infof("register log recive failed %v",err)
	//}
	//
	//log.Infof("init finished")

	// if recorder is nil, new it
	ttk := time.NewTicker(time.Duration(5) * time.Second)
	for {
		select {
		case <-ctx.Done():
			for guid := range clusterMeta.ClusterResources {
				//mt := &agent_metrics.ContainerMetrics{}

				_, err := nu.RPCCallCustom(guid, 10, 500, define.RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush, manager_containers.LogsPushOff)
				if err != nil {
					log.Errorf("get %v metrics %v", guid, err)
				}

				log.Infof("close metrics push %v", guid)
			}
			return
		case <-ttk.C:
			//log.Infof("strategy beat")
		}
	}
}

func Close(ctx context.Context, nu supbnervous.Controller, clusterMeta *clusters.Cluster, outPut chan<- *clusters.ClusterRes) {

}
