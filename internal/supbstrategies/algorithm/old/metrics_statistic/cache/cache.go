package cache

import (
	"sync"
	"time"
)

type Metrics struct {
	Cpu float64
	Memory float64
	Time time.Time
}

func (m *Metrics)DeepCopy() *Metrics {
	c := *m
	return &c
}

type MetricsCache struct {
	size int64
	head int64
	tail int64
	rwLock   *sync.RWMutex
	metricCahce []*Metrics
}

func NewMetricsCache(size int64) *MetricsCache {
	return &MetricsCache{
		head: 0,
		tail: 0,
		size: size + 1,
		rwLock: &sync.RWMutex{},
		metricCahce: make([]*Metrics, size+1),
	}
}

func (r *MetricsCache)Put(m *Metrics){
	r.rwLock.Lock()
	defer r.rwLock.Unlock()

	if (r.tail + 1) % r.size == r.head {
		r.metricCahce[r.head] = nil
		r.head = (r.head + 1) % r.size
	}

	r.metricCahce[r.tail]=m.DeepCopy()
	r.tail = (r.tail + 1) % r.size
}

func (r *MetricsCache) Read(size int64)[]*Metrics {
	r.rwLock.Lock()
	defer r.rwLock.Unlock()


	if size > (r.tail + r.size - r.head ) % r.size {
		size = (r.tail + r.size - r.head ) % r.size
	}

	data := make([]*Metrics,size)
	for i,j := int64(0),r.head ; i < size; i++ {
		data[i] = r.metricCahce[j].DeepCopy()
		j = (j+1) % r.size
	}

	return data
}