package metrics_statistic

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitee.com/info-superbahn-ict/superbahn/internal/supbstrategies/algorithm/old/metrics_statistic/cache"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources/manager_containers"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_clusters/clusters"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/third_party/elk"
	"github.com/sirupsen/logrus"
)

// type cache struct {
// 	Devices  map[string]string
// 	Services map[string]string
// }

var (
	recorder      elk.Recorder
	strategyCache *cache.MetricsCache
)

type CollectorMetrics struct {
	Guid             string    `json:"guid"`
	Name             string    `json:"name"`
	ID               string    `json:"id"`
	CPUPercentage    float64   `json:"cpuPercentage"`
	Memory           float64   `json:"memory"`
	MemoryPercentage float64   `json:"memoryPercentage"`
	MemoryLimit      float64   `json:"memoryLimit"`
	NetworkRx        float64   `json:"networkRx"`
	NetworkTx        float64   `json:"networkTx"`
	BlockRead        float64   `json:"blockRead"`
	BlockWrite       float64   `json:"blockWrite"`
	PidsCurrent      uint64    `json:"pidsCurrent"`
	Timestamp        time.Time `json:"timestamp"`
}

//type metricsELK struct {
//	Guid string   `json:"guid"`
//	Time time.Time	`json:"timestamp"`
//	Data CollectorMetrics	`json:"message"`
//}

func show (w http.ResponseWriter, r *http.Request) {
	// todo get data
	// buffers, err := ioutil.ReadAll(r.Body)
	// if err != nil {
	// 	msg := fmt.Sprintf("read body %v", err)
	// 	if _, err := fmt.Fprintf(w, msg); err != nil {
	// 		logrus.Errorf("write response %v", err)
	// 	}
	// 	logrus.Error(err)
	// 	return
	// }

	// logrus.Debugf("receive show clireq %s", buffers)

	// todo 解码参数

	data := strategyCache.Read(100)

	bts, err := json.Marshal(data)
	if err != nil {
		msg := fmt.Sprintf("marshal %v", err)
		if _, err := w.Write([]byte(msg)); err != nil {
			logrus.Errorf("write response %v", err)
		}
		logrus.Error(err)
		return
	}

	if bt, err := w.Write(bts); err != nil {
		logrus.Errorf("delete function %v", err)
	} else {
		logrus.Infof(fmt.Sprintf("DELETE REPONSE (%vB)", bt))
	}

}

func Init(ctx context.Context, nu supbnervous.Controller, clusterMeta *clusters.Cluster, outPut chan<- *clusters.ClusterRes) {
	var err error

	logrus.Infof("start metrics dumper")

	strategyCache = cache.NewMetricsCache(3600)

	if recorder == nil {
		url, ok := clusterMeta.ClusterMasterContainerEnvs["ELK_URL"]
		if !ok {
			logrus.Fatalf("cluster meta don't find ELK_URL")
		}

		if recorder, err = elk.NewMetricsRecorder(ctx, url); err != nil {
			logrus.Fatalf("new recoder %v", err)
		}
	}

	go func() {
		logrus.Infof("server start")
		http.HandleFunc("/test", show)
		if err = http.ListenAndServe(":8080", nil); err != nil {
			logrus.Infof("server closed")
		}
	}()
	//
	if err = nu.Subscribe(define.RPCFunctionNameOfManagerStrategyCollectorObjectMetrics);err !=nil {
		logrus.Fatalf("subscribe error")
	}
	go func() {
		for {
			ctxDeal,cancel := context.WithCancel(ctx)

			go func() {
				defer cancel()
				msg,err := nu.Receive(define.RPCFunctionNameOfManagerStrategyCollectorObjectMetrics)
				if err !=nil {
					logrus.Errorf("recive msg %v",err)
					return
				}

				putMetricData := &CollectorMetrics{
					Guid: msg.Key,
				}

				err = json.Unmarshal([]byte(msg.Value),putMetricData)
				if err != nil {
					logrus.Errorf("unmarshal data %v",err)
				}

				strategyCache.Put(&cache.Metrics{
					Cpu:    putMetricData.CPUPercentage,
					Memory: putMetricData.MemoryPercentage,
					Time:   putMetricData.Timestamp,
				})
				
				bts,err := json.Marshal(putMetricData)
				if err != nil {
					logrus.Errorf("marshal %v",err)
				}

				if err = recorder.Put("SUPB_"+putMetricData.Guid,string(bts));err !=nil {
					logrus.Errorf("recive %v  %v", putMetricData.Guid, err)
				}else {
					logrus.Info("put to elk ")
				}
			}()
			select {
			case <-ctxDeal.Done():
				continue
				case <-ctx.Done():
					return
			}
		}
	}()

	// err = nu.RPCRegister(define.RPCFunctionNameOfManagerStrategyCollectorObjectMetrics, func(args ...interface{}) (interface{}, error) {
	// 	if len(args) < 2 {
	// 		return "", fmt.Errorf("need two args, guid and data")
	// 	}

	// 	putMetricData := &CollectorMetrics{
	// 		Guid: args[0].(string),
	// 	}

	// 	err = json.Unmarshal([]byte(args[1].(string)), putMetricData)
	// 	if err != nil {
	// 		logrus.Errorf("unmarshal data %v", err)
	// 	}

	// 	strategyCache.Put(&cache.Metrics{
	// 		Cpu:    putMetricData.CPUPercentage,
	// 		Memory: putMetricData.MemoryPercentage,
	// 		Time:   putMetricData.Timestamp,
	// 	})

	// 	bts, err := json.Marshal(putMetricData)
	// 	if err != nil {
	// 		logrus.Errorf("marshal %v", err)
	// 	}

	// 	if err = recorder.Put("SUPB_"+putMetricData.Guid, string(bts)); err != nil {
	// 		logrus.Errorf("recive %v  %v", putMetricData.Guid, err)
	// 	}
	// 	return "RECEIVE", nil
	// })

	// if err != nil {
	// 	logrus.Errorf("register log receive failed")
	// }

	logrus.Infof("init finished")
}

/*
	the policy function maybe reboot automatically, because of the control meta change
*/
func MetricsDumper(ctx context.Context, nu supbnervous.Controller, clusterMeta *clusters.Cluster, outPut chan<- *clusters.ClusterRes) {
	log := logrus.WithContext(ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "policy.algorithm.executor",
	})
	log.Infof("start metrics dumper")

	for guid := range clusterMeta.ClusterResources {
		_, err := nu.RPCCallCustom(guid, 10, 500, define.RPCFunctionNameOfObjectRunningOnAgentForOpenMetricsPush, clusterMeta.ClusterMasterGuid)
		if err != nil {
			log.Errorf("get %v metrics %v", guid, err)
		}
		log.Infof("open log push %v", guid)
	}

	// if recorder is nil, new it
	ttk := time.NewTicker(time.Duration(5) * time.Second)
	for {
		select {
		case <-ctx.Done():
			for guid := range clusterMeta.ClusterResources {
				_, err := nu.RPCCallCustom(guid, 10, 500, define.RPCFunctionNameOfObjectRunningOnAgentForOpenMetricsPush, manager_containers.MetricsPushOff)
				if err != nil {
					log.Errorf("get %v metrics %v", guid, err)
				}

				log.Infof("close metrics push %v", guid)
			}
			return
		case <-ttk.C:
			//log.Infof("strategy beat")
		}
	}
}

func Close(ctx context.Context, nu supbnervous.Controller, clusterMeta *clusters.Cluster, outPut chan<- *clusters.ClusterRes) {

}
