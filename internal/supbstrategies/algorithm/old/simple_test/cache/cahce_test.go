package cache

import (
	"fmt"
	"testing"
	"time"
)

func TestCached(t *testing.T){
	caches := NewMetricsCache(10)

	for i := 0; i < 15; i++ {
		caches.Put(&Metrics{
			Cpu: 0.2+float64(i),
			Memory: 0.1,
			Time:time.Now(),
		})
	}

	data := caches.Read(20)
	for k,v := range data {
		fmt.Printf("get %v %v\n",k,v)
	}
	
}