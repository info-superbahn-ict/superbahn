package simple_test

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitee.com/info-superbahn-ict/superbahn/internal/supbstrategies/algorithm/old/simple_test/cache"
	// "gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources/manager_containers/containers"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_clusters/clusters"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

type CollectorMetrics struct {
	Guid             string    `json:"guid"`
	Name             string    `json:"name"`
	ID               string    `json:"id"`
	CPUPercentage    float64   `json:"cpuPercentage"`
	Memory           float64   `json:"memory"`
	MemoryPercentage float64   `json:"memoryPercentage"`
	MemoryLimit      float64   `json:"memoryLimit"`
	NetworkRx        float64   `json:"networkRx"`
	NetworkTx        float64   `json:"networkTx"`
	BlockRead        float64   `json:"blockRead"`
	BlockWrite       float64   `json:"blockWrite"`
	PidsCurrent      uint64    `json:"pidsCurrent"`
	Timestamp        time.Time `json:"timestamp"`
}

/*
	because of the policy maybe reboot, if you want to save some info, you can set controller
*/
// const (
// 	wSize     = 5
// 	hSize     = 3
// 	threshold = 11
// )

// type controller struct {
// 	Window [wSize]int
// 	Windex int
// }

var strategyCache *cache.MetricsCache

func Init(ctx context.Context, nu supbnervous.Controller, clusterMeta *clusters.Cluster, outPut chan<- *clusters.ClusterRes) {
	logrus.Infof("start simple controllor")

	// strategyCache = cache.NewMetricsCache(3600)

	interferenceGuid, ok := clusterMeta.ClusterMasterContainerEnvs["TEST_GUID"]
	if !ok {
		logrus.Fatalf("cluster meta don't find interference	guid")
	}

	logrus.Infof("find interference guid %v", interferenceGuid)

	if err := nu.Subscribe(define.RPCFunctionNameOfManagerStrategyCollectorObjectMetrics); err != nil {
		logrus.Fatalf("subscribe error")
	}

	oldValue := 5
	go func() {
		for {
			ctxDeal, cancel := context.WithCancel(ctx)

			go func() {
				defer cancel()
				msg, err := nu.Receive(define.RPCFunctionNameOfManagerStrategyCollectorObjectMetrics)
				if err != nil {
					logrus.Errorf("recive msg %v", err)
					return
				}

				putMetricData := &CollectorMetrics{
					Guid: msg.Key,
				}

				err = json.Unmarshal([]byte(msg.Value), putMetricData)
				if err != nil {
					logrus.Errorf("unmarshal data %v", err)
				}

				val := 9 - int( (putMetricData.CPUPercentage + 10) / 10 ) 
				if val <= 1 {
					val = 1
				}
				if val >= 9 {
					val = 9
				}
				
				if oldValue != val {
					oldValue = val
					ratio := fmt.Sprintf("%v",float64(val)/10.0)
					_,err = nu.RPCCallCustom(interferenceGuid,100,500,define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuRatio,ratio)
					if err !=nil {
						logrus.Errorf("control interferenceGuid %v, cpu ratio to %v %v",interferenceGuid,ratio,err)
					}
				}


			}()
			select {
			case <-ctxDeal.Done():
				continue
			case <-ctx.Done():
				return
			}
		}
	}()

}

/*
	the policy function maybe reboot automatically, because of the control meta change
*/
func SimpleStrategy(ctx context.Context, nu supbnervous.Controller, clusterMeta *clusters.Cluster, outPut chan<- *clusters.ClusterRes) {
	log := logrus.WithContext(ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "policy.algorithm.executor",
	})
	log.Infof("start simple policy")

	
	// if recorder is nil, new it
	ttk := time.NewTicker(time.Duration(5) * time.Second)
	for {
		select {
		case <-ctx.Done():
			return
		case <-ttk.C:
			//log.Infof("strategy beat")
		}
	}
}

func Close(ctx context.Context, nu supbnervous.Controller, clusterMeta *clusters.Cluster, outPut chan<- *clusters.ClusterRes) {

}
