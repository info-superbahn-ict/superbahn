package trace_dumper

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/olivere/elastic"
	"hash/fnv"
	"time"

	//elk "github.com/olivere/elastic/v7"
	"github.com/sirupsen/logrus"
	"os"
	"os/signal"
	"syscall"
	"testing"
	//"go.uber.org/zap/zapgrpc"
)

func TestSend(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	//rd, err :=elk.NewClient(elk.SetURL("http://152.136.134.100:9200"),elk.SetSniff(false))

	options := []elastic.ClientOptionFunc{elastic.SetURL("http://152.136.134.100:9200"),
		elastic.SetSniff(false), elastic.SetHealthcheck(true),
	}
	//options = append(options, elastic.SetBasicAuth("admin", "admin"))
	rd, err := elastic.NewClient(options...)
	if err != nil {
		logrus.Fatalf("new recoder %v", err)
	}

	_, err = rd.IndexPutTemplate("jaeger-span").BodyString(tmpSpan).Do(ctx)
	if err != nil {
		logrus.Fatalf("new recoder %v", err)
	}
	_, err = rd.IndexPutTemplate("jaeger-service").BodyString(tmpSrv).Do(ctx)
	if err != nil {
		logrus.Fatalf("new recoder %v", err)
	}

	data := &metricsELK{}
	err = json.Unmarshal([]byte(strTest), &data)
	if err != nil {
		fmt.Printf("unmarshall %v", err)
	}
	h := fnv.New64a()
	_, _ = h.Write([]byte(data.Process.ServiceName))
	_, _ = h.Write([]byte(data.OperationName))
	id := fmt.Sprintf("%x", h.Sum64())
	service := Service{
		ServiceName:   data.Process.ServiceName,
		OperationName: data.OperationName,
	}

	//_,err =rd.Index().Index("jaeger-span-2021-11-30").SlaveTypes("span").BodyJson(data).Do(ctx)
	//if err != nil {
	//	logrus.Fatalf("put span %v", err)
	//}
	//_,err =rd.Index().Index("jaeger-service-2021-11-30").SlaveTypes("service").BodyJson(data).Do(ctx)
	//if err != nil {
	//	logrus.Fatalf("put service %v", err)
	//}
	// fmt.Printf("span time:%v",time.Unix(int64(data.StartTimeMillis/1000),int64(data.StartTimeMillis%1000)))
	// fmt.Printf("\nspan time:%v\n",time.Unix(0,int64(data.StartTimeMillis*1e6)).Format("2006-01-02"))

	// return

	bulkIndexReq1 := elastic.NewBulkIndexRequest()
	bulkIndexReq1.Index("jaeger-service-2021-12-01").Id(id).Doc(service)
	bulkIndexReq2 := elastic.NewBulkIndexRequest()
	bulkIndexReq2.Index("jaeger-span-2021-12-01").Doc(&data)

	bks, err := rd.BulkProcessor().BulkSize(5 * 1000 * 1000).Workers(1).BulkActions(1000).FlushInterval(time.Millisecond * 200).Do(ctx)
	if err != nil {
		logrus.Fatalf("new recoder %v", err)
	}
	bks.Add(bulkIndexReq1)
	bks.Add(bulkIndexReq2)

	//si := getSpanAndServiceIndexFn()
	//spanIndex,serviceIndex := si(time.Unix(int64(data.StartTime),0))
	//h := fnv.New64a()
	//_,_=h.Write([]byte(data.Process.ServiceName))
	//_,_=h.Write([]byte(data.OperationName))
	//id:=fmt.Sprintf("%x", h.Sum64())

	//biq := elastic.NewBulkIndexRequest()
	//biq.Index("jaeger-span-test").SlaveTypes("span").Doc(data)
	//
	//biq2 := elastic.NewBulkIndexRequest()
	//biq2.Index("jaeger-service-test").SlaveTypes("service").Id(id).Doc(data)
	//
	//bks.Add(biq)
	//bks.Add(biq2)
	//
	////_, err = rd.Bulk().Index().Index("jaeger-service-test").SlaveTypes("service").Id("aa").BodyJson(data).Do(ctx)
	////bkreq :=elastic.NewBulkIndexRequest().Index(serviceIndex).SlaveTypes("service").Id(id).Doc(data)
	////bkreq2 := elastic.NewBulkIndexRequest().Index(spanIndex).SlaveTypes("span").Doc(data)
	//
	////bk := rd.Bulk()
	////bk.Add(bkreq)
	////bk.Add(bkreq2)
	////_, err = bk.Do(ctx)
	////rd.Bulk().Add(bkreq2)
	//
	//
	//
	//////
	//
	//////
	////service,_ := rd.BulkProcessor().BulkSize(5*1000*1000).Workers(1).BulkActions(1000).FlushInterval(time.Millisecond*200).Do(ctx)
	////
	//
	////service.Add(sr)
	////
	////sr2 := elastic.NewBulkIndexRequest().Index(spanIndex).SlaveTypes("span").Doc(data)
	////service.Add(sr2)
	////
	////service.Start(ctx)
	////
	////
	////
	////rd.Index().Index(strings.ToLower("jaeger-service-2021-11-25")).Id(id).BodyJson(data).Do(ctx)
	//if err !=nil {
	//	fmt.Printf("put %v",err)
	//}

	fmt.Printf("done")
	<-ctx.Done()
}

// type Service struct {
// 	ServiceName   string `json:"serviceName"`
// 	OperationName string `json:"operationName"`
// }

var strTest = `
{
  "traceID": "3c293970116a5f1b",
  "spanID": "3c293970116a5f1b",
  "flags": 1,
  "operationName": "test_span",
  "references": [],
  "startTime": 1638412432286947,
  "startTimeMillis": 1638412432286,
  "duration": 10,
  "tags": [{
          "key": "sampler.type",
          "type": "string",
          "value": "const"
      }, {
          "key": "sampler.param",
          "type": "bool",
          "value": "true"
      }, {
          "key": "worker_id",
          "type": "int64",
          "value": "1"
      }, {
          "key": "internal.span.format",
          "type": "string",
          "value": "proto"
      }
  ],
  "logs": [],
  "process": {
      "serviceName": "test",
      "tags": [{
              "key": "SUPB_GUID",
              "type": "string",
              "value": "019b543d00"
          }, {
              "key": "jaeger.version",
              "type": "string",
              "value": "Go-2.29.1"
          }, {
              "key": "hostname",
              "type": "string",
              "value": "6b82d3a38466"
          }, {
              "key": "ip",
              "type": "string",
              "value": "172.17.0.5"
          }, {
              "key": "client-uuid",
              "type": "string",
              "value": "16df4de63296a"
          }
      ]
  }
}
`
var tmpSpan = `
{
  "index_patterns": "*jaeger-span-*",
  "settings":{
    "index.number_of_shards": 5,
    "index.number_of_replicas": 1,
    "index.mapping.nested_fields.limit":50,
    "index.requests.cache.enable":true
  },
  "mappings":{
    "dynamic_templates":[
      {
        "span_tags_map":{
          "mapping":{
            "type":"keyword",
            "ignore_above":256
          },
          "path_match":"tag.*"
        }
      },
      {
        "process_tags_map":{
          "mapping":{
            "type":"keyword",
            "ignore_above":256
          },
          "path_match":"process.tag.*"
        }
      }
    ],
    "properties":{
      "traceID":{
        "type":"keyword",
        "ignore_above":256
      },
      "parentSpanID":{
        "type":"keyword",
        "ignore_above":256
      },
      "spanID":{
        "type":"keyword",
        "ignore_above":256
      },
      "operationName":{
        "type":"keyword",
        "ignore_above":256
      },
      "startTime":{
        "type":"long"
      },
      "startTimeMillis":{
        "type":"date",
        "format":"epoch_millis"
      },
      "duration":{
        "type":"long"
      },
      "flags":{
        "type":"integer"
      },
      "logs":{
        "type":"nested",
        "dynamic":false,
        "properties":{
          "timestamp":{
            "type":"long"
          },
          "fields":{
            "type":"nested",
            "dynamic":false,
            "properties":{
              "key":{
                "type":"keyword",
                "ignore_above":256
              },
              "value":{
                "type":"keyword",
                "ignore_above":256
              },
              "tagType":{
                "type":"keyword",
                "ignore_above":256
              }
            }
          }
        }
      },
      "process":{
        "properties":{
          "serviceName":{
            "type":"keyword",
            "ignore_above":256
          },
          "tag":{
            "type":"object"
          },
          "tags":{
            "type":"nested",
            "dynamic":false,
            "properties":{
              "key":{
                "type":"keyword",
                "ignore_above":256
              },
              "value":{
                "type":"keyword",
                "ignore_above":256
              },
              "tagType":{
                "type":"keyword",
                "ignore_above":256
              }
            }
          }
        }
      },
      "references":{
        "type":"nested",
        "dynamic":false,
        "properties":{
          "refType":{
            "type":"keyword",
            "ignore_above":256
          },
          "traceID":{
            "type":"keyword",
            "ignore_above":256
          },
          "spanID":{
            "type":"keyword",
            "ignore_above":256
          }
        }
      },
      "tag":{
        "type":"object"
      },
      "tags":{
        "type":"nested",
        "dynamic":false,
        "properties":{
          "key":{
            "type":"keyword",
            "ignore_above":256
          },
          "value":{
            "type":"keyword",
            "ignore_above":256
          },
          "tagType":{
            "type":"keyword",
            "ignore_above":256
          }
        }
      }
    }
  }
}
`

var tmpSrv = `
{
  "index_patterns": "*jaeger-service-*",
  "settings":{
    "index.number_of_shards": 5,
    "index.number_of_replicas": 1,
    "index.mapping.nested_fields.limit":50,
    "index.requests.cache.enable":true
  },
  "mappings":{
    "dynamic_templates":[
      {
        "span_tags_map":{
          "mapping":{
            "type":"keyword",
            "ignore_above":256
          },
          "path_match":"tag.*"
        }
      },
      {
        "process_tags_map":{
          "mapping":{
            "type":"keyword",
            "ignore_above":256
          },
          "path_match":"process.tag.*"
        }
      }
    ],
    "properties":{
      "serviceName":{
        "type":"keyword",
        "ignore_above":256
      },
      "operationName":{
        "type":"keyword",
        "ignore_above":256
      }
    }
  }
}
`
