package config

import (
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/options"
	"github.com/sirupsen/logrus"
	"io"
	"os"
)

func SetDefaultOpts(c *options.Options) {
	c.EtcdTimeOutSeconds = options.DefaultedEtcdTimeOut

	c.ElkTimeOutSeconds = options.DefaultedElkTimeOut

	c.WebServerUrl = options.DefaultWebUrl
	c.CliServerUrl = options.DefaultCliUrl

	c.NervousConfig = options.DefaultNervousConfig


	c.EtcdEndPoints = []string{options.DefaultedEtcd1}

	c.ElkUrl = options.DefaultedElkUrl

	c.LogPath = options.DefaultLogPath

	c.PluginPath = options.DefaultPluginsPath

}

func CheckOpts(c *options.Options) error {


	if c.EtcdEndPoints == nil {
		return fmt.Errorf("etcd points miss")
	}

	if c.ElkUrl == "" {
		return fmt.Errorf("elk url miss")
	}

	if c.NervousConfig == "" {
		return fmt.Errorf("nervous config miss")
	}

	if c.LogPath == "" {
		return fmt.Errorf("log path miss")
	}
	return nil
}



func InitLogs(c *options.Options)error  {
	if file,err :=os.OpenFile(c.LogPath,os.O_CREATE|os.O_WRONLY|os.O_APPEND,0777);err!=nil{
		return fmt.Errorf("open log file %v",err)
	}else{
		logrus.SetOutput(io.MultiWriter(io.Writer(file),os.Stdout))
	}
	return nil
}
