package policy

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbstrategies/runner"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/options"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/third_party/etcd"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"
	"time"
)


func NewCommand(ctx context.Context, name string) (*cobra.Command, error) {
	var opt = &options.Options{}

	// providers program entry, and passing consts pointer
	//todo init cmd
	cmd := &cobra.Command{
		Use:   name,
		Short: name + "policy",
		Long:  name + `octopus controller policy`,
		RunE: func(cmd *cobra.Command, args []string) error {
			return runRootCommand(ctx, opt)
		},
	}

	opt.EtcdEndPoints = []string{"10.16.0.180:10000"}
	opt.NervousConfig = "../../../config_nervous.json"
	//opt.LogPath = options.DefaultLogPathStrategy
	opt.PolicyName = "algorithmName"


	//cmd.Flags().StringSliceVar(&opt.EtcdEndPoints, "etcd", opt.EtcdEndPoints, "the etcd urls eg. \"10.130.10.245:14001,xxx.\"")
	cmd.Flags().StringVar(&opt.ClusterKey, "meta", opt.ClusterKey, "the meta key url eg. /controller/etcd/guid")
	//cmd.Flags().StringVar(&opt.ClusterResKey, "res", opt.ClusterResKey, "the meta key url eg. /controller/etcd/guid")
	cmd.Flags().StringVar(&opt.NervousConfig, "config", opt.NervousConfig, "the nervous config path")
	//cmd.Flags().StringVar(&opt.LogPath, "log", opt.LogPath, "the log file path")
	//cmd.Flags().StringVar(&opt.ContainerName, "name", opt.ContainerName, "the log file path")
	//cmd.Flags().StringVar(&opt.PolicyName, "policy", opt.PolicyName, "the log file path")

	return cmd, nil
}

// main routine
func runRootCommand(ctx context.Context, opt *options.Options) error {
	log := logrus.WithContext(ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "clusters run function",
	})

	//if err := config.InitLogs(opt); err != nil {
	//	return fmt.Errorf("init log %v", err)
	//}

	// todo spongeregister guid
	//randBytes := make([]byte, 8)
	//if _, err := rand.Read(randBytes); err != nil {
	//	return fmt.Errorf("generate name %v", err)
	//}
	//GUID := strings.Join([]string{"test_guid_", fmt.Sprintf("%x", randBytes)}, "-")

	GUID := os.Getenv("SUPB_GUID")
	log.Infof("CONTAINER: |%v|, GUID: |%v|", opt.ContainerName, GUID)

	ed, err := etcd.NewMetasRecorder(ctx, opt.EtcdEndPoints, time.Duration(opt.EtcdTimeOutSeconds)*time.Second)
	if err != nil {
		return fmt.Errorf("new etcd client %v", err)
	}
	log.Infof("connect etcd succeed")


	nu, err := kafkaNervous.NewNervous(ctx, opt.NervousConfig, GUID)
	if err != nil {
		return fmt.Errorf("new nerouvs client %v", err)
	}
	log.Infof("connect nervous succeed")


	rn,err := runner.NewStrategyRunner(ctx,ed,nu,GUID,opt.ClusterKey,opt.ClusterResKey,opt.PolicyName)
	if err != nil {
		return fmt.Errorf("run runner %v", err)
	}

	log.Infof("init strategy succeed, start run...")
	go rn.Run()

	<-ctx.Done()
	return nil
}
