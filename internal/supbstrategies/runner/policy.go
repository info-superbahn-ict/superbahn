package runner

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbstrategies/algorithm"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_clusters/clusters"
	m1 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/third_party/etcd"
	"github.com/coreos/etcd/mvcc/mvccpb"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"time"
)

const (
	StrategyRunnerSyncPeriodTime = 500
)

type StrategyRunner struct {
	ctx  context.Context
	etcd etcd.Recorder
	nu   supbnervous.Controller

	clusterKey    string
	clusterResKey string
	clusterMeta   *clusters.Cluster

	policyName   string
	policyDone   chan bool
	policyCancel context.CancelFunc

	outRes chan *clusters.ClusterRes

	init bool
	guid string
}

func NewStrategyRunner(ctx context.Context, etcd etcd.Recorder, nvnCli supbnervous.Controller, guid, clusterKey, clusterResKey string, strategyName string) (*StrategyRunner, error) {
	for _, s := range algorithm.StrategiesList {
		if s == strategyName {
			return &StrategyRunner{
				ctx:           ctx,
				etcd:          etcd,
				nu:            nvnCli,
				clusterKey:    clusterKey,
				clusterResKey: clusterResKey,
				policyDone:    make(chan bool, 1),
				guid:          guid,
				init:          false,
				policyName:    strategyName,
				outRes:        make(chan *clusters.ClusterRes, 1),
			}, nil
		}
	}
	return nil, fmt.Errorf("stratgy name must be [%v]", algorithm.StrategiesList)
}

func (r *StrategyRunner) Run() {
	go r.syncMeta()
	//go r.putMetaRes()
	<-r.ctx.Done()
}

//
//func (p *StrategyRunner) syncRunning() {
//	entry := log.G(p.ctx).WithFields(log.Fields{
//		define.LogsPrintCommonLabelOfFunction: "policy.syncRunning",
//	})
//
//	// spongeregister lease
//	lease := p.etcd.NewLease()
//	leaseResp, err := lease.Grant(p.ctx, consts.PolicyLeaseTime)
//	if err != nil {
//		entry.Errorf("lease grant %v", err)
//		return
//	}
//	leaseRespChan, err := lease.KeepAlive(p.ctx, leaseResp.ID)
//	if err != nil {
//		entry.Errorf("lease keep alive %v", err)
//		return
//	}
//
//	// put image
//	podName := p.controlMeta.Spec.StrategyRunner.PodNamePrefix + "-" + p.controlMeta.GUID
//
//	err = p.etcd.PutWitLease(consts.CacheRunningPrefix+podName, "Running", clientv3.WithLease(leaseResp.ID))
//	if err != nil {
//		entry.Errorf("spongeregister service %v", err)
//		return
//	}
//
//	for {
//		tick := time.NewTicker(consts.PolicyLeaseCheckPeriodTime * time.Second)
//		select {
//		case leaseKeepResp := <-leaseRespChan:
//			if leaseKeepResp == nil {
//				entry.Info("lease closed")
//			} else {
//				entry.Info("lease continue")
//			}
//		case <-p.ctx.Done():
//			if _, err = lease.Revoke(p.ctx, leaseResp.ID); err != nil {
//				entry.Errorf("lease revoke %v", err)
//				return
//			}
//			if err = lease.Close(); err != nil {
//				entry.Errorf("lease close %v", err)
//				return
//			}
//			return
//		case <-tick.C:
//			entry.Info("policy sync running beat")
//			continue
//		}
//	}
//}
//
//func (r *StrategyRunner) syncMeta() {
//	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
//		define.LogsPrintCommonLabelOfFunction: "policy.syncMeta",
//	})
//
//	_, cancel := context.WithCancel(r.ctx)
//	r.policyCancel = cancel
//	r.policyDone <- true
//	r.clusterMeta = clusters.NewDefaultCluster()
//
//	rch := r.etcd.GetWatcher(r.clusterKey + r.guid)
//	log.Infof("watcher key %v", r.clusterKey+r.guid)
//	tick := time.NewTicker(5 * time.Second)
//	for {
//		// transform policy updater message
//		select {
//		case resp := <-rch:
//			for _, evt := range resp.Events {
//				switch evt.Type {
//				case mvccpb.PUT:
//					log.Infof("put an policy [%v] [%v]", string(evt.Kv.Key),string(evt.Kv.Value))
//					if err := r.policyPutOperate(evt.Kv.Value); err != nil {
//						log.Errorf("policy put operate: %v", err)
//						continue
//					}
//				case mvccpb.DELETE:
//					algorithm.Close[r.policyName]()
//					log.Infof("del an policy [%v]", string(evt.Kv.Key))
//					if err := r.policyDelOperate(); err != nil {
//						log.Errorf("policy put operate: %v", err)
//						continue
//					}
//				}
//			}
//		case <-r.ctx.Done():
//			return
//		case <-tick.C:
//			//log.Debugf("policy sync meta beat")
//			continue
//		}
//	}
//}
func (r *StrategyRunner) syncMeta() {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "policy.syncMeta",
	})

	bts, err := r.etcd.Get(r.clusterKey)
	if err !=nil {
		log.Errorf("get %v",err)
		return
	}

	meta := &m1.SupbStrategyBaseInfo{}
	err = json.Unmarshal(bts,meta)
	if err !=nil {
		log.Errorf("get %v",err)
		return
	}
	uch := make(chan *m1.SupbStrategyBaseInfo,1)
	algorithm.Executor[r.policyName](r.ctx,r.nu,meta,uch)


	rch := r.etcd.GetWatcher(r.clusterKey)

	log.Infof("watcher key %v", r.clusterKey)

	tick := time.NewTicker(5 * time.Second)
	for {
		// transform policy updater message
		select {
		case resp := <-rch:
			for _, evt := range resp.Events {
				switch evt.Type {
				case mvccpb.PUT:
					log.Infof("put an policy [%v] [%v]", string(evt.Kv.Key),string(evt.Kv.Value))

					meta1 := &m1.SupbStrategyBaseInfo{}
					err = json.Unmarshal(bts,meta1)
					if err !=nil {
						log.Errorf("get %v",err)
						return
					}

					uch <- meta
				case mvccpb.DELETE:

				}
			}
		case <-r.ctx.Done():
			return
		case <-tick.C:
			//log.Debugf("policy sync meta beat")
			continue
		}
	}
}

//func (r *StrategyRunner) policyPutOperate(value []byte) error {
//	// cancel old policy
//	r.policyCancel()
//	_ = <-r.policyDone
//
//	logrus.Debugf("restart strategy")
//
//	// decode new control meta
//
//	err := yaml.Unmarshal(value, r.clusterMeta)
//	if err != nil {
//		return fmt.Errorf("decode meta %v", err)
//	}
//
//	// start new policy
//	ctx, cancel := context.WithCancel(r.ctx)
//	r.policyCancel = cancel
//	if !r.init {
//		r.init = true
//		algorithm.Init[r.policyName](r.ctx, r.nu, r.clusterMeta.DeepCopy(), r.outRes)
//	}
//	go func() {
//		algorithm.Executor[r.policyName](ctx, r.nu, r.clusterMeta.DeepCopy(), r.outRes)
//		r.policyDone <- true
//	}()
//
//	return nil
//}
//
//func (r *StrategyRunner) policyDelOperate() error {
//	r.policyCancel()
//	<-r.policyDone
//	r.init = false
//	algorithm.Close[r.policyName](r.ctx, r.nu, r.clusterMeta.DeepCopy(), r.outRes)
//	return nil
//}

//func (r *StrategyRunner) getMeta() error {
//	r.etcd.Get(r.clusterKey + r.guid)
//	if clt, err := clusters.GetCluster(r.clusterKey,r.guid, r.etcd); err != nil {
//		return fmt.Errorf("get meta %v", err)
//	} else {
//		r.clusterMeta = clt
//	}
//	return nil
//}

func (r *StrategyRunner) putMetaRes() {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "policy.syncMeta",
	})

	log.Infof("start listen output")
	for {
		select {
		case <-r.ctx.Done():
			log.Infof("close put res")
			return
		case res := <-r.outRes:
			if metaByte, err := yaml.Marshal(res); err != nil {
				log.Errorf("marshal res %v %v", res, err)
			} else {
				if err = r.etcd.Put(r.clusterResKey+r.guid, string(metaByte)); err != nil {
					log.Errorf("put to etcd %v", err)
				}
			}
			log.Infof("put %v", res)
		}
	}
}
