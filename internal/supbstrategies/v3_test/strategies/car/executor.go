package car

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbstrategies/v3_test/strategies/request"
	m1 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies/model"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"os"
	"strconv"
	"strings"
	"time"
)

/*
 	这个是策略执行主体，策略运行后，会自动运行改函数
		nu:		总线控制
		meta:	策略的元数据，你可以在里面找到你控制的应用的
*/
func Executor(ctx context.Context, tryTime, tryInterval int, logger log.Logger, rpc nervous.Controller, meta *m1.SupbStrategyBaseInfo, updateMeta <-chan *m1.SupbStrategyBaseInfo) {
	// 总控连接
	//url := "localhost:10000"

	sleepTime := 0
	sleepTimeStr := os.Getenv("SLEEP")

	if sleepTimeStr != "" {
		sT,err := strconv.Atoi(sleepTimeStr)
		if err !=nil {
			log.Errorf("%v",err)
			return
		}
		sleepTime = sT
		logger.Infof("SLEEP = %v",sleepTimeStr)
	}

	time.Sleep(time.Duration(sleepTime) *time.Second)

	logger.Infof("car algorithm start")

	// 获取策略应用实例
	var (
		err error
	)

	appkeys, guids, err := request.GetStrategyControlledApplicationsMultiContainer(string(meta.StrategyKey), tryTime, tryInterval, rpc)
	if err != nil {
		logger.Errorf("%v", err)
		return
	}
	logger.Infof("appkeys %v", appkeys)
	logger.Infof("guids %v", guids)

	// 策略样例，小车调控
	// 随便例举一个 appkey 和 该appkey下容器的guid

	var carGuid,interfereGuid,carKey string
	//for _, v := range appkeys {
	//	if strings.Contains(v, "supb.application.test.darknet") {
	//		darknetGuid = guids[v][0]
	//		darknetKey = v
	//		break
	//	}
	//}
	for _, v := range appkeys {
		if strings.Contains(v, "supb.application.test.car") {
			for k,v2 := range guids[v] {
				if strings.Contains(k,"supb.application.test.car.app") {
					carGuid = v2
					carKey = v
					break
				}
			}
		}
	}

	for _, v := range appkeys {
		if strings.Contains(v, "supb.application.test.car.interfere") {
			for k,v2 := range guids[v] {
				if strings.Contains(k,"supb.application.test.car.interfere.cpu") {
					interfereGuid = v2
				}
			}
		}
	}

	logger.Infof("car guid: %v interfere: %v",carGuid, interfereGuid)

	oldValue := 5

	tk := time.NewTicker(time.Second)
	for {
		cpu, err := request.GetApplicationContainerMetricsCPU(carKey, carGuid, tryTime, tryInterval, rpc)
		if err != nil {
			logger.Warnf("receive msg %v", err)
			continue
		}

		val := 9 - int((cpu+20)/10)
		if val <= 1 {
			val = 1
		}
		if val >= 9 {
			val = 9
		}

		if oldValue != val {
			oldValue = val
			ratio := fmt.Sprintf("%v", float64(val)/10.0)
			logger.Infof("set interfere ratio %v", ratio)
			if interfereGuid != "" {
				_, err = rpc.RPCCallCustom(interfereGuid, 100, 500, define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuRatioShare, ratio)
				if err != nil {
					logger.Errorf("control interferenceGuid %v, cpu ratio to %v %v", carGuid, ratio, err)
				}
			} else {
				logger.Infof("not interfere %v", interfereGuid)
			}
		}
		select {
		case <-tk.C:
			continue
		case <-ctx.Done():
			return
		}
	}
}
