package strategies

import (
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"
)

type option func(resources *SupbStrategy)

func WithRPC(rpc nervous.Controller) option {
	return func(strategy *SupbStrategy) {
		strategy.rpc = rpc
	}
}

func WithRecorder(recorder supbapis.Recorder) option {
	return func(strategy *SupbStrategy) {
		strategy.recorder = recorder
	}
}