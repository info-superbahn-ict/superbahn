package request

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbmanager/v3_test/modules/nervous-apis/clireq"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
)

func GetStrategyControlledApplications(key string,tryTime ,tryInterval int,rpc nervous.Controller) ([]string,map[string][]string,error) {
	req := clireq.StrategyRequest{
		Op: clireq.StrategyControlInfo,
		//Op: "ppppp",
		Key: key,
	}

	bts,err := json.Marshal(req)
	if err !=nil {
		return nil,nil,fmt.Errorf("marshall %v",err)
	}

	datas, err := rpc.RPCCallCustom(define.RPCCommonGuidOfManager, tryTime, tryInterval, clireq.StrategyRPC, string(bts))
	if err !=nil {
		return nil,nil,fmt.Errorf("rpc call %v",err)
	}

	//fmt.Printf("data:[%v]",datas)

	apps := clireq.Response{}
	err = json.Unmarshal([]byte(datas.(string)),&apps)
	if err !=nil {
		return nil,nil,fmt.Errorf("unmarshall %v",err)
	}

	var (
		guids = make(map[string][]string)
		appkeys []string
	)
	for _,v:= range apps.Data.([]interface{}) {
		key := v.(map[string]interface{})["application_key"].(string)
		appkeys = append(appkeys, key)

		for _, ct := range v.(map[string]interface{})["application_containers"].([]interface{}) {
			guid := ct.(map[string]interface{})["guid"].(string)
			guids[key] = append(guids[key], guid)
		}
	}

	return appkeys,guids,nil
}

func GetStrategyControlledApplicationsMultiContainer(key string,tryTime ,tryInterval int,rpc nervous.Controller) ([]string,map[string]map[string]string,error) {
	req := clireq.StrategyRequest{
		Op: clireq.StrategyControlInfo,
		//Op: "ppppp",
		Key: key,
	}

	bts,err := json.Marshal(req)
	if err !=nil {
		return nil,nil,fmt.Errorf("marshall %v",err)
	}

	datas, err := rpc.RPCCallCustom(define.RPCCommonGuidOfManager, tryTime, tryInterval, clireq.StrategyRPC, string(bts))
	if err !=nil {
		return nil,nil,fmt.Errorf("rpc call %v",err)
	}

	//fmt.Printf("data:[%v]",datas)

	apps := clireq.Response{}
	err = json.Unmarshal([]byte(datas.(string)),&apps)
	if err !=nil {
		return nil,nil,fmt.Errorf("unmarshall %v",err)
	}

	var (
		guids = make(map[string]map[string]string)
		appkeys []string
	)
	for _,v:= range apps.Data.([]interface{}) {
		key := v.(map[string]interface{})["application_key"].(string)
		appkeys = append(appkeys, key)

		guids[key] = make(map[string]string,len(v.(map[string]interface{})["application_containers"].([]interface{})))
		for _, ct := range v.(map[string]interface{})["application_containers"].([]interface{}) {
			guid := ct.(map[string]interface{})["guid"].(string)
			guids[key][ct.(map[string]interface{})["appkey"].(string)] = guid
		}
	}

	return appkeys,guids,nil
}

func GetApplicationContainerMetricsCPU(key ,guid string,tryTime ,tryInterval int, rpc nervous.Controller) (float64,error) {
	if rpc == nil {
		return 0,fmt.Errorf("rpc is nil")
	}

	req := clireq.MonitorRequest{
		Op:   clireq.MetricsContainerNow,
		Key:  key,
		Kind: clireq.MonitorApplication,
		Guid: guid,
	}

	btsm, err := json.Marshal(req)
	if err != nil {
		return 0,err
	}

	data, err := rpc.RPCCallCustom(define.RPCCommonGuidOfManager, tryTime, tryInterval, clireq.MonitorRPC, string(btsm))
	if err !=nil {
		return 0,fmt.Errorf("rpc call %v",err)
	}
	fmt.Printf("data: [%v]",data)
	resp := clireq.Response{}
	err = json.Unmarshal([]byte(data.(string)),&resp)
	if err !=nil {
		return 0,fmt.Errorf("unmarshall %v",err)
	}

	cpu := resp.Data.(map[string]interface{})["cpu_percentage"].(float64)
	if err != nil {
		return 0,fmt.Errorf("%v",err)
	}

	return cpu,nil
}
//
//
//
//	return resp.Data,nil
//}
//
//func GetApplicationContainerMetricsCPU(key string,guid string,url string) (float64,error) {
//	req := clireq.MonitorRequest{
//		Op: "monitor.metrics.container.now",
//		Key: key,
//		Kind: "application",
//		Guid: guid,
//	}
//
//	bts,_ := json.Marshal(req)
//
//	data, err := http.Post("http://"+url+"/web/apis/metrics","application/json",bytes.NewBuffer(bts))
//
//	bts2, err := ioutil.ReadAll(data.Body)
//	if err != nil {
//		return 0,fmt.Errorf("read body %v", err)
//	}
//
//	resp := clireq.Response{}
//	if err = json.Unmarshal(bts2,&resp);err !=nil{
//		return 0,fmt.Errorf("unmarshall body %v", err)
//	}
//
//	m := resp.Data.(map[string]interface{})
//
//	return m["cpu_percentage"].(float64),nil
//}
//
//
//func GetApplicationContainerMetricsMemory(key string,guid string,url string) (float64,error) {
//	req := clireq.MonitorRequest{
//		Op: "monitor.metrics.container.now",
//		Key: key,
//		Kind: "application",
//		Guid: guid,
//	}
//
//	bts,_ := json.Marshal(req)
//
//	data, err := http.Post("http://"+url+"/web/apis/metrics","application/json",bytes.NewBuffer(bts))
//
//	bts2, err := ioutil.ReadAll(data.Body)
//	if err != nil {
//		return 0,fmt.Errorf("read body %v", err)
//	}
//
//	resp := clireq.Response{}
//	if err = json.Unmarshal(bts2,&resp);err !=nil{
//		return 0,fmt.Errorf("unmarshall body %v", err)
//	}
//
//	m := resp.Data.(map[string]interface{})
//
//	return m["memory_percentage"].(float64),nil
//}
//
//func GetApplicationContainerMetricsTransport(key string,guid string,url string) (float64,error) {
//	req := clireq.MonitorRequest{
//		Op: "monitor.metrics.container.now",
//		Key: key,
//		Kind: "application",
//		Guid: guid,
//	}
//
//	bts,_ := json.Marshal(req)
//
//	data, err := http.Post("http://"+url+"/web/apis/metrics","application/json",bytes.NewBuffer(bts))
//
//	bts2, err := ioutil.ReadAll(data.Body)
//	if err != nil {
//		return 0,fmt.Errorf("read body %v", err)
//	}
//
//	resp := clireq.Response{}
//	if err = json.Unmarshal(bts2,&resp);err !=nil{
//		return 0,fmt.Errorf("unmarshall body %v", err)
//	}
//
//	m := resp.Data.(map[string]interface{})
//
//	return m["networkTx"].(float64),nil
//}
//
//func GetApplicationContainerMetricsReceive(key string,guid string,url string) (float64,error) {
//	req := clireq.MonitorRequest{
//		Op: "monitor.metrics.container.now",
//		Key: key,
//		Kind: "application",
//		Guid: guid,
//	}
//
//	bts,_ := json.Marshal(req)
//
//	data, err := http.Post("http://"+url+"/web/apis/metrics","application/json",bytes.NewBuffer(bts))
//
//	bts2, err := ioutil.ReadAll(data.Body)
//	if err != nil {
//		return 0,fmt.Errorf("read body %v", err)
//	}
//
//	resp := clireq.Response{}
//	if err = json.Unmarshal(bts2,&resp);err !=nil{
//		return 0,fmt.Errorf("unmarshall body %v", err)
//	}
//
//	m := resp.Data.(map[string]interface{})
//
//	return m["networkRx"].(float64),nil
//}