package strategies

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbstrategies/v3_test/strategies/car"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbstrategies/v3_test/strategies/simcar"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbstrategies/v3_test/strategies/sine"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies"
	m1 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies/model"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/coreos/etcd/mvcc/mvccpb"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"time"
)

const (
	rpcTryTime = ".rpc.try.time"
	rpcTryInterval = ".rpc.try.interval"
	etcdMetaKey = ".etcd.key"
	algorithm = ".algorithm"
	managerUrl =".manager.url"

	//algorithmSelected = "car"
	algorithmSelected = "simcar"
)


var Executors = map[string]func(ctx context.Context, tryTime, tryInterval int, logger log.Logger, rpc nervous.Controller, meta *m1.SupbStrategyBaseInfo, updateMeta <-chan *m1.SupbStrategyBaseInfo){
	"algorithmName": sine.Executor,
	"car": car.Executor,
	"simcar": simcar.Executor,
}

type SupbStrategy struct {
	ctx context.Context
	prefix string
	name string

	rpc nervous.Controller
	rpcTryTime int
	rpcTryInterval int

	recorder supbapis.Recorder

	etcdKey string
	managerUrl string

	logger log.Logger
}

func NewSupbStrategy(ctx context.Context,prefix string) *SupbStrategy {
	return &SupbStrategy{
		ctx:    ctx,
		prefix: prefix,
		logger: log.NewLogger(prefix),
	}
}

func (r *SupbStrategy) InitFlags(flags *pflag.FlagSet) {
	flags.String(r.prefix+etcdMetaKey,"a06f7e73-0280-4310-a0db-4eb6d072f81c","")
	flags.String(r.prefix+algorithm,algorithmSelected,"algorithm name")
	//flags.String(r.prefix+managerUrl,"localhost:10000","algorithm name")
	flags.String(r.prefix+managerUrl,"10.130.155.96:10000","algorithm name")

	flags.Int(r.prefix+rpcTryTime,100,"algorithm name")
	flags.Int(r.prefix+rpcTryInterval,500,"algorithm name")
}

func (r *SupbStrategy)ViperConfig(viper *viper.Viper)  {

}

func (r *SupbStrategy) InitViper(viper *viper.Viper) {
	key := viper.GetString(r.prefix+etcdMetaKey)
	r.name = viper.GetString(r.prefix+algorithm)
	r.managerUrl = viper.GetString(r.prefix+managerUrl)

	r.rpcTryTime = viper.GetInt(r.prefix+rpcTryTime)
	r.rpcTryInterval = viper.GetInt(r.prefix+rpcTryInterval)


	r.etcdKey = fmt.Sprintf(strategies.DynamicStrategyRecorderKeyFormat,key)
}

func (r *SupbStrategy) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *SupbStrategy) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}

	go r.Run()

	r.logger.Infof("%v initialized",r.prefix)
}

func (r *SupbStrategy) Close() error {
	r.logger.Infof("%v closed",r.prefix)
	return nil
}

func (r *SupbStrategy) Run()  {
	bts, err := r.recorder.Read(r.etcdKey)
	if err !=nil {
		r.logger.Errorf("get %v",err)
		return
	}

	meta := &m1.SupbStrategyBaseInfo{}
	err = json.Unmarshal(bts,meta)
	if err !=nil {
		r.logger.Errorf("get %v",err)
		return
	}
	uch := make(chan *m1.SupbStrategyBaseInfo,1)


	// todo add you policy
	// ****************
	go Executors[r.name](r.ctx,r.rpcTryTime,r.rpcTryInterval,log.NewLogger(r.prefix+".executor"),r.rpc,meta,uch)
	// ****************


	rch := r.recorder.WatchKey(r.etcdKey)
	tick := time.NewTicker(5 * time.Second)
	for {
		// transform policy updater message
		select {
		case resp := <-rch:
			for _, evt := range resp.Events {
				switch evt.Type {
				case mvccpb.PUT:
					r.logger.Infof("put an policy [%v] [%v]", string(evt.Kv.Key),string(evt.Kv.Value))
					meta1 := &m1.SupbStrategyBaseInfo{}
					err = json.Unmarshal(bts,meta1)
					if err !=nil {
						r.logger.Errorf("get %v",err)
						return
					}
					if len(uch) >=1{
						r.logger.Errorf("chan is full")
						continue
					}
					uch <- meta
				case mvccpb.DELETE:

				}
			}
		case <-r.ctx.Done():
			return
		case <-tick.C:
			r.logger.Infof("测试 日志显示")
			r.logger.Warnf("测试 日志显示")
			r.logger.Errorf("测试 日志显示")
			continue
		}
	}
}

