package template

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/internal/supbstrategies/v3_test/strategies/request"
	m2 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications/model"
	m1 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies/model"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
)












/*
 	这个是策略执行主体，策略运行后，会自动运行改函数
		nu:		总线控制
		meta:	策略的元数据，你可以在里面找到你控制的应用的
*/
func Executor (ctx context.Context,url string,logger log.Logger, rpc nervous.Controller, meta *m1.SupbStrategyBaseInfo, updateMeta <-chan *m1.SupbStrategyBaseInfo){
	// 总控连接
	//url := "localhost:10000"

	logger.Infof("algorithm start")
	// 获取策略应用实例
	apps,err := request.GetStrategyControlledApplications(string(meta.StrategyKey),url)
	logger.Infof("apps %v %v",apps,err)

	// 获取应用容器的实时监控
	var guid string
	for _,appi := range apps {
		app := appi.(*m2.SupbApplicationBaseInfo)
		for _,container := range app.ApplicationContainers {
			data,err := request.GetApplicationContainerMetrics(string(app.ApplicationKey), container.Guid,url)
			logger.Infof("apps %v %v",data,err)
			guid = container.Guid
		}
	}

	// 对容器进行操作，function见 define
	_,err = rpc.RPCCall(guid,define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuRatio,"0.80")
	logger.Infof("apps %v",err)


	// 策略样例，正弦调控

	// 随便例举一个 appkey 和 该appkey下容器的guid
	appkey := string(apps[0].(*m2.SupbApplicationBaseInfo).ApplicationKey)
	guid = string(apps[0].(*m2.SupbApplicationBaseInfo).ApplicationContainers[0].Guid)

	oldValue := 5
	for {
		ctxDeal, cancel := context.WithCancel(ctx)

		// 一次调节
		go func() {
			defer cancel()
			cpu,err := request.GetApplicationContainerMetricsCPU(appkey, guid,url)
			if err != nil {
				logger.Errorf("recive msg %v", err)
				return
			}

			val := 9 - int( (cpu + 10) / 10 )
			if val <= 1 {
				val = 1
			}
			if val >= 9 {
				val = 9
			}

			if oldValue != val {
				oldValue = val
				ratio := fmt.Sprintf("%v",float64(val)/10.0)
				_,err = rpc.RPCCallCustom(guid,100,500,define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuRatio,ratio)
				if err !=nil {
					logger.Errorf("control interferenceGuid %v, cpu ratio to %v %v",guid,ratio,err)
				}
			}
		}()
		select {
		case <-ctxDeal.Done():
			continue
		case <-ctx.Done():
			cancel()
			return
		}
	}
}
