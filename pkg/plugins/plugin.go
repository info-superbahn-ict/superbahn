package plugins

type PluginFactory interface {
	Run()
	Close()
	Call(string, ...interface{}) (interface{}, error)
}
