package register

import (
	"encoding/json"
	"fmt"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/options"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
)

const (
	tryTime     = 10
	tryInterval = 500
)

func Register(Nic supbnervous.Controller, info *spongeregister.Resource) (string, error) {
	bts, err := json.Marshal(info)
	if err != nil {
		return "", err
	}
	resp, err := Nic.RPCCallCustom(options.InitSpongeNervous, tryTime, tryInterval, define.REGISTER_DEVICE, string(bts))
	if err != nil {
		return "", fmt.Errorf("call get %v", err)
	}
	return resp.(string), nil
	//return uuid.NewV4().String(), nil
}

func RenewalDevice(Nic supbnervous.Controller, info *spongeregister.Resource) error {
	bts, err := json.Marshal(info)
	if err != nil {
		return err
	}
	_, err = Nic.RPCCallCustom(options.InitSpongeNervous, tryTime, tryInterval, define.RENEWAL_DEVICE, string(bts))
	if err != nil {
		return fmt.Errorf("call get %v", err)
	}
	return nil
}

func DeleteDevice(Nic supbnervous.Controller, info *spongeregister.Resource) error {
	bts, err := json.Marshal(info)
	if err != nil {
		return err
	}
	_, err = Nic.RPCCallCustom(options.InitSpongeNervous, tryTime, tryInterval, define.DELETE_RESOURCE, string(bts))
	if err != nil {
		return fmt.Errorf("call get %v", err)
	}
	return nil
}

func Reconnect(Nic supbnervous.Controller, info *spongeregister.Resource) error {
	bts, err := json.Marshal(info)
	if err != nil {
		return err
	}
	_, err = Nic.RPCCallCustom(options.InitSpongeNervous, tryTime, tryInterval, define.RECONNECT, string(bts))
	if err != nil {
		return fmt.Errorf("call get %v", err)
	}
	return nil
}

func UpdateStatusDevice(Nic supbnervous.Controller, syncInfo *spongeregister.SyncInfo) error {
	bts, err := json.Marshal(syncInfo)
	if err != nil {
		return err
	}
	_, err = Nic.RPCCallCustom(options.InitSpongeNervous, tryTime, tryInterval, define.UPDATE_STATUS, string(bts))
	if err != nil {
		return fmt.Errorf("call get %v", err)
	}
	return nil
}
