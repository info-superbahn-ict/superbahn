package register

import (
	"context"
	"fmt"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
	"testing"
)

func TestRegister(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, "nervous_config.json", "test")
	if err != nil {
		fmt.Printf("new kafkaNervous error: %v\n", err)
		return
	}

	tag := spongeregister.Tag{
		Key:   "key1",
		Type:  "1",
		Value: "111",
	}
	var tags []spongeregister.Tag
	tags = append(tags, tag)
	res := &spongeregister.Resource{
		Description: "ff:00:00:2e:45:66+2021e-80132-82079",
		Status:      1,
		OType:       2,
		IsMonitor:   1,
		IsControl:   1,
		Tags:        tags,
	}
	resp, _ := Register(Nic, res)
	fmt.Printf("%s\n", resp)
	Nic.Close()
}

func TestDeleteDevice(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, "nervous_config.json", "test")
	if err != nil {
		fmt.Printf("new kafkaNervous error: %v\n", err)
		return
	}
	//if err = Nic.Run(); err != nil {
	//	fmt.Printf("run error: %v\n", err)
	//	return
	//}

	res := &spongeregister.Resource{
		GuId: "01e1150600",
	}
	resp := DeleteDevice(Nic, res)
	fmt.Printf("%s\n", resp)
	Nic.Close()
}

func TestReconnect(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, "nervous_config.json", "test")
	if err != nil {
		fmt.Printf("new kafkaNervous error: %v\n", err)
		return
	}
	//if err = Nic.Run(); err != nil {
	//	fmt.Printf("run error: %v\n", err)
	//	return
	//}

	res := &spongeregister.Resource{
		GuId: "01df258f00",
	}
	resp := Reconnect(Nic, res)
	fmt.Printf("%s\n", resp)
	Nic.Close()
}

func TestRenewalDevice(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, "nervous_config.json", "test")
	if err != nil {
		fmt.Printf("new kafkaNervous error: %v\n", err)
		return
	}
	//if err = Nic.Run(); err != nil {
	//	fmt.Printf("run error: %v\n", err)
	//	return
	//}

	res := &spongeregister.Resource{
		GuId: "0126d73600",
	}
	resp := RenewalDevice(Nic, res)
	fmt.Printf("%s\n", resp)

}
