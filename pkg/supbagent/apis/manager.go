package apis

import (
	"context"
	"fmt"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/collector"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/controller"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/options"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources/manager_containers"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources/manager_host"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
)

type Manager struct {
	ctx              context.Context
	opt              *options.Options
	managers         map[string]interface{}
	nervousAgent     nervous.Controller
	nervousResources map[string]nervous.Controller
}

func NewManager(ctx context.Context, opt *options.Options,
	collector *collector.Manager,
	controller *controller.Manager,
	nervousAgent nervous.Controller,
	guid string,
) (*Manager, error) {
	m := &Manager{
		ctx:              ctx,
		opt:              opt,
		managers:         make(map[string]interface{}),
		nervousResources: make(map[string]nervous.Controller),
		nervousAgent:     nervousAgent,
	}
	for _, v := range resources.ResourceTypes {
		switch v {
		case resources.ResourceTypeHost:
			mh, err := manager_host.NewHostManager(ctx, controller, collector, guid)
			if err != nil {
				return nil, fmt.Errorf("new host manager %v", err)
			}
			m.managers[v] = mh
		case resources.ResourceTypeContainer:
			mc, err := manager_containers.NewContainerManager(ctx, controller, collector)
			if err != nil {
				return nil, fmt.Errorf("new host manager %v", err)
			}
			m.managers[v] = mc
		}
	}
	return m, nil
}

func (r *Manager) GetOption() *options.Options {
	return r.opt
}

func (r *Manager) GetHostManager() *manager_host.HostManager {
	return r.managers[resources.ResourceTypeHost].(*manager_host.HostManager)
}

func (r *Manager) GetContainerManager() *manager_containers.ContainerManager {
	return r.managers[resources.ResourceTypeContainer].(*manager_containers.ContainerManager)
}

func (r *Manager) GetAgentNervous() nervous.Controller {
	return r.nervousAgent
}
