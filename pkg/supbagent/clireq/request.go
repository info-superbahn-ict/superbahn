package clireq

import (
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
)

const (
	ListAll   = "true"
	DeleteAll = "true"
)

type ClientRequest struct {
	DeleteAll     string               `json:"deleteAll"`
	ResourceType  string               `json:"version"`
	ListAll       string               `json:"list_all"`
	Guid          string               `json:"guid"`
	ContainerId   string               `json:"ContainerId"`
	ImageTag      string               `json:"imageTag"`
	ContainerName string               `json:"containerName"`
	Runtime 		string			`json:"runtime"`
	Workdir 		string			`json:"workdir"`
	Envs          map[string]string    `json:"envs"`
	Ports         map[string]string    `json:"ports"`
	Cmds          []string             `json:"cmds"`
	Tags          []spongeregister.Tag `json:"tags"`

	//新增 SERVE gitlab-runner字段
	ImagePullPolicy	string `json:"imagePullPolicy"`
	Lifecycle 		map[string]string `json:"lifecycle"`
	LivenessProbe	map[string]string `json:"livenessProbe"`
	ReadinessProbe	map[string]string `json:"readinessProbe"`
	Reasources		map[string]string `json:"reasources"`
	SecurityContext	map[string]string `json:"securityContext"`
	TerminationMessagePath	 []string `json:"terminationMessagePath"`
	TerminationMessagePolicy []string `json:"terminationMessagePolicy"`
	VolumeMounts	map[string]string `json:"volumeMounts"`
}

type ClientResponse struct {
	Message       string   `json:"message"`
	Guid          string   `json:"guid"`
	ImageTag      string   `json:"imageTag"`
	ContainerName string   `json:"containerName"`
	ContainerId   string   `json:"ContainerId"`
	ContainerHost string   `json:"ContainerIp"`
	ContainerPort []string `json:"ContainerPorts"`
}
