package collector

import (
	"context"
	"fmt"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/collector/collector_docker"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/collector/collector_jaeger"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/collector/collector_prometheus"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/options"
	"gitee.com/info-superbahn-ict/superbahn/third_party/docker"
)

const (
	prometheusInterval = 1
)

type Manager struct {
	ctx context.Context

	docker     *collector_docker.Collector
	prometheus *collector_prometheus.Collector
	jaeger     *collector_jaeger.Collector
}

func NewCollector(ctx context.Context, opt *options.Options) (*Manager, error) {
	dk, err := docker.NewDockerController(ctx, opt.DockerUrl)
	if err != nil {
		return nil, fmt.Errorf("new controller_docker controller %v", err)
	}
	m := &Manager{ctx: ctx}

	m.docker, err = collector_docker.NewCollectorDocker(ctx, dk, collector_docker.DefaultInterval)
	if err != nil {
		return nil, fmt.Errorf("new collector_docker client %v", err)
	}

	m.prometheus, err = collector_prometheus.NewCollectorPrometheus(ctx, prometheusInterval)
	if err != nil {
		return nil, fmt.Errorf("new collector_docker client %v", err)
	}

	m.jaeger, err = collector_jaeger.NewCollectorJaeger(ctx, opt.JaegerCollectorUrl, opt.JaegerCollectorRoute)

	return m, nil
}

//func NewCollector(ctx context.Context, opt *options.Options, nu supbnervous.Controller) (*Manager, error) {
//	dk, err := docker.NewDockerController(ctx, opt.DockerUrl)
//	if err != nil {
//		return nil, fmt.Errorf("new controller_docker controller %v", err)
//	}
//	m := &Manager{ctx: ctx}
//
//	m.docker, err = collector_docker.NewCollectorDocker(ctx, dk, collector_docker.DefaultInterval)
//	if err != nil {
//		return nil, fmt.Errorf("new collector_docker client %v", err)
//	}
//
//	m.prometheus, err = collector_prometheus.NewCollectorPrometheus(ctx, prometheusInterval)
//	if err != nil {
//		return nil, fmt.Errorf("new collector_docker client %v", err)
//	}
//
//	m.jaeger, err = collector_jaeger.NewCollectorJaeger(ctx, opt.JaegerCollectorUrl, opt.JaegerCollectorRoute, nu)
//
//	return m, nil
//}

func (r *Manager) GetContainerCollector() *collector_docker.Collector {
	return r.docker
}

func (r *Manager) GetHostCollector() *collector_prometheus.Collector {
	return r.prometheus
}

func (r *Manager) GetJaegerCollector() *collector_jaeger.Collector {
	return r.jaeger
}
