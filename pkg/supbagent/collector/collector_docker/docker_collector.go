package collector_docker

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"sync"
	"time"

	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"

	"gitee.com/info-superbahn-ict/superbahn/third_party/docker"
	"github.com/docker/docker/api/types"
)

const (
	maxSize         = 1024
	DefaultInterval = 1000
)

type (
	Collector struct {
		cancel     context.CancelFunc
		ctx        context.Context
		controller docker.Controller
		metrics    *sync.Map
		logInfo    *sync.Map
		interval   time.Duration //毫秒
	}
)

type CollectorMetrics struct {
	Name             string    `json:"name"`
	ID               string    `json:"id"`
	CPUPercentage    float64   `json:"cpuPercentage"`
	Memory           float64   `json:"memory"`
	MemoryPercentage float64   `json:"memoryPercentage"`
	MemoryLimit      float64   `json:"memoryLimit"`
	NetworkRx        float64   `json:"networkRx"`
	NetworkTx        float64   `json:"networkTx"`
	BlockRead        float64   `json:"blockRead"`
	BlockWrite       float64   `json:"blockWrite"`
	PidsCurrent      uint64    `json:"pidsCurrent"`
	Timestamp        time.Time `json:"timestamp"`
}

func NewCollectorDocker(ctx context.Context, controller docker.Controller, interval int64) (*Collector, error) {
	var collector = &Collector{
		interval:   time.Duration(interval) * time.Millisecond,
		controller: controller,
		metrics:    &sync.Map{},
		logInfo:    &sync.Map{},
	}
	collector.ctx, collector.cancel = context.WithCancel(ctx)
	return collector, nil
}

/*
	收集指定容器 Id 的监控数据，监控数据结构体在  pkg/supbagent/resources/manager_containers/containers中有定义，此函数也会在那调用
*/
func (r *Collector) OutputMetrics(containerId string) (<-chan string, error) {
	ch := make(chan string, maxSize)
	if _, ok := r.metrics.Load(containerId); !ok {
		var val []chan string
		val = append(val, ch)
		r.metrics.Store(containerId, val)
		go r.run(containerId)
	} else {
		data, _ := r.metrics.Load(containerId)
		val := data.([]chan string)
		val = append(val, ch)
		r.metrics.Store(containerId, val)
	}
	return ch, nil
}

func (r *Collector) OutputLogs(containerId string) (<-chan string, error) {
	ch := make(chan string, maxSize)
	if _, ok := r.logInfo.Load(containerId); !ok {
		var val []chan string
		val = append(val, ch)
		r.logInfo.Store(containerId, val)
		go r.collectorLog(containerId)
	} else {
		data, _ := r.logInfo.Load(containerId)
		val := data.([]chan string)
		val = append(val, ch)
		r.logInfo.Store(containerId, val)
	}
	return ch, nil
}

func (r *Collector) CloseChan(containerId string) error {
	_, ok := r.metrics.Load(containerId)
	if !ok {
		return fmt.Errorf("container id is nil")
	}
	r.metrics.Delete(containerId)
	r.logInfo.Delete(containerId)
	return nil
}

func (r *Collector) Logs(containerId string) (string, error) {
	return r.controller.Log(containerId)
}

//func (r *Collector) LogInfoChan(containerId string)(<-chan string,<-chan string,error){
//	if _, ok := r.logInfo.Load(containerId); !ok {
//		var info []chan string
//		var Err []chan string
//		r.logInfo.Store(containerId, info)
//		r.logErr.Store(containerId,Err)
//		go r.collectorLog(containerId)
//	}
//	chI := make(chan string, maxSize)
//	chE := make(chan string, maxSize)
//	Info, _ := r.logInfo.Load(containerId)
//	Err, _ := r.logErr.Load(containerId)
//	InfoSlice := Info.([]chan string)
//	ErrSlice := Err.([]chan string)
//	InfoSlice = append(InfoSlice, chI)
//	ErrSlice = append(ErrSlice, chE)
//	r.logInfo.Store(containerId, InfoSlice)
//	r.logErr.Store(containerId, ErrSlice)
//	return chI,chE, nil
//}

func (r *Collector) run(containerId string) {
	ttk := time.NewTicker(r.interval)
	defer ttk.Stop()
	for {
		select {
		case <-r.ctx.Done():
			r.metrics.Range(func(key, value interface{}) bool {
				val := value.([]chan string)
				for _, c := range val {
					close(c)
				}
				return true
			})
			return
		case <-ttk.C:
			stats, err := r.controller.GetContainerStats(containerId)
			if err != nil {
				fmt.Printf("getContainerStats error %v\n", err)
				// continue
				return
			}
			metrics := getCollectorMetrics(stats)
			bts, _ := json.Marshal(metrics)
			infoStr := string(bts)
			data, ok := r.metrics.Load(containerId)
			if !ok {
				return
			}
			chanArray := data.([]chan string)
			for _, ch := range chanArray {
				if len(ch) < cap(ch) {
					ch <- infoStr
				}
			}
		default:
		}
	}
}

func (r *Collector) collectorLog(containerId string) {
	log, closeLogChan, err := r.controller.LogChan(containerId)
	if err != nil {
		//to do 打印日志
		return
	}
	logChan := log.GetChan()
	for {
		select {
		case <-r.ctx.Done():
			r.logInfo.Range(func(key, value interface{}) bool {
				val := value.([]chan string)
				for _, c := range val {
					close(c)
				}
				return true
			})
			if err = closeLogChan(); err != nil {
				logrus.WithContext(r.ctx).WithFields(logrus.Fields{
					define.LogsPrintCommonLabelOfFunction: "container.log.push",
				}).Warnf("close log chan %v", err)
			}
			return
		case infoStr := <-logChan:
			data, ok := r.logInfo.Load(containerId)
			if !ok {
				return
			}
			chanArray := data.([]chan string)
			for _, ch := range chanArray {
				if len(ch) < cap(ch) {
					ch <- infoStr
				}
			}
		default:
		}
	}
}

func getCollectorMetrics(stats *types.StatsJSON) *CollectorMetrics {
	var (
		memPercent        = 0.0
		cpuPercent        = 0.0
		blkRead, blkWrite uint64
		mem               = 0.0
		memLimit          = 0.0
		pids              uint64
		netRx             = 0.0
		netTx             = 0.0
	)
	//cpu
	cpuDelta := float64(stats.CPUStats.CPUUsage.TotalUsage - stats.PreCPUStats.CPUUsage.TotalUsage)
	systemDelta := float64(stats.CPUStats.SystemUsage - stats.PreCPUStats.SystemUsage)
	cpuPercent = (cpuDelta / systemDelta) * float64(len(stats.CPUStats.CPUUsage.PercpuUsage)) * 100.0
	//memory
	mem = float64(stats.MemoryStats.Usage)
	memLimit = float64(stats.MemoryStats.Limit)
	if stats.MemoryStats.Limit != 0 {
		memPercent = float64(stats.MemoryStats.Usage) / float64(stats.MemoryStats.Limit) * 100.0
	}
	//network
	for _, v := range stats.Networks {
		netRx += float64(v.RxBytes)
		netTx += float64(v.TxBytes)
	}
	//block
	var blkio = stats.BlkioStats
	for _, bioEntry := range blkio.IoServiceBytesRecursive {
		switch strings.ToLower(bioEntry.Op) {
		case "read":
			blkRead = blkRead + bioEntry.Value
		case "write":
			blkWrite = blkWrite + bioEntry.Value
		}
	}
	//pidsCurrent
	pids = stats.PidsStats.Current
	return &CollectorMetrics{
		Name:             stats.Name,
		ID:               stats.ID,
		CPUPercentage:    cpuPercent,
		Memory:           mem,
		MemoryLimit:      memLimit,
		MemoryPercentage: memPercent,
		NetworkRx:        netRx,
		NetworkTx:        netTx,
		BlockRead:        float64(blkRead),
		BlockWrite:       float64(blkWrite),
		PidsCurrent:      pids,
		Timestamp:        stats.Read,
	}
}

func (r *Collector) Close() error {
	r.cancel()
	return nil
}
