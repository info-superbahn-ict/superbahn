package collector_docker

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/third_party/docker"
	"testing"
	"time"
)

const defaultVersion = "v1.41"

func TestCollectorDocker_Output(t *testing.T) {
	var Id = "e383272c612e"
	dockerCli, _ := docker.NewDockerController(context.Background(), "tcp://10.16.0.180:23750")
	cli, _ := NewCollectorDocker(context.Background(), dockerCli, 1000)
	msg, _ := cli.OutputMetrics(Id)
	t1 := time.Now()
	for info := range msg {
		t2 := time.Now()
		fmt.Println(t2.Sub(t1))
		t1 = t2
		fmt.Println(info)

	}

	cli.Close()
	info, ok := <-msg
	if !ok {
		fmt.Println("OK!!")
	} else {
		fmt.Println("close failed")
		fmt.Println(info)
	}
}

func TestCollector_Close(t *testing.T) {
	var Id = "8830b732c9afff6665b67a2429df73901f9a9eed90ffa72f492be9054cf80a04"
	dockerCli, _ := docker.NewDockerController(context.Background(), "tcp://10.16.0.180:23750")
	cli, _ := NewCollectorDocker(context.Background(), dockerCli, 1000)
	msg, _ := cli.OutputMetrics(Id)
	cli.Close()
	info, ok := <-msg
	if !ok {
		fmt.Println("OK!!")
	} else {
		fmt.Println("close failed")
		fmt.Println(info)
	}
}
