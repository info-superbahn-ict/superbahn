package collector_jaeger

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"

	"github.com/gogo/protobuf/jsonpb"
	"github.com/jaegertracing/jaeger/model"

	"io/ioutil"
	"net/http"
	"sync"
	"time"

	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

const (
	maxSize  = 1024
	RootPath = "supbagent.collector.collector_jaeger."
)

type count struct {
	num  int
	lock sync.Mutex
}

func (c *count) add() int {
	defer c.lock.Unlock()
	c.lock.Lock()
	c.num++

	return c.num
}

var counter = count{num: 0}

type MessageSupb struct {
	Key  string `json:"key"`
	Data string `json:"data"`
}

//type RawSpan []byte
type Collector struct {
	ctx    context.Context
	cancel context.CancelFunc
	msg    *sync.Map
	url    string
	route  string
	//nervous supbnervous.Controller
}

//type HttpServerParmas struct {
//	HostPort     string
//	CollectorAPI string
//	// todo tls安全相关
//}

func NewCollectorJaeger(ctx context.Context, url, route string) (*Collector, error) {
	collector := &Collector{
		url:   url,
		route: route,
		msg:   &sync.Map{},
		//nervous: nu,
	}

	collector.ctx, collector.cancel = context.WithCancel(ctx)

	go collector.run()
	return collector, nil
}

//func (r *Collector) listentKafka() {
//	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
//		define.LogsPrintCommonLabelOfFunction: RootPath + "listentKafka",
//	})
//	err := r.nervous.Subscribe("jaeger_traces")
//
//	if err != nil {
//		log.Infof("subscribe error: %v", err)
//	}
//
//	log.Infof("Start Listen")
//
//	tk := time.NewTicker(time.Microsecond * time.Duration(10))
//	for {
//		select {
//		case <-tk.C:
//			mg, err := r.nervous.Receive("jaeger_traces")
//			if err != nil {
//				log.Infof("receive error: %v", err)
//				continue
//			}
//
//			log.Debugf("topic:%v key: %s", mg.Topic, mg.SlaveKey)
//			r.saveSpan(mg.Value)
//		case <-r.ctx.Done():
//			return
//		}
//	}
//}

/*
   jaeger-collector监听
*/
func (r *Collector) listensCollector() {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "listensCollector",
	})

	handler := http.NewServeMux()
	handler.HandleFunc(r.route, r.handleSpan)

	server := &http.Server{
		Addr:         r.url,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		Handler:      handler,
	}

	if err := server.ListenAndServe(); err != nil {
		log.Debugf("server stop %v", err)
	}

	log.Debugf("jaeger collector closed")
}

func (r *Collector) saveSpanRest(spanStr string) {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "getSpan",
	})

	message := &MessageSupb{}
	if err := json.Unmarshal([]byte(spanStr), message); err != nil {
		log.Errorf("unmarshall span err %v", err)
		return
	}

	span := &model.Span{}
	//span := &jaeger_model.Span{}

	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(message.Data)), span); err != nil {
		log.Errorf("unmarshall span err %v", err)
		return
	}

	// log.Infof("unmarshall over")
	// log.Infof("spanStr:%s",spanStr)
	//log.Debugf("unmarshall dada  tags %v",span.Tags)

	var kvs model.KeyValues = span.Process.Tags
	//var kvs jaeger_model.KeyValues = span.Process.Tags
	kv, found := kvs.FindByKey("SUPB_GUID")

	if !found {
		log.Errorf("Error Parsing GUID")
		return
	}

	guid := kv.VStr
	// log.Debugf("Jaeger collect guid: %v, total span collected: %v", guid, counter.add())

	if straceData, ok := r.msg.Load(guid); ok {
		for _, ch := range straceData.([]chan string) {
			if len(ch) < cap(ch) {
				ch <- spanStr
			}
		}
	}
}

func (r *Collector) saveSpanKafka(spanStr string) {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "getSpan",
	})

	var err error
	span := &model.Span{}
	//span := &jaeger_model.Span{}

	if err = jsonpb.Unmarshal(bytes.NewReader([]byte(spanStr)), span); err != nil {
		log.Errorf("unmarshall span err %v", err)
		return
	}

	// log.Infof("unmarshall over")
	log.Infof("spanStr:%s", spanStr)
	//log.Debugf("unmarshall dada  tags %v",span.Tags)

	var kvs model.KeyValues = span.Process.Tags
	//var kvs jaeger_model.KeyValues = span.Process.Tags
	kv, found := kvs.FindByKey("SUPB_GUID")

	if !found {
		log.Errorf("Error Parsing GUID")
		return
	}

	guid := kv.VStr
	// log.Debugf("Jaeger collect guid: %v, total span collected: %v", guid, counter.add())

	if straceData, ok := r.msg.Load(guid); ok {
		for _, ch := range straceData.([]chan string) {
			if len(ch) < cap(ch) {
				ch <- spanStr
			}
		}
	}
}

func (r *Collector) handleSpan(w http.ResponseWriter, rq *http.Request) {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "handleSpan",
	})

	// log.Infof("recv a trace")

	data, err := ioutil.ReadAll(rq.Body)
	_ = rq.Body.Close()

	if err != nil {
		log.Printf("err")
		return
	}
	_ = rq.Body.Close()

	r.saveSpanRest(string(data))
}

func (r *Collector) run() {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "run",
	})

	log.Infof("jaeger collector start listen...")

	go r.listensCollector()
	// go r.listentKafka()

	for range r.ctx.Done() {
		r.msg.Range(func(key, value interface{}) bool {
			val := value.([]chan string)
			for _, c := range val {
				close(c)
			}
			return true
		})
		return
	}
}

/*
	收集 jaeger-collector 的span数据
*/
func (r *Collector) Output(guid string) (<-chan string, error) {

	// 对于每个调用者，都产生一个 =单独channel
	ch := make(chan string, maxSize)

	if data, ok := r.msg.Load(guid); !ok {
		var newChanArray []chan string

		newChanArray = append(newChanArray, ch)
		r.msg.Store(guid, newChanArray)

	} else {
		chanArray := append(data.([]chan string), ch)
		r.msg.Store(guid, chanArray)
	}

	return ch, nil
}

func (r *Collector) CloseChan(guid string) error {

	if _, ok := r.msg.Load(guid); !ok {
		return fmt.Errorf("no such guid")
	} else {
		r.msg.Delete(guid)
	}
	return nil
}

func (r *Collector) Close() error {
	r.cancel()
	return nil
}
