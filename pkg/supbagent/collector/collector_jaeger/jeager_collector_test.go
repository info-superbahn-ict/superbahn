package collector_jaeger

import (
	"bytes"
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/options"
	// kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"testing"
	"time"

	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
)

func TestJaegerCollector_Output(t *testing.T) {

	ctx :=context.Background()
	// nu, err := kafkaNervous.NewNervous(ctx, "../../config/nervous_config.json", "test")
	// if err != nil {
	// 	fmt.Printf("new kafka %v", err)
	// }

	// cli, _ := NewCollectorJaeger(ctx,options.DefaultJaegerUrl,options.DefaultJaegerRoute,nu)
	cli, _ := NewCollectorJaeger(ctx,options.DefaultJaegerUrl,options.DefaultJaegerRoute)
	msg, _ := cli.Output("test_guid")

	spanInfo := <-msg

	fmt.Println(spanInfo)
	cli.Close()
}

// go test -v -run=TestServer
func TestServer(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	serve := http.NewServeMux()
	serve.HandleFunc("/get", get)

	server := &http.Server{
		Addr:         "10.16.0.180:10000",
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		Handler:      serve,
	}

	go func() {
		if err := server.ListenAndServe(); err != nil {
			logrus.WithContext(ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "server.web.Run",
			}).Infof("server stop %v", err)
		}
		logrus.WithContext(ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "server.web.Run",
		}).Infof("listen closed")
	}()

	time.Sleep(5 * time.Second)

	fmt.Printf("\nstart server\n")

	<-ctx.Done()
	//fmt.Printf("res: %v",resp.Body)
}

func get(w http.ResponseWriter, r *http.Request) {
	//time.Sleep(time.Second *2)

	if bt, err := w.Write([]byte("hello")); err != nil {
		logrus.Errorf("get function %v", err)
	} else {
		logrus.Infof(fmt.Sprintf("GET REPONSE (%vB)", bt))
	}
}

func TestClient(t *testing.T) {
	cli := http.Client{
		Timeout: time.Second * 1,
	}

	resp, err := cli.Post("http://10.16.0.180:10000/get", "application/json", bytes.NewBuffer([]byte("dd  ")))
	if err != nil {
		fmt.Printf("\ne: %v\n", err)
		return
	}
	defer resp.Body.Close()
	bts, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("read body %v", err)
	}

	fmt.Printf("\nres: %s\n", bts)
}
