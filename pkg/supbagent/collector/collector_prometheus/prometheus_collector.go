package collector_prometheus

import (
	"context"
	"encoding/json"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/mem"
	"github.com/shirou/gopsutil/net"
	"time"
)

const maxSize = 1024

type Collector struct {
	ctx      context.Context
	cancel   context.CancelFunc
	msg      []chan string
	interval time.Duration //毫秒
}

type CollectorMetrics struct {
	MemTotal        uint64    `json:"memTotal"`
	MemAvailable    uint64    `json:"memAvailable"`
	MemUsedPercent  float64   `json:"memUsedPercent"`
	CpuCount        int       `json:"cpuCount"`
	CpuTotalPercent float64   `json:"cpuTotalPercent"`
	BytesSent       uint64    `json:"bytesSent"`
	BytesRecv       uint64    `json:"bytesRecv"`
	Timestamp       time.Time `json:"timestamp"`
}

func NewCollectorPrometheus(ctx context.Context, interval int64) (*Collector, error) {
	var collector = &Collector{
		msg:      []chan string{},
		interval: time.Duration(interval) * time.Millisecond,
	}
	collector.ctx, collector.cancel = context.WithCancel(ctx)
	return collector, nil
}

/*
	收集指定主机的监控数据
*/
func (r *Collector) Output() (<-chan string, error) {
	ch := make(chan string, maxSize)
	if len(r.msg) == 0 {
		go r.run()
	}
	r.msg = append(r.msg, ch)
	return ch, nil
}

func (r *Collector) run() {
	timer := time.NewTimer(r.interval)
	defer timer.Stop()
	for {
		select {
		case <-r.ctx.Done():
			for _, ch := range r.msg {
				close(ch)
			}
			return
		case <-timer.C:
			timer.Reset(r.interval)
			info := r.getCollectorMetrics()
			bts, _ := json.Marshal(info)
			infoStr := string(bts)
			for _, ch := range r.msg {
				if len(ch) < cap(ch) {
					ch <- infoStr
				}
			}
		default:
		}
	}
}

func (r *Collector) getCollectorMetrics() *CollectorMetrics {
	info := &CollectorMetrics{}
	info.Timestamp = time.Now()

	v, _ := mem.VirtualMemory()
	n, _ := net.IOCounters(false)
	count, _ := cpu.Counts(true)
	cpuPercent, _ := cpu.Percent(r.interval, false)

	info.MemTotal = v.Total
	info.MemAvailable = v.Available
	info.MemUsedPercent = v.UsedPercent
	info.BytesRecv = n[0].BytesRecv
	info.BytesSent = n[0].BytesSent
	info.CpuCount = count
	info.CpuTotalPercent = cpuPercent[0]

	return info
}

func (r *Collector) Close() error {
	r.cancel()
	return nil
}
