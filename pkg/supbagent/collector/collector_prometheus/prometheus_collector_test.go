package collector_prometheus

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
)

func TestCollectorPrometheus_Close(t *testing.T) {
	x, _ := NewCollectorPrometheus(context.Background(), 1000)
	msg, _ := x.Output()
	// x.Start()
	for i := 0; i <= 50; i++ {
		mg, ok := <-msg
		if !ok {
			continue
		}
		str, _ := json.Marshal(mg)
		fmt.Println(string(str))
	}
	x.Close()
}
