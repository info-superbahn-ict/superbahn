package controller

import (
	"context"
	"fmt"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/controller/controller_cgroup"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/controller/controller_docker"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/options"
	"gitee.com/info-superbahn-ict/superbahn/third_party/docker"
)

const (
	prometheusInterval = 1
)

type Manager struct {
	ctx context.Context

	docker *controller_docker.ControllerDocker
	cgroup *controller_cgroup.ControllerCgroup
}

func NewController(ctx context.Context, opt *options.Options) (*Manager, error) {

	dk, err := docker.NewDockerController(ctx, opt.DockerUrl)
	if err != nil {
		return nil, fmt.Errorf("new controller_docker controller %v", err)
	}

	cg, err := controller_cgroup.NewControllerCgroup(ctx, opt.ContainerCgroupPerfix)
	if err != nil {
		return nil, fmt.Errorf("new controller_docker controller %v", err)
	}

	m := &Manager{ctx: ctx}

	m.docker, err = controller_docker.NewControllerDocker(ctx, dk)
	if err != nil {
		return nil, fmt.Errorf("new collector_docker client %v", err)
	}

	m.cgroup = cg

	return m, nil
}

func (r *Manager) GetContainerController() *controller_docker.ControllerDocker {
	return r.docker
}

func (r *Manager) GetCgroupController() *controller_cgroup.ControllerCgroup {
	return r.cgroup
}
