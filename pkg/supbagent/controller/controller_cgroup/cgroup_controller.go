package controller_cgroup

import (
	"bytes"
	"context"
	"encoding/binary"
	"fmt"
	"hash/adler32"
	"math"

	"github.com/containerd/cgroups"
	"github.com/opencontainers/runtime-spec/specs-go"
)

const (
	pathTypeSystem = `/system.slice`
	pathTypeDocker = `/docker`
)

type ControllerCgroup struct {
	ctx     context.Context
	cgroups map[string]cgroups.Cgroup
	getPath GetContainerCgroupPath
}

type GetContainerCgroupPath func(containerID string) (string, error)

func NewControllerCgroup(context context.Context, pathType string) (*ControllerCgroup, error) {
	var function GetContainerCgroupPath

	switch pathType {
	case pathTypeSystem:
		function = GetCgroupPathTypeSystem
	case pathTypeDocker:
		function = GetCgroupPathTypeDocker
	default:
		return nil, fmt.Errorf("invalid container cgroup path type: %v", pathType)
	}

	return &ControllerCgroup{
		ctx:     context,
		cgroups: make(map[string]cgroups.Cgroup),
		getPath: function,
	}, nil
}

// cgroupPath 指 subsystems 的相对路径，如/sys/fs/cgroup/cpu/ 目录下的相对路径 /test
// 在Ubuntu中，对于容器而言，相对路径即 /docker/{containerID<no-trunc>}
func GetCgroupPathTypeSystem(containerId string) (string, error) {
	return fmt.Sprintf("/system.slice/docker-%v.scope", containerId), nil
}
func GetCgroupPathTypeDocker(containerId string) (string, error) {
	return fmt.Sprintf("/docker/%v", containerId), nil
}

func (c *ControllerCgroup) GetCgroupPath(containerId string) (string, error) {
	return c.getPath(containerId)
}

func (c *ControllerCgroup) updateLinuxResources(cgroupPath string, resources *specs.LinuxResources) error {
	cgroup, ok := c.cgroups[cgroupPath]

	if !ok || cgroup == nil {
		var err error
		cgroup, err = cgroups.Load(cgroups.V1, cgroups.StaticPath(cgroupPath))
		c.cgroups[cgroupPath] = cgroup

		if err != nil {
			return err
		}
	}

	return cgroup.Update(resources)
}

func (c *ControllerCgroup) UpdateCpuResources(cgroupPath string, ratio float64) error {
	quota := int64(math.Ceil(100000 * ratio))
	period := uint64(100000)

	return c.updateLinuxResources(cgroupPath, &specs.LinuxResources{
		CPU: &specs.LinuxCPU{
			Quota:  &quota,
			Period: &period,
		},
	})
}

func (c *ControllerCgroup) UpdateCpuShares(cgroupPaths []string, ratio []int) error {
	errs := new(bytes.Buffer)
	for k, v := range cgroupPaths {
		share := uint64(ratio[k])
		err := c.updateLinuxResources(v, &specs.LinuxResources{
			CPU: &specs.LinuxCPU{
				Shares: &share,
			},
		})
		if err != nil {
			errs.WriteString(fmt.Sprintf("%v\n", err))
		}
	}

	if errs.Len() <= 0 {
		return nil
	} else {
		return fmt.Errorf("%v", errs.String())
	}
}

func (c *ControllerCgroup) UpdateCpuSets(cgroupPath, cpuSet string) error {

	// todo 验证输入限制

	return c.updateLinuxResources(cgroupPath, &specs.LinuxResources{
		CPU: &specs.LinuxCPU{
			Cpus: cpuSet,
		},
	})
}

// 使用 classId 区分数据包
func (c *ControllerCgroup) UpdateNetResources(cgroupPath string) error {
	var classId uint32

	// cgroup path hash
	hash := adler32.New()
	byteBuffer := hash.Sum([]byte(cgroupPath))
	err := binary.Read(bytes.NewBuffer(byteBuffer), binary.BigEndian, &classId)

	if err != nil {
		return err
	}

	fmt.Printf("classId %v", classId)

	return c.updateLinuxResources(cgroupPath, &specs.LinuxResources{
		Network: &specs.LinuxNetwork{
			ClassID:    &classId,
			Priorities: nil,
		},
	})
}

// todo control memory resources
func (c *ControllerCgroup) UpdateMemResources() error {
	return nil
}
