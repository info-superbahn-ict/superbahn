package controller_cgroup

import (
	"context"
	"testing"
)

func TestUpdateCpuResources(t *testing.T) {
	testCgroupPath := "/docker/bdbf8121fefe6ff74f80027024767ac24495dbc5bb145df959226f243b99767f"

	cc, err := NewControllerCgroup(context.Background(), pathTypeDocker)

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	err = cc.UpdateCpuResources(testCgroupPath, 0.2)

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

}

func TestUpdateCpuSets(t *testing.T) {
	testCgroupPath := "/docker/c13df1bf575ec82e91ae926af0090254e0c082e8a3d742d9fd60ed1b7570052b"

	cc, err := NewControllerCgroup(context.Background(), pathTypeDocker)

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	err = cc.UpdateCpuSets(testCgroupPath, "0")

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

}

func TestUpdateNetResources(t *testing.T) {
	testCgroupPath := "/docker/9f68b926f29d23f05dff079d9ad79ff4e6596786985dcbe21c8bfce88dd645f9"

	cc, err := NewControllerCgroup(context.Background(), pathTypeDocker)

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	err = cc.UpdateNetResources(testCgroupPath)

	if err != nil {
		t.Error(err)
		t.FailNow()
	}
}

func TestCPUDiffrentPaths(t *testing.T) {
	testCgroupPath := "/system.slice/docker-dc43f06c6be96e733f14c095f7c44678af086e86638b3f7e667fbe6e4fbaad46.scope"

	cc, err := NewControllerCgroup(context.Background(), pathTypeSystem)

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	err = cc.UpdateCpuResources(testCgroupPath, 0.2)

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

}

func TestCPUSetDiffrentPaths(t *testing.T) {
	testCgroupPath := "/system.slice/docker-dc43f06c6be96e733f14c095f7c44678af086e86638b3f7e667fbe6e4fbaad46.scope"

	cc, err := NewControllerCgroup(context.Background(), pathTypeSystem)

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	err = cc.UpdateCpuSets(testCgroupPath, "0")

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

}
