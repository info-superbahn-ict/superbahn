package controller_docker

import (
	"context"

	"gitee.com/info-superbahn-ict/superbahn/third_party/docker"
)

type ControllerDocker struct {
	ctx context.Context
	dk  docker.Controller
}

func NewControllerDocker(ctx context.Context, controller docker.Controller) (*ControllerDocker, error) {
	return &ControllerDocker{
		ctx: ctx,
		dk:  controller,
	}, nil
}

// func (r *ControllerDocker) StartStrategy(imageTag string, containerName string, envs map[string]string) (*docker.ContainerInformation, error) {
// 	return r.dk.StartStrategy(imageTag, containerName, envs)
// }

// func (r *ControllerDocker) StartContainer(imageTag string, containerName string, envs map[string]string) (*docker.ContainerInformation, error) {
// 	return r.dk.StartContainer(imageTag, containerName, envs)
// }

// func (r *ControllerDocker) StartContainerWithPorts(imageTag string, containerName string, envs map[string]string, ports map[string]string) (*docker.ContainerInformation, error) {
// 	return r.dk.StartContainerWithPorts(imageTag, containerName, envs, ports)
// }

func (r *ControllerDocker) StartContainer(imageTag , containerName, runtime,workDir,imagePullPolicy string, envs, ports,lifecycle,livenessProbe,readinessProbe,reasources,securityContext,volumeMounts map[string]string, cmds,terminationMessagePath,terminationMessagePolicy []string) (*docker.ContainerInformation, error) {
	return r.dk.StartContainerWithPortsForAgent(imageTag, containerName,runtime,workDir,imagePullPolicy, envs, ports,lifecycle,livenessProbe,readinessProbe,reasources,securityContext,volumeMounts, cmds ,terminationMessagePath,terminationMessagePolicy)
}

func (r *ControllerDocker) Contains(imageTag string) (bool, error) {
	return r.dk.Contains(imageTag)
}

func (r *ControllerDocker) ListTags() ([]string, error) {
	return r.dk.ListTags()
}

func (r *ControllerDocker) ListContainerIds() ([]string, error) {
	return r.dk.ListContainIds()
}

func (r *ControllerDocker) Stop(containerId string) error {
	return r.dk.Stop(containerId)
}

func (r *ControllerDocker) Resume(containerId string) error {
	return r.dk.Resume(containerId)
}

func (r *ControllerDocker) ReStart(containerId string) error {
	return r.dk.ReStart(containerId)
}

func (r *ControllerDocker) Remove(containerId string) error {
	return r.dk.Remove(containerId)
}

func (r *ControllerDocker) ContainerExec(containerId string, cmd []string) error {
	return r.dk.ContainerExec(containerId, cmd)
}

func (r *ControllerDocker) Close() error {
	return r.dk.Close()
}
