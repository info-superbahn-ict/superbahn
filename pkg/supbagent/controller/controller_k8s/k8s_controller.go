package controller_k8s

import (
	"bytes"
	"context"
	"io"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer/yaml"
	yamlutil "k8s.io/apimachinery/pkg/util/yaml"
	v1 "k8s.io/client-go/applyconfigurations/core/v1"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/restmapper"
)

type ControllerK8s struct {
	ctx context.Context

	kcs *kubernetes.Clientset
	dc  dynamic.Interface
}

func NewControllerK8s(ctx context.Context, kcs *kubernetes.Clientset, dc dynamic.Interface) (*ControllerK8s, error) {

	return &ControllerK8s{
		ctx: ctx,
		kcs: kcs,
		dc:  dc,
	}, nil
}

// Pod
func (r *ControllerK8s) GetPods(namespace string) (*corev1.PodList, error) {
	return r.kcs.CoreV1().Pods(namespace).List(r.ctx, metav1.ListOptions{})
}

func (r *ControllerK8s) GetPod(namespace, pod string) (*corev1.Pod, error) {
	return r.kcs.CoreV1().Pods(namespace).Get(r.ctx, pod, metav1.GetOptions{})
}

// Deployment
func (r *ControllerK8s) GetDeployment(namespace, deploymentName string) (*appsv1.Deployment, error) {
	return r.kcs.AppsV1().Deployments(namespace).Get(r.ctx, deploymentName, metav1.GetOptions{})
}

func (r *ControllerK8s) CreateDeployment(namespace string, deployment *appsv1.Deployment) (*appsv1.Deployment, error) {
	return r.kcs.AppsV1().Deployments(namespace).Create(r.ctx, deployment, metav1.CreateOptions{})
}

func (r *ControllerK8s) UpdateDeployment(namespace string, deployment *appsv1.Deployment) (*appsv1.Deployment, error) {
	return r.kcs.AppsV1().Deployments(namespace).Update(r.ctx, deployment, metav1.UpdateOptions{})
}

func (r *ControllerK8s) DeleteDeployment(namespace, deploymentName string) error {
	defaultDeletePolicy := metav1.DeletePropagationForeground
	return r.kcs.AppsV1().Deployments(namespace).Delete(r.ctx, deploymentName, metav1.DeleteOptions{PropagationPolicy: &defaultDeletePolicy})
}

// Service
func (r *ControllerK8s) GetService(namespace, serviceName string) (*corev1.Service, error) {
	return r.kcs.CoreV1().Services(namespace).Get(r.ctx, serviceName, metav1.GetOptions{})
}

func (r *ControllerK8s) ApplyService(namespace string, service *v1.ServiceApplyConfiguration) (*corev1.Service, error) {
	return r.kcs.CoreV1().Services(namespace).Apply(r.ctx, service, metav1.ApplyOptions{})
}

func (r *ControllerK8s) DeleteService(serviceName, namespace string) error {
	return r.kcs.CoreV1().Services(namespace).Delete(r.ctx, serviceName, metav1.DeleteOptions{})
}

// Object
func (r *ControllerK8s) CreateObject(res schema.GroupVersionResource, namespace string, obj *unstructured.Unstructured) (*unstructured.Unstructured, error) {
	return r.dc.Resource(res).Namespace(namespace).Create(r.ctx, obj, metav1.CreateOptions{})
}

func (r *ControllerK8s) UpdateObject(res schema.GroupVersionResource, namespace string, obj *unstructured.Unstructured) (*unstructured.Unstructured, error) {
	return r.dc.Resource(res).Namespace(namespace).Update(r.ctx, obj, metav1.UpdateOptions{})
}

func (r *ControllerK8s) DeleteObject(res schema.GroupVersionResource, name, namespace string, obj *unstructured.Unstructured) error {
	return r.dc.Resource(res).Namespace(namespace).Delete(r.ctx, name, metav1.DeleteOptions{})
}

// Yaml
func (r *ControllerK8s) ApplyYaml(bt []byte) error {
	decoder := yamlutil.NewYAMLOrJSONDecoder(bytes.NewReader(bt), 100)

	var err error
	for {
		var rawObj runtime.RawExtension
		if err = decoder.Decode(&rawObj); err != nil {
			break
		}

		obj, gvk, err := yaml.NewDecodingSerializer(unstructured.UnstructuredJSONScheme).Decode(rawObj.Raw, nil, nil)

		if err != nil {
			return err
		}

		unstructuredMap, err := runtime.DefaultUnstructuredConverter.ToUnstructured(obj)
		if err != nil {
			return err
		}

		unstructuredObj := &unstructured.Unstructured{Object: unstructuredMap}

		gr, err := restmapper.GetAPIGroupResources(r.kcs.Discovery())
		if err != nil {
			return err
		}

		mapper := restmapper.NewDiscoveryRESTMapper(gr)
		mapping, err := mapper.RESTMapping(gvk.GroupKind(), gvk.Version)
		if err != nil {
			return err
		}

		var dri dynamic.ResourceInterface
		if mapping.Scope.Name() == meta.RESTScopeNameNamespace {
			if unstructuredObj.GetNamespace() == "" {
				unstructuredObj.SetNamespace("default")
			}
			dri = r.dc.Resource(mapping.Resource).Namespace(unstructuredObj.GetNamespace())
		} else {
			dri = r.dc.Resource(mapping.Resource)
		}

		if _, err := dri.Create(r.ctx, unstructuredObj, metav1.CreateOptions{}); err != nil {
			return err
		}
	}

	if err != io.EOF {
		return err
	}

	return nil
}

func (r *ControllerK8s) DeleteYaml(bt []byte) error {
	decoder := yamlutil.NewYAMLOrJSONDecoder(bytes.NewReader(bt), 100)

	var err error
	for {
		var rawObj runtime.RawExtension
		if err = decoder.Decode(&rawObj); err != nil {
			break
		}

		obj, gvk, err := yaml.NewDecodingSerializer(unstructured.UnstructuredJSONScheme).Decode(rawObj.Raw, nil, nil)

		if err != nil {
			return err
		}

		unstructuredMap, err := runtime.DefaultUnstructuredConverter.ToUnstructured(obj)
		if err != nil {
			return err
		}

		unstructuredObj := &unstructured.Unstructured{Object: unstructuredMap}

		gr, err := restmapper.GetAPIGroupResources(r.kcs.Discovery())
		if err != nil {
			return err
		}

		mapper := restmapper.NewDiscoveryRESTMapper(gr)
		mapping, err := mapper.RESTMapping(gvk.GroupKind(), gvk.Version)
		if err != nil {
			return err
		}

		var dri dynamic.ResourceInterface
		if mapping.Scope.Name() == meta.RESTScopeNameNamespace {
			if unstructuredObj.GetNamespace() == "" {
				unstructuredObj.SetNamespace("default")
			}
			dri = r.dc.Resource(mapping.Resource).Namespace(unstructuredObj.GetNamespace())
		} else {
			dri = r.dc.Resource(mapping.Resource)
		}

		if err := dri.Delete(r.ctx, unstructuredObj.GetName(), metav1.DeleteOptions{}); err != nil {
			return err
		}
	}

	if err != io.EOF {
		return err
	}

	return nil
}
