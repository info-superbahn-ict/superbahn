package controller_k8s

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"testing"
	"time"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

func genTestKCS(kubeconfig string) (*kubernetes.Clientset, error) {
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		panic(err.Error())
	}

	// create the clientset
	clientset, err := kubernetes.NewForConfig(config)

	if err != nil {
		panic(err.Error())
	}

	return clientset, nil
}

func genTestKDC(kubeconfig string) (dynamic.Interface, error) {
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		panic(err.Error())
	}

	// create the clientset
	dc, err := dynamic.NewForConfig(config)

	if err != nil {
		panic(err.Error())
	}

	return dc, nil
}

func TestGetPods(t *testing.T) {
	kubeconfig := "/home/fhl/.kube/config"
	testNamespace := ""

	kc, _ := genTestKCS(kubeconfig)

	ck, err := NewControllerK8s(context.Background(), kc, nil)
	if err != nil {
		t.Error(err)
		t.Failed()
	}

	pods, err := ck.GetPods(testNamespace)
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("There are %d pods in the cluster namespace %s\n", len(pods.Items), testNamespace)
}

func TestGetPod(t *testing.T) {
	testPod := "kubernetes-bootcamp-69b8fbf65b-5x6xl"
	testNamespace := "default"
	kubeconfig := "/home/fhl/.kube/config"

	kc, _ := genTestKCS(kubeconfig)

	ck, err := NewControllerK8s(context.Background(), kc, nil)
	if err != nil {
		t.Error(err)
		t.Failed()
	}

	pod, err := ck.GetPod(testNamespace, testPod)

	if errors.IsNotFound(err) {
		fmt.Printf("Pod %s in namespace %s not found\n", pod, testNamespace)
	} else if statusError, isStatus := err.(*errors.StatusError); isStatus {
		fmt.Printf("Error getting pod %s in namespace %s: %v\n",
			pod, testNamespace, statusError.ErrStatus.Message)
	} else if err != nil {
		panic(err.Error())
	} else {
		fmt.Printf("Found pod %s in namespace %s\n", pod, testNamespace)
	}

	fmt.Println("pod name: ", pod.Name)
}

func TestCreateDeployment(t *testing.T) {
	testNamespace := "default"
	kubeconfig := "/home/fhl/.kube/config"

	kc, _ := genTestKCS(kubeconfig)

	ck, err := NewControllerK8s(context.Background(), kc, nil)
	if err != nil {
		t.Error(err)
		t.Failed()
	}

	deployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name: "demo-deployment",
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: int32Ptr(2),
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{
					"app": "demo",
				},
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"app": "demo",
					},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "kubernetes-bootcamp",
							Image: "hhitzhl/kubernetes-bootcamp:v1",
							Ports: []corev1.ContainerPort{
								{
									Name:          "http",
									Protocol:      corev1.ProtocolTCP,
									ContainerPort: 8080,
								},
							},
						},
					},
				},
			},
		},
	}

	rlt, err := ck.CreateDeployment(testNamespace, deployment)

	if err != nil {
		t.Error(err)
		t.Failed()
	}

	fmt.Printf("create delployment %s\n", rlt.Name)
}

func TestUpdateDeployment(t *testing.T) {
	testNamespace := "default"
	testDeploymentName := "demo-deployment"
	kubeconfig := "/home/fhl/.kube/config"

	kc, _ := genTestKCS(kubeconfig)

	ck, err := NewControllerK8s(context.Background(), kc, nil)
	if err != nil {
		t.Error(err)
		t.Failed()
	}

	// test get deployment
	deployment, err := ck.GetDeployment(testNamespace, testDeploymentName)
	if err != nil {
		t.Error(err)
		t.Failed()
	}

	fmt.Printf("get delployment %s, replicas: %d", deployment.Name, *deployment.Spec.Replicas)

	bt, _ := json.Marshal(deployment)
	fmt.Printf("\n %s", bt)

	fmt.Println("\nthen to update ...")
	time.Sleep(5 * time.Second)

	// test update deployment
	deployment.Spec.Replicas = int32Ptr(1)

	_, err = ck.UpdateDeployment(testNamespace, deployment)
	if err != nil {
		t.Error(err)
		t.Failed()
	}
	fmt.Printf("get delployment %s, replicas: %d", deployment.Name, *deployment.Spec.Replicas)

}

func TestDeleteDeployment(t *testing.T) {
	testNamespace := "default"
	testDeploymentName := "demo-deployment"
	kubeconfig := "/home/fhl/.kube/config"

	kc, _ := genTestKCS(kubeconfig)

	ck, err := NewControllerK8s(context.Background(), kc, nil)
	if err != nil {
		t.Error(err)
		t.Failed()
	}

	err = ck.DeleteDeployment(testNamespace, testDeploymentName)
	if err != nil {
		t.Error(err)
		t.Failed()
	}
	fmt.Printf("delete deployment: %s", testDeploymentName)
}

func TestCreateObj(t *testing.T) {
	testNamespace := "default"
	kubeconfig := "/home/fhl/.kube/config"

	dc, _ := genTestKDC(kubeconfig)

	ck, err := NewControllerK8s(context.Background(), nil, dc)
	if err != nil {
		t.Error(err)
		t.Failed()
	}

	deploymentRes := schema.GroupVersionResource{Group: "apps", Version: "v1", Resource: "deployments"}

	deployment := &unstructured.Unstructured{
		Object: map[string]interface{}{
			"apiVersion": "apps/v1",
			"kind":       "Deployment",
			"metadata": map[string]interface{}{
				"name": "demo-deployment",
			},
			"spec": map[string]interface{}{
				"replicas": 2,
				"selector": map[string]interface{}{
					"matchLabels": map[string]interface{}{
						"app": "demo",
					},
				},
				"template": map[string]interface{}{
					"metadata": map[string]interface{}{
						"labels": map[string]interface{}{
							"app": "demo",
						},
					},

					"spec": map[string]interface{}{
						"containers": []map[string]interface{}{
							{
								"name":  "web",
								"image": "nginx:1.12",
								"ports": []map[string]interface{}{
									{
										"name":          "http",
										"protocol":      "TCP",
										"containerPort": 80,
									},
								},
							},
						},
					},
				},
			},
		},
	}

	ck.CreateObject(deploymentRes, testNamespace, deployment)
}

func int32Ptr(i int32) *int32 { return &i }

func TestApplyYaml(t *testing.T) {
	yamlPath := "/home/fhl/k8s/yml-test/test-app.yml"
	kubeConfig := "/home/fhl/.kube/config"

	dc, _ := genTestKDC(kubeConfig)
	kcs, _ := genTestKCS(kubeConfig)

	ck, err := NewControllerK8s(context.Background(), kcs, dc)

	if err != nil {
		t.Error(err)
		t.Failed()
	}

	bt, err := ioutil.ReadFile(yamlPath)
	if err != nil {
		t.Error(err)
		t.Failed()
	}

	err = ck.ApplyYaml(bt)
	if err != nil {
		t.Error(err)
		t.Failed()
	}

	fmt.Println("apply yaml successfully")

}

func TestDeleteYaml(t *testing.T) {
	yamlPath := "/home/fhl/k8s/yml-test/test-app.yml"
	kubeConfig := "/home/fhl/.kube/config"

	dc, _ := genTestKDC(kubeConfig)
	kcs, _ := genTestKCS(kubeConfig)

	ck, err := NewControllerK8s(context.Background(), kcs, dc)

	if err != nil {
		t.Error(err)
		t.Failed()
	}

	bt, err := ioutil.ReadFile(yamlPath)
	if err != nil {
		t.Error(err)
		t.Failed()
	}

	err = ck.DeleteYaml(bt)
	if err != nil {
		t.Error(err)
		t.Failed()
	}

	fmt.Println("delete from yaml successfully")

}
