package options

const (
	/*
		默认变量
	*/

	DefaultNervousConfig = "../../config/nervous_config.json"

	DefaultLogPath = "/home/logs/agent.log"

	DefaultPluginsPath = "../../build/plugins/agent"
	DefaultDockerUrl   = "tcp://10.16.0.180:23750"
	InitSpongeNervous  = "init_sponge_nervous"

	DefaultJaegerUrl = "10.16.0.180:10013"
	DefaultJaegerRoute = "/collector"
)
