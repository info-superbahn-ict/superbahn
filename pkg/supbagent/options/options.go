package options

type Options struct {
	NervousConfig         string
	LogPath               string
	CliServerUrl          string
	PluginPath            string
	DockerUrl             string
	JaegerCollectorUrl    string
	JaegerCollectorRoute  string
	JaegerAgentHostPort   string
	ContainerCgroupPerfix string

	HostTag string
}
