package containers

import (
	"time"

	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
)

// todo 监控数据定义 example
type ContainerMetrics struct {
	Name             string    `json:"name"`
	ID               string    `json:"id"`
	CPUPercentage    float64   `json:"cpuPercentage"`
	Memory           float64   `json:"memory"`
	MemoryPercentage float64   `json:"memoryPercentage"`
	MemoryLimit      float64   `json:"memoryLimit"`
	NetworkRx        float64   `json:"networkRx"`
	NetworkTx        float64   `json:"networkTx"`
	BlockRead        float64   `json:"blockRead"`
	BlockWrite       float64   `json:"blockWrite"`
	PidsCurrent      uint64    `json:"pidsCurrent"`
	Timestamp        time.Time `json:"timestamp"`
}

func (r *ContainerMetrics) DeepCopy() *ContainerMetrics {
	c := *r
	return &c
}

// todo 容器抽象 example
type Container struct {
	GUID          string               `json:"version"`
	ContainerName string               `json:"container_name"`
	ContainerId   string               `json:"container_id"`
	ImageName     string               `json:"image_name"`
	Envs          map[string]string    `json:"envs"`
	Ports         map[string]string    `json:"ports"`
	Tags          []spongeregister.Tag `json:"tags"`
	Cmds          []string             `json:"cmds"`
	Status        int                  `json:"status"`
	//新增 SERVE gitlab-runner字段
	ImagePullPolicy	string `json:"imagePullPolicy"`
	Lifecycle 		map[string]string `json:"lifecycle"`
	LivenessProbe	map[string]string `json:"livenessProbe"`
	ReadinessProbe	map[string]string `json:"readinessProbe"`
	Reasources		map[string]string `json:"reasources"`
	SecurityContext	map[string]string `json:"securityContext"`
	TerminationMessagePath	 []string `json:"terminationMessagePath"`
	TerminationMessagePolicy []string `json:"terminationMessagePolicy"`
	VolumeMounts	map[string]string `json:"volumeMounts"`
}

func (r *Container) DeepCopy() *Container {
	c := *r
	return &c
}
