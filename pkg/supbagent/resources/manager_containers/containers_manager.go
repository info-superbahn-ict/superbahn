package manager_containers

import (
	"context"
	"fmt"
	"runtime"
	"strconv"
	"strings"

	"gitee.com/info-superbahn-ict/superbahn/pkg/register"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/collector"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/controller"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources/manager_containers/containers"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
)

const (
	RootPath = "supbagent.resource.manager_containers."

	WatchOpCreate = "create"
	WatchOpDelete = "delete"

	MetricsPushOff = "off"
	LogsPushOff    = "off"
	TracePushOff   = "off"

	MetricT = "metric"
	LogT    = "log"
	TraceT  = "trace"

	ApplicationKey = "applicationKey"
)

var functionList = []string{
	define.RPCFunctionNameOfObjectRunningOnAgentForListFunction,
	define.RPCFunctionNameOfObjectRunningOnAgentForStartObject,
	define.RPCFunctionNameOfObjectRunningOnAgentForStopObject,
	define.RPCFunctionNameOfObjectRunningOnAgentForResumeObject,
	define.RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush,
	define.RPCFunctionNameOfObjectRunningOnAgentForOpenMetricsPush,
	define.RPCFunctionNameOfObjectRunningOnAgentForOpenTracePush,
	define.RPCFunctionNameOfObjectRunningOnAgentForGetLimitationConfig,
	define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuRatio,
	define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuSets,
	define.RPCFunctionNameOfObjectRunningOnAgentForSetNetUpstreamBandwidth,
	define.RPCFunctionNameOfObjectRunningOnAgentForSetNetLatency,
}

type ContainerWatcher struct {
	Op   string
	Guid string
}

func (r *ContainerWatcher) DeepCopy() *ContainerWatcher {
	c := *r
	return &c
}

type ContainerManager struct {
	ctx context.Context

	// map guid -- > container
	containerSet map[string]*containers.Container
	functionMap  map[string]*ContainerFunctionMap

	controller *controller.Manager
	collector  *collector.Manager

	watchers []chan *ContainerWatcher
}

func NewContainerManager(ctx context.Context, controller *controller.Manager, collector *collector.Manager) (*ContainerManager, error) {
	return &ContainerManager{
		ctx:          ctx,
		containerSet: make(map[string]*containers.Container),
		functionMap:  make(map[string]*ContainerFunctionMap),
		controller:   controller,
		collector:    collector,
		watchers:     []chan *ContainerWatcher{},
	}, nil

}

func (r *ContainerManager) ListContainers() map[string]*containers.Container {
	resp := make(map[string]*containers.Container, len(r.containerSet))

	for k, v := range r.containerSet {
		resp[k] = v.DeepCopy()
	}

	return resp
}

func (r *ContainerManager) ContainContainer(guid string) bool {
	_, ok := r.containerSet[guid]

	logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "ContainContainer",
	}).Debugf("set: %v", r.containerSet)

	return ok
}

func (r *ContainerManager) GetContainer(guid string) (*containers.Container, error) {
	v, ok := r.containerSet[guid]
	if !ok {
		return nil, fmt.Errorf("guid is nil")
	}
	return v.DeepCopy(), nil
}

/*
   描述: 暂停容器

   error:

   eg: r.StopContainer(...)
*/
func (r *ContainerManager) StopContainer(guid string) error {
	log := logrus.WithContext(context.Background()).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "DeleteContainer",
	})

	ctn, ok := r.containerSet[guid]
	if !ok {
		return fmt.Errorf("no such guid : %v", guid)
	}

	ctnId := ctn.ContainerId
	log.Info("start to stop container: ", ctnId)

	if err := r.controller.GetContainerController().Stop(ctnId); err != nil {
		return fmt.Errorf("stop container %v", err)
	}
	log.Debug("stop container: ", ctnId, "successfully")
	ctn.Status = spongeregister.StatusPause

	return nil
}

/*
   描述: 恢复容器

   error:

   eg: r.ResumeContainer(...)
*/
func (r *ContainerManager) ResumeContainer(guid string) error {
	log := logrus.WithContext(context.Background()).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "DeleteContainer",
	})

	ctn, ok := r.containerSet[guid]
	if !ok {
		return fmt.Errorf("no such guid : %v", guid)
	}

	ctnId := ctn.ContainerId
	log.Info("start to resume container: ", ctnId)

	if err := r.controller.GetContainerController().Resume(ctnId); err != nil {
		return fmt.Errorf("resume container %v", err)
	}
	log.Debug("resume container: ", ctnId, "successfully")
	ctn.Status = spongeregister.StatusPause

	return nil
}

/*
   描述: 删除容器

   error:

   eg: r.DeleteContainer(...)
*/
func (r *ContainerManager) DeleteContainer(guid string, nu supbnervous.Controller) error {
	log := logrus.WithContext(context.Background()).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "DeleteContainer",
	})

	v, ok := r.containerSet[guid]
	if !ok {
		return fmt.Errorf("guid is nil")
	}
	v.Status = spongeregister.StatusTerminating

	if err := r.controller.GetContainerController().Stop(v.ContainerId); err != nil {
		return fmt.Errorf("stop container %v", err)
	}

	if err := r.controller.GetContainerController().Remove(v.ContainerId); err != nil {
		return fmt.Errorf("remove container %v", err)
	}

	info := &spongeregister.Resource{
		GuId: guid,
	}
	if err := register.DeleteDevice(nu, info); err != nil {
		return fmt.Errorf("delete device %v", err)
	}

	err := r.functionMap[guid].close()
	if err != nil {
		return fmt.Errorf("function map close chan %v", err)
	}
	delete(r.functionMap, guid)
	delete(r.containerSet, guid)

	log.Debug("delete ", guid, " successfully")

	c := &ContainerWatcher{
		Op:   WatchOpDelete,
		Guid: guid,
	}

	for _, v := range r.watchers {
		v <- c.DeepCopy()
	}

	return nil
}

//func (r *ContainerManager) DeleteFunctionMap(guid string) {
//	_ , ok :=  r.functionMap[guid]
//	if ok {
//		delete(r.functionMap,guid)
//	}
//}

func (r *ContainerManager) DeleteAllContainers(nu supbnervous.Controller) error {
	for guid, _ := range r.containerSet {
		if err := r.DeleteContainer(guid, nu); err != nil {
			return err
		}
	}
	return nil
}

/*
   描述: 注册容器

   error:

   eg: r.RegisterContainer(...)
*/
func (r *ContainerManager) RegisterContainer(image, containerName, hostGuid string, nu supbnervous.Controller, tags []spongeregister.Tag) (string, error) {
	log := logrus.WithContext(context.Background()).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "RegisterContainer",
	})

	if containerName == "" {
		containerName = uuid.NewV4().String()
	}

	_, ok := selectTag("remove", tags)

	if !ok {
		tag := spongeregister.Tag{
			Key:   "remove",
			Type:  "string",
			Value: "auto",
		}

		tags = append(tags, tag)

	}

	// 注册 guid
	resource := &spongeregister.Resource{
		OType:       3,
		Status:      1,
		PreNode:     hostGuid,
		Description: image + containerName,
		IsControl:   1,
		IsMonitor:   1,
		Tags:        tags,
	}

	log.Debug("container register: ", resource)

	guid, err := register.Register(nu, resource)
	if err != nil {
		return "", fmt.Errorf("spongeregister GUID %v", err)
	}

	if strings.Contains(guid, `no such func`) {
		return "", fmt.Errorf("spongeregister GUID %s", guid)
	}

	logrus.Info("Register Successfully: ", guid)

	return guid, nil
}

func (r *ContainerManager)ContainerIds()[]string{
	ids,i := make([]string,len(r.containerSet)),0
	for _,v := range r.containerSet {
		ids[i]=v.ContainerId
		i++
	}
	return ids
}

/*
   描述: 创建容器

   error:

   eg: r.CreateContainer(...)
*/
func (r *ContainerManager) CreateContainer(image, containerName, runtime, workDir, imagePullPolicy,hostGuid string, envs ,ports,lifecycle,livenessProbe,readinessProbe,reasources,securityContext,volumeMounts map[string]string, cmds,terminationMessagePath,terminationMessagePolicy []string, nu supbnervous.Controller, tags []spongeregister.Tag) (string, error) {
	log := logrus.WithContext(context.Background()).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "CreateContainer",
	})

	guid, err := r.RegisterContainer(image, containerName, hostGuid, nu, tags)

	if err != nil {
		return "", err
	}

	// guid 作为环境变量传入容器
	envs["SUPB_GUID"] = guid

	info, err := r.controller.GetContainerController().StartContainer(image, containerName,runtime,workDir,imagePullPolicy, envs, ports,lifecycle,livenessProbe,readinessProbe,reasources,securityContext,volumeMounts, cmds ,terminationMessagePath,terminationMessagePolicy)
	if err != nil {
		return "", err
	}

	envsCopy := make(map[string]string, len(envs))
	for k, v := range envs {
		envsCopy[k] = v
	}

	ct := &containers.Container{
		GUID:          guid,
		ContainerName: containerName,
		ContainerId:   info.CID,
		ImageName:     image,
		Envs:          envsCopy,
		Status:        spongeregister.StatusReady,
	}

	r.containerSet[guid] = ct
	r.functionMap[guid], err = NewFunctionMap(r.ctx, r.controller, r.collector, ct, nu,r.ContainerIds)
	if err != nil {
		return "", fmt.Errorf("new function map %v", err)
	}

	c := &ContainerWatcher{
		Op:   WatchOpCreate,
		Guid: guid,
	}
	for _, v := range r.watchers {
		v <- c.DeepCopy()
	}

	log.Info("Run Container Successfully: ", guid)

	return guid, nil
}

func selectTag(key string, tags []spongeregister.Tag) (spongeregister.Tag, bool) {

	for _, tag := range tags {

		if tag.Key == key {
			return tag, true
		}
	}

	return spongeregister.Tag{}, false
}

func (r *ContainerManager) Deploy(ctns []containers.Container, hostGuid string, nu supbnervous.Controller) (map[string]clireq.ClientResponse, error) {
	log := logrus.WithContext(context.Background()).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "Deploy",
	})

	log.Debug("agent starting to deploy cotainers: ", len(ctns))

	rltMap := make(map[string]clireq.ClientResponse)

	for _, ctn := range ctns {
		tag, ok := selectTag(ApplicationKey, ctn.Tags)
		if !ok {
			return rltMap, fmt.Errorf("key error")
		}
		key := tag.Value.(string)
		log.Debug("select tag ", ApplicationKey, " : ", key)

		guid, err := r.CreateContainer(ctn.ImageName, ctn.ContainerName, "", "",ctn.ImagePullPolicy, hostGuid, ctn.Envs, ctn.Ports,ctn.Lifecycle,ctn.LivenessProbe,ctn.ReadinessProbe,ctn.Reasources,ctn.SecurityContext,ctn.VolumeMounts,ctn.Cmds, ctn.TerminationMessagePath,ctn.TerminationMessagePolicy,nu, ctn.Tags)
		if err != nil {
			return rltMap, fmt.Errorf("create failed")
		}

		rltMap[key] = clireq.ClientResponse{
			Guid:          guid,
			ContainerName: ctn.ContainerName,
			ContainerId:   ctn.ContainerId,
			ImageTag:      ctn.ImageName,
		}
	}

	return rltMap, nil
}

func (r *ContainerManager) GetContainerMetricsChan(containerId string) (<-chan string, error) {
	return r.collector.GetContainerCollector().OutputMetrics(containerId)
}

func (r *ContainerManager) GetContainerLogChan(containerId string) (string, error) {
	return r.collector.GetContainerCollector().Logs(containerId)
}

// map guid --> function name --> function
func (r *ContainerManager) GetContainersRPCFunctionMap() (map[string]map[string]func(args ...interface{}) (interface{}, error), error) {
	CtsFMap := make(map[string]map[string]func(args ...interface{}) (interface{}, error), len(r.functionMap))

	for k, FMap := range r.functionMap {
		fm := make(map[string]func(args ...interface{}) (interface{}, error))

		fm[define.RPCFunctionNameOfObjectRunningOnAgentForListFunction] = FMap.ListFunction
		fm[define.RPCFunctionNameOfObjectRunningOnAgentForStartObject] = FMap.Run
		fm[define.RPCFunctionNameOfObjectRunningOnAgentForStopObject] = FMap.Stop
		fm[define.RPCFunctionNameOfObjectRunningOnAgentForResumeObject] = FMap.Resume
		fm[define.RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush] = FMap.PushLogsOnOff
		fm[define.RPCFunctionNameOfObjectRunningOnAgentForOpenMetricsPush] = FMap.PushMetricsOnOff
		fm[define.RPCFunctionNameOfObjectRunningOnAgentForOpenTracePush] = FMap.PushTraceOnOff

		fm[define.RPCFunctionNameOfObjectRunningOnAgentForGetLimitationConfig] = FMap.GetLimitationConfig
		fm[define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuRatio] = FMap.SetCpuRatio
		fm[define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuSets] = FMap.SetCpuSets
		fm[define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuRatioShare] = FMap.SetCpuShares
		fm[define.RPCFunctionNameOfObjectRunningOnAgentForSetNetUpstreamBandwidth] = FMap.SetNetUpstreamBandwidth
		fm[define.RPCFunctionNameOfObjectRunningOnAgentForSetNetLatency] = FMap.SetNetUpstreamBandwidth

		CtsFMap[k] = fm
	}

	return CtsFMap, nil
}

// map function name --> function
func (r *ContainerManager) GetContainerRPCFunctionMap(guid string) (map[string]func(args ...interface{}) (interface{}, error), error) {
	FMap, ok := r.functionMap[guid]
	if !ok {
		return nil, fmt.Errorf("guid not exist")
	}

	fm := make(map[string]func(args ...interface{}) (interface{}, error))

	fm[define.RPCFunctionNameOfObjectRunningOnAgentForListFunction] = FMap.ListFunction
	fm[define.RPCFunctionNameOfObjectRunningOnAgentForStartObject] = FMap.Run
	fm[define.RPCFunctionNameOfObjectRunningOnAgentForStopObject] = FMap.Stop
	fm[define.RPCFunctionNameOfObjectRunningOnAgentForResumeObject] = FMap.Resume
	fm[define.RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush] = FMap.PushLogsOnOff
	fm[define.RPCFunctionNameOfObjectRunningOnAgentForOpenMetricsPush] = FMap.PushMetricsOnOff
	fm[define.RPCFunctionNameOfObjectRunningOnAgentForOpenTracePush] = FMap.PushTraceOnOff

	fm[define.RPCFunctionNameOfObjectRunningOnAgentForGetLimitationConfig] = FMap.GetLimitationConfig
	fm[define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuRatio] = FMap.SetCpuRatio
	fm[define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuRatioShare] = FMap.SetCpuShares
	fm[define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuSets] = FMap.SetCpuSets
	fm[define.RPCFunctionNameOfObjectRunningOnAgentForSetNetUpstreamBandwidth] = FMap.SetNetUpstreamBandwidth
	fm[define.RPCFunctionNameOfObjectRunningOnAgentForSetNetLatency] = FMap.SetNetLatency

	return fm, nil
}

func (r *ContainerManager) GetContainerWatcher() <-chan *ContainerWatcher {
	res := make(chan *ContainerWatcher)
	r.watchers = append(r.watchers, res)
	return res
}

func (r *ContainerManager) GetContainerResource(guid string) (LimitationConfig, error) {
	limitation, err := r.functionMap[guid].GetLimitationConfig()

	if err != nil {
		return LimitationConfig{}, err
	}

	return limitation.(LimitationConfig), nil
}

type ContainerFunctionMap struct {
	ctx              context.Context
	contain          *containers.Container
	controller       *controller.Manager
	collector        *collector.Manager
	hostNu           supbnervous.Controller

	metricsPushGuid  string
	metricsChan      <-chan string
	logsPushGuid     string
	logsChan         <-chan string
	tracePushGuid    string
	traceChan        <-chan string
	LimitationConfig LimitationConfig

	getIds           func()[]string
}

func NewFunctionMap(ctx context.Context,
	controller *controller.Manager,
	collector *collector.Manager,
	contain *containers.Container,
	nu supbnervous.Controller,
	ids func()[]string) (*ContainerFunctionMap, error) {
	chm, err := collector.GetContainerCollector().OutputMetrics(contain.ContainerId)
	if err != nil {
		return nil, fmt.Errorf("get metics chan %v", err)
	}
	chl, err := collector.GetContainerCollector().OutputLogs(contain.ContainerId)
	if err != nil {
		return nil, fmt.Errorf("get logs chan %v", err)
	}

	cht, err := collector.GetJaegerCollector().Output(contain.GUID)
	if err != nil {
		return nil, fmt.Errorf("get trace chan %v", err)
	}

	DefaultCpuRatio := 1.0
	DefaultCpuSet := fmt.Sprintf(`0-%v`, runtime.NumCPU()-1)

	// default limitation
	limit := LimitationConfig{
		CpuRatio:             DefaultCpuRatio,
		CpuSets:              DefaultCpuSet,
		NetUpStreamBandwidth: ``,
		NetLatency:           ``,
		NetBrust:             10000,
	}

	cp := &ContainerFunctionMap{
		ctx:              ctx,
		contain:          contain,
		collector:        collector,
		controller:       controller,
		getIds:     ids,
		hostNu:           nu,
		LimitationConfig: limit,
	}

	cp.metricsChan = chm
	cp.metricsPushGuid = LogsPushOff

	cp.logsChan = chl
	cp.logsPushGuid = MetricsPushOff

	cp.traceChan = cht
	cp.tracePushGuid = TracePushOff

	go cp.pushMetrics()
	go cp.pushLogs()
	go cp.pushTraces()

	return cp, nil
}

func (r *ContainerFunctionMap) ListFunction(arg ...interface{}) (interface{}, error) {
	return functionList, nil
}

func (r *ContainerFunctionMap) Run(arg ...interface{}) (interface{}, error) {
	//if err := r.controller.GetContainerController().ReStart(r.contain.ContainerId);err!=nil{
	//	return nil, err
	//}
	return fmt.Sprintf("contain %v funtion run", r.contain.ContainerId), nil

}

func (r *ContainerFunctionMap) Stop(arg ...interface{}) (interface{}, error) {
	r.contain.Status = spongeregister.StatusPending
	err := r.controller.GetContainerController().Stop(r.contain.ContainerId)
	if err != nil {
		r.contain.Status = spongeregister.StatusError
		return fmt.Sprintf("contain %v funtion stop falied", r.contain.ContainerId), err
	}
	r.contain.Status = spongeregister.StatusPause

	return fmt.Sprintf("contain %v funtion stop", r.contain.ContainerId), nil
}

func (r *ContainerFunctionMap) Resume(arg ...interface{}) (interface{}, error) {
	r.contain.Status = spongeregister.StatusPending
	err := r.controller.GetContainerController().Resume(r.contain.ContainerId)
	if err != nil {
		r.contain.Status = spongeregister.StatusError
		return fmt.Sprintf("contain %v funtion resume falied", r.contain.ContainerId), err
	}
	r.contain.Status = spongeregister.StatusReady
	return fmt.Sprintf("contain %v funtion resume", r.contain.ContainerId), nil
}

func (r *ContainerFunctionMap) PushLogsOnOff(args ...interface{}) (interface{}, error) {
	if len(args) < 1 {
		return "", fmt.Errorf("args is nil")
	}
	log := logrus.WithContext(context.Background()).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "PushLogsOnOff",
	})
	switch args[0].(string) {
	case LogsPushOff:
		log.Infof("push log OFF")
		r.logsPushGuid = args[0].(string)
	default:
		log.Infof("push log ON %v", args[0].(string))
		r.logsPushGuid = args[0].(string)
	}
	return "SET SUCCEED", nil
}

func (r *ContainerFunctionMap) PushMetricsOnOff(args ...interface{}) (interface{}, error) {
	if len(args) < 1 {
		return "", fmt.Errorf("args is nil")
	}
	log := logrus.WithContext(context.Background()).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "PushMetricsOnOff",
	})
	switch args[0].(string) {
	case MetricsPushOff:
		log.Infof("push metrics OFF")
		r.metricsPushGuid = args[0].(string)

	default:
		log.Infof("push metrics ON %v", args[0].(string))
		r.metricsPushGuid = args[0].(string)

	}
	return "SET SUCCEED", nil
}

func (r *ContainerFunctionMap) PushTraceOnOff(args ...interface{}) (interface{}, error) {
	if len(args) < 1 {
		return "", fmt.Errorf("args is nil")
	}
	log := logrus.WithContext(context.Background()).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "PushTraceOnOff",
	})
	switch args[0].(string) {
	case TracePushOff:
		log.Infof("push trace OFF")
		r.tracePushGuid = args[0].(string)
	default:
		log.Infof("push trace ON %v", args[0].(string))
		r.tracePushGuid = args[0].(string)
	}
	return "SET SUCCEED", nil
}

func (r *ContainerFunctionMap) pushMetrics() {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "pushMetrics",
	})

	log.Infof("init push metics %v", r.contain.GUID)
	for {
		select {
		case metrics := <-r.metricsChan:
			guid := r.metricsPushGuid
			//log.Infof("metrics guid %v",guid)
			if guid != MetricsPushOff {
				log.Debugf("recive logs %v, push to %v", r.contain.GUID, guid)
				_, err := r.hostNu.RPCCallCustom(guid, 10, 500, define.RPCFunctionNameOfManagerStrategyCollectorObjectMetrics, r.contain.GUID, metrics)
				if err != nil {
					log.Warnf("push metrics to manager %v", err)
				}
			}
			//if guid != MetricsPushOff {
			//	log.Debugf("recive metics %v, push to %v", r.contain.GUID, guid)
			//	if err := r.hostNu.Send(define.RPCFunctionNameOfManagerStrategyCollectorObjectMetrics, r.contain.GUID, metrics); err != nil {
			//		log.Debugf("recive metics %v, push to %v", r.contain.GUID, guid)
			//		_, err := r.hostNu.RPCCallCustom(guid, 10, 500, define.RPCFunctionNameOfManagerStrategyCollectorObjectMetrics, r.contain.GUID, metrics)
			//		if err != nil {
			//			log.Warnf("push metrics to manager %v", err)
			//		}
			//	}
			//}
		case <-r.ctx.Done():
			return
		}
	}
}

func (r *ContainerFunctionMap) pushLogs() {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "pushLogs",
	})

	log.Infof("init push logs %v", r.contain.GUID)
	for {
		select {
		case logs := <-r.logsChan:
			guid := r.logsPushGuid

			if guid != MetricsPushOff {
				log.Debugf("recive logs %v, push to %v", r.contain.GUID, guid)
				_, err := r.hostNu.RPCCallCustom(guid, 10, 500, define.RPCFunctionNameOfManagerStrategyCollectorObjectLogs, r.contain.GUID, logs)
				if err != nil {
					log.Warnf("push metrics to manager %v", err)
				}
			}
		case <-r.ctx.Done():
			return
		}
	}
}

func (r *ContainerFunctionMap) pushTraces() {
	log := logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "pushTraces",
	})

	log.Infof("init push trace %v", r.contain.GUID)
	for {
		select {
		case traces := <-r.traceChan:
			guid := r.tracePushGuid

			if guid != TracePushOff {
				log.Debugf("recive trace %v, push to %v", r.contain.GUID, guid)
				_, err := r.hostNu.RPCCallCustom(guid, 10, 500, define.RPCFunctionNameOfManagerStrategyCollectorObjectTrace, r.contain.GUID, traces)
				if err != nil {
					log.Warnf("push trace to manager %v", err)
				}
			}
		case <-r.ctx.Done():
			return
		}
	}
}

type LimitationConfig struct {
	CpuRatio float64

	CpuSets string

	// tc 命令不允许 0 值
	NetUpStreamBandwidth string
	NetLatency           string
	NetBrust             uint32
}

/*
   描述: 显示当前的资源限制

   error:

   eg: r.GetLimitationConfig()
*/
func (r *ContainerFunctionMap) GetLimitationConfig(args ...interface{}) (interface{}, error) {
	return r.LimitationConfig, nil
}

/*
   描述: 设置容器的时间片占比
   ratio float64: [0, 1.]

   error:

   eg: r.SetCpuRatio(0.5)
*/
func (r *ContainerFunctionMap) SetCpuRatio(args ...interface{}) (interface{}, error) {
	if len(args) < 1 {
		return "", fmt.Errorf("args is nil")
	}
	log := logrus.WithContext(context.Background()).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "SetCpuRatio",
	})

	ratio, err := strconv.ParseFloat(args[0].(string), 64)
	if err != nil {
		return "SET CPU RATIO FAILURE", err
	}
	// cgroupPath := "/docker/" + r.contain.ContainerId
	cgroupPath, _ := r.controller.GetCgroupController().GetCgroupPath(r.contain.ContainerId)
	log.Debug("parse ratio: ", ratio, "cgroup path: ", cgroupPath)
	err = r.controller.GetCgroupController().UpdateCpuResources(cgroupPath, ratio)

	if err != nil {
		return "SET CPU RATIO FAILURE", err
	}

	r.LimitationConfig.CpuRatio = ratio
	return "SET CPU RATIO SUCCESSFUL", nil

}

/*
   描述: 设置容器的shares
   ratio float64: [0, 1.]

   error:

   eg: r.SetCpuRatio(0.5)
*/
func (r *ContainerFunctionMap) SetCpuShares(args ...interface{}) (interface{}, error) {
	if len(args) < 1 {
		return "", fmt.Errorf("args is nil")
	}
	log := logrus.WithContext(context.Background()).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "SetCpuRatio",
	})

	ratio, err := strconv.ParseFloat(args[0].(string), 64)
	if err != nil {
		return "SET CPU SHARE FAILURE", err
	}

	cts := r.getIds()
	log.Debugf("container ids %v",cts)

	if len(cts) < 1 {
		return "LIST ID LEN < 1", err
	}

	ratioBase := 1000.0
	ratioT := float64(len(cts)-1) * ratioBase * ratio / (1 - ratio)
	ratioS := make([]int, len(cts))
	cgroupPaths := make([]string, len(cts))
	for k, v := range cts {
		cgroupPath, _ := r.controller.GetCgroupController().GetCgroupPath(v)
		cgroupPaths[k] = cgroupPath
		if v == r.contain.ContainerId {
			ratioS[k] = int(ratioT)
		} else {
			ratioS[k] = int(ratioBase)
		}
	}

	log.Debug("parse ratio: ", ratio, "cgroup path: ", cgroupPaths)
	err = r.controller.GetCgroupController().UpdateCpuShares(cgroupPaths, ratioS)

	if err != nil {
		return "SET CPU SHARE FAILURE", err
	}

	r.LimitationConfig.CpuRatio = ratio
	return "SET CPU SHARE SUCCESSFUL", nil

}

/*
   描述: 设置容器的CPU核心分配
   cpuSets string: `0`  `0-1` `0,1`

   error:

   eg: r.SetCpuSets(`0`)
*/
func (r *ContainerFunctionMap) SetCpuSets(args ...interface{}) (interface{}, error) {
	if len(args) < 1 {
		return "", fmt.Errorf("args is nil")
	}
	log := logrus.WithContext(context.Background()).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "SetCpuRatio",
	})

	cpuSets := args[0].(string)

	// cgroupPath := "/docker/" + r.contain.ContainerId
	cgroupPath, _ := r.controller.GetCgroupController().GetCgroupPath(r.contain.ContainerId)

	log.Debug("parse cpu sets: ", cpuSets, "cgroup path: ", cgroupPath)
	err := r.controller.GetCgroupController().UpdateCpuSets(cgroupPath, cpuSets)

	if err != nil {
		return "SET CPU SETS FAILURE", err
	}

	r.LimitationConfig.CpuSets = cpuSets
	return "SET CPU SETS SUCCESSFUL", nil

}

/*
   描述: 设置容器的上行网络带宽
   rate string: 10mbit、5kbit

   error:

   eg: r.SetNetUpstreamBandwidth("10mbit")
*/
func (r *ContainerFunctionMap) SetNetUpstreamBandwidth(args ...interface{}) (interface{}, error) {
	if len(args) < 1 {
		return "", fmt.Errorf("args is nil")
	}
	log := logrus.WithContext(context.Background()).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "SetCpuRatio",
	})
	tcCmd := []string{"tc", "qdisc", "change", "dev", "eth0", "root", "tbf", "rate", "10mbit", "latency", "10ms", "burst", "10000"}

	rate := args[0].(string)
	if r.LimitationConfig.NetUpStreamBandwidth == "" {
		tcCmd[2] = "add"
	}

	tcCmd[8] = rate

	log.Debug("use tc cmd: %s", strings.Join(tcCmd, " "))
	err := r.controller.GetContainerController().ContainerExec(r.contain.ContainerId, tcCmd)

	if err != nil {
		return "SET Net Upstream Bandwidth FAILURE", err
	}

	r.LimitationConfig.NetUpStreamBandwidth = rate
	return "SET Net Upstream Bandwidth SUCCESSFUL", nil
}

/*
   描述: 设置容器的上行网络带宽
   latency string: "10ms"

   error:

   eg: r.SetNetLatency("10ms")
*/
func (r *ContainerFunctionMap) SetNetLatency(args ...interface{}) (interface{}, error) {
	if len(args) < 1 {
		return "", fmt.Errorf("args is nil")
	}
	log := logrus.WithContext(context.Background()).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: RootPath + "SetCpuRatio",
	})
	tcCmd := []string{"tc", "qdisc", "change", "dev", "eth0", "root", "tbf", "rate", "10mbit", "latency", "10ms", "burst", "10000"}

	latency := args[0].(string)
	if r.LimitationConfig.NetUpStreamBandwidth == "" {
		tcCmd[2] = "add"
	}

	tcCmd[10] = latency

	log.Debug("use tc cmd: %s", strings.Join(tcCmd, " "))
	err := r.controller.GetContainerController().ContainerExec(r.contain.ContainerId, tcCmd)

	if err != nil {
		return "SET Net Latency FAILURE", err
	}

	r.LimitationConfig.NetLatency = latency
	return "SET Net Latency SUCCESSFUL", nil

}

func (r *ContainerFunctionMap) close() error {
	return r.collector.GetContainerCollector().CloseChan(r.contain.ContainerId)
}
