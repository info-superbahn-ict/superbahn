package host

type Metrics struct {
	MemTotal        uint64  `json:"memTotal"`
	MemAvailable    uint64  `json:"memAvailable"`
	MemUsedPercent  float64 `json:"memUsedPercent"`
	CpuCount        int     `json:"cpuCount"`
	CpuTotalPercent float64 `json:"cpuTotalPercent"`
	BytesSent       uint64  `json:"bytesSent"`
	BytesRecv       uint64  `json:"bytesRecv"`
}

type Host struct {
	HostName string `json:"name"`
	HostIP   string `json:"ip"`
	CPUCores string `json:"cpu_cores"`
	MemSize  string `json:"mem_size"`
}

func (r *Host) DeepCopy() *Host {
	c := *r
	return &c
}

func (r *Metrics) DeepCopy() *Metrics {
	c := *r
	return &c
}
