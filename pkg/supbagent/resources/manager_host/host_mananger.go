package manager_host

import (
	"context"
	"fmt"
	"net"
	"strings"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/collector"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/collector/collector_prometheus"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/controller"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/resources/manager_host/host"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/shirou/gopsutil/cpu"
	Host "github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/mem"
)

var functionList = []string{
	define.RPCFunctionNameOfAgentForListObject,
	define.RPCFunctionNameOfAgentForGetAgentInfo,
	define.RPCFunctionNameOfAgentForCreateObject,
	define.RPCFunctionNameOfAgentForDeleteObject,
}

//type Controller interface {
//}
//
//// todo 收集器接口 example
//type Collector interface {
//	OutputMetrics() (<-chan string,error)
//}

type HostManager struct {
	ctx context.Context

	host host.Host

	controller *controller.Manager
	collector  *collector.Manager
	guid       string
}

func NewHostManager(ctx context.Context, ct *controller.Manager, cl *collector.Manager, guid string) (*HostManager, error) {
	manager := &HostManager{
		ctx:        ctx,
		collector:  cl,
		controller: ct,
		host: host.Host{
			HostName: "",
			HostIP:   "",
			CPUCores: "",
			MemSize:  "",
		},
		guid: guid,
	}
	manager.host.HostIP, _ = GetIP()
	manager.host.HostName, _ = GetName()
	manager.host.CPUCores, _ = GetCpuCores()
	manager.host.MemSize, _ = GetMemSize()
	return manager, nil
}

func (r *HostManager) GetHostGuid() string {
	return r.guid
}

func (r *HostManager) GetHost() *host.Host {
	return r.host.DeepCopy()
}

func (r *HostManager) GetMetricsChan(interval int64) (<-chan string, error) {
	collectorHost, error := collector_prometheus.NewCollectorPrometheus(r.ctx, interval)
	if error != nil {
		return nil, error
	}
	return collectorHost.Output()
}

func GetMemSize() (string, error) {
	v, _ := mem.VirtualMemory()
	return fmt.Sprint(v.Total), nil
}

func GetCpuCores() (string, error) {
	count, _ := cpu.Counts(true)
	return fmt.Sprint(count), nil
}

func GetIP() (ip string, e error) {
	conn, err := net.Dial("udp", "8.8.8.8:53")
	if err != nil {
		e = err
		return
	}
	localAddr := conn.LocalAddr().(*net.UDPAddr)
	ip = strings.Split(localAddr.String(), ":")[0]
	return
}

func GetName() (string, error) {
	hostInfo, _ := Host.Info()
	return hostInfo.Hostname, nil
}

func GetMac() (string, error) {
	interfaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	inter := interfaces[0]
	mac := inter.HardwareAddr.String() //获取本机MAC地址
	return mac, nil
}
