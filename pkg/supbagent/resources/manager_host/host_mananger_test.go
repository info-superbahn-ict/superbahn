package manager_host

import (
	"fmt"
	"testing"
)

func Test_getIP(t *testing.T) {
	ip, _ := GetIP()
	fmt.Println(ip)
}

func Test_getMemSize(t *testing.T) {
	count, _ := GetMemSize()
	fmt.Println(count)
}

func Test_getCpuCores(t *testing.T) {
	count, _ := GetCpuCores()
	fmt.Println(count)
}

func Test_getName(t *testing.T) {
	name, _ := GetName()
	fmt.Println(name)
}

func TestGetMac(t *testing.T) {
	mac, _ := GetMac()
	fmt.Println(mac)
}
