package resources

const (
	ResourceTypeHost      = "host"
	ResourceTypeContainer = "containers"
)

var ResourceTypes = []string{ResourceTypeHost, ResourceTypeContainer}
