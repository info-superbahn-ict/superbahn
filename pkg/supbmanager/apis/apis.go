package apis

import (
	"bytes"
	"context"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"strings"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/level"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/options"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_applications"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_bindings"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_bindings/bindings"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_clusters"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_strategies"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/sponge_resources"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/supbmanager"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/third_party/docker"
	"gitee.com/info-superbahn-ict/superbahn/third_party/elk"
	"github.com/coreos/etcd/clientv3"
)

const (
	RPCTryTime     = 100
	RPCTryInterval = 500
)

type Apis struct {
	//
	ctx               context.Context
	userId            string
	userToken         string
	manager           *supbmanager.Manager
	strategiesManager *manager_strategies.StrategiesManager
	clusterManager    *manager_clusters.ClustersManager
	bindingManager    *manager_bindings.BindingsManager
	//devicesManager    *sponge_devices.DevicesManager
	//servicesManager   *sponge_services.ServicesManager
	resourcesManager *sponge_resources.ResourcesManager
	applicationsManager *manager_applications.AppliationManager
}

func NewUserApis(ctx context.Context, userId, userToken string, manager *supbmanager.Manager) (*Apis, error) {
	m := &Apis{
		ctx:       ctx,
		userId:    userId,
		userToken: userToken,
		manager:   manager,
	}

	s, err := m.manager.GetApi().GetStrategiesManager(m.userId, m.userToken)
	if err != nil {
		return nil, fmt.Errorf("get strategies manager %v", err)
	}

	c, err := m.manager.GetApi().GetClustersManager(m.userId, m.userToken)
	if err != nil {
		return nil, fmt.Errorf("get clusters manager %v", err)
	}

	b, err := m.manager.GetApi().GetBindingsManager(m.userId, m.userToken)
	if err != nil {
		return nil, fmt.Errorf("get bindings manager %v", err)
	}

	a, err := m.manager.GetApi().GetApplicationManager(m.userId, m.userToken)
	if err != nil {
		return nil, fmt.Errorf("get bindings manager %v", err)
	}

	//d, err := m.manager.GetApi().GetDevicesManager(m.userId, m.userToken)
	//if err != nil {
	//	return nil, fmt.Errorf("get devices manager %v", err)
	//}
	//
	//i, err := m.manager.GetApi().GetServicesManager(m.userId, m.userToken)
	//if err != nil {
	//	return nil, fmt.Errorf("get services manager %v", err)
	//}

	r, err := m.manager.GetApi().GetResourceManager(m.userId, m.userToken)
	if err != nil {
		return nil, fmt.Errorf("get services manager %v", err)
	}
	m.strategiesManager = s
	m.clusterManager = c
	m.bindingManager = b
	//m.devicesManager = d
	//m.servicesManager = i
	m.resourcesManager = r
	m.applicationsManager = a
	return m, nil
}

/*
	kdksf sdf sdf asd
 */
func (r *Apis) RunStaticStrategyOnAgentFromRPC(name string) error {
	s, err := r.strategiesManager.GetStaticStrategy(name)
	if err != nil {
		return err
	}

	randBytes := make([]byte, 32)
	if _, err := rand.Read(randBytes); err != nil {
		return fmt.Errorf("generate name %v", err)
	}
	s.StrategyContainerName = strings.Join([]string{s.StrategyName, fmt.Sprintf("%x", randBytes)}, "-")

	info := &docker.ContainerInformation{
		Name:  s.StrategyContainerName,
		Image: s.StrategyImage,
		ENV: map[string]string{
			"ETCD_URL":     options.DefaultedEtcd1,
			"META_KEY":     fmt.Sprintf(manager_clusters.ClustersSavePathPrefixFormat, r.userId),
			"META_OUT_KEY": fmt.Sprintf(manager_clusters.ClustersResSavePathPrefixFormat, r.userId),
		},
	}

	bts, err := json.Marshal(info)
	if err != nil {
		return fmt.Errorf("marshall info %v", bts)
	}

	resp, err := r.GetNervous().RPCCallCustom(define.RPCCommonGuidOfAgent, RPCTryTime, RPCTryInterval, define.RPCFunctionNameOfAgentForCreateObject, string(bts))
	if err != nil {
		return fmt.Errorf("rpc call %v", bts)
	}

	if err = json.Unmarshal([]byte(resp.(string)), info); err != nil {
		return fmt.Errorf("marshall resp %v", bts)
	}

	s.StrategyContainerId = info.CID
	s.StrategyGuid = info.GUID

	errs := new(bytes.Buffer)

	err = r.strategiesManager.UpdateRunningStrategy(s.StrategyGuid, s.DeepCopy())
	if err != nil {
		_ = r.DeleteRunningStrategyOnAgentFromRPC(s.StrategyGuid)
		return fmt.Errorf("save running strategies %v", err)
	}

	err = r.clusterManager.NewCluster(s)
	if err != nil {
		errs.WriteString(fmt.Sprintf("save cluster %v\n", err))
		if err = r.DeleteRunningStrategyOnAgentFromRPC(s.StrategyGuid); err != nil {
			errs.WriteString(fmt.Sprintf("delete from rpc cluster %v\n", err))
		}
		if err = r.strategiesManager.DeleteRunningStrategy(s.StrategyGuid); err != nil {
			errs.WriteString(fmt.Sprintf("delete running %v\n", err))
		}
		return fmt.Errorf("%v", errs.String())
	}

	return nil
}

func (r *Apis) DeleteRunningStrategyOnAgentFromRPC(guid string) error {
	s, err := r.strategiesManager.GetRunningStrategy(guid)
	if err != nil {
		return err
	}

	errs := new(bytes.Buffer)

	err = r.clusterManager.DeleteCluster(s.StrategyGuid)
	if err != nil {
		errs.WriteString(fmt.Sprintf("delete cluster %v\n", err))
	}
	err = r.strategiesManager.DeleteRunningStrategy(s.StrategyGuid)
	if err != nil {
		errs.WriteString(fmt.Sprintf("delete running strategy %v\n", err))
	}

	_, err = r.manager.GetNervous().RPCCallCustom(define.RPCCommonGuidOfAgent, RPCTryTime, RPCTryInterval, define.RPCFunctionNameOfAgentForDeleteObject, guid)
	if err != nil {
		errs.WriteString(fmt.Sprintf("delete running container %v\n", err))
	}

	if errs.Len() > 0 {
		return fmt.Errorf("%v", errs.String())
	}

	return nil
}


//func (r *Apis)RunApplication(name string) error {
//	app,err := r.applicationsManager.GetApplication(name)
//	if err != nil {
//		return fmt.Errorf("get application %v", err)
//	}
//
//	r.ne
//
//	return nil
//}

// RunStaticStrategy todo run a static strategy
func (r *Apis) RunStaticStrategy(name string) error {
	s, err := r.strategiesManager.GetStaticStrategy(name)
	if err != nil {
		return err
	}

	randBytes := make([]byte, 8)
	if _, err := rand.Read(randBytes); err != nil {
		return fmt.Errorf("generate name %v", err)
	}
	s.StrategyContainerName = strings.Join([]string{s.StrategyName, fmt.Sprintf("%x", randBytes)}, "-")

	envs := s.Envs()
	envs["META_KEY"] = fmt.Sprintf(manager_clusters.ClustersSavePathPrefixFormat, r.userId)
	envs["META_OUT_KEY"] = fmt.Sprintf(manager_clusters.ClustersResSavePathPrefixFormat, r.userId)



	var info *docker.ContainerInformation
	if port, ok := envs["EXPOSE"]; !ok {
		info, err = r.manager.GetDocker().StartStrategy(s.StrategyImage, s.StrategyContainerName, envs)
	}else {
		info, err = r.manager.GetDocker().StartStrategyWithPort(s.StrategyImage, s.StrategyContainerName, envs,port)
	}
	
	if err != nil {
		return fmt.Errorf("start strategies %v", err)
	}

	s.StrategyContainerId = info.CID
	s.StrategyGuid = info.GUID
	s.StrategyContainerEnv = envs

	err = r.strategiesManager.UpdateRunningStrategy(s.StrategyGuid, s.DeepCopy())
	if err != nil {
		_ = r.manager.GetDocker().Stop(s.StrategyContainerId)
		return fmt.Errorf("save running strategies %v", err)
	}

	err = r.clusterManager.NewCluster(s)
	if err != nil {
		_ = r.manager.GetDocker().Stop(s.StrategyContainerId)
		_ = r.strategiesManager.DeleteRunningStrategy(s.StrategyGuid)
		return fmt.Errorf("save cluster %v", err)
	}

	return nil
}

// StopRunningStrategy todo stop a running strategy
func (r *Apis) StopRunningStrategy(guid string) error {
	s, err := r.strategiesManager.GetRunningStrategy(guid)
	if err != nil {
		return err
	}

	randBytes := make([]byte, 8)
	if _, err := rand.Read(randBytes); err != nil {
		return fmt.Errorf("generate guid %v", err)
	}
	fault := r.manager.GetDocker().Stop(s.StrategyContainerId)
	
	if fault != nil {
		return fmt.Errorf("stop strategies %v", err)
	}

	err = r.strategiesManager.UpdateStaticStrategy(s.StrategyName, s.DeepCopy())
	if err != nil {
		_ = r.manager.GetDocker().Stop(s.StrategyContainerId)
		return fmt.Errorf("save static strategies %v", err)
	}

	err = r.clusterManager.NewCluster(s)
	if err != nil {
		_ = r.manager.GetDocker().Stop(s.StrategyContainerId)
		_ = r.strategiesManager.DeleteRunningStrategy(s.StrategyGuid)
		return fmt.Errorf("save cluster %v", err)
	}
	return nil
}

// DeleteRunningStrategy todo delete a running strategy
func (r *Apis) DeleteRunningStrategy(guid string) error {
	s, err := r.strategiesManager.GetRunningStrategy(guid)
	if err != nil {
		return err
	}

	err = r.clusterManager.DeleteCluster(s.StrategyGuid)
	if err != nil {
		return fmt.Errorf("delete cluster %v", err)
	}
	err = r.strategiesManager.DeleteRunningStrategy(s.StrategyGuid)
	if err != nil {
		return fmt.Errorf("delete running strategy %v", err)
	}
	err = r.manager.GetDocker().Stop(s.StrategyContainerId)
	if err != nil {
		return fmt.Errorf("stop running container %v", err)
	}
	err = r.manager.GetDocker().Remove(s.StrategyContainerId)
	if err != nil {
		return fmt.Errorf("remove running container %v", err)
	}
	return nil
}
func (r *Apis) BindingResourcesToCluster(slaveGuid, masterGuid string) error {
	var slaveType int
	if r.resourcesManager.ExistResources(slaveGuid) {
		slaveType = r.resourcesManager.GetResources(slaveGuid).OType
	} else {
		return fmt.Errorf("slave not exist")
	}

	if !r.strategiesManager.ExistRunningStrategy(masterGuid) {
		return fmt.Errorf("strategy not exist")
	}

	sg, err := r.strategiesManager.GetRunningStrategy(masterGuid)
	if err != nil {
		return err
	}

	bindingName := strings.Join([]string{slaveGuid, "binding", sg.StrategyKind}, "-")

	if err := r.bindingManager.CreateBinding(bindingName, sg.StrategyKind, level.SlaveLevel1, masterGuid, slaveGuid, bindings.DefaultVersion); err != nil {
		return fmt.Errorf("create binding %v", err)
	}

	if err := r.clusterManager.BindNewResources(slaveGuid, level.SlaveLevel1, masterGuid, slaveType); err != nil {
		return fmt.Errorf("binding to cluster %v", err)
	}
	return nil
}

//func (r *Apis) BindingDeviceOrServiceToCluster(slaveGuid, masterGuid string) error {
//	var slaveType string
//	if r.devicesManager.ExistDevices(slaveGuid) {
//		slaveType = manager_clusters.SlaveTypeDevices
//	} else {
//		if r.servicesManager.ExistServices(slaveGuid) {
//			slaveType = manager_clusters.SlaveTypeServices
//		} else {
//			return fmt.Errorf("slave not exist")
//		}
//	}
//
//	if !r.strategiesManager.ExistRunningStrategy(masterGuid) {
//		return fmt.Errorf("master not exist")
//	}
//
//	sg, err := r.strategiesManager.GetRunningStrategy(masterGuid)
//	if err != nil {
//		return err
//	}
//
//	bindingName := strings.Join([]string{slaveGuid, "binding", sg.StrategyType}, "-")
//
//	if err := r.bindingManager.CreateBinding(bindingName, sg.StrategyType, level.SlaveLevel1, masterGuid, slaveGuid, bindings.DefaultVersion); err != nil {
//		return fmt.Errorf("create binding %v", err)
//	}
//
//	if err := r.clusterManager.BindNewDeviceOrService(slaveGuid, slaveType, level.SlaveLevel1, masterGuid); err != nil {
//		return fmt.Errorf("binding to cluster %v", err)
//	}
//	return nil
//}

func (r *Apis) GetResourcesWatcher() clientv3.WatchChan {
	return r.resourcesManager.NewWatcher()
}

//func (r *Apis) GetDevicesWatcher() clientv3.WatchChan {
//	return r.devicesManager.NewWatcher()
//}
//
//func (r *Apis) GetServicesWatcher() clientv3.WatchChan {
//	return r.servicesManager.NewWatcher()
//}

func (r *Apis) GetResourcesManager() *sponge_resources.ResourcesManager {
	return r.resourcesManager
}

func (r *Apis) GetApplicationManager() *manager_applications.AppliationManager {
	return r.applicationsManager
}

//func (r *Apis) GetDevicesManager() *sponge_devices.DevicesManager {
//	return r.devicesManager
//}
//
//func (r *Apis) GetServicesManager() *sponge_services.ServicesManager {
//	return r.servicesManager
//}

func (r *Apis) GetStrategyManager() *manager_strategies.StrategiesManager {
	return r.strategiesManager
}

func (r *Apis) GetBindingsManager() *manager_bindings.BindingsManager {
	return r.bindingManager
}

func (r *Apis) GetClustersManager() *manager_clusters.ClustersManager {
	return r.clusterManager
}

func (r *Apis) GetNervous() supbnervous.Controller {
	return r.manager.GetNervous()
}

func (r *Apis) GetElk() elk.Recorder {
	return r.manager.GetElk()
}

func (r *Apis) GetDocker() docker.Controller {
	return r.manager.GetDocker()
}

func (r *Apis) GetOption() *options.Options {
	return r.manager.GetOption()
}

func (r *Apis) ListImages() ([]string, error) {
	return r.manager.GetDocker().ListTags()
}

func (r *Apis) DeleteImage(image string) error {

	return r.manager.GetDocker().DeleteTag(image)
}

func (r *Apis) UploadImage(image string) error {

	return r.manager.GetDocker().UploadTag(image)
}

func (r *Apis) LoadImage(image string) error {

	return r.manager.GetDocker().LoadTag(image)
}
