package apis

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/options"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_clusters"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"strings"
	"testing"
)

func TestNewContainer(t *testing.T) {
	nu, err := kafkaNervous.NewNervous(context.Background(), "../../../config/nervous_config.json", "test")
	if err != nil {
		fmt.Printf("new nerouvs client %v", err)
		return
	}

	randBytes := make([]byte, 32)
	if _, err = rand.Read(randBytes); err != nil {
		fmt.Printf("generate name %v", err)
		return
	}
	StrategyContainerName := strings.Join([]string{"simple-test", fmt.Sprintf("%x", randBytes)}, "-")

	info := &clireq.ClientRequest{
		ContainerName: StrategyContainerName,
		ImageTag: "simple-test",
		Envs: map[string]string{
			"ETCD_URL":options.DefaultedEtcd1,
			"META_KEY":fmt.Sprintf(manager_clusters.ClustersSavePathPrefixFormat,"default"),
			"META_OUT_KEY":fmt.Sprintf(manager_clusters.ClustersResSavePathPrefixFormat,"default"),
		},
	}

	bts,err := json.Marshal(info)
	if err !=nil {
		fmt.Printf("marshall info %v",bts)
		return
	}

	resp,err := nu.RPCCallCustom(define.RPCCommonGuidOfAgent,100,500,define.RPCFunctionNameOfAgentForCreateObject,string(bts))
	if err !=nil {
		fmt.Printf("call create %v",err)
		return
	}

	_ = nu.Close()

	fmt.Printf("%s\n",resp)
}
