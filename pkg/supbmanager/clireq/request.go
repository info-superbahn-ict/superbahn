package clireq

const (
	//ResourceTypeDevices = "devices"
	//ResourceTypeServices = "services"
	ResourceTypeResources = "resources"
	ResourceTypeClusters = "clusters"
	ResourceTypeBindings = "bindings"
	ResourceTypeStrategies = "strategies"

	ListAll = "true"
	DeleteAll = "true"
	IsRunning = "running"
	MoreDetail = "more_detail"
)

type ClientRequest struct {
	ResourceType string 	`json:"version"`
	ListAll string			`json:"list_all"`
	DeleteAll string		`json:"delete_all"`
	IsRunning string		`json:"running"`
	MoreDetail string		`json:"details"`
	Guids []string			`json:"guids"`
	Names []string			`json:"names"`
	Slave string			`json:"slave"`
	Master string			`json:"master"`
	Data string				`json:"data"`
}