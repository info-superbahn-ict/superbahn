package level

const (
	SlaveLevel1 = "guaranteed"
	SlaveLevel2 = "burstable"
	SlaveLevel3 = "besteffort"

	MasterLevel1 = "log_dumper"
	MasterLevel2 = "metrics_statistic"
	MasterLevel3 = "trace_analysis"
	MasterLevel4 = "controller"
	MasterLevel5 = "scheduler"
)

var SlaveLevels = []string{SlaveLevel1,SlaveLevel2,SlaveLevel3}
var MasterLevels = []string{MasterLevel1,MasterLevel2,MasterLevel3,MasterLevel4,MasterLevel5}
