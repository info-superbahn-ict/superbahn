package options

const (
	/*
		默认变量
	*/
	DefaultedEtcd1       = "10.16.0.180:10000"
	DefaultedEtcdTimeOut = 5

	DefaultedElkUrl     = "http://152.136.134.100:9200"
	DefaultedElkTimeOut = 5

	DefaultWebUrl = "10.16.0.180:10003"
	DefaultCliUrl = "10.16.0.180:10002"

	DefaultNervousConfig   = "../../config/nervous_config.json"
	DefaultLogPath         = "/home/logs/manager.log"
	DefaultLogPathStrategy = "/home/logs/strategy.log"

	DefaultPluginsPath = "../../build/plugins/manager"
	DefaultDockerUrl   = "tcp://10.16.0.180:23750"
)
