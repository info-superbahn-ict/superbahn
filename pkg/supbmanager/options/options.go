package options

type Options struct {
	EtcdEndPoints      []string
	EtcdTimeOutSeconds int64

	ElkUrl            string
	ElkTimeOutSeconds int64

	WebServerUrl string
	CliServerUrl string
	DockerUrl string

	NervousConfig string
	LogPath string

	PluginPath string


	LogPathStrategy string
	ClusterKey string
	ClusterResKey string
	ContainerName string
	PolicyName string
}

