package application

type Application struct {
	ApplicationName string	`json:"name"`
	ApplicationKey string	`json:"key"`
	ApplicationParent []string	`json:"parent"`
	ApplicationChild []string	`json:"child"`	
	ApplicationImage string	`json:"image"`
	ApplicationContainerName string	`json:"containerName"`
	ApplicationContainerEnv map[string]string `json:"container_env"`
	ApplicationContainerPort map[string]string `json:"container_port"`
	ApplicationState string	`json:"state"`

	ApplicationGuid string	`json:"guid"`
	ApplicationContainerId string	`json:"container_id"`
}

func (r *Application) DeepCopy() *Application {
	c := *r
	c.ApplicationContainerEnv = map[string]string{}
	for k,v := range r.ApplicationContainerEnv {
		c.ApplicationContainerEnv[k]=v
	}

	c.ApplicationContainerPort = map[string]string{}
	for k,v := range r.ApplicationContainerPort {
		c.ApplicationContainerPort[k]=v
	}
	return &c
}

func (r *Application) Envs() map[string]string {
	envs := make(map[string]string)
	for k,v := range r.ApplicationContainerEnv {
		envs[k]=v
	}
	return envs
}


func (r *Application) Ports() map[string]string {
	prots := make(map[string]string)
	for k,v := range r.ApplicationContainerPort {
		prots[k]=v
	}
	return prots
}
