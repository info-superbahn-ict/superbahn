package manager_applications

import (
	"context"
	"fmt"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_applications/application"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"sync"
)

const (
	ApplicationsSavePathPrefixFormat  = "/repository/superbahnManager/applications/static/%v/"
)

type AppliationRecorder interface {
	Put(string, string) error
	Get(string) ([]byte, error)
	GetWithPrefix(string) (map[string][]byte, error)
	Del(string) error
}

type AppliationManager struct {
	ctx      context.Context
	recorder AppliationRecorder
	rwLock   *sync.RWMutex

	/*
		静态策略是指没有运行的策略，该策略没有guid，没有container
	*/
	applications             map[string]*application.Application
	/*
		权限认证
	*/
	userID    string
	userToken string
}

func NewAppliationManager(ctx context.Context, d AppliationRecorder, user, userToken string) *AppliationManager {
	r := &AppliationManager{
		ctx:       ctx,
		recorder:  d,
		userID:    user,
		userToken: userToken,
		rwLock:    &sync.RWMutex{},

		applications: make(map[string]*application.Application),
		// runningStrategies: make(map[string]*application.Application),
	}
	r.initLoadApplications()
	return r
}

func (r *AppliationManager) initLoadApplications() {
	// static strategies
	bts, err := r.recorder.GetWithPrefix(fmt.Sprintf(ApplicationsSavePathPrefixFormat, r.userID))
	if err != nil {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "application.initLoadStrategies",
		}).Warnf("get static strategies from recorder %v", err)
		return
	}

	for _, bt := range bts {
		// todo create static strategy
		st := &application.Application{}
		if err = yaml.Unmarshal(bt, st); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "StrategiesManager.initLoadStrategies",
			}).Warnf("unmarshall %v", err)
		} else {
			r.rwLock.Lock()
			r.applications[st.ApplicationName] = st
			r.rwLock.Unlock()
		}
	}

}


func (r *AppliationManager) UpdateApplication(name string, s *application.Application) error {

	r.applications[name] = s.DeepCopy()

	// todo save strategies
	bts, err := yaml.Marshal(s)
	if err == nil {
		if err = r.recorder.Put(fmt.Sprintf(ApplicationsSavePathPrefixFormat, r.userID)+name, string(bts)); err == nil {
			return nil
		}
	}

	// todo cancel

	delete(r.applications, name)
	logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "StrategiesManager.UpdateRunningStrategy",
	}).Errorf("update running strategies %v", err)
	return fmt.Errorf("update running strategy %v", err)
}


func (r *AppliationManager) GetApplication(name string) (*application.Application,error) {
	// todo check the static strategy exists or not
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	s, ok := r.applications[name]
	if !ok {
		return nil,fmt.Errorf("static strategies not exist")
	}
	return s.DeepCopy(),nil
}

func (r *AppliationManager) ListApplications() map[string]*application.Application {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	resp := make(map[string]*application.Application,len(r.applications))
	for k, v := range r.applications {
		resp[k] = v.DeepCopy()
	}

	return resp
}

func (r *AppliationManager) ListApplicationsList() []*application.Application {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	resp := make([]*application.Application,len(r.applications))
	k := 0
	for _, v := range r.applications {
		resp[k] = v.DeepCopy()
		k++
	}

	return resp
}



func (r *AppliationManager) ExistApplication(name string) bool {
	// todo check the strategy if static
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	_, ok := r.applications[name]
	return ok
}

func (r *AppliationManager) DeleteApplication(name string) error {
	// todo check the  strategy if static
	r.rwLock.Lock()
	defer r.rwLock.Unlock()
	s, ok := r.applications[name]
	if !ok {
		return fmt.Errorf("strategies is nil")
	}

	// todo delete static strategies
	if err := r.recorder.Del(fmt.Sprintf(ApplicationsSavePathPrefixFormat,r.userID)+s.ApplicationName); err != nil {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "StrategiesManager.DeleteStaticStrategy",
		}).Debugf("delete static strategy %v %v", s.ApplicationName, err)
		return fmt.Errorf("delete static strategy %v", err)
	}

	// todo cancel
	delete(r.applications, name)
	return nil
}