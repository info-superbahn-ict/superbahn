package bindings

import "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/level"

const (
	DefaultVersion     = "v1"
	DefaultName        = "hello-world-binding"
	DefaultSlaveLevel  = level.SlaveLevel1
	DefaultMasterLevel = level.MasterLevel1
)


type Binding struct {
	Version     string `json:"version"`
	BindingName string `json:"name"`
	SlaveGuid   string `json:"slave"`
	MasterGuid  string `json:"master"`
	SlaveLevel  string `json:"slave_level"`
	MasterLevel string `json:"master_level"`
}

func NewDefaultBinding() *Binding {
	return &Binding{
		Version:     DefaultVersion,
		BindingName: DefaultName,
		SlaveLevel:  DefaultSlaveLevel,
		MasterLevel: DefaultMasterLevel,
	}
}

func NewBinding(bindingName,slaveLevel,masterLevel,masterGuid,slaveGuid,version string) *Binding {
	return &Binding{
		Version:     version,
		BindingName: bindingName,
		SlaveGuid:   slaveGuid,
		MasterGuid:  masterGuid,
		SlaveLevel:  slaveLevel,
		MasterLevel: masterLevel,
	}
}

func (r *Binding) DeepCopy() *Binding {
	b := *r
	return &b
}