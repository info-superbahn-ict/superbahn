package manager_bindings

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_bindings/bindings"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_clusters"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"sync"
)

const (
	//todo %v %v %v --> user deviceGuid bindingName
	BindingsSavePathPrefixFormat = "/spongeregister/superbahnManager/binding/%v/%v/%v"
	ListBindingsSavePathPrefixFormat = "/spongeregister/superbahnManager/binding/%v/"
	BindingsandClusterSavePathPrefixFormat = "/spongeregister/superbahnManager/binding/%v/%v/%v%v"

	//SuperUserBindingsSavePathPrefixFormat = "/spongeregister/superbahnManager/binding/%v/%v/%v"
	//SuperUserListBindingsSavePathPrefixFormat = "/spongeregister/superbahnManager/binding/%v/"
)



type BindingsRecorder interface {
	Put(string, string) error
	Get(string) ([]byte, error)
	GetWithPrefix(string) (map[string][]byte, error)
	Del(string) error
}

type BindingsManager struct {
	ctx      context.Context
	recorder BindingsRecorder
	rwLock   *sync.RWMutex

	/*
		map device guid --> binding name --> binding
	 */
	bindings map[string]map[string]*bindings.Binding

	/*
		map slave guid --> master level --> master guid
	*/
	slaveLevel map[string]map[string]string

	/*
		map master guid --> slave guid --> slave level
	*/
	masterLevel map[string]map[string]string

	/*
		权限认证
	*/
	userID    string
	userToken string
}


func NewBindingsManager(ctx context.Context, d BindingsRecorder, user, userToken string) *BindingsManager {
	r := &BindingsManager{
		ctx:       ctx,
		recorder:  d,
		userID:    user,
		userToken: userToken,
		rwLock:    &sync.RWMutex{},
		bindings: make(map[string]map[string]*bindings.Binding),
		slaveLevel: make(map[string]map[string]string),
		masterLevel: make(map[string]map[string]string),
	}
	r.initLoadBindings()
	return r
}


func (r *BindingsManager) initLoadBindings() {
	bts, err := r.recorder.GetWithPrefix(fmt.Sprintf(ListBindingsSavePathPrefixFormat, r.userID))
	if err != nil {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "BindingsRecorder.initLoadBindings",
		}).Warnf("get bindings from recorder %v", err)
		return
	}

	for _ , bt := range bts {

		st := bindings.NewDefaultBinding()
		if err = yaml.Unmarshal(bt, st); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "BindingsRecorder.initLoadBindings",
			}).Warnf("unmarshall %v", err)
		} else {
			r.rwLock.Lock()
			if _,ok:=r.bindings[st.SlaveGuid];!ok{
				r.bindings[st.SlaveGuid] = map[string]*bindings.Binding{}
			}
			r.bindings[st.SlaveGuid][st.BindingName] = st

			if _,ok:=r.slaveLevel[st.SlaveGuid];!ok{
				r.slaveLevel[st.SlaveGuid] = map[string]string{}
			}
			r.slaveLevel[st.SlaveGuid][st.MasterLevel] = st.MasterGuid

			if _,ok:=r.masterLevel[st.MasterGuid];!ok{
				r.masterLevel[st.MasterGuid] = map[string]string{}
			}
			r.masterLevel[st.MasterGuid][st.SlaveGuid] = st.SlaveLevel
			r.rwLock.Unlock()
		}
	}
}


func (r *BindingsManager) CreateBinding(bindingName,masterLevel,slaveLevel,masterGuid,slaveGuid,version string)error{

	// todo check the binding if exist
	r.rwLock.Lock()
	defer r.rwLock.Unlock()

	if r.checkExistBindings(slaveGuid,bindingName) {
		return fmt.Errorf("binding is exist")
	}

	if r.checkExistSlaveLevel(slaveGuid,masterLevel){
		return fmt.Errorf("master level is used")
	}

	if r.checkExistMasterLevel(masterGuid,slaveGuid){
		return fmt.Errorf("slave aready binding to master")
	}

	// todo create bindings
	bd := bindings.NewBinding(bindingName,slaveLevel,masterLevel,masterGuid,slaveGuid,version)

	r.bindings[slaveGuid][bindingName]= bd
	r.slaveLevel[slaveGuid][masterLevel] = masterGuid
	r.masterLevel[masterGuid][slaveGuid]=slaveLevel

	// todo save bindings
	bts,err := yaml.Marshal(bd)
	if err ==nil{
		if err = r.recorder.Put(fmt.Sprintf(BindingsSavePathPrefixFormat,r.userID,slaveGuid,bindingName),string(bts));err==nil{
			return nil
		}
	}

	// todo cancel
	delete(r.bindings[slaveGuid], bindingName)
	delete(r.slaveLevel[slaveGuid], masterLevel)
	delete(r.masterLevel[masterGuid], slaveGuid)
	return fmt.Errorf("save binding %v",err)
}

func (r *BindingsManager) DeleteBinding(slaveGuid,bindingName string) error {
	r.rwLock.Lock()
	defer r.rwLock.Unlock()
	masterLevel := r.bindings[slaveGuid][bindingName].MasterLevel
	masterGuid := r.bindings[slaveGuid][bindingName].MasterGuid
	var c *manager_clusters.ClustersManager

	// todo check the binding if exist
	if !r.checkExistBindings(slaveGuid,bindingName) {
		return fmt.Errorf("binding is not exist")
	}

	if !r.checkExistSlaveLevel(slaveGuid,masterLevel){
		return fmt.Errorf("master level is not used")
	}

	if !r.checkExistMasterLevel(masterGuid,slaveGuid){
		return fmt.Errorf("slave haven't binding to master")
	}

	r.rwLock.Lock()
	defer r.rwLock.Unlock()
	// todo check the masterGuid cluster if nil
	 ok := c.ExistCluster(masterGuid)
	if !ok {
		r.rwLock.Unlock()
		return fmt.Errorf("cluster is nil")
	}

	// todo delete bindings
	if err := r.recorder.Del(fmt.Sprintf(BindingsandClusterSavePathPrefixFormat,r.userID,slaveGuid,bindingName,masterGuid));err !=nil{
		return fmt.Errorf("delete binding and clusterMasterContainerEnvs%v",err)
	}

	// todo cancel
	delete(r.bindings[slaveGuid],bindingName)
	delete(r.slaveLevel[slaveGuid], masterLevel)
	delete(r.masterLevel[masterGuid], slaveGuid)
	delete(c.GetCluster(slaveGuid).ClusterMasterContainerEnvs,masterGuid)

	return nil
}

func (r *BindingsManager) ListBindings() []*bindings.Binding {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	var resp []*bindings.Binding
	for _,sbd := range r.bindings {
		for _,v := range sbd {
			resp = append(resp, v.DeepCopy())
		}
	}
	return resp
}


func (r *BindingsManager) checkExistBindings(slaveGuid,bindingName string) bool {
	if _ ,ok := r.bindings[slaveGuid]; ok {
		if _,ok = r.bindings[slaveGuid][bindingName];ok{
			return true
		}
		return false
	}
	r.bindings[slaveGuid]=map[string]*bindings.Binding{}
	return false
}

func (r *BindingsManager) checkExistSlaveLevel(slaveGuid,masterLevel string) bool {
	if _ ,ok := r.slaveLevel[slaveGuid]; ok {
		if _,ok = r.slaveLevel[slaveGuid][masterLevel];ok{
			return true
		}
		return false
	}
	r.slaveLevel[slaveGuid]=map[string]string{}
	return false
}

func (r *BindingsManager) checkExistMasterLevel(masterGuid,slaveGuid string) bool {
	if _ ,ok := r.masterLevel[masterGuid]; ok {
		if _,ok = r.masterLevel[masterGuid][slaveGuid];ok{
			return true
		}
		return false
	}
	r.masterLevel[masterGuid]=map[string]string{}
	return false
}

func (r *BindingsManager) GetDevicesMasterLevel(slaveGuid string)map[string]string{
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	if _,ok := r.slaveLevel[slaveGuid];!ok{
		r.slaveLevel[slaveGuid] =  map[string]string{}
		return nil
	}

	resp := map[string]string{}
	for k,v := range r.slaveLevel[slaveGuid]{
		resp[k]=v
	}

	return resp
}

func (r *BindingsManager) GetDevicesBindings(slaveGuid string)map[string]*bindings.Binding {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	if _,ok := r.bindings[slaveGuid];!ok{
		r.bindings[slaveGuid] =  map[string]*bindings.Binding{}
		return nil
	}

	resp := map[string]*bindings.Binding{}
	for k,v := range r.bindings[slaveGuid]{
		resp[k]=v.DeepCopy()
	}

	return resp
}
