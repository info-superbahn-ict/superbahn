package clusters
//
//import (
//	"encoding/json"
//	"fmt"
//	"strings"
//)
//
//const (
//	HeadPath = "head/"
//	ArgsPath = "args/"
//	ResPath = "res/"
//	SlavesPath = "slaves/"
//	DevicesPath = "devices/"
//	ServicesPath = "services/"
//)
//
//type Recorder interface {
//	Put(string, string) error
//	Get(string) ([]byte, error)
//	GetWithPrefix(string) (map[string][]byte, error)
//	Del(string) error
//}
//
//type clusterHead struct {
//	ClusterVersion string	`json:"version"`
//	ClusterName string	`json:"name"`
//	ClusterMasterKind string	`json:"kind"`
//	ClusterMasterImage string	`json:"image"`
//
//	ClusterMasterGuid string	`json:"guid"`
//	ClusterMasterContainerId string	`json:"containerId"`
//	ClusterMasterContainerName string	`json:"containerName"`
//	ClusterMasterContainerEnvs map[string]string	`json:"envs"`
//
//	ClusterMasterContainerArgs map[string]string	`json:"args"`
//
//	ClusterSlaves map[string]string `json:"slaves"`
//	ClusterDevices map[string]string `json:"devices"`
//	ClusterServices map[string]string `json:"services"`
//}
//
//type clusterArgs struct {
//	ClusterMasterContainerArgs map[string]string	`json:"args"`
//}
//
//type clusterRes struct {
//	ClusterMasterContainerRes map[string]string	`json:"args"`
//}
//
//type clusterSlaves struct {
//	ClusterSlaves map[string]string `json:"slaves"`
//}
//
//type clusterDevices struct {
//	ClusterDevices map[string]string `json:"devices"`
//}
//
//type clusterServices struct {
//	ClusterServices map[string]string `json:"services"`
//}
//
//func GetCluster(clusterPrefix,guid string, etcd Recorder)( *Cluster,error) {
//	clt := &Cluster{}
//
//	headBts,err := etcd.Get(clusterPrefix+HeadPath+guid)
//	if err !=nil {
//		return nil,fmt.Errorf("get head %v",err)
//	}
//	argsBts,err := etcd.Get(clusterPrefix+ArgsPath+guid)
//	if err !=nil {
//		return nil,fmt.Errorf("get args %v",err)
//	}
//	resBts,err := etcd.Get(clusterPrefix+ResPath+guid)
//	if err !=nil {
//		return nil,fmt.Errorf("get res %v",err)
//	}
//	slavesBts,err := etcd.Get(clusterPrefix+SlavesPath+guid)
//	if err !=nil {
//		return nil,fmt.Errorf("get slaves %v",err)
//	}
//	devicesBts,err := etcd.Get(clusterPrefix+DevicesPath+guid)
//	if err !=nil {
//		return nil,fmt.Errorf("get devices %v",err)
//	}
//	servicesBts,err := etcd.Get(clusterPrefix+ServicesPath+guid)
//	if err !=nil {
//		return nil,fmt.Errorf("get services %v",err)
//	}
//
//	if err = json.Unmarshal(headBts,clt);err !=nil{
//		return nil,fmt.Errorf("unmarshal head %v",err)
//	}
//	if err = json.Unmarshal(argsBts,clt);err !=nil{
//		return nil,fmt.Errorf("unmarshal args %v",err)
//	}
//	if err = json.Unmarshal(resBts,clt);err !=nil{
//		return nil,fmt.Errorf("unmarshal res %v",err)
//	}
//	if err = json.Unmarshal(slavesBts,clt);err !=nil{
//		return nil,fmt.Errorf("unmarshal slaves %v",err)
//	}
//	if err = json.Unmarshal(devicesBts,clt);err !=nil{
//		return nil,fmt.Errorf("unmarshal devices %v",err)
//	}
//	if err = json.Unmarshal(servicesBts,clt);err !=nil{
//		return nil,fmt.Errorf("unmarshal services %v",err)
//	}
//
//	return clt,nil
//}
//
//func PutClusterAll(clusterPrefix string, etcd Recorder, cluster *Cluster) error {
//	head := &clusterHead{
//		ClusterVersion:cluster.ClusterVersion,
//		ClusterName:cluster.ClusterName,
//		ClusterMasterKind:cluster.ClusterMasterKind,
//		ClusterMasterImage:cluster.ClusterMasterImage,
//
//		ClusterMasterGuid:cluster.ClusterMasterGuid,
//		ClusterMasterContainerId:cluster.ClusterMasterContainerId,
//		ClusterMasterContainerName:cluster.ClusterMasterContainerName,
//		ClusterMasterContainerEnvs:cluster.ClusterMasterContainerEnvs,
//	}
//	args := &clusterArgs{
//		ClusterMasterContainerArgs: cluster.ClusterMasterContainerArgs,
//	}
//
//	res := &clusterRes{
//		ClusterMasterContainerRes: cluster.ClusterMasterContainerRes,
//	}
//
//	slaves := &clusterSlaves{
//		ClusterSlaves: cluster.ClusterSlaves,
//	}
//
//	devices := &clusterDevices{
//		ClusterDevices: cluster.ClusterDevices,
//	}
//
//	services :=&clusterServices{
//		ClusterServices: cluster.ClusterServices,
//	}
//
//
//	headBts,err :=json.Marshal(head)
//	if err !=nil {
//		return fmt.Errorf("marshall head %v",err)
//	}
//	argsBts,err :=json.Marshal(args)
//	if err !=nil {
//		return fmt.Errorf("marshall args %v",err)
//	}
//	resBts,err := json.Marshal(res)
//	if err !=nil {
//		return fmt.Errorf("marshall res %v",err)
//	}
//	slavesBts,err := json.Marshal(slaves)
//	if err !=nil {
//		return fmt.Errorf("marshall slaves %v",err)
//	}
//	devicesBts,err := json.Marshal(devices)
//	if err !=nil {
//		return fmt.Errorf("marshall devices %v",err)
//	}
//	servicesBts,err := json.Marshal(services)
//	if err !=nil {
//		return fmt.Errorf("marshall services %v",err)
//	}
//
//	if err = etcd.Put(clusterPrefix+HeadPath+cluster.ClusterMasterGuid,string(headBts));err !=nil{
//		return fmt.Errorf("put head %v",err)
//	}
//	if err = etcd.Put(clusterPrefix+ArgsPath+cluster.ClusterMasterGuid,string(argsBts));err !=nil{
//		return fmt.Errorf("put args %v",err)
//	}
//	if err = etcd.Put(clusterPrefix+ResPath+cluster.ClusterMasterGuid,string(resBts));err !=nil{
//		return fmt.Errorf("put res %v",err)
//	}
//	if err = etcd.Put(clusterPrefix+SlavesPath+cluster.ClusterMasterGuid,string(slavesBts));err !=nil{
//		return fmt.Errorf("put slaves %v",err)
//	}
//	if err = etcd.Put(clusterPrefix+DevicesPath+cluster.ClusterMasterGuid,string(devicesBts));err !=nil{
//		return fmt.Errorf("put devices %v",err)
//	}
//	if err = etcd.Put(clusterPrefix+ServicesPath+cluster.ClusterMasterGuid,string(servicesBts));err !=nil{
//		return fmt.Errorf("put services %v",err)
//	}
//
//	return nil
//}
//
//func PutClusterAll(clusterPrefix string, etcd Recorder, cluster *Cluster) error {
//	head := &clusterHead{
//		ClusterVersion:cluster.ClusterVersion,
//		ClusterName:cluster.ClusterName,
//		ClusterMasterKind:cluster.ClusterMasterKind,
//		ClusterMasterImage:cluster.ClusterMasterImage,
//
//		ClusterMasterGuid:cluster.ClusterMasterGuid,
//		ClusterMasterContainerId:cluster.ClusterMasterContainerId,
//		ClusterMasterContainerName:cluster.ClusterMasterContainerName,
//		ClusterMasterContainerEnvs:cluster.ClusterMasterContainerEnvs,
//	}
//	args := &clusterArgs{
//		ClusterMasterContainerArgs: cluster.ClusterMasterContainerArgs,
//	}
//
//	res := &clusterRes{
//		ClusterMasterContainerRes: cluster.ClusterMasterContainerRes,
//	}
//
//	slaves := &clusterSlaves{
//		ClusterSlaves: cluster.ClusterSlaves,
//	}
//
//	devices := &clusterDevices{
//		ClusterDevices: cluster.ClusterDevices,
//	}
//
//	services :=&clusterServices{
//		ClusterServices: cluster.ClusterServices,
//	}
//
//
//	headBts,err :=json.Marshal(head)
//	if err !=nil {
//		return fmt.Errorf("marshall head %v",err)
//	}
//	argsBts,err :=json.Marshal(args)
//	if err !=nil {
//		return fmt.Errorf("marshall args %v",err)
//	}
//	resBts,err := json.Marshal(res)
//	if err !=nil {
//		return fmt.Errorf("marshall res %v",err)
//	}
//	slavesBts,err := json.Marshal(slaves)
//	if err !=nil {
//		return fmt.Errorf("marshall slaves %v",err)
//	}
//	devicesBts,err := json.Marshal(devices)
//	if err !=nil {
//		return fmt.Errorf("marshall devices %v",err)
//	}
//	servicesBts,err := json.Marshal(services)
//	if err !=nil {
//		return fmt.Errorf("marshall services %v",err)
//	}
//
//	if err = etcd.Put(clusterPrefix+HeadPath+cluster.ClusterMasterGuid,string(headBts));err !=nil{
//		return fmt.Errorf("put head %v",err)
//	}
//	if err = etcd.Put(clusterPrefix+ArgsPath+cluster.ClusterMasterGuid,string(argsBts));err !=nil{
//		return fmt.Errorf("put args %v",err)
//	}
//	if err = etcd.Put(clusterPrefix+ResPath+cluster.ClusterMasterGuid,string(resBts));err !=nil{
//		return fmt.Errorf("put res %v",err)
//	}
//	if err = etcd.Put(clusterPrefix+SlavesPath+cluster.ClusterMasterGuid,string(slavesBts));err !=nil{
//		return fmt.Errorf("put slaves %v",err)
//	}
//	if err = etcd.Put(clusterPrefix+DevicesPath+cluster.ClusterMasterGuid,string(devicesBts));err !=nil{
//		return fmt.Errorf("put devices %v",err)
//	}
//	if err = etcd.Put(clusterPrefix+ServicesPath+cluster.ClusterMasterGuid,string(servicesBts));err !=nil{
//		return fmt.Errorf("put services %v",err)
//	}
//
//	return nil
//}
//
//func ListCluster(clusterPrefix string, etcd Recorder)( map[string]*Cluster,error) {
//	clTs := make(map[string]*Cluster)
//
//	headBts,err := etcd.GetWithPrefix(clusterPrefix+HeadPath)
//	if err !=nil {
//		return nil,fmt.Errorf("get head %v",err)
//	}
//	argsBts,err := etcd.GetWithPrefix(clusterPrefix+ArgsPath)
//	if err !=nil {
//		return nil,fmt.Errorf("get args %v",err)
//	}
//	resBts,err := etcd.GetWithPrefix(clusterPrefix+ResPath)
//	if err !=nil {
//		return nil,fmt.Errorf("get res %v",err)
//	}
//	slavesBts,err := etcd.GetWithPrefix(clusterPrefix+SlavesPath)
//	if err !=nil {
//		return nil,fmt.Errorf("get slaves %v",err)
//	}
//	devicesBts,err := etcd.GetWithPrefix(clusterPrefix+DevicesPath)
//	if err !=nil {
//		return nil,fmt.Errorf("get devices %v",err)
//	}
//	servicesBts,err := etcd.GetWithPrefix(clusterPrefix+ServicesPath)
//	if err !=nil {
//		return nil,fmt.Errorf("get services %v",err)
//	}
//
//
//	for _,v := range headBts {
//		clt := &Cluster{}
//		if err = json.Unmarshal(v,clt);err !=nil{
//			return nil,fmt.Errorf("unmarshal head %v",err)
//		}
//		clTs[clt.ClusterMasterGuid] = clt
//	}
//
//	for k,v := range argsBts {
//		re := strings.Split(k,"/")
//		guid := re[len(re)-1]
//		if err = json.Unmarshal(v,clTs[guid]);err !=nil{
//			return nil,fmt.Errorf("unmarshal head %v",err)
//		}
//	}
//
//	for k,v := range resBts {
//		re := strings.Split(k,"/")
//		guid := re[len(re)-1]
//		if err = json.Unmarshal(v,clTs[guid]);err !=nil{
//			return nil,fmt.Errorf("unmarshal head %v",err)
//		}
//	}
//
//	for k,v := range slavesBts {
//		re := strings.Split(k,"/")
//		guid := re[len(re)-1]
//		if err = json.Unmarshal(v,clTs[guid]);err !=nil{
//			return nil,fmt.Errorf("unmarshal head %v",err)
//		}
//	}
//
//	for k,v := range devicesBts {
//		re := strings.Split(k,"/")
//		guid := re[len(re)-1]
//		if err = json.Unmarshal(v,clTs[guid]);err !=nil{
//			return nil,fmt.Errorf("unmarshal head %v",err)
//		}
//	}
//
//	for k,v := range servicesBts {
//		re := strings.Split(k,"/")
//		guid := re[len(re)-1]
//		if err = json.Unmarshal(v,clTs[guid]);err !=nil{
//			return nil,fmt.Errorf("unmarshal head %v",err)
//		}
//	}
//
//	return clTs,nil
//}
//
//func DeleteCluster(clusterPrefix,guid string, etcd Recorder) error {
//	if err := etcd.Del(clusterPrefix+HeadPath+guid);err !=nil{
//		return fmt.Errorf("del head %v",err)
//	}
//	if err := etcd.Del(clusterPrefix+ArgsPath+guid);err !=nil{
//		return fmt.Errorf("del args %v",err)
//	}
//	if err := etcd.Del(clusterPrefix+ResPath+guid);err !=nil{
//		return fmt.Errorf("del res %v",err)
//	}
//	if err := etcd.Del(clusterPrefix+SlavesPath+guid);err !=nil{
//		return fmt.Errorf("del slaves %v",err)
//	}
//	if err := etcd.Del(clusterPrefix+DevicesPath+guid);err !=nil{
//		return fmt.Errorf("del devices %v",err)
//	}
//	if err := etcd.Del(clusterPrefix+ServicesPath+guid);err !=nil{
//		return fmt.Errorf("del services %v",err)
//	}
//	return nil
//}
//
//func NewClusterRes() *clusterRes {
//	return &clusterRes{
//		ClusterMasterContainerRes: make(map[string]string),
//	}
//}
//
//func NewClusterArgs() *clusterArgs {
//	return &clusterArgs{
//		ClusterMasterContainerArgs: make(map[string]string),
//	}
//}