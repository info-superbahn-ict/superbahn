package clusters

import "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_strategies/strategies"

const (
	DefaultVersion = "v1"
	DefaultShortDescription = "logging information"
	DefaultDescription = "logging detail information of the strategy"
	DefaultName = "hello-world-cluster"
	DefaultKind = strategies.KindController
	DefaultImage = "simple-test"
)

type Cluster struct {
	ClusterVersion string	`json:"version"`
	ClusterShortDescription string	`json:"short description"`
	ClusterDescription string	`json:"detail description"`
	ClusterName string	`json:"name"`
	ClusterMasterKind string	`json:"kind"`
	ClusterMasterImage string	`json:"image"`

	ClusterMasterGuid string	`json:"guid"`
	ClusterMasterContainerId string	`json:"containerId"`
	ClusterMasterContainerName string	`json:"containerName"`
	ClusterMasterContainerEnvs map[string]string	`json:"envs"`

	ClusterMasterContainerArgs map[string]string	`json:"args"`
	ClusterMasterContainerRes map[string]string		`json:"res"`

	ClusterSlaves map[string]int `json:"slaves"`
	//ClusterDevices map[string]string `json:"devices"`
	//ClusterServices map[string]string `json:"services"`
	ClusterResources map[string]string `json:"resources"`
}

type ClusterRes struct {
	ClusterMasterContainerRes map[string]string	`json:"res"`
}

func NewDefaultClusterRes() *ClusterRes {
	return &ClusterRes{
		ClusterMasterContainerRes: make(map[string]string),
	}
}

func NewDefaultCluster() *Cluster {
	return &Cluster{
		ClusterVersion:     	DefaultVersion,
		ClusterShortDescription:DefaultShortDescription,
		ClusterDescription:		DefaultDescription,
		ClusterName:        	DefaultName,
		ClusterMasterKind:  	DefaultKind,
		ClusterMasterImage: 	DefaultImage,
	}
}

func (r *ClusterRes) DeepCopy() *ClusterRes  {
	c := ClusterRes{
		ClusterMasterContainerRes: make(map[string]string),
	}
	for k,v := range r.ClusterMasterContainerRes {
		c.ClusterMasterContainerRes[k]=v
	}
	return &c
}

func NewCluster(s *strategies.Strategy) *Cluster {
	c := &Cluster{
		ClusterVersion: s.StrategyVersion,
		ClusterShortDescription: s.StrategyShortDescription,
		ClusterDescription: s.StrategyDescription,
		ClusterName:    s.StrategyName,
		ClusterMasterKind:    s.StrategyKind,
		ClusterMasterImage: s.StrategyImage,

		ClusterMasterGuid: s.StrategyGuid,
		ClusterMasterContainerId: s.StrategyContainerId,
		ClusterMasterContainerName: s.StrategyContainerName,

		ClusterMasterContainerEnvs: make(map[string]string),
		ClusterMasterContainerArgs: make(map[string]string),


		ClusterSlaves: make(map[string]int),
		//ClusterDevices: make(map[string]string),
		//ClusterServices: make(map[string]string),
		ClusterResources: make(map[string]string),
	}
	for k,v := range s.StrategyContainerEnv {
		c.ClusterMasterContainerEnvs[k]=v
	}
	return c
}

func (r *Cluster) DeepCopy() *Cluster {
	c := *r

	c.ClusterMasterContainerEnvs = map[string]string{}
	c.ClusterMasterContainerArgs = map[string]string{}
	c.ClusterSlaves = map[string]int{}
	//c.ClusterDevices = map[string]string{}
	//c.ClusterServices = map[string]string{}

	for k,v := range r.ClusterMasterContainerEnvs {
		c.ClusterMasterContainerEnvs[k]=v
	}
	for k,v := range r.ClusterMasterContainerArgs {
		c.ClusterMasterContainerArgs[k]=v
	}

	for k,v := range r.ClusterSlaves {
		c.ClusterSlaves[k]=v
	}
	//for k,v := range r.ClusterDevices {
	//	c.ClusterDevices[k]=v
	//}
	//for k,v := range r.ClusterServices {
	//	c.ClusterServices[k]=v
	//}
	return &c
}




