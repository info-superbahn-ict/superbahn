package clusters

import (
	"encoding/json"
	"fmt"
	"testing"
)


type v1 struct {
	HHl string `json:"kk"`
}

type v2 struct {
	HHK string `json:"ll"`
}

type v3 struct {
	HHl string `json:"kk"`
	HHK string `json:"ll"`
}

func TestCompact(t *testing.T) {
	l1 := v1{
		HHl: "pl",
	}
	l2 := v2{
		HHK: "pk",
	}
	l12 := v1{
		HHl: "plllll",
	}
	l3 := v3 {
	}

	bt1,_:=json.Marshal(l1)
	bt12,_:=json.Marshal(l12)
	bt2,_:=json.Marshal(l2)

	_ =json.Unmarshal(bt1,&l3)
	fmt.Printf("1:%v\n", l3)
	_ =json.Unmarshal(bt2,&l3)
	fmt.Printf("2:%v\n", l3)
	_ =json.Unmarshal(bt12,&l3)
	fmt.Printf("1:%v\n", l3)
}
