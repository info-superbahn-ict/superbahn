package manager_clusters

import (
	"context"
	"fmt"
	"sync"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_clusters/clusters"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_strategies/strategies"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

const (
	SlaveTypeDevices = "devices"
	SlaveTypeServices = "services"
	ClustersSavePathPrefixFormat = "/spongeregister/superbahnManager/clusters/%v/"
	ClustersResSavePathPrefixFormat = "/spongeregister/superbahnManager/clustersRes/%v/"
)

type ClustersRecorder interface {
	Put(string, string) error
	Get(string) ([]byte, error)
	GetWithPrefix(string) (map[string][]byte, error)
	Del(string) error
}

type ClustersManager struct {
	ctx      context.Context
	recorder ClustersRecorder
	rwLock   *sync.RWMutex

	clusters map[string]*clusters.Cluster

	/*
		权限认证
	*/
	userID    string
	userToken string
}

func NewClustersManager(ctx context.Context, d ClustersRecorder, user, userToken string) *ClustersManager {
	r := &ClustersManager{
		ctx:       ctx,
		recorder:  d,
		userID:    user,
		userToken: userToken,
		rwLock:    &sync.RWMutex{},
		clusters:make(map[string]*clusters.Cluster),
	}
	r.initLoadClusters()
	return r
}

func (r *ClustersManager) initLoadClusters() {
	bts, err := r.recorder.GetWithPrefix(fmt.Sprintf(ClustersSavePathPrefixFormat, r.userID))
	if err != nil {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "ClustersRecorder.initLoadClusters",
		}).Warnf("get clusters from recorder %v", err)
		return
	}

	for _, bt := range bts {
		// todo create clusters
		st := clusters.NewDefaultCluster()
		if err = yaml.Unmarshal(bt, st); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "ClustersRecorder.initLoadClusters",
			}).Warnf("unmarshall %v", err)
		} else {
			r.rwLock.Lock()
			r.clusters[st.ClusterMasterGuid] = st
			r.rwLock.Unlock()
		}
	}
}

func (r *ClustersManager) NewCluster(s *strategies.Strategy) error {
	r.rwLock.RLock()
	_,ok:=r.clusters[s.StrategyGuid]
	r.rwLock.RUnlock()

	// todo check the cluster if exist
	if ok {
		return fmt.Errorf("cluster is exist")
	}
	// todo create clusters
	c := clusters.NewCluster(s)

	// todo save clusters
	bts, err := yaml.Marshal(c)
	if err == nil {
		if err = r.recorder.Put(fmt.Sprintf(ClustersSavePathPrefixFormat, r.userID)+c.ClusterMasterGuid, string(bts)); err == nil {
			r.rwLock.Lock()
			r.clusters[c.ClusterMasterGuid]=c
			r.rwLock.Unlock()
			return nil
		}
	}

	logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "ClustersRecorder.UpdateClusters",
	}).Debugf("update cluster %v %v",c.ClusterMasterGuid, err)

	return fmt.Errorf("update cluster %v",err)
}

func (r *ClustersManager) BindNewResources(slaveGuid, slaveLevel ,masterGuid string,slaveTypes int) error {
	r.rwLock.Lock()
	defer r.rwLock.Unlock()

	// todo check the masterGuid cluster if nil
	if _, ok := r.clusters[masterGuid]; !ok {
		return fmt.Errorf("cluster is nil")
	}

	r.clusters[masterGuid].ClusterSlaves[slaveGuid] = slaveTypes
	r.clusters[masterGuid].ClusterResources[slaveGuid] = slaveLevel

	//switch slaveTypes {
	//case SlaveTypeDevices:
	//	r.clusters[masterGuid].ClusterDevices[slaveGuid] = slaveLevel
	//case SlaveTypeServices:
	//	r.clusters[masterGuid].ClusterServices[slaveGuid] = slaveLevel
	//}
	bts, err := yaml.Marshal(r.clusters[masterGuid])

	// todo save clusters
	if err == nil {
		if err = r.recorder.Put(fmt.Sprintf(ClustersSavePathPrefixFormat, r.userID)+masterGuid, string(bts)); err == nil {
			return nil
		}
	}

	//todo cancel
	delete(r.clusters[masterGuid].ClusterSlaves, slaveGuid)
	delete(r.clusters[masterGuid].ClusterResources, slaveGuid)
	//switch slaveTypes {
	//case SlaveTypeDevices:
	//	delete(r.clusters[masterGuid].ClusterDevices, slaveGuid)
	//case SlaveTypeServices:
	//	delete(r.clusters[masterGuid].ClusterServices, slaveGuid)
	//}


	logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "ClustersRecorder.BindNewResources",
	}).Debugf("bind new cluster %v", err)
	return fmt.Errorf("bind new cluster %v", err)
}

//func (r *ClustersManager) BindNewDeviceOrService(slaveGuid, slaveTypes, slaveLevel string, masterGuid string) error {
//	r.rwLock.Lock()
//	defer r.rwLock.Unlock()
//
//	// todo check the masterGuid cluster if nil
//	if _, ok := r.clusters[masterGuid]; !ok {
//		return fmt.Errorf("cluster is nil")
//	}
//
//	r.clusters[masterGuid].ClusterSlaves[slaveGuid] = slaveTypes
//
//	switch slaveTypes {
//	case SlaveTypeDevices:
//		r.clusters[masterGuid].ClusterDevices[slaveGuid] = slaveLevel
//	case SlaveTypeServices:
//		r.clusters[masterGuid].ClusterServices[slaveGuid] = slaveLevel
//	}
//	bts, err := yaml.Marshal(r.clusters[masterGuid])
//
//	// todo save clusters
//	if err == nil {
//		if err = r.recorder.Put(fmt.Sprintf(ClustersSavePathPrefixFormat, r.userID)+masterGuid, string(bts)); err == nil {
//			return nil
//		}
//	}
//
//	//todo cancel
//	delete(r.clusters[masterGuid].ClusterSlaves, slaveGuid)
//
//	switch slaveTypes {
//	case SlaveTypeDevices:
//		delete(r.clusters[masterGuid].ClusterDevices, slaveGuid)
//	case SlaveTypeServices:
//		delete(r.clusters[masterGuid].ClusterServices, slaveGuid)
//	}
//
//
//	logrus.WithContext(r.ctx).WithFields(logrus.Fields{
//		define.LogsPrintCommonLabelOfFunction: "ClustersRecorder.BindNewDeviceOrService",
//	}).Debugf("bind new cluster %v", err)
//	return fmt.Errorf("bind new cluster %v", err)
//}


func (r *ClustersManager) DeleteCluster(clusterGuid string) error {
	r.rwLock.Lock()
	// todo check the masterGuid cluster if nil
	cl, ok := r.clusters[clusterGuid]
	if !ok {
		r.rwLock.Unlock()
		return fmt.Errorf("cluster is nil")
	}

	delete(r.clusters,clusterGuid)
	//r.clusters[clusterGuid] = nil
	r.rwLock.Unlock()

	// todo delete clusters
	if err := r.recorder.Del(cl.ClusterMasterGuid);err !=nil {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "ClustersRecorder.DeleteCluster",
		}).Debugf("delete cluster %v %v",cl.ClusterMasterGuid, err)
		return fmt.Errorf("delete cluster %v",err)
	}
	return nil
}

func (r *ClustersManager) GetCluster(guid string) *clusters.Cluster {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	if r.ExistCluster(guid) {
		return r.clusters[guid].DeepCopy()
	}

	return nil
}

func (r *ClustersManager) GetStatisticClusterInfo(clusterGuid string) *clusters.Cluster {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	if !r.ExistCluster(clusterGuid) {
		 logrus.Errorf("cluster is nil")
	}

	return r.GetCluster(clusterGuid)
}

func (r *ClustersManager) GetRelationshipGraph(clusterGuid string) *clusters.Cluster {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()



	return nil
}

func (r *ClustersManager) ListCluster() map[string]*clusters.Cluster {
	resp := make(map[string]*clusters.Cluster)

	r.rwLock.RLock()
	for k,v :=range r.clusters {
		resp[k]=v.DeepCopy()
	}
	r.rwLock.RUnlock()

	return resp
}

func (r *ClustersManager) ExistCluster(clusterGuid string) bool {

	_, ok := r.clusters[clusterGuid]

	if !ok {
		return false
	}

	return true
}

