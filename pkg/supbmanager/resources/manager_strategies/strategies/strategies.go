package strategies

const (
	DefaultVersion = "v1"
	DefaultName = "hello-world"
	DefaultKind = "controller"
	DefaultImage = "simple-test"
	DefaultShortDescription = "logging information"
	DefaultDescription = "logging detail information of the strategy"

	KindController = "controller"
	KindScheduler = "scheduler"
)

type Strategy struct {
	StrategyVersion string	`json:"version"`
	StrategyShortDescription string `json:"shortDescription"`
	StrategyDescription string 		`json:"description"`
	StrategyName string		`json:"name"`
	StrategyKind string		`json:"kind"`
	StrategyImage string	`json:"image"`
	StrategyContainerEnv map[string]string	`json:"containerEnvs"`

	/*
		仅运行中的策略有的guid和container name and container id
	*/
	StrategyGuid string	`json:"guid"`
	StrategyContainerId string	`json:"containerId"`
	StrategyContainerName string	`json:"containerName"`

}

func NewDefaultStrategy() *Strategy {
	return &Strategy{
		StrategyVersion: DefaultVersion,
		StrategyShortDescription:DefaultShortDescription,
		StrategyDescription:	 DefaultDescription,
		StrategyName:    DefaultName,
		StrategyKind:    DefaultKind,
		StrategyImage:   DefaultImage,
	}
}

func (r *Strategy) DeepCopy() *Strategy {
	c := *r
	c.StrategyContainerEnv = map[string]string{}
	for k,v := range r.StrategyContainerEnv {
		c.StrategyContainerEnv[k]=v
	}
	return &c
}

func (r *Strategy) Envs() map[string]string {
	envs := make(map[string]string)
	for k,v := range r.StrategyContainerEnv {
		envs[k]=v
	}
	return envs
}
