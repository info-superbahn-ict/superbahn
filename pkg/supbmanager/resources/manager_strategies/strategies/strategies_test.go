package strategies

import (
	"fmt"
	"testing"
)

func TestDeepCopy(t *testing.T) {
	s1 := &Strategy{
		StrategyVersion: "v1",
		StrategyContainerEnv: map[string]string{
			"k1":"22",
		},
	}

	s2 := s1.DeepCopy()
	fmt.Printf("s1: %v\ns2: %v\n",s1,s2)
	s1.StrategyVersion = "v3"
	s1.StrategyContainerEnv["k2"]="33"
	fmt.Printf("s1: %v\ns2: %v\n",s1,s2)
}
