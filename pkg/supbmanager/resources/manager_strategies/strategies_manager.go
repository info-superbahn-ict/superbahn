package manager_strategies

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/level"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_strategies/strategies"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"sync"
)

const (
	StaticStrategiesSavePathPrefixFormat  = "/repository/superbahnManager/strategies/static/%v/"
	RunningStrategiesSavePathPrefixFormat = "/repository/superbahnManager/strategies/running/%v/"
)

type StrategiesRecorder interface {
	Put(string, string) error
	Get(string) ([]byte, error)
	GetWithPrefix(string) (map[string][]byte, error)
	Del(string) error
}

type StrategiesManager struct {
	ctx      context.Context
	recorder StrategiesRecorder
	rwLock   *sync.RWMutex

	/*
		静态策略是指没有运行的策略，该策略没有guid，没有container
	*/
	staticStrategies             map[string]*strategies.Strategy
	runningStrategies            map[string]*strategies.Strategy
	strategiesRunningToStaticMap map[string]string
	strategiesStaticToRunningMap map[string]string

	/*
		权限认证
	*/
	userID    string
	userToken string
}

func NewStrategiesManager(ctx context.Context, d StrategiesRecorder, user, userToken string) *StrategiesManager {
	r := &StrategiesManager{
		ctx:       ctx,
		recorder:  d,
		userID:    user,
		userToken: userToken,
		rwLock:    &sync.RWMutex{},

		staticStrategies: make(map[string]*strategies.Strategy),
		runningStrategies: make(map[string]*strategies.Strategy),
		strategiesRunningToStaticMap: make(map[string]string),
		strategiesStaticToRunningMap: make(map[string]string),
	}
	r.initLoadStrategies()
	return r
}

func (r *StrategiesManager) initLoadStrategies() {
	// static strategies
	bts, err := r.recorder.GetWithPrefix(fmt.Sprintf(StaticStrategiesSavePathPrefixFormat, r.userID))
	if err != nil {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "StrategiesManager.initLoadStrategies",
		}).Warnf("get static strategies from recorder %v", err)
		return
	}

	for _, bt := range bts {
		// todo create static strategy
		st := strategies.NewDefaultStrategy()
		if err = yaml.Unmarshal(bt, st); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "StrategiesManager.initLoadStrategies",
			}).Warnf("unmarshall %v", err)
		} else {
			r.rwLock.Lock()
			r.staticStrategies[st.StrategyName] = st
			r.rwLock.Unlock()
		}
	}

	// running strategies
	bts, err = r.recorder.GetWithPrefix(fmt.Sprintf(RunningStrategiesSavePathPrefixFormat, r.userID))
	if err != nil {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "StrategiesManager.initLoadStrategies",
		}).Warnf("get running strategies from recorder %v", err)
		return
	}

	for _, bt := range bts {
		st := strategies.NewDefaultStrategy()
		if err = yaml.Unmarshal(bt, st); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "StrategiesManager.initLoadStrategies",
			}).Warnf("unmarshall %v", err)
		} else {
			r.rwLock.Lock()
			r.runningStrategies[st.StrategyGuid] = st
			r.strategiesRunningToStaticMap[st.StrategyGuid] = st.StrategyName
			r.rwLock.Unlock()
		}
	}
}

func (r *StrategiesManager) UploadRunningStrategy(guid string)error {
	bts, err := r.recorder.GetWithPrefix(fmt.Sprintf(RunningStrategiesSavePathPrefixFormat, r.userID))
	if err != nil {
		return fmt.Errorf("upload running strategies from recorder: %v", err)
	}

	for _, bt := range bts {
		if r.ExistRunningStrategy(guid){
			// todo create static strategy
			st := strategies.NewDefaultStrategy()
			if err = yaml.Unmarshal(bt, st); err != nil {
				return fmt.Errorf("unmarshall %v", err)
			} else {
				r.rwLock.Lock()
				r.runningStrategies[st.StrategyGuid] = st
				r.strategiesRunningToStaticMap[st.StrategyGuid] = st.StrategyName
				r.rwLock.Unlock()
			}
		}
	}
	return nil
}

func (r *StrategiesManager) LoadStaticStrategy(name string)error {
	bts, err := r.recorder.GetWithPrefix(fmt.Sprintf(StaticStrategiesSavePathPrefixFormat, r.userID))
	if err != nil {
		return fmt.Errorf("load static strategies from recorder: %v", err)
	}

	for _, bt := range bts {
		if r.ExistStaticStrategy(name){
			// todo create static strategy
			st := strategies.NewDefaultStrategy()
			if err = yaml.Unmarshal(bt, st); err != nil {
				return fmt.Errorf("unmarshall %v", err)
			} else {
				r.rwLock.Lock()
				r.staticStrategies[st.StrategyName] = st
				r.rwLock.Unlock()
			}
		}

	}
	return nil
}


func (r *StrategiesManager) StopRunningStrategy(guid string)error {
	// todo check the running strategy exists or not
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	s,ok := r.runningStrategies[guid]
	if !ok {
		return fmt.Errorf("the objective running strategy does not exist")
	}else {
		//todo 加载一个静态策略作为运行策略停止之后的目标策略
		err := r.LoadStaticStrategy(guid)
		if err!= nil{
			return fmt.Errorf("fail to load a static strategy: %v", err)
		}else {
			st,_ := r.staticStrategies[guid]
			r.rwLock.Lock()
			defer r.rwLock.Unlock()
			r.staticStrategies[s.StrategyGuid]=s
			r.strategiesRunningToStaticMap[st.StrategyGuid] = s.StrategyName
			fault := r.DeleteRunningStrategy(guid)
			if fault != nil {
					return fmt.Errorf("fail to delete the running strategy: %v", fault)
				}
			}
		}
	return nil
}

// func (r *StrategiesManager) RestartStrategy(guid string)error {
// 	// todo check the running strategy exists or not
// 	r.rwLock.RLock()
// 	defer r.rwLock.RUnlock()

// 	st,ok := r.runningStrategies[guid]
// 	if !ok {
// 		return fmt.Errorf("running strategies not exist")
// 	}else{
// 		err := r.StopRunningStrategy(st.StrategyGuid)
// 		if err != nil {
// 			return err
// 		}
// 		// todo check the run strategy has stopped or not
// 		r.rwLock.Lock()
// 		defer r.rwLock.Unlock()
// 		if !ok {
// 			return fmt.Errorf("run strategies have not stopped")
// 		}else {
// 			r.RunStaticStrategy(st.StrategyName)
// 		}

// 	}
// 	return nil
// }

func (r *StrategiesManager) UpdateStaticStrategy(name string, s *strategies.Strategy) error {
	// todo check strategies
	ok := false
	for _,v := range level.MasterLevels {
		if v == s.StrategyKind {
			ok = true
			break
		}
	}
	if !ok {
		return fmt.Errorf("kind must be [%v]",level.MasterLevels)
	}

	// todo bind strategies
	r.rwLock.Lock()
	r.staticStrategies[name] = s
	r.rwLock.Unlock()

	// todo save strategies
	bts, err := yaml.Marshal(s)
	if err == nil {
		if err = r.recorder.Put(fmt.Sprintf(StaticStrategiesSavePathPrefixFormat, r.userID)+name, string(bts)); err == nil {
			return nil
		}
	}

	// todo cancel
	r.rwLock.Lock()
	defer r.rwLock.Unlock()

	delete(r.staticStrategies, name)

	logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "StrategiesManager.UpdateStaticStrategy",
	}).Errorf("update static strategies: %v", err)
	return fmt.Errorf("update static strategy: %v", err)
}

func (r *StrategiesManager) UpdateRunningStrategy(guid string, s *strategies.Strategy) error {
	r.rwLock.Lock()
	r.runningStrategies[guid] = s.DeepCopy()
	r.strategiesRunningToStaticMap[s.StrategyGuid] = s.StrategyName
	r.rwLock.Unlock()

	// todo save strategies
	bts, err := yaml.Marshal(s)
	if err == nil {
		if err = r.recorder.Put(fmt.Sprintf(RunningStrategiesSavePathPrefixFormat, r.userID)+guid, string(bts)); err == nil {
			return nil
		}
	}

	// todo cancel
	r.rwLock.Lock()
	delete(r.runningStrategies, guid)
	delete(r.strategiesRunningToStaticMap, s.StrategyGuid)
	r.rwLock.Unlock()

	logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "StrategiesManager.UpdateRunningStrategy",
	}).Errorf("update running strategies: %v", err)
	return fmt.Errorf("update running strategy: %v", err)
}

func (r *StrategiesManager) GetStaticStrategy(name string) (*strategies.Strategy,error) {
	// todo check the static strategy exists or not
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	s, ok := r.staticStrategies[name]
	if !ok {
		return nil,fmt.Errorf("static strategies not exist")
	}
	return s.DeepCopy(),nil
}

func (r *StrategiesManager) GetRunningStrategy(guid string) (*strategies.Strategy,error) {
	// todo check the running strategy exists or not
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	s, ok := r.runningStrategies[guid]
	if !ok {
		return nil,fmt.Errorf("running strategies not exist")
	}
	return s.DeepCopy(),nil
}

func (r *StrategiesManager) ListStaticStrategy() map[string]*strategies.Strategy {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	resp := make(map[string]*strategies.Strategy,len(r.staticStrategies))
	for k, v := range r.staticStrategies {
		resp[k] = v.DeepCopy()
	}

	return resp
}

func (r *StrategiesManager) ListStaticStrategyReturnList() []*strategies.Strategy {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	resp := make([]*strategies.Strategy,len(r.staticStrategies))
	k := 0
	for _, v := range r.staticStrategies {
		resp[k] = v.DeepCopy()
		k++
	}

	return resp
}


func (r *StrategiesManager) ListRunningStrategy() map[string]*strategies.Strategy {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	resp := make(map[string]*strategies.Strategy,len(r.runningStrategies))
	for k, v := range r.runningStrategies {
		resp[k] = v.DeepCopy()
	}

	return resp
}

func (r *StrategiesManager) ListRunningStrategyReturnList() []*strategies.Strategy {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	resp := make([]*strategies.Strategy,len(r.runningStrategies))
	k := 0
	for _, v := range r.runningStrategies {
		resp[k] = v.DeepCopy()
		k++
	}

	return resp
}

func (r *StrategiesManager) ExistStaticStrategy(name string) bool {
	// todo check the strategy if static
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	_, ok := r.staticStrategies[name]
	if ok {
		return true
	}

	return false
}

func (r *StrategiesManager) ExistRunningStrategy(guid string) bool {
	// todo check the strategy if running
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	_, ok := r.runningStrategies[guid]
	if ok {
		return true
	}

	return false
}

func (r *StrategiesManager) DeleteStaticStrategy(name string) error {
	// todo check the  strategy if static
	r.rwLock.Lock()
	defer r.rwLock.Unlock()
	s, ok := r.staticStrategies[name]
	if !ok {
		return fmt.Errorf("static strategy does not exist")
	}

	// todo delete static strategies
	if err := r.recorder.Del(fmt.Sprintf(StaticStrategiesSavePathPrefixFormat,r.userID)+s.StrategyName); err != nil {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "StrategiesManager.DeleteStaticStrategy",
		}).Debugf("delete static strategy: %v %v", s.StrategyName, err)
		return fmt.Errorf("delete static strategy: %v", err)
	}

	// todo cancel
	delete(r.staticStrategies, name)
	return nil
}

func (r *StrategiesManager) DeleteRunningStrategy(guid string) error {
	// todo check the strategy if running
	r.rwLock.Lock()
	defer r.rwLock.Unlock()
	s, ok := r.runningStrategies[guid]
	if !ok {
		return fmt.Errorf("running strategy does not exist")
	}

	// todo delete running strategies
	if err := r.recorder.Del(fmt.Sprintf(RunningStrategiesSavePathPrefixFormat,r.userID)+s.StrategyName); err != nil {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "StrategiesManager.DeleteRunningStrategy",
		}).Debugf("delete running strategy: %v %v", s.StrategyName, err)
		return fmt.Errorf("delete running strategy: %v", err)
	}

	// todo cancel
	delete(r.runningStrategies, guid)
	return nil
}

func (r *StrategiesManager) PushStaticStrategy(data string) error {
	st := &strategies.Strategy{}
	if err :=yaml.Unmarshal([]byte(data),st);err !=nil{
		return fmt.Errorf("decode %v",st)
	}

	if st.StrategyName == "" {
		return fmt.Errorf("miss strategy name")
	}

	if st.StrategyVersion == "" {
		return fmt.Errorf("miss strategy version")
	}

	if st.StrategyShortDescription == "" {
		return fmt.Errorf("miss strategy short description")
	}

	if st.StrategyDescription == "" {
		return fmt.Errorf("miss strategy detail description")
	}

	if st.StrategyKind == "" {
		return fmt.Errorf("miss strategy kind")
	}

	if st.StrategyImage == "" {
		return fmt.Errorf("miss strategy image")
	}

	if r.ExistStaticStrategy(st.StrategyName) {
		return fmt.Errorf("strategy is exist")
	}

	if err :=r.UpdateStaticStrategy(st.StrategyName,st);err !=nil{
		return fmt.Errorf("save %v",st)
	}

	return nil
}

func (r *StrategiesManager) PullStaticStrategy(name string) (string,error) {
	if !r.ExistStaticStrategy(name) {
		return "",fmt.Errorf("static not exist")
	}

	st,err :=r.GetStaticStrategy(name)
	if err !=nil {
		return "", fmt.Errorf("get: %v",err)
	}

	bts,err :=json.Marshal(st)
	if err !=nil {
		return "", fmt.Errorf("marshall %v",err)
	}

	return string(bts), nil

}