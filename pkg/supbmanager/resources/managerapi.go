package resources

import (
	"context"
	"fmt"
	"strings"
	"sync"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_applications"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_bindings"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_clusters"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_strategies"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/sponge_devices"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/sponge_resources"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/sponge_services"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/coreos/etcd/clientv3"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

const (
	UserIdListSavePath = "/spongeregister/superbahnManager/userInfo/idList"
)

type Recorder interface {
	Put(string, string) error
	Get(string) ([]byte, error)
	GetWithPrefix(string) (map[string][]byte, error)
	Del(string) error
	GetWatcher(string) clientv3.WatchChan
}

type ManagerApi struct {
	ctx      context.Context
	recorder Recorder
	rwLock   *sync.RWMutex


	/*
		map user id to manager
	 */
	strategyManagers map[string]*manager_strategies.StrategiesManager
	clusterManagers map[string]*manager_clusters.ClustersManager
	bindingManagers map[string]*manager_bindings.BindingsManager
	devicesManagers map[string]*sponge_devices.DevicesManager
	servicesManagers map[string]*sponge_services.ServicesManager
	resourcesManagers map[string]*sponge_resources.ResourcesManager
	applicationManagers map[string]*manager_applications.AppliationManager
	
	/*
		map user id to token
	 */
	userList map[string]string
}

func NewManagerApi(ctx context.Context, recorder Recorder) (*ManagerApi,error){
	r := &ManagerApi{
		ctx:              ctx,
		recorder:         recorder,
		rwLock:           &sync.RWMutex{},
		userList:         make(map[string]string),
		strategyManagers: make(map[string]*manager_strategies.StrategiesManager),
		clusterManagers:  make(map[string]*manager_clusters.ClustersManager),
		bindingManagers:  make(map[string]*manager_bindings.BindingsManager),
		devicesManagers:  make(map[string]*sponge_devices.DevicesManager),
		servicesManagers:  make(map[string]*sponge_services.ServicesManager),
		resourcesManagers:  make(map[string]*sponge_resources.ResourcesManager),
		applicationManagers:  make(map[string]*manager_applications.AppliationManager),

	}
	return r,r.initLoadUserInfo()
}

func (r *ManagerApi)initLoadUserInfo()error{
	bts,err := r.recorder.Get(UserIdListSavePath)
	if err !=nil {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "ManagerApi.initLoadUserInfo",
		}).Warnf("get userlist from recorder %v", err)
		return fmt.Errorf("load user info %v",err)
	}

	if err = yaml.Unmarshal(bts,r.userList);err!=nil{
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "ManagerApi.initLoadUserInfo",
		}).Warnf("umarshall userlist %v", err)
		return fmt.Errorf("umarshall userlist %v", err)
	}

	for k,v := range r.userList {
		r.strategyManagers[k] = manager_strategies.NewStrategiesManager(r.ctx,r.recorder,k,v)
		r.clusterManagers[k] = manager_clusters.NewClustersManager(r.ctx,r.recorder,k,v)
		r.bindingManagers[k] = manager_bindings.NewBindingsManager(r.ctx,r.recorder,k,v)
		r.devicesManagers[k] = sponge_devices.NewDevicesManager(r.ctx,r.recorder,k,v)
		r.servicesManagers[k] = sponge_services.NewServicesManager(r.ctx,r.recorder,k,v)
		r.resourcesManagers[k] = sponge_resources.NewResourcesManager(r.ctx,r.recorder,k,v)
		r.applicationManagers[k] = manager_applications.NewAppliationManager(r.ctx,r.recorder,k,v)
	}
	return nil
}

func (r *ManagerApi)certificate(userId,token string)bool{
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	tk,ok := r.userList[userId]
	if !ok {
		return false
	}
	if strings.Compare(tk,token) == 0 {
		return true
	}
	return false
}

func (r *ManagerApi) GetStrategiesManager(user,token string) (*manager_strategies.StrategiesManager,error){
	if r.certificate(user,token) {
		r.rwLock.RLock()
		defer r.rwLock.RUnlock()

		s := r.strategyManagers[user]
		return s,nil
	}
	return nil,fmt.Errorf("user certificate failed")
}

func (r *ManagerApi) GetClustersManager(user,token string) (*manager_clusters.ClustersManager,error){
	if r.certificate(user,token) {
		r.rwLock.RLock()
		defer r.rwLock.RUnlock()
		c := r.clusterManagers[user]

		return c,nil
	}
	return nil,fmt.Errorf("user certificate failed")
}

func (r *ManagerApi) GetBindingsManager(user,token string) (*manager_bindings.BindingsManager,error){
	if r.certificate(user,token) {
		r.rwLock.RLock()
		defer r.rwLock.RUnlock()

		c := r.bindingManagers[user]
		return c,nil
	}
	return nil,fmt.Errorf("user certificate failed")
}

//func (r *ManagerApi) GetDevicesManager(user,token string) (*sponge_devices.DevicesManager,error){
//	if r.certificate(user,token) {
//		r.rwLock.RLock()
//		defer r.rwLock.RUnlock()
//
//		c := r.devicesManagers[user]
//
//		return c,nil
//	}
//	return nil,fmt.Errorf("user certificate failed")
//}
//
//func (r *ManagerApi) GetServicesManager(user,token string) (*sponge_services.ServicesManager,error){
//	if r.certificate(user,token) {
//		r.rwLock.RLock()
//		defer r.rwLock.RUnlock()
//
//		c := r.servicesManagers[user]
//		return c,nil
//	}
//	return nil,fmt.Errorf("user certificate failed")
//}

func (r *ManagerApi) GetResourceManager(user,token string) (*sponge_resources.ResourcesManager,error){
	if r.certificate(user,token) {
		r.rwLock.RLock()
		defer r.rwLock.RUnlock()

		c := r.resourcesManagers[user]
		return c,nil
	}
	return nil,fmt.Errorf("user certificate failed")
}

func (r *ManagerApi) GetApplicationManager(user,token string) (*manager_applications.AppliationManager,error){
	if r.certificate(user,token) {
		r.rwLock.RLock()
		defer r.rwLock.RUnlock()

		c := r.applicationManagers[user]
		return c,nil
	}
	return nil,fmt.Errorf("user certificate failed")
}


func (r *ManagerApi) RegisterUser(userId, token string) error{
	r.rwLock.Lock()
	defer r.rwLock.Unlock()
	_,ok := r.userList[userId]

	if ok {
		return fmt.Errorf("user exist")
	}

	// todo spongeregister


	// todo end spongeregister
	r.userList[userId] = token

	bts,err := yaml.Marshal(r.userList)
	if err == nil {
		err = r.recorder.Put(UserIdListSavePath,string(bts))
		if err ==nil{
			return nil
		}
	}

	logrus.WithContext(r.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "ManagerApi.initLoadUserInfo",
	}).Warnf("save user list %v", err)

	delete(r.userList, userId)
	return nil
}