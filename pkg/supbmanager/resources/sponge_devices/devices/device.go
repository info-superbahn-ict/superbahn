package devices

const (
	DefaultVersion     = "v1"
	DefaultDeviceName  = "hello-world-cluster"
	DefaultDevicesType = "physical_device"
)

type Device struct {
	Version    string `json:"version"`
	DeviceName string `json:"name"`
	DeviceType string `json:"type"`

	Guid          string `json:"guid"`
	ResourceType  string `json:"resourceType"`
	ResourceID    string `json:"resourceID"`
	ProductID     string `json:"productID"`
	ManufactureID string `json:"manufactureID"`
}

func NewDefaultDevice() *Device {
	return &Device{
		Version:    DefaultVersion,
		DeviceName: DefaultDeviceName,
		DeviceType: DefaultDevicesType,
	}
}

func (r *Device) DeepCopy() *Device {
	d := *r
	return &d
}
