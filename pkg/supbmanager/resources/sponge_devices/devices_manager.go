package sponge_devices

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/sponge_devices/devices"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	//"gitee.com/info-superbahn-ict/superbahn/sync/sponge_sync_to_manager"
	"github.com/coreos/etcd/clientv3"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"sync"
)

const (
	DevicesSavePathPrefixFormat = "/spongeregister/superbahnManager/devices/%v/"
)

type DevicesRecorder interface {
	Put(string, string) error
	Get(string) ([]byte, error)
	GetWithPrefix(string) (map[string][]byte, error)
	Del(string) error
	GetWatcher(string) clientv3.WatchChan
}

type DevicesManager struct {
	ctx      context.Context
	recorder DevicesRecorder
	rwLock   *sync.RWMutex
	/*
		静态策略是指没有运行的策略，该策略没有guid，没有container
	*/
	devices map[string]*devices.Device

	/*
		权限认证
	*/
	userID    string
	userToken string
}

func NewDevicesManager(ctx context.Context, d DevicesRecorder, user, userToken string) *DevicesManager {
	r := &DevicesManager{
		ctx:       ctx,
		recorder:  d,
		userID:    user,
		userToken: userToken,
		rwLock:    &sync.RWMutex{},
		devices:   make(map[string]*devices.Device),
	}
	r.initLoadStrategies()
	return r
}

func (r *DevicesManager) initLoadStrategies() {
	bts, err := r.recorder.GetWithPrefix(fmt.Sprintf(DevicesSavePathPrefixFormat, r.userID))
	if err != nil {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "StrategiesManager.initLoadStrategies",
		}).Warnf("get devices from recorder %v", err)
		return
	}

	for _, bt := range bts {

		// todo create devices
		st := devices.NewDefaultDevice()
		if err = yaml.Unmarshal(bt, st); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "StrategiesManager.initLoadStrategies",
			}).Warnf("unmarshall %v", err)
		} else {
			r.rwLock.Lock()
			r.devices[st.DeviceName] = st
			r.rwLock.Unlock()
		}
	}
}

func (r *DevicesManager) ListDevices() map[string]*devices.Device {
	resp := make(map[string]*devices.Device)

	for k, v := range r.devices {
		resp[k] = v.DeepCopy()
	}
	return resp
}

func (r *DevicesManager) GetDevices(guid string) *devices.Device {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()
	// todo check the devices if exist
	if r.ExistDevices(guid) {
		return r.devices[guid].DeepCopy()
	}
	return nil
}

func (r *DevicesManager) ExistDevices(guid string) bool {
	if _, ok := r.devices[guid]; ok {
		return true
	}
	return false
}

func (r *DevicesManager) AddDevices(d *devices.Device) error {
	r.rwLock.Lock()
	defer r.rwLock.Unlock()

	// todo check the devices if exist
	if r.ExistDevices(d.Guid) {
		return fmt.Errorf("devices exist")
	}

	r.devices[d.Guid] = d.DeepCopy()

	return nil
}

func (r *DevicesManager) DeleteDevices(guid string) error {
	r.rwLock.Lock()
	defer r.rwLock.Unlock()

	// todo check the devices if exist
	if r.ExistDevices(guid) {
		return fmt.Errorf("devices not exist")
	}

	// todo cancel
	delete(r.devices, guid)

	return nil
}

func (r *DevicesManager) UpdateDevices(d *devices.Device) error {
	r.rwLock.Lock()
	defer r.rwLock.Unlock()

	r.devices[d.Guid] = d.DeepCopy()

	return nil
}
//
//func (r *DevicesManager) ParseFromSyncer(e *sponge_sync_to_manager.Resource) *devices.Device {
//	return &devices.Device{
//		Version:    devices.DefaultVersion,
//		DeviceName: e.Name,
//		DeviceType: e.SlaveTypes,
//
//		Guid:          e.Guid,
//		ResourceType:  e.ResourceType,
//		ResourceID:    e.ManufactureID,
//		ProductID:     e.ProductID,
//		ManufactureID: e.ManufactureID,
//	}
//}

func (r *DevicesManager) NewWatcher() clientv3.WatchChan {
	return r.recorder.GetWatcher(fmt.Sprintf(DevicesSavePathPrefixFormat, r.userID))
}