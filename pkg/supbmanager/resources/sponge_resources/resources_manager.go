package sponge_resources

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
	"github.com/coreos/etcd/clientv3"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"

	"sync"
)

const (
	ResourcesSavePathPrefixFormat = "/spongeregister/superbahnManager/resources/%v/"
)

type ResourcesRecorder interface {
	Put(string, string) error
	Get(string) ([]byte, error)
	GetWithPrefix(string) (map[string][]byte, error)
	Del(string) error
	GetWatcher(string) clientv3.WatchChan
}

type ResourcesManager struct {
	ctx      context.Context
	recorder ResourcesRecorder
	rwLock   *sync.RWMutex

	resources map[string]*spongeregister.Resource

	/*
		权限认证
	*/
	userID    string
	userToken string
}

func NewResourcesManager(ctx context.Context, d ResourcesRecorder, user, userToken string) *ResourcesManager {
	r := &ResourcesManager{
		ctx:       ctx,
		recorder:  d,
		userID:    user,
		userToken: userToken,
		rwLock:    &sync.RWMutex{},
		resources:  make(map[string]*spongeregister.Resource),
	}
	r.initLoadResources()
	return r
}

func (r *ResourcesManager) initLoadResources() {
	bts, err := r.recorder.GetWithPrefix(fmt.Sprintf(ResourcesSavePathPrefixFormat, r.userID))
	if err != nil {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "StrategiesManager.initLoadStrategies",
		}).Warnf("get Resources from recorder %v", err)
		return
	}

	for _, bt := range bts {
		// todo create Resources
		st := &spongeregister.Resource{}
		if err = yaml.Unmarshal(bt, st); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "StrategiesManager.initLoadStrategies",
			}).Warnf("unmarshall %v", err)
		} else {
			r.rwLock.Lock()
			r.resources[st.GuId] = st
			r.rwLock.Unlock()
		}
	}
}

func (r *ResourcesManager) ListResources() map[string]*spongeregister.Resource {
	resp := make(map[string]*spongeregister.Resource)

	for k, v := range r.resources {
		resp[k] = v.DeepCopy()
	}
	return resp
}

func (r *ResourcesManager) GetResources(guid string) *spongeregister.Resource{
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	// todo check the Resources if exist
	if r.ExistResources(guid) {
		return r.resources[guid].DeepCopy()
	}
	return nil
}

func (r *ResourcesManager) ListResourcesStatistic() map[string]int {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	resources := r.ListResources()
	if len(resources) == 0 {
		return nil
	}


	listMap := make(map[string]int,len(resources))

	for _,v:= range resources {
		switch v.OType {
		case spongeregister.TypeServer:
			if _,ok:=listMap["server"];!ok {
				listMap["server"]=0
			}
			listMap["server"] ++
		case spongeregister.TypePc:
			if _,ok:=listMap["pc"];!ok {
				listMap["pc"]=0
			}
			listMap["pc"] ++
		case spongeregister.TypeDocker:
			if _,ok:=listMap["container"];!ok {
				listMap["container"]=0
			}
			listMap["container"] ++
		case spongeregister.TypeDevice:
			if _,ok:=listMap["device"];!ok {
				listMap["device"]=0
			}
			listMap["device"] ++
		case spongeregister.TypeNervous:
			if _,ok:=listMap["nervous"];!ok {
				listMap["nervous"]=0
			}
			listMap["nervous"] ++
		case spongeregister.TypeControlCenter:
			if _,ok:=listMap["controller"];!ok {
				listMap["controller"]=0
			}
			listMap["controller"] ++
		case spongeregister.TypeStrategy:
			if _,ok:=listMap["strategy"];!ok {
				listMap["strategy"]=0
			}
			listMap["strategy"] ++
		case spongeregister.TypeCluster:
			if _,ok:=listMap["cluster"];!ok {
				listMap["cluster"]=0
			}
			listMap["cluster"] ++
		}
	}

	return listMap

	// for _, v := range r.resources {
	// 	result := make([][]string, v.OType)
	// 	var tmp []string
	// 	//todo get the number of current type resource
	// 	var i = 0
	// 	//todo current resource name
	// 	//var count = 0
	// 	for i < v.OType{
	// 		//count=count+1
	// 		for _,n := range r.ListResources(){
	// 			if n.OType == v.OType{
	// 				i=i+1
	// 				d := strconv.Itoa(i)
	// 				tmp = append(tmp, d)
	// 				result = append(result, tmp)
	// 			}
	// 		}
	// 	}
	// 	return  result
	// }

	// return nil
}

func (r *ResourcesManager) GetSpecificResources(ResourceID string) *spongeregister.Resource {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	// todo check the specific ResourceID Resources if exist
	if r.ExistResources(ResourceID) {
		return r.resources[ResourceID].DeepCopy()
	}

	return nil
}

func (r *ResourcesManager) ExistResources(guid string) bool {
	if _, ok := r.resources[guid]; ok {
		return true
	}
	return false
}


func (r *ResourcesManager) AddResources(s *spongeregister.Resource) error {
	r.rwLock.Lock()
	defer r.rwLock.Unlock()

	// todo check the Resources if exist
	if r.ExistResources(s.GuId) {
		return fmt.Errorf("Resources exist")
	}

	r.resources[s.GuId] = s.DeepCopy()

	return nil
}

func (r *ResourcesManager) DeleteResources(guid string) error {
	r.rwLock.Lock()
	defer r.rwLock.Unlock()

	// todo check the Resources if exist
	if r.ExistResources(guid) {
		return fmt.Errorf("Resources not exist")
	}

	//todo cancel
	delete(r.resources, guid)

	return nil
}

func (r *ResourcesManager) UpdateResources(s *spongeregister.Resource) error {
	r.rwLock.Lock()
	defer r.rwLock.Unlock()

	r.resources[s.GuId] = s.DeepCopy()

	return nil
}

func (r *ResourcesManager) NewWatcher() clientv3.WatchChan {
	return r.recorder.GetWatcher(fmt.Sprintf(ResourcesSavePathPrefixFormat, r.userID))
}
