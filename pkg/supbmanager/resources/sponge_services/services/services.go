package services

const (
	DefaultVersion = "v1"
	DefaultServiceName = "hello-world-cluster"
	DefaultServiceType = "physical_device"
)

type Service struct {
	Version       string `json:"version"`
	ServiceName          string `json:"name"`
	ServiceType    string `json:"type"`

	Guid          string `json:"guid"`
	ResourceType  string `json:"resourceType"`
	ResourceID    string `json:"resourceID"`
	ProductID     string `json:"productID"`
	ManufactureID string `json:"manufactureID"`
}

func NewDefaultService () *Service {
	return &Service{
		Version:     DefaultVersion,
		ServiceName: DefaultServiceName,
		ServiceType: DefaultServiceType,
	}
}

func (r *Service) DeepCopy() *Service {
	d := *r
	return &d
}