package sponge_services

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/sponge_services/services"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/coreos/etcd/clientv3"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"sync"
)

const (
	ServicesSavePathPrefixFormat = "/spongeregister/superbahnManager/services/%v/"
)

type ServicesRecorder interface {
	Put(string, string) error
	Get(string) ([]byte, error)
	GetWithPrefix(string) (map[string][]byte, error)
	Del(string) error
	GetWatcher(string) clientv3.WatchChan
}

type ServicesManager struct {
	ctx      context.Context
	recorder ServicesRecorder
	rwLock   *sync.RWMutex

	/*
		静态策略是指没有运行的策略，该策略没有guid，没有container
	*/
	services map[string]*services.Service

	/*
		权限认证
	*/
	userID    string
	userToken string
}

func NewServicesManager(ctx context.Context, d ServicesRecorder, user, userToken string) *ServicesManager {
	r := &ServicesManager{
		ctx:       ctx,
		recorder:  d,
		userID:    user,
		userToken: userToken,
		rwLock:    &sync.RWMutex{},
		services:  make(map[string]*services.Service),
	}
	r.initLoadStrategies()
	return r
}

func (r *ServicesManager) initLoadStrategies() {
	bts, err := r.recorder.GetWithPrefix(fmt.Sprintf(ServicesSavePathPrefixFormat, r.userID))
	if err != nil {
		logrus.WithContext(r.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "StrategiesManager.initLoadStrategies",
		}).Warnf("get services from recorder %v", err)
		return
	}

	for _, bt := range bts {
		// todo create services
		st := services.NewDefaultService()
		if err = yaml.Unmarshal(bt, st); err != nil {
			logrus.WithContext(r.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "StrategiesManager.initLoadStrategies",
			}).Warnf("unmarshall %v", err)
		} else {
			r.rwLock.Lock()
			r.services[st.ServiceName] = st
			r.rwLock.Unlock()
		}
	}
}

func (r *ServicesManager) ListServices() map[string]*services.Service {
	resp := make(map[string]*services.Service)

	for k, v := range r.services {
		resp[k] = v.DeepCopy()
	}
	return resp
}

func (r *ServicesManager) GetServices(guid string) *services.Service {
	r.rwLock.RLock()
	defer r.rwLock.RUnlock()

	// todo check the services if exist
	if r.ExistServices(guid) {
		return r.services[guid].DeepCopy()
	}
	return nil
}

func (r *ServicesManager) ExistServices(guid string) bool {
	if _, ok := r.services[guid]; ok {
		return true
	}
	return false
}


func (r *ServicesManager) AddServices(s *services.Service) error {
	r.rwLock.Lock()
	defer r.rwLock.Unlock()

	// todo check the services if exist
	if r.ExistServices(s.Guid) {
		return fmt.Errorf("services exist")
	}

	r.services[s.Guid] = s.DeepCopy()

	return nil
}

func (r *ServicesManager) DeleteServices(guid string) error {
	r.rwLock.Lock()
	defer r.rwLock.Unlock()

	// todo check the services if exist
	if r.ExistServices(guid) {
		return fmt.Errorf("services not exist")
	}

	//todo cancel
	delete(r.services, guid)

	return nil
}

func (r *ServicesManager) UpdateServices(s *services.Service) error {
	r.rwLock.Lock()
	defer r.rwLock.Unlock()

	r.services[s.Guid] = s.DeepCopy()

	return nil
}

//func (r *ServicesManager) ParseFromSyncer(e *sponge_sync_to_manager.Resource) *services.Service {
//	return &services.Service{
//		Version:    services.DefaultVersion,
//		ServiceName: e.Name,
//		ServiceType: e.SlaveTypes,
//
//		Guid:          e.Guid,
//		ResourceType:  e.ResourceType,
//		ResourceID:    e.ManufactureID,
//		ProductID:     e.ProductID,
//		ManufactureID: e.ManufactureID,
//	}
//}


func (r *ServicesManager) NewWatcher() clientv3.WatchChan {
	return r.recorder.GetWatcher(fmt.Sprintf(ServicesSavePathPrefixFormat, r.userID))
}