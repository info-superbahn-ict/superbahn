package supbmanager

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/options"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/third_party/docker"
	"gitee.com/info-superbahn-ict/superbahn/third_party/elk"
	"gitee.com/info-superbahn-ict/superbahn/third_party/etcd"
)

type Manager struct {
	etcd    etcd.Recorder
	nervous nervous.Controller
	elk     elk.Recorder
	docker  docker.Controller
	opt     *options.Options

	managerApi *resources.ManagerApi

}

func NewManager(
	ctx context.Context,
	etcd etcd.Recorder,
	elk elk.Recorder,
	nervous nervous.Controller,
	docker docker.Controller,
	opt *options.Options) (*Manager,error){

	m :=&Manager{
		etcd: etcd,
		elk: elk,
		docker: docker,
		nervous: nervous,
		opt: opt,
	}

	apis ,err := resources.NewManagerApi(ctx,etcd)
	if err !=nil {
		return nil, fmt.Errorf("new resources %v",err)
	}

	m.managerApi = apis

	return m,nil
}

func (r *Manager) GetApi() *resources.ManagerApi {
	return r.managerApi
}

func (r *Manager) GetDocker() docker.Controller {
	return r.docker
}

func (r *Manager) GetOption() *options.Options {
	return r.opt
}

func (r *Manager) GetEtcd() etcd.Recorder {
	return r.etcd
}

func (r *Manager) GetElk() elk.Recorder {
	return r.elk
}

func (r *Manager) GetNervous() nervous.Controller  {
	return r.nervous
}