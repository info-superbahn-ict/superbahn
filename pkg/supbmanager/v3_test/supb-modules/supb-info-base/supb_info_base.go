package supb_info_base

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/golang-collections/go-datastructures/queue"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"sync"
)

const (
	queueLen = ".queue.len"
)

type EventLabel string
type SubscriberLabel string
type PublisherLabel string

type Information interface {
	DeepCopy() Information
}

type informerEvent struct {
	info  Information
	event EventLabel
}

type SupbInfoBase struct {
	ctx    context.Context
	prefix string

	lock           *sync.RWMutex
	subscribeTable *sync.Map

	queue       chan *informerEvent
	maxQueueLen int

	cache *sync.Map

	logger log.Logger
}

func NewInformer(ctx context.Context, prefix string) *SupbInfoBase {
	return &SupbInfoBase{
		ctx: ctx,
		prefix: prefix,

		lock:           &sync.RWMutex{},
		subscribeTable: &sync.Map{},
		cache:          &sync.Map{},


		logger: log.NewLogger(prefix),
	}
}


func (r *SupbInfoBase) InitFlags(flags *pflag.FlagSet) {
	flags.Int(r.prefix+queueLen, 100, "metrics debug flag")
}

func (r *SupbInfoBase) ViperConfig(viper *viper.Viper) {

}

func (r *SupbInfoBase) InitViper(viper *viper.Viper) {
	r.maxQueueLen = viper.GetInt(r.prefix + queueLen)
}

func (r *SupbInfoBase) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *SupbInfoBase) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}

	r.queue = make(chan *informerEvent, r.maxQueueLen)

	go r.run()
	r.logger.Infof("%v initialized", r.prefix)
}

func (r *SupbInfoBase) Close() error {
	r.logger.Infof("%v closed", r.prefix)
	return nil
}


func (r *SupbInfoBase) Subscribe(label SubscriberLabel, event EventLabel) (<-chan Information, error) {
	table, ok := r.subscribeTable.LoadAndDelete(event)
	if !ok {
		table = &sync.Map{}
	}

	subTable, ok := table.(*sync.Map)
	if !ok {
		return nil, fmt.Errorf("type failed")
	}

	msg := make(chan Information)
	subTable.Store(label, msg)

	if _, loaded := r.subscribeTable.LoadOrStore(event, subTable); loaded {
		return nil, fmt.Errorf("other thread write")
	}

	cache, ok := r.cache.Load(event)
	if ok {
		eventCache, ok := cache.(*queue.Queue)
		if !ok {
			return nil, fmt.Errorf("cache type failed")
		}

		for !eventCache.Empty() {
			if dataS, err := eventCache.Get(eventCache.Len()); err != nil {
				for _, data := range dataS {
					d, ok := data.(Information)
					if !ok {
						return nil, fmt.Errorf("data type failed")
					}

					subTable.Range(func(key, value interface{}) bool {
						ch, ok := value.(chan Information)
						if !ok {
							return false
						}
						ch <- d.DeepCopy()
						return true
					})
				}
			}
		}
	}

	return msg, nil
}

func (r *SupbInfoBase) UnSubscribe(label SubscriberLabel, event EventLabel) {
	table, ok := r.subscribeTable.LoadAndDelete(event)
	if !ok {
		return
	}

	subTable, ok := table.(*sync.Map)
	if !ok {
		return
	}

	subTable.Delete(label)
}

func (r *SupbInfoBase) Publish(label PublisherLabel, event EventLabel, data Information) error {
	//r.lock.Lock()
	//defer r.lock.Unlock()

	if len(r.queue) >= r.maxQueueLen {
		return fmt.Errorf("queue is full")
	}

	r.queue <- &informerEvent{
		info:  data,
		event: event,
	}

	return nil
}

func (r *SupbInfoBase) run() {
	for true {
		select {
		case <-r.ctx.Done():
			return
		case data := <-r.queue:
			table, ok := r.subscribeTable.Load(data.event)
			if !ok {
				table = &sync.Map{}
			}
			subTable, ok := table.(*sync.Map)
			if !ok {
				r.logger.Infof("type failed")
				continue
			}

			subTable.Range(func(key, value interface{}) bool {
				ch, ok := value.(chan Information)
				if !ok {
					return false
				}
				ch <- data.info.DeepCopy()
				return true
			})
		}
	}
}
