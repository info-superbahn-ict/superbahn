package supb_info_base

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"testing"
)

type testData struct {
	I int
}

func (r *testData) DeepCopy() Information {
	c:= *r
	return &c
}

func TestName(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()


	info := NewInformer(ctx, "test")

	go func() {
		a,_ := info.Subscribe("a1", "test")
		for {
			select {
			case data := <-a:
				fmt.Printf("a1 revice: %v\n", data)
			case <-ctx.Done():
				return
			}
		}
	}()
	go func() {
		a,_ := info.Subscribe("a2", "test")
		for {
			select {
			case data := <-a:
				fmt.Printf("a2 revice: %v\n", data)
			case <-ctx.Done():
				return
			}
		}
	}()
	go func() {
		a,_ := info.Subscribe("a3", "test")
		for {
			select {
			case data := <-a:
				fmt.Printf("a3 revice: %v\n", data)
			case <-ctx.Done():
				return
			}
		}
	}()


	for i:=0;i<10;i++ {
		fmt.Printf("pub %v\n",i)
		_ = info.Publish("pb","test",&testData{I: i})
	}

	<-ctx.Done()
}
