package supb_modules

import (
	"bytes"
	"context"
	"fmt"
	supb_info_base "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supb-info-base"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbindex"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supblog"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbmetric"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbrpc"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbrunner"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbspg"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbstrategy"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbtrace"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/pflag"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres"

	"github.com/spf13/viper"
	"strings"
)

const (
	SupbResources = "resources"
	SupbRpc       = "rpc"
	SupbSync      = "sync"
	SupbRunner = "runner"

	SupbMetrics   = "collector.metrics"
	SupbLog   = "collector.logs"
	SupbTrace = "collector.trace"
	SupbStrategyGate = "strategy.gate"

	SupbIndex  = "index"
	SupbSponge = "sponge"

	SupbInfoBase = "info.base"
)

var SupbModules = []string{SupbResources, SupbRpc,SupbSync,SupbRunner,SupbMetrics,
	SupbLog,SupbTrace,SupbStrategyGate,SupbIndex,SupbSponge,SupbInfoBase}

type SupbManager struct {
	resMgr *supbres.SupbResources
	rpcMgr *supbrpc.SupbRpc

	metricsClr *supbmetric.SupbMetrics
	logClr *supblog.SupbLog
	traceClr *supbtrace.SupbTrace

	strategyClr *supbstrategy.SupbStrategy
	runner  *supbrunner.SupbRunner

	index  *supbindex.Index
	spg  *supbspg.SupbSponge

	infoBase *supb_info_base.SupbInfoBase

	logger log.Logger
}

func NewSupbManager(ctx context.Context, prefix string) *SupbManager {
	return &SupbManager{
		resMgr: supbres.NewSupbResManager(ctx, strings.Join([]string{prefix, SupbResources}, ".")),
		rpcMgr: supbrpc.NewSupbRpc(ctx, strings.Join([]string{prefix, SupbRpc}, ".")),
		runner: supbrunner.NewSupbRunner(ctx,strings.Join([]string{prefix, SupbRunner}, ".")),

		metricsClr: supbmetric.NewSupbMetrics(ctx,strings.Join([]string{prefix, SupbMetrics}, ".")),
		logClr: supblog.NewSupbLog(ctx,strings.Join([]string{prefix, SupbLog}, ".")),
		traceClr: supbtrace.NewSupbTrace(ctx,strings.Join([]string{prefix, SupbTrace}, ".")),

		strategyClr: supbstrategy.NewSupbStrategy(ctx,strings.Join([]string{prefix, SupbStrategyGate}, ".")),

		index: supbindex.NewIndex(ctx,strings.Join([]string{prefix, SupbIndex}, ".")),
		spg: supbspg.NewSupbSponge(ctx,strings.Join([]string{prefix, SupbSponge}, ".")),

		logger: log.NewLogger(prefix),
	}
}

func (r *SupbManager) InitFlags(flags *pflag.FlagSet) {
	r.rpcMgr.InitFlags(flags)
	r.resMgr.InitFlags(flags)

	r.metricsClr.InitFlags(flags)
	r.logClr.InitFlags(flags)
	r.traceClr.InitFlags(flags)

	r.strategyClr.InitFlags(flags)
	r.runner.InitFlags(flags)
	r.index.InitFlags(flags)
	r.spg.InitFlags(flags)
}

func (r *SupbManager) ViperConfig(viper *viper.Viper) {
	r.rpcMgr.ViperConfig(viper)
	r.resMgr.ViperConfig(viper)

	r.metricsClr.ViperConfig(viper)
	r.logClr.ViperConfig(viper)
	r.traceClr.ViperConfig(viper)

	r.strategyClr.ViperConfig(viper)
	r.runner.ViperConfig(viper)
	r.index.ViperConfig(viper)
	r.spg.ViperConfig(viper)
}

func (r *SupbManager) InitViper(viper *viper.Viper) {
	r.rpcMgr.InitViper(viper)
	r.resMgr.InitViper(viper)

	r.metricsClr.InitViper(viper)
	r.logClr.InitViper(viper)
	r.traceClr.InitViper(viper)

	r.strategyClr.InitViper(viper)
	r.runner.InitViper(viper)
	r.index.InitViper(viper)
	r.spg.InitViper(viper)
}

func (r *SupbManager) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}


func (r *SupbManager) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
	r.rpcMgr.Initialize()

	r.metricsClr.Initialize(supbmetric.WithRPC(r.rpcMgr.RPC()))
	r.logClr.Initialize(supblog.WithRPC(r.rpcMgr.RPC()))
	r.strategyClr.Initialize()

	r.runner.Initialize(supbrunner.WithRPC(r.rpcMgr.RPC()))
	r.index.Initialize(supbindex.WithResMgr(r.resMgr))
	r.traceClr.Initialize()
	r.spg.Initialize(supbspg.WithRPC(r.rpcMgr.RPC()))
	r.resMgr.Initialize(supbres.WithSponge(r.spg),supbres.WithRunner(r.runner))

	//r.resSyncer.Initialize(sync_res.WithAppManager(r.resMgr.ApplicationsManager()),
	//	sync_res.WithStrategyManager(r.resMgr.StrategiesManager()))
}

func (r *SupbManager) Close() error {
	errs := new(bytes.Buffer)



	if err := r.resMgr.Close(); err != nil {
		errs.WriteString(fmt.Sprintf("close resource manager %v\n", err))
	}

	if err := r.metricsClr.Close(); err != nil {
		errs.WriteString(fmt.Sprintf("close metrics manager %v\n", err))
	}
	if err := r.logClr.Close(); err != nil {
		errs.WriteString(fmt.Sprintf("close log manager %v\n", err))
	}
	if err := r.traceClr.Close(); err != nil {
		errs.WriteString(fmt.Sprintf("close trace manager %v\n", err))
	}


	if err := r.strategyClr.Close(); err != nil {
		errs.WriteString(fmt.Sprintf("close strategy manager %v\n", err))
	}
	if err := r.runner.Close(); err != nil {
		errs.WriteString(fmt.Sprintf("close runner manager %v\n", err))
	}
	if err := r.index.Close(); err != nil {
		errs.WriteString(fmt.Sprintf("close index manager %v\n", err))
	}
	if err := r.spg.Close(); err != nil {
		errs.WriteString(fmt.Sprintf("close spg manager %v\n", err))
	}

	if err := r.rpcMgr.Close(); err != nil {
		errs.WriteString(fmt.Sprintf("close rpc manager %v\n", err))
	}

	if errs.Len() > 0 {
		return fmt.Errorf("%v", errs)
	}
	return nil
}

func (r *SupbManager) RPC() *supbrpc.SupbRpc {
	return r.rpcMgr
}

func (r *SupbManager) Res() *supbres.SupbResources {
	return r.resMgr
}

func (r *SupbManager) Metrics() *supbmetric.SupbMetrics {
	return r.metricsClr
}

func (r *SupbManager) Logs() *supblog.SupbLog {
	return r.logClr
}

func (r *SupbManager) StrategyGate()  *supbstrategy.SupbStrategy {
	return r.strategyClr
}

func (r *SupbManager) Index()  *supbindex.Index{
	return r.index
}

func (r *SupbManager) Trace()  *supbtrace.SupbTrace{
	return r.traceClr
}