package supb_modules

import (
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"
)

type option func(resources *SupbManager)

func WithRecorder(recorder supbapis.Recorder)option{
	return func(res *SupbManager) {
		res.resMgr.OptionConfig(supbres.WithRecorder(recorder))
	}
}

//func WithRunner(runner supbapis.Runner)option{
//	return func(res *SupbManager) {
//		res.resMgr.OptionConfig(supbres.WithRunner(runner))
//	}
//}