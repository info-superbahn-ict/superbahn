package supbindex

import "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres"

type option func(resources *Index)

func WithResMgr(res *supbres.SupbResources) option {
	return func(index *Index) {
		index.resmgr = res
	}
}