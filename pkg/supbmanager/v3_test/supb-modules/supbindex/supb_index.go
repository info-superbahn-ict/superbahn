package supbindex

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	TypeStrategy     = "dynamic.strategy"
	TypeApplication  = "dynamic.application"
	TypeRelationShip = "relationship"

	ModsStrategy1 = "strategy.applications"
	ModsStrategy2 = "strategy.metrics"
	ModsStrategy3 = "strategy.logs"
	ModsStrategy4 = "strategy.traces"
	ModsStrategy5 = "strategy.common"
	ModsStrategy6 = "strategy.custom"

	ModsApplication1 = "application.devices"
	ModsApplication2 = "application.metrics"
	ModsApplication3 = "application.logs"
	ModsApplication4 = "application.trace"
	ModsApplication5 = "application.common"
	ModsApplication6 = "application.custom"
)

type Modules struct {
	Key   string
	mods  map[string]string
	graph map[string]string
}

type ItemKey struct {
	Page string `json:"object.page"`
	Key  string `json:"object.key"`
	Kind string `json:"object.relationship.kind"`
	Name string `json:"object.name"`
}

type Item struct {
	Key  string `json:"object.key"`
	Page string `json:"object.page"`
	Kind string `json:"object.relationship.kind"`

	Name     string `json:"object.name"`
	PageName string `json:"object.page.name"`

	DetailModsSelf    string `json:"object.details.self"`
	DetailModsLogs    string `json:"object.details.logs"`
	DetailModsMetrics string `json:"object.details.metrics"`
	DetailModsTrace   string `json:"object.details.trace"`
	DetailModsCommon  string `json:"object.details.common"`
	DetailModsCustom  string `json:"object.details.custom"`
}

func (r *Item) DeepCopy() *Item {
	c := *r
	return &c
}

type Index struct {
	ctx    context.Context
	prefix string

	logger log.Logger
	resmgr *supbres.SupbResources

	list []ItemKey
	maps map[ItemKey]*Item
	//mods  map[Item]Modules
	//graph map[Item]string
}

func NewIndex(ctx context.Context, prefix string) *Index {
	return &Index{
		ctx:    ctx,
		prefix: prefix,
		logger: log.NewLogger(prefix),

		//mods:  make(map[Item]Modules),
		//graph: make(map[Item]string),
		list: []ItemKey{},
		maps: make(map[ItemKey]*Item),
	}
}

func (r *Index) InitFlags(flags *pflag.FlagSet) {
}

func (r *Index) ViperConfig(viper *viper.Viper) {

}

func (r *Index) InitViper(viper *viper.Viper) {

}

func (r *Index) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *Index) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}

}

func (r *Index) Close() error {
	r.logger.Infof("%v closed", r.prefix)
	return nil
}

func (r *Index) ReSortList(id []int) {
	newList := make([]ItemKey, len(id))
	for k, v := range id {
		newList[k] = r.list[v]
	}
	r.list = newList
}

func (r *Index) UpdateList(items []*Item) {
	r.logger.Infof("items %v", items)
	newList := make([]ItemKey, len(items))
	for k, v := range items {
		ik := r.keyFromItem(v)
		r.logger.Infof("items %v %v", v, ik)
		newList[k] = ik
	}
	r.list = newList
}

func (r *Index) GetList() []*Item {
	resList := make([]*Item, len(r.list))
	for k, v := range r.list {
		resList[k] = r.maps[v].DeepCopy()
	}

	return resList
}

func (r *Index) SwitchOn(key ItemKey, mod string) error {
	if r.check(key, mod) {
		if err := r.modOnOffWithCreate(key, mod, "on"); err != nil {
			return err
		}
		r.addItemToList(key)
		return nil
	}
	return fmt.Errorf("unknwon %v and %v", key, mod)
}

func (r *Index) SwitchOff(key ItemKey, mod string) error {
	if r.check(key, mod) {
		if err := r.modOnOffWithCreate(key, mod, "off"); err != nil {
			return err
		}
		if r.isAllOff(key) {
			r.deleteItemFromList(key)
		}
		return nil
	}
	return fmt.Errorf("unknwon %v and %v", key, mod)
}

func (r *Index) GraphAdd(iKey ItemKey) error {
	if r.itemContains(iKey) {
		return fmt.Errorf("contain item")
	}
	r.maps[iKey] = &Item{
		Key:      iKey.Key,
		Page:     iKey.Page,
		Kind:     iKey.Kind,
		Name:     iKey.Name,
		PageName: "图谱",
	}
	r.addItemToList(iKey)
	return nil
}

func (r *Index) GraphDelete(iKey ItemKey) error {
	if r.itemContains(iKey) {
		delete(r.maps, iKey)
		r.deleteItemFromList(iKey)
		return nil
	}

	return fmt.Errorf("not found ikey")
}

func (r *Index) GetSwitch(iKey ItemKey) map[string]string {
	if !r.itemContains(iKey) {
		if _, ok := r.maps[iKey]; !ok {
			r.maps[iKey] = &Item{
				Key:               iKey.Key,
				Page:              iKey.Page,
				Kind:              iKey.Kind,
				Name:              iKey.Name,
				DetailModsCustom:  "off",
				DetailModsSelf:    "off",
				DetailModsLogs:    "off",
				DetailModsMetrics: "off",
				DetailModsTrace:   "off",
				DetailModsCommon:  "off",
			}
			switch iKey.Page {
			case TypeStrategy:
				r.maps[iKey].PageName = "策略"
			case TypeApplication:
				r.maps[iKey].PageName = "应用"
			}
		}
	}

	switch iKey.Page {
	case TypeStrategy:
		res := make(map[string]string, 6)
		res[ModsStrategy1] = r.maps[iKey].DetailModsSelf
		res[ModsStrategy2] = r.maps[iKey].DetailModsMetrics
		res[ModsStrategy3] = r.maps[iKey].DetailModsLogs
		res[ModsStrategy4] = r.maps[iKey].DetailModsTrace
		res[ModsStrategy5] = r.maps[iKey].DetailModsCommon
		res[ModsStrategy6] = r.maps[iKey].DetailModsCustom
		return res
	case TypeApplication:
		res := make(map[string]string, 6)
		res[ModsApplication1] = r.maps[iKey].DetailModsSelf
		res[ModsApplication2] = r.maps[iKey].DetailModsMetrics
		res[ModsApplication3] = r.maps[iKey].DetailModsLogs
		res[ModsApplication4] = r.maps[iKey].DetailModsTrace
		res[ModsApplication5] = r.maps[iKey].DetailModsCommon
		res[ModsApplication6] = r.maps[iKey].DetailModsCustom
		return res
	default:
		return nil
	}
}

func (r *Index) check(item ItemKey, mod string) bool {
	switch item.Page {
	case TypeStrategy:
		switch mod {
		case ModsStrategy1:
			return true
		case ModsStrategy2:
			return true
		case ModsStrategy3:
			return true
		case ModsStrategy4:
			return true
		case ModsStrategy5:
			return true
		case ModsStrategy6:
			return true
		default:
			return false
		}
	case TypeApplication:
		switch mod {
		case ModsApplication1:
			return true
		case ModsApplication2:
			return true
		case ModsApplication3:
			return true
		case ModsApplication4:
			return true
		case ModsApplication5:
			return true
		case ModsApplication6:
			return true
		default:
			return false
		}
	default:
		return false
	}
}

func (r *Index) modOnOffWithCreate(iKey ItemKey, mod, statue string) error {
	if _, ok := r.maps[iKey]; !ok {
		r.maps[iKey] = &Item{
			Key:               iKey.Key,
			Page:              iKey.Page,
			Kind:              iKey.Kind,
			Name:              iKey.Name,
			DetailModsCustom:  "off",
			DetailModsSelf:    "off",
			DetailModsLogs:    "off",
			DetailModsMetrics: "off",
			DetailModsTrace:   "off",
			DetailModsCommon:  "off",
		}
		switch iKey.Page {
		case TypeStrategy:
			r.maps[iKey].PageName = "策略"
		case TypeApplication:
			r.maps[iKey].PageName = "应用"
		default:
			return fmt.Errorf("unkwon key")
		}

	}

	switch iKey.Page {
	case TypeStrategy:
		switch mod {
		case ModsStrategy1:
			r.maps[iKey].DetailModsSelf = statue
		case ModsStrategy2:
			r.maps[iKey].DetailModsMetrics = statue
		case ModsStrategy3:
			r.maps[iKey].DetailModsLogs = statue
		case ModsStrategy4:
			r.maps[iKey].DetailModsTrace = statue
		case ModsStrategy5:
			r.maps[iKey].DetailModsCommon = statue
		case ModsStrategy6:
			r.maps[iKey].DetailModsCustom = statue
		default:
			return fmt.Errorf("unknown strategy type")
		}
	case TypeApplication:
		switch mod {
		case ModsApplication1:
			r.maps[iKey].DetailModsSelf = statue
		case ModsApplication2:
			r.maps[iKey].DetailModsMetrics = statue
		case ModsApplication3:
			r.maps[iKey].DetailModsLogs = statue
		case ModsApplication4:
			r.maps[iKey].DetailModsTrace = statue
		case ModsApplication5:
			r.maps[iKey].DetailModsCommon = statue
		case ModsApplication6:
			r.maps[iKey].DetailModsCustom = statue
		default:
			return fmt.Errorf("unknown application type")
		}
	default:
		return fmt.Errorf("unknown type")
	}
	return nil
}

func (r *Index) isAllOff(iKey ItemKey) bool {
	if r.maps[iKey].DetailModsCommon != "off" {
		return false
	}
	if r.maps[iKey].DetailModsSelf != "off" {
		return false
	}
	if r.maps[iKey].DetailModsMetrics != "off" {
		return false
	}
	if r.maps[iKey].DetailModsLogs != "off" {
		return false
	}
	if r.maps[iKey].DetailModsTrace != "off" {
		return false
	}
	if r.maps[iKey].DetailModsCustom != "off" {
		return false
	}

	return true
}

func (r *Index) deleteItemFromList(iKey ItemKey) {
	if !r.itemContains(iKey) {
		return
	}

	list := make([]ItemKey, len(r.list)-1)

	id := 0
	for _, v := range r.list {
		if r.equal(v, iKey) {
			continue
		}
		list[id] = v
		id++
	}
	//r.logger.Infof("append item")
	r.list = list
}

func (r *Index) addItemToList(iKey ItemKey) {
	if !r.itemContains(iKey) {
		r.list = append(r.list, iKey)
	}
}

func (r *Index) TypeKeyPair(page, key string) (ItemKey, error) {
	switch page {
	case TypeStrategy:
		st, err := r.resmgr.GetStrategy(types.DynamicStrategy, key)
		if err != nil {
			return ItemKey{}, err
		}
		return ItemKey{
			Page: page,
			Key:  key,
			Name: st.StrategyName,
			Kind: "",
		}, nil
	case TypeApplication:
		app, err := r.resmgr.GetApplication(types.DynamicApplication, key)
		if err != nil {
			return ItemKey{}, err
		}
		return ItemKey{
			Page: page,
			Key:  key,
			Name: app.ApplicationName,
			Kind: "",
		}, nil

	default:
		return ItemKey{Page: page, Key: key}, fmt.Errorf("unkown type")
	}
}

func (r *Index) TypeKeyPairRelationShip(page string, key, kind, name string) (ItemKey, error) {
	switch page {
	case TypeRelationShip:
		return ItemKey{Page: TypeRelationShip, Key: key, Name: name, Kind: kind}, nil
	default:
		return ItemKey{}, fmt.Errorf("unknown page")
	}
}

func (r *Index) itemContains(iKey ItemKey) bool {
	for _, v := range r.list {
		if r.equal(v, iKey) {
			return true
		}
	}
	return false
}

func (r *Index) equal(iKey ItemKey, item ItemKey) bool {
	if iKey.Key == item.Key && iKey.Page == item.Page && iKey.Kind == item.Kind && iKey.Name == item.Name {
		return true
	}
	return false
}

func (r *Index) keyFromItem(item *Item) ItemKey {
	return ItemKey{
		Key:  item.Key,
		Page: item.Page,
		Kind: item.Kind,
		Name: item.Name,
	}
}
