package log

import "time"

type ContainerLog struct {
	Type      string    `json:"type"`
	Timestamp time.Time `json:"timestamp"`
	Msg       interface{}    `json:"msg"`
}

type LogsInfo struct {
	Level  string    `json:"type"`
	Time   time.Time `json:"time"`
	Msg    string    `json:"msg"`
	Prefix string    `json:"prefix"`
}

type ContainerLogRes struct {
	Type      string    `json:"type"`
	Timestamp string `json:"timestamp"`
	Msg       interface{}    `json:"msg"`
}

func (r *ContainerLog) DeepCopy() *ContainerLog {
	c := *r
	return &c
}
