package supblog

import (
	"context"
	"fmt"
	supblog "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supblog/log"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"strings"
	"sync"
	"time"
)

const (
	managerGuid = ".guid"
	MaxLen      = 301
	logDebug    = "logDebug"
	tryTime     = 10
	tryInterval = 500
	LogsPushOff = "off"
)

type SupbLog struct {
	guid     string
	logDebug bool
	ctx      context.Context
	prefix   string
	rpc      nervous.Controller
	logger   log.Logger

	lock      *sync.RWMutex
	//dataIndex map[string]int
	dataIndex *sync.Map
	//data      map[string][]supblog.ContainerLog
	data      *sync.Map
	index     int
}

func NewSupbLog(ctx context.Context, prefix string) *SupbLog {
	return &SupbLog{
		ctx:    ctx,
		prefix: prefix,
		logger: log.NewLogger(prefix),

		lock:      &sync.RWMutex{},
		//dataIndex: make(map[string]int),
		dataIndex: &sync.Map{},
		//data:      make(map[string][]supblog.ContainerLog),
		data:      &sync.Map{},
	}
}

func (r *SupbLog) InitFlags(flags *pflag.FlagSet) {
	flags.Bool(r.prefix+logDebug, false, "log debug flag")
	flags.String(r.prefix+managerGuid, define.RPCCommonGuidOfManager, "log debug flag")
}

func (r *SupbLog) ViperConfig(viper *viper.Viper) {

}

func (r *SupbLog) InitViper(viper *viper.Viper) {
	r.logDebug = viper.GetBool(r.prefix + logDebug)
	r.guid = viper.GetString(r.prefix + managerGuid)
}

func (r *SupbLog) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *SupbLog) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}

	if r.logDebug {
		go r.productData()
	} else {
		if err := r.getLogInfo(); err != nil {
			r.logger.Errorf("getLogInfo failed: %v", err)
		}
	}

	r.logger.Infof("guid %v",r.guid)

	r.logger.Infof("%v initialized", r.prefix)
}

func (r *SupbLog) Close() error {
	r.logger.Infof("%v closed", r.prefix)
	if !r.logDebug {
		r.lock.Lock()
		r.data.Range(func(key, value interface{}) bool {
			if _, err := r.rpc.RPCCallCustom(key.(string), tryTime, tryInterval, define.RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush, LogsPushOff); err != nil {
				r.logger.Errorf("agent open log push: %v", err)
			}
			return true
		})

		//for guid, _ := range r.data {
		//	if _, err := r.rpc.RPCCallCustom(guid, tryTime, tryInterval, define.RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush, LogsPushOff); err != nil {
		//		r.logger.Errorf("agent open log push: %v", err)
		//	}
		//
		//}
		r.lock.Unlock()
	}
	return nil
}

func (r *SupbLog) DeleteGuid(guid string) {
	if r.logDebug {
		if _,ok := r.data.Load(guid);!ok {
			return
		}
		//if _, ok := r.data[guid]; !ok {
		//	return
		//}

		r.lock.Lock()
		defer r.lock.Unlock()

		r.data.Delete(guid)
		//delete(r.data, guid)
	}else {
		if _,ok := r.data.Load(guid);!ok {
			return
		}
		//if _, ok := r.data[guid]; !ok {
		//	return
		//}

		r.lock.Lock()
		defer r.lock.Unlock()

		r.data.Delete(guid)
		//delete(r.data, guid)
		if _, err := r.rpc.RPCCallCustom(guid, tryTime, tryInterval, define.RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush, LogsPushOff); err != nil {
			r.logger.Errorf("agent open log push: %v", err)
		}
	}
}

func (r *SupbLog) IsMonitor(guid string) bool {
	_,ok := r.data.Load(guid)
	//_, ok := r.data[guid]
	return ok
}

func (r *SupbLog) AddGuid(guid string) {

	//r.logger.Infof("add guid %v",guid)
	if r.logDebug {
		//if _, ok := r.data[guid]; !ok {
		//	r.data[guid] = make([]supblog.ContainerLog, MaxLen)
		//}
		dt,_ := r.data.LoadOrStore(guid,make([]supblog.ContainerLog, MaxLen))

		r.lock.Lock()
		defer r.lock.Unlock()
		now := time.Now()
		for i := 0; i < MaxLen; i++ {
			dt.([]supblog.ContainerLog)[i] = supblog.GetLogInfo()
			dt.([]supblog.ContainerLog)[i].Timestamp = now.Add(time.Duration(i-MaxLen) * time.Second)

			//r.data[guid][i] = supblog.GetLogInfo()
			//r.data[guid][i].Timestamp = now.Add(time.Duration(i-MaxLen) * time.Second)
		}
	} else {
		r.data.LoadOrStore(guid,make([]supblog.ContainerLog, MaxLen))
		r.dataIndex.LoadOrStore(guid,0)
		for i:=0 ;i <10 ; i ++ {
			if _, err := r.rpc.RPCCallCustom(guid, tryTime, tryInterval, define.RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush, r.guid); err == nil {
				break
			}
			time.Sleep(time.Second/2)
		}

		//if _, ok := r.data[guid]; !ok {
		//	r.data[guid] = make([]supblog.ContainerLog, MaxLen)
		//	r.dataIndex[guid] = 0
		//	for i:=0 ;i <10 ; i ++ {
		//		if _, err := r.rpc.RPCCallCustom(guid, tryTime, tryInterval, define.RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush, r.guid); err != nil {
		//			time.Sleep(time.Second/2)
		//			continue
		//		}
		//	}
		//	r.logger.Infof("agent open log push: %v", r.guid)
		//}
	}
}

func (r *SupbLog) GetLogLatest(guid string, ct int) ([]*supblog.ContainerLogRes, error) {
	dt,ok := r.data.Load(guid)
	if !ok {
		return nil, fmt.Errorf("guid is nil")
	}
	
	//if _, ok := r.data[guid]; !ok {
	//	return nil, fmt.Errorf("guid is nil")
	//}
	

	if ct >= MaxLen {
		ct = MaxLen - 1
	}

	ms := make([]*supblog.ContainerLogRes, ct)

	if r.logDebug {
		tail := r.index
		head := (tail + MaxLen - ct) % MaxLen

		for i, k := head, 0; i != tail; k++ {
			ms[k] = &supblog.ContainerLogRes{
				Type:      dt.([]supblog.ContainerLog)[i].Type,
				Timestamp: dt.([]supblog.ContainerLog)[i].Timestamp.Format("2006/01/02 15:04:05"),
				Msg:       dt.([]supblog.ContainerLog)[i].Msg,
			}
			i = (i + 1) % MaxLen
		}
		
		//for i, k := head, 0; i != tail; k++ {
		//	ms[k] = &supblog.ContainerLogRes{
		//		Type:      r.data[guid][i].Type,
		//		Timestamp: r.data[guid][i].Timestamp.Format("2006/01/02 15:04:05"),
		//		Msg:       r.data[guid][i].Msg,
		//	}
		//	i = (i + 1) % MaxLen
		//}
		r.logger.Infof("%v", len(ms))
	}else {
		tail,_ := r.dataIndex.Load(guid)
		//tail := r.dataIndex[guid]
		head := (tail.(int) + MaxLen - ct) % MaxLen
		for i, k := head, 0; i != tail; k++ {
			ms[k] = &supblog.ContainerLogRes{
				Type:      dt.([]supblog.ContainerLog)[i].Type,
				Timestamp: dt.([]supblog.ContainerLog)[i].Timestamp.Format("2006/01/02 15:04:05"),
				Msg:       dt.([]supblog.ContainerLog)[i].Msg,
			}
			i = (i + 1) % MaxLen
		}
		//for i, k := head, 0; i != tail; k++ {
		//	ms[k] = &supblog.ContainerLogRes{
		//		Type:      r.data[guid][i].Type,
		//		Timestamp: r.data[guid][i].Timestamp.Format("2006/01/02 15:04:05"),
		//		Msg:       r.data[guid][i].Msg,
		//	}
		//	i = (i + 1) % MaxLen
		//}
		r.logger.Infof("%v", len(ms))
	}

	return ms, nil
}

func (r *SupbLog) GetLogWithFilter(guid string, ct int, filter map[string]string) ([]*supblog.ContainerLogRes, error) {
	r.logger.Infof("guid %v ", guid)
	//if _, ok := r.data[guid]; !ok {
	//	return nil, fmt.Errorf("guid is nil")
	//}

	dt,ok := r.data.Load(guid)
	if !ok {
		return nil, fmt.Errorf("guid is nil")
	}

	if r.logDebug {
		ms := make([]*supblog.ContainerLog, ct)

		var realIndex = 0
		for i := r.index + 1; i != r.index && realIndex < ct; {
			l := dt.([]supblog.ContainerLog)[i]
			//l := r.data[guid][i]

			match := true
			for k, v := range filter {
				switch k {
				case "type":
					if !strings.EqualFold(l.Type, v) {
						match = false
					}
				case "context":
					if !strings.Contains(l.Msg.(map[string]interface{})["msg"].(string), v) {
						match = false
					}
				default:
					return nil, fmt.Errorf("unknown filter type")
				}
			}

			if match {
				ms[realIndex] = l.DeepCopy()
				realIndex++
			}

			i = (i + 1) % MaxLen
		}

		res := make([]*supblog.ContainerLogRes, realIndex)
		for i := 0; i < realIndex; i++ {
			res[i] = &supblog.ContainerLogRes{
				Type:      ms[i].Type,
				Timestamp: ms[i].Timestamp.Format("2006/01/02 15:04:05"),
				Msg:       ms[i].Msg,
			}
		}
		return res, nil
	}else {
		ms := make([]*supblog.ContainerLog, ct)

		var realIndex = 0
		is,_:= r.dataIndex.Load(guid)
		for i := is.(int) + 1; i != is.(int) && realIndex < ct; {
			l := dt.([]supblog.ContainerLog)[i]
			//l := r.data[guid][i]

			match := true
			for k, v := range filter {
				switch k {
				case "type":
					if !strings.EqualFold(l.Type, v) {
						match = false
					}
				case "context":
					if !strings.Contains(l.Msg.(map[string]interface{})["msg"].(string), v) {
						match = false
					}
				default:
					return nil, fmt.Errorf("unknown filter type")
				}
			}

			if match {
				ms[realIndex] = l.DeepCopy()
				realIndex++
			}

			i = (i + 1) % MaxLen
		}

		res := make([]*supblog.ContainerLogRes, realIndex)
		for i := 0; i < realIndex; i++ {
			res[i] = &supblog.ContainerLogRes{
				Type:      ms[i].Type,
				Timestamp: ms[i].Timestamp.Format("2006/01/02 15:04:05"),
				Msg:       ms[i].Msg,
			}
		}
		return res, nil
	}
}

func (r *SupbLog) GetLogStatistic(guid string) (map[string]map[string]int64, error) {
	//if _, ok := r.data[guid]; !ok {
	//	return nil, fmt.Errorf("guid is nil")
	//}
	dt,ok := r.data.Load(guid)
	if !ok {
		return nil, fmt.Errorf("guid is nil")
	}

	res := make(map[string]map[string]int64)
	//
	//for _,v := range r.LogLevels() {
	//	res[v] = make(map[time.Time]int64)
	//}

	if r.logDebug {
		for i := r.index + 1; i != r.index; {
			tp := dt.([]supblog.ContainerLog)[i].Type
			//tp := r.data[guid][i].Type
			sp := dt.([]supblog.ContainerLog)[i].Timestamp.Format("2006/01/02")
			//sp := r.data[guid][i].Timestamp.Format("2006/01/02")
			if _, ok := res[sp]; !ok {
				res[sp] = make(map[string]int64)
				for _, v := range r.LogLevels() {
					res[sp][v] = 0
				}
			}
			res[sp][tp]++
			i = (i + 1) % MaxLen
		}
	}else {
		is,_ := r.dataIndex.Load(guid)
		for i := is.(int) + 1; i != is.(int); {
			tp := dt.([]supblog.ContainerLog)[i].Type
			//tp := r.data[guid][i].Type
			sp := dt.([]supblog.ContainerLog)[i].Timestamp.Format("2006/01/02")
			//sp := r.data[guid][i].Timestamp.Format("2006/01/02")
			if _, ok := res[sp]; !ok {
				res[sp] = make(map[string]int64)
				for _, v := range r.LogLevels() {
					res[sp][v] = 0
				}
			}
			res[sp][tp]++
			i = (i + 1) % MaxLen
		}
	}

	return res, nil
}

//func (r *SupbLog) GetLogs(guid string, td time.Duration) ([]*supblog.ContainerLog, error) {
//	if _, ok := r.data[guid]; !ok {
//		return nil, fmt.Errorf("guid is nil")
//	}
//
//	now := time.Now()
//	before := now.Add(-td)
//	gp := int(td.Seconds())
//
//	if gp >= MaxLen {
//		gp = MaxLen - 1
//	}
//	tail := r.index
//	head := (tail + MaxLen - gp) % MaxLen
//
//	//r.logger.Infof("%v %v",r.data[guid][head].Timestamp,before)
//
//	if r.data[guid][head].Timestamp.Before(before) {
//		head = (head + 1) % MaxLen
//	}
//
//	ct := (tail + MaxLen - head) % MaxLen
//	if tail == head {
//		ct = MaxLen - 1
//	}
//	ms := make([]*supblog.ContainerLog, ct)
//
//	//r.logger.Infof("%v %v %v %v %v",now,before,gp,head,tail)
//
//	for i, k := head, 0; i != tail; k++ {
//		ms[k] = r.data[guid][i].DeepCopy()
//		i = (i + 1) % MaxLen
//	}
//
//	return ms, nil
//}

func (r *SupbLog) productData() {
	now := time.Now()
	for i := 0; i < MaxLen; i++ {
		r.data.Range(func(key, value interface{}) bool {
			value.([]supblog.ContainerLog)[i] = supblog.GetLogInfo()
			value.([]supblog.ContainerLog)[i].Timestamp = now.Add(time.Duration(i-MaxLen) * time.Second)
			return true
		})
		
		
		//for guid := range r.data {
		//	r.data[guid][i] = supblog.GetLogInfo()
		//	r.data[guid][i].Timestamp = now.Add(time.Duration(i-MaxLen) * time.Second)
		//}
	}

	r.index = 0
	tk := time.NewTicker(time.Second)
	for {
		select {
		case <-r.ctx.Done():
			return
		case <-tk.C:
			now := time.Now()
			r.lock.Lock()

			r.data.Range(func(key, value interface{}) bool {
				value.([]supblog.ContainerLog)[r.index] = supblog.GetLogInfo()
				value.([]supblog.ContainerLog)[r.index].Timestamp = now
				return true
			})

			//for guid := range r.data {
			//	r.data[guid][r.index] = supblog.GetLogInfo()
			//	r.data[guid][r.index].Timestamp = now
			//}

			r.index = (r.index + 1) % MaxLen
			r.lock.Unlock()
		}
	}
}

func (r *SupbLog) LogLevels() []string {
	return supblog.Types()
}

func (r *SupbLog) getLogInfo() error {
	err := r.rpc.RPCRegister(define.RPCFunctionNameOfManagerStrategyCollectorObjectLogs, func(args ...interface{}) (interface{}, error) {
		if len(args) < 2 {
			return "", fmt.Errorf("need two args, guid and data")
		}

		r.logger.Infof("recive %v",args[0].(string))

		now := time.Now()
		guid := args[0].(string)
		logInfo := args[1].(string)
		//logInfoTemp := &supblog.LogsInfo{}

		//if err := json.Unmarshal([]byte(logInfo),logInfoTemp);err ==nil {
		//	r.lock.Lock()
		//	if _, ok := r.data[guid]; !ok {
		//		r.data[guid] = make([]supblog.ContainerLog, MaxLen)
		//		for i := 0; i < MaxLen; i++ {
		//			r.data[guid][i] = supblog.ContainerLog{
		//				Timestamp: logInfoTemp.Time,
		//				Msg:       map[string]string{
		//					"msg":logInfoTemp.Msg,
		//					"prefix":logInfoTemp.Prefix,
		//				},
		//				Type:      logInfoTemp.Level,
		//			}
		//		}
		//		r.dataIndex[guid] = 0
		//	} else {
		//		index := r.dataIndex[guid]
		//		r.data[guid][index] = supblog.ContainerLog{
		//			Timestamp: logInfoTemp.Time,
		//			Msg:       map[string]string{
		//				"msg":logInfoTemp.Msg,
		//				"prefix":logInfoTemp.Prefix,
		//			},
		//			Type:      logInfoTemp.Level,
		//		}
		//		r.dataIndex[guid] = (r.dataIndex[guid] + 1) % MaxLen
		//	}
		//	r.lock.Unlock()
		//	return "RECEIVE", nil
		//}else {
			r.lock.Lock()

			if dt, ok := r.data.LoadOrStore(guid,make([]supblog.ContainerLog, MaxLen));!ok {
				for i := 0; i < MaxLen; i++ {
					dt.([]supblog.ContainerLog)[i] = supblog.ContainerLog{
						Timestamp: now.Add(time.Duration(i-MaxLen) * time.Second),
						Msg:       logInfo,
						Type:      "info",
					}
				}
				r.dataIndex.Store(guid,0)
			}else {
				is,_:= r.dataIndex.Load(guid)
				dt.([]supblog.ContainerLog)[is.(int)] = supblog.ContainerLog{
					Timestamp: now,
					Msg:       logInfo,
					Type:      "INFO",
				}
				r.dataIndex.Store(guid,(is.(int)+1)%MaxLen)

				r.data.Store(guid,dt)
			}

			//if _, ok := r.data[guid]; !ok {
			//	r.data[guid] = make([]supblog.ContainerLog, MaxLen)
			//	for i := 0; i < MaxLen; i++ {
			//		r.data[guid][i] = supblog.ContainerLog{
			//			Timestamp: now.Add(time.Duration(i-MaxLen) * time.Second),
			//			Msg:       logInfo,
			//			Type:      "info",
			//		}
			//	}
			//	r.dataIndex[guid] = 0
			//} else {
			//	index := r.dataIndex[guid]
			//	r.data[guid][index] = supblog.ContainerLog{
			//		Timestamp: now,
			//		Msg:       logInfo,
			//		Type:      "INFO",
			//	}
			//	r.dataIndex[guid] = (r.dataIndex[guid] + 1) % MaxLen
			//}
			r.lock.Unlock()
			return "RECEIVE", nil
		//}
	})
	if err != nil {
		return err
	}
	return nil
}
