package supblog

import nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"

type option func(resources *SupbLog)

func WithRPC(rpc nervous.Controller) option {
	return func(spg *SupbLog) {
		spg.rpc = rpc
	}
}