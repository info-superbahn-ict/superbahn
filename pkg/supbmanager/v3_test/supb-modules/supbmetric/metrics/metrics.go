package metrics

import (
	"time"
)

type ContainerMetrics struct {
	CpuTotal         float64   `json:"cpu_total"`
	CPUPercentage    float64   `json:"cpu_percentage"`
	MemoryTotal      float64   `json:"memory_total"`
	MemoryPercentage float64   `json:"memory_percentage"`
	NetworkRx        float64   `json:"networkRx"`
	NetworkTx        float64   `json:"networkTx"`
	Timestamp        time.Time `json:"timestamp"`
}

func (r *ContainerMetrics) DeepCopy() *ContainerMetrics {
	c := *r
	return &c
}

type CollectorMetrics struct {
	Name             string    `json:"name"`
	ID               string    `json:"id"`
	CPUPercentage    float64   `json:"cpuPercentage"`
	Memory           float64   `json:"memory"`
	MemoryPercentage float64   `json:"memoryPercentage"`
	MemoryLimit      float64   `json:"memoryLimit"`
	NetworkRx        float64   `json:"networkRx"`
	NetworkTx        float64   `json:"networkTx"`
	BlockRead        float64   `json:"blockRead"`
	BlockWrite       float64   `json:"blockWrite"`
	PidsCurrent      uint64    `json:"pidsCurrent"`
	Timestamp        time.Time `json:"timestamp"`
}
