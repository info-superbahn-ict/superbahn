package metrics

import (
	"fmt"
	"strconv"
	"testing"
)

func TestName(t *testing.T) {
	//a := time.Now()

	//fmt.Printf("%v %v",time.Now().Unix(),1640852440633467)
	//fmt.Printf("%v",time.Now().Format(time.RFC850))
	//fmt.Printf("%v",time.Unix(1640685568,0))
	//fmt.Printf("\n%v 		%v\n",a.Second(),a.Unix())
	//fmt.Printf("\n%v 		%v\n",time.Unix(a.Unix()+1,0),time.Unix(a.Unix(),0))

	a1 := make(chan string, 0)

	for i:=0;i<10; i++ {
		a1 <- strconv.Itoa(i)
	}

	for i:=0;i<10; i++ {
		k := <-a1
		fmt.Printf("%v\n",k)
	}

}
