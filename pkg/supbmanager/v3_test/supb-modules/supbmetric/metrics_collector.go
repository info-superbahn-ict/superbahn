package supbmetric

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbmetric/metrics"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"math"
	"math/rand"
	"sync"
	"time"
)

const (
	managerGuid    = ".guid"
	tryTime        = 100
	tryInterval    = 500
	MaxLen         = 3601
	metricsDebug   = "metricsDebug"
	MetricsPushOff = "off"
)

type SupbMetrics struct {
	ctx          context.Context
	prefix       string
	Guid         string
	metricsDebug bool
	rpc          nervous.Controller
	logger       log.Logger

	lock *sync.RWMutex
	dataIndex map[string]int
	//dataIndex *sync.Map
	//data      map[string][]metrics.ContainerMetrics
	data  *sync.Map
	index int
}

func NewSupbMetrics(ctx context.Context, prefix string) *SupbMetrics {
	return &SupbMetrics{
		ctx:    ctx,
		prefix: prefix,
		logger: log.NewLogger(prefix),

		lock: &sync.RWMutex{},
		//data: make(map[string][]metrics.ContainerMetrics),
		data: &sync.Map{},
		dataIndex: make(map[string]int),
		//dataIndex: &sync.Map{},
	}
}

func (r *SupbMetrics) InitFlags(flags *pflag.FlagSet) {
	flags.Bool(r.prefix+metricsDebug, false, "metrics debug flag")
	flags.String(r.prefix+managerGuid, define.RPCCommonGuidOfManager, "log debug flag")
}

func (r *SupbMetrics) ViperConfig(viper *viper.Viper) {

}

func (r *SupbMetrics) InitViper(viper *viper.Viper) {
	r.metricsDebug = viper.GetBool(r.prefix + metricsDebug)
	r.Guid = viper.GetString(r.prefix + managerGuid)
}

func (r *SupbMetrics) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *SupbMetrics) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}

	if r.metricsDebug {
		go r.productData()
	} else {
		if err := r.getMetricsInfo(); err != nil {
			log.Errorf("getMetricsInfo: %v", err)
		}
	}

	r.logger.Infof("%v initialized", r.prefix)
}

func (r *SupbMetrics) Close() error {
	if !r.metricsDebug {
		r.data.Range(func(key, value interface{}) bool {
			if _, err := r.rpc.RPCCallCustom(key.(string), tryTime, tryInterval, define.RPCFunctionNameOfObjectRunningOnAgentForOpenMetricsPush, MetricsPushOff); err != nil {
				r.logger.Errorf("agent open metrics push: %v", err)
			}
			return true
		})

		//for guid, _ := range r.data {
		//	if _, err := r.rpc.RPCCallCustom(guid, tryTime, tryInterval, define.RPCFunctionNameOfObjectRunningOnAgentForOpenMetricsPush, MetricsPushOff); err != nil {
		//		r.logger.Errorf("agent open metrics push: %v", err)
		//	}
		//}
	}
	r.logger.Infof("%v closed", r.prefix)
	return nil
}

func (r *SupbMetrics) DeleteGuid(guid string) {
	if _, ok := r.data.Load(guid); !ok {
		return
	}

	//if _, ok := r.data[guid]; !ok {
	//	return
	//}

	//r.lock.Lock()
	//defer r.lock.Unlock()

	//delete(r.data, guid)
	r.data.Delete(guid)
}

func (r *SupbMetrics) IsMonitor(guid string) bool {
	//_, ok := r.data[guid]
	_, ok := r.data.Load(guid)
	return ok
}

func (r *SupbMetrics) AddGuid(guid string) {
	if r.metricsDebug {
		dt, _ := r.data.LoadOrStore(guid, make([]metrics.ContainerMetrics, MaxLen))

		//if _, ok := r.data[guid]; !ok {
		//	r.data[guid] = make([]metrics.ContainerMetrics, MaxLen)
		//}

		r.lock.Lock()
		defer r.lock.Unlock()

		now := time.Now()
		for i := 0; i < MaxLen; i++ {
			baseCPU := (1+math.Sin(math.Pi*2*float64(i)/MaxLen))*20 + 40
			baseMemory := (1+math.Cos(math.Pi*2*float64(i)/MaxLen))*20 + 40
			baseRx := rand.NormFloat64()*50 + 500
			baseTx := rand.NormFloat64()*50 + 500

			rd := rand.NormFloat64() * 5

			//r.data[guid][i].CPUPercentage = baseCPU + rd
			//r.data[guid][i].MemoryPercentage = baseMemory + rd
			//r.data[guid][i].CpuTotal = 4000
			//r.data[guid][i].NetworkRx = baseRx
			//r.data[guid][i].NetworkTx = baseTx
			//r.data[guid][i].MemoryTotal = 8 * 1024 * 1024 * 1024
			//r.data[guid][i].Timestamp = now.Add(time.Duration(i-MaxLen) * time.Second)

			dt.([]metrics.ContainerMetrics)[i].CPUPercentage = baseCPU + rd
			dt.([]metrics.ContainerMetrics)[i].MemoryPercentage = baseMemory + rd
			dt.([]metrics.ContainerMetrics)[i].CpuTotal = 4000
			dt.([]metrics.ContainerMetrics)[i].NetworkRx = baseRx
			dt.([]metrics.ContainerMetrics)[i].NetworkTx = baseTx
			dt.([]metrics.ContainerMetrics)[i].MemoryTotal = 8 * 1024 * 1024 * 1024
			dt.([]metrics.ContainerMetrics)[i].Timestamp = now.Add(time.Duration(i-MaxLen) * time.Second)
		}
	} else {
		//r.dataIndex.Store(guid, 0)
		if _, ok := r.data.LoadOrStore(guid, make([]metrics.ContainerMetrics, MaxLen)); !ok {
			r.dataIndex[guid] = 0
			for i := 0; i < 10; i++ {
				if _, err := r.rpc.RPCCallCustom(guid, tryTime, tryInterval, define.RPCFunctionNameOfObjectRunningOnAgentForOpenMetricsPush, r.Guid); err == nil {
					break
				}
				time.Sleep(time.Second / 2)
			}
		}

		//if _, ok := r.data[guid]; !ok {
		//	r.data[guid] = make([]metrics.ContainerMetrics, MaxLen)
		//	r.dataIndex[guid] = 0
		//	for i:=0 ;i <10 ; i ++ {
		//		if _, err := r.rpc.RPCCallCustom(guid, tryTime, tryInterval, define.RPCFunctionNameOfObjectRunningOnAgentForOpenMetricsPush, r.Guid); err != nil {
		//			time.Sleep(time.Second/2)
		//			continue
		//		}
		//	}
		//	r.logger.Infof("open metrics %v to %v",guid,r.Guid)
		//}
	}
}

func (r *SupbMetrics) GetCPUWithTimeRange(guid string, start, end time.Time) ([][2]interface{}, error) {
	if r.metricsDebug {
		if end.Before(start) {
			return nil, fmt.Errorf("time range error")
		}

		if _, ok := r.data.Load(guid); !ok {
			series := make([][2]interface{}, end.Unix()-start.Unix()+1)
			for i, k := start, 0; !i.After(end); i, k = i.Add(time.Second), k+1 {
				series[k][0] = i.Format("2006/01/02 15:04:05")
				series[k][1] = 0.0
			}
			return series, nil
		}

		//if _, ok := r.data[guid]; !ok {
		//	series := make([][2]interface{}, end.Unix()-start.Unix()+1)
		//	for i, k := start, 0; !i.After(end); i, k = i.Add(time.Second), k+1 {
		//		series[k][0] = i.Format("2006/01/02 15:04:05")
		//		series[k][1] = 0.0
		//	}
		//	return series, nil
		//}

		id := 0
		for i := r.index + 1; i != r.index; {
			d, _ := r.data.Load(guid)

			if !d.([]metrics.ContainerMetrics)[i].Timestamp.Before(start) {
				id = i
				break
			}
			i = (i + 1) % MaxLen
		}

		series := make([][2]interface{}, (r.index+MaxLen-id)%MaxLen)
		for i, k := id, 0; i != r.index; i, k = (i+1)%MaxLen, k+1 {
			d, _ := r.data.Load(guid)

			series[k][0] = d.([]metrics.ContainerMetrics)[i].Timestamp.Format("2006/01/02 15:04:05")
			series[k][1] = d.([]metrics.ContainerMetrics)[i].CPUPercentage
		}
		return series, nil
	} else {

		if end.Before(start) {
			return nil, fmt.Errorf("time range error")
		}

		if _, ok := r.data.Load(guid); !ok {
			return nil, fmt.Errorf("guid is nil")
		}

		//if _, ok := r.data[guid]; !ok {
		//	//series := make([][2]interface{}, end.Unix()-start.Unix()+1)
		//	//for i, k := start, 0; !i.After(end); i, k = i.Add(time.Second), k+1 {
		//	//	series[k][0] = i.Format("2006/01/02 15:04:05")
		//	//	series[k][1] = 0.0
		//	//}
		//	//return series, fmt.Errorf("guid is nil")
		//	return nil, fmt.Errorf("guid is nil")
		//}

		is, _ := r.dataIndex[guid]
		id := ((is - 300) +MaxLen) % MaxLen

		z := metrics.ContainerMetrics{}
		d, _ := r.data.Load(guid)
		if is < 300 && d.([]metrics.ContainerMetrics)[MaxLen-1].Timestamp.Equal(z.Timestamp) {
			id = 0
		}

		//id := 0
		//is, _ := r.dataIndex.Load(guid)
		//for i := is.(int) + 1; i != is.(int); {
		//	dt, _ := r.data.Load(guid)
		//	if !dt.([]metrics.ContainerMetrics)[i].Timestamp.Before(start) {
		//		id = i
		//		r.logger.Infof("%v %v ", dt.([]metrics.ContainerMetrics)[i].Timestamp, start.UTC())
		//		break
		//	}
		//	i = (i + 1) % MaxLen
		//}

		series := make([][2]interface{}, (is+MaxLen-id)%MaxLen)
		dt, _ := r.data.Load(guid)
		for i, k := id, 0; i != is; i, k = (i+1)%MaxLen, k+1 {
			series[k][0] = dt.([]metrics.ContainerMetrics)[i].Timestamp.Format("2006/01/02 15:04:05")
			series[k][1] = dt.([]metrics.ContainerMetrics)[i].CPUPercentage
		}
		r.logger.Infof("#####%v %v %v %v", id, is, dt.([]metrics.ContainerMetrics)[is])
		return series, nil
	}
}

func (r *SupbMetrics) GetMemoryWithTimeRange(guid string, start, end time.Time) ([][2]interface{}, error) {
	if r.metricsDebug {
		if end.Before(start) {
			return nil, fmt.Errorf("time range error")
		}

		if _, ok := r.data.Load(guid); !ok {
			series := make([][2]interface{}, end.Unix()-start.Unix()+1)
			for i, k := start, 0; !i.After(end); i, k = i.Add(time.Second), k+1 {
				series[k][0] = i.Format("2006/01/02 15:04:05")
				series[k][1] = 0.0
			}
			return series, nil
		}

		//if _, ok := r.data[guid]; !ok {
		//	series := make([][2]interface{}, end.Unix()-start.Unix()+1)
		//	for i, k := start, 0; !i.After(end); i, k = i.Add(time.Second), k+1 {
		//		series[k][0] = i.Format("2006/01/02 15:04:05")
		//		series[k][1] = 0.0
		//	}
		//	return series, nil
		//}

		is, _ := r.dataIndex[guid]
		id := ((is - 300) +MaxLen) % MaxLen

		//id := 0
		//for i := r.index + 1; i != r.index; {
		//	d, _ := r.data.Load(guid)
		//
		//	if !d.([]metrics.ContainerMetrics)[i].Timestamp.Before(start) {
		//		id = i
		//		break
		//	}
		//	i = (i + 1) % MaxLen
		//}

		series := make([][2]interface{}, (r.index+MaxLen-id)%MaxLen)
		for i, k := id, 0; i != r.index; i, k = (i+1)%MaxLen, k+1 {
			d, _ := r.data.Load(guid)

			series[k][0] = d.([]metrics.ContainerMetrics)[i].Timestamp.Format("2006/01/02 15:04:05")
			series[k][1] = d.([]metrics.ContainerMetrics)[i].MemoryPercentage
		}
		return series, nil
	} else {

		if end.Before(start) {
			return nil, fmt.Errorf("time range error")
		}

		if _, ok := r.data.Load(guid); !ok {
			return nil, fmt.Errorf("guid is nil")
		}

		//if _, ok := r.data[guid]; !ok {
		//	//series := make([][2]interface{}, end.Unix()-start.Unix()+1)
		//	//for i, k := start, 0; !i.After(end); i, k = i.Add(time.Second), k+1 {
		//	//	series[k][0] = i.Format("2006/01/02 15:04:05")
		//	//	series[k][1] = 0.0
		//	//}
		//	//return series, fmt.Errorf("guid is nil")
		//	return nil, fmt.Errorf("guid is nil")
		//}

		is, _ := r.dataIndex[guid]
		id := ((is - 300) +MaxLen) % MaxLen
		z := metrics.ContainerMetrics{}
		d, _ := r.data.Load(guid)
		if is < 300 && d.([]metrics.ContainerMetrics)[MaxLen-1].Timestamp.Equal(z.Timestamp) {
			id = 0
		}

		//id := 0
		//is, _ := r.dataIndex[guid]
		//for i := is + 1; i != is; {
		//	dt, _ := r.data.Load(guid)
		//	if !dt.([]metrics.ContainerMetrics)[i].Timestamp.Before(start) {
		//		id = i
		//		break
		//	}
		//	i = (i + 1) % MaxLen
		//}

		series := make([][2]interface{}, (is+MaxLen-id)%MaxLen)
		for i, k := id, 0; i != is; i, k = (i+1)%MaxLen, k+1 {
			dt, _ := r.data.Load(guid)
			series[k][0] = dt.([]metrics.ContainerMetrics)[i].Timestamp.Format("2006/01/02 15:04:05")
			series[k][1] = dt.([]metrics.ContainerMetrics)[i].MemoryPercentage
		}
		return series, nil
	}
}

func (r *SupbMetrics) GetNetRxWithTimeRange(guid string, start, end time.Time) ([][2]interface{}, error) {
	if r.metricsDebug {
		if end.Before(start) {
			return nil, fmt.Errorf("time range error")
		}

		if _, ok := r.data.Load(guid); !ok {
			series := make([][2]interface{}, end.Unix()-start.Unix()+1)
			for i, k := start, 0; !i.After(end); i, k = i.Add(time.Second), k+1 {
				series[k][0] = i.Format("2006/01/02 15:04:05")
				series[k][1] = 0.0
			}
			return series, nil
		}

		is, _ := r.dataIndex[guid]
		id := ((is - 300) +MaxLen) % MaxLen



		//if _, ok := r.data[guid]; !ok {
		//	series := make([][2]interface{}, end.Unix()-start.Unix()+1)
		//	for i, k := start, 0; !i.After(end); i, k = i.Add(time.Second), k+1 {
		//		series[k][0] = i.Format("2006/01/02 15:04:05")
		//		series[k][1] = 0.0
		//	}
		//	return series, nil
		//}

		//id := 0
		//for i := r.index + 1; i != r.index; {
		//	d, _ := r.data.Load(guid)
		//
		//	if !d.([]metrics.ContainerMetrics)[i].Timestamp.Before(start) {
		//		id = i
		//		break
		//	}
		//	i = (i + 1) % MaxLen
		//}

		series := make([][2]interface{}, (r.index+MaxLen-id)%MaxLen)
		for i, k := id, 0; i != r.index; i, k = (i+1)%MaxLen, k+1 {
			d, _ := r.data.Load(guid)

			series[k][0] = d.([]metrics.ContainerMetrics)[i].Timestamp.Format("2006/01/02 15:04:05")
			series[k][1] = d.([]metrics.ContainerMetrics)[i].NetworkRx
		}
		return series, nil
	} else {

		if end.Before(start) {
			return nil, fmt.Errorf("time range error")
		}

		if _, ok := r.data.Load(guid); !ok {
			return nil, fmt.Errorf("guid is nil")
		}

		//if _, ok := r.data[guid]; !ok {
		//	//series := make([][2]interface{}, end.Unix()-start.Unix()+1)
		//	//for i, k := start, 0; !i.After(end); i, k = i.Add(time.Second), k+1 {
		//	//	series[k][0] = i.Format("2006/01/02 15:04:05")
		//	//	series[k][1] = 0.0
		//	//}
		//	//return series, fmt.Errorf("guid is nil")
		//	return nil, fmt.Errorf("guid is nil")
		//}
		is, _ := r.dataIndex[guid]
		id := ((is - 300) +MaxLen) % MaxLen
		z := metrics.ContainerMetrics{}
		d, _ := r.data.Load(guid)
		if is < 300 && d.([]metrics.ContainerMetrics)[MaxLen-1].Timestamp.Equal(z.Timestamp) {
			id = 0
		}

		//
		//id := 0
		//is, _ := r.dataIndex[guid]
		//for i := is+ 1; i != is; {
		//	dt, _ := r.data.Load(guid)
		//	if !dt.([]metrics.ContainerMetrics)[i].Timestamp.Before(start) {
		//		id = i
		//		break
		//	}
		//	i = (i + 1) % MaxLen
		//}

		series := make([][2]interface{}, (is+MaxLen-id)%MaxLen)
		for i, k := id, 0; i != is; i, k = (i+1)%MaxLen, k+1 {
			dt, _ := r.data.Load(guid)
			series[k][0] = dt.([]metrics.ContainerMetrics)[i].Timestamp.Format("2006/01/02 15:04:05")
			series[k][1] = dt.([]metrics.ContainerMetrics)[i].NetworkRx
		}
		return series, nil
	}
}

func (r *SupbMetrics) GetNetTxWithTimeRange(guid string, start, end time.Time) ([][2]interface{}, error) {
	if r.metricsDebug {
		if end.Before(start) {
			return nil, fmt.Errorf("time range error")
		}

		if _, ok := r.data.Load(guid); !ok {
			series := make([][2]interface{}, end.Unix()-start.Unix()+1)
			for i, k := start, 0; !i.After(end); i, k = i.Add(time.Second), k+1 {
				series[k][0] = i.Format("2006/01/02 15:04:05")
				series[k][1] = 0.0
			}
			return series, nil
		}

		//if _, ok := r.data[guid]; !ok {
		//	series := make([][2]interface{}, end.Unix()-start.Unix()+1)
		//	for i, k := start, 0; !i.After(end); i, k = i.Add(time.Second), k+1 {
		//		series[k][0] = i.Format("2006/01/02 15:04:05")
		//		series[k][1] = 0.0
		//	}
		//	return series, nil
		//}

		is, _ := r.dataIndex[guid]
		id := ((is - 300) +MaxLen) % MaxLen
		z := metrics.ContainerMetrics{}
		d, _ := r.data.Load(guid)
		if is < 300 && d.([]metrics.ContainerMetrics)[MaxLen-1].Timestamp.Equal(z.Timestamp) {
			id = 0
		}

		//id := 0
		//for i := r.index + 1; i != r.index; {
		//	d, _ := r.data.Load(guid)
		//
		//	if !d.([]metrics.ContainerMetrics)[i].Timestamp.Before(start) {
		//		id = i
		//		break
		//	}
		//	i = (i + 1) % MaxLen
		//}

		series := make([][2]interface{}, (r.index+MaxLen-id)%MaxLen)
		for i, k := id, 0; i != r.index; i, k = (i+1)%MaxLen, k+1 {
			d, _ := r.data.Load(guid)

			series[k][0] = d.([]metrics.ContainerMetrics)[i].Timestamp.Format("2006/01/02 15:04:05")
			series[k][1] = d.([]metrics.ContainerMetrics)[i].NetworkTx
		}
		return series, nil
	} else {

		if end.Before(start) {
			return nil, fmt.Errorf("time range error")
		}

		if _, ok := r.data.Load(guid); !ok {
			return nil, fmt.Errorf("guid is nil")
		}

		//if _, ok := r.data[guid]; !ok {
		//	//series := make([][2]interface{}, end.Unix()-start.Unix()+1)
		//	//for i, k := start, 0; !i.After(end); i, k = i.Add(time.Second), k+1 {
		//	//	series[k][0] = i.Format("2006/01/02 15:04:05")
		//	//	series[k][1] = 0.0
		//	//}
		//	//return series, fmt.Errorf("guid is nil")
		//	return nil, fmt.Errorf("guid is nil")
		//}

		is, _ := r.dataIndex[guid]
		id := ((is - 300) +MaxLen) % MaxLen
		z := metrics.ContainerMetrics{}
		d, _ := r.data.Load(guid)
		if is < 300 && d.([]metrics.ContainerMetrics)[MaxLen-1].Timestamp.Equal(z.Timestamp) {
			id = 0
		}

		//id := 0
		//is, _ := r.dataIndex[guid]
		//for i := is + 1; i != is; {
		//	dt, _ := r.data.Load(guid)
		//	if !dt.([]metrics.ContainerMetrics)[i].Timestamp.Before(start) {
		//		id = i
		//		break
		//	}
		//	i = (i + 1) % MaxLen
		//}

		series := make([][2]interface{}, (is+MaxLen-id)%MaxLen)
		for i, k := id, 0; i != is; i, k = (i+1)%MaxLen, k+1 {
			dt, _ := r.data.Load(guid)
			series[k][0] = dt.([]metrics.ContainerMetrics)[i].Timestamp.Format("2006/01/02 15:04:05")
			series[k][1] = dt.([]metrics.ContainerMetrics)[i].NetworkTx
		}
		return series, nil
	}
}

func (r *SupbMetrics) GetMetricsNow(guid string, td time.Duration) (*metrics.ContainerMetrics, error) {
	if r.metricsDebug {

		if _, ok := r.data.Load(guid); !ok {
			return &metrics.ContainerMetrics{
				CpuTotal:         0,
				CPUPercentage:    0,
				MemoryTotal:      0,
				MemoryPercentage: 0,
				NetworkTx:        0,
				NetworkRx:        0,
				Timestamp:        time.Now(),
			}, nil
		}

		//if _, ok := r.data[guid]; !ok {
		//	return &metrics.ContainerMetrics{
		//		CpuTotal:         0,
		//		CPUPercentage:    0,
		//		MemoryTotal:      0,
		//		MemoryPercentage: 0,
		//		NetworkTx:        0,
		//		NetworkRx:        0,
		//		Timestamp:        time.Now(),
		//	},nil
		//}
		i := (r.index - 1 + MaxLen) % MaxLen

		d, _ := r.data.Load(guid)
		return d.([]metrics.ContainerMetrics)[i].DeepCopy(), nil
	} else {
		dt, ok := r.data.Load(guid)
		if !ok {
			return nil, fmt.Errorf("guid is nil")
		}

		//if _, ok := r.data[guid]; !ok {
		//	return nil, fmt.Errorf("guid is nil")
		//}
		is, _ := r.dataIndex[guid]

		r.logger.Infof("guid %v, inde %v", guid, is)
		i := (is- 1 + MaxLen) % MaxLen
		return dt.([]metrics.ContainerMetrics)[i].DeepCopy(), nil
	}
}

//func (r *SupbMetrics) GetMetricsFromTimeToNow(guid string, td time.Duration) ([]*metrics.ContainerMetrics, error) {
//	if r.metricsDebug {
//
//
//		if _, ok := r.data[guid]; !ok {
//
//			r.logger.Infof("can't find guid%v",guid)
//
//			gp := int(td.Seconds())
//			now := time.Now()
//			ms := make([]*metrics.ContainerMetrics, gp)
//			for i := 0; i < gp; i++ {
//				ms[i] = &metrics.ContainerMetrics{
//					CpuTotal:         0,
//					CPUPercentage:    0,
//					MemoryTotal:      0,
//					MemoryPercentage: 0,
//					NetworkTx:        0,
//					NetworkRx:        0,
//					Timestamp:        now.Add(time.Duration(i-gp) * time.Second),
//				}
//			}
//			return ms, nil
//		}
//
//		now := time.Now()
//		before := now.Add(-td)
//		gp := int(td.Seconds())
//
//		if gp >= MaxLen {
//			gp = MaxLen - 1
//		}
//		tail := r.index
//		head := (tail + MaxLen - gp) % MaxLen
//
//		//r.logger.Infof("%v %v",r.data[guid][head].Timestamp,before)
//
//		if r.data[guid][head].Timestamp.Before(before) {
//			head = (head + 1) % MaxLen
//		}
//
//		ct := (tail + MaxLen - head) % MaxLen
//		if tail == head && gp > 1 {
//			ct = MaxLen - 1
//		}
//		ms := make([]*metrics.ContainerMetrics, ct)
//
//		//r.logger.Infof("%v %v %v %v %v",now,before,gp,head,tail)
//
//		for i, k := head, 0; i != tail; k++ {
//			ms[k] = r.data[guid][i].DeepCopy()
//			i = (i + 1) % MaxLen
//		}
//		return ms, nil
//
//	}else {
//		if _, ok := r.data[guid]; !ok {
//
//			r.logger.Infof("can't find guid%v",guid)
//
//			gp := int(td.Seconds())
//			now := time.Now()
//			ms := make([]*metrics.ContainerMetrics, gp)
//			for i := 0; i < gp; i++ {
//				ms[i] = &metrics.ContainerMetrics{
//					CpuTotal:         0,
//					CPUPercentage:    0,
//					MemoryTotal:      0,
//					MemoryPercentage: 0,
//					NetworkTx:        0,
//					NetworkRx:        0,
//					Timestamp:        now.Add(time.Duration(i-gp) * time.Second),
//				}
//			}
//			return ms, nil
//		}
//
//		now := time.Now()
//		before := now.Add(-td)
//		gp := int(td.Seconds())
//
//		if gp >= MaxLen {
//			gp = MaxLen - 1
//		}
//		tail := r.dataIndex[guid]
//		head := (tail + MaxLen - gp) % MaxLen
//
//		r.logger.Infof("head %v tail %v ",head,tail )
//		//r.logger.Infof("%v %v",r.data[guid][head].Timestamp,before)
//
//		if r.data[guid][head].Timestamp.Before(before) {
//			head = (head + 1) % MaxLen
//		}
//
//		ct := (tail + MaxLen - head) % MaxLen
//		if tail == head && gp > 1 {
//			ct = MaxLen - 1
//		}
//
//		r.logger.Infof("ct %v",ct)
//
//		ms := make([]*metrics.ContainerMetrics, ct)
//
//		//r.logger.Infof("%v %v %v %v %v",now,before,gp,head,tail)
//
//		for i, k := head, 0; i != tail; k++ {
//			ms[k] = r.data[guid][i].DeepCopy()
//			i = (i + 1) % MaxLen
//		}
//		return ms, nil
//	}
//}

func (r *SupbMetrics) productData() {
	now := time.Now()
	for i := 0; i < MaxLen; i++ {
		baseCPU := (1+math.Sin(math.Pi*2*float64(i)/MaxLen))*20 + 40
		baseMemory := (1+math.Cos(math.Pi*2*float64(i)/MaxLen))*20 + 40
		baseRx := rand.NormFloat64()*50 + 500
		baseTx := rand.NormFloat64()*50 + 500

		rd := rand.NormFloat64() * 5

		//for guid := range r.data {
		//	r.data[guid][i].CPUPercentage = baseCPU + rd
		//	r.data[guid][i].MemoryPercentage = baseMemory + rd
		//	r.data[guid][i].CpuTotal = 4000
		//	r.data[guid][i].MemoryTotal = 8 * 1024 * 1024 * 1024
		//	r.data[guid][i].NetworkRx = baseRx
		//	r.data[guid][i].NetworkTx = baseTx
		//	r.data[guid][i].Timestamp = now.Add(time.Duration(i-MaxLen) * time.Second)
		//}

		r.data.Range(func(key, value interface{}) bool {
			value.([]metrics.ContainerMetrics)[r.index].CPUPercentage = baseCPU + rd
			value.([]metrics.ContainerMetrics)[r.index].MemoryPercentage = baseMemory + rd
			value.([]metrics.ContainerMetrics)[r.index].CpuTotal = 4000
			value.([]metrics.ContainerMetrics)[r.index].MemoryTotal = 8 * 1024 * 1024 * 1024
			value.([]metrics.ContainerMetrics)[r.index].NetworkRx = baseRx
			value.([]metrics.ContainerMetrics)[r.index].NetworkTx = baseTx
			value.([]metrics.ContainerMetrics)[r.index].Timestamp = now.Add(time.Duration(i-MaxLen) * time.Second)
			return true
		})
	}

	r.index = 0
	tk := time.NewTicker(time.Second)
	for {
		select {
		case <-r.ctx.Done():
			return
		case <-tk.C:
			baseCPU := (1+math.Sin(math.Pi*2*float64(r.index)/MaxLen))*20 + 40
			baseMemory := (1+math.Cos(math.Pi*2*float64(r.index)/MaxLen))*20 + 40
			baseRx := rand.NormFloat64()*50 + 500
			baseTx := rand.NormFloat64()*50 + 500

			rd, nowT := rand.NormFloat64()*5, time.Now()
			r.lock.Lock()

			r.data.Range(func(key, value interface{}) bool {
				value.([]metrics.ContainerMetrics)[r.index].CPUPercentage = baseCPU + rd
				value.([]metrics.ContainerMetrics)[r.index].MemoryPercentage = baseMemory + rd
				value.([]metrics.ContainerMetrics)[r.index].CpuTotal = 4000
				value.([]metrics.ContainerMetrics)[r.index].MemoryTotal = 8 * 1024 * 1024 * 1024
				value.([]metrics.ContainerMetrics)[r.index].NetworkRx = baseRx
				value.([]metrics.ContainerMetrics)[r.index].NetworkTx = baseTx
				value.([]metrics.ContainerMetrics)[r.index].Timestamp = nowT
				return true
			})

			//for guid := range r.data {
			//	r.data[guid][r.index].CPUPercentage = baseCPU + rd
			//	r.data[guid][r.index].MemoryPercentage = baseMemory + rd
			//	r.data[guid][r.index].CpuTotal = 4000
			//	r.data[guid][r.index].MemoryTotal = 8 * 1024 * 1024 * 1024
			//	r.data[guid][r.index].NetworkRx = baseRx
			//	r.data[guid][r.index].NetworkTx = baseTx
			//	r.data[guid][r.index].Timestamp = nowT
			//}
			r.index = (r.index + 1) % MaxLen
			r.lock.Unlock()
		}
	}
}

func (r *SupbMetrics) getMetricsInfo() error {
	err := r.rpc.RPCRegister(define.RPCFunctionNameOfManagerStrategyCollectorObjectMetrics, func(args ...interface{}) (interface{}, error) {
		if len(args) < 2 {
			return "", fmt.Errorf("need two args, guid and data")
		}

		r.logger.Infof("receive metrics %v", args[0].(string))

		guid := args[0].(string)
		info := &metrics.CollectorMetrics{}
		json.Unmarshal([]byte(args[1].(string)), info)
		containerMetrics := metrics.ContainerMetrics{
			Timestamp:        info.Timestamp,
			NetworkTx:        info.NetworkTx,
			NetworkRx:        info.NetworkRx,
			MemoryPercentage: info.MemoryPercentage,
			MemoryTotal:      info.MemoryLimit,
			CPUPercentage:    info.CPUPercentage,
			CpuTotal:         4000, //这个实际统计信息中没有包含，后续补上
		}

		r.lock.Lock()

		dt, _ := r.data.Load(guid)
		index, _ := r.dataIndex[guid]
		dt.([]metrics.ContainerMetrics)[index] = containerMetrics

		r.dataIndex[guid]=(index+1)%MaxLen
		r.data.Store(guid, dt)

		//if _, ok := r.data[guid]; !ok {
		//	r.data[guid] = make([]metrics.ContainerMetrics, MaxLen)
		//	for i := 0; i < MaxLen; i++ {
		//		r.data[guid][i] = containerMetrics
		//	}
		//	r.dataIndex[guid] = 0
		//} else {
		//	index := r.dataIndex[guid]
		//	r.data[guid][index] = containerMetrics
		//	r.dataIndex[guid] = (r.dataIndex[guid] + 1) % MaxLen
		//}
		r.lock.Unlock()
		return "RECEIVE", nil
	})
	if err != nil {
		return err
	}
	return nil
}
