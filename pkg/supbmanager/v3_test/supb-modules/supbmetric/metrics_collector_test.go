package supbmetric

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"testing"
)

func TestCollector(t *testing.T) {
	a := sync.Map{}

	a.Store("1",1)

	v,_ := a.Load("1")
	fmt.Printf("%v",v)

	//a.Store("1",2)
	v,ok := a.LoadOrStore("2",2)
	fmt.Printf("%v %v",v,ok)

	return
	ctx, cancel := context.WithCancel(context.Background())
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	lc := NewSupbMetrics(ctx,"test")

	go lc.productData()



}
