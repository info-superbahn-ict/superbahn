package supbmetric

import nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"

type option func(resources *SupbMetrics)

func WithRPC(rpc nervous.Controller) option {
	return func(spg *SupbMetrics) {
		spg.rpc = rpc
	}
}