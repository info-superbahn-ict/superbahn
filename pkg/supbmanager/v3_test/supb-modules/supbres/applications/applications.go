package applications

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"
	uuid "github.com/satori/go.uuid"
	"sync"
)

const (


	StaticApplicationRecorderKeyFormat  = "/repository/superbahnManager/application/static/%v"
	DynamicApplicationRecorderKeyFormat = "/repository/superbahnManager/application/dynamic/%v"
)

type SupbApplications struct {
	lock *sync.RWMutex

	staticApplications  map[types.SupbApplicationKey]*model.SupbApplicationBaseInfo
	dynamicApplications map[types.SupbApplicationKey]*model.SupbApplicationBaseInfo

	//staticAssume  map[types.SupbApplicationKey]*putAssume
	//dynamicAssume map[types.SupbApplicationKey]*putAssume


	//staticLinksAssume  map[types.SupbApplicationKey]*linkAssume
	//dynamicLinksAssume map[types.SupbApplicationKey]*linkAssume
	//
	//staticUnLinksAssume  map[types.SupbApplicationKey]*unLinkAssume
	//dynamicUnLinksAssume map[types.SupbApplicationKey]*unLinkAssume

	removeAssume map[types.SupbApplicationKey]*removeAssume

	recorder supbapis.Recorder
}

func NewSupbAppManager() *SupbApplications {
	r := &SupbApplications{
		lock:                &sync.RWMutex{},
		staticApplications:  make(map[types.SupbApplicationKey]*model.SupbApplicationBaseInfo),
		dynamicApplications: make(map[types.SupbApplicationKey]*model.SupbApplicationBaseInfo),
		//staticAssume:        make(map[types.SupbApplicationKey]*putAssume),
		//dynamicAssume:       make(map[types.SupbApplicationKey]*putAssume),

		//staticLinksAssume:  make(map[types.SupbApplicationKey]*linkAssume),
		//dynamicLinksAssume: make(map[types.SupbApplicationKey]*linkAssume),
		//
		//staticUnLinksAssume:  make(map[types.SupbApplicationKey]*unLinkAssume),
		//dynamicUnLinksAssume: make(map[types.SupbApplicationKey]*unLinkAssume),

		removeAssume: make(map[types.SupbApplicationKey]*removeAssume),
	}

	return r
}

func (r *SupbApplications) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *SupbApplications) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *SupbApplications) ListApplications(appTypes string) ([]*model.SupbApplicationBaseInfo, error) {
	r.lock.RLock()
	defer r.lock.RUnlock()

	switch appTypes {
	case types.StaticApplication:
		list, index := make([]*model.SupbApplicationBaseInfo, len(r.staticApplications)), 0
		for _, v := range r.staticApplications {
			list[index], index = v.DeepCopy(), index+1
		}
		return list, nil
	case types.DynamicApplication:
		list, index := make([]*model.SupbApplicationBaseInfo, len(r.dynamicApplications)), 0
		for _, v := range r.dynamicApplications {
			list[index], index = v.DeepCopy(), index+1
		}
		return list, nil
	default:
		return nil, fmt.Errorf("unknown type")
	}
}

func (r *SupbApplications) GetApplication(appTypes string, applicationKey types.SupbApplicationKey) (*model.SupbApplicationBaseInfo, error) {
	r.lock.RLock()
	defer r.lock.RUnlock()

	switch appTypes {
	case types.StaticApplication:
		sapp, ok := r.staticApplications[applicationKey]
		if !ok {
			return nil, fmt.Errorf("unknown static applicationKey")
		}
		return sapp.DeepCopy(), nil
	case types.DynamicApplication:
		dapp, ok := r.dynamicApplications[applicationKey]
		if !ok {
			return nil, fmt.Errorf("unknown dynamic applicationKey")
		}
		return dapp.DeepCopy(), nil
	default:
		return nil, fmt.Errorf("unknown type")
	}
}

func (r *SupbApplications) PutApplicationsAssume(appTypes string, baseInfo *model.SupbApplicationBaseInfo) (*putAssume, error) {
	r.lock.Lock()
	defer r.lock.Unlock()

	if r.recorder == nil {
		return nil, fmt.Errorf("recorder is nil")
	}

	assume := &putAssume{
		supb:  r,
		types: appTypes,
		key:   baseInfo.ApplicationKey,
		app:   baseInfo,
		done:  false,
	}


	switch appTypes {
	case types.StaticApplication:
		if err := model.ValidateStaticApplication(baseInfo); err != nil {
			return nil, fmt.Errorf("validate ")
		}

		if app,ok := r.staticApplications[baseInfo.ApplicationKey]; ok {
			assume.app.ApplicationVersion = app.ApplicationVersion + 1
		}else {
			assume.app.ApplicationVersion = 0
		}

		//if _, ok := r.staticAssume[baseInfo.ApplicationKey]; ok {
		//	return nil, fmt.Errorf("the app are writing")
		//}

		bts, err := json.Marshal(baseInfo)
		if err != nil {
			return nil, fmt.Errorf("unmarshall %v", err)
		}

		assume.writeAssume, err = r.recorder.WriteAssume(fmt.Sprintf(StaticApplicationRecorderKeyFormat, baseInfo.ApplicationKey), bts)
		if err != nil {
			return nil, fmt.Errorf("recorder write %v", err)
		}

		//r.staticAssume[baseInfo.ApplicationKey] = assume
	case types.DynamicApplication:
		if err := model.ValidateDynamicApplication(baseInfo); err != nil {
			return nil, fmt.Errorf("validate ")
		}

		if app,ok := r.dynamicApplications[baseInfo.ApplicationKey]; ok {
			assume.app.ApplicationVersion = app.ApplicationVersion + 1
		}else {
			assume.app.ApplicationVersion = 0
		}

		//if _, ok := r.dynamicAssume[baseInfo.ApplicationKey]; ok {
		//	return nil, fmt.Errorf("the app are writing")
		//}

		bts, err := json.Marshal(baseInfo)
		if err != nil {
			return nil, fmt.Errorf("unmarshall %v", err)
		}

		assume.writeAssume, err = r.recorder.WriteAssume(fmt.Sprintf(DynamicApplicationRecorderKeyFormat, baseInfo.ApplicationKey), bts)
		if err != nil {
			return nil, fmt.Errorf("recorder write %v", err)
		}

		//r.dynamicAssume[baseInfo.ApplicationKey] = assume
	default:
		return nil, fmt.Errorf("unknown type")
	}
	return assume, nil
}

func (r *SupbApplications) RemoveApplicationAssume(AppTypes string, applicationKey types.SupbApplicationKey) (*removeAssume, error) {
	r.lock.Lock()
	defer r.lock.Unlock()

	if r.recorder == nil {
		return nil, fmt.Errorf("recorder is nil")
	}

	assume := &removeAssume{
		supb:  r,
		types: AppTypes,
		key:   applicationKey,
		done:  false,
	}

	var (
		ok bool
		err error
	)
	switch AppTypes {
	case types.StaticApplication:
		assume.app,ok = r.staticApplications[applicationKey]
		if !ok {
			return nil,fmt.Errorf("the app are not exit")
		}

		assume.deleteAssume, err = r.recorder.DeleteAssume(fmt.Sprintf(StaticApplicationRecorderKeyFormat, applicationKey))
		if err != nil {
			return nil, fmt.Errorf("recorder write %v", err)
		}

		r.removeAssume[applicationKey] = assume
		//delete(r.staticsStrategies, applicationKey)
	case types.DynamicApplication:
		assume.app,ok = r.dynamicApplications[applicationKey]
		if !ok {
			return nil,fmt.Errorf("the app are not exit")
		}

		assume.deleteAssume, err = r.recorder.DeleteAssume(fmt.Sprintf(DynamicApplicationRecorderKeyFormat, applicationKey))
		if err != nil {
			return nil, fmt.Errorf("recorder write %v", err)
		}

		r.removeAssume[applicationKey] = assume
		//delete(r.dynamicStrategies, applicationKey)
	default:
		return nil, fmt.Errorf("unknown type")
	}
	return assume, nil
}

func (r *SupbApplications) LinkControllerAssume(appTypes string, appKey types.SupbApplicationKey, strategyKey types.SupbStrategyKey, controller *model.ApplicationController) (*linkAssume,error) {
	r.lock.Lock()
	defer r.lock.Unlock()

	if r.recorder == nil {
		return nil,fmt.Errorf("recorder is nil")
	}

	assume := &linkAssume{
		supb:  r,
		types: appTypes,
		key:   appKey,
		done:  false,
	}

	switch appTypes {
	case types.StaticApplication:
		sapp,ok := r.staticApplications[appKey]
		if !ok {
			return nil,fmt.Errorf("application is not exist")
		}

		//if _ , ok = r.staticAssume[appKey]; ok {
		//	return nil,fmt.Errorf("app are linking")
		//}

		sappCopy := sapp.DeepCopy()
		if controller.StrategyTypes == types.StaticStrategy {
			if _,ok = sapp.ApplicationStaticStrategies[strategyKey]; ok {
				return nil,fmt.Errorf("link static %v conflict", strategyKey)
			}
			sappCopy.ApplicationStaticStrategies[strategyKey] = controller
		}else {
			if _,ok = sapp.ApplicationDynamicStrategies[strategyKey]; ok {
				return nil,fmt.Errorf("link dynamic %v conflict", strategyKey)
			}
			sappCopy.ApplicationDynamicStrategies[strategyKey] = controller
		}
		sappCopy.ApplicationVersion++
		assume.app = sappCopy

		bts,err := json.Marshal(sappCopy)
		if err != nil {
			return nil,fmt.Errorf("marshall %v",err)
		}

		assume.writeAssume,err = r.recorder.WriteAssume(fmt.Sprintf(StaticApplicationRecorderKeyFormat, appKey),bts)
		if err != nil {
			return nil,fmt.Errorf("recorder write %v",err)
		}

		//r.staticLinksAssume[appKey] = assume
	case types.DynamicApplication:
		dapp,ok := r.dynamicApplications[appKey]
		if !ok {
			return nil,fmt.Errorf("application is not exist")
		}

		//if _ , ok = r.dynamicAssume[appKey]; ok {
		//	return nil,fmt.Errorf("app are linking")
		//}

		//if _,ok = dapp.ApplicationDynamicStrategies[strategyKey]; ok {
		//	return nil,fmt.Errorf("application strategyKey %v has controller", strategyKey)
		//}


		dappCopy := dapp.DeepCopy()
		if controller.StrategyTypes == types.StaticStrategy {
			if _,ok = dapp.ApplicationStaticStrategies[strategyKey]; ok {
				return nil,fmt.Errorf("link static %v conflict", strategyKey)
			}
			dappCopy.ApplicationStaticStrategies[strategyKey] = controller
		} else {
			if _,ok = dapp.ApplicationDynamicStrategies[strategyKey]; ok {
				return nil,fmt.Errorf("link dynamic %v conflict", strategyKey)
			}
			dappCopy.ApplicationDynamicStrategies[strategyKey] = controller
		}
		dappCopy.ApplicationVersion++
		assume.app = dappCopy

		bts,err := json.Marshal(dappCopy)
		if err != nil {
			return nil,fmt.Errorf("marshall %v",err)
		}

		assume.writeAssume, err = r.recorder.WriteAssume(fmt.Sprintf(DynamicApplicationRecorderKeyFormat, appKey),bts)
		if err != nil {
			return nil,fmt.Errorf("recorder write %v",err)
		}

		//r.dynamicLinksAssume[appKey] = assume
	default:
		return nil,fmt.Errorf("unknown type")
	}
	return assume,nil
}


func (r *SupbApplications) UnLinkControllerAssume(appTypes string, appKey types.SupbApplicationKey,strategyTypes string, strategyKey types.SupbStrategyKey) (*unLinkAssume,error) {
	r.lock.Lock()
	defer r.lock.Unlock()

	if r.recorder == nil {
		return nil,fmt.Errorf("recorder is nil")
	}

	assume := &unLinkAssume{
		supb:  r,
		types: appTypes,
		key:   appKey,
		done:  false,
	}


	switch appTypes {
	case types.StaticApplication:
		sapp, ok := r.staticApplications[appKey]
		if !ok {
			return nil,fmt.Errorf("application is not exist")
		}

		appCopy := sapp.DeepCopy()
		appCopy.ApplicationVersion++
		switch strategyTypes {
		case types.StaticStrategy:
			if _, ok = appCopy.ApplicationStaticStrategies[strategyKey]; !ok {
				return nil,fmt.Errorf("don't have static strategy")
			}
			delete(appCopy.ApplicationStaticStrategies, strategyKey)
		case types.DynamicStrategy:
			if _, ok = appCopy.ApplicationDynamicStrategies[strategyKey]; !ok {
				return nil,fmt.Errorf("don't have dynamic strategy")
			}
			delete(appCopy.ApplicationDynamicStrategies, strategyKey)
		}
		assume.app = appCopy


		bts, err := json.Marshal(appCopy)
		if err != nil {
			return nil,fmt.Errorf("marshall %v", err)
		}

		assume.writeAssume,err = r.recorder.WriteAssume(fmt.Sprintf(StaticApplicationRecorderKeyFormat, appKey), bts)
		if err != nil {
			return nil,fmt.Errorf("recorder write %v", err)
		}

		//r.staticUnLinksAssume[appKey] = assume
	case types.DynamicApplication:
		dapp, ok := r.dynamicApplications[appKey]
		if !ok {
			return nil,fmt.Errorf("application is not exist")
		}


		//
		//if _, ok = dapp.ApplicationDynamicStrategies[strategyKey]; ok {
		//	return nil,fmt.Errorf("application strategyKey %v don't has controller", strategyKey)
		//}
		//if _ , ok = r.dynamicUnLinksAssume[appKey]; ok {
		//	return nil,fmt.Errorf("app are unlinking")
		//}


		appCopy := dapp.DeepCopy()
		appCopy.ApplicationVersion++
		switch strategyTypes {
		case types.StaticStrategy:
			if _, ok = appCopy.ApplicationStaticStrategies[strategyKey]; !ok {
				return nil,fmt.Errorf("don't have static strategy")
			}
			delete(appCopy.ApplicationStaticStrategies, strategyKey)
		case types.DynamicStrategy:
			if _, ok = appCopy.ApplicationDynamicStrategies[strategyKey]; !ok {
				return nil,fmt.Errorf("don't have dynamic strategy")
			}
			delete(appCopy.ApplicationDynamicStrategies, strategyKey)
		}
		assume.app = appCopy


		bts, err := json.Marshal(appCopy)
		if err != nil {
			return nil,fmt.Errorf("marshall %v", err)
		}

		assume.writeAssume,err = r.recorder.WriteAssume(fmt.Sprintf(DynamicApplicationRecorderKeyFormat, appKey), bts)
		if err != nil {
			return nil,fmt.Errorf("recorder write %v", err)
		}

		//r.dynamicUnLinksAssume[appKey] = assume
	default:
		return nil,fmt.Errorf("unknown type")
	}
	return assume,nil
}

func (r *SupbApplications) DeriveKey(parentKey types.SupbApplicationKey) types.SupbApplicationKey {
	r.lock.RLock()
	defer r.lock.RUnlock()

	for tryTime:=3; tryTime >0 ; tryTime-- {
		key := types.SupbApplicationKey(uuid.NewV4().String())
		if _,ok := r.staticApplications[key];!ok {
			if _,ok := r.dynamicApplications[key];!ok {
				return key
			}
		}
	}
	return ""
}