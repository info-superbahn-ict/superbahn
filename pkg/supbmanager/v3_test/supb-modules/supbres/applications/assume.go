package applications

import (
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"
)

type putAssume struct {
	supb  *SupbApplications
	types string
	key   types.SupbApplicationKey
	app   *model.SupbApplicationBaseInfo

	writeAssume supbapis.Assume

	done bool
}

func (r *putAssume) Done() uint64 {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return 0
	}

	switch r.types {
	case types.StaticApplication:
		//delete(r.supb.staticAssume, r.key)

		if app,ok := r.supb.staticApplications[r.key];ok{
			if app.ApplicationVersion >= r.app.ApplicationVersion{
				return 0
			}
		}

		r.supb.staticApplications[r.key]=r.app
	case types.DynamicApplication:
		//delete(r.supb.dynamicAssume, r.key)
		if app,ok := r.supb.dynamicApplications[r.key];ok{
			if app.ApplicationVersion >= r.app.ApplicationVersion{
				return 0
			}
		}
		r.supb.dynamicApplications[r.key]=r.app
	}
	r.writeAssume.Done()
	r.done = true

	return r.app.ApplicationVersion
}

func (r *putAssume) Cancel() {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return
	}

	//switch r.types {
	//case types.StaticApplication:
	//	delete(r.supb.staticAssume, r.key)
	//case types.DynamicApplication:
	//	delete(r.supb.dynamicAssume, r.key)
	//}
	r.writeAssume.Cancel()
	r.done = true
}

type linkAssume struct {
	supb *SupbApplications
	types string

	key types.SupbApplicationKey

	app *model.SupbApplicationBaseInfo

	writeAssume supbapis.Assume

	done bool
}

func (r *linkAssume) Done() uint64 {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return 0
	}

	switch r.types {
	case types.StaticApplication:
		//delete(r.supb.staticLinksAssume, r.key)
		app,ok := r.supb.staticApplications[r.key]

		// 被删除了
		if !ok {
			return 0
		}

		// 被更改了
		if app.ApplicationVersion >= r.app.ApplicationVersion{
			return 0
		}
		r.supb.staticApplications[r.key]=r.app
	case types.DynamicApplication:
		//delete(r.supb.dynamicLinksAssume, r.key)
		app,ok := r.supb.dynamicApplications[r.key]

		// 被删除了
		if !ok {
			return 0
		}

		// 被更改了
		if app.ApplicationVersion >= r.app.ApplicationVersion{
			return 0
		}
		r.supb.dynamicApplications[r.key]=r.app
	}
	r.writeAssume.Done()
	r.done = true

	return r.app.ApplicationVersion
}

func (r *linkAssume) Cancel() {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return
	}

	//switch r.types {
	//case types.StaticApplication:
	//	delete(r.supb.staticLinksAssume, r.key)
	//case types.DynamicApplication:
	//	delete(r.supb.dynamicLinksAssume, r.key)
	//}
	r.writeAssume.Cancel()
	r.done = true
}


type unLinkAssume struct {
	supb *SupbApplications
	types string

	key types.SupbApplicationKey

	app *model.SupbApplicationBaseInfo

	writeAssume supbapis.Assume

	done bool
}

func (r *unLinkAssume) Done() uint64 {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return 0
	}

	switch r.types {
	case types.StaticApplication:
		//delete(r.supb.staticUnLinksAssume, r.key)
		app,ok := r.supb.staticApplications[r.key]

		// 被删除了
		if !ok {
			return 0
		}

		// 被更改了
		if app.ApplicationVersion >= r.app.ApplicationVersion{
			return 0
		}
		r.supb.staticApplications[r.key]=r.app
	case types.DynamicApplication:
		//delete(r.supb.dynamicUnLinksAssume, r.key)

		app,ok := r.supb.dynamicApplications[r.key]

		// 被删除了
		if !ok {
			return 0
		}

		// 被更改了
		if app.ApplicationVersion >= r.app.ApplicationVersion{
			return 0
		}
		r.supb.dynamicApplications[r.key]=r.app
	}
	r.writeAssume.Done()
	r.done = true

	return r.app.ApplicationVersion
}

func (r *unLinkAssume) Cancel() {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return
	}

	//switch r.types {
	//case types.StaticApplication:
	//	delete(r.supb.staticUnLinksAssume, r.key)
	//case types.DynamicApplication:
	//	delete(r.supb.dynamicUnLinksAssume, r.key)
	//}
	r.writeAssume.Cancel()
	r.done = true
}


type removeAssume struct {
	supb  *SupbApplications
	types string
	key   types.SupbApplicationKey
	app   *model.SupbApplicationBaseInfo

	deleteAssume supbapis.Assume

	done bool
}

func (r *removeAssume) Done() uint64 {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return 0
	}

	delete(r.supb.removeAssume, r.key)
	switch r.types {
	case types.StaticApplication:
		app,ok := r.supb.staticApplications[r.key]

		// 被其他线程删除了
		if !ok {
			return 0
		}

		// 被写了
		if app.ApplicationVersion > r.app.ApplicationVersion {
			return 0
		}

		// 没有改变
		delete(r.supb.staticApplications, r.key)
	case types.DynamicApplication:
		app,ok := r.supb.dynamicApplications[r.key]

		// 被其他应用删除了
		if !ok {
			return 0
		}

		// 被写了
		if app.ApplicationVersion > r.app.ApplicationVersion {
			return 0
		}

		// 没有改变
		delete(r.supb.dynamicApplications, r.key)
	}
	r.deleteAssume.Done()
	r.done = true

	return r.app.ApplicationVersion
}

func (r *removeAssume) Cancel() {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return
	}
	delete(r.supb.removeAssume, r.key)

	r.deleteAssume.Cancel()
	r.done = true
}