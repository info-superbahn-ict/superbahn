package model

import (
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/compose"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
)

const (
	BestEffort = "BestEffort"
	Burstable  = "Burstable"
	Guaranteed = "Guaranteed"
)

type ApplicationController struct {
	StrategyLevel  types.SupbStrategyLevel `json:"strategy_level"`
	StrategyKey    types.SupbStrategyKey   `json:"strategy_key"`
	StrategyStatus string                  `json:"strategy_status"`
	StrategyTypes  string                  `json:"strategy_types"`
}

type SupbApplicationBaseInfo struct {
	ApplicationVersion   uint64                   `json:"application_version"`
	ApplicationKey       types.SupbApplicationKey `json:"application_key"`
	ApplicationKeyParent types.SupbApplicationKey `json:"application_key_parent"`

	ApplicationName             string   `json:"application_name"`
	ApplicationShortDescription string   `json:"application_description_short"`
	ApplicationDescription      string   `json:"application_description_long"`
	ApplicationLevel            string   `json:"application_level"`
	ApplicationStatus           string   `json:"application_status"`
	ApplicationType             string   `json:"application_kind"`
	ApplicationParent           []string `json:"application_parent"`
	ApplicationChild            []string `json:"application_child"`

	//ApplicationGuid                map[string]string                                `json:"application_guid"`
	//ApplicationContainerId         string                                           `json:"application_container_id"`
	ApplicationContainers          []*compose.Containers                            `json:"application_containers"`
	ApplicationContainerNamePrefix string                                           `json:"application_container_name_prefix"`
	ApplicationStaticStrategies    map[types.SupbStrategyKey]*ApplicationController `json:"application_strategies_static"`
	ApplicationDynamicStrategies   map[types.SupbStrategyKey]*ApplicationController `json:"application_strategies_dynamic"`

	ApplicationCompose string      `json:"application_compose_yaml"`
	ApplicationTags    []types.Tag `json:"application_tags"`
}

func (r *SupbApplicationBaseInfo) DeepCopy() *SupbApplicationBaseInfo {
	c := *r
	//c.ApplicationCompose = r.ApplicationCompose.DeepCopy()
	copy(c.ApplicationParent, r.ApplicationParent)
	copy(c.ApplicationChild, r.ApplicationChild)
	copy(c.ApplicationTags, r.ApplicationTags)

	st := make(map[types.SupbStrategyKey]*ApplicationController, len(r.ApplicationStaticStrategies))
	for k, v := range r.ApplicationStaticStrategies {
		st[k] = v
	}

	dt := make(map[types.SupbStrategyKey]*ApplicationController, len(r.ApplicationDynamicStrategies))
	for k, v := range r.ApplicationDynamicStrategies {
		dt[k] = v
	}

	ct := make([]*compose.Containers, len(r.ApplicationContainers))
	for k, v := range r.ApplicationContainers {
		ct[k] = v.DeepCopy()
	}

	c.ApplicationStaticStrategies = st
	c.ApplicationDynamicStrategies = dt
	c.ApplicationContainers = ct
	return &c
}

func (r *SupbApplicationBaseInfo) Devices() map[string]interface{} {
	cts := make([]*compose.Containers,len(r.ApplicationContainers))

	for k,v := range r.ApplicationContainers {
		cts[k]=v.DeepCopy()
	}

	return map[string]interface{}{
		"containers":cts,
	}
}

func (r *SupbApplicationBaseInfo) DevicesInfo(guid string) interface{} {

	for _ ,v := range r.ApplicationContainers {
		if v.Guid == guid {
			return v.DeepCopy()
		}
	}

	return nil
}

func MarshallCompose(app *SupbApplicationBaseInfo) error {
	cts,err := compose.FromCompose(app.ApplicationCompose)
	if err !=nil {
		return err
	}

	app.ApplicationContainers = cts
	return nil
}

func ValidateStaticApplication(app *SupbApplicationBaseInfo) error {

	if app.ApplicationKey == "" {
		return fmt.Errorf("invalid strategy key")
	}

	flag := true
	for _, v := range Levels() {
		if v == app.ApplicationLevel {
			flag = false
		}
	}
	if flag {
		return fmt.Errorf("invalid strategy level")
	}

	return nil
}

func ValidateDynamicApplication(app *SupbApplicationBaseInfo) error {

	return nil
}

func Levels() []string {
	return []string{
		BestEffort, Burstable, Guaranteed,
	}
}
