package applications

import (
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"
)

type option func(*SupbApplications)


func WithRecorder(recorder supbapis.Recorder) option {
	return func(supbres *SupbApplications) {
		supbres.recorder = recorder
	}
}

type Recorder interface {
	Write(key string, value []byte) error
	Read(key string) ([]byte,error)
}

