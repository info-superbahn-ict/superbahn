package assumes

import "gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"

type Assumes struct {
	assumes []supbapis.Assume
}

func NewAssumes(opts...option) *Assumes {
	a := &Assumes{}

	for _,apply := range opts {
		apply(a)
	}
	return a
}

func (r *Assumes) AddAssume(assume supbapis.Assume)  {
	r.assumes = append(r.assumes, assume)
}

func (r *Assumes) Done()  {
	for _,as := range r.assumes {
		as.Done()
	}
}

func (r *Assumes) Cancel()  {
	for _,as := range r.assumes {
		as.Cancel()
	}
}