package assumes

import "gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"

type option func(assumes *Assumes)

func WithLen(ct int) option {
	return func(ass *Assumes) {
		ass.assumes = make([]supbapis.Assume,ct)
	}
}
