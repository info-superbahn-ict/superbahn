package compose

type Compose struct {
	Containers map[string]*Containers `json:"containers"`
}

func (receiver *Compose) DeepCopy() *Compose {
	return nil
}

type RunningBaseInfo struct {
	Guid string
	CID  string
	ContainerName string
	Host string
	Port []string
}

func (receiver *RunningBaseInfo) DeepCopy() *RunningBaseInfo {
	return nil
}
