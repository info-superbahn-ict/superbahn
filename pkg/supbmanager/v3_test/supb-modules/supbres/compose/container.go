package compose

import (
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
	"gopkg.in/yaml.v2"
)

type Containers struct {
	AppKey string `json:"appkey" yaml:"appkey"`
	Parent []string `json:"parent" yaml:"parent"`
	Child  []string `json:"child" yaml:"child"`

	Name   string `json:"name" yaml:"name"`
	Image  string `json:"image" yaml:"image"`
	Cid    string `json:"id" yaml:"id"`
	Guid   string `json:"guid" yaml:"guid"`
	Status string `json:"status" yaml:"status"`

	Runtime string `json:"runtime" yaml:"runtime"`
	Workdir string `json:"workdir" yaml:"workdir"`
	PortMap map[string]string `json:"ports" yaml:"ports"`
	EnvsKey map[string]string `json:"envs" yaml:"envs"`
	Cmd     []string          `json:"cmd" yaml:"cmd"`

	Tags    []spongeregister.Tag `json:"tags"`

	ImagePullPolicy string `json:"imagePullPolicy" yaml:"imagePullPolicy"`
	Lifecycle 		map[string]string `json:"lifecycle" yaml:"lifecycle"`
	LivenessProbe	map[string]string `json:"livenessProbe" yaml:"livenessProbe"`
	ReadinessProbe	map[string]string `json:"readinessProbe" yaml:"readinessProbe"`
	Reasources		map[string]string `json:"reasources" yaml:"reasources"`
	SecurityContext	map[string]string `json:"securityContext" yaml:"securityContext"`
	TerminationMessagePath	 []string `json:"terminationMessagePath" yaml:"terminationMessagePath"`
	TerminationMessagePolicy []string `json:"terminationMessagePolicy" yaml:"terminationMessagePolicy"`
	VolumeMounts	map[string]string `json:"volumeMounts" yaml:"volumeMounts"`
}

func (r *Containers) DeepCopy() *Containers {
	c := *r
	copy(c.Cmd, r.Cmd)
	copy(c.Tags,r.Tags)
	copy(c.Parent,r.Parent)
	copy(c.Child,r.Child)

	c.PortMap = make(map[string]string, len(r.PortMap))
	for k,v := range r.PortMap{
		c.PortMap[k]=v
	}

	c.EnvsKey = make(map[string]string, len(r.EnvsKey))
	for k,v := range r.EnvsKey{
		c.EnvsKey[k]=v
	}

	return &c
}

func FromCompose(yamlStr string) ([]*Containers, error) {
	var compose Compose

	if err := yaml.Unmarshal([]byte(yamlStr), &compose); err != nil {
		return nil, err
	}

	cts := make([]*Containers, len(compose.Containers))
	i := 0
	for _, v := range compose.Containers {
		cts[i] = v.DeepCopy()
		i++
	}

	return cts, nil
}
