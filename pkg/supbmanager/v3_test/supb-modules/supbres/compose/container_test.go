package compose

import (
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
	"gopkg.in/yaml.v2"
	"testing"
)

func TestContainer(t *testing.T) {
	c1 := &Containers{
		AppKey: "trace_jaeger_client",
		Parent: []string{"trace_jaeger_collector"},
		Child:  []string{},

		Name:   "",
		Image:  "jaeger-env",
		Cid:    "",
		Guid:   "",
		Status: "NotReady",

		PortMap: map[string]string{
			"80": "8080",
		},
		EnvsKey: map[string]string{
			"SUPB_GUID": "123456",
		},
		Cmd: []string{
			"/bin/sh", "boot.sh", "hello",
		},
		Tags: []spongeregister.Tag{
			{
				Key: "HOST",
				Type: "string",
				Value: "CPU",
			},
			{
				Key: "HOST",
				Type: "string",
				Value: "CPU",
			},
		},
	}

	c2 := &Containers{
		AppKey: "trace_jaeger_collector",
		Parent: []string{"trace_jaeger_collector"},
		Child:  []string{},

		Name:   "",
		Image:  "jaeger-collector",
		Cid:    "",
		Guid:   "",
		Status: "NotReady",

		PortMap: map[string]string{
			"80": "8080",
		},
		EnvsKey: map[string]string{
			"SUPB_GUID": "123456",
		},
		Cmd: []string{
			"/bin/sh", "boot.sh", "hello",
		},
	}

	cp := Compose{
		Containers: map[string]*Containers{
			"collector": c2,
			"agent": c1,
		},
	}

	bts,err := yaml.Marshal(cp)
	if err != nil {
		t.Errorf("%v",err)
		return
	}

	fmt.Printf("YAML:%s",bts)
}
