package supbres

import (
	m2 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/assumes"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/compose"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/status"
	m1 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
)

func (r *SupbResources) examples() {
	ass := assumes.NewAssumes()

	//s1Key := types.SupbStrategyKey(uuid.NewV4().String())
	s1 := &m1.SupbStrategyBaseInfo{
		StrategyShortDescription: "正弦实验调控策略，保障正弦应用CPU波形正常",
		StrategyDescription:      "正弦应用是CPU资源使用呈现正弦波形的自定义测试应用，将正弦应用和干扰应用进行绑核运行，正弦应用波形会严重失真，然后用调节应用进行调节，让其在干扰下也恢复波形",
		StrategyName:             "正弦调控策略",
		StrategyType:             types.StaticStrategy,
		StrategyLevel:            m1.LevelWrite1,
		StrategyStatus:           status.StrategyNotReady,
		StrategyKey:              "supb.strategy.test.control.sine",
		StrategyKeyParent:        "supb.strategy.test.control.sine",

		StrategyCompose: strategy1,

		/*
			仅运行中的策略有的guid和container name and container id
		*/
		StrategyContainers: []*compose.Containers{},
		StrategyContainerNamePrefix: "SUPB_STG",
		StrategySlaves:              make(map[types.SupbApplicationKey]*m1.StrategySlave),

		StrategyTags: []types.Tag{
			{
				Key: "tag1",
				Type: "string",
				Value: "I am strategy",
			},
		},
	}
	_ = m1.MarshallCompose(s1)
	a1, err := r.strategyMgr.PutStrategiesAssume(types.StaticStrategy, s1)
	if err != nil {
		r.logger.Errorf("insert example %v", err)
	}
	ass.AddAssume(a1)

	//s1Key := types.SupbStrategyKey(uuid.NewV4().String())
	s2 := &m1.SupbStrategyBaseInfo{
		StrategyShortDescription: "小车调控策略，小车控制应用的正常控制",
		StrategyDescription:      "小车控制应用是小车后端控制应用",
		StrategyName:             "小车调控策略",
		StrategyType:             types.StaticStrategy,
		StrategyLevel:            m1.LevelWrite1,
		StrategyStatus:           status.StrategyNotReady,
		StrategyKey:              "supb.strategy.test.control.car",
		StrategyKeyParent:        "supb.strategy.test.control.car",

		StrategyCompose: strategy2,

		/*
			仅运行中的策略有的guid和container name and container id
		*/
		StrategyContainers: []*compose.Containers{},
		StrategyContainerNamePrefix: "SUPB_STG",
		StrategySlaves:              make(map[types.SupbApplicationKey]*m1.StrategySlave),

		StrategyTags: []types.Tag{
			{
				Key: "tag1",
				Type: "string",
				Value: "I am strategy",
			},
			{
				Key: "URL",
				Type: "string",
				Value: "http://39.101.140.145:10046/",
			},
		},
	}
	_ = m1.MarshallCompose(s2)
	a2, err := r.strategyMgr.PutStrategiesAssume(types.StaticStrategy, s2)
	if err != nil {
		r.logger.Errorf("insert example %v", err)
	}
	ass.AddAssume(a2)

	s3 := &m1.SupbStrategyBaseInfo{
		StrategyShortDescription: "自动驾驶小车调控策略，小车控制应用的正常控制",
		StrategyDescription:      "自动驾驶小车控制应用是小车后端控制应用",
		StrategyName:             "自动驾驶小车调控策略",
		StrategyType:             types.StaticStrategy,
		StrategyLevel:            m1.LevelWrite1,
		StrategyStatus:           status.StrategyNotReady,
		StrategyKey:              "supb.strategy.test.control.hydramini",
		StrategyKeyParent:        "supb.strategy.test.control.hydramini",

		StrategyCompose: strategy3,

		/*
			仅运行中的策略有的guid和container name and container id
		*/
		StrategyContainers: []*compose.Containers{},
		StrategyContainerNamePrefix: "SUPB_STG",
		StrategySlaves:              make(map[types.SupbApplicationKey]*m1.StrategySlave),

		StrategyTags: []types.Tag{
			{
				Key: "tag1",
				Type: "string",
				Value: "I am strategy",
			},
			{
				Key: "URL",
				Type: "string",
				Value: "http://39.101.140.145:10046/",
			},
		},
	}
	_ = m1.MarshallCompose(s3)
	a3, err := r.strategyMgr.PutStrategiesAssume(types.StaticStrategy, s3)
	if err != nil {
		r.logger.Errorf("insert example %v", err)
	}
	ass.AddAssume(a3)
	//
	//s3Key := types.SupbStrategyKey(uuid.NewV4().String())
	//s3 := &m1.SupbStrategyBaseInfo{
	//	StrategyShortDescription: "应用的trace收集并存储到elk",
	//	StrategyDescription:      "",
	//	StrategyName:             "trace监控",
	//	StrategyType:             types.StaticStrategy,
	//	StrategyLevel:            m1.LevelRead,
	//	StrategyStatus:           status.StrategyNotReady,
	//	StrategyKey:              s3Key,
	//	StrategyKeyParent:        s3Key,
	//
	//	StrategyCompose: yaml,
	//
	//	/*
	//		仅运行中的策略有的guid和container name and container id
	//	*/
	//	StrategyContainers: []*compose.Containers{},
	//	StrategyContainerNamePrefix: "SUPB_traces",
	//	StrategySlaves:              make(map[types.SupbApplicationKey]*m1.StrategySlave),
	//
	//	StrategyTags: []types.Tag{
	//		{
	//			Key: "tag1",
	//			Type: "string",
	//			Value: "I am strategy",
	//		},
	//	},
	//}
	//_ = m1.MarshallCompose(s3)
	//a3, err := r.strategyMgr.PutStrategiesAssume(types.StaticStrategy, s3)
	//if err != nil {
	//	r.logger.Errorf("insert example %v", err)
	//}
	//ass.AddAssume(a3)
	//
	//s4Key := types.SupbStrategyKey(uuid.NewV4().String())
	//s4 := &m1.SupbStrategyBaseInfo{
	//	StrategyShortDescription: "应用的简单调控策略",
	//	StrategyDescription:      "",
	//	StrategyName:             "简单调控",
	//	StrategyType:             types.StaticStrategy,
	//	StrategyLevel:            m1.LevelWrite1,
	//	StrategyStatus:           status.StrategyNotReady,
	//	StrategyKey:              s4Key,
	//	StrategyKeyParent:        s4Key,
	//
	//	StrategyCompose: yaml,
	//
	//	/*
	//		仅运行中的策略有的guid和container name and container id
	//	*/
	//	StrategyContainers: []*compose.Containers{},
	//	StrategyContainerNamePrefix: "SUPB_test",
	//	StrategySlaves:              make(map[types.SupbApplicationKey]*m1.StrategySlave),
	//
	//	StrategyTags: []types.Tag{
	//		{
	//			Key: "tag1",
	//			Type: "string",
	//			Value: "I am strategy",
	//		},
	//	},
	//}
	//_ = m1.MarshallCompose(s4)
	//a4, err := r.strategyMgr.PutStrategiesAssume(types.StaticStrategy, s4)
	//if err != nil {
	//	r.logger.Errorf("insert example %v", err)
	//}
	//ass.AddAssume(a4)

	//s5Key := types.SupbStrategyKey(uuid.NewV4().String())
	//s5 := &m1.SupbStrategyBaseInfo{
	//	StrategyShortDescription: "填充数据的例子，无作用，不能运行",
	//	StrategyDescription:      "",
	//	StrategyName:             "简单例子",
	//	StrategyType:             types.StaticStrategy,
	//	StrategyLevel:            m1.LevelRead,
	//	StrategyStatus:           status.StrategyNotReady,
	//	StrategyKey:              s5Key,
	//	StrategyKeyParent:        s5Key,
	//
	//	StrategyCompose: "",
	//
	//	/*
	//		仅运行中的策略有的guid和container name and container id
	//	*/
	//	StrategyContainers: []*compose.Containers{},
	//	StrategyContainerNamePrefix: "SUPB_test",
	//	StrategySlaves:              make(map[types.SupbApplicationKey]*m1.StrategySlave),
	//}
	//a5, err := r.strategyMgr.PutStrategiesAssume(types.StaticStrategy, s5)
	//if err != nil {
	//	r.logger.Errorf("insert example %v", err)
	//}
	//ass.AddAssume(a5)

	//s6Key := types.SupbStrategyKey(uuid.NewV4().String())
	//s6 := &m1.SupbStrategyBaseInfo{
	//	StrategyShortDescription: "填充数据的例子，无作用，不能运行",
	//	StrategyDescription:      "",
	//	StrategyName:             "简单例子",
	//	StrategyType:             types.StaticStrategy,
	//	StrategyLevel:            m1.LevelRead,
	//	StrategyStatus:           status.StrategyNotReady,
	//	StrategyKey:              s6Key,
	//	StrategyKeyParent:        s6Key,
	//
	//	StrategyCompose: "",
	//
	//	/*
	//		仅运行中的策略有的guid和container name and container id
	//	*/
	//	StrategyContainers: []*compose.Containers{},
	//	StrategyContainerNamePrefix: "SUPB_test",
	//	StrategySlaves:              make(map[types.SupbApplicationKey]*m1.StrategySlave),
	//}
	//a6, err := r.strategyMgr.PutStrategiesAssume(types.StaticStrategy, s6)
	//if err != nil {
	//	r.logger.Errorf("insert example %v", err)
	//}
	//ass.AddAssume(a6)

	//p1Key := types.SupbApplicationKey(uuid.NewV4().String())
	app1 := &m2.SupbApplicationBaseInfo{
		ApplicationKey: "supb.application.test.sine",
		ApplicationKeyParent:"supb.application.test.sine",

		ApplicationName:             "自定义测试应用",
		ApplicationShortDescription: "能够产生单核正弦CPU利用率波形的测试应用",
		ApplicationDescription:      "使用单核心，运行时通过设置周期来产生正弦波形，如果出现其他应用争抢CPU核心，正弦波将会变形，用来测试干扰和干扰调节程序",
		ApplicationLevel:            m2.Guaranteed,
		ApplicationStatus:           status.ApplicationNotReady,
		ApplicationType:             types.StaticApplication,
		ApplicationParent:           []string{},
		ApplicationChild:            []string{},

		ApplicationContainers: []*compose.Containers{},
		ApplicationContainerNamePrefix: "SUPB_APP",
		ApplicationStaticStrategies:    make(map[types.SupbStrategyKey]*m2.ApplicationController),

		ApplicationCompose: app1,
		ApplicationTags: []types.Tag{
			{
				Key: "tag1",
				Type: "string",
				Value: "hello world",
			},
		},
	}
	_ = m2.MarshallCompose(app1)
	p1, err := r.appMgr.PutApplicationsAssume(types.StaticApplication, app1)
	if err != nil {
		r.logger.Errorf("insert example %v", err)
	}
	ass.AddAssume(p1)

	app2 := &m2.SupbApplicationBaseInfo{
		ApplicationKey: "supb.application.test.interfere",
		ApplicationKeyParent:"supb.application.test.interfere",

		ApplicationName:             "自定义干扰应用",
		ApplicationShortDescription: "能够吃满单核CPU资源的干扰应用",
		ApplicationDescription:      "单线程，能够吃满单核CPU的应用，用来争抢其他应用的资源，作为干扰源，需要同目标应用进行绑核运行",
		ApplicationLevel:            m2.BestEffort,
		ApplicationStatus:           status.ApplicationNotReady,
		ApplicationType:             types.StaticApplication,
		ApplicationParent:           []string{},
		ApplicationChild:            []string{},

		ApplicationContainers: []*compose.Containers{},
		ApplicationContainerNamePrefix: "SUPB_APP",
		ApplicationStaticStrategies:    make(map[types.SupbStrategyKey]*m2.ApplicationController),

		ApplicationCompose: app2,
		ApplicationTags: []types.Tag{
			{
				Key: "tag1",
				Type: "string",
				Value: "hello world",
			},
		},
	}
	_ = m2.MarshallCompose(app2)
	p2, err := r.appMgr.PutApplicationsAssume(types.StaticApplication, app2)
	if err != nil {
		r.logger.Errorf("insert example %v", err)
	}
	ass.AddAssume(p2)

	app3 := &m2.SupbApplicationBaseInfo{
		ApplicationKey: "supb.application.test.car",
		ApplicationKeyParent:"supb.application.test.car",

		ApplicationName:             "小车应用",
		ApplicationShortDescription: "搭载图像识别的小车，测试测调的实验场景",
		ApplicationDescription:      "小车应用由三个部分组成，第一小车应用，第二图像处理应用，第三前端控制，通过干扰应用影响小车控制响应的延迟来制造干扰场景，通过调节策略来控制小车资源使用，来保证控制服务质量",
		ApplicationLevel:            m2.Guaranteed,
		ApplicationStatus:           status.ApplicationNotReady,
		ApplicationType:             types.StaticApplication,
		ApplicationParent:           []string{},
		ApplicationChild:            []string{},

		ApplicationContainers: []*compose.Containers{},
		ApplicationContainerNamePrefix: "SUPB_APP",
		ApplicationStaticStrategies:    make(map[types.SupbStrategyKey]*m2.ApplicationController),

		ApplicationCompose: app3_t,
		ApplicationTags: []types.Tag{
			{
				Key: "tag1",
				Type: "string",
				Value: "hello world",
			},
		},
	}
	_ = m2.MarshallCompose(app3)
	p3, err := r.appMgr.PutApplicationsAssume(types.StaticApplication, app3)
	if err != nil {
		r.logger.Errorf("insert example %v", err)
	}
	ass.AddAssume(p3)

	app4 := &m2.SupbApplicationBaseInfo{
		ApplicationKey: "supb.application.test.car.interfere",
		ApplicationKeyParent:"supb.application.test.car.interfere",

		ApplicationName:             "小车应用干扰",
		ApplicationShortDescription: "干扰小车的容器，吃CPU资源",
		ApplicationDescription:      "小车分三个容器，需要干扰的是运行在小车上的容器，需要进行绑核运行",
		ApplicationLevel:            m2.BestEffort,
		ApplicationStatus:           status.ApplicationNotReady,
		ApplicationType:             types.StaticApplication,
		ApplicationParent:           []string{},
		ApplicationChild:            []string{},

		ApplicationContainers: []*compose.Containers{},
		ApplicationContainerNamePrefix: "SUPB_APP",
		ApplicationStaticStrategies:    make(map[types.SupbStrategyKey]*m2.ApplicationController),

		ApplicationCompose: app4,
		ApplicationTags: []types.Tag{
			{
				Key: "tag1",
				Type: "string",
				Value: "hello world",
			},
		},
	}
	_ = m2.MarshallCompose(app4)
	p4, err := r.appMgr.PutApplicationsAssume(types.StaticApplication, app4)
	if err != nil {
		r.logger.Errorf("insert example %v", err)
	}
	ass.AddAssume(p4)

	app5 := &m2.SupbApplicationBaseInfo{
		ApplicationKey: "supb.application.test.hydramini",
		ApplicationKeyParent:"supb.application.test.hydramini",

		ApplicationName:             "自动驾驶小车应用",
		ApplicationShortDescription: "利用神经网络的和模拟器组成的自动驾驶算法测试应用",
		ApplicationDescription:      "应用分为两个容器，agent 和 server，agent提供转发功能，server提供inference",
		ApplicationLevel:            m2.Guaranteed,
		ApplicationStatus:           status.ApplicationNotReady,
		ApplicationType:             types.StaticApplication,
		ApplicationParent:           []string{},
		ApplicationChild:            []string{},

		ApplicationContainers: []*compose.Containers{},
		ApplicationContainerNamePrefix: "SUPB_APP",
		ApplicationStaticStrategies:    make(map[types.SupbStrategyKey]*m2.ApplicationController),

		ApplicationCompose: app_5,
		ApplicationTags: []types.Tag{
			{
				Key: "tag1",
				Type: "string",
				Value: "hello world",
			},
		},
	}
	_ = m2.MarshallCompose(app5)
	p5, err := r.appMgr.PutApplicationsAssume(types.StaticApplication, app5)
	if err != nil {
		r.logger.Errorf("insert example %v", err)
	}
	ass.AddAssume(p5)

	app6 := &m2.SupbApplicationBaseInfo{
		ApplicationKey: "supb.application.test.hydramini.interfere",
		ApplicationKeyParent:"supb.application.test.hydramini.interfere",

		ApplicationName:             "自动驾驶干扰应用",
		ApplicationShortDescription: "干扰自动驾驶的神经网络计算服务，让计算出现延迟，影响实时响应",
		ApplicationDescription:      "自动驾驶模拟应用包含三个部分，模拟器、agent和server，server根据图片推理路径",
		ApplicationLevel:            m2.Guaranteed,
		ApplicationStatus:           status.ApplicationNotReady,
		ApplicationType:             types.StaticApplication,
		ApplicationParent:           []string{},
		ApplicationChild:            []string{},

		ApplicationContainers: []*compose.Containers{},
		ApplicationContainerNamePrefix: "SUPB_APP",
		ApplicationStaticStrategies:    make(map[types.SupbStrategyKey]*m2.ApplicationController),

		ApplicationCompose: app_6,
		ApplicationTags: []types.Tag{
			{
				Key: "tag1",
				Type: "string",
				Value: "hello world",
			},
		},
	}
	_ = m2.MarshallCompose(app6)
	p6, err := r.appMgr.PutApplicationsAssume(types.StaticApplication, app6)
	if err != nil {
		r.logger.Errorf("insert example %v", err)
	}
	ass.AddAssume(p6)

	app7 := &m2.SupbApplicationBaseInfo{
		ApplicationKey: "supb.application.gitlab.runner",
		ApplicationKeyParent:"supb.application.gitlab.runner",

		ApplicationName:             "SERVE平台gitlab-runner应用",
		ApplicationShortDescription: "SERVE平台gitlab-runner",
		ApplicationDescription:      "SERVE平台gitlab-runner",
		ApplicationLevel:            m2.Guaranteed,
		ApplicationStatus:           status.ApplicationNotReady,
		ApplicationType:             types.StaticApplication,
		ApplicationParent:           []string{},
		ApplicationChild:            []string{},

		ApplicationContainers: []*compose.Containers{},
		ApplicationContainerNamePrefix: "SUPB_APP",
		ApplicationStaticStrategies:    make(map[types.SupbStrategyKey]*m2.ApplicationController),

		ApplicationCompose: app_7,
		ApplicationTags: []types.Tag{
			{
				Key: "tag1",
				Type: "string",
				Value: "hello world",
			},
		},
	}
	p7, err := r.appMgr.PutApplicationsAssume(types.StaticApplication, app7)
	if err != nil {
		r.logger.Errorf("insert example %v", err)
	}
	ass.AddAssume(p7)

	ass.Done()
}

const yaml = `
containers:
  agent:
    appkey: trace_jaeger_client
    parent: trace_jaeger_collector
    child: ""
    name: ""
    image: jaeger-env
    cid: ""
    guid: ""
    status: NotReady
    ports:
      "80": "8080"
    envs:
      SUPB_GUID: "123456"
    cmd:
    - /bin/sh
    - boot.sh
    - hello
  collector:
    appkey: trace_jaeger_collector
    parent: supb_agent
    child: trace_jaeger_client
    name: ""
    image: jaeger-collector
    cid: ""
    guid: ""
    status: NotReady
    ports:
      "80": "8080"
    envs:
      SUPB_GUID: "123456"
    cmd:
    - /bin/sh
    - boot.sh
    - hello
`

const app1 = `
containers:
  emu-service:
    appkey: "supb.application.test.sine.cpu"
    parent: []
    child: []
    name: ""
    image: 39.101.140.145:5000/cpu-ctrl-core-1
    cid: ""
    guid: ""
    status: NotReady
    ports:
    envs:
      SUPB_GUID: ""
      SUPB_T: "30"
      SUPB_I: "0.04"
    cmd:
    - /bin/sh
    - boot.sh
    tags:
    - key: HOST
      type: string
      value: CPU
    - key: HOST
      type: string
      value: GPU
    - key: cpu.set
      type: string
      value: "5"
`

const app2 = `
containers:
  cpu-interference-service:
    appkey: "supb.application.test.interfere.cpu"
    parent: []
    child: []
    name: ""
    image: 39.101.140.145:5000/interference-cpu-core-1
    cid: ""
    guid: ""
    status: NotReady
    ports:
    envs:
      SUPB_GUID: ""
    cmd:
    - /bin/sh
    - boot.sh
    tags:
    - key: HOST
      type: string
      value: CPU
    - key: HOST
      type: string
      value: GPU
    - key: cpu.set
      type: string
      value: "5"
`



const strategy1 = `
containers:
  cpu-sine-control-service:
    appkey: "supb.strategy.test.control.sine.cpu"
    parent: []
    child: []
    name: ""
    image: 39.101.140.145:5000/test.control.sine
    cid: ""
    guid: ""
    status: NotReady
    ports:
    envs:
      SUPB_GUID: ""
      ETCD_KEY: ""
      ETCD_URL: "10.16.0.180:10000"
    cmd:
    - /bin/sh
    - boot.sh
    tags:
    - key: HOST
      type: string
      value: CPU
    - key: HOST
      type: string
      value: GPU
`

const app3 = `
containers:
  darknet-service:
    appkey: "supb.application.test.car.darknet"
    parent: []
    child: 
    - supb.application.test.car.app
    name: ""
    image: sauronwu/superb_darknet_demo:latest
    cid: ""
    guid: ""
    status: NotReady
    runtime: nvidia
    workdir: /usr/src/darknet-superb-service
    ports:
      "11900-12000": "11900-12000" 
    envs:
      SUPB_GUID: ""
      SUPB_SERVICE_PORT: "11935"
      SUPB_MIN_PORT: "11900"
      SUPB_MAX_PORT: "12000"
    cmd:
    - /bin/bash
    - /usr/src/darknet-superb-service/boot.sh
    tags:
    - key: HOST
      type: string
      value: GPU
  car-service:
    appkey: "supb.application.test.car.app"
    parent: 
    - supb.application.test.car.darknet
    child: 
    - supb.application.test.car.frontend
    name: ""
    image: car-ctrl-service:latest
    cid: ""
    guid: ""
    status: NotReady
    ports:
      "10039": "8000"
    envs:
      SUPB_GUID: ""
      JAEGER_AGENT_PORT: "5775"
      JAEGER_AGENT_HOST: "10.16.0.180"
      DARK_NET_HOST: "10.118.0.35"
    cmd:
    - /bin/bash
    - /root/boot.sh
    tags:
    - key: HOST
      type: string
      value: CAR
    - key: cpu.set
      type: string
      value: "1"
  car-frontend:
    appkey: "supb.application.test.car.frontend"
    parent: 
    - supb.application.test.car.app
    child: []
    name: ""
    image: car-front-end:latest
    cid: ""
    guid: ""
    status: NotReady
    ports:
      "10041": "8080"
    envs:
      SUPB_GUID: ""
      JAEGER_AGENT_PORT: "5775"
      JAEGER_AGENT_HOST: "10.16.0.180"
      CAR_PATH: ""
    cmd: []
    tags:
    - key: HOST
      type: string
      value: CPU
`


const app3_t = `
containers:
  darknet-service:
    appkey: "supb.application.test.car.darknet"
    parent: []
    child: 
    - supb.application.test.car.app
    name: ""
    image: sauronwu/superb_darknet_demo:latest
    cid: ""
    guid: ""
    status: NotReady
    runtime: nvidia
    workdir: /usr/src/darknet-superb-service
    ports:
      "11900-12000": "11900-12000" 
    envs:
      SUPB_GUID: ""
      SUPB_SERVICE_PORT: "11935"
      SUPB_MIN_PORT: "11900"
      SUPB_MAX_PORT: "12000"
    cmd:
    - /bin/bash
    - /usr/src/darknet-superb-service/boot.sh
    tags:
    - key: HOST
      type: string
      value: GPU
  car-service:
    appkey: "supb.application.test.car.app"
    parent: 
    - supb.application.test.car.darknet
    child: 
    - supb.application.test.car.frontend
    name: ""
    image: car-ctrl-service
    cid: ""
    guid: ""
    status: NotReady
    ports:
      "10039": "8000"
    envs:
      SUPB_GUID: ""
      JAEGER_AGENT_PORT: "5775"
      JAEGER_AGENT_HOST: "10.16.0.180"
      DARK_NET_HOST: "10.118.0.35"
      FRAME_RATE: "15"
    cmd:
    - /bin/bash
    - /root/boot.sh
    tags:
    - key: HOST
      type: string
      value: CAR
    - key: cpu.set
      type: string
      value: "1"
  car-frontend:
    appkey: "supb.application.test.car.frontend"
    parent: 
    - supb.application.test.car.app
    child: []
    name: ""
    image: car-front-end:latest
    cid: ""
    guid: ""
    status: NotReady
    ports:
      "10041": "8080"
    envs:
      SUPB_GUID: ""
      CAR_PATH: "10.30.150.1:10039"
    cmd: []
    tags:
    - key: HOST
      type: string
      value: CPU
`

const app4 = `
containers:
  cpu-interference-service:
    appkey: "supb.application.interfere.cpu"
    parent: []
    child: []
    name: ""
    image: 39.101.140.145:5000/interference-cpu-core-1
    cid: ""
    guid: ""
    status: NotReady
    ports:
    envs:
      SUPB_GUID: ""
    cmd:
    - /bin/sh
    - /root/boot.sh
    tags:
    - key: HOST
      type: string
      value: CAR
    - key: cpu.set
      type: string
      value: "1"
    - key: cpu.ratio
      type: string
      value: "0.95"
`

const strategy2 = `
containers:
  cpu-sine-control-service:
    appkey: "supb.strategy.test.control.car.cpu"
    parent: []
    child: []
    name: ""
    image: 39.101.140.145:5000/test.control.car
    cid: ""
    guid: ""
    status: NotReady
    ports:
    envs:
      SUPB_GUID: ""
      ETCD_KEY: ""
      SLEEP: "10"
      ETCD_URL: "10.16.0.180:10000"
    cmd:
    - /bin/sh
    - /root/boot.sh
    tags:
    - key: HOST
      type: string
      value: CPU
    - key: HOST
      type: string
      value: GPU
`

const app_5 = `
containers:
  hydramini_agent:
    appkey: "supb.application.test.hydramini.agent"
    parent: []
    child: 
    - supb.application.test.hydramini.server
    name: ""
    image: sauronwu/superb_hydramini_demo:latest
    cid: ""
    guid: ""
    status: NotReady
    ports:
     "9090": "9090" 
    envs:
      SUPB_GUID: ""
      HOSTIP: "0.0.0.0"
      NNGHOST: "tcp://10.16.0.180:8899"
      JAEGER_AGENT_HOST: "10.16.0.180"
      JAEGER_AGENT_PORT: "5775"
    cmd:
    - /home/hydra-mini-demo/agent/run.sh
    tags:
    - key: HOST
      type: string
      value: CPU
    - key: cpu.set
      type: string
      value: "2"
  hydramini_server:
    appkey: "supb.application.test.hydramini.server"
    parent: 
    - supb.application.test.hydramini.agent
    child: [] 
    name: ""
    image: sauronwu/superb_hydramini_demo:latest
    cid: ""
    guid: ""
    status: NotReady
    ports:
      "8899": "8899"
    envs:
      SUPB_GUID: ""
      NNGHOST: "tcp://0.0.0.0:8899"
      MODEL: "/home/hydra-mini-demo/server/model.h5"
      PARENT_PORT: ""
      JAEGER_AGENT_HOST: "10.16.0.180"
      JAEGER_AGENT_PORT: "5775"
    cmd:
    - /home/hydra-mini-demo/server/run.sh
    tags:
    - key: HOST
      type: string
      value: CPU
    - key: cpu.set
      type: string
      value: "1"
`

const app_6 = `
containers:
  cpu-interference-service:
    appkey: "supb.application.test.hydramini.interfere.cpu"
    parent: []
    child: []
    name: ""
    image: 39.101.140.145:5000/interference-cpu-core-1
    cid: ""
    guid: ""
    status: NotReady
    ports:
    envs:
      SUPB_GUID: ""
    cmd:
    - /bin/sh
    - /root/boot.sh
    tags:
    - key: HOST
      type: string
      value: CPU
    - key: cpu.set
      type: string
      value: "1"
    - key: cpu.ratio
      type: string
      value: "0.90"
`

const strategy3 = `
containers:
  cpu-sine-control-service:
    appkey: "supb.strategy.test.control.hydramini.cpu"
    parent: []
    child: []
    name: ""
    image: 39.101.140.145:5000/test.control.simcar
    cid: ""
    guid: ""
    status: NotReady
    ports:
    envs:
      SUPB_GUID: ""
      ETCD_KEY: ""
      SLEEP: "1"
      ETCD_URL: "10.16.0.180:10000"
    cmd:
    - /bin/sh
    - /root/boot.sh
    tags:
    - key: HOST
      type: string
      value: CPU
    - key: HOST
      type: string
      value: GPU
`

const app_7 = `
apiVersion: v1   #版本号
kind: Pod 		 #Pod类型（指定资源角色、类型）
metadata:
  annotations:   #自定义注释列表
    checksum/configmap: b3673879a41d62ec260419fe7df405d13722d0b79a5bf8e86089a1e7dbc20e4a
    checksum/secrets: 560d8c5a61004e91d062d4965083dbc2a7e97f5401da93fb00ed57d738c287ec
    prometheus.io/port: "9252"   #对外暴露port,在需要监控的pod的deployment中指定
    prometheus.io/scrape: "true"
  generateName: gitlab-runner-gitlab-runner-7bccb598c7-
  labels:       #自定义标签名字
    app: gitlab-runner-gitlab-runner   #pod的应用名
    chart: gitlab-runner-0.37.2   # Helm 的应用打包格式，是helm的软件包，采用TAR格式，包含一组定义Kubernetes资源相关的yanl文件
    heritage: Helm   #Helm 是一个命令行下的客户端工具。主要用于 Kubernetes 应用程序 Chart 的创建、打包、发布以及创建和管理本地和远程的 Chart 仓库
    pod-template-hash: 7bccb598c7
    release: gitlab-runner   #使用 helm install 命令在 Kubernetes 集群中部署的 Chart 称为 Release
  name: gitlab-runner-gitlab-runner-7bccb598c7-gfghw  #pod的名称
  namespace: gitlab-runner   #pod的命名空间
  ownerReferences:   #监听事件 指定属主
  - apiVersion: apps/v1
    blockOwnerDeletion: true
    controller: true
    kind: ReplicaSet
    name: gitlab-runner-gitlab-runner-7bccb598c7
    uid: 25cc57fd-c170-4787-ad9c-ea8044d6ba3c
  resourceVersion: "797278"
  selfLink: /api/v1/namespaces/gitlab-runner/pods/gitlab-runner-gitlab-runner-7bccb598c7-gfghw
  uid: cf418f2f-04f4-4f5c-bc0f-e52b13777094
spec:   #pod中容器的详细定义
  containers:
    gitlab-runner-service:
	  appkey: "supb.application.gitlab.runner.gitlab-runner"
      parent: []
      child: [] 
      name: ""
      image: gitlab/gitlab-runner:alpine-v14.7.0   #容器的镜像名称
      cid: ""
      guid: ""
      status: NotReady
      ports:
        "8899": "8899"
      envs:   #容器运行前需设置的环境变量列表(K-V对，环境变量名-环境变量的值) （env）
      - name: CI_SERVER_URL
        value: https://gitlab.agileserve.org.cn:8001
      - name: CLONE_URL
      - name: RUNNER_EXECUTOR
        value: kubernetes
      - name: REGISTER_LOCKED
        value: "false"
      - name: RUNNER_TAG_LIST
        value: k8s-cd
      - name: KUBERNETES_PULL_POLICY
        value: if-not-present
      cmd: #容器启动的命令，如果不指定使用打包时使用的启动命令(command)
      - /usr/bin/dumb-init
      - --
      - /bin/bash
      - /configmaps/entrypoint
      #tags:
      #- key: HOST
        #type: string
        #value: CPU
      #- key: cpu.set
       # type: string
       # value: "1"
      imagePullPolicy: IfNotPresent   #优先使用本地镜像，否则下载镜像
      lifecycle:   #pod的生命周期
        preStop:
          exec:
            command:
            - /entrypoint
            - unregister
            - --all-runners
      livenessProbe:   #对Pod内个容器健康检查的设置，当探测无响应几次后将自动重启该容器，检查方法有exec、httpGet和tcpSocket，对一个容器只需设置其中一种方法即可
        exec:   #对Pod容器内检查方式设置为exec方式
          command:   #exec方式需要制定的命令或脚本
          - /bin/bash
          - /configmaps/check-live
        failureThreshold: 3   #探针的最大失败次数，如果达到了最大的失败次数，在存活性探针的情况，容器将重新启动
        initialDelaySeconds: 60   #容器启动完成后首次探测的时间，单位为秒
        periodSeconds: 10   #对容器监控检查的定期探测时间设置，单位秒，默认10秒一次
        successThreshold: 1   
        timeoutSeconds: 1   #对容器健康检查探测等待响应的超时时间，单位秒，默认1秒
      name: gitlab-runner-gitlab-runner   #容器名称
      ports:   #需要暴露的端口库号列表
      - containerPort: 9252   #容器需要监听的端口号
        name: metrics    #端口号名称
        protocol: TCP    #端口协议，支持TCP和UDP，默认TCP
      readinessProbe:
        exec:
          command:
          - /usr/bin/pgrep
          - gitlab.*runner
        failureThreshold: 3
        initialDelaySeconds: 10
        periodSeconds: 10
        successThreshold: 1
        timeoutSeconds: 1
      resources:   #资源限制和请求的设置
        limits:   #资源限制的设置
          cpu: 500m   #Cpu的限制，单位为core数，将用于docker run --cpu-shares参数
          memory: 1Gi   #内存限制，单位可以为Mib/Gib，将用于docker run --memory参数
        requests:   #资源请求的设置
          cpu: 500m   #Cpu请求，容器启动的初始可用数量
          memory: 1Gi   #内存请求，容器启动的初始可用数量
      securityContext:   #安全上下文，定义pod或container的权限和访问控制
        allowPrivilegeEscalation: false
      terminationMessagePath: /dev/termination-log   #容器的异常终止消息的路径，当容器退出时，可以通过容器的状态看到退出的信息
      terminationMessagePolicy: File  #退出信息会从文件中读取
      volumeMounts:   #挂载到容器内部的存储卷配置，可以通过 terminationMessagePolicy 来修改，将 terminationMessagePolicy 修改为：FallbackToLogsOnError，从日志中来读取
      - mountPath: /secrets   #存储卷在容器内mount的绝对路径，应少于512字符
        name: runner-secrets   #引用pod定义的共享存储卷的名称，需用volumes[]部分定义的的卷名
      - mountPath: /home/gitlab-runner/.gitlab-runner
        name: etc-gitlab-runner
      - mountPath: /configmaps
        name: configmaps
      - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
        name: gitlab-runner-gitlab-runner-token-rvkzm
        readOnly: true   #是否为只读模式
  dnsPolicy: ClusterFirst  #设置DNS的策略
  enableServiceLinks: true   #默认的配置，所有请求会优先在集群所在域查询，如果没有才会转发到上游DNS
  initContainers:   #pod节点中包含的容器初始化？
  - command:
    - sh
    - /configmaps/configure
    env:
    - name: CI_SERVER_URL
      value: https://gitlab.agileserve.org.cn:8001
    - name: CLONE_URL
    - name: RUNNER_EXECUTOR
      value: kubernetes
    - name: REGISTER_LOCKED
      value: "false"
    - name: RUNNER_TAG_LIST
      value: k8s-cd
    - name: KUBERNETES_PULL_POLICY
      value: if-not-present
    image: gitlab/gitlab-runner:alpine-v14.7.0
    imagePullPolicy: IfNotPresent
    name: configure
    resources:
      limits:
        cpu: 500m
        memory: 1Gi
      requests:
        cpu: 500m
        memory: 1Gi
    securityContext:
      allowPrivilegeEscalation: false
    terminationMessagePath: /dev/termination-log
    terminationMessagePolicy: File
    volumeMounts:
    - mountPath: /secrets
      name: runner-secrets
    - mountPath: /configmaps
      name: configmaps
      readOnly: true
    - mountPath: /init-secrets
      name: init-runner-secrets
      readOnly: true
    - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
      name: gitlab-runner-gitlab-runner-token-rvkzm
      readOnly: true
  nodeName: ubuntu-219
  priority: 0
  restartPolicy: Always
  schedulerName: default-scheduler
  securityContext:
    fsGroup: 65533
    runAsUser: 100
  serviceAccount: gitlab-runner-gitlab-runner
  serviceAccountName: gitlab-runner-gitlab-runner
  terminationGracePeriodSeconds: 3600
  tolerations:
  - effect: NoExecute
    key: node.kubernetes.io/not-ready
    operator: Exists
    tolerationSeconds: 300
  - effect: NoExecute
    key: node.kubernetes.io/unreachable
    operator: Exists
    tolerationSeconds: 300
  volumes:
  - emptyDir:
      medium: Memory
    name: runner-secrets
  - emptyDir:
      medium: Memory
    name: etc-gitlab-runner
  - name: init-runner-secrets
    projected:
      defaultMode: 420
      sources:
      - secret:
          items:
          - key: runner-registration-token
            path: runner-registration-token
          - key: runner-token
            path: runner-token
          name: gitlab-runner-gitlab-runner
  - configMap:
      defaultMode: 420
      name: gitlab-runner-gitlab-runner
    name: configmaps
  - name: gitlab-runner-gitlab-runner-token-rvkzm
    secret:
      defaultMode: 420
      secretName: gitlab-runner-gitlab-runner-token-rvkzm
`