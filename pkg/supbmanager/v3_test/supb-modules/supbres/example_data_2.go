package supbres

import (
	m2 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/assumes"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/compose"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/status"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
)

func (r *SupbResources) examples_2() {
	ass := assumes.NewAssumes()

	app1 := &m2.SupbApplicationBaseInfo{
		ApplicationKey:       "supb.application.test.sine",
		ApplicationKeyParent: "supb.application.test.sine",

		ApplicationName:             "自定义测试应用",
		ApplicationShortDescription: "能够产生单核正弦CPU利用率波形的测试应用",
		ApplicationDescription:      "使用单核心，运行时通过设置周期来产生正弦波形，如果出现其他应用争抢CPU核心，正弦波将会变形，用来测试干扰和干扰调节程序",
		ApplicationLevel:            m2.Guaranteed,
		ApplicationStatus:           status.ApplicationNotReady,
		ApplicationType:             types.StaticApplication,
		ApplicationParent:           []string{},
		ApplicationChild:            []string{},

		ApplicationContainers:          []*compose.Containers{},
		ApplicationContainerNamePrefix: "SUPB_APP",
		ApplicationStaticStrategies:    make(map[types.SupbStrategyKey]*m2.ApplicationController),

		ApplicationCompose: app_t_1,
		ApplicationTags: []types.Tag{
			{
				Key:   "tag1",
				Type:  "string",
				Value: "hello world",
			},
		},
	}
	_ = m2.MarshallCompose(app1)

	p1, err := r.appMgr.PutApplicationsAssume(types.StaticApplication, app1)
	if err != nil {
		r.logger.Errorf("insert example %v", err)
	}
	ass.AddAssume(p1)

	app2 := &m2.SupbApplicationBaseInfo{
		ApplicationKey:       "supb.application.test.sine",
		ApplicationKeyParent: "supb.application.test.sine",

		ApplicationName:             "自定义测试应用",
		ApplicationShortDescription: "能够产生单核正弦CPU利用率波形的测试应用",
		ApplicationDescription:      "使用单核心，运行时通过设置周期来产生正弦波形，如果出现其他应用争抢CPU核心，正弦波将会变形，用来测试干扰和干扰调节程序",
		ApplicationLevel:            m2.Guaranteed,
		ApplicationStatus:           status.ApplicationNotReady,
		ApplicationType:             types.StaticApplication,
		ApplicationParent:           []string{},
		ApplicationChild:            []string{},

		ApplicationContainers:          []*compose.Containers{},
		ApplicationContainerNamePrefix: "SUPB_APP",
		ApplicationStaticStrategies:    make(map[types.SupbStrategyKey]*m2.ApplicationController),

		ApplicationCompose: app_t_2,
		ApplicationTags: []types.Tag{
			{
				Key:   "tag1",
				Type:  "string",
				Value: "hello world",
			},
		},
	}
	_ = m2.MarshallCompose(app2)

	p2, err := r.appMgr.PutApplicationsAssume(types.StaticApplication, app2)
	if err != nil {
		r.logger.Errorf("insert example %v", err)
	}
	ass.AddAssume(p2)

	app3 := &m2.SupbApplicationBaseInfo{
		ApplicationKey:       "supb.application.test.sine",
		ApplicationKeyParent: "supb.application.test.sine",

		ApplicationName:             "自定义测试应用",
		ApplicationShortDescription: "能够产生单核正弦CPU利用率波形的测试应用",
		ApplicationDescription:      "使用单核心，运行时通过设置周期来产生正弦波形，如果出现其他应用争抢CPU核心，正弦波将会变形，用来测试干扰和干扰调节程序",
		ApplicationLevel:            m2.Guaranteed,
		ApplicationStatus:           status.ApplicationNotReady,
		ApplicationType:             types.StaticApplication,
		ApplicationParent:           []string{},
		ApplicationChild:            []string{},

		ApplicationContainers:          []*compose.Containers{},
		ApplicationContainerNamePrefix: "SUPB_APP",
		ApplicationStaticStrategies:    make(map[types.SupbStrategyKey]*m2.ApplicationController),

		ApplicationCompose: app_t_3,
		ApplicationTags: []types.Tag{
			{
				Key:   "tag1",
				Type:  "string",
				Value: "hello world",
			},
		},
	}
	_ = m2.MarshallCompose(app3)

	p3, err := r.appMgr.PutApplicationsAssume(types.StaticApplication, app3)
	if err != nil {
		r.logger.Errorf("insert example %v", err)
	}
	ass.AddAssume(p3)

	app4 := &m2.SupbApplicationBaseInfo{
		ApplicationKey:       "supb.application.test.sine",
		ApplicationKeyParent: "supb.application.test.sine",

		ApplicationName:             "自定义测试应用",
		ApplicationShortDescription: "能够产生单核正弦CPU利用率波形的测试应用",
		ApplicationDescription:      "使用单核心，运行时通过设置周期来产生正弦波形，如果出现其他应用争抢CPU核心，正弦波将会变形，用来测试干扰和干扰调节程序",
		ApplicationLevel:            m2.Guaranteed,
		ApplicationStatus:           status.ApplicationNotReady,
		ApplicationType:             types.StaticApplication,
		ApplicationParent:           []string{},
		ApplicationChild:            []string{},

		ApplicationContainers:          []*compose.Containers{},
		ApplicationContainerNamePrefix: "SUPB_APP",
		ApplicationStaticStrategies:    make(map[types.SupbStrategyKey]*m2.ApplicationController),

		ApplicationCompose: app_t_4,
		ApplicationTags: []types.Tag{
			{
				Key:   "tag1",
				Type:  "string",
				Value: "hello world",
			},
		},
	}
	_ = m2.MarshallCompose(app4)

	p4, err := r.appMgr.PutApplicationsAssume(types.StaticApplication, app4)
	if err != nil {
		r.logger.Errorf("insert example %v", err)
	}
	ass.AddAssume(p4)

	ass.Done()
}

const app_t_1 = `
containers:
  emu-service:
    appkey: "supb.application.test.sine.cpu"
    parent: []
    child: []
    name: ""
    image: 39.101.140.145:5000/cpu-ctrl-core-1
    cid: ""
    guid: ""
    status: NotReady
    ports:
    envs:
      SUPB_GUID: ""
      SUPB_T: "30"
      SUPB_I: "0.04"
    cmd:
    - /bin/sh
    - boot.sh
    tags:
    - key: HOST
      type: string
      value: SERVER0
    - key: cpu.set
      type: string
      value: "5"
`

const app_t_2 = `
containers:
  emu-service:
    appkey: "supb.application.test.sine.cpu"
    parent: []
    child: []
    name: ""
    image: 39.101.140.145:5000/cpu-ctrl-core-1
    cid: ""
    guid: ""
    status: NotReady
    ports:
    envs:
      SUPB_GUID: ""
      SUPB_T: "30"
      SUPB_I: "0.04"
    cmd:
    - /bin/sh
    - boot.sh
    tags:
    - key: HOST
      type: string
      value: SERVER1
    - key: cpu.set
      type: string
      value: "5"
`

const app_t_3 = `
containers:
  emu-service:
    appkey: "supb.application.test.sine.cpu"
    parent: []
    child: []
    name: ""
    image: 39.101.140.145:5000/cpu-ctrl-core-1
    cid: ""
    guid: ""
    status: NotReady
    ports:
    envs:
      SUPB_GUID: ""
      SUPB_T: "30"
      SUPB_I: "0.04"
    cmd:
    - /bin/sh
    - boot.sh
    tags:
    - key: HOST
      type: string
      value: SERVER2
    - key: cpu.set
      type: string
      value: "5"
`

const app_t_4 = `
containers:
  emu-service:
    appkey: "supb.application.test.sine.cpu"
    parent: []
    child: []
    name: ""
    image: 39.101.140.145:5000/cpu-ctrl-core-1
    cid: ""
    guid: ""
    status: NotReady
    ports:
    envs:
      SUPB_GUID: ""
      SUPB_T: "30"
      SUPB_I: "0.04"
    cmd:
    - /bin/sh
    - boot.sh
    tags:
    - key: HOST
      type: string
      value: SERVER3
    - key: cpu.set
      type: string
      value: "5"
`
