package supbres

import (
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/syncer"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbrunner"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbspg"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"
)

type option func(resources *SupbResources)

func WithRecorder(recorder supbapis.Recorder) option {
	return func(supbres *SupbResources) {
		supbres.appMgr.OptionConfig(applications.WithRecorder(recorder))
		supbres.strategyMgr.OptionConfig(strategies.WithRecorder(recorder))
		supbres.resSyncer.OptionConfig(syncer.WithRecorder(recorder))
	}
}

func WithRunner(runner *supbrunner.SupbRunner) option {
	return func(supbres *SupbResources) {
		supbres.runner = runner
	}
}

func WithSponge(spg *supbspg.SupbSponge) option {
	return func(supbres *SupbResources) {
		supbres.spg = spg
	}
}