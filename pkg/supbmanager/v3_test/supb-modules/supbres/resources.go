package supbres

import (
	"context"
	"crypto/rand"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications"
	m2 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/assumes"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/compose"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/status"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies"
	m1 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/syncer"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbrunner"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbspg"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	uuid "github.com/satori/go.uuid"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"strings"
)

type SupbResources struct {
	ctx    context.Context
	prefix string

	appMgr      *applications.SupbApplications
	strategyMgr *strategies.SupbStrategies

	resSyncer *syncer.SyncMod
	spg *supbspg.SupbSponge
	runner *supbrunner.SupbRunner

	logger log.Logger

	debugMod bool
}

func NewSupbResManager(ctx context.Context, prefix string) *SupbResources {
	return &SupbResources{
		ctx:         ctx,
		prefix:      prefix,
		appMgr:      applications.NewSupbAppManager(),
		strategyMgr: strategies.NewSupbResManager(),

		resSyncer: syncer.NewSyncMod(ctx, strings.Join([]string{prefix, "syncer"}, ".")),
		logger:    log.NewLogger(prefix),
	}
}

func (r *SupbResources) StrategiesManager() *strategies.SupbStrategies {
	return r.strategyMgr
}

func (r *SupbResources) ApplicationsManager() *applications.SupbApplications {
	return r.appMgr
}

func (r *SupbResources) InitFlags(flags *pflag.FlagSet) {
	flags.Bool(r.prefix+".debug",false,"")
}

func (r *SupbResources) ViperConfig(viper *viper.Viper) {

}

func (r *SupbResources) InitViper(viper *viper.Viper) {
	r.debugMod = viper.GetBool(r.prefix+".debug")
}

func (r *SupbResources) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *SupbResources) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
	r.logger.Infof("%v initialized", r.prefix)

	r.resSyncer.Initialize(syncer.WithStrategyManager(r.strategyMgr),
		syncer.WithAppManager(r.appMgr))
	r.examples()
}

func (r *SupbResources) Close() error {
	r.logger.Infof("%v closed", r.prefix)
	return nil
}

func (r *SupbResources) RunStaticStrategies(strategyKey, strategyName string, links map[string]string) (*m1.SupbStrategyBaseInfo, error) {
	sst, err := r.strategyMgr.GetStrategies(types.StaticStrategy, types.SupbStrategyKey(strategyKey))
	if err != nil {
		return nil, fmt.Errorf("get static strategies %v", err)
	}

	ams := assumes.NewAssumes()

	sst.StrategyKeyParent = sst.StrategyKey

	randBytes := make([]byte, 5)
	_, _ = rand.Read(randBytes)
	sst.StrategyKey = types.SupbStrategyKey(fmt.Sprintf("%v_%x",sst.StrategyKeyParent, randBytes))
	sst.StrategyType = types.DynamicStrategy
	sst.StrategyStatus = status.StrategyReady
	sst.StrategyName = strategyName
	//sst.StrategyContainerNamePrefix = containerName

	for _, v := range sst.StrategyContainers {
		v.Guid = uuid.NewV4().String()
		randBytes := make([]byte, 4)
		_, _ = rand.Read(randBytes)
		v.Name = sst.StrategyContainerNamePrefix + fmt.Sprintf("_%x", randBytes)
		v.Status = "ready"
	}

	for appKey, appType := range links {
		app, err := r.appMgr.GetApplication(appType, types.SupbApplicationKey(appKey))
		if err != nil {
			ams.Cancel()
			return nil, fmt.Errorf("%v", err)
		}

		if ass, err := r.appMgr.LinkControllerAssume(app.ApplicationType, app.ApplicationKey, sst.StrategyKey, &m2.ApplicationController{
			StrategyLevel:  sst.StrategyLevel,
			StrategyKey:    sst.StrategyKey,
			StrategyStatus: sst.StrategyStatus,
			StrategyTypes:  sst.StrategyType,
		}); err != nil {
			ams.Cancel()
			return nil, fmt.Errorf("link app to strategy %v", err)
		} else {
			ams.AddAssume(ass)
		}

		sst.StrategySlaves[app.ApplicationKey] = &m1.StrategySlave{
			SlaveKey:    app.ApplicationKey,
			SlaveLevel:  app.ApplicationLevel,
			SlaveTypes:  app.ApplicationType,
			SlaveStatus: app.ApplicationStatus,
		}
	}

	//for _, slave := range sst.StrategySlaves {
	//	if app, err := r.appMgr.GetApplication(slave.SlaveTypes, slave.SlaveKey); err != nil {
	//		ams.Cancel()
	//		return nil, fmt.Errorf("slave not found")
	//	} else {
	//		// 查看应用有没有绑定静态策略
	//		if _, ok := app.ApplicationStaticStrategies[sst.StrategyKeyParent]; !ok {
	//			ams.Cancel()
	//			return nil, fmt.Errorf("link is incomplete")
	//		}
	//
	//		// 查看应用动态策略是否冲突
	//		conflict := false
	//		for _, ctl := range app.ApplicationDynamicStrategies {
	//			if ctl.StrategyLevel == m1.LevelRead {
	//				continue
	//			}
	//
	//			if sst.StrategyLevel == ctl.StrategyLevel {
	//				conflict = true
	//				break
	//			}
	//		}
	//		if conflict {
	//			r.logger.Warnf("application link conflict auto, cancel link")
	//			delete(sst.StrategySlaves, slave.SlaveKey)
	//			continue
	//		}
	//
	//		 r.resSyncer.CreateLinkSyncAssume(app.ApplicationType, app.ApplicationKey, sst.StrategyType, sst.StrategyKey)
	//		//// 动态策略不冲突则继续
	//		//app.ApplicationDynamicStrategies[sst.StrategyKey] = &m2.ApplicationController{
	//		//	StrategyKey: sst.StrategyKey,
	//		//	StrategyTypes: sst.StrategyType,
	//		//	StrategyLevel: sst.StrategyLevel,
	//		//	StrategyStatus: sst.StrategyStatus,
	//		//}
	//		//
	//		//if ass, err := r.appMgr.PutApplicationsAssume(app.ApplicationType, app); err != nil {
	//		//	ams.Cancel()
	//		//	return nil, fmt.Errorf("ams put application %v", err)
	//		//} else {
	//		//	ams.AddAssume(ass)
	//		//}
	//	}
	//}

	// 启动策略
	if !r.debugMod {
		for _, v := range sst.StrategyContainers {
			v.EnvsKey["ETCD_KEY"] = string(sst.StrategyKey)
			v.EnvsKey["RPC_GUID"] = string(sst.StrategyKey)
			//v.Tags = append(v.Tags, )
			info, err := r.runner.Run(r.spg.GetAgentGuid(v), v)
			if err != nil {
				ams.Cancel()
				return nil, fmt.Errorf("running compose %v", err)
			}
			v.Cid = info.CID
			v.Guid = info.Guid
			v.Name = info.ContainerName
		}
	}

	// 存储策略
	if ass, err := r.strategyMgr.PutStrategiesAssume(types.DynamicStrategy, sst.DeepCopy()); err != nil {
		ams.Cancel()
		return nil, fmt.Errorf("ams put strategy %v", err)
	} else {
		sst.StrategyVersion = ass.Done()
	}

	// 执行
	ams.Done()

	for appKey, appType := range links {
		r.resSyncer.CreateLinkSyncAssume(appType, types.SupbApplicationKey(appKey), sst.StrategyType, sst.StrategyKey)
	}

	r.resSyncer.StrategyPutInformer(sst.DeepCopy())
	return sst.DeepCopy(), nil
}

func (r *SupbResources) RunStaticApplication(applicationKey, appName string, links map[string]string) (*m2.SupbApplicationBaseInfo, error) {
	app, err := r.appMgr.GetApplication(types.StaticApplication, types.SupbApplicationKey(applicationKey))
	if err != nil {
		return nil, fmt.Errorf("%v", err)
	}

	ams := assumes.NewAssumes()

	app.ApplicationKeyParent = app.ApplicationKey

	randBytes := make([]byte, 5)
	_, _ = rand.Read(randBytes)
	app.ApplicationKey = types.SupbApplicationKey(fmt.Sprintf("%v_%x",app.ApplicationKeyParent, randBytes))

	app.ApplicationStatus = status.ApplicationReady
	app.ApplicationType = types.DynamicApplication
	app.ApplicationName = appName
	//app.ApplicationContainerNamePrefix = appContainName

	for _, v := range app.ApplicationContainers {
		v.Guid = uuid.NewV4().String()
		randBytes := make([]byte, 4)
		_, _ = rand.Read(randBytes)
		v.Name = app.ApplicationContainerNamePrefix + fmt.Sprintf("_%x", randBytes)
		v.Status = "ready"

	}

	for strategyKey, strategyType := range links {
		st, err := r.strategyMgr.GetStrategies(strategyType, types.SupbStrategyKey(strategyKey))
		if err != nil {
			ams.Cancel()
			return nil, fmt.Errorf("%v", err)
		}

		switch strategyType {
		case types.StaticStrategy:
			app.ApplicationStaticStrategies[st.StrategyKey] = &m2.ApplicationController{
				StrategyLevel:  st.StrategyLevel,
				StrategyKey:    st.StrategyKey,
				StrategyStatus: st.StrategyStatus,
				StrategyTypes:  st.StrategyType,
			}
		case types.DynamicStrategy:
			app.ApplicationDynamicStrategies[st.StrategyKey] = &m2.ApplicationController{
				StrategyLevel:  st.StrategyLevel,
				StrategyKey:    st.StrategyKey,
				StrategyStatus: st.StrategyStatus,
				StrategyTypes:  st.StrategyType,
			}
		}

		if ass, err := r.strategyMgr.LinkSlaveAssume(st.StrategyType, st.StrategyKey, app.ApplicationKey, &m1.StrategySlave{
			SlaveKey:    app.ApplicationKey,
			SlaveLevel:  app.ApplicationLevel,
			SlaveTypes:  app.ApplicationType,
			SlaveStatus: app.ApplicationStatus,
		}); err != nil {
			ams.Cancel()
			return nil, fmt.Errorf("link strategy to app %v", err)
		} else {
			ams.AddAssume(ass)
		}
	}
	//for _, strategyController := range app.ApplicationStaticStrategies {
	//	if st, err := r.strategyMgr.GetStrategies(strategyController.StrategyTypes, strategyController.StrategyKey); err != nil {
	//		ams.Cancel()
	//		return nil, fmt.Errorf("contorller not fount")
	//	} else {
	//		// 是否连接静态策略
	//		if _, ok := st.StrategySlaves[app.ApplicationKeyParent]; !ok {
	//			ams.Cancel()
	//			return nil, fmt.Errorf("link is incomplete")
	//		}
	//		// 生成连接动态策略
	//		r.resSyncer.CreateLinkSyncAssume(app.ApplicationType, app.ApplicationKey, st.StrategyType, st.StrategyKey)
	//	}
	//}
	//
	//for _, strategyController := range app.ApplicationDynamicStrategies {
	//	if st, err := r.strategyMgr.GetStrategies(strategyController.StrategyTypes, strategyController.StrategyKey); err != nil {
	//		ams.Cancel()
	//		return nil, fmt.Errorf("contorller not fount")
	//	} else {
	//		// 是否连接静态策略
	//		if _, ok := st.StrategySlaves[app.ApplicationKeyParent]; !ok {
	//			ams.Cancel()
	//			return nil, fmt.Errorf("link is incomplete")
	//		}
	//
	//		r.resSyncer.CreateLinkSyncAssume(app.ApplicationType, app.ApplicationKey, st.StrategyType, st.StrategyKey)
	//	}
	//}

	// 启动应用
	if !r.debugMod {
		//if len(app.ApplicationContainers) > 1 {
		//	if strings.Contains(string(app.ApplicationKey),"supb.application.test.car") {
		//
		//		var info *compose.RunningBaseInfo
		//
		//		for _,v := range app.ApplicationContainers {
		//			if strings.Contains(v.AppKey,"supb.application.test.car.darknet") {
		//				guid := r.spg.GetAgentGuid(v)
		//				r.logger.Infof("scheduler to %v",guid)
		//
		//				info, err = r.runner.Run(guid, v)
		//				if err != nil {
		//					ams.Cancel()
		//					return nil, fmt.Errorf("running compose %v", err)
		//				}
		//				v.Cid = info.CID
		//				v.Guid = info.Guid
		//				v.Name = info.ContainerName
		//				r.logger.Infof("run %v ok return %v",v.AppKey,info.CID)
		//			}
		//		}
		//
		//		for _,v := range app.ApplicationContainers {
		//			if strings.Contains(v.AppKey,"supb.application.test.car.app") {
		//				guid := r.spg.GetAgentGuid(v)
		//				r.logger.Infof("scheduler to %v",guid)
		//
		//				if info != nil {
		//					v.EnvsKey["DARK_NET_HOST"] = info.Host
		//				}
		//
		//				info, err = r.runner.Run(guid, v)
		//				if err != nil {
		//					ams.Cancel()
		//					return nil, fmt.Errorf("running compose %v", err)
		//				}
		//				v.Cid = info.CID
		//				v.Guid = info.Guid
		//				v.Name = info.ContainerName
		//				r.logger.Infof("run %v ok return %v",v.AppKey,info.CID)
		//			}
		//
		//		}
		//
		//		for _,v := range app.ApplicationContainers {
		//			//v.EnvsKey["DARK_NET_HOST"] = info.Host
		//			if strings.Contains(v.AppKey,"supb.application.test.car.frontend") {
		//				//v.EnvsKey["CAR_PATH"] = info.Host + ":10039"
		//				guid := r.spg.GetAgentGuid(v)
		//				r.logger.Infof("scheduler to %v",guid)
		//
		//				info, err = r.runner.Run(guid, v)
		//				if err != nil {
		//					ams.Cancel()
		//					return nil, fmt.Errorf("running compose %v", err)
		//				}
		//				v.Cid = info.CID
		//				v.Guid = info.Guid
		//				v.Name = info.ContainerName
		//				r.logger.Infof("run %v ok return %v",v.AppKey,info.CID)
		//				app.ApplicationTags = append(app.ApplicationTags, types.Tag{
		//					Key: "URL",
		//					Type: "string",
		//					Value: "http://"+info.Host+":10041/car_war/",
		//				})
		//			}
		//
		//		}
		//
		//	} else {
		//		for _, v := range app.ApplicationContainers {
		//			guid := r.spg.GetAgentGuid(v)
		//			r.logger.Infof("scheduler to %v",guid)
		//			info, err := r.runner.Run(guid, v)
		//			if err != nil {
		//				ams.Cancel()
		//				return nil, fmt.Errorf("running compose %v", err)
		//			}
		//			v.Cid = info.CID
		//			v.Guid = info.Guid
		//			v.Name = info.ContainerName
		//		}
		//	}
		//
		//	//for _,v := range clist {
		//	//	r.logger.Infof("clist %v", v)
		//	//}
		//	//r.logger.Infof("Map %v",cmap)
		//
		//}
		if strings.Contains(string(app.ApplicationKey),"supb.application.gitlab.runner"){
			var info *compose.RunningBaseInfo

			for _,v := range app.ApplicationContainers {
				if strings.Contains(v.AppKey,"supb.application.gitlab.runner.gitlab_runner") {
					guid := r.spg.GetAgentGuid(v)
					r.logger.Infof("scheduler to %v",guid)

					if info != nil {
						v.EnvsKey["DARK_NET_HOST"] = info.Host
					}

					info, err = r.runner.Run(guid, v)
					if err != nil {
						ams.Cancel()
						return nil, fmt.Errorf("running compose %v", err)
					}
					//赋值参数还需要修改
					v.Cid = info.CID
					v.Guid = info.Guid
					v.Name = info.ContainerName
					r.logger.Infof("run %v ok return %v",v.AppKey,info.CID)
				}

			}

		} else {
			for _, v := range app.ApplicationContainers {
				info, err := r.runner.Run(r.spg.GetAgentGuid(v), v)
				if err != nil {
					ams.Cancel()
					return nil, fmt.Errorf("running compose %v", err)
				}
				v.Cid = info.CID
				v.Guid = info.Guid
				v.Name = info.ContainerName
			}
		}
	}

	//app.ApplicationContainerId = info.CID
	//app.ApplicationGuid = info.Guid
	//app.ApplicationContainerNamePrefix = info.ContainerName

	// 存储策略
	if ass, err := r.appMgr.PutApplicationsAssume(types.DynamicApplication, app.DeepCopy()); err != nil {
		ams.Cancel()
		return nil, fmt.Errorf("ams put strategy %v", err)
	} else {
		app.ApplicationVersion = ass.Done()
	}

	// 执行
	ams.Done()

	for strategyKey, strategyType := range links {
		r.resSyncer.CreateLinkSyncAssume(app.ApplicationType, app.ApplicationKey, strategyType, types.SupbStrategyKey(strategyKey))
	}

	r.resSyncer.ApplicationPutInformer(app.DeepCopy())
	return app.DeepCopy(), nil
}

func (r *SupbResources) GetStrategy(strategyTypes, strategyKey string) (*m1.SupbStrategyBaseInfo, error) {
	return r.strategyMgr.GetStrategies(strategyTypes, types.SupbStrategyKey(strategyKey))
}

func (r *SupbResources) ListStrategies(strategyTypes string) ([]*m1.SupbStrategyBaseInfo, error) {
	return r.strategyMgr.ListStrategies(strategyTypes)
}

func (r *SupbResources) GetApplication(appTypes, appKey string) (*m2.SupbApplicationBaseInfo, error) {
	return r.appMgr.GetApplication(appTypes, types.SupbApplicationKey(appKey))
}

func (r *SupbResources) ListApplications(appTypes string) ([]*m2.SupbApplicationBaseInfo, error) {
	return r.appMgr.ListApplications(appTypes)
}

func (r *SupbResources) RemoveDynamicStrategy(strategyKey string) (*m1.SupbStrategyBaseInfo, error) {
	st, err := r.strategyMgr.GetStrategies(types.DynamicStrategy, types.SupbStrategyKey(strategyKey))
	if err != nil {
		return nil, fmt.Errorf("get dynamic strategy %v", err)
	}

	// 正式远端移除策略
	if !r.debugMod {
		for _, v := range st.StrategyContainers {
			err = r.runner.Remove(r.spg.GetContainerGuid(v), v)
			if err != nil {
				return nil, fmt.Errorf("stop compose %v", err)
			}
		}
	}

	// 假定本地移除策略
	if ass, err := r.strategyMgr.RemoveStrategiesAssume(types.DynamicStrategy, st.StrategyKey); err != nil {
		return nil, fmt.Errorf("ams remove dynamic strategies %v", err)
	} else {
		st.StrategyVersion = ass.Done()
	}

	// 正式本地移除和通知应用
	r.resSyncer.StrategyDelInformer(st.DeepCopy())
	return st.DeepCopy(), nil
}

func (r *SupbResources) StopDynamicStrategy(strategyKey string) (*m1.SupbStrategyBaseInfo, error) {
	st, err := r.strategyMgr.GetStrategies(types.DynamicStrategy, types.SupbStrategyKey(strategyKey))
	if err != nil {
		return nil, fmt.Errorf("get dynamic strategy %v", err)
	}

	// 正式远端停止策略
	if !r.debugMod {
		for _, v := range st.StrategyContainers {
			err = r.runner.Stop(r.spg.GetContainerGuid(v), v)
			if err != nil {
				return nil, fmt.Errorf("stop compose %v", err)
			}
		}
	}

	// 假定本地更新状态
	st.StrategyStatus = status.StrategyNotReady
	if ass, err := r.strategyMgr.PutStrategiesAssume(types.DynamicStrategy, st.DeepCopy()); err != nil {
		return nil, fmt.Errorf("ams remove dynamic strategies %v", err)
	} else {
		st.StrategyVersion = ass.Done()
	}

	// 正式本地停止和通知应用
	r.resSyncer.StrategyPutInformer(st.DeepCopy())
	return st.DeepCopy(), nil
}

func (r *SupbResources) ReStartDynamicStrategy(strategyKey string) (*m1.SupbStrategyBaseInfo, error) {
	st, err := r.strategyMgr.GetStrategies(types.DynamicStrategy, types.SupbStrategyKey(strategyKey))
	if err != nil {
		return nil, fmt.Errorf("get dynamic strategy %v", err)
	}

	// 正式远端停止策略
	if !r.debugMod {
		for _, v := range st.StrategyContainers {
			err = r.runner.Restart(r.spg.GetContainerGuid(v), v)
			if err != nil {
				return nil, fmt.Errorf("running compose %v", err)
			}
		}
	}

	st.StrategyStatus = status.StrategyReady
	if ass, err := r.strategyMgr.PutStrategiesAssume(types.DynamicStrategy, st.DeepCopy()); err != nil {
		return nil, fmt.Errorf("remove dynamic strategies %v", err)
	} else {
		st.StrategyVersion = ass.Done()
	}

	// 正式本地停止和通知应用
	r.resSyncer.StrategyPutInformer(st.DeepCopy())
	return st.DeepCopy(), nil
}

func (r *SupbResources) RemoveStaticStrategy(strategyKey string) (*m1.SupbStrategyBaseInfo, error) {
	st, err := r.strategyMgr.GetStrategies(types.StaticStrategy, types.SupbStrategyKey(strategyKey))
	if err != nil {
		return nil, fmt.Errorf("get dynamic strategy %v", err)
	}

	// 假定本地移除策略
	if ass, err := r.strategyMgr.RemoveStrategiesAssume(types.StaticStrategy, st.StrategyKey); err != nil {
		return nil, fmt.Errorf("ams remove dynamic strategies %v", err)
	} else {
		st.StrategyVersion = ass.Done()
	}

	r.resSyncer.StrategyDelInformer(st.DeepCopy())
	return st.DeepCopy(), nil
}

func (r *SupbResources) RemoveDynamicApplication(applicationKey string) (*m2.SupbApplicationBaseInfo, error) {
	app, err := r.appMgr.GetApplication(types.DynamicApplication, types.SupbApplicationKey(applicationKey))
	if err != nil {
		return nil, fmt.Errorf("get dynamic application %v", err)
	}

	// 正式远端移除应用
	if !r.debugMod {
		for _, v := range app.ApplicationContainers {
			err = r.runner.Remove(r.spg.GetContainerGuid(v), v)
			if err != nil {
				return nil, fmt.Errorf("running compose %v", err)
			}
		}
	}

	// 假定本地移除应用
	if ass, err := r.appMgr.RemoveApplicationAssume(types.DynamicStrategy, app.ApplicationKey); err != nil {
		return nil, fmt.Errorf("ams remove dynamic strategies %v", err)
	} else {
		app.ApplicationVersion = ass.Done()
	}

	// 正式本地移除和通知应用
	r.resSyncer.ApplicationDelInformer(app.DeepCopy())
	return app.DeepCopy(), nil
}

func (r *SupbResources) StopDynamicApplication(applicationKey string) (*m2.SupbApplicationBaseInfo, error) {
	app, err := r.appMgr.GetApplication(types.DynamicApplication, types.SupbApplicationKey(applicationKey))
	if err != nil {
		return nil, fmt.Errorf("get dynamic strategy %v", err)
	}

	// 正式远端停止应用
	if !r.debugMod {
		for _, v := range app.ApplicationContainers {
			err = r.runner.Stop(r.spg.GetContainerGuid(v), v)
			if err != nil {
				return nil, fmt.Errorf("running compose %v", err)
			}
		}
	}

	// 假定本地更新状态
	app.ApplicationStatus = status.ApplicationNotReady
	if ass, err := r.appMgr.PutApplicationsAssume(types.DynamicApplication, app); err != nil {
		return nil, fmt.Errorf("ams remove dynamic strategies %v", err)
	} else {
		app.ApplicationVersion = ass.Done()
	}

	// 正式本地停止和通知策略
	r.resSyncer.ApplicationPutInformer(app)
	return app.DeepCopy(), nil
}

func (r *SupbResources) ReStartDynamicApplication(applicationKey string) (*m2.SupbApplicationBaseInfo, error) {
	app, err := r.appMgr.GetApplication(types.DynamicApplication, types.SupbApplicationKey(applicationKey))
	if err != nil {
		return nil, fmt.Errorf("get dynamic strategy %v", err)
	}

	// 正式远端停止应用
	if !r.debugMod {
		for _, v := range app.ApplicationContainers {
			err = r.runner.Restart(r.spg.GetContainerGuid(v), v)
			if err != nil {
				return nil, fmt.Errorf("running compose %v", err)
			}
		}
	}

	// 假定本地更新状态
	app.ApplicationStatus = status.ApplicationReady
	if ass, err := r.appMgr.PutApplicationsAssume(types.DynamicApplication, app); err != nil {
		return nil, fmt.Errorf("ams remove dynamic strategies %v", err)
	} else {
		app.ApplicationVersion = ass.Done()
	}

	// 正式本地停止和通知策略
	r.resSyncer.ApplicationPutInformer(app.DeepCopy())
	return app.DeepCopy(), nil
}

func (r *SupbResources) RemoveStaticApplication(applicationKey string) (*m2.SupbApplicationBaseInfo, error) {
	app, err := r.appMgr.GetApplication(types.StaticApplication, types.SupbApplicationKey(applicationKey))
	if err != nil {
		return nil, fmt.Errorf("get dynamic application %v", err)
	}

	// 假定本地移除应用
	if ass, err := r.appMgr.RemoveApplicationAssume(types.StaticStrategy, app.ApplicationKey); err != nil {
		return nil, fmt.Errorf("ams remove dynamic strategies %v", err)
	} else {
		app.ApplicationVersion = ass.Done()
	}

	r.resSyncer.ApplicationDelInformer(app.DeepCopy())
	return app.DeepCopy(), nil
}

func (r *SupbResources) PutStrategy(strategyTypes string, strategy *m1.SupbStrategyBaseInfo) (*m1.SupbStrategyBaseInfo, error) {
	// check
	//st, err := r.strategyMgr.GetStrategies(strategyTypes, strategy.StrategyKey)
	//if err != nil && !errors.Is(err, types.StrategyIsNilError) {
	//	return err
	//}

	// 存在需要check
	switch strategyTypes {
	case types.StaticStrategy:
		if err := m1.ValidateStaticStrategy(strategy); err != nil {
			return nil, fmt.Errorf("validate %v", err)
		}
	case types.DynamicStrategy:
		if err := m1.ValidateDynamicStrategy(strategy); err != nil {
			return nil, fmt.Errorf("validate %v", err)
		}
	}

	// put
	ass, err := r.strategyMgr.PutStrategiesAssume(strategyTypes, strategy.DeepCopy())
	if err != nil {
		return nil, err
	}
	//// 是否新增了连接
	//for slaveKey, slave := range strategy.StrategySlaves {
	//	if _, ok := st.StrategySlaves[slaveKey]; !ok {
	//		if _, err := r.resSyncer.CreateLinkSync(slave.SlaveTypes, slave.SlaveKey, strategy.StrategyType, strategy.StrategyKey); err != nil {
	//			ass.Cancel()
	//			return fmt.Errorf("create link %v", err)
	//		}
	//	}
	//}

	strategy.StrategyVersion = ass.Done()
	r.resSyncer.StrategyPutInformer(strategy.DeepCopy())
	return strategy, nil
}

func (r *SupbResources) PutApplication(appTypes string, application *m2.SupbApplicationBaseInfo) (*m2.SupbApplicationBaseInfo, error) {
	//app, err := r.appMgr.GetApplication(appTypes, app.ApplicationKey)
	//if err != nil && !errors.Is(err, types.ApplicationIsNilError) {
	//	return err
	//}

	switch appTypes {
	case types.StaticApplication:
		if err := m2.ValidateStaticApplication(application); err != nil {
			return nil, fmt.Errorf("validate %v", err)
		}
	case types.DynamicApplication:
		if err := m2.ValidateDynamicApplication(application); err != nil {
			return nil, fmt.Errorf("validate %v", err)
		}
	}

	// put
	ass, err := r.appMgr.PutApplicationsAssume(appTypes, application.DeepCopy())
	if err != nil {
		return nil, err
	}
	//// 是否新增了连接
	//for _, st := range application.ApplicationStaticStrategies {
	//	if _, err := r.resSyncer.CreateLinkSync(application.ApplicationType, application.ApplicationKey, st.StrategyTypes, st.StrategyKey); err != nil {
	//		ass.Cancel()
	//		return fmt.Errorf("create link %v", err)
	//	}
	//}
	//
	//for _, st := range application.ApplicationDynamicStrategies {
	//	if _, err := r.resSyncer.CreateLinkSync(application.ApplicationType, application.ApplicationKey, st.StrategyTypes, st.StrategyKey); err != nil {
	//		ass.Cancel()
	//		return fmt.Errorf("create link %v", err)
	//	}
	//}
	application.ApplicationVersion = ass.Done()
	r.resSyncer.ApplicationPutInformer(application.DeepCopy())
	return application, nil
}

func (r *SupbResources) Link(appType, appKey string, strategyType, strategyKey string) error {
	app, err := r.appMgr.GetApplication(appType, types.SupbApplicationKey(appKey))
	if err != nil {
		return fmt.Errorf("%v", err)
	}
	st, err := r.strategyMgr.GetStrategies(strategyType, types.SupbStrategyKey(strategyKey))
	if err != nil {
		return fmt.Errorf("get dynamic strategy %v", err)
	}

	ams := assumes.NewAssumes()

	if ass, err := r.appMgr.LinkControllerAssume(app.ApplicationType, app.ApplicationKey, st.StrategyKey, &m2.ApplicationController{
		StrategyLevel:  st.StrategyLevel,
		StrategyKey:    st.StrategyKey,
		StrategyStatus: st.StrategyStatus,
		StrategyTypes:  strategyType,
	}); err != nil {
		ams.Cancel()
		return fmt.Errorf("link app to strategy %v", err)
	} else {
		ams.AddAssume(ass)
	}

	if ass, err := r.strategyMgr.LinkSlaveAssume(st.StrategyType, st.StrategyKey, app.ApplicationKey, &m1.StrategySlave{
		SlaveKey:    app.ApplicationKey,
		SlaveLevel:  app.ApplicationLevel,
		SlaveTypes:  app.ApplicationType,
		SlaveStatus: app.ApplicationStatus,
	}); err != nil {

		ams.Cancel()
		return fmt.Errorf("link strategy to app %v", err)
	} else {
		ams.AddAssume(ass)
	}
	ams.Done()

	r.resSyncer.CreateLinkSyncAssume(app.ApplicationType, app.ApplicationKey, st.StrategyType, st.StrategyKey)
	return nil
}

func (r *SupbResources) UnLink(appType, appKey string, strategyType, strategyKey string) error {
	st, err := r.strategyMgr.GetStrategies(strategyType, types.SupbStrategyKey(strategyKey))
	if err != nil {
		return fmt.Errorf("get dynamic strategy %v", err)
	}

	app, err := r.appMgr.GetApplication(appType, types.SupbApplicationKey(appKey))
	if err != nil {
		return fmt.Errorf("get dynamic strategy %v", err)
	}

	ams := assumes.NewAssumes()

	if ass, err := r.appMgr.UnLinkControllerAssume(app.ApplicationType, app.ApplicationKey, st.StrategyType, st.StrategyKey); err != nil {
		ams.Cancel()
		return fmt.Errorf("%v", err)
	} else {
		ams.AddAssume(ass)
	}

	if ass, err := r.strategyMgr.UnLinkSlaveAssume(st.StrategyType, st.StrategyKey, app.ApplicationKey); err != nil {
		ams.Cancel()
		return fmt.Errorf("%v", err)
	} else {
		ams.AddAssume(ass)
	}

	ams.Done()
	return nil
}

func (r *SupbResources) AddStrategy(strategy *m1.SupbStrategyBaseInfo) (*m1.SupbStrategyBaseInfo, error) {
	// 存在需要check

	strategy.StrategyStatus = status.StrategyNotReady
	strategy.StrategyKey = types.SupbStrategyKey(uuid.NewV4().String())
	strategy.StrategyKeyParent = strategy.StrategyKey
	strategy.StrategyType = types.StaticStrategy

	if err := m1.ValidateStaticStrategy(strategy); err != nil {
		return nil, fmt.Errorf("validate %v", err)
	}

	if err := m1.MarshallCompose(strategy); err != nil {
		return nil, fmt.Errorf("%v", err)
	}

	// put
	ass, err := r.strategyMgr.PutStrategiesAssume(types.StaticStrategy, strategy.DeepCopy())
	if err != nil {
		return nil, err
	}
	// 是存在连接
	//for _, slave := range strategy.StrategySlaves {
	//	if _, err := r.resSyncer.CreateLinkSync(slave.SlaveTypes, slave.SlaveKey, strategy.StrategyType, strategy.StrategyKey); err != nil {
	//		ass.Cancel()
	//		return fmt.Errorf("create link %v", err)
	//	}
	//}

	strategy.StrategyVersion = ass.Done()
	r.resSyncer.StrategyPutInformer(strategy.DeepCopy())
	return strategy, nil
}

func (r *SupbResources) AddApplication(app *m2.SupbApplicationBaseInfo) (*m2.SupbApplicationBaseInfo, error) {
	// 存在需要check

	app.ApplicationStatus = status.ApplicationNotReady
	app.ApplicationKey = types.SupbApplicationKey(uuid.NewV4().String())
	app.ApplicationKeyParent = app.ApplicationKey
	app.ApplicationType = types.StaticApplication

	if err := m2.ValidateStaticApplication(app); err != nil {
		return nil, fmt.Errorf("validate %v", err)
	}

	if err := m2.MarshallCompose(app); err != nil {
		return nil, fmt.Errorf("%v", err)
	}

	// put
	ass, err := r.appMgr.PutApplicationsAssume(types.StaticApplication, app.DeepCopy())
	if err != nil {
		return nil, err
	}
	// 是存在连接
	//for _, slave := range strategy.StrategySlaves {
	//	if _, err := r.resSyncer.CreateLinkSync(slave.SlaveTypes, slave.SlaveKey, strategy.StrategyType, strategy.StrategyKey); err != nil {
	//		ass.Cancel()
	//		return fmt.Errorf("create link %v", err)
	//	}
	//}

	app.ApplicationVersion = ass.Done()
	r.resSyncer.ApplicationPutInformer(app.DeepCopy())
	return app, nil
}
