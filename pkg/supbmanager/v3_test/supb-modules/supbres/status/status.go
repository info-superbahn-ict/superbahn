package status

const (
	ApplicationReady    = "Ready"
	ApplicationNotReady = "NotReady"

	StrategyReady       = "Ready"
	StrategyPending     = "Pending"
	StrategyNotReady    = "NotReady"
	StrategyStopping    = "Stopping"
	StrategyTerminating = "Terminating"
	StrategyPause       = "Pause"
	StrategyError       = "Error"
)

func DynamicStatus() []string {
	return []string{StrategyReady, StrategyPending, StrategyNotReady, StrategyStopping, StrategyTerminating, StrategyPause, StrategyError}
}
