package strategies

import (
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"
)

type putAssume struct {
	supb     *SupbStrategies
	types    string
	key      types.SupbStrategyKey
	strategy *model.SupbStrategyBaseInfo

	writeAssume supbapis.Assume

	done bool
}

func (r *putAssume) Done() uint64 {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return 0
	}

	switch r.types {
	case types.StaticStrategy:
		// 删除假设
		//delete(r.supb.staticAssume, r.key)

		// 如果当前版本更新，则不覆盖，（其他线程进行了put）
		if st,ok := r.supb.staticsStrategies[r.key];ok{
			if st.StrategyVersion >= r.strategy.StrategyVersion{
				return 0
			}
		}

		// 否者覆盖
		r.supb.staticsStrategies[r.key]=r.strategy
	case types.DynamicStrategy:
		// 删除假设
		//delete(r.supb.dynamicAssume, r.key)

		// 如果当前版本更新，则不覆盖，（其他线程进行了put）
		if st,ok := r.supb.dynamicStrategies[r.key];ok{
			if st.StrategyVersion >= r.strategy.StrategyVersion{
				return 0
			}
		}

		// 否者覆盖
		r.supb.dynamicStrategies[r.key]=r.strategy
	}
	r.writeAssume.Done()
	r.done = true

	return r.strategy.StrategyVersion
}

func (r *putAssume) Cancel() {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return
	}
	//
	//switch r.types {
	//case types.StaticStrategy:
	//	delete(r.supb.staticAssume, r.key)
	//case types.DynamicStrategy:
	//	delete(r.supb.dynamicAssume, r.key)
	//}
	r.writeAssume.Cancel()
	r.done = true
}

type linkAssume struct {
	supb *SupbStrategies
	types string

	key types.SupbStrategyKey

	strategy *model.SupbStrategyBaseInfo

	writeAssume supbapis.Assume

	done bool
}

func (r *linkAssume) Done() uint64 {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return 0
	}

	switch r.types {
	case types.StaticStrategy:
		//delete(r.supb.staticLinksAssume, r.key)
		st,ok := r.supb.staticsStrategies[r.key]

		// 被删除了
		if !ok {
			return 0
		}

		// 被更改了
		if st.StrategyVersion >= r.strategy.StrategyVersion{
			return 0
		}
		r.supb.staticsStrategies[r.key]=r.strategy
	case types.DynamicStrategy:
		//delete(r.supb.dynamicLinksAssume, r.key)
		st,ok := r.supb.dynamicStrategies[r.key]

		// 被删除了
		if !ok {
			return 0
		}

		// 被更改了
		if st.StrategyVersion >= r.strategy.StrategyVersion{
			return 0
		}
		r.supb.dynamicStrategies[r.key]=r.strategy
	}
	r.writeAssume.Done()
	r.done = true

	return r.strategy.StrategyVersion
}

func (r *linkAssume) Cancel() {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return
	}

	//switch r.types {
	//case types.StaticStrategy:
	//	delete(r.supb.staticLinksAssume, r.key)
	//case types.DynamicStrategy:
	//	delete(r.supb.dynamicLinksAssume, r.key)
	//}
	r.writeAssume.Cancel()
	r.done = true
}

type unLinkAssume struct {
	supb *SupbStrategies
	types string

	key types.SupbStrategyKey

	strategy *model.SupbStrategyBaseInfo

	writeAssume supbapis.Assume

	done bool
}

func (r *unLinkAssume) Done() uint64 {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return 0
	}

	switch r.types {
	case types.StaticStrategy:
		//delete(r.supb.staticLinksAssume, r.key)
		st,ok := r.supb.staticsStrategies[r.key]

		// 被删除了
		if !ok {
			return 0
		}

		// 被更改了
		if st.StrategyVersion >= r.strategy.StrategyVersion{
			return 0
		}

		r.supb.staticsStrategies[r.key]=r.strategy
	case types.DynamicStrategy:
		//delete(r.supb.dynamicLinksAssume, r.key)

		st,ok := r.supb.dynamicStrategies[r.key]

		// 被删除了
		if !ok {
			return 0
		}

		// 被更改了
		if st.StrategyVersion >= r.strategy.StrategyVersion{
			return 0
		}
		r.supb.dynamicStrategies[r.key]=r.strategy
	}
	r.writeAssume.Done()
	r.done = true

	return r.strategy.StrategyVersion
}

func (r *unLinkAssume) Cancel() {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return
	}

	//switch r.types {
	//case types.StaticStrategy:
	//	delete(r.supb.staticLinksAssume, r.key)
	//case types.DynamicStrategy:
	//	delete(r.supb.dynamicLinksAssume, r.key)
	//}
	r.writeAssume.Cancel()
	r.done = true
}


type removeAssume struct {
	supb     *SupbStrategies
	types    string
	key      types.SupbStrategyKey
	strategy *model.SupbStrategyBaseInfo

	deleteAssume supbapis.Assume

	done bool
}

func (r *removeAssume) Done() uint64 {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return 0
	}

	delete(r.supb.removeAssume, r.key)
	switch r.types {
	case types.StaticStrategy:
		st,ok := r.supb.staticsStrategies[r.key]

		// 被其他应用删除了
		if !ok {
			return 0
		}

		// 被写了
		if st.StrategyVersion > r.strategy.StrategyVersion {
			return 0
		}

		// 没有改变
		delete(r.supb.staticsStrategies, r.key)
	case types.DynamicStrategy:
		st,ok := r.supb.dynamicStrategies[r.key]

		// 被其他应用删除了
		if !ok {
			return 0
		}

		// 被写了
		if st.StrategyVersion > r.strategy.StrategyVersion {
			return 0
		}

		// 没有改变
		delete(r.supb.dynamicStrategies, r.key)
	}
	r.deleteAssume.Done()
	r.done = true

	return r.strategy.StrategyVersion
}

func (r *removeAssume) Cancel() {
	r.supb.lock.Lock()
	defer r.supb.lock.Unlock()

	if r.done {
		return
	}
	delete(r.supb.removeAssume, r.key)

	r.deleteAssume.Cancel()
	r.done = true
}