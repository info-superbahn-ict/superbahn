package model

import (
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/compose"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
)

const (
	LevelRead   = "read.only"
	LevelWrite1 = "write.level1"
	LevelWrite2 = "write.level2"
	LevelWrite3 = "write.level3"
)

type StrategySlave struct {
	SlaveTypes  string                   `json:"application_types"`
	SlaveLevel  string                   `json:"application_level"`
	SlaveKey    types.SupbApplicationKey `json:"application_key"`
	SlaveStatus string                   `json:"application_status"`
}

func (r *StrategySlave) DeepCopy() *StrategySlave {
	c := *r
	return &c
}

type SupbStrategyBaseInfo struct {
	StrategyVersion          uint64 `json:"strategy_version"`
	StrategyShortDescription string `json:"strategy_description_short"`
	StrategyDescription      string `json:"strategy_description_long"`
	StrategyName             string `json:"strategy_name"`
	StrategyType             string `json:"strategy_type"`
	StrategyStatus           string `json:"strategy_status"`

	StrategyKey       types.SupbStrategyKey   `json:"strategy_key"`
	StrategyKeyParent types.SupbStrategyKey   `json:"strategy_parent"`
	StrategyLevel     types.SupbStrategyLevel `json:"strategy_level"`

	StrategySlaves map[types.SupbApplicationKey]*StrategySlave `json:"strategy_slaves"`

	StrategyContainerNamePrefix string                `json:"strategy_container_name_prefix"`
	StrategyContainers          []*compose.Containers `json:"strategy_containers"`
	StrategyCompose             string      `json:"strategy_compose_yaml"`
	StrategyTags				[]types.Tag  `json:"strategy_tags"`
}

func (r *SupbStrategyBaseInfo) DeepCopy() *SupbStrategyBaseInfo {
	c := *r
	//c.StrategyCompose = r.StrategyCompose.DeepCopy()
	copy(c.StrategyTags,r.StrategyTags)

	ss := make(map[types.SupbApplicationKey]*StrategySlave, len(r.StrategySlaves))
	for k, v := range r.StrategySlaves {
		ss[k] = v.DeepCopy()
	}

	ct := make([]*compose.Containers, len(r.StrategyContainers))
	for k, v := range r.StrategyContainers {
		ct[k] = v.DeepCopy()
	}


	c.StrategySlaves = ss
	c.StrategyContainers = ct
	return &c
}

func MarshallCompose(st *SupbStrategyBaseInfo) error {
	cts,err := compose.FromCompose(st.StrategyCompose)
	if err !=nil {
		return err
	}

	st.StrategyContainers = cts
	return nil
}

func ValidateStaticStrategy(strategy *SupbStrategyBaseInfo) error {
	if strategy.StrategyKey == "" {
		return fmt.Errorf("invalid strategy key")
	}

	flag := true
	for _,v := range Levels() {
		if types.SupbStrategyLevel(v) == strategy.StrategyLevel {
			flag = false
		}
	}
	if flag {
		return fmt.Errorf("invalid strategy level")
	}

	return nil
}

func ValidateDynamicStrategy(strategy *SupbStrategyBaseInfo) error {

	return nil
}

func ValidateStrategySlave(slave *StrategySlave) error {

	return nil
}

func Levels() []string{
	return []string{
		LevelRead,LevelWrite1,LevelWrite2,LevelWrite3,
	}
}