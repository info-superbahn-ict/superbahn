package strategies

import (
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"
)

type option func(*SupbStrategies)

func WithRecorder(recorder supbapis.Recorder) option {
	return func(supbres *SupbStrategies) {
		supbres.recorder = recorder
	}
}