package strategies

import (
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"
	uuid "github.com/satori/go.uuid"
	"sync"
)

const (
	StaticStrategyRecorderKeyFormat  = "/repository/superbahnManager/strategy/static/%v"
	DynamicStrategyRecorderKeyFormat = "/repository/superbahnManager/strategy/dynamic/%v"
)

type SupbStrategies struct {
	lock *sync.RWMutex

	staticsStrategies map[types.SupbStrategyKey]*model.SupbStrategyBaseInfo
	dynamicStrategies map[types.SupbStrategyKey]*model.SupbStrategyBaseInfo

	//staticAssume  map[types.SupbStrategyKey]*putAssume
	//dynamicAssume map[types.SupbStrategyKey]*putAssume

	//staticLinksAssume  map[types.SupbStrategyKey]*linkAssume
	//dynamicLinksAssume map[types.SupbStrategyKey]*linkAssume
	//
	//staticUnLinksAssume  map[types.SupbStrategyKey]*unLinkAssume
	//dynamicUnLinksAssume map[types.SupbStrategyKey]*unLinkAssume

	removeAssume map[types.SupbStrategyKey]*removeAssume
	recorder     supbapis.Recorder
}

func NewSupbResManager() *SupbStrategies {
	return &SupbStrategies{
		lock:              &sync.RWMutex{},
		staticsStrategies: make(map[types.SupbStrategyKey]*model.SupbStrategyBaseInfo),
		dynamicStrategies: make(map[types.SupbStrategyKey]*model.SupbStrategyBaseInfo),

		//staticAssume:  make(map[types.SupbStrategyKey]*putAssume),
		//dynamicAssume: make(map[types.SupbStrategyKey]*putAssume),

		//staticLinksAssume:  make(map[types.SupbStrategyKey]*linkAssume),
		//dynamicLinksAssume: make(map[types.SupbStrategyKey]*linkAssume),
		//
		//staticUnLinksAssume:  make(map[types.SupbStrategyKey]*unLinkAssume),
		//dynamicUnLinksAssume: make(map[types.SupbStrategyKey]*unLinkAssume),

		removeAssume: make(map[types.SupbStrategyKey]*removeAssume),
	}
}

func (r *SupbStrategies) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}


func (r *SupbStrategies) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *SupbStrategies) ListStrategies(stTypes string) ([]*model.SupbStrategyBaseInfo, error) {
	r.lock.RLock()
	defer r.lock.RUnlock()

	switch stTypes {
	case types.StaticStrategy:
		list, index := make([]*model.SupbStrategyBaseInfo, len(r.staticsStrategies)), 0
		for _, v := range r.staticsStrategies {
			list[index], index = v.DeepCopy(), index+1
		}
		return list, nil
	case types.DynamicStrategy:
		list, index := make([]*model.SupbStrategyBaseInfo, len(r.dynamicStrategies)), 0
		for _, v := range r.dynamicStrategies {
			list[index], index = v.DeepCopy(), index+1
		}
		return list, nil
	default:
		return nil, fmt.Errorf("unknown type")
	}
}

func (r *SupbStrategies) GetStrategies(stTypes string, strategyKey types.SupbStrategyKey) (*model.SupbStrategyBaseInfo, error) {
	r.lock.RLock()
	defer r.lock.RUnlock()


	switch stTypes {
	case types.StaticStrategy:
		sst, ok := r.staticsStrategies[strategyKey]
		if !ok {
			return nil, types.StrategyIsNilError
		}
		return sst.DeepCopy(), nil
	case types.DynamicStrategy:
		dst, ok := r.dynamicStrategies[strategyKey]
		if !ok {
			return nil, types.StrategyIsNilError
		}
		return dst.DeepCopy(), nil
	default:
		return nil, fmt.Errorf("unknown type")
	}
}

/*
	put 是添加操作，如果已经存在则覆盖
*/
func (r *SupbStrategies) PutStrategiesAssume(stTypes string, strategy *model.SupbStrategyBaseInfo) (*putAssume, error) {
	r.lock.Lock()
	defer r.lock.Unlock()

	if r.recorder == nil {
		return nil, fmt.Errorf("recorder is nil")
	}

	assume := &putAssume{
		supb:     r,
		types:    stTypes,
		key:      strategy.StrategyKey,
		done:     false,
	}



	switch stTypes {
	case types.StaticStrategy:
		if err := model.ValidateStaticStrategy(strategy); err != nil {
			return nil, fmt.Errorf("validate ")
		}

		//if _,ok := r.staticAssume[strategy.StrategyKey]; ok {
		//	return nil,fmt.Errorf("the strategy are writing")
		//}

		if _,ok := r.dynamicStrategies[strategy.StrategyKey];ok {
			return nil,fmt.Errorf("the strategy key are exist in dynamic strategy")
		}

		if st,ok := r.staticsStrategies[strategy.StrategyKey]; ok {
			strategy.StrategyVersion = st.StrategyVersion + 1
		}else {
			strategy.StrategyVersion = 0
		}
		assume.strategy = strategy

		bts, err := json.Marshal(strategy)
		if err != nil {
			return nil, fmt.Errorf("unmarshall %v", err)
		}

		assume.writeAssume, err = r.recorder.WriteAssume(fmt.Sprintf(StaticStrategyRecorderKeyFormat, strategy.StrategyKey), bts)
		if err != nil {
			return nil, fmt.Errorf("recorder write %v", err)
		}

		//r.staticAssume[strategy.StrategyKey] = assume
	case types.DynamicStrategy:
		if err := model.ValidateDynamicStrategy(strategy); err != nil {
			return nil, fmt.Errorf("validate ")
		}

		//if _,ok := r.dynamicAssume[strategy.StrategyKey]; ok {
		//	return nil,fmt.Errorf("the strategy are writing")
		//}

		if _,ok := r.staticsStrategies[strategy.StrategyKey];ok {
			return nil,fmt.Errorf("the strategy key are exist in static strategy")
		}

		if st,ok := r.dynamicStrategies[strategy.StrategyKey]; ok {
			strategy.StrategyVersion = st.StrategyVersion + 1
		}else {
			strategy.StrategyVersion = 0
		}
		assume.strategy = strategy

		bts, err := json.Marshal(strategy)
		if err != nil {
			return nil, fmt.Errorf("unmarshall %v", err)
		}

		assume.writeAssume, err = r.recorder.WriteAssume(fmt.Sprintf(DynamicStrategyRecorderKeyFormat, strategy.StrategyKey), bts)
		if err != nil {
			return nil, fmt.Errorf("recorder write %v", err)
		}

		//r.dynamicAssume[strategy.StrategyKey] = assume
	default:
		return nil, fmt.Errorf("unknown type")
	}
	return assume, nil
}

func (r *SupbStrategies) RemoveStrategiesAssume(stTypes string, strategyKey types.SupbStrategyKey) (*removeAssume, error) {
	r.lock.Lock()
	defer r.lock.Unlock()

	if r.recorder == nil {
		return nil, fmt.Errorf("recorder is nil")
	}

	assume := &removeAssume{
		supb:     r,
		types:    stTypes,
		key:      strategyKey,
		done:     false,
	}

	var (
		ok bool
		err error
	)
	switch stTypes {
	case types.StaticStrategy:
		assume.strategy ,ok = r.staticsStrategies[strategyKey]
		if !ok {
			return nil,fmt.Errorf("the strategy are not exit")
		}

		assume.deleteAssume, err = r.recorder.DeleteAssume(fmt.Sprintf(StaticStrategyRecorderKeyFormat, strategyKey))
		if err != nil {
			return nil, fmt.Errorf("recorder write %v", err)
		}

		r.removeAssume[strategyKey] = assume
		//delete(r.staticsStrategies, strategyKey)
	case types.DynamicStrategy:
		assume.strategy ,ok = r.dynamicStrategies[strategyKey]
		if !ok {
			return nil,fmt.Errorf("the strategy are not exit")
		}

		assume.deleteAssume, err = r.recorder.DeleteAssume(fmt.Sprintf(DynamicStrategyRecorderKeyFormat, strategyKey))
		if err != nil {
			return nil, fmt.Errorf("recorder write %v", err)
		}

		r.removeAssume[strategyKey] = assume
		//delete(r.dynamicStrategies, strategyKey)
	default:
		return nil, fmt.Errorf("unknown type")
	}
	return assume, nil
}

func (r *SupbStrategies) LinkSlaveAssume(strategyTypes string, strategyKey types.SupbStrategyKey, applicationKey types.SupbApplicationKey, slave *model.StrategySlave) (*linkAssume,error) {
	r.lock.Lock()
	defer r.lock.Unlock()

	if r.recorder == nil {
		return nil,fmt.Errorf("recorder is nil")
	}

	if err := model.ValidateStrategySlave(slave); err != nil {
		return nil,fmt.Errorf("validate ")
	}

	assume := &linkAssume{
		supb:  r,
		types: strategyTypes,
		key:   strategyKey,
		done:  false,
	}



	switch strategyTypes {
	case types.StaticStrategy:
		// do check
		sst, ok := r.staticsStrategies[strategyKey]
		if !ok {
			return nil,fmt.Errorf("strategy is not exist")
		}
		if _, ok = sst.StrategySlaves[applicationKey]; ok {
			return nil,fmt.Errorf("strategy is link to %v", applicationKey)
		}
		//if lk, ok := r.staticLinksAssume[strategyKey]; ok {
		//	return nil,fmt.Errorf("strategy are link to %v", lk.strategy.StrategyKey)
		//}

		// config assumes
		sstCopy := sst.DeepCopy()
		sstCopy.StrategyVersion++
		sstCopy.StrategySlaves[applicationKey] = slave.DeepCopy()
		assume.strategy = sstCopy

		bts, err := json.Marshal(sstCopy)
		if err != nil {
			return nil,fmt.Errorf("marshall %v", err)
		}
		assume.writeAssume, err = r.recorder.WriteAssume(fmt.Sprintf(StaticStrategyRecorderKeyFormat, strategyKey), bts)
		if err != nil {
			return nil,fmt.Errorf("recorder write %v", err)
		}

		//r.staticLinksAssume[strategyKey] = assume
	case types.DynamicStrategy:

		dst, ok := r.dynamicStrategies[strategyKey]
		if !ok {
			return nil,fmt.Errorf("strategy is not exist")
		}


		if _, ok = dst.StrategySlaves[applicationKey]; ok {
			return nil,fmt.Errorf("strategy applicationKey %v is exist", applicationKey)
		}

		//if lk, ok := r.dynamicLinksAssume[strategyKey]; ok {
		//	return nil,fmt.Errorf("strategy are link to %v", lk.strategy.StrategyKey)
		//}



		dstCopy := dst.DeepCopy()
		dstCopy.StrategyVersion++
		dstCopy.StrategySlaves[applicationKey] = slave.DeepCopy()
		assume.strategy = dstCopy

		//fmt.Printf("here4 %v\n",dstCopy)

		bts, err := json.Marshal(dstCopy)
		if err != nil {
			return nil,fmt.Errorf("marshall %v", err)
		}


		//err = r.recorder.Write(fmt.Sprintf(StaticStrategyRecorderKeyFormat, strategyKey), bts)
		//if err != nil {
		//	return nil,fmt.Errorf("recorder write %v", err)
		//}

		assume.writeAssume, err = r.recorder.WriteAssume(fmt.Sprintf(StaticStrategyRecorderKeyFormat, strategyKey), bts)
		if err != nil {
			return nil,fmt.Errorf("recorder write %v", err)
		}

		//r.dynamicLinksAssume[strategyKey] = assume
	default:
		return nil,fmt.Errorf("unknown type")
	}
	return assume,nil
}

func (r *SupbStrategies) UnLinkSlaveAssume(strategyTypes string, strategyKey types.SupbStrategyKey, appKey types.SupbApplicationKey) (*unLinkAssume,error) {
	r.lock.Lock()
	defer r.lock.Unlock()

	if r.recorder == nil {
		return nil,fmt.Errorf("recorder is nil")
	}

	assume := &unLinkAssume{
		supb:  r,
		types: strategyTypes,
		key:   strategyKey,
		done:  false,
	}

	fmt.Printf("%v %v %v",strategyTypes,strategyKey,appKey)

	switch strategyTypes {
	case types.StaticStrategy:
		sst, ok := r.staticsStrategies[strategyKey]
		if !ok {
			return nil,fmt.Errorf("strategy is not exist")
		}

		if _, ok = sst.StrategySlaves[appKey]; !ok {
			return nil,fmt.Errorf("static don't have strategy")
		}

		//if _ , ok = r.staticUnLinksAssume[appKey]; ok {
		//	return nil,fmt.Errorf("strategy are unlinking")
		//}

		stCopy := sst.DeepCopy()
		stCopy.StrategyVersion++
		delete(stCopy.StrategySlaves, appKey)
		assume.strategy = stCopy


		bts, err := json.Marshal(stCopy)
		if err != nil {
			return nil,fmt.Errorf("marshall %v", err)
		}

		assume.writeAssume,err = r.recorder.WriteAssume(fmt.Sprintf(StaticStrategyRecorderKeyFormat, strategyKey), bts)
		if err != nil {
			return nil,fmt.Errorf("recorder write %v", err)
		}

		//r.staticUnLinksAssume[strategyKey] = assume
	case types.DynamicStrategy:
		dst, ok := r.dynamicStrategies[strategyKey]
		if !ok {
			return nil,fmt.Errorf("strategy is not exist")
		}
		if _, ok = dst.StrategySlaves[appKey]; !ok {
			return nil,fmt.Errorf("dynamic don't have strategy")
		}
		stCopy := dst.DeepCopy()
		stCopy.StrategyVersion++
		delete(stCopy.StrategySlaves, appKey)
		assume.strategy = stCopy

		bts, err := json.Marshal(stCopy)
		if err != nil {
			return nil,fmt.Errorf("marshall %v", err)
		}

		assume.writeAssume,err = r.recorder.WriteAssume(fmt.Sprintf(DynamicStrategyRecorderKeyFormat, appKey), bts)
		if err != nil {
			return nil,fmt.Errorf("recorder write %v", err)
		}

		//r.dynamicUnLinksAssume[strategyKey] = assume
	default:
		return nil,fmt.Errorf("unknown type")
	}
	return assume,nil
}

func (r *SupbStrategies) DeriveKey(parentKey types.SupbStrategyKey) types.SupbStrategyKey {
	r.lock.RLock()
	defer r.lock.RUnlock()

	for tryTime:=3; tryTime >0 ; tryTime-- {
		key := types.SupbStrategyKey(uuid.NewV4().String())
		if _,ok := r.staticsStrategies[key];!ok {
			if _,ok := r.dynamicStrategies[key];!ok {
				return key
			}
		}
	}
	return ""
}