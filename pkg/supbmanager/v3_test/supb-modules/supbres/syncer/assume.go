package syncer

import (
	"context"
	"encoding/json"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications"
	m2 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies"
	m1 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/coreos/etcd/clientv3"
	"github.com/coreos/etcd/mvcc/mvccpb"
)

type CreateAssume struct {
	ctx context.Context
	appInformer clientv3.WatchChan
	stInformer clientv3.WatchChan

	cancel context.CancelFunc

	logger log.Logger

	stMgr  *strategies.SupbStrategies
	appMgr *applications.SupbApplications

	appKey types.SupbApplicationKey
	appType string
	strategyKey types.SupbStrategyKey
	strategyType string
	done bool
}


func (r *CreateAssume) Done() uint64 {

	if r.done {
		return 0
	}

	var appVersion, stVersion uint64 = 0,0
	go func() {
		for {
			select {
			case <-r.ctx.Done():
				r.logger.Infof("link cancel")
				return
			case aInfo := <-r.appInformer:
				for _, event := range aInfo.Events {
					switch event.Type {
					case mvccpb.PUT:
						r.logger.Infof("DETECT APPLICATION %v PUT", r.appKey)

						app := &m2.SupbApplicationBaseInfo{}
						if err := json.Unmarshal(event.Kv.Value, app); err != nil {
							r.logger.Errorf("unmarshall %v", err)
							continue
						}

						// 检测到写入的数据版本，比
						if app.ApplicationVersion == appVersion {
							continue
						}

						st, err := r.stMgr.GetStrategies(r.strategyType, r.strategyKey)
						if err != nil {
							r.logger.Errorf("%v", err)
							continue
						}

						// 如果app修改了连接，那么strategy也需要删除连接
						deleted := false
						switch r.strategyType {
						case types.StaticStrategy:
							// 删除连接
							// 检查app不能修改连接中策略的信息状态
							if _, ok := app.ApplicationStaticStrategies[r.strategyKey]; !ok {
								delete(st.StrategySlaves, r.appKey)
								deleted = true
							}
						case types.DynamicStrategy:
							if _, ok := app.ApplicationDynamicStrategies[r.strategyKey]; !ok {
								delete(st.StrategySlaves, r.appKey)
								deleted = true
							}
						}

						if !deleted {
							// 同步状态
							if _,ok := st.StrategySlaves[r.appKey];ok {
								st.StrategySlaves[r.appKey].SlaveKey = app.ApplicationKey
								st.StrategySlaves[r.appKey].SlaveStatus = app.ApplicationStatus
								st.StrategySlaves[r.appKey].SlaveLevel = app.ApplicationLevel
								st.StrategySlaves[r.appKey].SlaveTypes = app.ApplicationType
							}else{
								st.StrategySlaves[r.appKey] = &m1.StrategySlave{
									SlaveKey: app.ApplicationKey,
									SlaveStatus: app.ApplicationStatus,
									SlaveLevel: app.ApplicationLevel,
									SlaveTypes: app.ApplicationType,
								}
							}
						}
						// 更新策略
						ass, err := r.stMgr.PutStrategiesAssume(st.StrategyType, st)
						if err != nil {
							r.logger.Errorf("put strategy %v", err)
							continue
						}
						stVersion = ass.Done()

						r.logger.Infof("AUTO UPDATE STRATEGY %v", r.strategyKey)

						if deleted {
							r.logger.Infof("AUTO BREAK LINK")
							return
						}
					case mvccpb.DELETE:
						r.logger.Infof("DETECT APPLICATION %v PUT", r.appKey)

						st, err := r.stMgr.GetStrategies(r.strategyType, r.strategyKey)
						if err != nil {
							r.logger.Errorf("get strategy %v", err)
							continue
						}

						delete(st.StrategySlaves, r.appKey)

						// 更新策略
						ass, err := r.stMgr.PutStrategiesAssume(st.StrategyType, st)
						if err != nil {
							r.logger.Errorf("put strategy %v", err)
							continue
						}
						stVersion = ass.Done()
						r.logger.Infof("AUTO BREAK LINK %v", r.strategyKey)
						return
					}
				}
			case sInfo := <-r.stInformer:
				for _, event := range sInfo.Events {
					switch event.Type {
					case mvccpb.PUT:
						r.logger.Infof("DETECT STRATEGY %v PUT", r.strategyKey)

						st := &m1.SupbStrategyBaseInfo{}
						if err := json.Unmarshal(event.Kv.Value, st); err != nil {
							r.logger.Errorf("unmarshall %v", err)
							continue
						}

						if st.StrategyVersion == stVersion {
							continue
						}

						app, err := r.appMgr.GetApplication(r.appType, r.appKey)
						if err != nil {
							r.logger.Errorf("%v", err)
							continue
						}

						// 如果策略删除了连接，那么app也需要删除连接
						deleted := false
						if _, ok := st.StrategySlaves[app.ApplicationKey]; !ok {
							switch r.strategyType {
							case types.StaticStrategy:
								delete(app.ApplicationStaticStrategies, st.StrategyKey)
							case types.DynamicStrategy:
								delete(app.ApplicationDynamicStrategies, st.StrategyKey)
							}
							deleted = true
						}

						if !deleted {
							// 同步状态
							switch r.strategyType {
							case types.StaticStrategy:
								if _,ok := app.ApplicationStaticStrategies[st.StrategyKey]; ok {
									app.ApplicationStaticStrategies[st.StrategyKey].StrategyKey = st.StrategyKey
									app.ApplicationStaticStrategies[st.StrategyKey].StrategyLevel = st.StrategyLevel
									app.ApplicationStaticStrategies[st.StrategyKey].StrategyStatus = st.StrategyStatus
									app.ApplicationStaticStrategies[st.StrategyKey].StrategyTypes = st.StrategyType
								}else{
									app.ApplicationStaticStrategies[st.StrategyKey] = &m2.ApplicationController{
										StrategyKey: st.StrategyKey,
										StrategyLevel: st.StrategyLevel,
										StrategyTypes: st.StrategyType,
										StrategyStatus: st.StrategyStatus,
									}
								}
							case types.DynamicStrategy:
								if _,ok := app.ApplicationDynamicStrategies[st.StrategyKey]; ok {
									app.ApplicationDynamicStrategies[st.StrategyKey].StrategyKey = st.StrategyKey
									app.ApplicationDynamicStrategies[st.StrategyKey].StrategyLevel = st.StrategyLevel
									app.ApplicationDynamicStrategies[st.StrategyKey].StrategyStatus = st.StrategyStatus
									app.ApplicationDynamicStrategies[st.StrategyKey].StrategyTypes = st.StrategyType
								}else{
									app.ApplicationDynamicStrategies[st.StrategyKey] = &m2.ApplicationController{
										StrategyKey: st.StrategyKey,
										StrategyLevel: st.StrategyLevel,
										StrategyTypes: st.StrategyType,
										StrategyStatus: st.StrategyStatus,
									}
								}
							}
						}
						// 更新策略
						ass, err := r.appMgr.PutApplicationsAssume(app.ApplicationType, app)
						if err != nil {
							r.logger.Errorf("put application %v", err)
							continue
						}
						appVersion = ass.Done()
						r.logger.Infof("AUTO UPDATE APPLICATION %v", r.appKey)

						if deleted {
							r.logger.Infof("AUTO BREAK LINK")
							return
						}
					case mvccpb.DELETE:
						r.logger.Infof("DETECT STRATEGY %v PUT", r.strategyKey)

						app, err := r.appMgr.GetApplication(r.appType, types.SupbApplicationKey(r.appKey))
						if err != nil {
							r.logger.Errorf("get application %v", err)
							continue
						}

						switch r.strategyType {
						case types.StaticStrategy:
							delete(app.ApplicationStaticStrategies, types.SupbStrategyKey(r.strategyKey))
						case types.DynamicStrategy:
							delete(app.ApplicationDynamicStrategies, types.SupbStrategyKey(r.strategyKey))
						}


						// 更新策略
						ass, err := r.appMgr.PutApplicationsAssume(app.ApplicationType, app)
						if err != nil {
							r.logger.Errorf("put strategy %v", err)
							continue
						}
						appVersion = ass.Done()
						r.logger.Infof("AUTO BREAK LINK %v", r.strategyKey)
						return
					}
				}
			}
		}
	}()

	r.done = true

	return 0
}

func (r *CreateAssume) Cancel() {


	if r.done {
		return
	}

	r.cancel()

	r.done = true
}