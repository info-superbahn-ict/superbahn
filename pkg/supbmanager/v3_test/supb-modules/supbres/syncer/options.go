package syncer

import (
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"
)

type option func(resources *SyncMod)

func WithAppManager(appMgr *applications.SupbApplications) option {
	return func(mod *SyncMod) {
		mod.appMgr = appMgr
	}
}


func WithStrategyManager(stMgr *strategies.SupbStrategies) option {
	return func(mod *SyncMod) {
		mod.stMgr = stMgr
	}
}

func WithRecorder(recorder supbapis.Recorder) option {
	return func(mod *SyncMod) {
		mod.recorder = recorder
	}
}
