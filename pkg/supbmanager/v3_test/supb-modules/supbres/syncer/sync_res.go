package syncer

import (
	"context"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications"
	m2 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/applications/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies"
	m1 "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/strategies/model"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/types"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
)

type SyncMod struct {
	ctx    context.Context
	prefix string
	logger log.Logger

	stMgr  *strategies.SupbStrategies
	appMgr *applications.SupbApplications

	recorder supbapis.Recorder


	strategyPutHandler map[types.SupbStrategyKey]map[types.SupbApplicationKey]func(info *m1.SupbStrategyBaseInfo) bool

	strategyDelHandler map[types.SupbStrategyKey]map[types.SupbApplicationKey]func(info *m1.SupbStrategyBaseInfo) bool

	applicationPutHandler map[types.SupbApplicationKey]map[types.SupbStrategyKey]func(ino *m2.SupbApplicationBaseInfo) bool

	applicationDelHandler map[types.SupbApplicationKey]map[types.SupbStrategyKey]func(ino *m2.SupbApplicationBaseInfo) bool
}

func NewSyncMod(ctx context.Context, prefix string) *SyncMod {
	return &SyncMod{
		ctx:    ctx,
		prefix: prefix,
		logger: log.NewLogger(prefix),
	}
}

func (r *SyncMod) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}

}

func (r *SyncMod) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}

	r.applicationPutHandler = make(map[types.SupbApplicationKey]map[types.SupbStrategyKey]func(ino *m2.SupbApplicationBaseInfo) bool)
	r.applicationDelHandler = make(map[types.SupbApplicationKey]map[types.SupbStrategyKey]func(ino *m2.SupbApplicationBaseInfo) bool)

	r.strategyPutHandler = make(map[types.SupbStrategyKey]map[types.SupbApplicationKey]func(ino *m1.SupbStrategyBaseInfo) bool)
	r.strategyDelHandler = make(map[types.SupbStrategyKey]map[types.SupbApplicationKey]func(ino *m1.SupbStrategyBaseInfo) bool)
}

func (r *SyncMod) Close() error {
	r.logger.Infof("%v closed", r.prefix)
	return nil
}

func (r *SyncMod) StrategyPutInformer(strategy *m1.SupbStrategyBaseInfo) {
	if _, ok := r.strategyPutHandler[strategy.StrategyKey]; !ok {
		return
	}
	for k ,v := range r.strategyPutHandler[strategy.StrategyKey]{
		if v(strategy) {
			delete(r.strategyPutHandler[strategy.StrategyKey], k)
		}
	}
}

func (r *SyncMod) StrategyDelInformer(strategy *m1.SupbStrategyBaseInfo) {
	if _, ok := r.strategyDelHandler[strategy.StrategyKey]; !ok {
		return
	}
	for k ,v := range r.strategyDelHandler[strategy.StrategyKey]{
		if v(strategy) {
			delete(r.strategyDelHandler[strategy.StrategyKey], k)
		}
	}
}

func (r *SyncMod) ApplicationPutInformer(app *m2.SupbApplicationBaseInfo) {
	if _, ok := r.applicationPutHandler[app.ApplicationKey]; !ok {
		return
	}
	for k ,v := range r.applicationPutHandler[app.ApplicationKey]{
		if v(app) {
			delete(r.applicationPutHandler[app.ApplicationKey], k)
		}
	}
}

func (r *SyncMod) ApplicationDelInformer(app *m2.SupbApplicationBaseInfo) {
	if _, ok := r.applicationDelHandler[app.ApplicationKey]; !ok {
		return
	}
	for k ,v := range r.applicationDelHandler[app.ApplicationKey]{
		if v(app) {
			delete(r.applicationDelHandler[app.ApplicationKey], k)
		}
	}
}

func (r *SyncMod) CreateLinkSyncAssume(appType string, appKey types.SupbApplicationKey, strategyType string, strategyKey types.SupbStrategyKey){
	// 如果
	if _, ok := r.strategyPutHandler[strategyKey]; !ok {
		r.strategyPutHandler[strategyKey] = make(map[types.SupbApplicationKey]func(info *m1.SupbStrategyBaseInfo) bool)
		// put策略，应用做什么反应？
	}

	r.strategyPutHandler[strategyKey][appKey] = func(st *m1.SupbStrategyBaseInfo) bool {
		// 找到应用
		r.logger.Debugf("handler strategy put")
		app, err := r.appMgr.GetApplication(appType, appKey)
		if err != nil {
			r.logger.Warnf("handler %v", err)
			return true
		}

		change, deleted := false, false

		//r.logger.Infof("%v \n\n\n%v",st,app)

		if _, ok := st.StrategySlaves[app.ApplicationKey]; !ok {
			// 如果策略删除了连接，应用也要删除
			switch st.StrategyType {
			case types.StaticStrategy:
				if _, ok := app.ApplicationStaticStrategies[st.StrategyKey]; ok {
					delete(app.ApplicationStaticStrategies, st.StrategyKey)
					deleted = true
					change = true
				}
			case types.DynamicStrategy:
				if _, ok := app.ApplicationDynamicStrategies[st.StrategyKey]; ok {
					delete(app.ApplicationDynamicStrategies, st.StrategyKey)
					deleted = true
					change = true
				}
			}
		}else {
			// 如果st更改了,app也要状态同步一下
			switch st.StrategyType {
			case types.StaticStrategy:
				if v, ok := app.ApplicationStaticStrategies[st.StrategyKey]; ok {
					if v.StrategyStatus != st.StrategyStatus {
						app.ApplicationStaticStrategies[st.StrategyKey].StrategyStatus = st.StrategyStatus
						change = true
					}
					if v.StrategyTypes != st.StrategyType {
						app.ApplicationStaticStrategies[st.StrategyKey].StrategyTypes = st.StrategyType
						change = true
					}
					if v.StrategyLevel != st.StrategyLevel {
						app.ApplicationStaticStrategies[st.StrategyKey].StrategyLevel = st.StrategyLevel
						change = true
					}
				}else{
					app.ApplicationStaticStrategies[st.StrategyKey] = &m2.ApplicationController{
						StrategyKey:    st.StrategyKey,
						StrategyLevel:  st.StrategyLevel,
						StrategyTypes:  st.StrategyType,
						StrategyStatus: st.StrategyStatus,
					}
					change = true
				}
			case types.DynamicStrategy:
				if v, ok := app.ApplicationDynamicStrategies[st.StrategyKey]; ok {
					if v.StrategyStatus != st.StrategyStatus {
						app.ApplicationDynamicStrategies[st.StrategyKey].StrategyStatus = st.StrategyStatus
						change = true
					}
					if v.StrategyTypes != st.StrategyType {
						app.ApplicationDynamicStrategies[st.StrategyKey].StrategyTypes = st.StrategyType
						change = true
					}
					if v.StrategyLevel != st.StrategyLevel {
						app.ApplicationDynamicStrategies[st.StrategyKey].StrategyLevel = st.StrategyLevel
						change = true
					}
				}else{
					app.ApplicationDynamicStrategies[st.StrategyKey] = &m2.ApplicationController{
						StrategyKey:    st.StrategyKey,
						StrategyLevel:  st.StrategyLevel,
						StrategyTypes:  st.StrategyType,
						StrategyStatus: st.StrategyStatus,
					}
					change = true
				}
			}
		}

		if change {
			ass, err := r.appMgr.PutApplicationsAssume(appType, app)
			if err != nil {
				r.logger.Errorf("handle %v", err)
			} else {
				ass.Done()
			}
		}
		return deleted
	}

	if _, ok := r.strategyDelHandler[strategyKey]; !ok {
		r.strategyDelHandler[strategyKey] = make(map[types.SupbApplicationKey]func(info *m1.SupbStrategyBaseInfo) bool)
	}

	r.strategyDelHandler[strategyKey][appKey] = func(st *m1.SupbStrategyBaseInfo) bool {
		// 找到应用
		r.logger.Debugf("handler strategy del")
		app, err := r.appMgr.GetApplication(appType, appKey)
		if err != nil {
			r.logger.Infof("handler %v", err)
			return true
		}

		change := false

		// 策略被删除了，所以应用也要连接，如果它存在的话
		switch st.StrategyType {
		case types.StaticStrategy:
			if _, ok := app.ApplicationStaticStrategies[st.StrategyKey]; ok {
				delete(app.ApplicationStaticStrategies, st.StrategyKey)
				change = true
			}
		case types.DynamicStrategy:
			if _, ok := app.ApplicationDynamicStrategies[st.StrategyKey]; ok {
				delete(app.ApplicationDynamicStrategies, st.StrategyKey)
				change = true
			}
		}

		if change {
			ass, err := r.appMgr.PutApplicationsAssume(appType, app)
			if err != nil {
				r.logger.Errorf("handle %v", err)
			} else {
				ass.Done()
			}
		}

		return true
	}

	if _, ok := r.applicationPutHandler[appKey]; !ok {
		r.applicationPutHandler[appKey] = make(map[types.SupbStrategyKey]func(info *m2.SupbApplicationBaseInfo) bool)
	}

	r.applicationPutHandler[appKey][strategyKey] = func(app *m2.SupbApplicationBaseInfo) bool {
		// 要找到策略
		r.logger.Debugf("handler application put")
		st, err := r.stMgr.GetStrategies(strategyType, strategyKey)
		if err != nil {
			r.logger.Warnf("handler %v", err)
			return true
		}

		change, deleted := false, false
		// 如果应用删除了连接，策略也要删除

		switch st.StrategyType {
		case types.StaticStrategy:
			if _, ok := app.ApplicationStaticStrategies[st.StrategyKey]; !ok {
				// 如过 app 删除了连接

				// 如果 st 还存在该连接，则删除它，否则不管
				if _, ok := st.StrategySlaves[app.ApplicationKey]; ok {
					delete(st.StrategySlaves, app.ApplicationKey)
					deleted = true
					change = true
				}
			} else {
				// app 没有删除连接

				// 如果 st 存在 则更新，不存在则创建
				if v, ok := st.StrategySlaves[app.ApplicationKey]; ok {
					if v.SlaveStatus != app.ApplicationStatus {
						st.StrategySlaves[app.ApplicationKey].SlaveStatus = app.ApplicationStatus
						change = true
					}
					if v.SlaveTypes != app.ApplicationType {
						st.StrategySlaves[app.ApplicationKey].SlaveTypes = app.ApplicationType
						change = true
					}
					if v.SlaveLevel != app.ApplicationLevel {
						st.StrategySlaves[app.ApplicationKey].SlaveLevel = app.ApplicationLevel
						change = true
					}
				} else {
					st.StrategySlaves[app.ApplicationKey] = &m1.StrategySlave{
						SlaveKey:    app.ApplicationKey,
						SlaveStatus: app.ApplicationStatus,
						SlaveLevel:  app.ApplicationLevel,
						SlaveTypes:  app.ApplicationType,
					}
					change = true
				}
			}

		case types.DynamicStrategy:
			if _, ok := app.ApplicationDynamicStrategies[st.StrategyKey]; !ok {
				// 如过 app 删除了连接

				// 如果 st 还存在该连接，则删除它，否则不管
				if _, ok := st.StrategySlaves[app.ApplicationKey]; ok {
					delete(st.StrategySlaves, app.ApplicationKey)
					deleted = true
					change = true
				}

			} else {
				// app 没有删除连接

				// 如果 st 存在 则更新，不存在则创建
				if v, ok := st.StrategySlaves[app.ApplicationKey]; ok {
					if v.SlaveStatus != app.ApplicationStatus {
						st.StrategySlaves[app.ApplicationKey].SlaveStatus = app.ApplicationStatus
						change = true
					}
					if v.SlaveTypes != app.ApplicationType {
						st.StrategySlaves[app.ApplicationKey].SlaveTypes = app.ApplicationType
						change = true
					}
					if v.SlaveLevel != app.ApplicationLevel {
						st.StrategySlaves[app.ApplicationKey].SlaveLevel = app.ApplicationLevel
						change = true
					}
				} else {
					st.StrategySlaves[app.ApplicationKey] = &m1.StrategySlave{
						SlaveKey:    app.ApplicationKey,
						SlaveStatus: app.ApplicationStatus,
						SlaveLevel:  app.ApplicationLevel,
						SlaveTypes:  app.ApplicationType,
					}
					change = true
				}
			}
		}

		if change {
			ass, err := r.stMgr.PutStrategiesAssume(strategyType, st)
			if err != nil {
				r.logger.Errorf("handle %v", err)
			} else {
				ass.Done()
			}
		}
		return deleted
	}

	if _, ok := r.applicationDelHandler[appKey]; !ok {
		r.applicationDelHandler[appKey] = make(map[types.SupbStrategyKey]func(info *m2.SupbApplicationBaseInfo) bool)
	}

	r.applicationDelHandler[appKey][strategyKey] =  func(app *m2.SupbApplicationBaseInfo) bool {
		// 找到策略
		r.logger.Debugf("handler application del")
		st, err := r.stMgr.GetStrategies(strategyType, strategyKey)
		if err != nil {
			r.logger.Infof("handler %v", err)
			return true
		}

		change := false

		// 应用被删除了，所以策略也要删除，如果它存在的话
		if _,ok := st.StrategySlaves[app.ApplicationKey]; ok {
			delete(st.StrategySlaves, app.ApplicationKey)
			change = true
		}

		if change {
			ass, err := r.stMgr.PutStrategiesAssume(strategyType, st)
			if err != nil {
				r.logger.Errorf("handle %v", err)
			} else {
				ass.Done()
			}
		}

		return true
	}

	r.logger.Infof("LINK CREATED")
}
