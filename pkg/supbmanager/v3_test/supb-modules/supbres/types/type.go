package types

import "errors"

const (
	StaticApplication  = "static"
	DynamicApplication = "dynamic"

	StaticStrategy  = "static"
	DynamicStrategy = "dynamic"
)

type SupbApplicationKey string
type SupbStrategyLevel string
type SupbStrategyKey string
type SupbContainerGuid string

var (
	StrategyIsNilError    = errors.New("strategy is nil")
	ApplicationIsNilError = errors.New("application is nil")
)


type Tag struct {
	Key   string      `json:"key"`
	Type  string      `json:"type"`
	Value string `json:"value"`
}
