package supbrpc

type option func(resources *SupbRpc)


func WithGUID(guid string) option {
	return func(runner *SupbRpc) {
		runner.Guid = guid
	}
}