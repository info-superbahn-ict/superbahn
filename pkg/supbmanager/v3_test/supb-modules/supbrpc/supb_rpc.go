package supbrpc

import (
	"context"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	rpcConfigFile = ".config.file"
	rpcManagerGuid = ".guid"

	debug = ".debug"
)

type SupbRpc struct {
	ctx context.Context
	prefix string
	Guid string

	rpc nervous.Controller
	rpcCfg string

	logger log.Logger

	//debug bool
}

func NewSupbRpc(ctx context.Context,prefix string) *SupbRpc{
	return &SupbRpc{
		ctx:    ctx,
		prefix: prefix,
		logger: log.NewLogger(prefix),
	}
}

func (r *SupbRpc) InitFlags(flags *pflag.FlagSet) {
	flags.String(r.prefix+rpcConfigFile,"../../../config/nervous_config.json","rpc config file .yaml")
	flags.String(r.prefix+rpcManagerGuid,define.RPCCommonGuidOfManager,"rpc config file .yaml")
	//flags.Bool(r.prefix+rpcManagerGuid,false,"rpc config file .yaml")
}

func (r *SupbRpc)ViperConfig(viper *viper.Viper)  {

}

func (r *SupbRpc) InitViper(viper *viper.Viper) {
	r.rpcCfg = viper.GetString(r.prefix+rpcConfigFile)
	r.Guid = viper.GetString(r.prefix+rpcManagerGuid)
}

func (r *SupbRpc) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *SupbRpc) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}

	rpc,err := kafkaNervous.NewNervous(r.ctx,r.rpcCfg,r.Guid)
	if err !=nil {
		r.logger.Errorf("new rpc %v",err)
		return
	}

	r.rpc = rpc
	r.logger.Infof("%v initialized",r.prefix)
}

func (r *SupbRpc) Close() error {
	r.logger.Infof("%v closed",r.prefix)
	return r.rpc.Close()
}


func (r *SupbRpc) RPC() nervous.Controller {
	return r.rpc
}