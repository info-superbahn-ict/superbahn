package supbrunner

import (
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
)

type option func(resources *SupbRunner)

func WithRPC(rpc nervous.Controller) option {
	return func(runner *SupbRunner) {
		runner.rpc = rpc
	}
}