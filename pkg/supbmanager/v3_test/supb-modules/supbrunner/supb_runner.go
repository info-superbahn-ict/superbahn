package supbrunner

import (
	"context"
	"encoding/json"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbagent/clireq"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/compose"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"time"
)

type SupbRunner struct {
	ctx    context.Context
	prefix string
	logger log.Logger

	rpc nervous.Controller
}

func NewSupbRunner(ctx context.Context, prefix string) *SupbRunner {
	return &SupbRunner{
		logger: log.NewLogger(prefix),
		prefix: prefix,
		ctx:    ctx,
	}
}

func (r *SupbRunner) InitFlags(flags *pflag.FlagSet) {

}

func (r *SupbRunner) ViperConfig(viper *viper.Viper) {

}

func (r *SupbRunner) InitViper(viper *viper.Viper) {

}

func (r *SupbRunner) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *SupbRunner) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}

	//rpc,err := kafkaNervous.NewNervous(r.ctx,r.rpcCfg,r.Guid)
	//if err !=nil {
	//	r.logger.Errorf("new rpc %v",err)
	//	return
	//}

	//r.rpc = rpc
	r.logger.Infof("%v initialized", r.prefix)
}

func (r *SupbRunner) Close() error {
	r.logger.Infof("%v closed", r.prefix)
	return nil
}

func (r *SupbRunner) Run(guid string, container *compose.Containers) (*compose.RunningBaseInfo, error) {
	req := &clireq.ClientRequest{
		Guid:          container.Guid,
		ContainerId:   container.Cid,
		ImageTag:      container.Image,
		ContainerName: container.Name,
		Runtime:       container.Runtime,
		Workdir:       container.Workdir,
		Envs:          container.EnvsKey,
		Ports:         container.PortMap,
		Cmds:          container.Cmd,
		Tags:          container.Tags,
		//新增 SERVE gitlab-runner字段
		ImagePullPolicy:	container.ImagePullPolicy,
		Lifecycle: 			container.Lifecycle,
		LivenessProbe:		container.LivenessProbe,
		ReadinessProbe:		container.ReadinessProbe,
		Reasources:			container.Reasources,
		SecurityContext:	container.SecurityContext,
		TerminationMessagePath:	 container.TerminationMessagePath,
		TerminationMessagePolicy: container.TerminationMessagePolicy,
		VolumeMounts:	container.VolumeMounts,
	}
	bts, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}

	resp, err := r.rpc.RPCCallCustom(guid, 100, 500, define.RPCFunctionNameOfAgentForCreateObject, string(bts))
	if err != nil {
		return nil, err
	}

	//r.logger.Infof("recieve %v",resp)

	info := &clireq.ClientResponse{}
	if err := json.Unmarshal([]byte(resp.(string)), info); err != nil {
		return nil, err
	}

	for _, v := range container.Tags {
		if v.Key == "cpu.set" {
			for i := 0; i < 10; i++ {
				_, err := r.rpc.RPCCallCustom(info.Guid, 100, 500, define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuSets, v.Value)
				if err != nil {
					r.logger.Errorf("set container core %v failed %v", info.Guid,err)
					time.Sleep(time.Second / 2)
				}else {
					break
				}
			}
			r.logger.Infof("set container core %v succeed", info.Guid)
		}
		if v.Key == "cpu.ratio" {
			for i := 0; i < 10; i++ {
				_, err := r.rpc.RPCCallCustom(info.Guid, 100, 500, define.RPCFunctionNameOfObjectRunningOnAgentForSetCpuRatioShare, v.Value)
				if err != nil {
					r.logger.Errorf("set container shares %v failed %v", info.Guid, err)
					time.Sleep(time.Second / 2)
				} else {
					r.logger.Infof("set container shares %v succeed", info.Guid)
					break
				}
			}
			r.logger.Infof("set container core %v succeed", info.Guid)
		}
	}

	return &compose.RunningBaseInfo{
		Guid:          info.Guid,
		CID:           info.ContainerId,
		ContainerName: info.ContainerName,
		Host:          info.ContainerHost,
		Port:          info.ContainerPort,
	}, nil
}

func (r *SupbRunner) Remove(guid string, container *compose.Containers) error {
	req := &clireq.ClientRequest{
		Guid: container.Guid,
	}
	bts, err := json.Marshal(req)
	if err != nil {
		return err
	}
	_, err = r.rpc.RPCCallCustom(guid, 100, 500, define.RPCFunctionNameOfAgentForDeleteObject, string(bts))
	if err != nil {
		return err
	}
	return nil
}

func (r *SupbRunner) Stop(guid string, container *compose.Containers) error {
	req := &clireq.ClientRequest{
		Guid: container.Guid,
	}
	bts, err := json.Marshal(req)
	if err != nil {
		return err
	}
	_, err = r.rpc.RPCCall(guid, define.RPCFunctionNameOfAgentForStopObject, string(bts))
	if err != nil {
		return err
	}
	return nil
}

func (r *SupbRunner) Restart(guid string, container *compose.Containers) error {
	req := &clireq.ClientRequest{
		Guid: container.Guid,
	}
	bts, err := json.Marshal(req)
	if err != nil {
		return err
	}
	_, err = r.rpc.RPCCall(guid, define.RPCFunctionNameOfAgentForResumeObject, string(bts))
	if err != nil {
		return err
	}
	return nil
}
