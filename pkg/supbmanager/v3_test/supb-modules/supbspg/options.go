package supbspg

import nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"

type option func(resources *SupbSponge)

func WithRPC(rpc nervous.Controller) option {
	return func(spg *SupbSponge) {
		spg.rpc = rpc
	}
}