package supbspg

import (
	"context"
	"encoding/json"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/compose"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	TryInterval = ".interval"
	TryTimes    = ".times"
)

type SupbSponge struct {
	ctx    context.Context
	prefix string

	interval     int
	tryTime      int
	containerMap map[string]string
	rpc          nervous.Controller
	logger       log.Logger
	resource     map[string]spongeregister.Resource
}

func NewSupbSponge(ctx context.Context, prefix string) *SupbSponge {
	return &SupbSponge{
		ctx:          ctx,
		prefix:       prefix,
		logger:       log.NewLogger(prefix),
		resource:     make(map[string]spongeregister.Resource),
		containerMap: make(map[string]string),
	}
}

func (r *SupbSponge) InitFlags(flags *pflag.FlagSet) {
	pflag.Int(r.prefix+TryInterval, 500, "nervous try interval")
	pflag.Int(r.prefix+TryTimes, 10, "nervous try interval")
}

func (r *SupbSponge) ViperConfig(viper *viper.Viper) {
}

func (r *SupbSponge) InitViper(viper *viper.Viper) {
	r.interval = viper.GetInt(r.prefix + TryInterval)
	r.tryTime = viper.GetInt(r.prefix + TryTimes)
}

func (r *SupbSponge) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *SupbSponge) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}

	go r.run()
	r.logger.Infof("%v initialized", r.prefix)
}

func (r *SupbSponge) Close() error {
	r.logger.Infof("%v closed", r.prefix)
	return nil
}

func (r *SupbSponge) run() {
	resp, err := r.rpc.RPCCallCustom(define.INIT_SPONGE_OCTOPUS, r.tryTime, r.interval, define.LIST_ALIVE_AGENTS)
	if err != nil {
		r.logger.Errorf("%v", err)
		return
	}
	//r.logger.Infof("%v info",resp)
	//解码
	data := []byte(resp.(string))
	resSync := &spongeregister.ListResources{
		Resources: make(map[string]spongeregister.Resource),
	}
	if err := json.Unmarshal(data, resSync); err != nil {
		log.Errorf("json.Unmarshal %v", err)
	}
	r.resource = resSync.Resources

	r.logger.Infof("agetnResources: %v", r.resource)
}

func (r *SupbSponge) GetAgentGuid(ct *compose.Containers) string {
	hostMap := make(map[string]int)
	for _, tag := range ct.Tags {
		if tag.Key == "HOST" {
			hostMap[tag.Value.(string)] = 1
		}
	}

	r.logger.Infof("map: %v",hostMap)
	r.logger.Infof("res: %v",r.resource)

	for guid, info := range r.resource {
		for _, tag := range info.Tags {
			if tag.Key == "HOST" && hostMap[tag.Value.(string)] == 1 {
				r.containerMap[ct.AppKey] = guid
				return guid
			}
		}
	}

	return ""
}

func (r *SupbSponge) GetContainerGuid(ct *compose.Containers) string {
	if _, ok := r.containerMap[ct.AppKey]; !ok {
		return ""
	}
	return r.containerMap[ct.AppKey]
}
