package supbstrategy

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"sync"
)

type SupbStrategy struct {
	ctx    context.Context
	prefix string
	Guid   string

	logger log.Logger

	lock  *sync.RWMutex
	index int
}

func NewSupbStrategy(ctx context.Context, prefix string) *SupbStrategy {
	return &SupbStrategy{
		ctx:    ctx,
		prefix: prefix,
		logger: log.NewLogger(prefix),

		lock: &sync.RWMutex{},
	}
}

func (r *SupbStrategy) InitFlags(flags *pflag.FlagSet) {
}

func (r *SupbStrategy) ViperConfig(viper *viper.Viper) {

}

func (r *SupbStrategy) InitViper(viper *viper.Viper) {
}

func (r *SupbStrategy) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *SupbStrategy) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}

	//r.rpc = rpc
	//go r.productData()
	r.logger.Infof("%v initialized", r.prefix)
}

func (r *SupbStrategy) Close() error {
	r.logger.Infof("%v closed", r.prefix)
	return nil
}

func (r *SupbStrategy) GetCommonData(key string) (interface{},error) {
	var res interface{}
	switch key {
	case "指标1":
		data := [][]interface{}{
			{1,2,3},
			{2,1,3},
			{3,2,3},
			{4,6,3},
			{5,1,3},
		}
		res = map[string]interface{}{
			"table":"scatter",
			"data": data,
		}
	case "指标2":
		data := [][]interface{}{
			{1,23},
			{2,12},
			{3,22},
			{4,11},
			{5,17},
		}
		res = map[string]interface{}{
			"table":"line",
			"data": data,
		}
	case "指标3":
		data := [][]interface{}{
			{"星期一",23},
			{"星期二",12},
			{"星期三",22},
			{"星期四",11},
			{"星期五",17},
		}
		res = map[string]interface{}{
			"table":"bar",
			"data": data,
		}
	case "指标4":
		data := [][]interface{}{
			{"0~25%",0.3},
			{"25~50%",0.3},
			{"50~75%",0.2},
			{"75~100%",0.1},
			{"其他",0.1},
		}
		res = map[string]interface{}{
			"table":"pie",
			"data": data,
		}
	default:
		return nil,fmt.Errorf("unkwon metrics")
	}

	return res,nil
}

func (r *SupbStrategy) GetCommonInfo() (map[string]string,error) {
	res := map[string]string{
		"指标1":"scatter",
		"指标2":"line",
		"指标3":"bar",
		"指标4":"pie",
	}
	return res,nil
}

//func (r *SupbStrategy) GetUrl() error {
//
//	return ,nil
//}