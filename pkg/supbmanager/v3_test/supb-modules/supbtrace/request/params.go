package request

type Params struct {
	// 必选
	Service string `json:"service"`

	// 可选
	Operation   string `json:"operation"`
	Start       string `json:"star"`
	End         string `json:"end"`
	LookBack    string `json:"lookback"`
	MaxDuration string `json:"maxDuration"`
	MinDuration string `json:"minDuration"`
	Limit       string `json:"limit"`
}


type CommonResp struct {
	Data   interface{} `json:"data"`
	Total  int         `json:"total"`
	Limit  int         `json:"limit"`
	Offset int         `json:"offset"`
	Errors interface{} `json:"errors"`
}