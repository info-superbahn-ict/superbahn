package supbtrace

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbtrace/request"
	"io/ioutil"
	"net/http"
	"sync"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbtrace/trace"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	debug = ".debug"
	jaegerIP              = ".jaeger.ip"
	jaegerPort              = ".jaeger.port"

	getTraces       = `getTraces`
	getTrace        = `getTrace`
	getServices     = `getServices`
	getDependencies = `getDependencies`
	getOperations   = `getOperations`
)

type SupbTrace struct {
	ctx    context.Context
	prefix string
	Guid   string

	logger     log.Logger
	httpClient http.Client

	lock  *sync.RWMutex
	data  map[string][]trace.TracesELK
	urls  map[string]string
	index int

	debugMode bool
}

func NewSupbTrace(ctx context.Context, prefix string) *SupbTrace {
	return &SupbTrace{
		ctx:        ctx,
		prefix:     prefix,
		logger:     log.NewLogger(prefix),
		httpClient: *http.DefaultClient,

		lock: &sync.RWMutex{},
		data: make(map[string][]trace.TracesELK),
	}
}

func (r *SupbTrace) InitFlags(flags *pflag.FlagSet) {
	//flags.String(r.prefix + jaegerIP,"47.108.237.20","jaeger ui ip")
	flags.String(r.prefix + jaegerIP,"10.16.0.180","jaeger ui ip")
	flags.String(r.prefix + jaegerPort,"16686","jaeger ui ip")
	flags.Bool(r.prefix + debug,false,"debug model")
}

func (r *SupbTrace) ViperConfig(viper *viper.Viper) {
	viper.SetDefault(r.prefix + jaegerIP,"10.16.0.180")
	viper.SetDefault(r.prefix + jaegerPort,"16686")
	viper.SetDefault(r.prefix + debug,false)
}

func (r *SupbTrace) InitViper(viper *viper.Viper) {
	ip := viper.GetString(r.prefix + jaegerIP)
	port := viper.GetString(r.prefix + jaegerPort)
	r.debugMode = viper.GetBool(r.prefix + debug)

	urls := make(map[string]string)

	urls[getTraces] = "http://" + ip + ":"+port+"/api/traces"
	urls[getTrace] ="http://" + ip + ":"+port+"/api/traces/"
	urls[getServices] = "http://" + ip + ":"+port+"/api/services"
	urls[getDependencies] = "http://" + ip + ":"+port+"/api/dependencies"
	urls[getOperations] ="http://" + ip + ":"+port+"/api/services/"

	r.urls = urls
}

func (r *SupbTrace) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *SupbTrace) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}

	//r.rpc = rpc
	//go r.productData()

	r.logger.Infof("%v initialized", r.prefix)
}

//type Params struct {
//	// 必选
//	Service string `json:"service"`
//
//	// 可选
//	Operation   string `json:"operation"`
//	Start       string `json:"star"`
//	End         string `json:"end"`
//	LookBack    string `json:"lookback"`
//	MaxDuration string `json:"maxDuration"`
//	MinDuration string `json:"minDuration"`
//	Limit       string `json:"limit"`
//}
//
//type CommonResp struct {
//	Data   interface{} `json:"data"`
//	Total  int         `json:"total"`
//	Limit  int         `json:"limit"`
//	Offset int         `json:"offset"`
//	Errors interface{} `json:"errors"`
//}

// type CommonResp struct {
// 	Total  int         `json:"total"`
// 	Limit  int         `json:"limit"`
// 	Offset int         `json:"offset"`
// 	Errors interface{} `json:"errors"`
// }

// type TracesResponse struct {
// 	Data []interface{} `json:"data"`
// 	*CommonResp
// }

// type TraceResponse struct {
// 	Data []interface{} `json:"data"`
// 	*CommonResp
// }

// type ServicesResponse struct {
// 	Data []string `json:"data"`
// 	*CommonResp
// }

// type Depndency struct {
// 	Parent    string `json:"parent"`
// 	Child     string `json:"child"`
// 	CallCount int    `json:"callCount"`
// }

// type DependenciesResponse struct {
// 	Data []Depndency `json:"data"`
// 	*CommonResp
// }

// type OperationsResponse struct {
// 	Data []string `json:"data"`
// 	*CommonResp
// }

/*
   描述: traces信息查询
   request Params

   error:

   eg: r.GetTraces(Params{
		Service:     "frontend",
		Start:       "1640837157459000",
		End:         "1640923557459000",
		MaxDuration: "300ms",
		MinDuration: "100ms",
	})
*/
func (r *SupbTrace) GetTraces(params request.Params) (interface{}, error) {
	req, _ := http.NewRequest("Get", r.urls[getTraces], nil)

	query := req.URL.Query()

	if params.Service == "" {
		return nil, fmt.Errorf("no service")
	}
	query.Add("service", params.Service)

	if params.Operation != "" {
		query.Add("operation", params.Operation)
	}

	if params.Start != "" {
		query.Add("start", params.Start)
	}

	if params.End != "" {
		query.Add("end", params.End)
	}

	if params.LookBack != "" {
		query.Add("lookback", params.LookBack)
	}

	if params.MaxDuration != "" {
		query.Add("maxDuration", params.MaxDuration)
	}

	if params.MinDuration != "" {
		query.Add("minDuration", params.MinDuration)
	}

	if params.Limit != "" {
		query.Add("limit", params.Limit)
	}

	req.URL.RawQuery = query.Encode()
	reqUrl := req.URL.String()

	//r.logger.Infof("get traces: ", req.URL)
	resp, err := r.httpClient.Get(reqUrl)

	if err != nil {
		r.logger.Infof("get traces failed ")
	}

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, fmt.Errorf("read body failed")
	}
	//r.logger.Infof("receive body: ", string(body))

	// fmtResp := TracesResponse{}
	fmtResp := request.CommonResp{}
	jsonErr := json.Unmarshal(body, &fmtResp)
	if jsonErr != nil {
		return nil, fmt.Errorf("parse body failed")
	}

	if fmtResp.Errors != nil {
		return nil, fmt.Errorf("request error: %v", fmtResp.Errors)
	}

	defer resp.Body.Close()
	return fmtResp.Data, nil
}

/*
   描述: trace具体信息查询
   traceId string

   error:

   eg: r.GetTrace("7648504b4a5b7402")
*/
func (r *SupbTrace) GetTrace(traceId string) (interface{}, error) {
	//if r.debugMode{
	//	return []interface{}{
	//		[
	//			{
	//				"processes":{
	//					"p1":{
	//						"serviceName":"driver", "tags":[{"key":"client-uuid", "type":"string", "value":"4db891d4fc8b897d"},{"key":"hostname", "type":"string", "value":"d0c17d0f9747"},{"key":"ip", "type":"string", "value":"172.21.0.3"},{"key":"jaeger.version", "type":"string", "value":"Go-2.30.0"}]
	//					},
	//					"p2":{
	//						"serviceName":"customer", "tags":[{"key":"client-uuid", "type":"string", "value":"500e33ef3f260d3f"},{"key":"hostname", "type":"string", "value":"d0c17d0f9747"},{"key":"ip", "type":"string", "value":"172.21.0.3"},{"key":"jaeger.version", "type":"string", "value":"Go-2.30.0"}]
	//					},
	//					"p3":{
	//						"serviceName":"frontend", "tags":[{"key":"client-uuid", "type":"string", "value":"2e4a3b5058b7650c"},{"key":"hostname", "type":"string", "value":"d0c17d0f9747"},{"key":"ip", "type":"string", "value":"172.21.0.3"},{"key":"jaeger.version", "type":"string", "value":"Go-2.30.0"}]
	//					},
	//					"p4":{
	//						"serviceName":"redis", "tags":[{"key":"client-uuid", "type":"string", "value":"6480f385ed0b5812"},{"key":"hostname", "type":"string", "value":"d0c17d0f9747"},{"key":"ip", "type":"string", "value":"172.21.0.3"},{"key":"jaeger.version", "type":"string", "value":"Go-2.30.0"}]
	//					},
	//					"p5":{
	//						"serviceName":"mysql", "tags":[{"key":"client-uuid", "type":"string", "value":"123f64c6473c7a92"},{"key":"hostname", "type":"string", "value":"d0c17d0f9747"},{"key":"ip", "type":"string", "value":"172.21.0.3"},{"key":"jaeger.version", "type":"string", "value":"Go-2.30.0"}]
	//					},
	//					"p6":{
	//						"serviceName":"route", "tags":[{"key":"client-uuid", "type":"string", "value":"1195cf8dfa5f37c3"},{"key":"hostname", "type":"string", "value":"d0c17d0f9747"},{"key":"ip", "type":"string", "value":"172.21.0.3"},{"key":"jaeger.version", "type":"string", "value":"Go-2.30.0"}]
	//					}
	//				},
	//				"spans":[
	//					{"duration":193739, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"Searching for nearby drivers"},{"key":"level", "type":"string", "value":"info"},{"key":"location", "type":"string", "value":"728,326"}], "timestamp":1640852440689743},{"fields":[{"key":"event", "type":"string", "value":"Retrying GetDriver after error"},{"key":"error", "type":"string", "value":"redis timeout"},{"key":"level", "type":"string", "value":"error"},{"key":"retry_no", "type":"int64", "value":1}], "timestamp":1640852440762667},{"fields":[{"key":"event", "type":"string", "value":"Retrying GetDriver after error"},{"key":"error", "type":"string", "value":"redis timeout"},{"key":"level", "type":"string", "value":"error"},{"key":"retry_no", "type":"int64", "value":1}], "timestamp":1640852440842497},{"fields":[{"key":"event", "type":"string", "value":"Search successful"},{"key":"level", "type":"string", "value":"info"},{"key":"num_drivers", "type":"int64", "value":10}], "timestamp":1640852440883420}], "operationName":"/driver.DriverService/FindNearest", "processID":"p1", "references":[{"refType":"CHILD_OF", "spanID":"1e8add1def67e9e2", "traceID":"7648504b4a5b7402"}], "spanID":"2ff5a9c1be93bece", "startTime":1640852440689733, "tags":[{"key":"span.kind", "type":"string", "value":"server"},{"key":"component", "type":"string", "value":"gRPC"},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":313836, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"HTTP request received"},{"key":"level", "type":"string", "value":"info"},{"key":"method", "type":"string", "value":"GET"},{"key":"url", "type":"string", "value":"/customer?customer=731"}], "timestamp":1640852440375340},{"fields":[{"key":"event", "type":"string", "value":"Loading customer"},{"key":"customer_id", "type":"string", "value":"731"},{"key":"level", "type":"string", "value":"info"}], "timestamp":1640852440375368}], "operationName":"HTTP GET /customer", "processID":"p2", "references":[{"refType":"CHILD_OF", "spanID":"733800741ac4fb35", "traceID":"7648504b4a5b7402"}], "spanID":"6941905daddfd6c0", "startTime":1640852440375328, "tags":[{"key":"span.kind", "type":"string", "value":"server"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"/customer?customer=731"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":194287, "flags":1, "logs":[], "operationName":"/driver.DriverService/FindNearest", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"7648504b4a5b7402", "traceID":"7648504b4a5b7402"}], "spanID":"1e8add1def67e9e2", "startTime":1640852440689504, "tags":[{"key":"span.kind", "type":"string", "value":"client"},{"key":"component", "type":"string", "value":"gRPC"},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":314508, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"GetConn"}], "timestamp":1640852440374962},{"fields":[{"key":"event", "type":"string", "value":"ConnectStart"},{"key":"addr", "type":"string", "value":"0.0.0.0:8081"},{"key":"network", "type":"string", "value":"tcp"}], "timestamp":1640852440375025},{"fields":[{"key":"event", "type":"string", "value":"ConnectDone"},{"key":"addr", "type":"string", "value":"0.0.0.0:8081"},{"key":"network", "type":"string", "value":"tcp"}], "timestamp":1640852440375150},{"fields":[{"key":"event", "type":"string", "value":"GotConn"}], "timestamp":1640852440375164},{"fields":[{"key":"event", "type":"string", "value":"WroteHeaders"}], "timestamp":1640852440375248},{"fields":[{"key":"event", "type":"string", "value":"WroteRequest"}], "timestamp":1640852440375250},{"fields":[{"key":"event", "type":"string", "value":"GotFirstResponseByte"}], "timestamp":1640852440689421},{"fields":[{"key":"event", "type":"string", "value":"PutIdleConn"}], "timestamp":1640852440689440},{"fields":[{"key":"event", "type":"string", "value":"ClosedBody"}], "timestamp":1640852440689450}], "operationName":"HTTP GET", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"2e5bea7fd04c8285", "traceID":"7648504b4a5b7402"}], "spanID":"733800741ac4fb35", "startTime":1640852440374942, "tags":[{"key":"span.kind", "type":"string", "value":"client"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"http://0.0.0.0:8081/customer?customer=731"},{"key":"http.url", "type":"string", "value":"0.0.0.0:8081"},{"key":"net/http.reused", "type":"bool", "value":false},{"key":"net/http.was_idle", "type":"bool", "value":false},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":314554, "flags":1, "logs":[], "operationName":"HTTP GET: /customer", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"7648504b4a5b7402", "traceID":"7648504b4a5b7402"}], "spanID":"2e5bea7fd04c8285", "startTime":1640852440374897, "tags":[{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":20528, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"Found drivers"},{"key":"level", "type":"string", "value":"info"}], "timestamp":1640852440710237}], "operationName":"FindDriverIDs", "processID":"p4", "references":[{"refType":"CHILD_OF", "spanID":"2ff5a9c1be93bece", "traceID":"7648504b4a5b7402"}], "spanID":"167591e6c7bc679f", "startTime":1640852440689758, "tags":[{"key":"param.location", "type":"string", "value":"728,326"},{"key":"span.kind", "type":"string", "value":"client"},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":13047, "flags":1, "logs":[], "operationName":"GetDriver", "processID":"p4", "references":[{"refType":"CHILD_OF", "spanID":"2ff5a9c1be93bece", "traceID":"7648504b4a5b7402"}], "spanID":"669bd654388b646d", "startTime":1640852440710294, "tags":[{"key":"param.driverID", "type":"string", "value":"T759739C"},{"key":"span.kind", "type":"string", "value":"client"},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":10397, "flags":1, "logs":[], "operationName":"GetDriver", "processID":"p4", "references":[{"refType":"CHILD_OF", "spanID":"2ff5a9c1be93bece", "traceID":"7648504b4a5b7402"}], "spanID":"70a32acbe87baa40", "startTime":1640852440723357, "tags":[{"key":"param.driverID", "type":"string", "value":"T785756C"},{"key":"span.kind", "type":"string", "value":"client"},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":28866, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"redis timeout"},{"key":"driver_id", "type":"string", "value":"T765499C"},{"key":"error", "type":"string", "value":"redis timeout"},{"key":"level", "type":"string", "value":"error"}], "timestamp":1640852440762587}], "operationName":"GetDriver", "processID":"p4", "references":[{"refType":"CHILD_OF", "spanID":"2ff5a9c1be93bece", "traceID":"7648504b4a5b7402"}], "spanID":"1df0a0a4a8857de7", "startTime":1640852440733772, "tags":[{"key":"param.driverID", "type":"string", "value":"T765499C"},{"key":"span.kind", "type":"string", "value":"client"},{"key":"error", "type":"bool", "value":true},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":10883, "flags":1, "logs":[], "operationName":"GetDriver", "processID":"p4", "references":[{"refType":"CHILD_OF", "spanID":"2ff5a9c1be93bece", "traceID":"7648504b4a5b7402"}], "spanID":"674d3d7683c721bc", "startTime":1640852440762681, "tags":[{"key":"param.driverID", "type":"string", "value":"T765499C"},{"key":"span.kind", "type":"string", "value":"client"},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":12592, "flags":1, "logs":[], "operationName":"GetDriver", "processID":"p4", "references":[{"refType":"CHILD_OF", "spanID":"2ff5a9c1be93bece", "traceID":"7648504b4a5b7402"}], "spanID":"55ca472050e0b533", "startTime":1640852440773572, "tags":[{"key":"param.driverID", "type":"string", "value":"T717365C"},{"key":"span.kind", "type":"string", "value":"client"},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":13560, "flags":1, "logs":[], "operationName":"GetDriver", "processID":"p4", "references":[{"refType":"CHILD_OF", "spanID":"2ff5a9c1be93bece", "traceID":"7648504b4a5b7402"}], "spanID":"1374551667c1101f", "startTime":1640852440786173, "tags":[{"key":"param.driverID", "type":"string", "value":"T761944C"},{"key":"span.kind", "type":"string", "value":"client"},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":13225, "flags":1, "logs":[], "operationName":"GetDriver", "processID":"p4", "references":[{"refType":"CHILD_OF", "spanID":"2ff5a9c1be93bece", "traceID":"7648504b4a5b7402"}], "spanID":"01e844889e4a8fd2", "startTime":1640852440799749, "tags":[{"key":"param.driverID", "type":"string", "value":"T755815C"},{"key":"span.kind", "type":"string", "value":"client"},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":29496, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"redis timeout"},{"key":"driver_id", "type":"string", "value":"T758315C"},{"key":"error", "type":"string", "value":"redis timeout"},{"key":"level", "type":"string", "value":"error"}], "timestamp":1640852440842416}], "operationName":"GetDriver", "processID":"p4", "references":[{"refType":"CHILD_OF", "spanID":"2ff5a9c1be93bece", "traceID":"7648504b4a5b7402"}], "spanID":"337c5ca9b515d52f", "startTime":1640852440812992, "tags":[{"key":"param.driverID", "type":"string", "value":"T758315C"},{"key":"span.kind", "type":"string", "value":"client"},{"key":"error", "type":"bool", "value":true},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":8213, "flags":1, "logs":[], "operationName":"GetDriver", "processID":"p4", "references":[{"refType":"CHILD_OF", "spanID":"2ff5a9c1be93bece", "traceID":"7648504b4a5b7402"}], "spanID":"0ab9e7a36fcd85da", "startTime":1640852440842513, "tags":[{"key":"param.driverID", "type":"string", "value":"T758315C"},{"key":"span.kind", "type":"string", "value":"client"},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":11219, "flags":1, "logs":[], "operationName":"GetDriver", "processID":"p4", "references":[{"refType":"CHILD_OF", "spanID":"2ff5a9c1be93bece", "traceID":"7648504b4a5b7402"}], "spanID":"4a4798aa0dc990e4", "startTime":1640852440850743, "tags":[{"key":"param.driverID", "type":"string", "value":"T706155C"},{"key":"span.kind", "type":"string", "value":"client"},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":10164, "flags":1, "logs":[], "operationName":"GetDriver", "processID":"p4", "references":[{"refType":"CHILD_OF", "spanID":"2ff5a9c1be93bece", "traceID":"7648504b4a5b7402"}], "spanID":"5a25b0ecc2df874f", "startTime":1640852440861979, "tags":[{"key":"param.driverID", "type":"string", "value":"T727770C"},{"key":"span.kind", "type":"string", "value":"client"},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":11239, "flags":1, "logs":[], "operationName":"GetDriver", "processID":"p4", "references":[{"refType":"CHILD_OF", "spanID":"2ff5a9c1be93bece", "traceID":"7648504b4a5b7402"}], "spanID":"11e8718aea792142", "startTime":1640852440872160, "tags":[{"key":"param.driverID", "type":"string", "value":"T714415C"},{"key":"span.kind", "type":"string", "value":"client"},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":313767, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"Waiting for lock behind 1 transactions"},{"key":"blockers", "type":"string", "value":"[6185-1]"}], "timestamp":1640852440375394},{"fields":[{"key":"event", "type":"string", "value":"Acquired lock with 0 transactions waiting behind"}], "timestamp":1640852440402856}], "operationName":"SQL SELECT", "processID":"p5", "references":[{"refType":"CHILD_OF", "spanID":"6941905daddfd6c0", "traceID":"7648504b4a5b7402"}], "spanID":"48ff0cffe1bf0c53", "startTime":1640852440375378, "tags":[{"key":"span.kind", "type":"string", "value":"client"},{"key":"peer.service", "type":"string", "value":"mysql"},{"key":"sql.query", "type":"string", "value":"SELECT * FROM customer WHERE customer_id=731"},{"key":"request", "type":"string", "value":"6185-2"},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":48603, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"HTTP request received"},{"key":"level", "type":"string", "value":"info"},{"key":"method", "type":"string", "value":"GET"},{"key":"url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=130%2C360"}], "timestamp":1640852440884526}], "operationName":"HTTP GET /route", "processID":"p6", "references":[{"refType":"CHILD_OF", "spanID":"5de2c0e369dd0aa1", "traceID":"7648504b4a5b7402"}], "spanID":"3df4911f71792df0", "startTime":1640852440884519, "tags":[{"key":"span.kind", "type":"string", "value":"server"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=130%2C360"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":51523, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"HTTP request received"},{"key":"level", "type":"string", "value":"info"},{"key":"method", "type":"string", "value":"GET"},{"key":"url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=73%2C856"}], "timestamp":1640852440884476}], "operationName":"HTTP GET /route", "processID":"p6", "references":[{"refType":"CHILD_OF", "spanID":"0c142d0f76016532", "traceID":"7648504b4a5b7402"}], "spanID":"020d54c227e8ba63", "startTime":1640852440884461, "tags":[{"key":"span.kind", "type":"string", "value":"server"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=73%2C856"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":67939, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"HTTP request received"},{"key":"level", "type":"string", "value":"info"},{"key":"method", "type":"string", "value":"GET"},{"key":"url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=383%2C838"}], "timestamp":1640852440884645}], "operationName":"HTTP GET /route", "processID":"p6", "references":[{"refType":"CHILD_OF", "spanID":"5e9d09b4a812d409", "traceID":"7648504b4a5b7402"}], "spanID":"781328a15aaf273c", "startTime":1640852440884635, "tags":[{"key":"span.kind", "type":"string", "value":"server"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=383%2C838"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":29265, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"HTTP request received"},{"key":"level", "type":"string", "value":"info"},{"key":"method", "type":"string", "value":"GET"},{"key":"url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=599%2C555"}], "timestamp":1640852440953108}], "operationName":"HTTP GET /route", "processID":"p6", "references":[{"refType":"CHILD_OF", "spanID":"4287ac9e2cd47bc8", "traceID":"7648504b4a5b7402"}], "spanID":"0eed5efefe8c9451", "startTime":1640852440953089, "tags":[{"key":"span.kind", "type":"string", "value":"server"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=599%2C555"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":52698, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"HTTP request received"},{"key":"level", "type":"string", "value":"info"},{"key":"method", "type":"string", "value":"GET"},{"key":"url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=148%2C102"}], "timestamp":1640852440936369}], "operationName":"HTTP GET /route", "processID":"p6", "references":[{"refType":"CHILD_OF", "spanID":"31f9cb733e299d23", "traceID":"7648504b4a5b7402"}], "spanID":"3c28caf0ff4e642c", "startTime":1640852440936353, "tags":[{"key":"span.kind", "type":"string", "value":"server"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=148%2C102"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":64787, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"HTTP request received"},{"key":"level", "type":"string", "value":"info"},{"key":"method", "type":"string", "value":"GET"},{"key":"url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=697%2C943"}], "timestamp":1640852440933773}], "operationName":"HTTP GET /route", "processID":"p6", "references":[{"refType":"CHILD_OF", "spanID":"673b2a625ba928ad", "traceID":"7648504b4a5b7402"}], "spanID":"39723b75ce18353e", "startTime":1640852440933755, "tags":[{"key":"span.kind", "type":"string", "value":"server"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=697%2C943"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":34331, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"HTTP request received"},{"key":"level", "type":"string", "value":"info"},{"key":"method", "type":"string", "value":"GET"},{"key":"url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=787%2C682"}], "timestamp":1640852440982782}], "operationName":"HTTP GET /route", "processID":"p6", "references":[{"refType":"CHILD_OF", "spanID":"57c57568b3af5912", "traceID":"7648504b4a5b7402"}], "spanID":"66ffd724cb2e21b1", "startTime":1640852440982759, "tags":[{"key":"span.kind", "type":"string", "value":"server"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=787%2C682"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":56808, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"HTTP request received"},{"key":"level", "type":"string", "value":"info"},{"key":"method", "type":"string", "value":"GET"},{"key":"url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=14%2C108"}], "timestamp":1640852440989408}], "operationName":"HTTP GET /route", "processID":"p6", "references":[{"refType":"CHILD_OF", "spanID":"37c5f6bcec8b0cc8", "traceID":"7648504b4a5b7402"}], "spanID":"05aa832f0f1864a4", "startTime":1640852440989391, "tags":[{"key":"span.kind", "type":"string", "value":"server"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=14%2C108"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":58800, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"HTTP request received"},{"key":"level", "type":"string", "value":"info"},{"key":"method", "type":"string", "value":"GET"},{"key":"url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=248%2C98"}], "timestamp":1640852440999011}], "operationName":"HTTP GET /route", "processID":"p6", "references":[{"refType":"CHILD_OF", "spanID":"4307b4b26e55d506", "traceID":"7648504b4a5b7402"}], "spanID":"28b2156058957294", "startTime":1640852440998988, "tags":[{"key":"span.kind", "type":"string", "value":"server"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=248%2C98"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":63857, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"HTTP request received"},{"key":"level", "type":"string", "value":"info"},{"key":"method", "type":"string", "value":"GET"},{"key":"url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=448%2C176"}], "timestamp":1640852441017521}], "operationName":"HTTP GET /route", "processID":"p6", "references":[{"refType":"CHILD_OF", "spanID":"6cdaa9f0f69c7d39", "traceID":"7648504b4a5b7402"}], "spanID":"21937450a15354c4", "startTime":1640852441017505, "tags":[{"key":"span.kind", "type":"string", "value":"server"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"/route?dropoff=728%2C326\u0026pickup=448%2C176"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":49527, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"GetConn"}], "timestamp":1640852440883926},{"fields":[{"key":"event", "type":"string", "value":"GotConn"}], "timestamp":1640852440883934},{"fields":[{"key":"event", "type":"string", "value":"WroteHeaders"}], "timestamp":1640852440883956},{"fields":[{"key":"event", "type":"string", "value":"WroteRequest"}], "timestamp":1640852440883958},{"fields":[{"key":"event", "type":"string", "value":"GotFirstResponseByte"}], "timestamp":1640852440933332},{"fields":[{"key":"event", "type":"string", "value":"PutIdleConn"}], "timestamp":1640852440933385},{"fields":[{"key":"event", "type":"string", "value":"ClosedBody"}], "timestamp":1640852440933416}], "operationName":"HTTP GET", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"274cdfb51025ba79", "traceID":"7648504b4a5b7402"}], "spanID":"5de2c0e369dd0aa1", "startTime":1640852440883889, "tags":[{"key":"span.kind", "type":"string", "value":"client"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"http://0.0.0.0:8083/route?dropoff=728%2C326\u0026pickup=130%2C360"},{"key":"http.url", "type":"string", "value":"0.0.0.0:8083"},{"key":"net/http.reused", "type":"bool", "value":true},{"key":"net/http.was_idle", "type":"bool", "value":true},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":49538, "flags":1, "logs":[], "operationName":"HTTP GET: /route", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"7648504b4a5b7402", "traceID":"7648504b4a5b7402"}], "spanID":"274cdfb51025ba79", "startTime":1640852440883881, "tags":[{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":52083, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"GetConn"}], "timestamp":1640852440884053},{"fields":[{"key":"event", "type":"string", "value":"GotConn"}], "timestamp":1640852440884057},{"fields":[{"key":"event", "type":"string", "value":"WroteHeaders"}], "timestamp":1640852440884068},{"fields":[{"key":"event", "type":"string", "value":"WroteRequest"}], "timestamp":1640852440884069},{"fields":[{"key":"event", "type":"string", "value":"GotFirstResponseByte"}], "timestamp":1640852440936071},{"fields":[{"key":"event", "type":"string", "value":"PutIdleConn"}], "timestamp":1640852440936097},{"fields":[{"key":"event", "type":"string", "value":"ClosedBody"}], "timestamp":1640852440936112}], "operationName":"HTTP GET", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"09bac3b9d89b6db3", "traceID":"7648504b4a5b7402"}], "spanID":"0c142d0f76016532", "startTime":1640852440884029, "tags":[{"key":"span.kind", "type":"string", "value":"client"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"http://0.0.0.0:8083/route?dropoff=728%2C326\u0026pickup=73%2C856"},{"key":"http.url", "type":"string", "value":"0.0.0.0:8083"},{"key":"net/http.reused", "type":"bool", "value":true},{"key":"net/http.was_idle", "type":"bool", "value":true},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":52089, "flags":1, "logs":[], "operationName":"HTTP GET: /route", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"7648504b4a5b7402", "traceID":"7648504b4a5b7402"}], "spanID":"09bac3b9d89b6db3", "startTime":1640852440884025, "tags":[{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":68663, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"GetConn"}], "timestamp":1640852440884151},{"fields":[{"key":"event", "type":"string", "value":"ConnectStart"},{"key":"addr", "type":"string", "value":"0.0.0.0:8083"},{"key":"network", "type":"string", "value":"tcp"}], "timestamp":1640852440884217},{"fields":[{"key":"event", "type":"string", "value":"ConnectDone"},{"key":"addr", "type":"string", "value":"0.0.0.0:8083"},{"key":"network", "type":"string", "value":"tcp"}], "timestamp":1640852440884356},{"fields":[{"key":"event", "type":"string", "value":"GotConn"}], "timestamp":1640852440884371},{"fields":[{"key":"event", "type":"string", "value":"WroteHeaders"}], "timestamp":1640852440884566},{"fields":[{"key":"event", "type":"string", "value":"WroteRequest"}], "timestamp":1640852440884567},{"fields":[{"key":"event", "type":"string", "value":"GotFirstResponseByte"}], "timestamp":1640852440952734},{"fields":[{"key":"event", "type":"string", "value":"PutIdleConn"}], "timestamp":1640852440952770},{"fields":[{"key":"event", "type":"string", "value":"ClosedBody"}], "timestamp":1640852440952789}], "operationName":"HTTP GET", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"05e532680f3b567a", "traceID":"7648504b4a5b7402"}], "spanID":"5e9d09b4a812d409", "startTime":1640852440884128, "tags":[{"key":"span.kind", "type":"string", "value":"client"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"http://0.0.0.0:8083/route?dropoff=728%2C326\u0026pickup=383%2C838"},{"key":"http.url", "type":"string", "value":"0.0.0.0:8083"},{"key":"net/http.reused", "type":"bool", "value":false},{"key":"net/http.was_idle", "type":"bool", "value":false},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":68668, "flags":1, "logs":[], "operationName":"HTTP GET: /route", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"7648504b4a5b7402", "traceID":"7648504b4a5b7402"}], "spanID":"05e532680f3b567a", "startTime":1640852440884124, "tags":[{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":29655, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"GetConn"}], "timestamp":1640852440952925},{"fields":[{"key":"event", "type":"string", "value":"GotConn"}], "timestamp":1640852440952931},{"fields":[{"key":"event", "type":"string", "value":"WroteHeaders"}], "timestamp":1640852440952967},{"fields":[{"key":"event", "type":"string", "value":"WroteRequest"}], "timestamp":1640852440952969},{"fields":[{"key":"event", "type":"string", "value":"GotFirstResponseByte"}], "timestamp":1640852440982468},{"fields":[{"key":"event", "type":"string", "value":"PutIdleConn"}], "timestamp":1640852440982525},{"fields":[{"key":"event", "type":"string", "value":"ClosedBody"}], "timestamp":1640852440982544}], "operationName":"HTTP GET", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"6f7a0cb878f213c4", "traceID":"7648504b4a5b7402"}], "spanID":"4287ac9e2cd47bc8", "startTime":1640852440952889, "tags":[{"key":"span.kind", "type":"string", "value":"client"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"http://0.0.0.0:8083/route?dropoff=728%2C326\u0026pickup=599%2C555"},{"key":"http.url", "type":"string", "value":"0.0.0.0:8083"},{"key":"net/http.reused", "type":"bool", "value":true},{"key":"net/http.was_idle", "type":"bool", "value":true},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":29664, "flags":1, "logs":[], "operationName":"HTTP GET: /route", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"7648504b4a5b7402", "traceID":"7648504b4a5b7402"}], "spanID":"6f7a0cb878f213c4", "startTime":1640852440952881, "tags":[{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":52996, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"GetConn"}], "timestamp":1640852440936226},{"fields":[{"key":"event", "type":"string", "value":"GotConn"}], "timestamp":1640852440936231},{"fields":[{"key":"event", "type":"string", "value":"WroteHeaders"}], "timestamp":1640852440936250},{"fields":[{"key":"event", "type":"string", "value":"WroteRequest"}], "timestamp":1640852440936252},{"fields":[{"key":"event", "type":"string", "value":"GotFirstResponseByte"}], "timestamp":1640852440989124},{"fields":[{"key":"event", "type":"string", "value":"PutIdleConn"}], "timestamp":1640852440989183},{"fields":[{"key":"event", "type":"string", "value":"ClosedBody"}], "timestamp":1640852440989199}], "operationName":"HTTP GET", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"5b73be9ce8f40ab8", "traceID":"7648504b4a5b7402"}], "spanID":"31f9cb733e299d23", "startTime":1640852440936203, "tags":[{"key":"span.kind", "type":"string", "value":"client"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"http://0.0.0.0:8083/route?dropoff=728%2C326\u0026pickup=148%2C102"},{"key":"http.url", "type":"string", "value":"0.0.0.0:8083"},{"key":"net/http.reused", "type":"bool", "value":true},{"key":"net/http.was_idle", "type":"bool", "value":true},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":53013, "flags":1, "logs":[], "operationName":"HTTP GET: /route", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"7648504b4a5b7402", "traceID":"7648504b4a5b7402"}], "spanID":"5b73be9ce8f40ab8", "startTime":1640852440936197, "tags":[{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":65175, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"GetConn"}], "timestamp":1640852440933584},{"fields":[{"key":"event", "type":"string", "value":"GotConn"}], "timestamp":1640852440933591},{"fields":[{"key":"event", "type":"string", "value":"WroteHeaders"}], "timestamp":1640852440933624},{"fields":[{"key":"event", "type":"string", "value":"WroteRequest"}], "timestamp":1640852440933626},{"fields":[{"key":"event", "type":"string", "value":"GotFirstResponseByte"}], "timestamp":1640852440998674},{"fields":[{"key":"event", "type":"string", "value":"PutIdleConn"}], "timestamp":1640852440998705},{"fields":[{"key":"event", "type":"string", "value":"ClosedBody"}], "timestamp":1640852440998728}], "operationName":"HTTP GET", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"13f6d36ed8229530", "traceID":"7648504b4a5b7402"}], "spanID":"673b2a625ba928ad", "startTime":1640852440933553, "tags":[{"key":"span.kind", "type":"string", "value":"client"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"http://0.0.0.0:8083/route?dropoff=728%2C326\u0026pickup=697%2C943"},{"key":"http.url", "type":"string", "value":"0.0.0.0:8083"},{"key":"net/http.reused", "type":"bool", "value":true},{"key":"net/http.was_idle", "type":"bool", "value":true},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":65189, "flags":1, "logs":[], "operationName":"HTTP GET: /route", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"7648504b4a5b7402", "traceID":"7648504b4a5b7402"}], "spanID":"13f6d36ed8229530", "startTime":1640852440933542, "tags":[{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":34675, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"GetConn"}], "timestamp":1640852440982635},{"fields":[{"key":"event", "type":"string", "value":"GotConn"}], "timestamp":1640852440982642},{"fields":[{"key":"event", "type":"string", "value":"WroteHeaders"}], "timestamp":1640852440982665},{"fields":[{"key":"event", "type":"string", "value":"WroteRequest"}], "timestamp":1640852440982667},{"fields":[{"key":"event", "type":"string", "value":"GotFirstResponseByte"}], "timestamp":1640852441017223},{"fields":[{"key":"event", "type":"string", "value":"PutIdleConn"}], "timestamp":1640852441017266},{"fields":[{"key":"event", "type":"string", "value":"ClosedBody"}], "timestamp":1640852441017285}], "operationName":"HTTP GET", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"0d1e7300330c304f", "traceID":"7648504b4a5b7402"}], "spanID":"57c57568b3af5912", "startTime":1640852440982610, "tags":[{"key":"span.kind", "type":"string", "value":"client"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"http://0.0.0.0:8083/route?dropoff=728%2C326\u0026pickup=787%2C682"},{"key":"http.url", "type":"string", "value":"0.0.0.0:8083"},{"key":"net/http.reused", "type":"bool", "value":true},{"key":"net/http.was_idle", "type":"bool", "value":true},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":34682, "flags":1, "logs":[], "operationName":"HTTP GET: /route", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"7648504b4a5b7402", "traceID":"7648504b4a5b7402"}], "spanID":"0d1e7300330c304f", "startTime":1640852440982605, "tags":[{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":57146, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"GetConn"}], "timestamp":1640852440989276},{"fields":[{"key":"event", "type":"string", "value":"GotConn"}], "timestamp":1640852440989281},{"fields":[{"key":"event", "type":"string", "value":"WroteHeaders"}], "timestamp":1640852440989294},{"fields":[{"key":"event", "type":"string", "value":"WroteRequest"}], "timestamp":1640852440989295},{"fields":[{"key":"event", "type":"string", "value":"GotFirstResponseByte"}], "timestamp":1640852441046346},{"fields":[{"key":"event", "type":"string", "value":"PutIdleConn"}], "timestamp":1640852441046388},{"fields":[{"key":"event", "type":"string", "value":"ClosedBody"}], "timestamp":1640852441046407}], "operationName":"HTTP GET", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"72cf84a573cec105", "traceID":"7648504b4a5b7402"}], "spanID":"37c5f6bcec8b0cc8", "startTime":1640852440989260, "tags":[{"key":"span.kind", "type":"string", "value":"client"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"http://0.0.0.0:8083/route?dropoff=728%2C326\u0026pickup=14%2C108"},{"key":"http.url", "type":"string", "value":"0.0.0.0:8083"},{"key":"net/http.reused", "type":"bool", "value":true},{"key":"net/http.was_idle", "type":"bool", "value":true},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":57153, "flags":1, "logs":[], "operationName":"HTTP GET: /route", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"7648504b4a5b7402", "traceID":"7648504b4a5b7402"}], "spanID":"72cf84a573cec105", "startTime":1640852440989255, "tags":[{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":59180, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"GetConn"}], "timestamp":1640852440998833},{"fields":[{"key":"event", "type":"string", "value":"GotConn"}], "timestamp":1640852440998838},{"fields":[{"key":"event", "type":"string", "value":"WroteHeaders"}], "timestamp":1640852440998861},{"fields":[{"key":"event", "type":"string", "value":"WroteRequest"}], "timestamp":1640852440998887},{"fields":[{"key":"event", "type":"string", "value":"GotFirstResponseByte"}], "timestamp":1640852441057890},{"fields":[{"key":"event", "type":"string", "value":"PutIdleConn"}], "timestamp":1640852441057974},{"fields":[{"key":"event", "type":"string", "value":"ClosedBody"}], "timestamp":1640852441057996}], "operationName":"HTTP GET", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"70047f18599635c1", "traceID":"7648504b4a5b7402"}], "spanID":"4307b4b26e55d506", "startTime":1640852440998816, "tags":[{"key":"span.kind", "type":"string", "value":"client"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"http://0.0.0.0:8083/route?dropoff=728%2C326\u0026pickup=248%2C98"},{"key":"http.url", "type":"string", "value":"0.0.0.0:8083"},{"key":"net/http.reused", "type":"bool", "value":true},{"key":"net/http.was_idle", "type":"bool", "value":true},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":59190, "flags":1, "logs":[], "operationName":"HTTP GET: /route", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"7648504b4a5b7402", "traceID":"7648504b4a5b7402"}], "spanID":"70047f18599635c1", "startTime":1640852440998808, "tags":[{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":64250, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"GetConn"}], "timestamp":1640852441017389},{"fields":[{"key":"event", "type":"string", "value":"GotConn"}], "timestamp":1640852441017394},{"fields":[{"key":"event", "type":"string", "value":"WroteHeaders"}], "timestamp":1640852441017410},{"fields":[{"key":"event", "type":"string", "value":"WroteRequest"}], "timestamp":1640852441017412},{"fields":[{"key":"event", "type":"string", "value":"GotFirstResponseByte"}], "timestamp":1640852441081516},{"fields":[{"key":"event", "type":"string", "value":"PutIdleConn"}], "timestamp":1640852441081548},{"fields":[{"key":"event", "type":"string", "value":"ClosedBody"}], "timestamp":1640852441081618}], "operationName":"HTTP GET", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"4f3f03438f880b00", "traceID":"7648504b4a5b7402"}], "spanID":"6cdaa9f0f69c7d39", "startTime":1640852441017368, "tags":[{"key":"span.kind", "type":"string", "value":"client"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"http://0.0.0.0:8083/route?dropoff=728%2C326\u0026pickup=448%2C176"},{"key":"http.url", "type":"string", "value":"0.0.0.0:8083"},{"key":"net/http.reused", "type":"bool", "value":true},{"key":"net/http.was_idle", "type":"bool", "value":true},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":64257, "flags":1, "logs":[], "operationName":"HTTP GET: /route", "processID":"p3", "references":[{"refType":"CHILD_OF", "spanID":"7648504b4a5b7402", "traceID":"7648504b4a5b7402"}], "spanID":"4f3f03438f880b00", "startTime":1640852441017363, "tags":[{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null},
	//					{"duration":706959, "flags":1, "logs":[{"fields":[{"key":"event", "type":"string", "value":"HTTP request received"},{"key":"level", "type":"string", "value":"info"},{"key":"method", "type":"string", "value":"GET"},{"key":"url", "type":"string", "value":"/dispatch?customer=731\u0026nonse=0.6347636392951159"}], "timestamp":1640852440374815},{"fields":[{"key":"event", "type":"string", "value":"Getting customer"},{"key":"customer_id", "type":"string", "value":"731"},{"key":"level", "type":"string", "value":"info"}], "timestamp":1640852440374868},{"fields":[{"key":"event", "type":"string", "value":"Found customer"},{"key":"level", "type":"string", "value":"info"}], "timestamp":1640852440689458},{"fields":[{"key":"event", "type":"string", "value":"baggage"},{"key":"key", "type":"string", "value":"customer"},{"key":"value", "type":"string", "value":"Japanese Desserts"}], "timestamp":1640852440689484},{"fields":[{"key":"event", "type":"string", "value":"Finding nearest drivers"},{"key":"level", "type":"string", "value":"info"},{"key":"location", "type":"string", "value":"728,326"}], "timestamp":1640852440689488},{"fields":[{"key":"event", "type":"string", "value":"Found drivers"},{"key":"level", "type":"string", "value":"info"}], "timestamp":1640852440883803},{"fields":[{"key":"event", "type":"string", "value":"Finding route"},{"key":"dropoff", "type":"string", "value":"728,326"},{"key":"level", "type":"string", "value":"info"},{"key":"pickup", "type":"string", "value":"130,360"}], "timestamp":1640852440883842},{"fields":[{"key":"event", "type":"string", "value":"Finding route"},{"key":"dropoff", "type":"string", "value":"728,326"},{"key":"level", "type":"string", "value":"info"},{"key":"pickup", "type":"string", "value":"73,856"}], "timestamp":1640852440884001},{"fields":[{"key":"event", "type":"string", "value":"Finding route"},{"key":"dropoff", "type":"string", "value":"728,326"},{"key":"level", "type":"string", "value":"info"},{"key":"pickup", "type":"string", "value":"383,838"}], "timestamp":1640852440884101},{"fields":[{"key":"event", "type":"string", "value":"Finding route"},{"key":"dropoff", "type":"string", "value":"728,326"},{"key":"level", "type":"string", "value":"info"},{"key":"pickup", "type":"string", "value":"697,943"}], "timestamp":1640852440933435},{"fields":[{"key":"event", "type":"string", "value":"Finding route"},{"key":"dropoff", "type":"string", "value":"728,326"},{"key":"level", "type":"string", "value":"info"},{"key":"pickup", "type":"string", "value":"148,102"}], "timestamp":1640852440936122},{"fields":[{"key":"event", "type":"string", "value":"Finding route"},{"key":"dropoff", "type":"string", "value":"728,326"},{"key":"level", "type":"string", "value":"info"},{"key":"pickup", "type":"string", "value":"599,555"}], "timestamp":1640852440952805},{"fields":[{"key":"event", "type":"string", "value":"Finding route"},{"key":"dropoff", "type":"string", "value":"728,326"},{"key":"level", "type":"string", "value":"info"},{"key":"pickup", "type":"string", "value":"787,682"}], "timestamp":1640852440982554},{"fields":[{"key":"event", "type":"string", "value":"Finding route"},{"key":"dropoff", "type":"string", "value":"728,326"},{"key":"level", "type":"string", "value":"info"},{"key":"pickup", "type":"string", "value":"14,108"}], "timestamp":1640852440989219},{"fields":[{"key":"event", "type":"string", "value":"Finding route"},{"key":"dropoff", "type":"string", "value":"728,326"},{"key":"level", "type":"string", "value":"info"},{"key":"pickup", "type":"string", "value":"248,98"}], "timestamp":1640852440998743},{"fields":[{"key":"event", "type":"string", "value":"Finding route"},{"key":"dropoff", "type":"string", "value":"728,326"},{"key":"level", "type":"string", "value":"info"},{"key":"pickup", "type":"string", "value":"448,176"}], "timestamp":1640852441017297},{"fields":[{"key":"event", "type":"string", "value":"Found routes"},{"key":"level", "type":"string", "value":"info"}], "timestamp":1640852441081635},{"fields":[{"key":"event", "type":"string", "value":"Dispatch successful"},{"key":"driver", "type":"string", "value":"T761944C"},{"key":"eta", "type":"string", "value":"2m0s"},{"key":"level", "type":"string", "value":"info"}], "timestamp":1640852441081700}], "operationName":"HTTP GET /dispatch", "processID":"p3", "references":[], "spanID":"7648504b4a5b7402", "startTime":1640852440374766, "tags":[{"key":"sampler.type", "type":"string", "value":"probabilistic"},{"key":"sampler.param", "type":"float64", "value":1},{"key":"span.kind", "type":"string", "value":"server"},{"key":"http.method", "type":"string", "value":"GET"},{"key":"http.url", "type":"string", "value":"/dispatch?customer=731\u0026nonse=0.6347636392951159"},{"key":"component", "type":"string", "value":"net/http"},{"key":"http.status_code", "type":"int64", "value":200},{"key":"internal.span.format", "type":"string", "value":"proto"}], "traceID":"7648504b4a5b7402", "warnings":null}], "traceID":"7648504b4a5b7402", "warnings":null}
	//				]
	//	}, nil
	//}

	if traceId == "" {
		return nil, fmt.Errorf("traceId error")
	}

	req, _ := http.NewRequest("Get", r.urls[getTrace]+traceId, nil)
	reqUrl := req.URL.String()

	r.logger.Infof("get trace: ", req.URL)
	resp, err := r.httpClient.Get(reqUrl)

	if err != nil {
		r.logger.Infof("get trace failed ")
	}

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, fmt.Errorf("read body failed")
	}
	//r.logger.Infof("receive body: ", string(body))

	// fmtResp := TraceResponse{}
	fmtResp := request.CommonResp{}
	jsonErr := json.Unmarshal(body, &fmtResp)
	if jsonErr != nil {
		return nil, fmt.Errorf("parse body failed")
	}

	if fmtResp.Errors != nil {
		return nil, fmt.Errorf("request error: %v", fmtResp.Errors)
	}

	defer resp.Body.Close()
	return fmtResp.Data, nil
}

/*
   描述: Serives信息查询

   error:

   eg: r.GetServices()
*/
func (r *SupbTrace) GetServices() (interface{}, error) {
	if r.debugMode {
		return []string{"mysql","jaeger-query","frontend","driver","customer","route","redis"},nil
	}


	req, _ := http.NewRequest("Get", r.urls[getServices], nil)
	reqUrl := req.URL.String()

	r.logger.Infof("get servies: ", req.URL)
	resp, err := r.httpClient.Get(reqUrl)

	if err != nil {
		r.logger.Infof("get servies failed ")
	}

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, fmt.Errorf("read body failed")
	}
	r.logger.Infof("receive body: ", string(body))

	// fmtResp := ServicesResponse{}
	fmtResp := request.CommonResp{}
	jsonErr := json.Unmarshal(body, &fmtResp)
	if jsonErr != nil {
		return nil, fmt.Errorf("parse body failed")
	}

	if fmtResp.Errors != nil {
		return nil, fmt.Errorf("request error: %v", fmtResp.Errors)
	}

	defer resp.Body.Close()
	return fmtResp.Data, nil
}

/*
   描述: Dependencies信息查询
   request := Params

   error:

   eg: r.GetDependencies(Params{
	   End:      "1640923636720", // now time
	   LookBack: "604800000",
   })
*/

func (r *SupbTrace) GetDependenciesDAG(params request.Params) ([]interface{}, error) {
	data , err := r.getDependencies(params)
	if err !=nil {
		return nil, err
	}

	return data.([]interface{}), nil

	//for k,v := range data.(map[string]interface{}){
	//
	//}
}


func (r *SupbTrace) getDependencies(params request.Params) (interface{}, error) {
	if r.debugMode {
		return []map[string]interface{}{
			{"callCount":3,"child":"mysql","parent":"customer"},
			{"callCount":3,"child":"driver","parent":"frontend"},
			{"callCount":3,"child":"customer","parent":"frontend"},
			{"callCount":30,"child":"route","parent":"frontend"},
			{"callCount":41,"child":"redis","parent":"driver"},
		},nil
	}

	// defaultParams := Params{
	// 	End:      "1640052650859",
	// 	LookBack: "604800000",
	// }

	req, _ := http.NewRequest("Get", r.urls[getDependencies], nil)
	query := req.URL.Query()

	if params.End == "" {
		return nil, fmt.Errorf("no end time")
	}
	query.Add("endTs", params.End)

	if params.LookBack == "" {
		return nil, fmt.Errorf("no lookback")
	}
	query.Add("lookback", params.LookBack)

	req.URL.RawQuery = query.Encode()
	reqUrl := req.URL.String()

	//r.logger.Infof("get dependencies: ", req.URL)
	resp, err := r.httpClient.Get(reqUrl)

	if err != nil {
		r.logger.Infof("get dependencies failed ")
	}

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, fmt.Errorf("read body failed")
	}
	r.logger.Infof("receive body: ", string(body))

	// fmtResp := DependenciesResponse{}
	fmtResp := request.CommonResp{}
	jsonErr := json.Unmarshal(body, &fmtResp)
	if jsonErr != nil {
		return nil, fmt.Errorf("parse body failed")
	}

	if fmtResp.Errors != nil {
		return nil, fmt.Errorf("request error: %v", fmtResp.Errors)
	}

	defer resp.Body.Close()
	return fmtResp.Data, nil
}

/*
   描述: Operations信息查询
   service string

   error:

   eg: r.GetOperations("frontend")
*/
func (r *SupbTrace) GetOperations(service string) (interface{}, error) {
	if r.debugMode {
		return []string{
			"HTTP GET",
			"HTTP GET: /customer",
			"HTTP GET: /route",
			"HTTP GET /dispatch",
			"HTTP GET /",
			"/driver.DriverService/FindNearest",
			"HTTP GET /config",
		},nil
	}

	if service == "" {
		return nil, fmt.Errorf("operations error")
	}

	req, _ := http.NewRequest("Get", r.urls[getOperations]+service+`/operations`, nil)

	reqUrl := req.URL.String()

	//r.logger.Infof("get operations: ", req.URL)
	resp, err := r.httpClient.Get(reqUrl)

	if err != nil {
		r.logger.Infof("get operations failed ")
	}

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, fmt.Errorf("read body failed")
	}
	//r.logger.Infof("receive body: ", string(body))

	// fmtResp := OperationsResponse{}
	fmtResp := request.CommonResp{}
	jsonErr := json.Unmarshal(body, &fmtResp)
	if jsonErr != nil {
		return nil, fmt.Errorf("parse body failed")
	}

	if fmtResp.Errors != nil {
		return nil, fmt.Errorf("request error: %v", fmtResp.Errors)
	}

	defer resp.Body.Close()
	return fmtResp.Data, nil
}

func (r *SupbTrace) Close() error {
	r.logger.Infof("%v closed", r.prefix)
	return nil
}
