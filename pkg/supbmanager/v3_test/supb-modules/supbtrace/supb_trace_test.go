package supbtrace

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbtrace/request"
	"github.com/spf13/viper"
	"strconv"
	"testing"
	"time"
)

func TestGetTraces(t *testing.T) {
	ctx := context.Background()
	supbTrace := NewSupbTrace(ctx, "test")
	vp := viper.New()
	supbTrace.ViperConfig(vp)
	supbTrace.InitViper(vp)
	supbTrace.Initialize()

	fmt.Printf("%v\n",strconv.Itoa((int(time.Now().Unix()) - 604800) *1000000))
	fmt.Printf("%v\n",strconv.Itoa((int(time.Now().Unix())) *1000000))
	params := request.Params{
		Service:     "image-process",
		Operation: "image_procss",
		Start:       strconv.Itoa((int(time.Now().Unix()) - 6048000) *1000000),
		End:         strconv.Itoa(int(time.Now().Unix()) * 1000000),
		Limit: "",
		MinDuration: "",
		MaxDuration: "",
	}

	rlt, _ := supbTrace.GetTraces(params)

	fmt.Printf("ss: %v",len(rlt.([]interface{})))
	return

	ids := make([]string, len(rlt.([]interface{})))
	idNotes := make([]string, len(rlt.([]interface{})))
	for k, v := range rlt.([]interface{}) {
		traceId := v.(map[string]interface{})["traceID"].(string)
		ids[k] = traceId
		idNotes[k] = v.(map[string]interface{})["spans"].([]interface{})[0].(map[string]interface{})["operationName"].(string)
	}

	fmt.Printf("%v\n", ids)
	fmt.Printf("%v\n", idNotes)

	//fmt.Printf("%v\n",rlt.([]interface{})[0].(map[string]interface{})["processes"].(map[string]interface{})["p1"].(map[string]interface{})["serviceName"])
	//for k,v :=range rlt.([]interface{}) {
	//	fmt.Printf("\n%v\t\t%v\n",k,v)
	//}

	//	proc := rlt.([]interface{})[0].(map[string]interface{})["processes"].(map[string]interface{})
	//	spans := rlt.([]interface{})[0].(map[string]interface{})["spans"].([]interface{})
	//	srvs := make(map[string]int64,len(proc))
	//	count := make(map[string]int64,len(proc))
	//	//srvsName := make(map[string]string,len(proc))
	//	for k ,_ := range proc{
	//		//sn := v.(map[string]interface{})["serviceName"].(string)
	//		//srvsName[sn] = 0
	//		srvs[k] = 0
	//		count[k] = 0
	//	}
	//
	//	for _,v2 := range spans {
	//		p := v2.(map[string]interface{})["processID"].(string)
	//		srvs[p] += int64(v2.(map[string]interface{})["duration"].(float64))
	//		count[p] ++
	//	}
	//
	//	res := make(map[string]int64,len(proc))
	//	for k,v := range srvs {
	//		res[proc[k].(map[string]interface{})["serviceName"].(string)] = v / count[k]
	//	}
	//
	////[customer:313836 driver:193739 frontend:108212 mysql:313767 redis:14879 route:52861]
	//
	//
	//	fmt.Printf("%v",res)
	//
	//	if err != nil {
	//		fmt.Println(err)
	//	}
	//
		strRlt, err := json.Marshal(rlt)
		if err != nil {
			fmt.Println("marshal error")
		}
		fmt.Println("rlt: ", string(strRlt))
}

func TestGetTrace(t *testing.T) {
	ctx := context.Background()
	supbTrace := NewSupbTrace(ctx, "test")
	vp := viper.New()
	supbTrace.ViperConfig(vp)
	supbTrace.InitViper(vp)
	supbTrace.Initialize()

	rlt, err := supbTrace.GetTrace("b419cc182e934b9c")

	if err != nil {
		fmt.Println(err)
		return
	}

	strRlt, err := json.Marshal(rlt)
	if err != nil {
		fmt.Println("marshal error")
	}

	fmt.Println("rlt: ", string(strRlt))



	//categories := make(map[string]string)
	trace := rlt.([]interface{})[0]
	spans := trace.(map[string]interface{})["spans"].([]interface{})
	spanData,sid := make([][5]interface{},len(spans)),0

	for _,v := range spans {
		refs := v.(map[string]interface{})["references"].([]interface{})
		spanId := v.(map[string]interface{})["spanID"].(string)
		if len(refs) == 0 {
			spanData[0][0] = spanId
			spanData[0][1] = v.(map[string]interface{})["operationName"]
			spanData[0][2] = v.(map[string]interface{})["startTime"]
			spanData[0][4] = v.(map[string]interface{})["duration"]
			spanData[0][3] = spanData[0][2].(float64) + spanData[0][4].(float64)
			break
		}
	}

	lid,tail := 1,1
	for {
		for _,v := range spans {
			refs := v.(map[string]interface{})["references"].([]interface{})
			spanId := v.(map[string]interface{})["spanID"].(string)
			for _, rf := range refs {
				parentId := rf.(map[string]interface{})["spanID"].(string)
				for i:=sid; i< lid;i++ {
					if spanData[i][0].(string) == parentId {
						spanData[tail][0] = spanId
						spanData[tail][1] = v.(map[string]interface{})["operationName"]
						spanData[tail][2] = v.(map[string]interface{})["startTime"]
						spanData[tail][4] = v.(map[string]interface{})["duration"]
						spanData[tail][3] =  spanData[tail][2].(float64) + spanData[tail][4].(float64)
						tail++
					}
				}
			}
		}
		sid = lid
		lid = tail
		if tail >= len(spans) {
			break
		}
	}

	respData := make([][5]interface{},len(spans))
	for k,v := range spanData {
		respData[len(spans)-1-k] = v
	}

	fmt.Printf("\n%v\n",respData)
	//
	//strRlt, err := json.Marshal(rlt)
	//if err != nil {
	//	fmt.Println("marshal error")
	//}
	//
	//fmt.Println("rlt: ", string(strRlt))
}

func TestGetService(t *testing.T) {
	ctx := context.Background()
	supbTrace := NewSupbTrace(ctx, "test")
	vp := viper.New()
	supbTrace.ViperConfig(vp)
	vp.SetDefault("test.debug",false)
	vp.SetDefault("test.jaeger.ip","39.101.140.145")
	vp.SetDefault("test.jaeger.port","10046")
	supbTrace.InitViper(vp)
	supbTrace.Initialize()

	rlt, err := supbTrace.GetServices()

	if err != nil {
		fmt.Println(err)
	}

	strRlt, err := json.Marshal(rlt)
	if err != nil {
		fmt.Println("marshal error")
	}
	fmt.Println("rlt: ", string(strRlt))
}

func TestGetDependencies(t *testing.T) {
	ctx := context.Background()
	supbTrace := NewSupbTrace(ctx, "test")
	vp := viper.New()
	supbTrace.ViperConfig(vp)
	supbTrace.InitViper(vp)
	supbTrace.Initialize()

	fmt.Printf("%v\n",int(time.Now().Unix()))
	rlt, err := supbTrace.GetDependenciesDAG(request.Params{
		End: strconv.Itoa(int(time.Now().Unix()*1000)),
		// LookBack: "1",
		LookBack: "604800000",
	})

	if err != nil {
		fmt.Println(err)
	}

	strRlt, err := json.Marshal(rlt)
	if err != nil {
		fmt.Println("marshal error")
	}
	fmt.Println("rlt: ", string(strRlt))
}

func TestGetOperations(t *testing.T) {
	ctx := context.Background()
	supbTrace := NewSupbTrace(ctx, "test")
	vp := viper.New()
	supbTrace.ViperConfig(vp)
	supbTrace.InitViper(vp)
	supbTrace.Initialize()

	rlt, err := supbTrace.GetOperations("frontend")

	if err != nil {
		fmt.Println(err)
	}

	strRlt, err := json.Marshal(rlt)
	if err != nil {
		fmt.Println("marshal error")
	}
	fmt.Println("rlt: ", string(strRlt))
}
