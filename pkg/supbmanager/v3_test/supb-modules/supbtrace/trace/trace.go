package trace

import "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbtrace/trace/format"

type TracesELK struct {
	TraceID       format.TraceID     `json:"traceID"`
	SpanID        format.SpanID      `json:"spanID"`
	ParentSpanID  format.SpanID      `json:"parentSpanID,omitempty"` // deprecated
	Flags         uint32             `json:"flags,omitempty"`
	OperationName string             `json:"operationName"`
	References    []format.Reference `json:"references"`
	StartTime     uint64             `json:"startTime"` // microseconds since Unix epoch
	// ElasticSearch does not support a UNIX Epoch timestamp in microseconds,
	// so Jaeger maps StartTime to a 'long' type. This extra StartTimeMillis field
	// works around this issue, enabling timerange queries.
	StartTimeMillis uint64            `json:"startTimeMillis"`
	Duration        uint64            `json:"duration"` // microseconds
	Tags            []format.KeyValue `json:"tags"`
	// Alternative representation of tags for better kibana support
	Tag     map[string]interface{} `json:"tag,omitempty"`
	Logs    []format.Log           `json:"logs"`
	Process format.Process         `json:"process,omitempty"`
}
