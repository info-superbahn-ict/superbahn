package webreq

import "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_strategies/strategies"

const (
	// get
	OpListStaticStrategies  = "listStaticStrategies"
	OpListRunningStrategies = "listRunningStrategies"
	OpListApplications = 	"listApplications"
	OpListImage             = "listImage"

	OpGetStaticStrategy  = "getStaticStrategies"
	OpGetRunningStrategy = "getRunningStrategies"
	OpGetClusterInfo     = "getClusterInfo"
	//OpGetClusterDevices = "getClusterDevices"
	//OpGetClusterServices = "getClusterServices"
	OpGetClusterResources     = "getClusterResources"
	OpGetStatisticClusterInfo = "getStatisticClusterInfo"
	//OpGetDevicesStatistic = "getDevicesStatistic"
	//OpGetServiceStatistic = "getServicesStatistic"
	OpGetSpecificResources   = "getSpecificResources"
	OpListResourcesStatistic = "listResourcesStatistic"
	OpGetRelationshipGraph   = "getRelationshipGraph"

	// delete
	OpDeleteStaticStrategy  = "deleteStaticStrategy"
	OpDeleteRunningStrategy = "deleteRunningStrategy"
	OpDeleteImage           = "deleteImage"

	// load
	OpLoadStrategy = "loadStrategy"
	OpLoadImage    = "loadImage"

	// upload
	OpUploadStrategy = "uploadStrategy"
	OpUploadImage    = "uploadImage"

	// update
	OpUpdateStaticStrategy = "updateStaticStrategy"
	OpUpdateRunningStrategy = "updateRunningStrategy"

	// run
	OpRunStrategy = "runStaticStrategy"
	OpRunApplication = "runApplication"

	//stop
	OpStopRunningStrategy = "stopRunningStrategy"

	//restart
	OpRestartStrategy = "restartStrategy"

	// push
	OpPushStaticStrategy = "pushStaticStrategy"

	// pull
	OpPullStaticStrategy = "pullStaticStrategy"
)

type Request struct {
	Op       string               `json:"op"`
	Object   string               `json:"object"`
	Data     string               `json:"data"`
	Strategy *strategies.Strategy `json:"strategy"`
}
