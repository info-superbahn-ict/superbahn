package kafka_nervous

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"

	//"os"
	//"os/signal"
	//"syscall"
	"testing"
)

func TestTicker(t *testing.T) {
	tryTime := 10
	tryInterval := 500
	for i:=0;i<tryTime;i++ {
		c := time.NewTicker(time.Millisecond * time.Duration(tryInterval))
		select {
		case <-c.C:
			fmt.Printf("%v\n",time.Now())
			continue
		}
	}
}

func TestJson(t *testing.T){
	m :=new(MethodInfo)
	m.FromJson("{\"source_guid\":\"ff\",\"target_guid\":\"aa\",\"register_name\":\"rn\",\"parma_array\":[3,\"in\"]}")
	fmt.Println(m.SourceGuid)
	fmt.Println(m.TargetGuid)
	fmt.Println(m.RegisterName)
	fmt.Println(m.ParamArray)
	jsonStr,_:=m.ToJson()
	fmt.Println(string(jsonStr))
}

func TestLocalSendReceive(t *testing.T){
	n,_:= NewNervous(context.Background(),"./nervous_config.json","test917")
	wg:=sync.WaitGroup{}
	wg.Add(1)
	n.Subscribe("test917t")
	go func(){
		msg,_:=n.Receive("test917t")
		fmt.Println(msg.Topic)
		fmt.Println(msg.Key)
		fmt.Println(msg.Value)
		wg.Done()
	}()
	n.Send("test917t","keyf","fff")
	wg.Wait()
	n.Close()
}

func TestLocalRPC(t *testing.T){
	c,_:= NewNervous(context.Background(),"../../../config/nervous_config.json","testrpcserver")
	//_ = c.Run()
	//s,_:= NewNervous(context.Background(),"./nervous_config.json","testrpcserver")
	//s.StartRPCServer()
	testAdd := func(toAdd ...interface{})(interface{},error){
		fmt.Println("in testAdd")
		//return toAdd[0].(float64)+1,nil
		return nil,nil
	}
	/*testFuncList := func(args ...interface{})(interface{},error){
		return s.RPCList()
	}*/
	_ = c.RPCRegister("testAdd",testAdd)
	//s.RPCRegister("funcList",testFuncList)
	if result,err:=c.RPCCall("testrpcserver","testAdd",2);err!=nil{
		fmt.Printf("mock call error:%v\n",err)
	}else{
		fmt.Printf("result: %d\n",int(result.(float64)))
	}
	if result,err:=c.RPCCall("testrpcserver","funcList");err!=nil{
		fmt.Printf("mock call error:%v\n",err)
	}else{
		if result == nil{
		fmt.Printf("result is nil\n")
		c.Close()
		return
		}
		fmt.Println("rcp",result)
		var tmpR []interface{}
		tmpR = result.([]interface{})
		for i:=0;i<len(tmpR);i++{
			fmt.Println(tmpR[i].(string))
		}
	}
	fmt.Println("close mock client and web_server")
	c.Close()
}
func TestLocalRPC2(t *testing.T) {

	for i:=0;i<10 ; i++ {

		c,_:= NewNervous(context.Background(),"../../../config/nervous_config.json","testrpcserver")

		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)
		c.RPCRegister(fmt.Sprintf("%v_%v",i,rand.Int()),nil)

		list, _ := c.RPCList()
		fmt.Printf("%v\n",len(list))
		c.Close()
	}


}
/*
func TestSend(t *testing.T){
	fmt.Println("send?")
	ctx, cancel := context.WithCancel(context.Background())
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	k, err := NewNervous(ctx, "./nervous_config.json","")
	if err != nil {
		fmt.Printf("new spongeregister %v", err)
		return
	}


	err = k.Send("resport","hello world")
	if err != nil {
		fmt.Printf("send %v", err)
		return
	}
}

func TestRecv(t *testing.T){
	ctx, cancel := context.WithCancel(context.Background())
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	k, err := NewNervous(ctx, "./nervous_config.json","")
	if err != nil {
		fmt.Printf("new spongeregister %v", err)
		return
	}

	err = k.Subscribe("report")
	if err != nil {
		fmt.Printf("add topic %v", err)
		return
	}

	value,err:=k.Receive("report")
	if err != nil {
		fmt.Printf("recv %v", string(value))
		return
	}
}*/
