package kafka_nervous
import(
	"encoding/json"
)
type BaseInfo struct{
	SourceGuid string `json:"source_guid"`
	TargetGuid string `json:"target_guid"`
}

type MethodInfo struct{
	BaseInfo
	RegisterName string `json:"register_name"`
	ParamArray []interface{} `json:"param_array"`
}

type ReturnInfo struct{
	BaseInfo
	Status int `json:"status"`
	ReturnValue interface{} `json:"return_value"`
}

func (m *MethodInfo)FromJson(jsonStr string)error{
	return json.Unmarshal([]byte(jsonStr),m)
}

func (m *MethodInfo)ToJson()(string,error){
	jsonBytes,err:= json.Marshal(*m)
	return string(jsonBytes),err
}

func (r *ReturnInfo)FromJson(jsonStr string)error{
	return json.Unmarshal([]byte(jsonStr),r)
}

func (r *ReturnInfo)ToJson()(string,error){
	jsonBytes,err:=json.Marshal(*r)
	return string(jsonBytes),err
}
