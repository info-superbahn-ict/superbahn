package app

import (
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbsponge/logging"
	"github.com/astaxie/beego/validation"
)

func MarkErrors(errors []*validation.Error) {
	for _, err := range errors {
		logging.Info(err.Key, err.Message)
	}

	return
}
