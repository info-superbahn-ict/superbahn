package e

var MsgFlags = map[int]string{
	SUCCESS:                          "ok",
	ERROR:                            "fail",
	INVALID_PARAMS:                   "请求参数错误",
	ERROR_EXIST_RESOURCE:             "已存在该资源名称",
	ERROR_EXIST_RESOURCE_FAIL:        "获取已存在资源失败",
	ERROR_NOT_EXIST_RESOURCE:         "该资源不存在",
	ERROR_GET_RESOURCES_FAIL:         "获取所有资源失败",
	ERROR_COUNT_RESOURCE_FAIL:        "统计资源失败",
	ERROR_ADD_RESOURCE_FAIL:          "新增资源失败",
	ERROR_EDIT_RESOURCE_FAIL:         "修改资源失败",
	ERROR_DELETE_RESOURCE_FAIL:       "删除资源失败",
	ERROR_EXPORT_RESOURCE_FAIL:       "导出资源失败",
	ERROR_IMPORT_RESOURCE_FAIL:       "导入资源失败",
	ERROR_DISCONNECT_RESOURCE_FAIL:   "断开资源失败",
	ERROR_GET_RELATIONMAP_FAIL:       "获取设备服务关联图失败",
	ERROR_GET_RELATIONRESOURCES_FAIL: "获取设备关联资源失败",
	ERROR_GET_SUNBURSTMAP_FAIL:       "获取旭日关系图失败",
	ERROR_GET_RELATIONGRAPH_FAIL:     "获取特定guId关联关系图失败",
	ERROR_GET_UTILIZATION_FAIL:       "获取利用率统计信息失败",
	ERROR_GET_TAGGROUPINFOS_FAIL:     "获取tag分组信息失败",
	ERROR_GET_ATLASMAP_FAIL:          "获取应用服务图谱失败",
	ERROR_AUTH_CHECK_TOKEN_FAIL:      "Token鉴权失败",
	ERROR_AUTH_CHECK_TOKEN_TIMEOUT:   "Token已超时",
	ERROR_AUTH_TOKEN:                 "Token生成失败",
	ERROR_AUTH:                       "Token错误",
	ERROR_UPLOAD_SAVE_IMAGE_FAIL:     "保存图片失败",
	ERROR_UPLOAD_CHECK_IMAGE_FAIL:    "检查图片失败",
	ERROR_UPLOAD_CHECK_IMAGE_FORMAT:  "校验图片错误，图片格式或大小有问题",
}

var TypeNames = map[int]string{
	1: "服务器",
	2: "个人计算机",
	3: "容器",
	4: "非计算机的外部设备",
	5: "总线",
	6: "控制中心",
	7: "策略",
	8: "集群",
}

var EngTypeNames = map[int]string{
	1: "Server",
	2: "PC",
	3: "Docker",
	4: "External Device",
	5: "Bus",
	6: "Control",
	7: "Strategy",
	8: "Cluster",
}

var UtiString = map[int]string{
	1: "0%~25%",
	2: "25%~50%",
	3: "50%~75%",
	4: "75%~100%",
}

var UtiQueryStrs = []string{"'0%~25%' as name,sum(case when utilization<25 then 1 else 0 end) as value", "'25%~50%' as name,sum(case when utilization>=25 and utilization<50 then 1 else 0 end) as value", "'50%~75%' as name,sum(case when utilization>=50 and utilization<75 then 1 else 0 end) as value", "'75%~100%' as name,sum(case when utilization>=75 and utilization<100 then 1 else 0 end) as value"}

//var UtiQueryString = map[int]string{
//	1:" < 25",
//	2:""
//}

func GetMsg(code int) string {
	msg, ok := MsgFlags[code]
	if ok {
		return msg
	}

	return MsgFlags[ERROR]
}

func GetTypeName(oType int) string {
	name, ok := TypeNames[oType]
	if ok {
		return name
	}
	return TypeNames[ERROR]
}

func GetEngTypeName(oType int) string {
	name, ok := EngTypeNames[oType]
	if ok {
		return name
	}
	return EngTypeNames[ERROR]
}
