package kafka

import (
	"context"
	"encoding/json"
	"fmt"
	kafkaNervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous/kafka-nervous"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"gitee.com/info-superbahn-ict/superbahn/sync/spongeregister"
	"testing"
)

const path = "../../../config/nervous_config.json"

var GUID string

func Test_registerDevice(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, "nervous_config.json", "test")
	if err != nil {
		t.Errorf("new kafkaNervous error: %v\n", err)
		return
	}

	tag := spongeregister.Tag{
		Key:   "key1",
		Type:  "1",
		Value: "111",
	}
	var tags []spongeregister.Tag
	tags = append(tags, tag)
	res := &spongeregister.Resource{
		Description: "ff:00:00:2e:45:66+2021e-80132-82079",
		Status:      1,
		OType:       2,
		IsMonitor:   1,
		IsControl:   1,
		//AutoRemove:  1,
		Tags: tags,
	}
	bts, _ := json.Marshal(res)
	fmt.Println(string(bts))
	resp, err := Nic.RPCCall(define.INIT_SPONGE_NERVOUS, define.REGISTER_DEVICE, "")
	if err != nil {
		t.Errorf("RPC call failed: %v", err)
		return
	}
	fmt.Printf("%s\n", resp)
	GUID = resp.(string)
	Nic.Close()
}

func Test_getDeviceInfos(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, path, "test")
	if err != nil {
		t.Errorf("new kafkaNervous error: %v\n", err)
		return
	}
	defer Nic.Close()

	resp, err := Nic.RPCCall(define.INIT_SPONGE_OCTOPUS, define.LIST_INFOS, " ")
	if err != nil {
		t.Errorf("call kafkaNervous error: %v\n", err)
		return
	}
	fmt.Printf("%v\n", resp)
}

func Test_checkProperty(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, path, "test")
	if err != nil {
		t.Errorf("new kafkaNervous error: %v\n", err)
		return
	}
	defer Nic.Close()
	guids := [...]string{GUID, "aa", "111"}

	resp, err := Nic.RPCCall(define.INIT_SPONGE_OCTOPUS, define.CHECK_PROPERTY, guids)
	if err != nil {
		t.Errorf("call kafkaNervous error: %v\n", err)
		return
	}
	fmt.Printf("%v\n", resp)
}

func Test_listTagGroupInfos(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, path, "test")
	if err != nil {
		t.Errorf("new kafkaNervous error: %v\n", err)
		return
	}
	defer Nic.Close()

	resp, err := Nic.RPCCall(define.INIT_SPONGE_OCTOPUS, define.LIST_ALIVE_AGENTS)
	if err != nil {
		t.Errorf("call kafkaNervous error: %v\n", err)
		return
	}
	fmt.Printf("%s\n", resp.(string))
}

func Test_syncPush(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, path, "test")
	if err != nil {
		t.Errorf("new kafkaNervous error: %v\n", err)
		return
	}
	defer Nic.Close()
	flag := define.SPONGE_SYNC_PUSH_OFF

	resp, err := Nic.RPCCall(define.INIT_SPONGE_OCTOPUS, define.SPONGE_SYNC_PUSH, flag)
	if err != nil {
		t.Errorf("call kafkaNervous error: %v\n", err)
		return
	}
	fmt.Printf("%v\n", resp)
}

func Test_updateStatus(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, path, "test")
	if err != nil {
		t.Errorf("new kafkaNervous error: %v\n", err)
		return
	}
	defer Nic.Close()
	infos := spongeregister.SyncInfo{
		Guids: make(map[string]int),
		//Metrics: make(map[string]spongeregister.SyncMetric),
	}
	//infos.Guids[GUID] = 0
	infos.Guids["01df258f00"] = 0
	bts, _ := json.Marshal(infos)

	resp, err := Nic.RPCCall(define.INIT_SPONGE_NERVOUS, define.UPDATE_STATUS, string(bts))
	if err != nil {
		t.Errorf("call kafkaNervous error: %v\n", err)
		return
	}
	fmt.Printf("%v\n", resp)
}

func Test_reconnect(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, path, "test")
	if err != nil {
		t.Errorf("new kafkaNervous error: %v\n", err)
		return
	}
	defer Nic.Close()
	guids := GUID

	resp, err := Nic.RPCCall(define.INIT_SPONGE_NERVOUS, define.RECONNECT, guids)
	if err != nil {
		t.Errorf("call kafkaNervous error: %v\n", err)
		return
	}
	fmt.Printf("%v\n", resp)
}

func Test_renewalDevice(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, path, "test")
	if err != nil {
		t.Errorf("new kafkaNervous error: %v\n", err)
		return
	}
	defer Nic.Close()
	guids := GUID

	resp, err := Nic.RPCCall(define.INIT_SPONGE_NERVOUS, define.RENEWAL_DEVICE, guids)
	if err != nil {
		t.Errorf("call kafkaNervous error: %v\n", err)
		return
	}
	fmt.Printf("%v\n", resp)
}

func Test_deleteResource(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, path, "test")
	if err != nil {
		t.Errorf("new kafkaNervous error: %v\n", err)
		return
	}
	defer Nic.Close()
	//guids := GUID

	res := spongeregister.Resource{
		GuId: "0126d73600",
	}
	bts, _ := json.Marshal(res)

	resp, err := Nic.RPCCall(define.INIT_SPONGE_NERVOUS, define.DELETE_RESOURCE, string(bts))
	if err != nil {
		t.Errorf("call kafkaNervous error: %v\n", err)
		return
	}
	fmt.Printf("%v\n", resp)
}

func Test_deleteResources(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, path, "test")
	if err != nil {
		t.Errorf("new kafkaNervous error: %v\n", err)
		return
	}
	defer Nic.Close()
	info := spongeregister.Resource{
		Status: 0,
	}
	bts, _ := json.Marshal(info)

	resp, err := Nic.RPCCall(define.INIT_SPONGE_NERVOUS, define.DELETE_RESOURCES, string(bts))
	if err != nil {
		t.Errorf("call kafkaNervous error: %v\n", err)
		return
	}
	fmt.Printf("%v\n", resp)
}

func Test_updateTag(t *testing.T) {
	ctx := context.Background()
	Nic, err := kafkaNervous.NewNervous(ctx, path, "test")
	if err != nil {
		t.Errorf("new kafkaNervous error: %v\n", err)
		return
	}
	defer Nic.Close()
	tag := spongeregister.Tag{
		Key:   "key_test",
		Type:  "string",
		Value: "111",
	}
	info := spongeregister.SyncTagInfo{
		GuId: "01df258f00",
		Tag:  &tag,
	}

	bts, _ := json.Marshal(info)

	resp, err := Nic.RPCCall(define.INIT_SPONGE_NERVOUS, define.UPDATE_TAG, string(bts))
	if err != nil {
		t.Errorf("call kafkaNervous error: %v\n", err)
		return
	}
	fmt.Printf("%v\n", resp)
}
