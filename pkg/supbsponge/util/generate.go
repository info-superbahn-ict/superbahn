package util

import (
	"bytes"
	"encoding/binary"
	"reflect"
	"strconv"
	"unsafe"
)

type DeviceInfo struct {
	ManufacturerId string
	ProductId      string
	ResourceType   int
	ResourceId     int
}

func GenerateDeviceInfo(di DeviceInfo) string {
	reByte := make([]byte, 0)
	reByte = append(reByte, stringToBytes(di.ManufacturerId)...)
	reByte = append(reByte, stringToBytes(di.ProductId)...)
	reByte = append(reByte, stringToBytes(strconv.Itoa(di.ResourceType))...)
	reByte = append(reByte, stringToBytes(strconv.Itoa(di.ResourceId))...)

	return string(reByte)
}

func intToBytes(number int) []byte {
	data := int64(number)
	byteBuf := bytes.NewBuffer([]byte{})
	binary.Write(byteBuf, binary.BigEndian, data)
	return byteBuf.Bytes()
}

func stringToBytes(s string) []byte {
	sh := (*reflect.StringHeader)(unsafe.Pointer(&s))
	bh := reflect.SliceHeader{sh.Data, sh.Len, 0}
	return *(*[]byte)(unsafe.Pointer(&bh))
}
