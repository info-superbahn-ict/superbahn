package util

import "math/rand"

func RandInt(min, max int) int {
	i := rand.Intn(max)
	if i < min {
		return RandInt(min, max)
	}
	return i
}
