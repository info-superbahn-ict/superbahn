package supb_modules

import (
	"bytes"
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbrpc"
	nervous "gitee.com/info-superbahn-ict/superbahn/pkg/supbnervous"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"strings"
)

const (
	SupbRpc       = "rpc"
)

var SupbModules = []string{SupbRpc}

type SupbManager struct {
	rpcMgr *supbrpc.SupbRpc

	logger log.Logger
}

func NewSupbManager(ctx context.Context, prefix string) *SupbManager {
	return &SupbManager{

		rpcMgr: supbrpc.NewSupbRpc(ctx, strings.Join([]string{prefix, SupbRpc}, ".")),

		logger: log.NewLogger(prefix),
	}
}

func (r *SupbManager) InitFlags(flags *pflag.FlagSet) {
	r.rpcMgr.InitFlags(flags)
}

func (r *SupbManager) ViperConfig(viper *viper.Viper) {
	r.rpcMgr.ViperConfig(viper)
}

func (r *SupbManager) InitViper(viper *viper.Viper) {
	r.rpcMgr.InitViper(viper)
}

func (r *SupbManager) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}


func (r *SupbManager) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
	r.rpcMgr.Initialize()
}

func (r *SupbManager) Close() error {
	errs := new(bytes.Buffer)
	if err := r.rpcMgr.Close(); err != nil {
		errs.WriteString(fmt.Sprintf("close resource manager %v\n", err))
	}

	if errs.Len() > 0 {
		return fmt.Errorf("%v", errs)
	}
	return nil
}

func (r *SupbManager) RPC() nervous.Controller{
	return r.rpcMgr.RPC()
}
