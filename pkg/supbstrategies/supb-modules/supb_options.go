package supb_modules

import "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbrpc"

type option func(resources *SupbManager)


func WithGUID(guid string) option {
	return func(mgr *SupbManager) {
		mgr.rpcMgr.OptionConfig(supbrpc.WithGUID(guid))
	}
}
