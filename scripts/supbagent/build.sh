work_dir="../.."

mkdir -p ${work_dir}/build/bin
mkdir -p ${work_dir}/build/plugins/agent

go build -mod vendor -o ${work_dir}/build/bin/agent ${work_dir}/cmd/supbagent/main.go
go build -mod vendor --buildmode=plugin -o ${work_dir}/build/plugins/agent/nervous_host.so ${work_dir}/internal/supbagent/plugins/nervous_host_cli/...
go build -mod vendor --buildmode=plugin -o ${work_dir}/build/plugins/agent/nervous_resources.so ${work_dir}/internal/supbagent/plugins/nervous_resources_cli/...
go build -mod vendor --buildmode=plugin -o ${work_dir}/build/plugins/agent/heartbeat.so ${work_dir}/internal/supbagent/plugins/heartbeat/...



