supb_test=`docker ps -a | grep supbtest:v0.1 | awk '{print $1}' `
jaeger_env=`docker ps -a | grep jaeger-env:v0.10 | awk '{print $1}' `
cpu_ctrl=`docker ps -a | grep cpu-ctrl-core-1 | awk '{print $1}' `
sha1sum=`docker ps -a | grep sha1sum_test | awk '{print $1}' `
jamming_cpu=`docker ps -a | grep jamming_cpu | awk '{print $1}' `


docker stop $supb_test
docker rm $supb_test

docker stop $jaeger_env
docker rm $jaeger_env

docker stop $cpu_ctrl
docker rm $cpu_ctrl

docker stop $sha1sum
docker rm $sha1sum


docker stop $jamming_cpu
docker rm $jamming_cpu
