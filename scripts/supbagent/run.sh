# workdir
work_dir="../.."

cgroup_path_perfix="/system.slice"
#ip="10.16.0.180"
#host="23750"

#ip="172.16.31.37"
#host="2375"

ip="0.0.0.0"
host="2375"
host_tag="CPU"


NERVOUS=${work_dir}/config/nervous_config.json
LOG=${work_dir}/build/log/control.log

# run
${work_dir}/build/bin/agent   --nvn=$NERVOUS --log $LOG --docker=tcp://${ip}:${host} --supb-collector-url=${ip}:10013 --supb-collector-route="/collector" --jaeger-agent=${ip}:10035 --host-tag=${host_tag} --container-cgroup-perfix=${cgroup_path_perfix}

