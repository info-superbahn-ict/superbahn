# workdir
work_dir="../.."
guid=$1

# run
go build -mod vendor -o ${work_dir}/build/bin/supbctl ${work_dir}/cmd/supbctl/main.go


${work_dir}/build/bin/supbctl agent create -c $guid -e SUPB_T=60 -e SUPB_I=0.04 -i cpu-ctrl-core-1:v0.1 -p 10066:10067
