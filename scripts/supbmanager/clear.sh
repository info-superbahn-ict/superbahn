metric_cid=`docker ps -a | grep metrics_statistic | awk '{print $1}' `
log_cid=`docker ps -a | grep log_dump_to_elk | awk '{print $1}' `
trace_cid=`docker ps -a | grep trace_dump_to_elk | awk '{print $1}' `
simple_cid=`docker ps -a | grep simple_test | awk '{print $1}' `


docker stop $metric_cid
docker rm $metric_cid

docker stop $log_cid
docker rm $log_cid

docker stop $trace_cid
docker rm $trace_cid

docker stop $simple_cid
docker rm $simple_cid

etcdctl del / --prefix
