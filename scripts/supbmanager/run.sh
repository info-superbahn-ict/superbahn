# workdir
work_dir="../.."

ELK_URL=http://152.136.134.100:9200
ETCD_URL=10.16.0.180:10000
NERVOUS=${work_dir}/config/nervous_config.json
PLUGINS=$HOME/.kube/config
LOG=$HOME/control.log

# run
${work_dir}/build/bin/controler --elk $ELK_URL --etcd $ETCD_URL --nvn=$NERVOUS --log $LOG
