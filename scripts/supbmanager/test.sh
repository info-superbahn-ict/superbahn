# workdir
work_dir="../.."
guid=$1

# run
go build -mod vendor -o ${work_dir}/build/bin/supbctl ${work_dir}/cmd/supbctl/main.go

${work_dir}/build/bin/supbctl manager get --type=strategies --all
#${work_dir}/build/bin/supbctl manager run --type=strategies TRACE_DUMPER
#${work_dir}/build/bin/supbctl manager run --type=strategies LOG_DUMP_ELK
${work_dir}/build/bin/supbctl manager run --type=strategies METRICS_STATISTIC
${work_dir}/build/bin/supbctl manager run --type=strategies SIMPLE_TEST

#${work_dir}/build/bin/supbctl manager bind --master=trace_dumper --slave=${guid}
#${work_dir}/build/bin/supbctl manager bind --master=log_dumper --slave=${guid}
${work_dir}/build/bin/supbctl manager bind --master=metrics_dumper --slave=${guid}
#${work_dir}/build/bin/supbctl manager bind --master=simple_test --slave=${guid}
