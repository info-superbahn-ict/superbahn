# 
name="simple-test"
if [ ! -n "$1" ] ;then
    name="simple-test"
else
    name=$1
fi

# build
go build -mod vendor -o ../../build/bin/strategies ../../cmd/supbstrategies/v3_test/main.go

# build images
cp ../../build/bin/strategies image/
cp ../../config/nervous_config.json image/
docker rmi $name
docker build --tag $name image/

