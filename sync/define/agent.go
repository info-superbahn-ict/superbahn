package define

const (
	// this is error, because agent has guid
	RPCCommonGuidOfAgent = "agent"

	// guid of host
	RPCFunctionNameOfAgentForListObject   = "list.resource"
	RPCFunctionNameOfAgentForGetAgentInfo = "get.agent.info"
	RPCFunctionNameOfAgentForCreateObject = "agent.create"
	RPCFunctionNameOfAgentForDeployment   = "agent.deploy"
	RPCFunctionNameOfAgentForDeleteObject = "agent.delete"
	RPCFunctionNameOfAgentForStopObject   = "agent.stop"
	RPCFunctionNameOfAgentForResumeObject = "agent.resume"

	// guid of container
	RPCFunctionNameOfObjectRunningOnAgentForListFunction    = "list.function"
	RPCFunctionNameOfObjectRunningOnAgentForStartObject     = "run.function"
	RPCFunctionNameOfObjectRunningOnAgentForStopObject      = "stop.function"
	RPCFunctionNameOfObjectRunningOnAgentForResumeObject    = "resume.function"
	RPCFunctionNameOfObjectRunningOnAgentForOpenMetricsPush = "push.metrics.on.off"
	RPCFunctionNameOfObjectRunningOnAgentForOpenLogsPush    = "push.logs.on.off"
	RPCFunctionNameOfObjectRunningOnAgentForOpenTracePush   = "push.traces.on.off"

	// resource limitaion
	RPCFunctionNameOfObjectRunningOnAgentForGetLimitationConfig     = "get.limitation.config"
	RPCFunctionNameOfObjectRunningOnAgentForSetCpuRatio             = "set.cpu.ratio"
	RPCFunctionNameOfObjectRunningOnAgentForSetCpuRatioShare        = "set.cpu.ratio.share"
	RPCFunctionNameOfObjectRunningOnAgentForSetCpuSets              = "set.cpu.sets"
	RPCFunctionNameOfObjectRunningOnAgentForSetNetUpstreamBandwidth = "set.net.upstream.bandwidth"
	RPCFunctionNameOfObjectRunningOnAgentForSetNetLatency           = "set.net.latency"
)
