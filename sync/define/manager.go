package define

const (
	RPCCommonGuidOfManager = "manager"
	RPCCommonGuidOfManagerTest = "manager.test"

	LogsPrintCommonLabelOfFunction = "function"



	RPCFunctionNameOfManagerStrategyCollectorObjectMetrics       = "push_metrics"
	RPCFunctionNameOfManagerStrategyCollectorObjectLogs          = "push_logs"
	RPCFunctionNameOfManagerStrategyCollectorObjectTrace          = "push_trace"
	RPCFunctionNameOfManagerReceiveResourceSyncMessageFromSponge = "function_name_sponge_sync_object_to_manager"
)


