package define

const (
	INIT_CLIENT_2_SPONGE  = "init_client_2_sponge"
	INIT_OCTOPUS_2_SPONGE = "init_octopus_2_sponge"

	INIT_SPONGE_NERVOUS = "init_sponge_nervous"
	INIT_SPONGE_OCTOPUS = "sponge"

	LIST_INFOS     = "listInfos"
	CHECK_PROPERTY = "checkProperty"

	REGISTER_DEVICE  = "registerDevice"
	DELETE_RESOURCE  = "deleteResource"
	DELETE_RESOURCES = "deleteResources"
	RECONNECT        = "reconnect"
	RENEWAL_DEVICE   = "renewalDevice"
	UPDATE_STATUS    = "updateStatus"
	LIST_TAGGROUPINFOS = "listTagGroupInfos"
	LIST_ALIVE_AGENTS = "listAliveAgents"
	UPDATE_TAG = "updateTag"

	SPONGE_SYNC_PUSH     = "push"
	SPONGE_SYNC_PUSH_ON  = "on"
	SPONGE_SYNC_PUSH_OFF = "off"
)

// todo 默认注册guid
