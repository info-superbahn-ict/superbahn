package spongeregister

//类型
const (
	TypeServer        = 1 //服务器
	TypePc            = 2 //个人计算机
	TypeDocker        = 3 //容器
	TypeDevice        = 4 //非计算机的外部设备
	TypeNervous       = 5 //总线
	TypeControlCenter = 6 //控制中心
	TypeStrategy      = 7 //策略
	TypeCluster       = 8 //集群

	StatusPending     = 1
	StatusReady       = 2
	StatusPause       = 3
	StatusError       = 4
	StatusTerminating = 5
	StatusTerminated  = 6

	OpADD = "add"
	OpDEL = "delete"

	// 掉线、重连、更新
	OpUPD = "update"
)

type SyncInfo struct {
	// map [guid] status
	Guids map[string]int `json:"guids"`

	Metrics map[string]SyncMetric `json:"metrics"`
}

type SyncMetric struct {
	CpuSet   string
	CpuRatio float64
}

type TagGroupSearchInfo struct {
	OType  int `json:"oType"`
	Status int `json:"status"`
	Tag    Tag `json:"tag"`
	//SlaveKey    string `json:"key"`
	//Value  string `json:"value"`
}

type ListResources struct {
	// map guid to resource
	Resources map[string]Resource `json:"resources"`
}

type ResourceSync struct {
	Op string `json:"op"`

	// 注册生成的guid
	GuId string `json:"guId"`

	// 注册时提供的唯一信息
	// host: host_name + mac
	// container: image_id + container_name
	Description string `json:"description"`

	// 状态
	Status int    `json:"status"`
	Area   string `json:"area"`

	// 等同depend_node
	PreNode string `json:"preNode"`
	SubNode string `json:"subNode"`

	// 类型
	OType int `json:"oType"`

	// 支持监控
	IsMonitor int `json:"isMonitor"`

	// 支持控制
	IsControl int `json:"isControl"`

	//// 支持自动删除
	//AutoRemove int `json:"autoRemove"`

	Tags []Tag `json:"tags"`
}

// application reference
// type Tag struct {
// 	// 用户定义
// 	applicationKey string

// 	// 当前 app 的上游
// 	parentApplicationKeys []string

// 	// 当前 app 的下游
// 	childApplicationKeys []string
// }

type Tag struct {
	Key   string	`json:"key"`
	Type  string	`json:"type"`
	Value interface{}	`json:"value"`
}

type SyncTagInfo struct {
	GuId string
	Tag  *Tag
}

type Resource struct {
	// 注册生成的guid
	GuId string `json:"guId"`

	// 注册时提供的唯一信息
	// host: host_name + mac
	// container: image_name + rand byte
	Description string `json:"description"`

	// 状态
	Status int    `json:"status"`
	Area   string `json:"area"`

	// 等同depend_node
	PreNode string `json:"preNode"`
	SubNode string `json:"subNode"`

	// 类型
	OType int `json:"oType"`

	// 支持监控
	IsMonitor int `json:"isMonitor"`

	// 支持控制
	IsControl int `json:"isControl"`

	// 支持自动删除
	//AutoRemove int `json:"autoRemove"`

	Tags []Tag `json:"tags"`
	////支持统计
	//StatisticResources map[string]string `json:"resources"`
}

func (r *Resource) DeepCopy() *Resource {
	d := *r
	return &d
}
