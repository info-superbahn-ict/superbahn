package docker

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"strconv"

	v1 "github.com/opencontainers/image-spec/specs-go/v1"

	"os"
	"strings"
	"time"

	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/resources/manager_strategies/strategies"

	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/docker/docker/api/types/strslice"
	"github.com/docker/docker/pkg/stdcopy"
	"github.com/sirupsen/logrus"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
)

const (
	maxLogChanLen  = 1024
	defaultVersion = "v1.41"
)

type Controller interface {
	Contains(imageTag string) (bool, error)
	ListTags() ([]string, error)
	ListContainIds() ([]string, error)
	DeleteTag(imageTag string) error
	UploadTag(imageTag string) error
	LoadTag(imageTag string) error
	StartStrategy(imageTag string, containerName string, envs map[string]string) (*ContainerInformation, error)
	StartStrategyWithPort(imageTag string, containerName string, envs map[string]string, port string) (*ContainerInformation, error)
	StartContainer(imageTag string, containerName string, envs map[string]string) (*ContainerInformation, error)
	StartContainerWithPorts(tag, name string, envs map[string]string, ports map[string]string) (*ContainerInformation, error)
	StartContainerWithPortsForCar(tag, name string, envs map[string]string, ports map[string]string) (*ContainerInformation, error)
	StartContainerWithPortsForAgent(tag, name ,runtime ,workDir ,ImagePullPolicy string ,envs, ports,Lifecycle,LivenessProbe,ReadinessProbe,Reasources,SecurityContext,VolumeMounts map[string]string, cmds,TerminationMessagePath,TerminationMessagePolicy []string) (*ContainerInformation, error)
	Stop(containerId string) error
	ReStart(containerId string) error
	Resume(containerId string) error
	Remove(containerId string) error
	Log(containerId string) (string, error)
	LogChan(id string) (*LogWriter, func() error, error)
	Close() error
	GetContainerStats(containerId string) (*types.StatsJSON, error)
	ContainerExec(containerId string, cmd []string) error
}

func NewDockerController(ctx context.Context, url string) (Controller, error) {
	cli, err := client.NewClientWithOpts(client.WithHost(url), client.WithVersion(defaultVersion))
	if err != nil {
		return nil, fmt.Errorf("create client %v", err)
	}

	return &dockerController{
		ctx: ctx,
		cli: cli,
	}, err
}

type dockerController struct {
	ctx        context.Context
	cli        *client.Client
	strategies map[string]string
}

//type ContainerInfo interface {
//	GetCID() string
//	GetName() string
//	GetImage() string
//	GetGUID() string
//	GetEnvMap() map[string]string
//	GetEnvStr() []string
//
//	SetGUID(string)
//	SetName(string)
//	SetImage(string)
//	SetCID(string)
//	SetEnvMap(map[string]string)
//}

type ContainerInformation struct {
	Name  string
	Image string
	CID   string
	GUID  string
	ENV   map[string]string

	//新增 SERVE gitlab-runner字段
	ImagePullPolicy	string
	Lifecycle		map[string]string
	LivenessProbe	map[string]string
	ReadinessProbe	map[string]string
	Reasources		map[string]string
	SecurityContext	map[string]string
	VolumeMounts	map[string]string
	TerminationMessagePath		[]string
	TerminationMessagePolicy	[]string
}

type LogWriter struct {
	log chan string
}

func NewLoggerWriter() *LogWriter {
	return &LogWriter{
		log: make(chan string, 1024),
	}
}

func (r *LogWriter) Write(p []byte) (int, error) {
	//logrus.WithContext(context.Background()).WithFields(logrus.Fields{
	//	define.LogsPrintCommonLabelOfFunction: "log.chan.write",
	//}).Debugf("wirte log %s", p)
	if cap(r.log) >= 0 {
		r.log <- string(p)
		return len(p), nil
	}
	return 0, fmt.Errorf("chan is nil")
}

func (r *LogWriter) GetChan() <-chan string {

	return r.log
}

//func (i *ContainerInformation) GetName() string {
//	return i.BindingName
//}
//
//func (i *ContainerInformation) GetImage() string {
//	return i.Image
//}
//
//func (i *ContainerInformation) SetName(s string) {
//	i.BindingName = s
//}
//
//func (i *ContainerInformation) SetImage(s string) {
//	i.Image = s
//}
//
//func (i *ContainerInformation) GetEnvMap() map[string]string {
//	res := make(map[string]string)
//	for k, v := range i.ENV {
//		res[k] = v
//	}
//	return res
//}

func (i *ContainerInformation) GetEnvStr() []string {
	res := make([]string, len(i.ENV))
	index := 0
	for k, v := range i.ENV {
		res[index] = strings.Join([]string{k, v}, "=")
		index++
	}
	return res
}

//
//func (i *ContainerInformation) SetGUID(s string) {
//	i.GUID = s
//}
//
//func (i *ContainerInformation) SetCID(s string) {
//	i.CID = s
//}
//
//func (i *ContainerInformation) SetEnvMap(m map[string]string) {
//	i.ENV = m
//}
//
//func NewContainerInfo() ContainerInfo {
//	return &ContainerInformation{}
//}
//
//func (i *ContainerInformation) GetCID() string {
//	return i.CID
//}
//
//func (i *ContainerInformation) GetGUID() string {
//	return i.GUID
//}

func (d *dockerController) Contains(s string) (bool, error) {
	tags, err := d.ListTags()
	if err != nil {
		return false, fmt.Errorf("list tags %v", err)
	}

	for _, tag := range tags {
		pt := strings.Split(tag, ":")[0]
		if strings.EqualFold(tag, s) || strings.EqualFold(pt, s) {
			return true, nil
		}
	}

	return false, err
}

func (d *dockerController) ListTags() ([]string, error) {
	images, err := d.cli.ImageList(d.ctx, types.ImageListOptions{})
	if err != nil {
		return nil, fmt.Errorf("list images %v", err)
	}

	var tags []string

	for _, image := range images {
		for _, tag := range image.RepoTags {
			tags = append(tags, tag)
		}
	}

	return tags, nil
}

func (d *dockerController) ListContainIds() ([]string, error) {
	containers, err := d.cli.ContainerList(d.ctx, types.ContainerListOptions{})
	if err != nil {
		return nil, fmt.Errorf("list images %v", err)
	}

	var ids []string

	for _, ct := range containers {
		ids = append(ids, ct.ID)
	}

	return ids, nil
}


func (d *dockerController) DeleteTag(imageTag string) error {

	tags, err := d.ListTags()
	if err != nil {
		return fmt.Errorf("list tags %v", err)
	}
	var deleteIndex = 0
	for deleteIndex <= len(tags) {
		if tags[deleteIndex] == imageTag {
			break
		} else {
			deleteIndex = deleteIndex + 1
		}
		tags = append(tags[:deleteIndex], tags[(deleteIndex+1):]...)
	}
	//logrus.WithContext(d.ctx).WithFields(logrus.Fields{
	//	define.LogsPrintCommonLabelOfFunction: "Strategy_controllers.DeleteTag",
	//}).Errorf("delete image tags %v", err)
	//
	//return fmt.Errorf("delete image tags %v", err)
	return nil
}

func (d *dockerController) UploadTag(imageTag string) error {

	_, err := d.cli.ImageList(d.ctx, types.ImageListOptions{})
	if err != nil {
		return fmt.Errorf("upload image %v", err)
	}
	st := strategies.NewDefaultStrategy().StrategyImage
	if _, err = d.Contains(imageTag); err != nil {
		return fmt.Errorf("the imageTag does not exist%v", err)
	} else {
		d.strategies[imageTag] = st
	}

	return nil
}

func (d *dockerController) LoadTag(imageTag string) error {

	_, err := d.cli.ImageList(d.ctx, types.ImageListOptions{})
	if err != nil {
		return fmt.Errorf("load image %v", err)
	}
	st := strategies.DefaultImage
	if _, err = d.Contains(imageTag); err != nil {
		return fmt.Errorf("the imageTag does not exist%v", err)
	} else {
		d.strategies[imageTag] = st
	}

	return nil
}

func (d *dockerController) StartContainer(tag, name string, envs map[string]string) (*ContainerInformation, error) {
	var (
		//pc, ph = "80", "80"
		c = &ContainerInformation{
			Name:  name,
			Image: tag,
			ENV:   envs,
		}
	)

	//port, err := nat.NewPort("tcp", pc)
	//if err != nil {
	//	return nil, fmt.Errorf("new port set %v", err)
	//}

	config := &container.Config{
		Image: c.Image,
		//ExposedPorts: nat.PortSet{
		//	port: struct{}{},
		//},
		Cmd: strslice.StrSlice{
			"/bin/sh", "/root/boot.sh", c.Name,
		},
		Env: c.GetEnvStr(),
	}

	hostConfig := &container.HostConfig{
		//PortBindings: nat.PortMap{
		//	port: []nat.PortBinding{
		//		{
		//			HostPort: ph,
		//		},
		//	},
		//},
	}

	resp, err := d.cli.ContainerCreate(d.ctx, config, hostConfig, nil, &v1.Platform{}, c.Name)
	if err != nil {
		return nil, fmt.Errorf("new container create %v", err)
	}

	if err = d.cli.ContainerStart(d.ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		return nil, fmt.Errorf("new container start %v", err)
	}

	c.CID = resp.ID

	return c, nil
}

// ports: map[hoptPort] containerPort
func (d *dockerController) StartContainerWithPorts(tag, name string, envs map[string]string, ports map[string]string) (*ContainerInformation, error) {
	var (
		c = &ContainerInformation{
			Name:  name,
			Image: tag,
			ENV:   envs,
		}
	)

	st := nat.PortSet{}
	mp := nat.PortMap{}
	for ph, pc := range ports {
		pcSplit := strings.Split(pc, "/")
		pcType := "tcp"
		PcPort := pcSplit[0]
		if len(pcSplit) > 1 {
			pcType = pcSplit[1]
		}
		portPc, err := nat.NewPort(pcType, PcPort)
		if err != nil {
			return nil, fmt.Errorf("new port set %v", err)
		}
		st[portPc] = struct{}{}
		mp[portPc] = []nat.PortBinding{
			{
				HostPort: ph,
			},
		}
	}

	logrus.WithContext(d.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
	}).Debugf("portMap %v  portSet %v", mp, st)

	config := &container.Config{
		Image:        c.Image,
		ExposedPorts: st,
		Cmd: strslice.StrSlice{
			"/bin/sh", "/root/boot.sh", c.Name,
		},
		Env: c.GetEnvStr(),
	}

	hostConfig := &container.HostConfig{
		PortBindings: mp,
	}

	resp, err := d.cli.ContainerCreate(d.ctx, config, hostConfig, nil, nil, c.Name)
	if err != nil {
		return nil, fmt.Errorf("new container create %v", err)
	}

	if err = d.cli.ContainerStart(d.ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		return nil, fmt.Errorf("new container start %v", err)
	}

	c.CID = resp.ID

	return c, nil
}

func (d *dockerController) StartContainerWithPortsForCar(tag, name string, envs map[string]string, ports map[string]string) (*ContainerInformation, error) {
	var (
		c = &ContainerInformation{
			Name:  name,
			Image: tag,
			ENV:   envs,
		}
	)

	st := nat.PortSet{}
	mp := nat.PortMap{}
	for ph, pc := range ports {
		pcSplit := strings.Split(pc, "/")
		pcType := "tcp"
		PcPort := pcSplit[0]
		if len(pcSplit) > 1 {
			pcType = pcSplit[1]
		}
		portPc, err := nat.NewPort(pcType, PcPort)
		if err != nil {
			return nil, fmt.Errorf("new port set %v", err)
		}
		st[portPc] = struct{}{}
		mp[portPc] = []nat.PortBinding{
			{
				HostPort: ph,
			},
		}
	}

	logrus.WithContext(d.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
	}).Debugf("portMap %v  portSet %v", mp, st)

	config := &container.Config{
		Image:        c.Image,
		ExposedPorts: st,
		Cmd: strslice.StrSlice{
			"/bin/sh", "/root/boot.sh", c.Name,
		},
		Env: c.GetEnvStr(),
	}

	hostConfig := &container.HostConfig{
		PortBindings: mp,
		Privileged:   true,
	}

	resp, err := d.cli.ContainerCreate(d.ctx, config, hostConfig, nil, nil, c.Name)
	if err != nil {
		return nil, fmt.Errorf("new container create %v", err)
	}

	if err = d.cli.ContainerStart(d.ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		return nil, fmt.Errorf("new container start %v", err)
	}

	c.CID = resp.ID

	return c, nil
}

func (d *dockerController) StartContainerWithPortsForAgent(tag, name ,runtime ,workDir ,imagePullPolicy string ,envs, ports,lifecycle,livenessProbe,readinessProbe,reasources,securityContext,volumeMounts map[string]string, cmds,terminationMessagePath,terminationMessagePolicy []string) (*ContainerInformation, error) {
	var (
		c = &ContainerInformation{
			Name:  name,
			Image: tag,
			ENV:   envs,

			//新增 SERVE gitlab-runner字段
			ImagePullPolicy:	imagePullPolicy,
			Lifecycle:			lifecycle,
			LivenessProbe:		livenessProbe,
			ReadinessProbe:		readinessProbe,
			Reasources:			reasources,
			SecurityContext:	securityContext,
			VolumeMounts:		volumeMounts,
			TerminationMessagePath:		terminationMessagePath,
			TerminationMessagePolicy:	terminationMessagePolicy,
		}
	)

	st := nat.PortSet{}
	mp := nat.PortMap{}
	for ph, pc := range ports {
		if strings.Contains(ph,"-") {
			pcsSplit := strings.Split(pc, "/")
			phsSplit := strings.Split(ph, "/")


			pcsType := "tcp"
			pcPorts := pcsSplit[0]
			if len(pcsSplit) > 1 {
				pcsType = pcsSplit[1]
			}

			pcs := strings.Split(pcPorts,"-")
			phs := strings.Split(phsSplit[0],"-")

			pcStart,err := strconv.Atoi(pcs[0])
			if err != nil {
				return nil, fmt.Errorf("split pcs %v to int %v", pcs,err)
			}
			pcEnd,err := strconv.Atoi(pcs[1])
			if err != nil {
				return nil, fmt.Errorf("split pcs %v to int %v", pcs,err)
			}

			phStart,err := strconv.Atoi(phs[0])
			if err != nil {
				return nil, fmt.Errorf("split pcs %v to int %v", pcs,err)
			}
			phEnd,err := strconv.Atoi(phs[1])
			if err != nil {
				return nil, fmt.Errorf("split pcs %v to int %v", pcs,err)
			}
			logrus.WithContext(d.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
			}).Debugf("port map from container: %v-%v to host: %v-%v",pcStart,pcEnd,phStart,phEnd)
			for i,j:=pcStart,phStart; i<= pcEnd && j < phEnd; i,j = i+1,j+1 {
				pci := strconv.Itoa(i)
				phi := strconv.Itoa(j)

				portPc, err := nat.NewPort(pcsType, pci)
				if err != nil {
					return nil, fmt.Errorf("new port set %v", err)
				}
				st[portPc] = struct{}{}
				mp[portPc] = []nat.PortBinding{
					{
						HostPort: phi,
					},
				}
			}

		}else {
			pcSplit := strings.Split(pc, "/")
			pcType := "tcp"
			PcPort := pcSplit[0]
			if len(pcSplit) > 1 {
				pcType = pcSplit[1]
			}
			portPc, err := nat.NewPort(pcType, PcPort)
			if err != nil {
				return nil, fmt.Errorf("new port set %v", err)
			}
			st[portPc] = struct{}{}
			mp[portPc] = []nat.PortBinding{
				{
					HostPort: ph,
				},
			}
		}
	}

	logrus.WithContext(d.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
	}).Debugf("portMap %v  portSet %v  runtime %v dir %v", mp, st,runtime,workDir)

	config := &container.Config{
		Image:        c.Image,
		ExposedPorts: st,
		Cmd:          cmds,
		Env:          c.GetEnvStr(),
		WorkingDir: workDir,
	}

	hostConfig := &container.HostConfig{
		PortBindings: mp,
		Privileged:   true,
		Runtime: runtime,
	}



	resp, err := d.cli.ContainerCreate(d.ctx, config, hostConfig, nil, nil, c.Name)
	if err != nil {
		return nil, fmt.Errorf("new container create %v", err)
	}

	if err = d.cli.ContainerStart(d.ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		return nil, fmt.Errorf("new container start %v", err)
	}

	c.CID = resp.ID

	return c, nil
}

func (d *dockerController) StartStrategyWithPort(tag, name string, envs map[string]string, Hport string) (*ContainerInformation, error) {
	var (
		pc, ph = "8080", Hport
	)

	var (
		c = &ContainerInformation{
			Name:  name,
			Image: tag,
			ENV:   envs,
		}
	)

	port, err := nat.NewPort("tcp", pc)
	if err != nil {
		return nil, fmt.Errorf("new port set %v", err)
	}

	config := &container.Config{
		Image: c.Image,
		ExposedPorts: nat.PortSet{
			port: struct{}{},
		},
		Cmd: strslice.StrSlice{
			"/bin/bash", "/root/boot.sh", c.Name,
		},
	}

	hostConfig := &container.HostConfig{
		PortBindings: nat.PortMap{
			port: []nat.PortBinding{
				{
					HostPort: ph,
				},
			},
		},
		Privileged: true,
	}

	resp, err := d.cli.ContainerCreate(d.ctx, config, hostConfig, nil, nil, c.Name)
	if err != nil {
		return nil, fmt.Errorf("new container create %v", err)
	}

	if err = d.cli.ContainerStart(d.ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		return nil, fmt.Errorf("new container start %v", err)
	}

	out, err := d.cli.ContainerLogs(d.ctx, resp.ID, types.ContainerLogsOptions{
		ShowStdout: true,
		Follow:     true,
	})
	if err != nil {
		return nil, fmt.Errorf("new container logs %v", err)

	}

	buf := new(bytes.Buffer)
	go func() {
		_, err := stdcopy.StdCopy(buf, os.Stderr, out)
		if err != nil {
			logrus.WithContext(d.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
			}).Debugf("std copy %v", err)
		}
	}()

	timeOut := time.Tick(time.Second * 10)
	for {
		aLog := buf.String()
		if strings.Contains(aLog, "CONTAINER") {
			if err = out.Close(); err != nil {
				return nil, fmt.Errorf("close log %v", err)
			}

			c.CID = resp.ID
			c.GUID = strings.Split(aLog, "|")[3]
			return c, nil
		}

		tk := time.Tick(time.Millisecond * 500)
		select {
		case <-tk:
			logrus.WithContext(d.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
			}).Debugf("heart")
		case <-timeOut:
			if err = out.Close(); err != nil {
				return nil, fmt.Errorf("time out and close log %v", err)
			}
			return nil, fmt.Errorf("time out")
		case <-d.ctx.Done():
			if err = out.Close(); err != nil {
				return nil, fmt.Errorf("close log %v", err)
			}
			return nil, nil
		}
	}
	c.CID = resp.ID

	return c, nil
}

func (d *dockerController) StartStrategy(tag, name string, envs map[string]string) (*ContainerInformation, error) {
	var (
		//pc, ph = "80", "80"
		c = &ContainerInformation{
			Name:  name,
			Image: tag,
			ENV:   envs,
		}
	)

	//port, err := nat.NewPort("tcp", pc)
	//if err != nil {
	//	return nil, fmt.Errorf("new port set %v", err)
	//}

	config := &container.Config{
		Image: c.Image,
		//ExposedPorts: nat.PortSet{
		//	port: struct{}{},
		//},
		Cmd: strslice.StrSlice{
			"/bin/bash", "/root/boot.sh", c.Name,
		},
		Env: c.GetEnvStr(),
	}

	hostConfig := &container.HostConfig{
		//PortBindings: nat.PortMap{
		//	port: []nat.PortBinding{
		//		{
		//			HostPort: ph,
		//		},
		//	},
		//},
	}

	resp, err := d.cli.ContainerCreate(d.ctx, config, hostConfig, nil, nil, c.Name)
	if err != nil {
		return nil, fmt.Errorf("new container create %v", err)
	}

	if err = d.cli.ContainerStart(d.ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		return nil, fmt.Errorf("new container start %v", err)
	}

	out, err := d.cli.ContainerLogs(d.ctx, resp.ID, types.ContainerLogsOptions{
		ShowStdout: true,
		Follow:     true,
	})
	if err != nil {
		return nil, fmt.Errorf("new container logs %v", err)

	}

	buf := new(bytes.Buffer)
	go func() {
		_, err := stdcopy.StdCopy(buf, os.Stderr, out)
		if err != nil {
			logrus.WithContext(d.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
			}).Debugf("std copy %v", err)
		}
	}()

	timeOut := time.Tick(time.Second * 10)
	for {
		aLog := buf.String()
		if strings.Contains(aLog, "CONTAINER") {
			if err = out.Close(); err != nil {
				return nil, fmt.Errorf("close log %v", err)
			}

			c.CID = resp.ID
			c.GUID = strings.Split(aLog, "|")[3]
			return c, nil
		}

		tk := time.Tick(time.Millisecond * 500)
		select {
		case <-tk:
			logrus.WithContext(d.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
			}).Debugf("heart")
		case <-timeOut:
			if err = out.Close(); err != nil {
				return nil, fmt.Errorf("time out and close log %v", err)
			}
			return nil, fmt.Errorf("time out")
		case <-d.ctx.Done():
			if err = out.Close(); err != nil {
				return nil, fmt.Errorf("close log %v", err)
			}
			return nil, nil
		}
	}
}

func (d *dockerController) Stop(containerID string) error {
	timeout := time.Second * 10
	err := d.cli.ContainerStop(d.ctx, containerID, &timeout)
	if err != nil {
		return fmt.Errorf("stop contain %v", err)
	}

	logrus.WithContext(d.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.stop",
	}).Debugf("stop contain succeed")
	return nil
}

func (d *dockerController) Resume(containerID string) error {
	err := d.cli.ContainerStart(d.ctx, containerID, types.ContainerStartOptions{})
	if err != nil {
		return fmt.Errorf("stop contain %v", err)
	}

	logrus.WithContext(d.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.stop",
	}).Debugf("stop contain succeed")
	return nil
}

func (d *dockerController) ReStart(containerID string) error {
	timeout := time.Second * 10
	err := d.cli.ContainerRestart(d.ctx, containerID, &timeout)
	if err != nil {
		return fmt.Errorf("restart contain %v", err)
	}

	logrus.WithContext(d.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.restatrt",
	}).Debugf("restart contain succeed")
	return nil
}

func (d *dockerController) Remove(id string) error {
	err := d.cli.ContainerRemove(d.ctx, id, types.ContainerRemoveOptions{})
	if err != nil {
		return fmt.Errorf("remove contain %v", err)
	}

	logrus.WithContext(d.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.remove",
	}).Debugf("remove contain succeed")
	return nil
}

func (d *dockerController) Log(id string) (string, error) {
	out, err := d.cli.ContainerLogs(d.ctx, id, types.ContainerLogsOptions{
		ShowStdout: true,
	})

	if err != nil {
		logrus.WithContext(d.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
		}).Debugf("log  %v", err)
		return "", fmt.Errorf("new container logs %v", err)

	}
	buf := new(bytes.Buffer)
	_, err = stdcopy.StdCopy(buf, buf, out)
	if err != nil {
		logrus.WithContext(d.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
		}).Debugf("std copy %v", err)
		return "", fmt.Errorf("std copy %v", err)
	}
	logrus.WithContext(d.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
	}).Debugf("logs %v", buf.String())
	_ = out.Close()
	return buf.String(), nil
}

func (d *dockerController) LogChan(id string) (*LogWriter, func() error, error) {
	out, err := d.cli.ContainerLogs(d.ctx, id, types.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: true,
		Details:    true,
		Follow:     true,
	})

	if err != nil {
		logrus.WithContext(d.ctx).WithFields(logrus.Fields{
			define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
		}).Debugf("log  %v", err)
		return nil, nil, fmt.Errorf("new container logs %v", err)
	}

	logInfo := NewLoggerWriter()

	go func() {
		_, err := stdcopy.StdCopy(logInfo, logInfo, out)
		if err != nil {
			logrus.WithContext(d.ctx).WithFields(logrus.Fields{
				define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
			}).Debugf("std copy %v", err)
		}
	}()

	return logInfo, out.Close, nil

	//_, err = stdcopy.StdCopy(buf, buf, out)
	//if err != nil {
	//	logrus.WithContext(d.ctx).WithFields(logrus.Fields{
	//		define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
	//	}).Debugf("std copy %v", err)
	//	return "", fmt.Errorf("std copy %v", err)
	//}
	//logrus.WithContext(d.ctx).WithFields(logrus.Fields{
	//	define.LogsPrintCommonLabelOfFunction: "third_party.collector_docker.start",
	//}).Debugf("logs %v", buf.String())
	//
	//_ = out.Close()
	//return buf.String(), nil
}

func (d *dockerController) Close() error {
	return d.cli.Close()
}

func (d *dockerController) GetContainerStats(containerId string) (*types.StatsJSON, error) {
	containerStats, err := d.cli.ContainerStats(d.ctx, containerId, false)
	if err != nil {
		return nil, err
	}
	defer containerStats.Body.Close()

	var v *types.StatsJSON
	dec := json.NewDecoder(containerStats.Body)
	if err := dec.Decode(&v); err != nil {
		return nil, err
	}
	return v, nil
}

func (d *dockerController) ContainerExec(containerId string, cmd []string) error {
	execConfig := types.ExecConfig{
		User:         "root",
		Privileged:   true,
		Tty:          false,
		AttachStdin:  false,
		AttachStderr: true,
		AttachStdout: true,
		DetachKeys:   "ctrl-p,ctrl-q",
		Env:          nil,
		WorkingDir:   "/",
		Cmd:          cmd,
	}

	response, err := d.cli.ContainerExecCreate(d.ctx, containerId, execConfig)

	if err != nil {
		return err
	}

	execStartCheck := types.ExecStartCheck{
		Detach: false,
		Tty:    false,
	}

	err = d.cli.ContainerExecStart(d.ctx, response.ID, execStartCheck)

	if err != nil {
		return err
	}

	return nil
}

// 列出镜像
func listImageTags(cli *client.Client) {
	images, err := cli.ImageList(context.Background(), types.ImageListOptions{})
	log(err)

	for _, image := range images {
		fmt.Println(image)
	}
}

// 创建容器
func createContainer(cli *client.Client) string {
	exports := make(nat.PortSet, 10)
	port, err := nat.NewPort("tcp", "80")
	log(err)
	exports[port] = struct{}{}
	config := &container.Config{Image: "nginx", ExposedPorts: exports}

	portBind := nat.PortBinding{HostPort: "80"}
	portMap := make(nat.PortMap, 0)
	tmp := make([]nat.PortBinding, 0, 1)
	tmp = append(tmp, portBind)
	portMap[port] = tmp
	hostConfig := &container.HostConfig{PortBindings: portMap}

	containerName := "hel"
	body, err := cli.ContainerCreate(context.Background(), config, hostConfig, nil, nil, containerName)
	log(err)
	fmt.Printf("ID: %s\n", body.ID)
	return body.ID
}

// 启动
func startContainer(containerID string, cli *client.Client) {
	err := cli.ContainerStart(context.Background(), containerID, types.ContainerStartOptions{})
	log(err)
	if err == nil {
		fmt.Println("容器", containerID, "启动成功")
	}
}

// 停止
func stopContainer(containerID string, cli *client.Client) {
	timeout := time.Second * 10
	err := cli.ContainerStop(context.Background(), containerID, &timeout)
	if err != nil {
		log(err)
	} else {
		fmt.Printf("容器%s已经被停止\n", containerID)
	}
}

// 删除
func removeContainer(containerID string, cli *client.Client) (string, error) {
	err := cli.ContainerRemove(context.Background(), containerID, types.ContainerRemoveOptions{})
	log(err)
	return containerID, err
}

func log(err error) {
	if err != nil {
		fmt.Printf("%v\n", err)
		panic(err)
	}
}
