package docker

import (
	"context"
	"crypto/rand"
	"fmt"
	"testing"
)

func TestStartToRemove(t *testing.T) {
	ctx := context.Background()

	c, err := NewDockerController(ctx, "")
	if err != nil {
		fmt.Println(err)
	}

	m := map[string]string{
		"Name":  "name",
		"Image": "tag",
	}
	id, err := c.StartStrategy("simple-test", "xxkkll", m)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("start cid: %v, guid: %v\n", id.CID, id.GUID)

	err = c.Stop(id.GUID)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("stop cid: %v, guid: %v\n", id.CID, id.GUID)

	err = c.Remove(id.CID)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("remove cid: %v, guid: %v\n", id.CID, id.GUID)
}

func TestListImages(t *testing.T) {
	ctx := context.Background()

	c, err := NewDockerController(ctx, "tcp://10.16.0.180:23750")
	if err != nil {
		fmt.Println(err)
	}

	tags, err := c.ListTags()
	if err != nil {
		fmt.Println(err)
	}

	for _, tag := range tags {
		fmt.Printf("image name: %v\n", tag)
	}
}

func TestContains(t *testing.T) {
	ctx := context.Background()

	c, err := NewDockerController(ctx, "")
	if err != nil {
		fmt.Println(err)
	}

	tag := "centos-go:l"
	ctn, err := c.Contains(tag)
	if err != nil {
		fmt.Println(err)
	}

	if ctn {
		fmt.Printf("contian image: %v\n", tag)
	} else {
		fmt.Printf("not contian image: %v\n", tag)
	}
}

func TestRandName(t *testing.T) {
	randBytes := make([]byte, 8)
	_, err := rand.Read(randBytes)
	if err != nil {
		fmt.Printf("generate name %v", err)
	}
	fmt.Printf("%x", randBytes)
}

func TestContainerExec(t *testing.T) {
	dockerUrl := "tcp://172.16.31.37:2375"
	testContainerId := "2af82fcffdead606cfbc53b4411a3c47023f6452193968f33db6a2150d2c4e2e"

	tcCmd := []string{"tc", "qdisc", "change", "dev", "eth0", "root", "tbf", "rate", "10mbit", "latency", "10ms", "burst", "10000"}

	controller, err := NewDockerController(context.Background(), dockerUrl)

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	controller.ContainerExec(testContainerId, tcCmd)

}
