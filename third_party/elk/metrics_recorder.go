package elk

import (
	"context"
	"fmt"
	elk "github.com/olivere/elastic/v7"
	"strings"
)

type Recorder interface {
	Certificate() error
	Put(string, string) error
	PutJson(string, interface{}) error
	PutJsonService(key string,id string, data interface{}) error
	Get(string,float64,float64) ([]string, error)
	Close() error
}

type recordMetricsClient struct {
	client *elk.Client
	ctx    context.Context
}

func NewMetricsRecorder(ctx context.Context, url string) (Recorder, error) {
	cli, err := elk.NewClient(elk.SetSniff(false), elk.SetURL(url))
	if err != nil {
		return nil, fmt.Errorf("new client %v", err)
	}

	return &recordMetricsClient{
		ctx:    ctx,
		client: cli,
	}, nil
}

func (c *recordMetricsClient) Put(key string, data string) error {
	if _, err := c.client.Index().Index(strings.ToLower(key)).BodyJson(data).Do(c.ctx);err != nil {
		return err
	}
	return nil
}

func (c *recordMetricsClient) PutJson(key string, data interface{}) error {
	if _, err := c.client.Index().Index(strings.ToLower(key)).Type("span").BodyJson(&data).Do(c.ctx);err != nil {
		return err
	}
	return nil
}

func (c *recordMetricsClient) PutJsonService(key string,id string, data interface{}) error {

	if _, err := c.client.Index().Index(strings.ToLower(key)).Type("service").Id(id).BodyJson(&data).Do(c.ctx);err != nil {
		return err
	}
	return nil
}

func (c *recordMetricsClient) Get(guid string, begin, end float64) ([]string, error) {
	var err error

	fa := elk.NewRangeQuery("TimeStamp").Gt(begin).Lt(end)
	srv := c.client.Search().Index(guid).Sort("TimeStamp", true).Query(fa)

	srs, err := srv.Do(c.ctx)
	if err != nil {
		return nil, fmt.Errorf("do search %v", err)
	}

	mts := make([]string, len(srs.Hits.Hits))

	for _, hit := range srs.Hits.Hits {
		//dt := agent_metrics.ContainerMetrics{}
		//err = json.Unmarshal(hit.Source,dt)
		//if err != nil {
		//	return nil, fmt.Errorf("decode data %v", err)
		//}
		mts = append(mts, string(hit.Source))
	}

	return mts, nil
}

func (c *recordMetricsClient) Certificate() error {
	return nil
}

func (c *recordMetricsClient) Close() error {
	c.client.Stop()
	return nil
}
