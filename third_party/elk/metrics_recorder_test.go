package elk

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/options"
	"testing"
)

func TestElkPut(t *testing.T) {
	ctx := context.Background()

	cli, err := NewMetricsRecorder(ctx, options.DefaultedElkUrl)
	if err != nil {
		t.Error(err)
		return
	}

	str1 := `{"data":[{"content":{"cpu_core_allocated":3,"cpu_utilization":5},"type":"cpu"},{"content":{"memory_allocated":7823,"memory_usage":54}`
	str1 += `,"type":"memory"}],"guid":"11","timestamp":1634112645,"time":"2021-10-13T16:10:45.6184143+08:00"}`

	fmt.Printf("ok")
	//dt := containers.ContainerMetrics{}
	//err = json.Unmarshal([]byte(str1),dt)
	//if err != nil {
	//	fmt.Println(err)
	//}

	err = cli.Put("tangshibo", str1)
	if err != nil {
		fmt.Println(err)
	}
}
