package etcd

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/sync/define"
	"github.com/coreos/etcd/clientv3"
	"github.com/sirupsen/logrus"
	"time"
)

type Recorder interface {
	Certificate() error
	Put(string,string) error
	//Put(string,[]byte) error
	PutWithLease(string,string,clientv3.OpOption) error
	Get(string) ([]byte, error)
	GetWithPrefix(string) (map[string][]byte, error)
	Del(string) error
	DelWithPrefix(string) error
	NewLease() clientv3.Lease
	GetWatcher(string) clientv3.WatchChan
	Close() error
}

type recordMetasClient struct {
	client *clientv3.Client
	ctx    context.Context
}

func NewMetasRecorder(ctx context.Context, points []string, timeOut time.Duration) (Recorder, error) {

	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   points,
		DialTimeout: timeOut,
	})

	if err != nil {
		return nil, fmt.Errorf("connect to etcd %v %v", points[0], err)
	}

	return &recordMetasClient{
		ctx:    ctx,
		client: cli,
	}, nil
}

func (c *recordMetasClient) Put(key, value string) error {
	ctx, cancel := context.WithTimeout(c.ctx, time.Second)
	_, err := c.client.Put(ctx, key, value)
	cancel()
	if err != nil {
		return fmt.Errorf("put to etcd failed, err:%v\n", err)
	}
	return nil
}

func (c *recordMetasClient) PutWithLease(key, value string, option clientv3.OpOption) error {
	ctx, cancel := context.WithTimeout(c.ctx, time.Second)
	_, err := c.client.Put(ctx, key, value, option)
	cancel()
	if err != nil {
		return fmt.Errorf("put to etcd with lease failed, err:%v\n", err)
	}
	return nil
}

func (c *recordMetasClient) Get(key string) ([]byte, error) {
	logrus.WithContext(c.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "etcd.get",
	}).Debugf("KEY: %v",key)
	ctx, cancel := context.WithTimeout(c.ctx, time.Second)
	resp, err := c.client.Get(ctx, key)
	cancel()
	if err != nil {
		return nil, fmt.Errorf("get to etcd failed, err:%v\n", err)
	}

	if len(resp.Kvs) > 0 {
		return resp.Kvs[0].Value, nil
	} else {
		return nil, fmt.Errorf("value is nil")
	}
}

func (c *recordMetasClient) GetWithPrefix(key string) (map[string][]byte, error) {
	logrus.WithContext(c.ctx).WithFields(logrus.Fields{
		define.LogsPrintCommonLabelOfFunction: "etcd.getPrefix",
	}).Debugf("PreKEY: %v",key)

	ctx, cancel := context.WithTimeout(c.ctx, time.Second)
	resp, err := c.client.Get(ctx, key, clientv3.WithPrefix())
	cancel()
	if err != nil {
		return nil, fmt.Errorf("get to etcd with prefix failed, err:%v\n", err)
	}

	if len(resp.Kvs) > 0 {
		kv := make(map[string][]byte)
		for _, v := range resp.Kvs {
			kv[string(v.Key)] = v.Value
		}
		return kv, nil
	} else {
		return nil, nil
	}
}

func (c *recordMetasClient) Del(key string) error {
	ctx, cancel := context.WithTimeout(c.ctx, time.Second)
	_, err := c.client.Delete(ctx, key)
	cancel()
	if err != nil {
		return fmt.Errorf("delete from etcd failed, err:%v\n", err)
	}
	return nil
}

func (c *recordMetasClient) DelWithPrefix(key string) error {
	ctx, cancel := context.WithTimeout(c.ctx, time.Second)
	_, err := c.client.Delete(ctx, key,clientv3.WithPrefix())
	cancel()
	if err != nil {
		return fmt.Errorf("delete from etcd with prefix failed, err:%v\n", err)
	}
	return nil
}

func (c *recordMetasClient) NewLease() clientv3.Lease {
	return clientv3.NewLease(c.client)
}

func (c *recordMetasClient) Certificate()error{
	return nil
}

func (c *recordMetasClient) Close() error {
	return c.client.Close()
}

func (c *recordMetasClient) GetWatcher(prefix string) clientv3.WatchChan {
	return c.client.Watch(c.ctx, prefix, clientv3.WithPrefix())
}