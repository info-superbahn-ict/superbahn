package jaeger_model

import (
	"github.com/gogo/protobuf/proto"
	"time"
)

type Flags uint32
type SpanID uint64
type SpanRefType int32
type ValueType int32
type KeyValues []KeyValue

type TraceID struct {
	Low  uint64 `json:"lo"`
	High uint64 `json:"hi"`
}

type SpanRef struct {
	TraceID              TraceID     `protobuf:"bytes,1,opt,name=trace_id,json=traceId,proto3,customtype=TraceID" json:"trace_id"`
	SpanID               SpanID      `protobuf:"bytes,2,opt,name=span_id,json=spanId,proto3,customtype=SpanID" json:"span_id"`
	RefType              SpanRefType `protobuf:"varint,3,opt,name=ref_type,json=refType,proto3,enum=jaeger.api_v2.SpanRefType" json:"ref_type,omitempty"`
	XXX_NoUnkeyedLiteral struct{}    `json:"-"`
	XXX_unrecognized     []byte      `json:"-"`
	XXX_sizecache        int32       `json:"-"`
}

type KeyValue struct {
	Key                  string    `protobuf:"bytes,1,opt,name=key,proto3" json:"key,omitempty"`
	VType                ValueType `protobuf:"varint,2,opt,name=v_type,json=vType,proto3,enum=jaeger.api_v2.ValueType" json:"v_type,omitempty"`
	VStr                 string    `protobuf:"bytes,3,opt,name=v_str,json=vStr,proto3" json:"v_str,omitempty"`
	VBool                bool      `protobuf:"varint,4,opt,name=v_bool,json=vBool,proto3" json:"v_bool,omitempty"`
	VInt64               int64     `protobuf:"varint,5,opt,name=v_int64,json=vInt64,proto3" json:"v_int64,omitempty"`
	VFloat64             float64   `protobuf:"fixed64,6,opt,name=v_float64,json=vFloat64,proto3" json:"v_float64,omitempty"`
	VBinary              []byte    `protobuf:"bytes,7,opt,name=v_binary,json=vBinary,proto3" json:"v_binary,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

type Log struct {
	Timestamp            time.Time  `protobuf:"bytes,1,opt,name=timestamp,proto3,stdtime" json:"timestamp"`
	Fields               []KeyValue `protobuf:"bytes,2,rep,name=fields,proto3" json:"fields"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

type Process struct {
	ServiceName          string     `protobuf:"bytes,1,opt,name=service_name,json=serviceName,proto3" json:"service_name,omitempty"`
	Tags                 []KeyValue `protobuf:"bytes,2,rep,name=tags,proto3" json:"tags"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

type Span struct {
	TraceID              TraceID       `protobuf:"bytes,1,opt,name=trace_id,json=traceId,proto3,customtype=TraceID" json:"trace_id"`
	SpanID               SpanID        `protobuf:"bytes,2,opt,name=span_id,json=spanId,proto3,customtype=SpanID" json:"span_id"`
	OperationName        string        `protobuf:"bytes,3,opt,name=operation_name,json=operationName,proto3" json:"operation_name,omitempty"`
	References           []SpanRef     `protobuf:"bytes,4,rep,name=references,proto3" json:"references"`
	Flags                Flags         `protobuf:"varint,5,opt,name=flags,proto3,customtype=Flags" json:"flags"`
	StartTime            time.Time     `protobuf:"bytes,6,opt,name=start_time,json=startTime,proto3,stdtime" json:"start_time"`
	Duration             time.Duration `protobuf:"bytes,7,opt,name=duration,proto3,stdduration" json:"duration"`
	Tags                 []KeyValue    `protobuf:"bytes,8,rep,name=tags,proto3" json:"tags"`
	Logs                 []Log         `protobuf:"bytes,9,rep,name=logs,proto3" json:"logs"`
	Process              *Process      `protobuf:"bytes,10,opt,name=process,proto3" json:"process,omitempty"`
	ProcessID            string        `protobuf:"bytes,11,opt,name=process_id,json=processId,proto3" json:"process_id,omitempty"`
	Warnings             []string      `protobuf:"bytes,12,rep,name=warnings,proto3" json:"warnings,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *Span) Reset()         { *m = Span{} }
func (m *Span) String() string { return proto.CompactTextString(m) }
func (*Span) ProtoMessage()    {}

func (kvs KeyValues) FindByKey(key string) (KeyValue, bool) {
	for _, kv := range kvs {
		if kv.Key == key {
			return kv, true
		}
	}
	return KeyValue{}, false
}