package supbapis

type Assume interface {
	Done() uint64
	Cancel()
}

