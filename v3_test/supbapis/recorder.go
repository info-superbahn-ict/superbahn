package supbapis

import "github.com/coreos/etcd/clientv3"

type Recorder interface {
	WriteAssume(key string, value []byte) (Assume,error)

	//Write(key string, value []byte) error
	Read(key string) ([]byte,error)

	DeleteAssume(key string) (Assume,error)

	WatchKey(key string) clientv3.WatchChan
	WatchPrefix(prefix string) clientv3.WatchChan
}

