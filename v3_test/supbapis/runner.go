package supbapis

import "gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/compose"

type Runner interface {
	Run(compose *compose.Containers) (*compose.RunningBaseInfo, error)
	Remove(compose *compose.Containers) error

	Stop(compose *compose.Containers) error
	Restart(compose *compose.Containers) error
}
