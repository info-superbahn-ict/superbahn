package docker_cli

import (
	"context"
	"crypto/rand"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/pkg/supbmanager/v3_test/supb-modules/supbres/compose"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"strings"
)

type Client struct {
	ctx    context.Context
	prefix string

	logger log.Logger
}

func NewDockerClient(ctx context.Context, prefix string) *Client {
	return &Client{
		ctx:    ctx,
		prefix: prefix,
		logger: log.NewLogger(prefix),
	}
}

func (r *Client) InitFlags(flags *pflag.FlagSet) {

}

func (r *Client) ViperConfig(viper *viper.Viper) {

}

func (r *Client) InitViper(viper *viper.Viper) {

}

func (r *Client) OptionConfig(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}
}

func (r *Client) Initialize(opts ...option) {
	for _, apply := range opts {
		apply(r)
	}

	r.logger.Infof("%v initialized", r.prefix)
}

func (r *Client) Close() error {
	r.logger.Infof("%v closed", r.prefix)
	return nil
}

func (r *Client) Run(cmp *compose.Containers) (*compose.RunningBaseInfo, error) {
	randBytes := make([]byte, 8)
	if _, err := rand.Read(randBytes); err != nil {
		return nil, fmt.Errorf("generate name %v", err)
	}
	name := strings.Join([]string{"CONTAIN", fmt.Sprintf("%x", randBytes)}, "_")

	randBytes = make([]byte, 16)
	if _, err := rand.Read(randBytes); err != nil {
		return nil, fmt.Errorf("generate name %v", err)
	}
	CID := fmt.Sprintf("%x", randBytes)

	randBytes = make([]byte, 16)
	if _, err := rand.Read(randBytes); err != nil {
		return nil, fmt.Errorf("generate name %v", err)
	}
	Guid := fmt.Sprintf("%x", randBytes)
	return &compose.RunningBaseInfo{
		ContainerName: name,
		CID:           CID,
		Guid:          Guid,
	}, nil
}

func (r *Client) Remove(compose *compose.Containers) error {
	return nil
}

func (r *Client) Stop(compose *compose.Containers) error {
	return nil
}

func (r *Client) Restart(compose *compose.Containers) error {
	return nil
}
