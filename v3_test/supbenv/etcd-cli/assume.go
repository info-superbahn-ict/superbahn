package etcd_cli

import (
	"context"
	"github.com/coreos/etcd/clientv3"
)

type writeAssume struct {
	cli *clientv3.Client
	key string
	value string
	done bool
}

func (r *writeAssume) Done() uint64 {
	if r.done {
		return 0
	}

	_ ,_ = r.cli.Put(context.Background(),r.key,r.value)

	r.done = true
	return 1
}

func (r *writeAssume) Cancel() {

	if r.done {
		return
	}
	r.done = true
}


type deleteAssume struct {
	cli *clientv3.Client
	key string
	done bool
}

func (r *deleteAssume) Done() uint64 {
	if r.done {
		return 0
	}

	_ ,_ = r.cli.Delete(context.Background(),r.key)

	r.done = true

	return 1
}

func (r *deleteAssume) Cancel() {

	if r.done {
		return
	}
	r.done = true
}

type testAssume struct {

}

func (r *testAssume) Done() uint64 {
	return 1
}

func (r *testAssume) Cancel() {

}