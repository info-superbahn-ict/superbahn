package etcd_cli

import (
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/coreos/etcd/clientv3"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"time"
)

const (
	configEndPoint = ".endpoint"
	configClientTimeOut = ".timeout"
	configTestMode = ".test"
)

type Client struct {
	ctx context.Context
	prefix string

	cli *clientv3.Client
	argEndPoints []string
	argTimeOut int

	logger log.Logger

	isTest bool
}

func NewEtcdClient(ctx context.Context,prefix string)*Client {
	return &Client{
		ctx:    ctx,
		prefix: prefix,
		logger: log.NewLogger(prefix),
	}
}

func (r *Client) InitFlags(flags *pflag.FlagSet)  {
	flags.Bool(r.prefix + configTestMode,false,"set to test mode, the etcd is not used")
	flags.StringSlice(r.prefix+configEndPoint,[]string{"39.101.140.145:10000"},"etcd endpoints host:port")
	//flags.StringSlice(r.prefix+configEndPoint,[]string{"10.16.0.180:10000"},"etcd endpoints host:port")
	flags.Int(r.prefix+configClientTimeOut,5,"etcd connect time out")
}

func (r *Client) ViperConfig(viper *viper.Viper)  {

}

func (r *Client) InitViper(viper *viper.Viper)  {
	r.isTest = viper.GetBool(r.prefix + configTestMode)
	r.argEndPoints = viper.GetStringSlice(r.prefix+ configEndPoint)
	r.argTimeOut = viper.GetInt(r.prefix+ configClientTimeOut)
}

func (r *Client) OptionConfig(opts...option){
	for _,apply:= range opts {
		apply(r)
	}
}

func (r *Client) Initialize(opts...option) {
	for _,apply:= range opts {
		apply(r)
	}

	if r.isTest {
		r.logger.Infof("%v initialized",r.prefix)
		return
	}

	client,err := clientv3.New(clientv3.Config{
		Endpoints:   r.argEndPoints,
		DialTimeout: time.Duration(r.argTimeOut) * time.Second,
	})
	if err !=nil {
		r.logger.Errorf("supb envs initialized %v",err)
	}

	r.logger.Infof("%v initialized",r.prefix)
	r.cli = client
}

func (r *Client) Close()error{
	if r.isTest {
		r.logger.Infof("%v closed",r.prefix)
		return nil
	}

	//if r.isTest {
	//	_, _ = r.cli.Delete(context.Background(),"/",clientv3.WithPrefix())
	//}
	r.logger.Infof("%v closed",r.prefix)
	return nil
}

func (r *Client) WriteAssume(key string, value []byte) (supbapis.Assume,error) {
	r.logger.Debugf("WRITE")

	if r.isTest{
		return &testAssume{},nil
	}

	ctx, cancel := context.WithTimeout(r.ctx, time.Second)
	_, err := r.cli.Put(ctx, "/test", "test")
	cancel()
	if err != nil {
		return nil,fmt.Errorf("etcd %v", err)
	}

	return &writeAssume{
		cli: r.cli,
		key: key,
		value: string(value),
	},nil
}

func (r *Client) DeleteAssume(key string) (supbapis.Assume,error){
	r.logger.Debugf("WRITE")

	if r.isTest{
		return &testAssume{},nil
	}

	ctx, cancel := context.WithTimeout(r.ctx, time.Second)
	_, err := r.cli.Put(ctx, "/test", "test")
	cancel()
	if err != nil {
		return nil,fmt.Errorf("etcd %v", err)
	}

	return &deleteAssume{
		cli: r.cli,
		key: key,
	},nil
}

func (r *Client) WatchKey(prefix string) clientv3.WatchChan {
	r.logger.Debugf("WATCH")

	if r.isTest{
		ch :=make (chan clientv3.WatchResponse)
		return ch
	}

	return r.cli.Watch(r.ctx, prefix)
}

func (r *Client) WatchPrefix(prefix string) clientv3.WatchChan {
	r.logger.Debugf("WATCH PREFIX")
	return r.cli.Watch(r.ctx, prefix, clientv3.WithPrefix())
}

func (r *Client) Read(key string) ([]byte, error) {
	r.logger.Debugf("GET")

	ctx, cancel := context.WithTimeout(r.ctx, time.Second)
	defer cancel()

	resp, err :=r.cli.Get(ctx, key)
	if err != nil {
		return nil, fmt.Errorf("get to etcd failed, err:%v\n", err)
	}
	if len(resp.Kvs) > 0 {
		return resp.Kvs[0].Value, nil
	} else {
		return nil, fmt.Errorf("value is nil")
	}
}
func (r *Client) Put(key, value string) error {
	r.logger.Debugf("PUT")

	ctx, cancel := context.WithTimeout(r.ctx, time.Second)
	defer cancel()

	if _, err := r.cli.Put(ctx, key, value); err != nil {
		return fmt.Errorf("put to etcd failed, err:%v\n", err)
	}
	return nil
}

func (r *Client) Get(key string) ([]byte, error) {
	r.logger.Debugf("GET")

	ctx, cancel := context.WithTimeout(r.ctx, time.Second)
	defer cancel()

	resp, err :=r.cli.Get(ctx, key)
	if err != nil {
		return nil, fmt.Errorf("get to etcd failed, err:%v\n", err)
	}
	if len(resp.Kvs) > 0 {
		return resp.Kvs[0].Value, nil
	} else {
		return nil, fmt.Errorf("value is nil")
	}
}