package informer

import (
	"context"
	"sync"
)

type EventLabel string
type SubscriberLabel string
type PublisherLabel string

type SupbInformer struct {
	ctx context.Context
	prefix context.Context

	lock *sync.RWMutex
	SubscribeTable map[EventLabel]map[SubscriberLabel]chan interface{}
}

func NewInformer(ctx context.Context,prefix string)*SupbInformer {
	return &SupbInformer{
		lock: &sync.RWMutex{},
		SubscribeTable: make(map[EventLabel]map[SubscriberLabel]chan interface{}),
	}
}

func (r *SupbInformer)Subscribe(label SubscriberLabel, event EventLabel) <-chan interface{} {
	r.lock.Lock()
	defer r.lock.Unlock()

	subscriber,ok := r.SubscribeTable[event]
	if !ok {
		subscriber = make(map[SubscriberLabel]chan interface{})
	}

	msg := make(chan interface{})
	subscriber[label]= make(chan interface{})

	r.SubscribeTable[event]=subscriber
	return msg
}

func (r *SupbInformer)UnSubscribe(label SubscriberLabel, event EventLabel){
	r.lock.Lock()
	defer r.lock.Unlock()

	_ ,ok := r.SubscribeTable[event]
	if !ok {
		return
	}

	_,ok = r.SubscribeTable[event][label]
	if !ok {
		return
	}

	delete(r.SubscribeTable[event], label)
}

func (r *SupbInformer) Publish(label SubscriberLabel, event EventLabel, data interface{}) {
	
}