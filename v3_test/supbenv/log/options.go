package log

import "github.com/sirupsen/logrus"

type option func(log *logrus.Entry)

func WithPrefix(px string) option{
	return func(logger *logrus.Entry) {
		prefix = px
	}
}

func WithFormat(ft string) option{
	return func(logger *logrus.Entry) {
		format = ft
	}
}