package supbenv

import (
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
)

type option func(envs *SupbEnvs)

func WithFormat(format string) option{
	return func(envs *SupbEnvs) {
		log.OptionConfig(log.WithFormat(format))
	}
}