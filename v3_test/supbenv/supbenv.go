package supbenv

import (
	"bytes"
	"context"
	"fmt"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbapis"
	dli "gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/docker-cli"
	eli "gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/etcd-cli"
	"gitee.com/info-superbahn-ict/superbahn/v3_test/supbenv/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"strings"
)

const (
	recorderEtcd = "etcd"
	runnerDocker = "docker"
)

type SupbEnvs struct {
	ctx context.Context
	prefix string
	recorder *eli.Client
	runner *dli.Client
}

func NewSupbEnvs(ctx context.Context,prefix string)*SupbEnvs {
	return &SupbEnvs{
		ctx: ctx,
		prefix: prefix,
		recorder: eli.NewEtcdClient(ctx,strings.Join([]string{prefix, recorderEtcd},".")),
		runner: dli.NewDockerClient(ctx,strings.Join([]string{prefix, runnerDocker},".")),
	}
}

func (r *SupbEnvs) InitFlags(flags *pflag.FlagSet)  {
	log.InitFlags(flags)
	r.recorder.InitFlags(flags)
	r.runner.InitFlags(flags)
}

func (r *SupbEnvs) ViperConfig(viper *viper.Viper)  {
	log.ViperConfig(viper)
	r.recorder.ViperConfig(viper)
	r.runner.ViperConfig(viper)
}

func (r *SupbEnvs) InitViper(viper *viper.Viper)  {
	log.InitViper(viper)
	r.recorder.InitViper(viper)
	r.recorder.InitViper(viper)
}

func (r *SupbEnvs) OptionConfig(opts...option){
	for _,apply:= range opts {
		apply(r)
	}

	log.OptionConfig(log.WithPrefix(r.prefix+".log"))
}

func (r *SupbEnvs) Initialize(opts...option) {
	for _,apply:= range opts {
		apply(r)
	}

	log.Initialize()
	r.recorder.Initialize()
	r.runner.Initialize()
}

func  (r *SupbEnvs)Close()error{
	errs := new(bytes.Buffer)
	if err := r.recorder.Close();err!=nil{
		errs.WriteString(fmt.Sprintf("close recorder %v",err))
	}

	if err := log.Close();err!=nil{
		errs.WriteString(fmt.Sprintf("close recorder %v",err))
	}
	if errs.Len() > 0 {
		return fmt.Errorf("%v",errs.String())
	}
	return nil
}

func (r *SupbEnvs) Logger(prefix string) log.Logger {
	return log.NewLogger(prefix)
}

func (r *SupbEnvs) Recorder() supbapis.Recorder {
	return r.recorder
}

func (r *SupbEnvs) Runner() supbapis.Runner {
	return r.runner
}