# github.com/EDDYCJY/go-gin-example v0.0.0-20201228125222-28f372bf41f9
## explicit; go 1.13
github.com/EDDYCJY/go-gin-example/pkg/file
# github.com/Microsoft/go-winio v0.5.1
## explicit; go 1.12
github.com/Microsoft/go-winio
github.com/Microsoft/go-winio/pkg/guid
# github.com/Shopify/sarama v1.30.0 => github.com/Shopify/sarama v1.28.0
## explicit; go 1.13
github.com/Shopify/sarama
# github.com/StackExchange/wmi v1.2.1
## explicit; go 1.13
github.com/StackExchange/wmi
# github.com/Valiben/gin_unit_test v0.0.0-20181205064931-674aee46d090
## explicit
github.com/Valiben/gin_unit_test
github.com/Valiben/gin_unit_test/utils
# github.com/astaxie/beego v1.12.3
## explicit; go 1.13
github.com/astaxie/beego/logs
github.com/astaxie/beego/validation
# github.com/compose-spec/compose-go v1.0.8
## explicit; go 1.16
# github.com/compose-spec/godotenv v1.1.1
## explicit; go 1.16
# github.com/containerd/cgroups v1.0.1
## explicit; go 1.13
github.com/containerd/cgroups
github.com/containerd/cgroups/stats/v1
# github.com/containerd/containerd v1.5.8
## explicit; go 1.16
github.com/containerd/containerd/errdefs
github.com/containerd/containerd/log
github.com/containerd/containerd/platforms
# github.com/coreos/etcd v3.3.13+incompatible
## explicit
github.com/coreos/etcd/auth/authpb
github.com/coreos/etcd/clientv3
github.com/coreos/etcd/etcdserver/api/v3rpc/rpctypes
github.com/coreos/etcd/etcdserver/etcdserverpb
github.com/coreos/etcd/mvcc/mvccpb
github.com/coreos/etcd/pkg/types
# github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf
## explicit
# github.com/coreos/go-systemd/v22 v22.3.2
## explicit; go 1.12
github.com/coreos/go-systemd/v22/dbus
# github.com/davecgh/go-spew v1.1.1
## explicit
github.com/davecgh/go-spew/spew
# github.com/dgrijalva/jwt-go v3.2.0+incompatible
## explicit
github.com/dgrijalva/jwt-go
# github.com/distribution/distribution/v3 v3.0.0-20210316161203-a01c71e2477e
## explicit; go 1.15
# github.com/docker/compose-cli v1.0.22
## explicit; go 1.16
# github.com/docker/compose/v2 v2.2.0
## explicit; go 1.17
# github.com/docker/distribution v2.7.1+incompatible
## explicit
github.com/docker/distribution/digestset
github.com/docker/distribution/reference
github.com/docker/distribution/registry/api/errcode
# github.com/docker/docker v20.10.10+incompatible
## explicit
github.com/docker/docker/api
github.com/docker/docker/api/types
github.com/docker/docker/api/types/blkiodev
github.com/docker/docker/api/types/container
github.com/docker/docker/api/types/events
github.com/docker/docker/api/types/filters
github.com/docker/docker/api/types/image
github.com/docker/docker/api/types/mount
github.com/docker/docker/api/types/network
github.com/docker/docker/api/types/registry
github.com/docker/docker/api/types/strslice
github.com/docker/docker/api/types/swarm
github.com/docker/docker/api/types/swarm/runtime
github.com/docker/docker/api/types/time
github.com/docker/docker/api/types/versions
github.com/docker/docker/api/types/volume
github.com/docker/docker/client
github.com/docker/docker/errdefs
github.com/docker/docker/pkg/stdcopy
# github.com/docker/go-connections v0.4.0
## explicit
github.com/docker/go-connections/nat
github.com/docker/go-connections/sockets
github.com/docker/go-connections/tlsconfig
# github.com/docker/go-units v0.4.0
## explicit
github.com/docker/go-units
# github.com/eapache/go-resiliency v1.2.0
## explicit
github.com/eapache/go-resiliency/breaker
# github.com/eapache/go-xerial-snappy v0.0.0-20180814174437-776d5712da21
## explicit
github.com/eapache/go-xerial-snappy
# github.com/eapache/queue v1.1.0
## explicit
github.com/eapache/queue
# github.com/fsnotify/fsnotify v1.5.1
## explicit; go 1.13
github.com/fsnotify/fsnotify
# github.com/gin-contrib/cors v1.3.1
## explicit; go 1.13
github.com/gin-contrib/cors
# github.com/gin-contrib/sse v0.1.0
## explicit; go 1.12
github.com/gin-contrib/sse
# github.com/gin-gonic/gin v1.7.7
## explicit; go 1.13
github.com/gin-gonic/gin
github.com/gin-gonic/gin/binding
github.com/gin-gonic/gin/internal/bytesconv
github.com/gin-gonic/gin/internal/json
github.com/gin-gonic/gin/render
# github.com/go-ini/ini v1.32.1-0.20180214101753-32e4be5f41bb
## explicit
github.com/go-ini/ini
# github.com/go-logr/logr v1.2.0
## explicit; go 1.16
github.com/go-logr/logr
# github.com/go-ole/go-ole v1.2.5
## explicit; go 1.12
github.com/go-ole/go-ole
github.com/go-ole/go-ole/oleutil
# github.com/go-playground/locales v0.14.0
## explicit; go 1.13
github.com/go-playground/locales
github.com/go-playground/locales/currency
# github.com/go-playground/universal-translator v0.18.0
## explicit; go 1.13
github.com/go-playground/universal-translator
# github.com/go-playground/validator/v10 v10.9.0
## explicit; go 1.13
github.com/go-playground/validator/v10
# github.com/go-sql-driver/mysql v1.6.0
## explicit; go 1.10
github.com/go-sql-driver/mysql
# github.com/godbus/dbus/v5 v5.0.4
## explicit; go 1.12
github.com/godbus/dbus/v5
# github.com/gogo/protobuf v1.3.2
## explicit; go 1.15
github.com/gogo/protobuf/gogoproto
github.com/gogo/protobuf/jsonpb
github.com/gogo/protobuf/proto
github.com/gogo/protobuf/protoc-gen-gogo/descriptor
github.com/gogo/protobuf/sortkeys
github.com/gogo/protobuf/types
# github.com/golang-collections/go-datastructures v0.0.0-20150211160725-59788d5eb259
## explicit
github.com/golang-collections/go-datastructures/queue
# github.com/golang/protobuf v1.5.2
## explicit; go 1.9
github.com/golang/protobuf/proto
github.com/golang/protobuf/ptypes
github.com/golang/protobuf/ptypes/any
github.com/golang/protobuf/ptypes/duration
github.com/golang/protobuf/ptypes/timestamp
# github.com/golang/snappy v0.0.4
## explicit
github.com/golang/snappy
# github.com/gomodule/redigo v2.0.1-0.20180401191855-9352ab68be13+incompatible
## explicit
github.com/gomodule/redigo/internal
github.com/gomodule/redigo/redis
# github.com/google/go-cmp v0.5.6
## explicit; go 1.8
github.com/google/go-cmp/cmp
github.com/google/go-cmp/cmp/internal/diff
github.com/google/go-cmp/cmp/internal/flags
github.com/google/go-cmp/cmp/internal/function
github.com/google/go-cmp/cmp/internal/value
# github.com/google/gofuzz v1.1.0
## explicit; go 1.12
github.com/google/gofuzz
# github.com/googleapis/gnostic v0.5.5
## explicit; go 1.12
github.com/googleapis/gnostic/compiler
github.com/googleapis/gnostic/extensions
github.com/googleapis/gnostic/jsonschema
github.com/googleapis/gnostic/openapiv2
# github.com/gorilla/websocket v1.4.2
## explicit; go 1.12
github.com/gorilla/websocket
# github.com/hashicorp/errwrap v1.0.0
## explicit
# github.com/hashicorp/go-multierror v1.1.0
## explicit; go 1.14
# github.com/hashicorp/go-uuid v1.0.2
## explicit
github.com/hashicorp/go-uuid
# github.com/hashicorp/go-version v1.3.0
## explicit
# github.com/hashicorp/hcl v1.0.0
## explicit
github.com/hashicorp/hcl
github.com/hashicorp/hcl/hcl/ast
github.com/hashicorp/hcl/hcl/parser
github.com/hashicorp/hcl/hcl/printer
github.com/hashicorp/hcl/hcl/scanner
github.com/hashicorp/hcl/hcl/strconv
github.com/hashicorp/hcl/hcl/token
github.com/hashicorp/hcl/json/parser
github.com/hashicorp/hcl/json/scanner
github.com/hashicorp/hcl/json/token
# github.com/imdario/mergo v0.3.12
## explicit; go 1.13
github.com/imdario/mergo
# github.com/inconshreveable/mousetrap v1.0.0
## explicit
github.com/inconshreveable/mousetrap
# github.com/jaegertracing/jaeger v1.28.0
## explicit; go 1.17
github.com/jaegertracing/jaeger/model
# github.com/jcmturner/aescts/v2 v2.0.0
## explicit; go 1.13
github.com/jcmturner/aescts/v2
# github.com/jcmturner/dnsutils/v2 v2.0.0
## explicit; go 1.13
github.com/jcmturner/dnsutils/v2
# github.com/jcmturner/gofork v1.0.0
## explicit
github.com/jcmturner/gofork/encoding/asn1
github.com/jcmturner/gofork/x/crypto/pbkdf2
# github.com/jcmturner/gokrb5/v8 v8.4.2
## explicit; go 1.14
github.com/jcmturner/gokrb5/v8/asn1tools
github.com/jcmturner/gokrb5/v8/client
github.com/jcmturner/gokrb5/v8/config
github.com/jcmturner/gokrb5/v8/credentials
github.com/jcmturner/gokrb5/v8/crypto
github.com/jcmturner/gokrb5/v8/crypto/common
github.com/jcmturner/gokrb5/v8/crypto/etype
github.com/jcmturner/gokrb5/v8/crypto/rfc3961
github.com/jcmturner/gokrb5/v8/crypto/rfc3962
github.com/jcmturner/gokrb5/v8/crypto/rfc4757
github.com/jcmturner/gokrb5/v8/crypto/rfc8009
github.com/jcmturner/gokrb5/v8/gssapi
github.com/jcmturner/gokrb5/v8/iana
github.com/jcmturner/gokrb5/v8/iana/addrtype
github.com/jcmturner/gokrb5/v8/iana/adtype
github.com/jcmturner/gokrb5/v8/iana/asnAppTag
github.com/jcmturner/gokrb5/v8/iana/chksumtype
github.com/jcmturner/gokrb5/v8/iana/errorcode
github.com/jcmturner/gokrb5/v8/iana/etypeID
github.com/jcmturner/gokrb5/v8/iana/flags
github.com/jcmturner/gokrb5/v8/iana/keyusage
github.com/jcmturner/gokrb5/v8/iana/msgtype
github.com/jcmturner/gokrb5/v8/iana/nametype
github.com/jcmturner/gokrb5/v8/iana/patype
github.com/jcmturner/gokrb5/v8/kadmin
github.com/jcmturner/gokrb5/v8/keytab
github.com/jcmturner/gokrb5/v8/krberror
github.com/jcmturner/gokrb5/v8/messages
github.com/jcmturner/gokrb5/v8/pac
github.com/jcmturner/gokrb5/v8/types
# github.com/jcmturner/rpc/v2 v2.0.3
## explicit; go 1.13
github.com/jcmturner/rpc/v2/mstypes
github.com/jcmturner/rpc/v2/ndr
# github.com/jinzhu/gorm v1.9.16
## explicit; go 1.12
github.com/jinzhu/gorm
# github.com/jinzhu/inflection v1.0.0
## explicit
github.com/jinzhu/inflection
# github.com/josharian/intern v1.0.0
## explicit; go 1.5
github.com/josharian/intern
# github.com/json-iterator/go v1.1.12
## explicit; go 1.12
github.com/json-iterator/go
# github.com/klauspost/compress v1.13.6
## explicit; go 1.15
github.com/klauspost/compress
github.com/klauspost/compress/fse
github.com/klauspost/compress/huff0
github.com/klauspost/compress/internal/snapref
github.com/klauspost/compress/zstd
github.com/klauspost/compress/zstd/internal/xxhash
# github.com/leodido/go-urn v1.2.1
## explicit; go 1.13
github.com/leodido/go-urn
# github.com/magiconair/properties v1.8.5
## explicit; go 1.13
github.com/magiconair/properties
# github.com/mailru/easyjson v0.7.7
## explicit; go 1.12
github.com/mailru/easyjson
github.com/mailru/easyjson/buffer
github.com/mailru/easyjson/jlexer
github.com/mailru/easyjson/jwriter
# github.com/mattn/go-isatty v0.0.14
## explicit; go 1.12
github.com/mattn/go-isatty
# github.com/mattn/go-shellwords v1.0.12
## explicit; go 1.13
# github.com/mitchellh/mapstructure v1.4.2
## explicit; go 1.14
github.com/mitchellh/mapstructure
# github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd
## explicit
github.com/modern-go/concurrent
# github.com/modern-go/reflect2 v1.0.2
## explicit; go 1.12
github.com/modern-go/reflect2
# github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826
## explicit
# github.com/morikuni/aec v1.0.0
## explicit
# github.com/olivere/elastic v6.2.37+incompatible
## explicit
github.com/olivere/elastic
github.com/olivere/elastic/config
github.com/olivere/elastic/uritemplates
# github.com/olivere/elastic/v7 v7.0.27
## explicit; go 1.15
github.com/olivere/elastic/v7
github.com/olivere/elastic/v7/config
github.com/olivere/elastic/v7/uritemplates
# github.com/opencontainers/go-digest v1.0.0
## explicit; go 1.13
github.com/opencontainers/go-digest
# github.com/opencontainers/image-spec v1.0.2
## explicit
github.com/opencontainers/image-spec/specs-go
github.com/opencontainers/image-spec/specs-go/v1
# github.com/opencontainers/runtime-spec v1.0.3-0.20210326190908-1c3f411f0417
## explicit
github.com/opencontainers/runtime-spec/specs-go
# github.com/opentracing/opentracing-go v1.2.0
## explicit; go 1.14
github.com/opentracing/opentracing-go
github.com/opentracing/opentracing-go/ext
github.com/opentracing/opentracing-go/log
# github.com/pelletier/go-toml v1.9.4
## explicit; go 1.12
github.com/pelletier/go-toml
# github.com/pierrec/lz4 v2.6.1+incompatible
## explicit
github.com/pierrec/lz4
github.com/pierrec/lz4/internal/xxh32
# github.com/pkg/errors v0.9.1
## explicit
github.com/pkg/errors
# github.com/rcrowley/go-metrics v0.0.0-20201227073835-cf1acfcdf475
## explicit
github.com/rcrowley/go-metrics
# github.com/rs/cors v1.8.0
## explicit; go 1.13
github.com/rs/cors
# github.com/satori/go.uuid v1.2.0
## explicit
github.com/satori/go.uuid
# github.com/shiena/ansicolor v0.0.0-20151119151921-a422bbe96644
## explicit
github.com/shiena/ansicolor
# github.com/shirou/gopsutil v3.21.10+incompatible
## explicit
github.com/shirou/gopsutil/cpu
github.com/shirou/gopsutil/host
github.com/shirou/gopsutil/internal/common
github.com/shirou/gopsutil/mem
github.com/shirou/gopsutil/net
github.com/shirou/gopsutil/process
# github.com/sirupsen/logrus v1.8.1
## explicit; go 1.13
github.com/sirupsen/logrus
# github.com/spf13/afero v1.6.0
## explicit; go 1.13
github.com/spf13/afero
github.com/spf13/afero/mem
# github.com/spf13/cast v1.4.1
## explicit
github.com/spf13/cast
# github.com/spf13/cobra v1.2.1
## explicit; go 1.14
github.com/spf13/cobra
# github.com/spf13/jwalterweatherman v1.1.0
## explicit
github.com/spf13/jwalterweatherman
# github.com/spf13/pflag v1.0.5
## explicit; go 1.12
github.com/spf13/pflag
# github.com/spf13/viper v1.9.0
## explicit; go 1.12
github.com/spf13/viper
github.com/spf13/viper/internal/encoding
github.com/spf13/viper/internal/encoding/hcl
github.com/spf13/viper/internal/encoding/json
github.com/spf13/viper/internal/encoding/toml
github.com/spf13/viper/internal/encoding/yaml
# github.com/subosito/gotenv v1.2.0
## explicit
github.com/subosito/gotenv
# github.com/tklauser/go-sysconf v0.3.9
## explicit; go 1.13
github.com/tklauser/go-sysconf
# github.com/tklauser/numcpus v0.3.0
## explicit; go 1.11
github.com/tklauser/numcpus
# github.com/uber/jaeger-client-go v2.29.1+incompatible
## explicit
github.com/uber/jaeger-client-go
github.com/uber/jaeger-client-go/internal/baggage
github.com/uber/jaeger-client-go/internal/reporterstats
github.com/uber/jaeger-client-go/internal/spanlog
github.com/uber/jaeger-client-go/internal/throttler
github.com/uber/jaeger-client-go/log
github.com/uber/jaeger-client-go/thrift
github.com/uber/jaeger-client-go/thrift-gen/agent
github.com/uber/jaeger-client-go/thrift-gen/jaeger
github.com/uber/jaeger-client-go/thrift-gen/sampling
github.com/uber/jaeger-client-go/thrift-gen/zipkincore
github.com/uber/jaeger-client-go/utils
# github.com/uber/jaeger-lib v2.4.1+incompatible
## explicit
github.com/uber/jaeger-lib/metrics
# github.com/ugorji/go/codec v1.2.6
## explicit; go 1.11
github.com/ugorji/go/codec
# github.com/unknwon/com v1.0.1
## explicit
github.com/unknwon/com
# github.com/xeipuuv/gojsonpointer v0.0.0-20190905194746-02993c407bfb
## explicit
# github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415
## explicit
# github.com/xeipuuv/gojsonschema v1.2.0
## explicit
# go.uber.org/atomic v1.9.0
## explicit; go 1.13
go.uber.org/atomic
# go.uber.org/multierr v1.6.0
## explicit; go 1.12
go.uber.org/multierr
# go.uber.org/zap v1.19.1
## explicit; go 1.13
go.uber.org/zap
go.uber.org/zap/buffer
go.uber.org/zap/internal/bufferpool
go.uber.org/zap/internal/color
go.uber.org/zap/internal/exit
go.uber.org/zap/zapcore
# golang.org/x/crypto v0.0.0-20211117183948-ae814b36b871
## explicit; go 1.17
golang.org/x/crypto/md4
golang.org/x/crypto/pbkdf2
golang.org/x/crypto/sha3
# golang.org/x/net v0.0.0-20211209124913-491a49abca63
## explicit; go 1.17
golang.org/x/net/context
golang.org/x/net/context/ctxhttp
golang.org/x/net/http/httpguts
golang.org/x/net/http2
golang.org/x/net/http2/hpack
golang.org/x/net/idna
golang.org/x/net/internal/socks
golang.org/x/net/internal/timeseries
golang.org/x/net/proxy
golang.org/x/net/trace
# golang.org/x/oauth2 v0.0.0-20210819190943-2bc19b11175f
## explicit; go 1.11
golang.org/x/oauth2
golang.org/x/oauth2/internal
# golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
## explicit
# golang.org/x/sys v0.0.0-20211124211545-fe61309f8881
## explicit; go 1.17
golang.org/x/sys/cpu
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/plan9
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/term v0.0.0-20210615171337-6886f2dfbf5b
## explicit; go 1.17
golang.org/x/term
# golang.org/x/text v0.3.7
## explicit; go 1.17
golang.org/x/text/internal/language
golang.org/x/text/internal/language/compact
golang.org/x/text/internal/tag
golang.org/x/text/language
golang.org/x/text/secure/bidirule
golang.org/x/text/transform
golang.org/x/text/unicode/bidi
golang.org/x/text/unicode/norm
# golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac
## explicit
golang.org/x/time/rate
# google.golang.org/appengine v1.6.7
## explicit; go 1.11
google.golang.org/appengine/internal
google.golang.org/appengine/internal/base
google.golang.org/appengine/internal/datastore
google.golang.org/appengine/internal/log
google.golang.org/appengine/internal/remote_api
google.golang.org/appengine/internal/urlfetch
google.golang.org/appengine/urlfetch
# google.golang.org/genproto v0.0.0-20210828152312-66f60bf46e71
## explicit; go 1.11
google.golang.org/genproto/googleapis/rpc/status
# google.golang.org/grpc v1.41.0 => google.golang.org/grpc v1.26.0
## explicit; go 1.11
google.golang.org/grpc
google.golang.org/grpc/attributes
google.golang.org/grpc/backoff
google.golang.org/grpc/balancer
google.golang.org/grpc/balancer/base
google.golang.org/grpc/balancer/roundrobin
google.golang.org/grpc/binarylog/grpc_binarylog_v1
google.golang.org/grpc/codes
google.golang.org/grpc/connectivity
google.golang.org/grpc/credentials
google.golang.org/grpc/credentials/internal
google.golang.org/grpc/encoding
google.golang.org/grpc/encoding/proto
google.golang.org/grpc/grpclog
google.golang.org/grpc/health/grpc_health_v1
google.golang.org/grpc/internal
google.golang.org/grpc/internal/backoff
google.golang.org/grpc/internal/balancerload
google.golang.org/grpc/internal/binarylog
google.golang.org/grpc/internal/buffer
google.golang.org/grpc/internal/channelz
google.golang.org/grpc/internal/envconfig
google.golang.org/grpc/internal/grpcrand
google.golang.org/grpc/internal/grpcsync
google.golang.org/grpc/internal/resolver/dns
google.golang.org/grpc/internal/resolver/passthrough
google.golang.org/grpc/internal/syscall
google.golang.org/grpc/internal/transport
google.golang.org/grpc/keepalive
google.golang.org/grpc/metadata
google.golang.org/grpc/naming
google.golang.org/grpc/peer
google.golang.org/grpc/resolver
google.golang.org/grpc/serviceconfig
google.golang.org/grpc/stats
google.golang.org/grpc/status
google.golang.org/grpc/tap
# google.golang.org/protobuf v1.27.1
## explicit; go 1.9
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genid
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/order
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protodesc
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/descriptorpb
google.golang.org/protobuf/types/known/anypb
google.golang.org/protobuf/types/known/durationpb
google.golang.org/protobuf/types/known/timestamppb
# gopkg.in/inf.v0 v0.9.1
## explicit
gopkg.in/inf.v0
# gopkg.in/ini.v1 v1.63.2
## explicit
gopkg.in/ini.v1
# gopkg.in/yaml.v2 v2.4.0
## explicit; go 1.15
gopkg.in/yaml.v2
# gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
## explicit
gopkg.in/yaml.v3
# k8s.io/api v0.23.3
## explicit; go 1.16
k8s.io/api/admissionregistration/v1
k8s.io/api/admissionregistration/v1beta1
k8s.io/api/apiserverinternal/v1alpha1
k8s.io/api/apps/v1
k8s.io/api/apps/v1beta1
k8s.io/api/apps/v1beta2
k8s.io/api/authentication/v1
k8s.io/api/authentication/v1beta1
k8s.io/api/authorization/v1
k8s.io/api/authorization/v1beta1
k8s.io/api/autoscaling/v1
k8s.io/api/autoscaling/v2
k8s.io/api/autoscaling/v2beta1
k8s.io/api/autoscaling/v2beta2
k8s.io/api/batch/v1
k8s.io/api/batch/v1beta1
k8s.io/api/certificates/v1
k8s.io/api/certificates/v1beta1
k8s.io/api/coordination/v1
k8s.io/api/coordination/v1beta1
k8s.io/api/core/v1
k8s.io/api/discovery/v1
k8s.io/api/discovery/v1beta1
k8s.io/api/events/v1
k8s.io/api/events/v1beta1
k8s.io/api/extensions/v1beta1
k8s.io/api/flowcontrol/v1alpha1
k8s.io/api/flowcontrol/v1beta1
k8s.io/api/flowcontrol/v1beta2
k8s.io/api/networking/v1
k8s.io/api/networking/v1beta1
k8s.io/api/node/v1
k8s.io/api/node/v1alpha1
k8s.io/api/node/v1beta1
k8s.io/api/policy/v1
k8s.io/api/policy/v1beta1
k8s.io/api/rbac/v1
k8s.io/api/rbac/v1alpha1
k8s.io/api/rbac/v1beta1
k8s.io/api/scheduling/v1
k8s.io/api/scheduling/v1alpha1
k8s.io/api/scheduling/v1beta1
k8s.io/api/storage/v1
k8s.io/api/storage/v1alpha1
k8s.io/api/storage/v1beta1
# k8s.io/apimachinery v0.23.3
## explicit; go 1.16
k8s.io/apimachinery/pkg/api/errors
k8s.io/apimachinery/pkg/api/meta
k8s.io/apimachinery/pkg/api/resource
k8s.io/apimachinery/pkg/apis/meta/v1
k8s.io/apimachinery/pkg/apis/meta/v1/unstructured
k8s.io/apimachinery/pkg/conversion
k8s.io/apimachinery/pkg/conversion/queryparams
k8s.io/apimachinery/pkg/fields
k8s.io/apimachinery/pkg/labels
k8s.io/apimachinery/pkg/runtime
k8s.io/apimachinery/pkg/runtime/schema
k8s.io/apimachinery/pkg/runtime/serializer
k8s.io/apimachinery/pkg/runtime/serializer/json
k8s.io/apimachinery/pkg/runtime/serializer/protobuf
k8s.io/apimachinery/pkg/runtime/serializer/recognizer
k8s.io/apimachinery/pkg/runtime/serializer/streaming
k8s.io/apimachinery/pkg/runtime/serializer/versioning
k8s.io/apimachinery/pkg/runtime/serializer/yaml
k8s.io/apimachinery/pkg/selection
k8s.io/apimachinery/pkg/types
k8s.io/apimachinery/pkg/util/errors
k8s.io/apimachinery/pkg/util/framer
k8s.io/apimachinery/pkg/util/intstr
k8s.io/apimachinery/pkg/util/json
k8s.io/apimachinery/pkg/util/managedfields
k8s.io/apimachinery/pkg/util/naming
k8s.io/apimachinery/pkg/util/net
k8s.io/apimachinery/pkg/util/runtime
k8s.io/apimachinery/pkg/util/sets
k8s.io/apimachinery/pkg/util/validation
k8s.io/apimachinery/pkg/util/validation/field
k8s.io/apimachinery/pkg/util/wait
k8s.io/apimachinery/pkg/util/yaml
k8s.io/apimachinery/pkg/version
k8s.io/apimachinery/pkg/watch
k8s.io/apimachinery/third_party/forked/golang/reflect
# k8s.io/client-go v0.23.3
## explicit; go 1.16
k8s.io/client-go/applyconfigurations/admissionregistration/v1
k8s.io/client-go/applyconfigurations/admissionregistration/v1beta1
k8s.io/client-go/applyconfigurations/apiserverinternal/v1alpha1
k8s.io/client-go/applyconfigurations/apps/v1
k8s.io/client-go/applyconfigurations/apps/v1beta1
k8s.io/client-go/applyconfigurations/apps/v1beta2
k8s.io/client-go/applyconfigurations/autoscaling/v1
k8s.io/client-go/applyconfigurations/autoscaling/v2
k8s.io/client-go/applyconfigurations/autoscaling/v2beta1
k8s.io/client-go/applyconfigurations/autoscaling/v2beta2
k8s.io/client-go/applyconfigurations/batch/v1
k8s.io/client-go/applyconfigurations/batch/v1beta1
k8s.io/client-go/applyconfigurations/certificates/v1
k8s.io/client-go/applyconfigurations/certificates/v1beta1
k8s.io/client-go/applyconfigurations/coordination/v1
k8s.io/client-go/applyconfigurations/coordination/v1beta1
k8s.io/client-go/applyconfigurations/core/v1
k8s.io/client-go/applyconfigurations/discovery/v1
k8s.io/client-go/applyconfigurations/discovery/v1beta1
k8s.io/client-go/applyconfigurations/events/v1
k8s.io/client-go/applyconfigurations/events/v1beta1
k8s.io/client-go/applyconfigurations/extensions/v1beta1
k8s.io/client-go/applyconfigurations/flowcontrol/v1alpha1
k8s.io/client-go/applyconfigurations/flowcontrol/v1beta1
k8s.io/client-go/applyconfigurations/flowcontrol/v1beta2
k8s.io/client-go/applyconfigurations/internal
k8s.io/client-go/applyconfigurations/meta/v1
k8s.io/client-go/applyconfigurations/networking/v1
k8s.io/client-go/applyconfigurations/networking/v1beta1
k8s.io/client-go/applyconfigurations/node/v1
k8s.io/client-go/applyconfigurations/node/v1alpha1
k8s.io/client-go/applyconfigurations/node/v1beta1
k8s.io/client-go/applyconfigurations/policy/v1
k8s.io/client-go/applyconfigurations/policy/v1beta1
k8s.io/client-go/applyconfigurations/rbac/v1
k8s.io/client-go/applyconfigurations/rbac/v1alpha1
k8s.io/client-go/applyconfigurations/rbac/v1beta1
k8s.io/client-go/applyconfigurations/scheduling/v1
k8s.io/client-go/applyconfigurations/scheduling/v1alpha1
k8s.io/client-go/applyconfigurations/scheduling/v1beta1
k8s.io/client-go/applyconfigurations/storage/v1
k8s.io/client-go/applyconfigurations/storage/v1alpha1
k8s.io/client-go/applyconfigurations/storage/v1beta1
k8s.io/client-go/discovery
k8s.io/client-go/dynamic
k8s.io/client-go/kubernetes
k8s.io/client-go/kubernetes/scheme
k8s.io/client-go/kubernetes/typed/admissionregistration/v1
k8s.io/client-go/kubernetes/typed/admissionregistration/v1beta1
k8s.io/client-go/kubernetes/typed/apiserverinternal/v1alpha1
k8s.io/client-go/kubernetes/typed/apps/v1
k8s.io/client-go/kubernetes/typed/apps/v1beta1
k8s.io/client-go/kubernetes/typed/apps/v1beta2
k8s.io/client-go/kubernetes/typed/authentication/v1
k8s.io/client-go/kubernetes/typed/authentication/v1beta1
k8s.io/client-go/kubernetes/typed/authorization/v1
k8s.io/client-go/kubernetes/typed/authorization/v1beta1
k8s.io/client-go/kubernetes/typed/autoscaling/v1
k8s.io/client-go/kubernetes/typed/autoscaling/v2
k8s.io/client-go/kubernetes/typed/autoscaling/v2beta1
k8s.io/client-go/kubernetes/typed/autoscaling/v2beta2
k8s.io/client-go/kubernetes/typed/batch/v1
k8s.io/client-go/kubernetes/typed/batch/v1beta1
k8s.io/client-go/kubernetes/typed/certificates/v1
k8s.io/client-go/kubernetes/typed/certificates/v1beta1
k8s.io/client-go/kubernetes/typed/coordination/v1
k8s.io/client-go/kubernetes/typed/coordination/v1beta1
k8s.io/client-go/kubernetes/typed/core/v1
k8s.io/client-go/kubernetes/typed/discovery/v1
k8s.io/client-go/kubernetes/typed/discovery/v1beta1
k8s.io/client-go/kubernetes/typed/events/v1
k8s.io/client-go/kubernetes/typed/events/v1beta1
k8s.io/client-go/kubernetes/typed/extensions/v1beta1
k8s.io/client-go/kubernetes/typed/flowcontrol/v1alpha1
k8s.io/client-go/kubernetes/typed/flowcontrol/v1beta1
k8s.io/client-go/kubernetes/typed/flowcontrol/v1beta2
k8s.io/client-go/kubernetes/typed/networking/v1
k8s.io/client-go/kubernetes/typed/networking/v1beta1
k8s.io/client-go/kubernetes/typed/node/v1
k8s.io/client-go/kubernetes/typed/node/v1alpha1
k8s.io/client-go/kubernetes/typed/node/v1beta1
k8s.io/client-go/kubernetes/typed/policy/v1
k8s.io/client-go/kubernetes/typed/policy/v1beta1
k8s.io/client-go/kubernetes/typed/rbac/v1
k8s.io/client-go/kubernetes/typed/rbac/v1alpha1
k8s.io/client-go/kubernetes/typed/rbac/v1beta1
k8s.io/client-go/kubernetes/typed/scheduling/v1
k8s.io/client-go/kubernetes/typed/scheduling/v1alpha1
k8s.io/client-go/kubernetes/typed/scheduling/v1beta1
k8s.io/client-go/kubernetes/typed/storage/v1
k8s.io/client-go/kubernetes/typed/storage/v1alpha1
k8s.io/client-go/kubernetes/typed/storage/v1beta1
k8s.io/client-go/pkg/apis/clientauthentication
k8s.io/client-go/pkg/apis/clientauthentication/install
k8s.io/client-go/pkg/apis/clientauthentication/v1
k8s.io/client-go/pkg/apis/clientauthentication/v1alpha1
k8s.io/client-go/pkg/apis/clientauthentication/v1beta1
k8s.io/client-go/pkg/version
k8s.io/client-go/plugin/pkg/client/auth/exec
k8s.io/client-go/rest
k8s.io/client-go/rest/watch
k8s.io/client-go/restmapper
k8s.io/client-go/tools/auth
k8s.io/client-go/tools/clientcmd
k8s.io/client-go/tools/clientcmd/api
k8s.io/client-go/tools/clientcmd/api/latest
k8s.io/client-go/tools/clientcmd/api/v1
k8s.io/client-go/tools/metrics
k8s.io/client-go/tools/reference
k8s.io/client-go/transport
k8s.io/client-go/util/cert
k8s.io/client-go/util/connrotation
k8s.io/client-go/util/flowcontrol
k8s.io/client-go/util/homedir
k8s.io/client-go/util/keyutil
k8s.io/client-go/util/workqueue
# k8s.io/klog/v2 v2.30.0
## explicit; go 1.13
k8s.io/klog/v2
# k8s.io/kube-openapi v0.0.0-20211115234752-e816edb12b65
## explicit; go 1.16
k8s.io/kube-openapi/pkg/schemaconv
k8s.io/kube-openapi/pkg/util/proto
# k8s.io/utils v0.0.0-20211116205334-6203023598ed
## explicit; go 1.12
k8s.io/utils/clock
k8s.io/utils/clock/testing
k8s.io/utils/integer
k8s.io/utils/internal/third_party/forked/golang/net
k8s.io/utils/net
# sigs.k8s.io/json v0.0.0-20211020170558-c049b76a60c6
## explicit; go 1.16
sigs.k8s.io/json
sigs.k8s.io/json/internal/golang/encoding/json
# sigs.k8s.io/structured-merge-diff/v4 v4.2.1
## explicit; go 1.13
sigs.k8s.io/structured-merge-diff/v4/fieldpath
sigs.k8s.io/structured-merge-diff/v4/schema
sigs.k8s.io/structured-merge-diff/v4/typed
sigs.k8s.io/structured-merge-diff/v4/value
# sigs.k8s.io/yaml v1.2.0
## explicit; go 1.12
sigs.k8s.io/yaml
